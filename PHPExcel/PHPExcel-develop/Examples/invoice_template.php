<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
//error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** PHPExcel_IOFactory */
require_once dirname(__FILE__) . '/../Classes/PHPExcel/IOFactory.php';



echo date('H:i:s') , " Load from Excel5 template" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel5');
$objPHPExcel = $objReader->load("templates/invoice_template.xls");


echo date('H:i:s') , " Add new data to the template" , EOL;
$data = array(array('car_make'		=> 'KIA',
					'car_model'		=> 'Sorrento',
					'chassis_no'		=> '1245-2644',
					'manu_year'		=> 1998,
					'unit_price'	=> 52664
				   ),
			  array('car_make'		=> 'HYUNDAI',
					'car_model'		=> 'Sonata',
					'chassis_no'		=> '6643-2155',
					'manu_year'		=> 2008,
					'unit_price'	=> 65446
				   ),
			  array('car_make'		=> 'BMW',
					'car_model'		=> 'A6',
					'chassis_no'		=> '6656-4445',
					'manu_year'		=> 2015,
					'unit_price'	=> 55458
				   )

			 );


$data2  = array('customer_name' => 'Raul Jeong',
			  		'customer_id'   => 2547,
			  		'address'		=> 'Incheon, Korea',
			  		'car_price_type' => 'CIF',
			  		'phone_no'      => 'TEL : 032-254-2658',
			  		'country'		=> 'Korea',
			  		'invoice_no'	=> '214-2568-22555',
			  		'port_leaved_date' => '2014-08-12',
			  		'today_date'	=> '2015-02-15'
			  	   );

//$objPHPExcel->getActiveSheet()->setCellValue('D1', PHPExcel_Shared_Date::PHPToExcel(time()));

$baseRow = 23;
foreach($data as $r => $dataRow) {
	$row = $baseRow + $r;
	$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
	
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $r+1)
	                              ->setCellValue('K'.$row, $dataRow['car_make'])
	                              ->setCellValue('N'.$row, $dataRow['car_model'])
	                              ->setCellValue('O'.$row, $dataRow['chassis_no'])
	                              ->setCellValue('T'.$row, $dataRow['manu_year'])
	                              ->setCellValue('AH'.$row, $dataRow['unit_price']);
	                              //->setCellValue('E'.$row, '=C'.$row.'*D'.$row);
}


	$objPHPExcel->getActiveSheet()->setCellValue('J14',  $data2['customer_name'])
	                              ->setCellValue('AA14', $data2['customer_id'])
	                              ->setCellValue('J16',  $data2['address'])
	                              ->setCellValue('AA15', $data2['car_price_type'])
	                              ->setCellValue('J17', $data2['phone_no'])
	                              ->setCellValue('AA17', $data2['country'])
	                              ->setCellValue('J18', $data2['invoice_no'])
	                              ->setCellValue('AA18', $data2['port_leaved_date'])
	                              ->setCellValue('AC13', $data2['today_date']);


$objPHPExcel->getActiveSheet()->removeRow($baseRow-1,1);


echo date('H:i:s') , " Write to Excel5 format" , EOL;
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(str_replace('.php', '.xls', __FILE__));
echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;


// Echo memory peak usage
echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

// Echo done
echo date('H:i:s') , " Done writing file" , EOL;
echo 'File has been created in ' , getcwd() , EOL;
