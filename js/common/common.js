function MM_preloadImages() { //v3.0
var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
	var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
	var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
	var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_showHideLayers() { //v6.0
	var i,p,v,obj,args=MM_showHideLayers.arguments;
	for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
		if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
		obj.visibility=v; }
}
/*=============================================================================*
 * 앞자리의 연속된 Zero 값을 자른다.
 * param : sVal 입력스트링
 * return : String  Zero값을 자른 값
 *============================================================================*/
function trimZero(sVal) {
	var i;
	i = 0;
	while (sVal.substring(i,i+1) == '0') {
		i++;
	}
	return sVal.substring(i);
}

/*=============================================================================*
 * 입력값의 앞에 정해진 자리수만큼 0을 채운다.
 * param : sVal 입력스트링, iSize
 * return : String
 *============================================================================*/
function fillZero(sVal, iSize) {
	while(sVal.length < iSize) {
		sVal = "0" + sVal;
	}
	return sVal;
}


/*=============================================================================*
 * 길이가1인 경우 앞에 "0"을 붙인다.
 *
 * param : sVal 입력스트링
 *
 * return : String  "0"값을 포함하는 값
 *============================================================================*/
function addZero(sVal) {
	var iLen = sVal.length;   //인수값의 길이를 구한다.
	if(iLen == 1) {
		sVal = "0"+sVal;
	}
	else if(iLen == 0) {
		return;
	}
	return sVal;
}

/*=============================================================================*
 * 날짜 여부를 확인한다.(월일 or 년월 or 년월일)
 *
 * param : sYmd 입력스트링(MMDD or YYYYMM or YYYYMMDD)
 *
 * return : Boolean true이면 날짜 범위임
 *
 * 수정   : 월이나 일에 00 입력시 스크립트 에러. trimZero 부분을 수정(2003/11/19)
 *============================================================================*/
function isDate(sYmd) {
	var bResult;  // 결과값을 담는 변수(Boolean)

	switch (sYmd.length) {
		case 4://월일
			bResult = isDateMD(sYmd);
			break;
		case 6://년월
			bResult =  isDateYM(sYmd);
			break;
		case 8://년월일
			bResult =  isDateYMD(sYmd);
			break;
		default:
			bResult = false;  // 날짜 값이 아님
			break;
	}
	return bResult;
}

/*=============================================================================*
 * 날짜 여부를 확인한다.(년월일)
 *
 * param : sYmd 입력스트링(YYYYMMDD)
 *
 * return : Boolean true이면 날짜 범위임
 *============================================================================*/
function isDateYMD(sYmd) {
	// 길이 확인      //@@ 12.5 순서 변경
	if(sYmd.length != 8) {
		alert('Date error');
		return false;
	}

	// 숫자 확인
	if(!isNumber(sYmd)) {
		alert('Enter number here');
		return false;
	}

	var iYear = parseInt(sYmd.substring(0,4),10);  // 년도 입력(YYYY)
	var iMonth = parseInt(sYmd.substring(4,6),10);   //월입력(MM)
	var iDay = parseInt(sYmd.substring(6,8),10);     //일자입력(DD)

	if((iMonth < 1) ||(iMonth >12)) {
		alert('Date error');
		return false;
	}

	//각 달의 총 날수를 구한다
	var iLastDay = lastDay(sYmd.substring(0,6));  // 해당월의 마지말날 계산

	if((iDay < 1) || (iDay > iLastDay)) {
		alert('Date error');
		return false;
	}
	return true;
}

/*=============================================================================*
 * 날짜 여부를 확인한다.(월일)
 *
 * param : sMD 입력스트링(MMDD)
 *
 * return : Boolean true이면 날짜 범위임
 *============================================================================*/
function isDateMD(sMD) {
	// 숫자 확인
	if(!isNumber(sMD)) {
		alert('Enter number here');
		return false;
	}

	// 길이 확인
	if(sMD.length != 4) {
		alert('Enter date');
		return false;
	}

	var iMonth = parseInt(sMD.substring(0,2),10);  //해당월을 숫자값으로
	var iDay = parseInt(sMD.substring(2,4),10);    //해당일을 숫자값으로

	if((iMonth < 1) ||(iMonth >12)) {
		alert('Date error');
		return false;
	}

	//각 달의 총 날수를 구한다
	if (iMonth < 8 ) {
		var iLastDay = 30 + (iMonth%2);
	}

	else
	{
		var iLastDay = 31 - (iMonth%2);
	}

	if (iMonth == 2) {
		iLastDay = 29;
	}

	if((iDay < 1) || (iDay > iLastDay)) {
		alert('Date error');
		return false;
	}
	return true;
}

/*=============================================================================*
 * 날짜 여부를 확인한다.(년월)
 *
 * param : sYM 입력스트링(YYYYMM)
 *
 * return : Boolean true이면 날짜 범위임
 *============================================================================*/
function isDateYM(sYM) {
	// 숫자 확인
	if(!isNumber(sYM)) {
		alert('Enter number here');
		return false;
	}

	// 길이 확인
	if(sYM.length != 6) {
		alert('Enter date');
		return false;
	}

	var iYear = parseInt(sYM.substring(0,4),10); //년도값을 숫자로
	var iMonth = parseInt(sYM.substring(4,6),10);  //월을 숫자로

	if((iMonth < 1) ||(iMonth >12)) {
		alert('Date error');
		return false;
	}
	return true;
}




/*=============================================================================*
 * 년월을 입력받아 마지막 일를 반환한다(년월)
 *
 * param : sYM 입력스트링(YYYYMM)
 *
 * return : String 해당월의 마지막날
 *============================================================================*/
function lastDay(sYM) {
	if(sYM.length != 6) {
		alert("Enter date");
		return;
	}

	if(!isDateYM(sYM)) {
		return;
	}

	daysArray = new makeArray(12);    // 배열을 생성한다.

	for (i=1; i<8; i++) {
		daysArray[i] = 30 + (i%2);
	}
	for (i=8; i<13; i++) {
		daysArray[i] = 31 - (i%2);
	}
	var sYear = sYM.substring(0, 4) * 1;
	var sMonth    = sYM.substring(4, 6) * 1;

	if (((sYear % 4 == 0) && (sYear % 100 != 0)) || (sYear % 400 == 0)) {
		daysArray[2] = 29;
	}
	else
	{
		daysArray[2] = 28;
	}

	return daysArray[sMonth].toString();
}


/*=============================================================================*
 * 대소문자를 포함한 영문자인지 확인한다.
 *
 * param : sVal 입력문자열
 *
 * return : Boolean true이면 알파벳
 *============================================================================*/
function isAlpha(sVal) {
	// Alphabet 값
	var sAlphabet="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	var iLen=sVal.length;   //입력값의 길이

	for(i=0;i<iLen;i++) {
		if(sAlphabet.indexOf(sVal.substring(i,i+1))<0) {
			alert("This text is no allowed");
			return false;
		}
	}
	return true;
}

/*=============================================================================*
 * 영문자와 숫자 구성된 문자열인지 확인
 *
 * param : sVal 입력문자열
 *
 * return : Boolean true이면 영문자,숫자로 구성된 문자열
 *============================================================================*/
function isAlphaNumeric(sVal) {
	var sAlphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	var iLen      = sVal.length;

	for ( i = 0; i < iLen; i++ ) {
		if ( sAlphabet.indexOf(sVal.substring(i, i+1)) < 0 ) {
			return false;
		}
	}
	return true;
}

/*=============================================================================*
 * 문자열의 길이를 return (한글:2자)
 *
 * param : sVal 입력문자열
 *
 * return : int 입력문자열의 길이
 *============================================================================*/
function strLength(sVal) {
	var sBit = '';    // 문자열의 문자(Char)
	var iLen = 0; //문자열 길이

	for ( i = 0 ; i < sVal.length ; i++ ) {
		sBit = sVal.charAt(i);
		if ( escape( sBit ).length > 4 ) {
			iLen = iLen + 2;
		}
		else
		{
			iLen = iLen + 1;
		}
	}
	return iLen;
}


/*=============================================================================*
 * 문자열 길이 체크
 * param : str 필드객체, field 필드명
 * return : boolean
 *============================================================================*/
function chkStrLength(str,field) {
	iSize = str.getAttribute("Maxlength")

	if (field == null)
		field = '';

	if ( strLength(str.value) > iSize) {
		//        if (flag=1)
		alert("Maximum length is " +iSize);
		//        else
		//            alert(field+" 최대길이는 "+iSize+"자 입니다.");
		str.select();
		str.focus();
		return false;
	}

	return true;
}


/*=============================================================================*
 * 입력받은 날짜로부터 몇일 후의 날짜를 반환하기
 *
 * param : ObjDate객체, 일수, 결과Data객체
 *
 * return :
 *============================================================================*/
function calcDate(objDate,iDay,objResultDate) {
	daysArray = new makeArray(12); //월별 공간을 생성

	for(i=1; i<13; i++) {
		daysArray[i] = 30 + (i%2);
	}

	var sYear      = objDate.value.substring(0, 4) * 1;
	var sMonth     = objDate.value.substring(4, 6) * 1;
	var sDay       = objDate.value.substring(6, 8) * 1;

	daysArray[2] = lastDay(sYear + "02");

	var iMoveRemain = iDay * 1 + sDay;
	var iCurMonth   = sMonth;
	var iCurYear    = sYear;

	while (iMoveRemain > daysArray[iCurMonth]) {
		iMoveRemain = iMoveRemain - daysArray[iCurMonth];

		iCurMonth = iCurMonth + 1;
		if (iCurMonth > 12) {
			iCurMonth = 1;
			iCurYear = iCurYear + 1;
			daysArray[2] = lastDay(iCurYear + "02");
		}
	} //end of while

	iCurMonth = addZero(iCurMonth.toString());
	iMoveRemain = addZero(iMoveRemain.toString());

	objResultDate.value = iCurYear + iCurMonth + iMoveRemain;
}

/*=============================================================================*
 * 숫자 0으로 초기화 된 1차원 배열을 생성한다.
 *
 * param : iSize 배열 크기
 *
 * return : this 배열
 *============================================================================*/
function makeArray(iSize) {
	this.length = iSize;

	for (i = 1; i <= iSize; i++) {
		this[i] = 0;
	}
	return this;
}

/*=============================================================================*
 * 숫자 분리자(,)(.)가 있는 숫자이거나 일반숫자형태인지 검사한다.
 *
 * param : sVal
 *
 * return : Boolean
 *============================================================================*/
function isMoneyNumber(sVal) {
	var iAbit;

	if (sVal.length < 1) return true;
	for (i=0; i<sVal.length; i++) {
		iAbit = parseInt(sVal.substring(i,i+1));
		if (!(('0' < iAbit) || ('9' > iAbit))) {
			if (sVal.substring(i, i+1) == ',' || sVal.substring(i, i+1) == '.' ) {
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}

/*=============================================================================*
 * 숫자 분리자(,)만 있는 숫자이거나 일반숫자형태인지 검사한다.
 *
 * param : sVal
 *
 * return : Boolean
 *============================================================================*/
function isMoneyNumber2(sVal) {
	var iAbit;

	if (sVal.length < 1) return true;
	for (i=0; i<sVal.length; i++) {
		iAbit = parseInt(sVal.substring(i,i+1));
		if (!(('0' < iAbit) || ('9' > iAbit))) {
			if (sVal.substring(i, i+1) == ',') {
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}

/*=============================================================================*
 * 숫자 분리자(.)만 있는 숫자이거나 일반숫자형태인지 검사한다.
 * param : sVal
 * return : Boolean
 *============================================================================*/
function isMoneyNumber3(sVal) {
	var iAbit;
	var deci_cnt = 0;
	if (sVal.length < 1) return true;
	for (i=0; i<sVal.length; i++) {
		iAbit = parseInt(sVal.substring(i,i+1));
		if (!(('0' < iAbit) || ('9' > iAbit))) {
			if (sVal.substring(i, i+1) == '.' ) {
				deci_cnt = deci_cnt + 1;//소수점 이하가 있는지 파악 (1이면 소수점 이하 존재)
			}
			else
			{
				return false;
			}
		}
	}
	if (deci_cnt > 1) {
		return false;
	}
	return true;
}
/*=============================================================================*
 * 숫자 분리자(.)만 있는 숫자인지 검사한다.
 *
 * param : sVal
 *
 * return : Boolean
 *============================================================================*/
function isMoneyNumber4(sVal){
	var deci_cnt = 0;
	for (i=0; i<sVal.length; i++) {

		if (sVal.substring(i, i+1) == '.' ){
			deci_cnt = deci_cnt + 1;//소수점있는지여부
		}

	}
	if (deci_cnt > 0) {
		return true;
	}
	return false;
}


/*=============================================================================*
 * 소수점이 있는 숫자이면서 정해진 자릿수에 맞는 형식인지 확인한다.
 * param : sVal 입력객체, iSize1 정수자릿수, iSize2 소수자릿수
 * return : boolean
 *============================================================================*/
function isMoneyNumber5(sVal, iSize1, iSize2) {
	if(isMoneyNumber(sVal))        // ,나 .가 들어가는 숫자인지 확인
	{
		var e = sVal.value;
		e = e.split(".");
		e[0] = numOffMask(e[0]);
		if (!e[1]) {
			e[1] = 0;
		}

		var aVal = e[0] + "." + e[1];

		if (isNumberDot(aVal)) {
			// 입력된 값이 설정된 정수자릿수 또는 소숫점 이하보다 크면 false
			if (e[0].length > iSize1 || e[1].length > iSize2)
				return false;
			else
				return true;
		}
		else {
			return false;
		}

	}
	else {
		return false;
	}
}



/*=============================================================================*
 * 소수점 숫자표현(소수점 위의 3자리마다 "," 맞춤)
 * param : val
 *
 * return : String
 *============================================================================*/
function getMoneyType(val) {
	if (typeof val == "number") {
		val = val.toString();
	}

	var value = getOnlyNumberDot(val);

	var sResult = "";

	if (value.length == 0) {
		alert("Enter number here");
		return;
	}

	if (! isMoneyNumber(value)) {
		alert("Enter number here");
		return;
	}

	var nI;
	var nJ = -1;
	var subOne;
	var flag = false;

	for (nI = value.length - 1; nI >= 0; nI--) {
		subOne = value.substring(nI, nI + 1);
		sResult = subOne + sResult;


		if (subOne == '.') {
			flag = true;
		}

		if (flag == true) {
			nJ = nJ + 1;
		}

		if ((nJ % 3 == 0) && (nI != 0) && (nJ != 0)) {
			sResult = "," + sResult;
		}
	}
	return sResult;
}

/*=============================================================================*
 * 부호가 있는 소수점 숫자표현(소수점 위의 3자리마다 "," 맞춤)
 *
 * param : val
 *
 * return : String
 *============================================================================*/
function getSignMoneyType(val) {
	if (typeof val == "number") {
		val = val.toString();
	}

	var s1    = val.substring(0,1);
	var slen    = val.length;
	var sign    = "";
	var ret        = "";
	if (val == "-Infinity") {
		return "0";
	}

	if(slen>1 ) {
		if(s1 == "-") {
			sign = "-";
			ret = sign + getMoneyType(val.substring(1,slen));
		}
		else
		{
			ret = getMoneyType(val);
		}
	}
	else
	{
		ret = val;
	}
	return  ret;
}

/*=============================================================================*
 * 콤마를 제거한 숫자형태 문자열로 반환(부호와 소수점도 없앰)
 *
 * param : val
 *
 * return : String
 *============================================================================*/
function getOnlyNumber(val) {
	var value = "";
	var abit;

	if (typeof val != "number" && typeof val !="string") {
		return "0";
	}
	if (val.length < 1) {
		return "0";
	}
	if (val == "NaN") {
		return "0";
	}
	if (val == "-Infinity") {
		return "0";
	}

	for (i=0;i<val.length;i++) {
		abit = parseInt(val.substring(i,i+1));
		if (('0' < abit) || ('9' > abit) ) {
			value = value + abit;
		}

	}
	return value;
}



/*=============================================================================*
 * 콤마를 제거한 숫자형태 문자열로 반환(부호, 소수점 그대로)
 *
 * param : val
 *
 * return : String
 *============================================================================*/
function getOnlyNumberDot(val) {

	if (typeof val != "number" && typeof val !="string") {
		return "0";
	}
	if (val.length < 1) {
		return "0";
	}
	if (val == "NaN") {
		return "0";
	}
	if (val == "-Infinity") {
		return "0";
	}

	var value = "";
	var abit; // 소수부분

	var delimter = val.indexOf(".");
	var numberInteger = ""; // 정수부분

	if(delimter < 0) {
		numberInteger = val;
		abit ='';
	} else {
		numberInteger = val.substring(0,delimter);
		abit = val.substring(delimter+1);
	}

	var number="";
	var leng=numberInteger.length ;
	for(i=0 ; i<leng ; i++) {
		var tmp = numberInteger.substring(i,i+1);
		if(tmp != ",") {
			number = number+tmp;
		}
	}

	if(abit.length==0) {
		value=number;
	}
	else
	{
		value = number+"."+abit;
	}
	return value;

}



/*=============================================================================*
 * 콤마를 제거한 부호가 있는 숫자형태 문자열로 반환
 *
 * param : val
 *
 * return : String
 *============================================================================*/
function getOnlySignNumber(val) {
	if (val == "-") return 0;
	var price = eval(getOnlyNumber(val));
	if (val.substring(0,1) == "-") {
		price *= -1;
	}
	return price;
}

/*=============================================================================*
 * 앞뒤 공백을 제거한다.
 *
 * param : sVal
 *
 * return : String
 *============================================================================*/
function Trim(sVal) {
	return(LTrim(RTrim(sVal)));
}

/*=============================================================================*
 * 앞 공백을 제거한다.
 *
 * param : sVal
 *
 * return : String
 *============================================================================*/
function LTrim(sVal) {
	var i;
	i = 0;
	while (sVal.substring(i,i+1) == ' ') {
		i++;
	}
	return sVal.substring(i);
}

/*=============================================================================*
 * 뒤 공백을 제거한다.
 *
 * param : sVal
 *
 * return : String
 *============================================================================*/
function RTrim(sVal) {
	var i = sVal.length - 1;
	while (i >= 0 && sVal.substring(i,i+1) == ' ') {
		i--;
	}
	return sVal.substring(0,i+1);

}


//------------------------------------------------------------------------------
// DESCRIPTION  : 공백문자 제거
// 함수명       : MTrim(공백이 있는 문자열)
// Return Value : 공백이 제거된 문자열
//------------------------------------------------------------------------------
function MTrim(sVal){
	var strOri = sVal;
	var space = " ";

	while (strOri.indexOf(space) != -1){
		strOri = strOri.replace(space, "");
	}

	return strOri;
}

/*=============================================================================*
 * 공백만 존재하거나 아무것도 없는지 확인한다.
 *
 * param : sVal
 *
 * return : boolean (true이면 공백이나 Empty이다)
 *============================================================================*/
function isEmpty(sVal){
	if (MTrim(sVal) == '') {
		return true;
	}
	return false;
}

/*=============================================================================*
 * 현재 컨트롤과 MaxLength 받아서 MaxLength 되면 다음 컨트롤로 이동
 * use onKeyUp
 * param : objCurrent, objNext
 *
 * return :
 *============================================================================*/
function focusMove(objCurrent, objNext) {
	if ( objCurrent.getAttribute("Maxlength") == objCurrent.value.length) {
		objNext.focus();
	}
}

/*=============================================================================*
 * 현재 컨트롤과 MaxLength 받아서 MaxLength 되면 다음 컨트롤로 이동(선택)
 * param : objCurrent, objNext
 * return :
 *============================================================================*/
function focusMoveSelect(objCurrent, objNext) {
	if ( objCurrent.getAttribute("Maxlength") == objCurrent.value.length) {
		objNext.focus();
		objNext.select();
	}
}


/*=============================================================================*
 * 완료된 날짜값에 대해 "/" 추가
 * param : me(value)
 * return : String
 *============================================================================*/
function calOnMask(me){

	if (event.keyCode<48||event.keyCode>57){//숫자외금지
		event.returnValue=false;
	}
	if(me.length > 3 ) {
		var a1 = me.substring(0,4) + "/";
		var a2 = me.substr(4,me.length);
		var a3 = "";
		if (me.length > 5){
			a2 = me.substring(4,6) + "/";
			a3 = me.substr(6,me.length);
		}

		me= a1 + a2 + a3;

	}
	return me;
}


/*=============================================================================*
 * 날짜값 "/" 제거
 * param :  me(value)
 * return : String "/" 제거된 날짜값
 *============================================================================*/
function calOffMask(me){
	var tmp=me.split("/");
	tmp=tmp.join("");
	return tmp;
}


/*=============================================================================*
 * 날짜값 자동 "/" 붙임.(완성된 날짜값에 대해 /붙임)
 *
 * param :
 *
 * return :
 *============================================================================*/
function cal_value2(me){

	if(me.length == 8 ) {
		var a1 = me.substring(0,4) + "/";
		var a2 = me.substring(4,6) + "/";
		var    a3 = me.substr(6,me.length);

		me= a1 + a2 + a3;

	}
	return me;
}


/*=============================================================================*
 * 오늘 날짜 생성 ( "/" 붙여서 리턴)
 *
 * param :
 *
 * return : todate
 *============================================================================*/
function todate() {
	var now=new Date()
	var jyear = now.getYear();
	var month=now.getMonth() + 1;
	var jmonth = month + "";
	if (jmonth.length < 2) {
		jmonth = "0" + jmonth;
	}
	var dat=now.getDate();
	var jdate = dat + "";
	if (jdate.length < 2) {
		jdate = "0" + jdate;
	}

	//var day=Birdy.getDay()//요일
	var tdy = jyear+"/"+jmonth+"/"+jdate;
	return tdy;
}



/*--------------------------------------------------------------------------------------------
Spec      : 숫자입력시 3자리마다 자동으로 콤마 찍기
Argument : string
Return   : string
Example  : onkeyup="comma_value(this)"
---------------------------------------------------------------------------------------------*/
function comma_value(sval) {
	if (event.keyCode != 9 && event.keyCode != 37 && event.keyCode != 39) {
		var cur = sval.value;
		var setMinus = 0;

		if (cur.charAt(0) == "-") {
			setMinus = 1;
		}

		cur=cur.replace(/[^.0-9]/g ,"");
		cur=cur.replace(/[.]+/g ,".");

		if (setMinus == 1)
			sval.value = "-" + formatNumbertoString(cur);
		else
			sval.value = formatNumbertoString(cur);
	}
}


/*--------------------------------------------------------------------------------------------
Spec      : 숫자입력시 3자리마다 자동으로 콤마 찍기
Argument : string
Return   : string
Example  : onkeyup="comma_value(str)"
---------------------------------------------------------------------------------------------*/
function formatNumbertoString(cur) {
	leftString = cur;
	rightString = ".";
	dotIndex = 0;

	for(i = 0; i < cur.length; i++){
		// 1) '.'이 처음에 입력 되었을때 앞에 0을 더해 "0."을 리턴
		// 2) "0."이외의 입력 일 때 "0"만 리턴
		if(cur.charAt(i) == "." || (cur.length > 1 && cur.charAt(0) == "0" && cur.charAt(1) != ".")) {
			dotIndex = i;
			if(dotIndex == 0) {
				if   (cur.charAt(0) == ".")   leftString="0.";
				else                          leftString="";
				return leftString;
			}
			break;
		}
	}

	if(dotIndex != 0)    //dot가 있을 경우..
	{
		leftString = cur.substr(0, dotIndex);
		rightString = cur.substr(dotIndex+1);
		rightString = rightString.replace(/\./g,"");
	}
	else //없으면..
	{
		leftString = cur;
	}

	len=leftString.length-3;
	while(len>0) {
		leftString=leftString.substr(0,len)+","+leftString.substr(len);
		len-=3;
	}

	if(rightString != ".")
		return (leftString + "." + rightString);
	else
		return leftString;
}

// 숫자만 입력 (소수점 허용, 음수 허용)
// 사용법 : onKeyPress = onlyNum();
function onlyNum() {
	if (event.keyCode < 45 || event.keyCode > 57 || event.keyCode == 47)
		event.returnValue = false;
}

// 숫자만 입력 (소수점 허용, 음수 불가)
// 사용법 : onKeyPress = onlyNum2();
function onlyNum2() {
	if (event.keyCode < 46 || event.keyCode > 57 || event.keyCode == 47)
		event.returnValue = false;
}

// 숫자만 입력 (소수점 불가, 음수 허용)
// 사용법 : onKeyPress = onlyNum3();
function onlyNum3() {
	if (event.keyCode < 45 || event.keyCode > 57 || event.keyCode == 46 || event.keyCode == 47)
		event.returnValue = false;
}

// 숫자만 입력 (소수점 불가, 음수 불가)
// 사용법 : onKeyPress = onlyNum4();
function onlyNum4() {
	if (event.keyCode < 48 || event.keyCode > 57)
		event.returnValue=false;
}



function onlyChar(sval) {
	var sBit = '';
	str = sval.value;

	for(i=0;i<str.length;i++) {
		sBit = str.charAt(i);

		if(escape( sBit ).length <= 4) {
			var sAlphabet="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

			if(sAlphabet.indexOf(sBit) < 0) {
				alert("Enter character here");
				return false;
			}
		}
	}
	return true;
}



/*=============================================================================*
 * 입력완료된 숫자값에 대하여 콤마를 찍어줄때 사용(소수점 이하 처리 안됨)
 * 콤마 형식을 사용할 경우에는 onkeyup이벤트로 사용하기 바라며,
 * 다음의 펑션을 호출할때는 comma_value(me) 펑션을 호출하기 바람.
 * param : value
 *============================================================================*/
function numOnMask(me){
	var tmpH = null;
	if(me.charAt(0)=="-"){//음수가 들어왔을때 '-'를 빼고적용되게..
		tmpH=me.substring(0,1);
		me=me.substring(1,me.length);
	}    //me.indexOf('-')
	if(me.length > 3){
		var c=0;
		var myArray=new Array();
		for(var i=me.length;i>0;i=i-3){
			myArray[c++]=me.substring(i-3,i);
		}
		myArray.reverse();
		me=myArray.join(",");
	}
	if(tmpH){
		me=tmpH+me;
	}
	return me;
}

/*=============================================================================*
 * 콤마가 들어간 숫자에서 ","를 뺀다.
 * param : value
 *============================================================================*/
function numOffMask(me){
	var tmp=me.split(",");
	tmp=tmp.join("");
	return tmp;
}

// 입력 완료된 숫자 값에 컴마를 적용하여준다(소수점 이하는 "," 안 붙음)
// return : String
function numOnMask2(me){
	var tmpH;
	if(!isMoneyNumber4(me)) {
		if(me.charAt(0)=="-"){//음수가 들어왔을때 '-'를 빼고적용되게..
			tmpH=me.substring(0,1);
			me=me.substring(1,me.length);
		}    //me.indexOf('-')
		if(me.length > 3){
			var c=0;
			var myArray=new Array();
			for(var i=me.length;i>0;i=i-3){
				myArray[c++]=me.substring(i-3,i);
			}
			myArray.reverse();
			me=myArray.join(",");
		}
		if(tmpH){
			me=tmpH+me;
		}
	}else{
		var e = me;
		e = e.split(".");
		var myStr = e[0];
		//alert(myStr);
		if(myStr.charAt(0)=="-"){//음수가 들어왔을때 '-'를 빼고적용되게..
			tmpH=myStr.substring(0,1);
			myStr=myStr.substring(1,me.length);
		}    //me.indexOf('-')
		if(myStr.length > 3){
			var c=0;
			var myArray=new Array();
			for(var i=myStr.length;i>0;i=i-3){
				myArray[c++]=myStr.substring(i-3,i);
			}
			myArray.reverse();
			myStr=myArray.join(",");
		}
		if(tmpH){
			me=tmpH+myStr+"."+e[1];
		}
		else {
			me=myStr+"."+e[1];
		}
	}

	return me;
}


// 입력 완료된 숫자 값에 컴마를 적용하고 소수점 이하는 삭제한다
// return : String
function numOnMask3(me){ //단순히 값에 컴마를 적용할때 사용
	var tmpH;
	if(isMoneyNumber3(me)) { // 양수&음수 체크 (true : 양수, false : 음수)
		var e = me;
		e = e.split(".");
		var myStr = e[0];
		if(myStr.length > 3){
			var c=0;
			var myArray=new Array();
			for(var i=myStr.length;i>0;i=i-3){
				myArray[c++]=myStr.substring(i-3,i);
			}
			myArray.reverse();
			myStr=myArray.join(",");
		}
		me = myStr;
	}else{ // 음수 일때
		var e = me;
		e = e.split(".");
		var myStr = e[0];
		if(myStr.charAt(0)=="-"){//음수가 들어왔을때 '-'를 빼고적용되게..
			tmpH=myStr.substring(0,1);
			myStr=myStr.substring(1,me.length);
		}
		if(myStr.length > 3){
			var c=0;
			var myArray=new Array();
			for(var i=myStr.length;i>0;i=i-3){
				myArray[c++]=myStr.substring(i-3,i);
			}
			myArray.reverse();
			myStr=myArray.join(",");
		}
		if(tmpH){
			me=tmpH+myStr;
		}
		else {
			me=myStr;
		}
	}

	return me;
}


/*=============================================================================*
 * 입력값을 소수점 이하 몇 자리까지 보여줄지 정한다.
 * 소수점 이하 자리수가 입력된 값보다 작으면 0으로 채운다.
 * param : sVal 입력스트링, iSize 소수자릿수
 * return : String
 *============================================================================*/
function numOnMask4(sVal,iSize) {
	if(isNumberDot(sVal))        // 숫자형인지 확인
	{
		var e = sVal;
		e = e.split(".");
		if (!e[1]) {
			if (iSize == 0) {
				sVal = numOnMask(e[0]);
				return sVal;
			}
			else {
				e[1] = "0";
			}
		}
		while (e[1].length < iSize) {    // 주어진 소숫점 이하 자릿수 만큼 뒤에 "0" 추가
			e[1] = e[1] + "0";
		}
		sVal = numOnMask(e[0]) + "." + e[1].substr(0,iSize);
		return sVal;
	}
	else {
		return false;
	}
}



/*=============================================================================*
 * 입력값에 마스킹을 적용한다.(소수점 이하와 부호를 삭제하고 콤마추가)
 * param : sVal 입력스트링
 * return : String
 *============================================================================*/
function numOnMask5(sVal) {
	var e = sVal;
	e = e.split(".");

	if(!isMoneyNumber3(e[0]))                // true이면 양수, false이면 음수
		e[0] = e[0].substring(1)

	return numOnMask(e[0]);
}


/*=============================================================================*
 * 입력값에 마스킹을 적용한다.(부호를 삭제하고 콤마추가. 소수점은 그대로 둠)
 * param : sVal 입력스트링
 * return : String
 *============================================================================*/
function numOnMask6(sVal) {
	var e = sVal;
	e = e.split(".");

	if(!isMoneyNumber3(e[0]))        // isMoneyNumber3 - true이면 양수, false이면 음수
		e[0] = e[0].substring(1);

	return numOnMask(e[0]) + "." + e[1];
}



/*=============================================================================*
 * 숫자 외의 값이 입력되어있으면 false 리턴
 * param : sval (object)
 * return :
 *============================================================================*/
function onlyNumber(sval) {
	var strVal = sval.value

	if (strVal.length < 1) {
		return false;
	}

	strVal = numOffMask(strVal);
	var result = isNumberDot(strVal);
	if (!result) {
		//alert("숫자만 입력 가능합니다.");
		sval.focus();
		return false;
	}
}



/*=============================================================================*
 * 특수문자 값이 입력되었는지 체크(특수문자가 있으면 false 리턴)
 * param : sval (object)
 * return :
 *============================================================================*/
function chkValidChar(sval) {
	var re = new RegExp("[%\']","ig");
	var retVal = re.test(sval.value);

	if (retVal == true) {
		alert("You can't enter this character : \n\n              %    \' ");
		return false;
	}
	else
		return true;
}


/*=============================================================================*
 *
 * 윈도우 오픈1 (사용자 지정 위치생성)
 * param : wUrl        지정url
 * param : wTitle    지정타이틀
 * param : wTop        지정 창 높이정렬기준
 * param : wLeft    지정 창 왼쪽정렬기준
 * param : wWidth    창넓이
 * param : wHeight    창높이
 * param : wSco        스크롤바 생성유무, 1:생성 0:비생성
 *============================================================================*/
function win_open(wUrl,wTitle,wTop,wLeft,wWidth,wHeight,wSco) {
	window.open(wUrl,wTitle,"top="+wTop+",left="+wLeft+",width="+wWidth+",height="+wHeight+",scrollbars="+wSco);
}

/*=============================================================================*
 *
 * 윈도우 오픈2 (무조건 가운데 생성)
 * param : wUrl        지정url
 * param : wTitle    지정타이틀
 * param : wWidth    창넓이
 * param : wHeight    창높이
 * param : wSco        스크롤바 생성유무, 1:생성 0:비생성
 *============================================================================*/
function win_open2(wUrl,wTitle,wWidth,wHeight,wSco) {
	var top_po  = (screen.availHeight/2) - (wHeight/2);
	var left_po = (screen.availWidth/2) - (wWidth/2);
	//alert(top_po +" "+left_po+ " "+wHeight+" "+wWidth);
	window.open(wUrl,wTitle,"top="+top_po+",left="+left_po+",width="+wWidth+",height="+wHeight+",scrollbars="+wSco);
}
function checkMemberExists(){
	var responseText;
	var member_id = document.getElementById("member_id").value;
	$.ajax({
		url: "/?c=common&m=dup_chk_member_id&member_id="+member_id, 
		async: false,
		type: "get",
		success: function(data,status){
    		responseText= data;
    	}
  	});
  	if(responseText=="SUCCESS"||member_id==''){
  		return true;
  	}else{
  		alert("Member ID is already exist!");
  		return false;
  	}
	
}

/*=============================================================================*
 *
 * 윈도우 오픈3 (무조건 전체 생성)
 * param : wUrl        지정url
 * param : wTitle    지정타이틀
 * param : wWidth    창넓이
 * param : wHeight    창높이
 * param : wSco        스크롤바 생성유무, 1:생성 0:비생성
 *============================================================================*/
function win_open3(wUrl,wTitle) {
	var wWidth = screen.availWidth;
	var wHeight = screen.availHeight;

	window.open(wUrl,wTitle,"top=0,left=0,width="+wWidth+",height="+wHeight+",scrollbars=0");

}

function cust_key_num(sval,obj) {
	if (event.keyCode<48||event.keyCode>57){//숫자외금지
		event.returnValue=false;
	}
	if (sval.length == 5) {
		obj.focus();
		obj.select();
	}
}

function acnt_set_num(sval,obj) {//계좌번호 2자리 세팅
	var acnt_val = sval.value;
	var acnt_length = acnt_val.length;
	if (obj.value == "") return;
	if (acnt_val == "") acnt_length = 0;

	if (acnt_length < 2) {
		for (var i = 0; i < (2 - acnt_length); i++) {
			acnt_val = "0" + acnt_val;
		}
		sval.value = acnt_val;
	}
}

function acnt_key_num() {
	if (event.keyCode<48||event.keyCode>57){//숫자외금지
		event.returnValue=false;
	}
}

function rdo_val(sval) {
	// 이펑션은 체크박스와 옵션버튼의 활성화 비활성화에 사용
	if (sval.checked == true) {
		sval.checked = false;
	}else {
		sval.checked = true;
	}
}


function disp_err(sval) {
	ErrMsgForm.errMsg.value = sval;
	window.open("/err_msg.screen","error","top=300px,left=310px,height=300px,width=417px,resizable=0,scrollbars=1");
}

function disp_err1(sval) {
	ErrMsgForm.errMsg.value = sval;
	window.open("/err_msg.screen","error","top=300px,left=310px,height=300px,width=500px,resizable=0,scrollable=1");
}


/**
 * 두 날짜에 며칠 차이나는지 구함
 * from_val이 to_val보다 크면 -붙여서 리턴
 */
function getDayInterval(from_val,to_val) {
	var day   = 1000 * 3600 * 24; //24시간
	if(isDate(from_val)==false) {
		return;
	}
	if(isDate(to_val)==false)
		return;

	var from_date=toTimeObject(from_val);
	var to_date=toTimeObject(to_val);
	var day_interval=parseInt((to_date - from_date) / day, 10);
	//alert(to_date+" - "+from_date+"="+day_interval);
	return day_interval;
}

/**
 * Time 스트링을 자바스크립트 Date 객체로 변환
 * parameter time: Time 형식의 String
 */
function toTimeObject(time) { //parseTime(time)
	var year  = time.substr(0,4);
	var month = time.substr(4,2) - 1; // 1월=0,12월=11
	var day   = time.substr(6,2);

	return new Date(year,month,day);
}


// readonly나 disabled로 막힌 경우를 제외하고 tabindex 값을 지정하여 입력박스 순서대로 이동
function tabIndexing() {
	elements = document.all;
	for (i=0;i<elements.length ;i++) {
		if(elements[i].readOnly == false){
			elements[i].tabIndex = i;
		}
	}
}

//validtion을 체크하여 메시지를 리턴 한다.
function validate(obj, message, obj2) {

	var objType = "";

	objType = obj.type;

	if(!objType) {
		//체크박스나 라디오 버튼일 경우
		if(obj.length > 0)
			objType = obj[0].type;
		else
			return false;
	}

	if(obj2 == "undefined") obj2 = null;

	switch(objType) {
		case "select-one":
			if(obj.selectedIndex==0) {
				if(message!="")
					alert(message);

				if(obj2 != null)
					obj2.focus();
				else
					obj.focus();
				return false;
			}
			break;
		case "text":
			// 다중 input 박스인지 확인
			if(typeof(obj.value) == 'undefined') {
				if(typeof(obj.length) == 'number') {
					for(var i = 0; i < obj.length; i++) {
						if(Trim(obj[i].value)=="") {
							if(message!="")
								alert(message);
							if(obj2 != null)
								obj2.focus();
							else
								obj[i].focus();
							return false;
						}
					}
				}
			} else {
				if(Trim(obj.value)=="") {
					if(message!="")
						alert(message);
					if(obj2 != null)
						obj2.focus();
					else
						obj.focus();
					return false;
				}
			}
			break;
		case "password":
			if(Trim(obj.value)=="") {
				if(message!="")
					alert(message);
				if(obj2 != null)
					obj2.focus();
				else
					obj.focus();
				return false;
			}
			break;
		case "hidden":
			if(Trim(obj.value)=="") {
				if(message!="")
					alert(message);
				if(obj2 != null)
					obj2.focus();
				return false;
			}
			break;
		case "textarea":
			if(Trim(obj.value)=="") {
				if(message!="")
					alert(message);
				if(obj2 != null)
					obj2.focus();
				else
					obj.focus();
				return false;
			}
			break;
		case "checkbox":
			if(obj.length > 0) {
				for(var i=0; i<obj.length; i++) {
					if(obj[i].checked ) {
						return true;
					}
				}
				if(message!="")
					alert(message);
				if(obj2 != null)
					obj2.focus();
				return false;
			}
			else
			{
				if(!obj.checked) {
					if(message!="")
						alert(message);
					if(obj2 != null)
						obj2.focus();
					return false;
				}
			}
			break;
		case "radio":
			if(obj.length > 0) {
				for(var i=0; i<obj.length; i++) {
					if(obj[i].checked ) {
						return true;
					}
				}
				if(message!="")
					alert(message);
				if(obj2 != null)
					obj2.focus();
				return false;
			}
			else
			{
				if(!obj.checked) {
					if(message!="")
						alert(message);
					if(obj2 != null)
						obj2.focus();
					return false;
				}
			}

			break;

	}

	return true;

}


/***************************************************
우편번호 창을 띄운다.
 ****************************************************/
function openZipcode(fName, zName, aName) {
	var sUrl = "/?c=fw_zipcode&fName=" + fName + "&zName=" + zName + "&aName=" + aName;
	win_open2(sUrl, "zipcode", 420, 370, 'NO');
}

/***************************************************^M
우편번호 창을 띄운다2.^M
 ****************************************************/
function openZipcode2(fName, zName1, zName2, aName) {
	var sUrl = "/?c=fw_zipcode&fName=" + fName + "&zName1=" + zName1 + "&zName2=" + zName2 + "&aName=" + aName;
	win_open2(sUrl, "zipcode", 460, 470, 'NO');
}
/***************************************************
아이디 중복체크 창을 띄운다.
 ****************************************************/
function openDupIdCheck(frmName, inputName) {
	var sUrl = "/?c=common&m=dupIdCheckIndex&frmName=" + frmName + "&inputName=" + inputName;
	win_open2(sUrl, "dupIdCheck", 420, 370, 'NO');
}

/***************************************************
공통스타일 복사
 ****************************************************/
function copyCommonStyle(obj) {
	if(!validate(obj.elements["values[commonStyle]"], "공통 스타일을 선택 하십시오")) return;
	obj.action = "/?c=common&m=setCommonStyle";
	obj.submit();
}
/***************************************************
사업자 등록번호 휴효성 체크
 ****************************************************/
function bizNoCheck(strCk) {
	var strCk1 = strCk.substring(0,3);
	var strCk2 = strCk.substring(3,5);
	var strCk3 = strCk.substring(5,10);

	arrCkValue = new Array(10);

	if ( (strCk1=="111") && (strCk2=="11") && (strCk3=="11111") ) return true;

	arrCkValue[0] = ( parseFloat(strCk1.substring(0 ,1))  * 1 ) % 10;
	arrCkValue[1] = ( parseFloat(strCk1.substring(1 ,2))  * 3 ) % 10;
	arrCkValue[2] = ( parseFloat(strCk1.substring(2 ,3))  * 7 ) % 10;
	arrCkValue[3] = ( parseFloat(strCk2.substring(0 ,1))  * 1 ) % 10;
	arrCkValue[4] = ( parseFloat(strCk2.substring(1 ,2))  * 3 ) % 10;
	arrCkValue[5] = ( parseFloat(strCk3.substring(0 ,1))  * 7 ) % 10;
	arrCkValue[6] = ( parseFloat(strCk3.substring(1 ,2))  * 1 ) % 10;
	arrCkValue[7] = ( parseFloat(strCk3.substring(2 ,3))  * 3 ) % 10;

	intCkTemp     = parseFloat(strCk3.substring(3 ,4))  * 5  + "0";
	arrCkValue[8] = parseFloat(intCkTemp.substring(0,1)) + parseFloat(intCkTemp.substring(1,2));
	arrCkValue[9] = parseFloat(strCk3.substring(4,5));
	intCkLastid = ( 10 - ( ( arrCkValue[0]+arrCkValue[1]+arrCkValue[2]+arrCkValue[3]+arrCkValue[4]+arrCkValue[5]+arrCkValue[6]+arrCkValue[7]+arrCkValue[8] ) % 10 ) ) % 10;

	if (arrCkValue[9] != intCkLastid) return false;
	else return true;
}


/**********************************************
- 주민등록번호 유효화 체크
- 1:외국인 여부 (false:외국인)
- 2:주민번호 앞자리
- 3:주민번호 뒷자리
 **********************************************/
function regNoCheck(flag, jumin_1, jumin_2) {
	var chk_foreiner;

	if(flag) { //내국인

		chk_foreiner = "inner";

	} else {

		for(i=0; i < 2; i++){
			if(f.chk_for[i].checked){
				chk_foreiner = f.chk_for[i].value;
			}
		}

	}

	if(chk_foreiner=="inner"){

		var checkOK = "0123456789";
		var checkStr = jumin_1.value;
		var allValid = true;
		var decPoints = 0;
		var allNum = "";

		for (i = 0;  i < checkStr.length;  i++) {
			ch = checkStr.charAt(i);
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
					break;
			if (j == checkOK.length) {
				allValid = false;
				break;
			}
			if (ch != ",")
				allNum += ch;
		}

		if (!allValid) {
			alert("주민등록번호는 숫자로 입력하셔야 합니다.");
			jumin_1.focus();
			return (false);
		}

		if (jumin_1.value.length < 6) {
			alert("주민등록번호 앞자리는 6자리 입니다.");
			jumin_1.focus();
			return (false);
		}

		var checkOK = "0123456789";
		var checkStr = jumin_2.value;
		var allValid = true;
		var decPoints = 0;
		var allNum = "";

		for (i = 0;  i < checkStr.length;  i++) {
			ch = checkStr.charAt(i);
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
					break;
			if (j == checkOK.length) {
				allValid = false;
				break;
			}
			if (ch != ",")
				allNum += ch;
		}

		if (!allValid) {
			alert("주민등록번호는 숫자로 입력하셔야 합니다.");
			jumin_2.focus();
			return (false);
		}

		if (jumin_2.value.length < 7) {
			alert("주민등록번호 뒷자리는 7자리 입니다.");
			jumin_2.focus();
			return (false);
		}

		//2005.11.18//kwh
		if (jumin_2.value=="1111111") {
			alert("주민등록번호가 정확하지 않습니다.");
			jumin_2.focus();
			return (false);
		}
		//2005.11.18//kwh

		if (!checkRegNum( jumin_1.value + jumin_2.value)) {
			alert("주민등록번호가 정확하지 않습니다.");
			jumin_1.focus();
			return (false);
		}

	} else {

		var fgn_reg_no = jumin_1.value + jumin_2.value;

		if (fgn_reg_no == ''){
			alert('외국인등록번호를 입력하십시오.');
			jumin_1.focus();
			return false;
		}

		if (fgn_reg_no.length != 13) {
			alert('외국인등록번호 자리수가 맞지 않습니다.');
			return false;
		}

		if ((fgn_reg_no.charAt(6) == "5") || (fgn_reg_no.charAt(6) == "6")){
			birthYear = "19";
		}else if ((fgn_reg_no.charAt(6) == "7") || (fgn_reg_no.charAt(6) == "8")){
			birthYear = "20";
		}else if ((fgn_reg_no.charAt(6) == "9") || (fgn_reg_no.charAt(6) == "0")){
			birthYear = "18";
		}else{
			alert("등록번호에 오류가 있습니다. 다시 확인하십시오.");
			return false;
		}

		birthYear += fgn_reg_no.substr(0, 2);
		birthMonth = fgn_reg_no.substr(2, 2) - 1;
		birthDate = fgn_reg_no.substr(4, 2);
		birth = new Date(birthYear, birthMonth, birthDate);

		if ( birth.getYear() % 100 != fgn_reg_no.substr(0, 2) || birth.getMonth() != birthMonth || birth.getDate() != birthDate ) {
			alert('생년월일에 오류가 있습니다. 다시 확인하십시오.');
			return false;
		}

		if (fgn_no_chksum(fgn_reg_no) == false){
			alert('외국인등록번호에 오류가 있습니다. 다시 확인하십시오.');
			return false;
		}

	}
	return true;
}

/**********************************************
- 숫자여부 체크
 **********************************************/
function checkRegNum(str) {
	var num = new Array(14);
	var digit = new Array(13);

	digit[1] = 2;
	digit[2] = 3;
	digit[3] = 4;
	digit[4] = 5;
	digit[5] = 6;
	digit[6] = 7;
	digit[7] = 8;
	digit[8] = 9;
	digit[9] = 2;
	digit[10] = 3;
	digit[11] = 4;
	digit[12] = 5;

	//사람이 입력한 주민등록 번호를 배열에 넣는다
	for(var j=1 ; j<=13 ; j++) {
		num[j] = parseInt(str.charAt(j-1),10)
	}
	sum = 0;

	//check_digit와 번호를 연산한다
	for(i=1; i<=12; i++) {
		sum += digit[i] * num[i];
	}

	div = (sum%11);

	if(div == 1) {
		comp = 0;
	} else if(div == 0) {
		comp = 1;
	} else if((div != 0)&&(div != 1)) {
		comp = 11 - div;
	}

	if(div == 0) {

		if(num[13] == 1)
			return true;
		else
			return false;

	} else if(div == 1) {

		if(num[13] == 0)
			return true;
		else
			return false;

	} else if((11-div) == num[13]) {

		return true;

	} else {

		return false;

	}
}
/**********************************************
- e-mail체크
 **********************************************/
function emailCheck(obj) {
	var email = obj.value;
	var len = obj.value.length;
	if(len > 0) {
		if (email.indexOf('@')==-1 || email.indexOf('.')==-1 || email.indexOf('@.')!=-1 || email.indexOf('.@')!=-1 || email.charAt(len-1)=="." || email.charAt(len-1)=="@"  || email.charAt(0)==" " || email.charAt(len-1)==" ") {
			alert('Please Input correct E-Mail.');
			obj.focus();
			return false;
		}
		for(var i=0; i<len; i++) {
			if (email.charAt(i) == " ") {
				alert('Please Input correct E-Mail.');
				obj.focus();
				return false;
			}
		}
	}
	return true;
}



/**********************************************
- 달력
 **********************************************/

var target;                                                                    // 호출한 Object의 저장
var stime;
document.write("<div id=minical oncontextmenu='return false' ondragstart='return false' onselectstart='return false' style=\"background:buttonface; margin:2; padding:2;margin-top:2;border-top:1 solid buttonshadow;border-left: 1 solid buttonshadow;border-right: 1 solid buttonshadow;border-bottom:1 solid buttonshadow;width:200;display:none;position: absolute; z-index: 99\"></div>");

function Calendar(obj) {
	var now = obj.value.split("-");
	var x, y;

	target = obj;                                                                // Object 저장;

	x = (document.layers) ? loc.pageX : obj.offsetLeft;
	y = (document.layers) ? loc.pageY : obj.offsetTop;
	while((obj = obj.offsetParent) != null){
		x += obj.offsetLeft;
		y += obj.offsetTop;
	}
	document.getElementById('minical').style.top = y+20+'px';
	document.getElementById('minical').style.left = x-50+'px';
	document.getElementById('minical').style.display = (document.getElementById('minical').style.display == "block") ? "none" : "block";

	if (now.length == 3) {                                                        // 정확한지 검사
		Show_cal(now[0],now[1],now[2]);                                            // 넘어온 값을 년월일로 분리
	}
	else {
		now = new Date();
		Show_cal(now.getFullYear(), now.getMonth()+1, now.getDate());            // 현재 년/월/일을 설정하여 넘김.
	}
}

function doOver(mEvent) {                                                                // 마우스가 칼렌다위에 있으면
	var an = navigator.appName;
	if (navigator.appName == "Netscape") {
		var el = mEvent.target;
	}
	else {
		var el = mEvent.srcElement;
	}
	cal_Day = el.title;

	if (cal_Day.length > 7) {                                                    // 날자 값이 있으면.
		el.style.borderTopColor = el.style.borderLeftColor = "buttonhighlight";
		el.style.borderRightColor = el.style.borderBottomColor = "buttonshadow";
	}
	window.clearTimeout(stime);                                                    // Clear
}

function doClick(mEvent) {                                                            // 날자를 선택하였을 경우
	var an = navigator.appName;
	if (navigator.appName == "Netscape") {
		var el = mEvent.target;
	}
	else {
		var el = mEvent.srcElement;
	}
	cal_Day = el.title;
	el.style.borderColor = "red";                            // 테두리 색을 빨간색으로
	if (cal_Day.length > 7) {                                                    // 날자 값이있으면
		target.value=cal_Day                                                    // 값 설정
	}
	document.getElementById('minical').style.display='none';                                                // 화면에서 지움}
}

function doOut(mEvent) {
	var an = navigator.appName;
	if (navigator.appName == "Netscape") {
		var el = mEvent.target;
	}
	else {
		var el = mEvent.srcElement;
	}
	cal_Day = el.title;

	if (cal_Day.length > 7) {
		el.style.borderColor = "white";
	}
	//stime=window.setTimeout("document.getElementById('minical').style.display='none';", 200);
}

function day2(d) {                                                                // 2자리 숫자료 변경
	var str = new String();

	if (parseInt(d) < 10) {
		str = "0" + parseInt(d);
	} else {
		str = "" + parseInt(d);
	}
	return str;
}

function Show_cal(sYear, sMonth, sDay) {
	var Months_day = new Array(0,31,28,31,30,31,30,31,31,30,31,30,31)
	var Weekday_name = new Array("日", "月", "火", "水", "木", "金", "土");
	var intThisYear = new Number(), intThisMonth = new Number(), intThisDay = new Number();
	document.getElementById('minical').innerHTML = "";
	datToday = new Date();                                                    // 현재 날자 설정

	intThisYear = parseInt(sYear);
	intThisMonth = parseInt(sMonth);
	intThisDay = parseInt(sDay);

	if (intThisYear == 0) intThisYear = datToday.getFullYear();                // 값이 없을 경우
	if (intThisMonth == 0) intThisMonth = parseInt(datToday.getMonth())+1;    // 월 값은 실제값 보다 -1 한 값이 돼돌려 진다.
	if (intThisDay == 0) intThisDay = datToday.getDate();

	switch(intThisMonth) {
		case 1:
			intPrevYear = intThisYear -1;
			intPrevMonth = 12;
			intNextYear = intThisYear;
			intNextMonth = 2;
			break;
		case 12:
			intPrevYear = intThisYear;
			intPrevMonth = 11;
			intNextYear = intThisYear + 1;
			intNextMonth = 1;
			break;
		default:
			intPrevYear = intThisYear;
			intPrevMonth = parseInt(intThisMonth) - 1;
			intNextYear = intThisYear;
			intNextMonth = parseInt(intThisMonth) + 1;
			break;
	}

	NowThisYear = datToday.getFullYear();                                        // 현재 년
	NowThisMonth = datToday.getMonth()+1;                                        // 현재 월
	NowThisDay = datToday.getDate();                                            // 현재 일

	datFirstDay = new Date(intThisYear, intThisMonth-1, 1);                        // 현재 달의 1일로 날자 객체 생성(월은 0부터 11까지의 정수(1월부터 12월))
	intFirstWeekday = datFirstDay.getDay();                                        // 현재 달 1일의 요일을 구함 (0:일요일, 1:월요일)

	intSecondWeekday = intFirstWeekday;
	intThirdWeekday = intFirstWeekday;

	datThisDay = new Date(intThisYear, intThisMonth, intThisDay);                // 넘어온 값의 날자 생성
	intThisWeekday = datThisDay.getDay();                                        // 넘어온 날자의 주 요일

	varThisWeekday = Weekday_name[intThisWeekday];                                // 현재 요일 저장

	intPrintDay = 1                                                                // 달의 시작 일자
	secondPrintDay = 1
	thirdPrintDay = 1

	Stop_Flag = 0

	if ((intThisYear % 4)==0) {                                                    // 4년마다 1번이면 (사로나누어 떨어지면)
		if ((intThisYear % 100) == 0) {
			if ((intThisYear % 400) == 0) {
				Months_day[2] = 29;
			}
		} else {
			Months_day[2] = 29;
		}
	}
	intLastDay = Months_day[intThisMonth];                                        // 마지막 일자 구함
	Stop_flag = 0

	//Cal_HTML = "<TABLE tyle='background:buttonface; margin:5; padding:5;margin-top:2;border-top:1 solid buttonshadow;border-left: 1 solid buttonshadow;border-right: 1 solid buttonshadow;border-bottom:1 solid buttonshadow;width:160;'><TR><TD><TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0 ONMOUSEOVER=doOver(event); ONMOUSEOUT=doOut(event); STYLE='font-size:8pt;font-family:Tahoma;'>"
	//        + "<TR ALIGN=CENTER><TD COLSPAN=7 nowrap=nowrap ALIGN=CENTER><SPAN TITLE='先月' STYLE=cursor:pointer; onClick='Show_cal("+intPrevYear+","+intPrevMonth+",1);'><FONT COLOR=Navy>◀</FONT></SPAN> "
	//        + "<B STYLE=color:red>"+get_Yearinfo(intThisYear,intThisMonth,intThisDay)+"年"+get_Monthinfo(intThisYear,intThisMonth,intThisDay)+"月</B>"
	//        + " <SPAN TITLE='來月' STYLE=cursor:pointer; onClick='Show_cal("+intNextYear+","+intNextMonth+",1);'><FONT COLOR=Navy>▶</FONT></SPAN></TD></TR><TR><TD HEIGHT=2></TD></TR>"
	//        + "<TR ALIGN=CENTER BGCOLOR='ThreedFace' STYLE='color:White;font-weight:bold;color:#000000;'><TD>日</TD><TD>月</TD><TD>火</TD><TD>水</TD><TD>木</TD><TD>金</TD><TD>土</TD></TR>";

	Cal_HTML = "<TABLE tyle='background:buttonface; margin:5; padding:5;margin-top:2;border-top:1 solid buttonshadow;border-left: 1 solid buttonshadow;border-right: 1 solid buttonshadow;border-bottom:1 solid buttonshadow;width:160;'><TR><TD><TABLE WIDTH=170 BORDER=0 CELLPADDING=0 CELLSPACING=0 ONMOUSEOVER=doOver(event); ONMOUSEOUT=doOut(event); STYLE='font-size:11px;font-family:gulim;'>"
		+ "<TR ALIGN=CENTER><TD COLSPAN=7 nowrap=nowrap ALIGN=CENTER><SPAN title='이전월' STYLE=cursor:pointer; onClick='Show_cal("+intPrevYear+","+intPrevMonth+",1);'><FONT COLOR=Navy>◀</FONT></SPAN> "
		+ "<B STYLE=color:red>"+get_Yearinfo(intThisYear,intThisMonth,intThisDay)+"년"+get_Monthinfo(intThisYear,intThisMonth,intThisDay)+"월</B>"
		+ " <SPAN title='다음월' STYLE=cursor:pointer; onClick='Show_cal("+intNextYear+","+intNextMonth+",1);'><FONT COLOR=Navy>▶</FONT></SPAN></TD></TR><TR><TD HEIGHT=2></TD></TR>"
		+ "<TR ALIGN=CENTER BGCOLOR='ThreedFace' STYLE='color:White;font-weight:bold;color:#000000;'><TD>일</TD><TD>월</TD><TD>화</TD><TD>수</TD><TD>목</TD><TD>금</TD><TD>토</TD></TR>";

	for (intLoopWeek=1; intLoopWeek < 7; intLoopWeek++) {                        // 주단위 루프 시작, 최대 6주
		Cal_HTML += "<TR ALIGN=center BGCOLOR=WHITE>"
		for (intLoopDay=1; intLoopDay <= 7; intLoopDay++) {                        // 요일단위 루프 시작, 일요일 부터
			if (intThirdWeekday > 0) {                                            // 첫주 시작일이 1보다 크면
				Cal_HTML += "<TD onClick=doClick(event);>";
				intThirdWeekday--;
			} else {
				if (thirdPrintDay > intLastDay) {                                // 입력 날짝 월말보다 크다면
					Cal_HTML += "<TD onClick=doClick(event);>";
				} else {                                                        // 입력날짜가 현재월에 해당 되면
					Cal_HTML += "<TD onClick=doClick(event); title="+intThisYear+"-"+day2(intThisMonth).toString()+"-"+day2(thirdPrintDay).toString()+" STYLE=\"cursor:pointer;border:1px solid white;";
					if (intThisYear == NowThisYear && intThisMonth==NowThisMonth && thirdPrintDay==intThisDay) {
						Cal_HTML += "background-color:cyan;";
					}

					switch(intLoopDay) {
						case 1:                                                    // 일요일이면 빨간 색으로
							Cal_HTML += "color:red;"
							break;
						case 7:
							Cal_HTML += "color:blue;"
							break;
						default:
							Cal_HTML += "color:black;"
							break;
					}

					Cal_HTML += "\">"+thirdPrintDay;

				}
				thirdPrintDay++;

				if (thirdPrintDay > intLastDay) {                                // 만약 날짜 값이 월말 값보다 크면 루프문 탈출
					Stop_Flag = 1;
				}
			}
			Cal_HTML += " </TD>";
		}
		Cal_HTML += "</TR>";
		if (Stop_Flag==1) break;
	}
	Cal_HTML += "</TABLE></TD><TR></TABLE>";

	document.getElementById('minical').innerHTML = Cal_HTML;
}

function get_Yearinfo(year,month,day) {                                            // 년 정보를 콤보 박스로 표시
	var min = parseInt(year) - 100;
	var max = parseInt(year) + 10;
	var i = new Number();
	var str = new String();

	str = "<select class='input_2' style='width:60px' onChange='Show_cal(this.value,"+month+","+day+");' ONMOUSEOVER=doOver(event);>";
	for (i=min; i<=max; i++) {
		if (i == parseInt(year)) {
			str += "<OPTION VALUE="+i+" selected ONMOUSEOVER=doOver(event);>"+i+"</OPTION>";
		} else {
			str += "<OPTION VALUE="+i+" ONMOUSEOVER=doOver(event);>"+i+"</OPTION>";
		}
	}
	str += "</SELECT>";
	return str;
}


function get_Monthinfo(year,month,day) {                                        // 월 정보를 콤보 박스로 표시
	var i = new Number();
	var str = new String();

	str = "<SELECT class='input_2' style='width:40px' onChange='Show_cal("+year+",this.value,"+day+");' ONMOUSEOVER=doOver(event);>";
	for (i=1; i<=12; i++) {
		if (i == parseInt(month)) {
			str += "<OPTION VALUE="+i+" selected ONMOUSEOVER=doOver(event);>"+i+"</OPTION>";
		} else {
			str += "<OPTION VALUE="+i+" ONMOUSEOVER=doOver(event);>"+i+"</OPTION>";
		}
	}
	str += "</SELECT>";
	return str;
}




/********************************************************
금칙단어 필터링
 ********************************************************/
function wordFilter(filterWord, val) {
	if(filterWord=="") {
		return true;
	}
	else
	{
		var word = filterWord.split(",");
		for(var i=0; i<word.length; i++) {
			if(val.indexOf(word[i]) > -1) {
				if(Trim(word[i]) != "") {
					alert("[" + word[i] + "] 와 같은 단어는 등록 하실 수 없습니다.");
					return false;
					break;
				}
			}
		}
		return true;
	}
}

/********************************************************
등록금지 아이디 필터링
 ********************************************************/
function idFilter(filterWord, val) {
	if(filterWord=="") {
		return true;
	}
	else
	{
		var word = filterWord.split(",");
		for(var i=0; i<word.length; i++) {
			if(word[i]!="") {
				if(val.indexOf(Trim(word[i]))!=-1) {
					alert("[" + word[i] + "] 와 같은 단어는 등록 하실 수 없습니다.");
					return false;
					break;
				}
			}
		}
		return true;
	}
}

/********************************************************
등록금지 특수문자 필터링
 ********************************************************/
function idCharFilter(filterWord, val) {
	filterWord = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,0,1,2,3,4,5,6,7,8,9,_,-";
	var startword = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";

	var word = filterWord.split(",");
	for(var i=0; i<val.length; i++) {
		var isFalse = false;
		for(var j=0; j<word.length; j++) {
			if(val.charAt(i)==word[j]) {
				isFalse = true;
				break;
			}
		}

		if(isFalse==false) {
			alert("아이디는 영문 대소문자와 '_,-' 만 사용할수 있습니다.");
			return false;
			break;
		}
	}

	word = startword.split(",");
	var isFalse = false;
	for(var i=0; i<word.length; i++) {

		if(val.charAt(0)==word[i]) {
			isFalse = true;
			break;
		}
	}

	if(isFalse==false) {
		alert("아이디는 영문자로 시작해야 합니다.");
		return false;
	}

	return true;

}
/********************************************************
등록금지 특수문자 필터링(별명등록시)
 ********************************************************/
function nickNameCharFilter(filterWord, val) {
	filterWord = "&,$,%,#,!,(,),{,},[,],|,\\,',\",?,/,.,>,<,\,,~,+,=,*,@,";

	var word = filterWord.split(",");
	for(var i=0; i<val.length; i++) {
		var isFalse = true;
		for(var j=0; j<word.length; j++) {
			if(val.charAt(i)==word[j]) {
				isFalse = false;
				break;
			}
		}

		if(isFalse==false) {
			alert("특수문자는 사용할 수 없습니다");
			return false;
			break;
		}
	}

	return true;

}
/********************************************************
테이블에 라운드를 주기위한 스크립트(보더 1인 라운드테이블)
 ********************************************************/
function makeTable1Round(objID) {
	var obj = document.getElementById(objID);
	var Parent, objTmp, Table, TBody, TR, TD;
	var bdcolor, bgcolor, Space;
	var trIDX, tdIDX, MAX;
	var styleWidth, styleHeight;

	// get parent node
	Parent = obj.parentNode;
	objTmp = document.createElement('SPAN');
	Parent.insertBefore(objTmp, obj);
	Parent.removeChild(obj);

	// get attribute
	bdcolor = obj.getAttribute('rborder');
	bgcolor = obj.getAttribute('rbgcolor');
	radius = 3;

	//if (radius == null || radius < 1) radius = 1;
	//else if (radius > 6) radius = 6;

	MAX = radius * 2 + 1;

	/*
create table {{
	 */
	pTable = document.createElement('TABLE');
	pTBody = document.createElement('TBODY');
	pTr    = document.createElement('TR');
	pTd    = document.createElement('TD');
	pTd.style.width="100%"

	pTable.cellSpacing = 0;
	pTable.cellPadding = 0;
	pTable.border="0";
	pTable.style.width="100%"


	Table = document.createElement('TABLE');
	TBody = document.createElement('TBODY');

	Table.cellSpacing = 0;
	Table.cellPadding = 0;
	Table.style.width="100%"

	for (trIDX=0; trIDX < MAX; trIDX++) {
		//alert("ok");
		TR = document.createElement('TR');
		Space = Math.abs(trIDX - parseInt(radius));
		for (tdIDX=0; tdIDX < MAX; tdIDX++) {
			TD = document.createElement('TD');

			styleWidth = '1px'; styleHeight = '1px';
			if (tdIDX == 0 || tdIDX == MAX - 1) styleHeight = null;
			else if (trIDX == 0 || trIDX == MAX - 1) styleWidth = null;
			else if (radius > 2) {
				if (Math.abs(tdIDX - radius) == 1) styleWidth = '2px';
				if (Math.abs(trIDX - radius) == 1) styleHeight = '2px';
			}

			if (styleWidth != null) TD.style.width = styleWidth;
			if (styleHeight != null) TD.style.height = styleHeight;

			//if(tdIDX==0) TD.appendChild(img);
			//if(tdIDX+1 == MAX ) TD.appendChild(img);
			if(trIDX==radius && tdIDX!= radius) {
				var img = document.createElement('IMG');
				img.src = "http://bms.wellpage.co.kr/images/bbsBtnSkin/DEFAULT/btn_write.gif";
				img.style.width = styleWidth;
				img.style.height = "0px";
				TD.appendChild(img);
			}

			if (Space == tdIDX || Space == MAX - tdIDX - 1) {TD.style.backgroundColor = bdcolor;}
			else if (tdIDX > Space && Space < MAX - tdIDX - 1) {TD.style.backgroundColor = bgcolor;}


			if (Space == 0 && tdIDX == radius) {TD.style.width="100%";TD.appendChild(obj); }
			TR.appendChild(TD);
		}
		TBody.appendChild(TR);
	}

	/*
}}
	 */
	Table.appendChild(TBody);

	pTd.appendChild(Table);
	pTr.appendChild(pTd);
	pTBody.appendChild(pTr);
	pTable.appendChild(pTBody);

	// insert table and remove original table
	Parent.insertBefore(pTable, objTmp);
}

/********************************************************
테이블에 라운드를 주기위한 스크립트(보더 2인 라운드테이블)
 ********************************************************/
function makeTable2Round(objID) {
	var obj = document.getElementById(objID);
	var Parent, objTmp, Table, TBody;
	var bdcolor, bgcolor, Space;
	var styleWidth, styleHeight;

	// get parent node
	Parent = obj.parentNode;
	objTmp = document.createElement('SPAN');
	Parent.insertBefore(objTmp, obj);
	Parent.removeChild(obj);

	// get attribute
	bdcolor = obj.getAttribute('rborder');
	bgcolor = obj.getAttribute('rbgcolor');

	Table = document.createElement('TABLE');
	TBody = document.createElement('TBODY');
	Table.cellSpacing = 0;
	Table.cellPadding = 0;
	Table.style.width="100%"

	TBody = makeTable2Top(TBody, bdcolor, bgcolor);
	TBody = makeTable2Middle(TBody, bdcolor, bgcolor, obj);
	TBody = makeTable2Bottom(TBody, bdcolor, bgcolor);

	Table.appendChild(TBody);
	Parent.insertBefore(Table, objTmp);
}

/********************************************************
테이블에 라운드를 주기위한 스크립트(보더 3인 라운드테이블)
 ********************************************************/
function makeTable3Round(objID) {
	var obj = document.getElementById(objID);
	var Parent, objTmp, Table, TBody;
	var bdcolor, bgcolor, Space;
	var styleWidth, styleHeight;

	// get parent node
	Parent = obj.parentNode;
	objTmp = document.createElement('SPAN');
	Parent.insertBefore(objTmp, obj);
	Parent.removeChild(obj);

	// get attribute
	bdcolor = obj.getAttribute('rborder');
	bgcolor = obj.getAttribute('rbgcolor');

	Table = document.createElement('TABLE');
	TBody = document.createElement('TBODY');
	Table.cellSpacing = 0;
	Table.cellPadding = 0;
	Table.style.width="100%"

	TBody = makeTable3Top(TBody, bdcolor, bgcolor);
	TBody = makeTable3Middle(TBody, bdcolor, bgcolor, obj);
	TBody = makeTable3Bottom(TBody, bdcolor, bgcolor);

	Table.appendChild(TBody);
	Parent.insertBefore(Table, objTmp);
}

/********************************************************
테이블에 라운드를 주기위한 스크립트(보더 5인 라운드테이블)
 ********************************************************/
function makeTable5Round(objID) {
	var obj = document.getElementById(objID);
	var Parent, objTmp, Table, TBody;
	var bdcolor, bgcolor, Space;
	var styleWidth, styleHeight;

	// get parent node
	Parent = obj.parentNode;
	objTmp = document.createElement('SPAN');
	Parent.insertBefore(objTmp, obj);
	Parent.removeChild(obj);

	// get attribute
	bdcolor = obj.getAttribute('rborder');
	bgcolor = obj.getAttribute('rbgcolor');

	Table = document.createElement('TABLE');
	TBody = document.createElement('TBODY');
	Table.cellSpacing = 0;
	Table.cellPadding = 0;
	Table.style.width="100%"

	TBody = makeTable5Top(TBody, bdcolor, bgcolor);
	TBody = makeTable5Middle(TBody, bdcolor, bgcolor, obj);
	TBody = makeTable5Bottom(TBody, bdcolor, bgcolor);

	Table.appendChild(TBody);
	Parent.insertBefore(Table, objTmp);
}

/********************************************************
라운드 테이블 그리기
보더 2인 라운드 테이블 상단
 ********************************************************/
function makeTable2Top(TBody, bdcolor, bgcolor) {
	var TR;
	var TD;

	for(i=0; i<5; i++) {
		TR = document.createElement('TR');
		for(j=0; j<11; j++) {
			TD = document.createElement('TD');
			TD.style.width= "1px";
			var img = document.createElement('IMG');
			img.src = "http://bms.wellpage.co.kr/images/bbsBtnSkin/DEFAULT/btn_write.gif";
			img.style.width = "1px";
			img.style.height = "0px";

			if(i==0) {
				if(j==4 || j==5 || j==6)
					TD.style.backgroundColor = bdcolor;
				TD.appendChild(img);
			}
			else if(i==1) {
				if(j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8)
					TD.style.backgroundColor = bdcolor;
				TD.appendChild(img);
			}
			else if(i==2) {
				if(j==1 || j==2 || j==3 || j==4 || j==6 || j==7 || j==8 || j==9)
					TD.style.backgroundColor = bdcolor;
				if(j==5) {
					TD.style.backgroundColor = bgcolor;
					TD.style.width="100%"
				}
			}
			else if(i==3) {
				if(j==1 || j==2 || j==8 || j==9)
					TD.style.backgroundColor = bdcolor;
				if(j==3 || j==4 || j==5 || j==6 || j==7)
					TD.style.backgroundColor = bgcolor;
				TD.appendChild(img);
			}
			else if(i==4) {
				if(j==0 || j==1 || j==2 || j==8 || j==9 || j==10)
					TD.style.backgroundColor = bdcolor;
				if(j==3 || j==4 || j==5 || j==6 || j==7)
					TD.style.backgroundColor = bgcolor;
				TD.appendChild(img);
			}

			TR.appendChild(TD);
		}
		TBody.appendChild(TR);
	}
	return TBody;
}

/********************************************************
라운드 테이블 그리기
보더 2인 라운드 테이블 중단
 ********************************************************/
function makeTable2Middle(TBody, bdcolor, bgcolor, obj) {
	var TR;
	var TD;

	TR = document.createElement('TR');
	for(j=0; j<11; j++) {
		TD = document.createElement('TD');

		if(j==0 || j==1 || j==9 || j==10)
			TD.style.backgroundColor = bdcolor;

		if(j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8)
			TD.style.backgroundColor = bgcolor;
		if(j==5)
			TD.appendChild(obj);

		TR.appendChild(TD);
	}
	TBody.appendChild(TR);
	return TBody;
}

/********************************************************
라운드 테이블 그리기
보더 2인 라운드 테이블 하단
 ********************************************************/
function makeTable2Bottom(TBody, bdcolor, bgcolor) {
	var TR;
	var TD;

	for(i=0; i<5; i++) {
		TR = document.createElement('TR');
		for(j=0; j<11; j++) {
			TD = document.createElement('TD');

			if(i==4) {
				if(j==4 || j==5 || j==6)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==3) {
				if(j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==2) {
				if(j==1 || j==2 || j==3 || j==4 || j==6 || j==7 || j==8 || j==9)
					TD.style.backgroundColor = bdcolor;
				if(j==5) {
					TD.style.backgroundColor = bgcolor;
				}
			}
			else if(i==1) {
				if(j==1 || j==2 || j==8 || j==9)
					TD.style.backgroundColor = bdcolor;
				if(j==3 || j==4 || j==5 || j==6 || j==7)
					TD.style.backgroundColor = bgcolor;
			}
			else if(i==0) {
				if(j==0 || j==1 || j==2 || j==8 || j==9 || j==10)
					TD.style.backgroundColor = bdcolor;
				if(j==3 || j==4 || j==5 || j==6 || j==7)
					TD.style.backgroundColor = bgcolor;
			}

			TR.appendChild(TD);
		}

		TBody.appendChild(TR);
	}
	return TBody;
}

/********************************************************
라운드 테이블 그리기
보더 3인 라운드 테이블 상단
 ********************************************************/
function makeTable3Top(TBody, bdcolor, bgcolor) {
	var TR;
	var TD;

	for(i=0; i<5; i++) {
		TR = document.createElement('TR');
		for(j=0; j<11; j++) {
			TD = document.createElement('TD');
			TD.style.width= "1px";
			var img = document.createElement('IMG');
			img.src = "http://bms.wellpage.co.kr/images/bbsBtnSkin/DEFAULT/btn_write.gif";
			img.style.width = "1px";
			img.style.height = "0px";

			if(i==0) {
				if(j==4 || j==5 || j==6)
					TD.style.backgroundColor = bdcolor;
				TD.appendChild(img);
			}
			else if(i==1) {
				if(j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8)
					TD.style.backgroundColor = bdcolor;
				TD.appendChild(img);
			}
			else if(i==2) {
				if(j==1 || j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8 || j==9)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==3) {
				if(j==1 || j==2 || j==3 || j==4 || j==6 || j==7 || j==8 || j==9)
					TD.style.backgroundColor = bdcolor;
				if(j==5) {
					TD.style.width="100%"
					TD.style.backgroundColor = bgcolor;
				}
				TD.appendChild(img);
			}
			else if(i==4) {
				if(j==0 || j==1 || j==2 || j==3 || j==7 || j==8 || j==9 || j==10)
					TD.style.backgroundColor = bdcolor;
				if(j==4 || j==5 || j==6)
					TD.style.backgroundColor = bgcolor;
				TD.appendChild(img);
			}

			TR.appendChild(TD);
		}
		TBody.appendChild(TR);
	}
	return TBody;
}

/********************************************************
라운드 테이블 그리기
보더 3인 라운드 테이블 중단
 ********************************************************/
function makeTable3Middle(TBody, bdcolor, bgcolor, obj) {
	var TR;
	var TD;

	TR = document.createElement('TR');
	for(j=0; j<11; j++) {
		TD = document.createElement('TD');

		if(j==0 || j==1 || j==2 || j==8 || j==9 || j==10)
			TD.style.backgroundColor = bdcolor;

		if(j==3 || j==4 || j==5 || j==6 || j==7)
			TD.style.backgroundColor = bgcolor;
		if(j==5)
			TD.appendChild(obj);

		TR.appendChild(TD);
	}
	TBody.appendChild(TR);
	return TBody;
}

/********************************************************
라운드 테이블 그리기
보더 3인 라운드 테이블 하단
 ********************************************************/
function makeTable3Bottom(TBody, bdcolor, bgcolor) {
	var TR;
	var TD;

	for(i=0; i<5; i++) {
		TR = document.createElement('TR');
		for(j=0; j<11; j++) {
			TD = document.createElement('TD');

			if(i==4) {
				if(j==4 || j==5 || j==6)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==3) {
				if(j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==2) {
				if(j==1 || j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8 || j==9)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==1) {
				if(j==1 || j==2 || j==3 || j==4 || j==6 || j==7 || j==8 || j==9)
					TD.style.backgroundColor = bdcolor;
				if(j==5) {
					TD.style.width="100%"
					TD.style.backgroundColor = bgcolor;
				}
			}
			else if(i==0) {
				if(j==0 || j==1 || j==2 || j==3 || j==7 || j==8 || j==9 || j==10)
					TD.style.backgroundColor = bdcolor;
				if(j==4 || j==5 || j==6)
					TD.style.backgroundColor = bgcolor;
			}

			TR.appendChild(TD);
		}

		TBody.appendChild(TR);
	}
	return TBody;
}

/********************************************************
라운드 테이블 그리기
보더 5인 라운드 테이블 상단
 ********************************************************/
function makeTable5Top(TBody, bdcolor, bgcolor) {
	var TR;
	var TD;

	for(i=0; i<8; i++) {
		TR = document.createElement('TR');
		for(j=0; j<17; j++) {
			TD = document.createElement('TD');
			TD.style.width= "1px";
			var img = document.createElement('IMG');
			img.src = "http://bms.wellpage.co.kr/images/bbsBtnSkin/DEFAULT/btn_write.gif";
			img.style.width = "1px";
			img.style.height = "0px";

			if(i==0) {
				if(j==4 || j==5 || j==6 || j==7 || j==8 || j==9 || j==10 || j==11 || j==12)
					TD.style.backgroundColor = bdcolor;
				TD.appendChild(img);
			}
			else if(i==1) {
				if(j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8 || j==9 || j==10 || j==11 || j==12 || j==13 || j==14)
					TD.style.backgroundColor = bdcolor;
				TD.appendChild(img);
			}
			else if(i==2 || i==3) {
				if(j==1 || j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8 || j==9 || j==10 || j==11 || j==12 || j==13 || j==14 || j==15)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==4) {
				if(j==0 || j==1 || j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8 || j==9 || j==10 || j==11 || j==12 || j==13 || j==14 || j==15 || j==16)
					TD.style.backgroundColor = bdcolor;
				TD.appendChild(img);
			}
			else if(i==5) {
				if(j==0 || j==1 || j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==9 || j==10 || j==11 || j==12 || j==13 || j==14 || j==15 || j==16)
					TD.style.backgroundColor = bdcolor;
				if(j==8) {
					TD.style.width="100%"
					TD.style.backgroundColor = bgcolor;
				}

				TD.appendChild(img);
			}
			else if(i==6 || i==7) {
				if(j==0 || j==1 || j==2 || j==3 || j==4 || j==5 || j==11 || j==12 || j==13 || j==14 || j==15 || j==16)
					TD.style.backgroundColor = bdcolor;
				if(j==6 || j==7 || j==8 || j==9 || j==10)
					TD.style.backgroundColor = bgcolor;

				TD.appendChild(img);
			}

			TR.appendChild(TD);
		}
		TBody.appendChild(TR);
	}
	return TBody;
}

/********************************************************
라운드 테이블 그리기
보더 5인 라운드 테이블 중단
 ********************************************************/
function makeTable5Middle(TBody, bdcolor, bgcolor, obj) {
	var TR;
	var TD;

	TR = document.createElement('TR');
	for(j=0; j<17; j++) {
		TD = document.createElement('TD');

		if(j==0 || j==1 || j==2 || j==3 || j==4 || j==12 || j==13 || j==14 || j==15 || j==16)
			TD.style.backgroundColor = bdcolor;

		if(j==5 || j==6 || j==7 || j==8 || j==9 || j==10 || j==11)
			TD.style.backgroundColor = bgcolor;
		if(j==8)
			TD.appendChild(obj);

		TR.appendChild(TD);
	}
	TBody.appendChild(TR);
	return TBody;
}

/********************************************************
라운드 테이블 그리기
보더 5인 라운드 테이블 하단
 ********************************************************/
function makeTable5Bottom(TBody, bdcolor, bgcolor) {
	var TR;
	var TD;

	for(i=0; i<8; i++) {
		TR = document.createElement('TR');
		for(j=0; j<17; j++) {
			TD = document.createElement('TD');

			if(i==7) {
				if(j==4 || j==5 || j==6 || j==7 || j==8 || j==9 || j==10 || j==11 || j==12)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==6) {
				if(j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8 || j==9 || j==10 || j==11 || j==12 || j==13 || j==14)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==4 || i==5) {
				if(j==1 || j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8 || j==9 || j==10 || j==11 || j==12 || j==13 || j==14 || j==15)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==3) {
				if(j==0 || j==1 || j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==8 || j==9 || j==10 || j==11 || j==12 || j==13 || j==14 || j==15 || j==16)
					TD.style.backgroundColor = bdcolor;
			}
			else if(i==2) {
				if(j==0 || j==1 || j==2 || j==3 || j==4 || j==5 || j==6 || j==7 || j==9 || j==10 || j==11 || j==12 || j==13 || j==14 || j==15 || j==16)
					TD.style.backgroundColor = bdcolor;
				if(j==8)
					TD.style.backgroundColor = bgcolor;
			}
			else if(i==0 || i==1) {
				if(j==0 || j==1 || j==2 || j==3 || j==4 || j==5 || j==11 || j==12 || j==13 || j==14 || j==15 || j==16)
					TD.style.backgroundColor = bdcolor;
				if(j==6 || j==7 || j==8 || j==9 || j==10)
					TD.style.backgroundColor = bgcolor;
			}


			TR.appendChild(TD);
		}

		TBody.appendChild(TR);
	}
	return TBody;
}

/********************************************************
 * 새로고침 방지
 ********************************************************/
/*
function doNotReload() {
if((event.ctrlKey == true && (event.keyCode == 78 || event.keyCode == 82)) //ctrl+N , ctrl+R
|| (event.keyCode == 116)) // function F5
{
event.keyCode = 0;
event.cancelBubble = true;
event.returnValue = false;
}
}
document.onkeydown = doNotReload;

// 모든 링크와 img 태그에 onfocus=blur() 처리
function bluring() {
if(event.srcElement.tagName == "a"||event.srcElement.tagName == "img")
document.body.focus();
}
document.onfocusin=bluring;
 */


/********************************************************
 * 확대보기 레이어생성-공통사용
 ********************************************************/
function getNowScroll() {
	var de = document.documentElement;
	var b = document.body;
	var now = {};

	now.X = document.all ? (!de.scrollLeft ? b.scrollLeft : de.scrollLeft) : (window.pageXOffset ? window.pageXOffset : window.scrollX);
	now.Y = document.all ? (!de.scrollTop ? b.scrollTop : de.scrollTop) : (window.pageYOffset ? window.pageYOffset : window.scrollY);

	return now;
}

/********************************************************
 * 확대보기 레이어생성-공통사용
 ********************************************************/
var commonImageLayerDiv = null;
function imgLayerOpen(ev, imgSrc, imgSize) {
	var nowScroll = getNowScroll();

	var tmp = imgSize.split('|');
	var image_width  = tmp[0] + "px";
	var image_height = tmp[1] + "px";
	if(image_width=="0px") image_width = "";
	else image_width = "width:" + image_width + ";";
	if(image_height=="0px") image_height = "";
	else image_height = "height:" + image_height + ";";

	var imgUrl = "<img src=\"" + imgSrc + "\" alt=\"확대보기\" style=\"" + image_width + image_height + "\"/>";

	var posX = ev.x;
	var posY = ev.y;

	//레이어 생성
	if(commonImageLayerDiv==null) {
		commonImageLayerDiv = document.createElement("div");
		commonImageLayerDiv.style.position="absolute";
		commonImageLayerDiv.style.left = posX + nowScroll.X ;
		commonImageLayerDiv.style.top  = posY + nowScroll.Y ;
		commonImageLayerDiv.innerHTML  = imgUrl;
		commonImageLayerDiv.style.display = 'block';
		document.body.appendChild(commonImageLayerDiv);
	}
	else
	{
		commonImageLayerDiv.style.left = posX + nowScroll.X ;
		commonImageLayerDiv.style.top  = posY + nowScroll.Y ;
		commonImageLayerDiv.innerHTML  = imgUrl;
		commonImageLayerDiv.style.display = 'block';
	}
}




/*--------------------------- COMMON --------------------------------*/

//텍스트 박스 focus시
function f_text(obj) {
	obj.style.border = "1px solid #60a123";
}

//텍스트 박스 blur시
function b_text(obj) {
	obj.style.border = "1px solid #cccccc";
}

// Ajax 비동기통신 함수
function Ajax_Call(url, gp, tf, fn, sn, isXML) {
	AJAX.create(); // Ajax 오브젝트 생성

	if(isXml = true)
		AJAX.openXML(gp, url, tf);
	else
		AJAX.openText(gp, url, tf);

	AJAX.setStatusSuccessHandler(fn); // 성공시 실행할 함수
	AJAX.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
	AJAX.send(sn); // 파라미터(ex: 'id=BulDDuk&Check=OK') , POST 방식도 이런식으로 처리해야함
}


//클립보드 복사
function clipboard(str) {
	window.clipboardData.setData('Text', str);
	alert('복사 되었습니다.');
}

//플래쉬 적용
function setFlash(arg1, arg2, arg3, arg4) {
	document.write("<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'");
	document.write("        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' width='"+arg2+"' height='"+arg3+"'>");
	document.write("<param name='movie' value='"+arg1+"'>");
	document.write("<param name='quality' value='high'>");
	if(arg4==true) {
		document.write('<param name="wmode" value="transparent">');
	}

	document.write("<embed src='"+arg1+"' quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' width='"+arg2+"' height='"+arg3+"' wmode='transparent'></embed>");
	document.write("</object>");
}

function JS_viewObj(source,w,h,qual,flavar) {
	document.write('<embed src="'+ source + flavar +'" quality="' + qual + '" pluginspage=http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="'+  w +'" height="' + h + '" wmode="transparent"></embed>');
	//document.write(objhtml);
}



//onkeydown textarea 탭사용
function useTab(el){
	if(9==event.keyCode){
		(el.selection=document.selection.createRange()).text="\t";
		event.returnValue=false;
	}
}


var useBSNns;

if (useBSNns) {
	if (typeof(bsn) == "undefined")
		bsn = {}
	var _bsn = bsn;
} else {
	var _bsn = this;
}

_bsn.Crossfader = function (divs, fadetime, delay ) {
	this.nAct = -1;
	this.aDivs = divs;

	for (var i=0;i<divs.length;i++) {
		document.getElementById(divs[i]).style.opacity = 0;
		document.getElementById(divs[i]).style.position = "absolute";
		document.getElementById(divs[i]).style.filter = "alpha(opacity=0)";
		document.getElementById(divs[i]).style.visibility = "hidden";
	}

	this.nDur = fadetime;
	this.nDelay = delay;
	this._newfade();
}

_bsn.Crossfader.prototype._newfade = function() {
	if (this.nID1)
		clearInterval(this.nID1);
	this.nOldAct = this.nAct;
	this.nAct++;

	if (!this.aDivs[this.nAct])	this.nAct = 0;

	if (this.nAct == this.nOldAct)
		return false;

	document.getElementById( this.aDivs[this.nAct] ).style.visibility = "visible";
	this.nInt = 50;
	this.nTime = 0;
	var p=this;
	this.nID2 = setInterval(function() { p._fade() }, this.nInt);
}

_bsn.Crossfader.prototype._fade = function() {
	this.nTime += this.nInt;
	var ieop = Math.round( this._easeInOut(this.nTime, 0, 1, this.nDur) * 100 );
	var op = ieop / 100;
	document.getElementById( this.aDivs[this.nAct] ).style.opacity = op;
	document.getElementById( this.aDivs[this.nAct] ).style.filter = "alpha(opacity="+ieop+")";

	if (this.nOldAct > -1) {
		document.getElementById( this.aDivs[this.nOldAct] ).style.opacity = 1 - op;
		document.getElementById( this.aDivs[this.nOldAct] ).style.filter = "alpha(opacity="+(100 - ieop)+")";
	}

	if (this.nTime == this.nDur) {
		clearInterval( this.nID2 );

		if (this.nOldAct > -1)
			document.getElementById( this.aDivs[this.nOldAct] ).style.visibility = "hidden";
		var p=this;
		this.nID1 = setInterval(function() { p._newfade() }, this.nDelay);
	}
}

_bsn.Crossfader.prototype._easeInOut = function(t,b,c,d) {
	return c/2 * (1 - Math.cos(Math.PI*t/d)) + b;
}

//cf1 <- div
//var cf = new Crossfader( new Array('cf1','cf2'), 1000, 2000 );



/*--------------------------- //COMMON ------------------------------*/
