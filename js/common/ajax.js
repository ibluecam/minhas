/************************************************

	Ajax 제어를 위한 클래스

************************************************/


var AJAX =
{
	XmlHttp: null,

	create : function ()
	{
		try {
				if ( window.XMLHttpRequest )
				{
					AJAX.XmlHttp = new XMLHttpRequest();

					// 일부의 모질라 버전을은 readyState property,
					// onreadystate event를 지원하지 않으므로. - from xmlextrs

					if ( this.XmlHttp.readyState == null )
					{
						this.XmlHttp.readyState = 1;

						this.XmlHttp.addEventListener( "load", function ()
						{
							this.XmlHttp.readyState = 4;

							if ( typeof this.XmlHttp.onreadystatechange == "function" ) { tmpXmlHtp.onreadystatechange(); } }, false);
						}
					}
					else {
	  try {
		AJAX.XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");

	  }catch (e1) {
	  try{
		AJAX.XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e2) { }
	  }
	 }
				}
				catch (e)
				{
						alert( "브라우저가 AJAX 를 지원하지 않네요.." );
				}
		}
}


// XML 방식으로 읽기

AJAX.openXML =  function ( method, url, async, uname, pswd )
{
	if ( AJAX.XmlHttp != null )
	{
			if ( uname == undefined )
			{
				AJAX.XmlHttp.open(method, url, async, uname, pswd);
			}
			else
			{
				AJAX.XmlHttp.open(method, url, async);
			}

			AJAX.XmlHttp.onreadystatechange = function ()
			{
					if ( AJAX.XmlHttp.readyState == 4 )
					{
							if ( AJAX.XmlHttp.status == 200 ) { AJAX.statusSuccessHandler(AJAX.XmlHttp.responseXML); }  // 200 : 성공
							else
							{
								AJAX.statusErrorHandler();
								alert( "읽기실패" );
							}
					}
			}
	}
	else { alert ( "오브젝트 생성실패" ); }
}


// Text 방식으로 읽기

AJAX.openText =  function ( method, url, async, uname, pswd )
{
	if ( AJAX.XmlHttp != null )
	{
			if ( uname == undefined )	{ AJAX.XmlHttp.open(method, url, async, uname, pswd); }
			else						{ AJAX.XmlHttp.open(method, url, async); }

			AJAX.XmlHttp.onreadystatechange = function ()
			{
					if ( AJAX.XmlHttp.readyState == 4 )
					{
							if ( AJAX.XmlHttp.status == 200 ) { AJAX.statusSuccessHandler( AJAX.XmlHttp.responseText ); }  // 200 : 성공
							else
							{
									AJAX.statusErrorHandler();
									alert( "읽기실패" );
							}
					}
			}
	} else { alert ( "오브젝트 생성실패" ); }
}


AJAX.send = function ( content )
{
	if ( content == undefined )	{ AJAX.XmlHttp.send( null ); }
	else						{ AJAX.XmlHttp.send( content ); }
}


AJAX.setOnReadyStateChange = function ( funcname )
{
	if ( AJAX.XmlHttp )		{ AJAX.XmlHttp.onreadystatechange = funcname; }
	else					{ alert ( "오브젝트 생성실패" ); }
}


// status 200 일 때 처리 함수
AJAX.statusSuccessHandler = function ( data )
{
	alert( data );
}


AJAX.setStatusSuccessHandler = function ( funcname )
{
	AJAX.statusSuccessHandler = funcname;
}


// status 200 일 때 기본 처리 함수
// setStatusSuccessHandler() 로 대체 가능

AJAX.statusErrorHandler = function ( status )
{
	AJAX.rtnText = AJAX.XmlHttp.responseText;
}


AJAX.setStatusSuccessHandler = function ( funcname )
{
	AJAX.statusSuccessHandler = funcname
}


AJAX.setStatusErrorHandler = function ( funcname )
{
	AJAX.statusErrorHandler = funcname
}


AJAX.setRequestHeader = function ( label, value )
{
	AJAX.XmlHttp.setRequestHeader( label, value );
}