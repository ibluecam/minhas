$(function() {


	/*
	* 내용을 iframe에 넣어서 CSS분리
	*/
	$('*[IframeContentsYN="Y"]')
		.after(function(){
			var Obj = $(this);
			var Html = Obj.html();

			var Width = Obj.width();
			var Height = Obj.height();

			var DateTime = new Date();
			var Id = DateTime.getTime();

			var Iframe = $('<iframe></iframe>');

			Iframe
				.width(Width)
				.height(Height)
				.css('border', '0px')
				.attr('allowTransparency', 'true')
				.attr('id', Id);

			Obj.before(Iframe);

			var JsIframe = document.getElementById(Id);
			if(JsIframe!=null){
				JsIframe = JsIframe.contentWindow || JsIframe.contentDocument;
				JsIframe.document.open();
				JsIframe.document.write(Html);
				JsIframe.document.close();
			}
			Iframe.contents().find('body')
				.css('margin','0')
				.css('padding','0');

			Iframe.height(Iframe.contents().find('body').height() + 40);
			Obj.hide();

			Iframe.load(function(){
				Iframe.height(Iframe.contents().find('body').height() + 40);
			});
		});

	/*
	* 채크박스 해제시 value 넘겨드림
	*/
	$('input[type="checkbox"]')
		.after(function(){
			var Obj = $(this);
			var Name = Obj.attr('name');
			var Value = Obj.attr('value');
			var NotValue = Obj.attr('NotValue');

			if(typeof(NotValue) == 'string')
			{
				Obj.attr('name', '');

				var Dummy = $('<input type="hidden" name="'+Name+'">');
				Obj.after(Dummy);

				if(Obj.attr('checked'))
				{
					Dummy.val(Value);
				}
				else
				{
					Dummy.val(NotValue);
				}

				Obj.click(function() {
					if(Obj.attr('checked'))
					{
						Dummy.val(Value);
					}
					else
					{
						Dummy.val(NotValue);
					}
				});


			}

		});
	/*
	* 입력예
	*/
	$('input[ViewExYN="Y"]')
		.before(function(){
			var Obj = $(this);
			var Str = Obj.attr('ViewEx');
			var Ex = $('<span>'+Str+'</span>');

			Ex.css('position', 'absolute')
				.css('text-align', 'center')
				.css('width', Obj.width()+'px')
				.css('height', Obj.height()+'px')
				.css('padding-top',((Obj.height()-12)/2)+'px')
				.css('font-size','12px')
				//.css('font-weight','bold')
				.css('color','gray');

			if(Obj.val() == '')
			{
				Obj.before(Ex);

				Obj.focus(function(){
					Ex.remove();
				});

				Ex.click(function(){
					Obj.focus();
					Ex.remove();
				});
			}

		});
	/*
	* 다중옵션 "," (콤마) 로 구분됨
	*/
	$('input[MultiCheckBoxYN="Y"]')
		.after(function() {
			var Obj = $(this);
			Obj.hide();

			var ReadOnly = Obj.attr('readonly');

			var Parent = Obj.parents().first();

			//console.log(Parent);

			var Value = Obj.attr('value');
			Value = Value.split('^');

			for(var CheckLoop = 0; CheckLoop < Value.length; CheckLoop++)
			{
				Value[Value[CheckLoop]] = true;;
			}

			//console.log(Value);

			var Values = Obj.attr('Values');
			Values = Values.split('^');

			var OnImg = Obj.attr('OnImg');
			var OffImg = Obj.attr('OffImg');

			var Items = Array();

			// 생성 및 뿌려드림
			for(var Loop = 0; Loop < Values.length; Loop++)
			{
				Items[Loop] = {
					Obj:$('<ul Idx="'+Loop+'"></ul>'),
					Obj1:$('<li></li>'),
					Obj2:$('<li></li>'),
					Visual:$('<img src="'+OffImg+'">'),
					Value:Values[Loop],
					CheckYN:'N'
				};

				if(Value[Values[Loop]] == true)
				{
					Items[Loop].Visual.attr('src', OnImg);
					Items[Loop].CheckYN = 'Y';
				}


				Items[Loop].Obj
					.append(Items[Loop].Obj1)
					.append(Items[Loop].Obj2);

				Items[Loop].Obj1.append(Items[Loop].Visual);
				Items[Loop].Obj2.html('&nbsp;'+Values[Loop]);

				if(!ReadOnly)
				{
					Items[Loop].Obj.click(function(){
						var Item = Items[$(this).attr('Idx')];

						if(Item.CheckYN == 'Y')
						{
							Item.Visual.attr('src', OffImg);
							Item.CheckYN = 'N';
						}
						else if(Item.CheckYN == 'N')
						{
							Item.Visual.attr('src', OnImg);
							Item.CheckYN = 'Y';
						}

						// 선택한놈들 추출해서 value 에다가 넣어드림
						var Value = '';
						for(var i = 0; i < Items.length; i++)
						{
							if(Items[i].CheckYN == 'Y')
							{
								if(Value != '')
								{
									Value += '^';
								}
								Value += Items[i].Value;
							}
						}
						Obj.val(Value);
					});
				}

				Parent.append(Items[Loop].Obj);
			}
		});


	/*
	* 인쇄
	*/
	$('*[BtnPrintYN="Y"]')
		.click(function(){
			var Obj = $(this);
			var PrintArea = $('*[PrintAreaYN="Y"]').clone();
			var Css = $('link[rel="stylesheet"]').clone();

			var Contents = $('<body></body>');
			var Body = $('<div style="background-color:white; width:100%; height:100%;"><div>');
			Body.append(Css);
			Body.append(PrintArea);
			Contents.append(Body);


			var WindowObj = window.open('about:blank', '', 'width=800, height=600, scrollbars=yes');
			WindowObj.document.open();
			WindowObj.document.write(Contents.html());
			WindowObj.document.close();
			WindowObj.print();
		});

	/*
	* UI 라디오박스
	*/
	if($('*[RadiosYN="Y"]').length){
		$('*[RadiosYN="Y"]')
			.buttonset();
	}

	/*
	* 숫자만 입력
	*/
	$('*[NumberOnlyYN="Y"]')
		.after(function() {
			Html.Input.NumberOnly(this);
		});

	/*
	* 다음으로 이동
	*/
	$('*[NextYN="Y"]')
		.after(function() {
			Html.Input.AutoNextFocus(this);
		});


	/*
	* 전화번호 입력란
	*/
	$('*[TelYN="Y"]')
		.after(function() {
			var Obj = $(this);
			var Parent = Obj.parents().first();
			var HyphenStr = ' - ';
			var Name = Obj.attr('Name');
			var Value = Obj.attr('Value');
			var Values = Html.Parse.ValueOfHyphen(Value);

			var NameArr = Html.Parse.NameLoop(Name);

			if(NameArr.length < 2)
			{
				NameArr[0] = '';
				NameArr[1] = '';
				NameArr[2] = '';
			}
			else
			{
				Obj.attr('name', '');
			}

			var ObjArr = Array();
			for(var i = 0; i <= (NameArr.length - 1); i++)
			{
				ObjArr[i] = Obj.clone();

				if(i != 0)
				{
					Parent.append(HyphenStr);
				}

				Parent.append(ObjArr[i]);
				ObjArr[i].attr('name', NameArr[i]);
				ObjArr[i].attr('value', Values[i]);
				ObjArr[i].attr('title', ObjArr[i].attr('title')+(i+1));

				if(i == 0)
				{
					ObjArr[i].attr('size', '3');
					ObjArr[i].attr('maxlength', '3');
				}
				else
				{
					ObjArr[i].attr('size', '4');
					ObjArr[i].attr('maxlength', '4');
				}

				ObjArr[i].keyup(function(){
					Obj.val(Html.Input.Get.Values(ObjArr));
				});

				Html.Input.AutoNextFocus(ObjArr[i]);
				Html.Input.NumberOnly(ObjArr[i]);
			}

			Obj.hide();
		});

	/*
	* 이메일 입력란
	*/
	$('*[EmailYN="Y"]')
		.hide()
		.after(function() {
			var Obj = $(this);
			var Name = $('<input type="text" class="'+Obj.attr('class')+'">');
			var At = $('<span> @ </span>');
			var Server = $('<input type="url" size="10" class="'+Obj.attr('class')+'">');
			var Select = $('<select class="'+Obj.attr('class')+'"></select>')
							.append('<option value="">::서버선택::</option>')
							.append('<option value="@">::직접입력::</option>')
							.append('<option value="@n-os.com">n-os.com</option>')
							.append('<option value="@nate.com.com">nate.com</option>')
							.append('<option value="@naver.com">naver.com</option>')
							.append('<option value="@daum.net">daum.net</option>')
							.append('<option value="@gmail.com">gmail.com</option>');

			// 기존 입력값 보상해줌
			if(Obj.val() != '')
			{
				var Email = Obj.val().split('@');
				if(Email[0] != '')
				{
					Name.val(Email[0]);
				}
				if(Email[1] != '')
				{
					// 여기 나중에 수정해야함
					// 기존꺼랑 중복되더라 ㅋㅋ
					Select.append('<option value="@'+Email[1]+'">'+Email[1]+'</option>');
					Select.val('@'+Email[1]);
				}
			}

			Obj.after(Name);
			$(Name).after(At);
			$(At).after(Server);
			$(Server).after(Select);

			Name.attr('class', Obj.attr('class'));
			Name.attr('size', Obj.attr('size'));
			Name.attr('maxsize', Obj.attr('maxsize'));

			Name.show();
			At.show();
			Server.hide();
			Select.show();

			Select.change(function()
			{
				if($(this).val() == '@')
				{
					//At.show()
					Server.show();
				}
				else
				{
					//At.hide()
					Server.hide();
				}
				Server.val('');
				Update()
			});

			Name.keyup(function()
			{
				Update()
			});
			Server.keyup(function()
			{
				Update()
			});

			function Update()
			{
				if(Select.find('option:selected').val() == '@')
				{
					Obj.val(Name.val() + '@' + Server.val());
				}
				else
				{
					Obj.val(Name.val() + Select.find('option:selected').val());
				}
			}

		});


	/*
	* 시간
	*/
	$('*[DateTimeYN="Y"]')
		.hide()
		.after(function() {
			var Obj = $(this);

			var DateTime = Html.Parse.DateTime($(this).val());

			var Parent = Obj.parents().first();

			var Date = $('<input type="text" class="date '+Obj.attr('class')+'">');
			Date.attr('size', '10');
			Date.val(DateTime['Date']);

			Obj.after(Date);
			Html.Input.Datepicker(Date);

			var Hour = $('<select class="time '+Obj.attr('class')+'"></select>');
			var HourStr = $('<span>시</span>');
			var Min = $('<select class="time '+Obj.attr('class')+'"></select>');
			var MinStr = $('<span>분</span>');

			//console.log(Parent);

			// 00 ~ 23
			for(var H = 0; H <= 23; H ++)
			{
				if(H < 10)
				{
					var Hstr = '0'+H;
				}
				else
				{
					var Hstr = H;
				}
				Hour.append('<option value="'+Hstr+'">'+Hstr+'</option>');
			}

			// 00 ~ 59
			for(var M = 0; M <= 59; M = M + 5)
			{
				if(M < 10)
				{
					var Mstr = '0'+M;
				}
				else
				{
					var Mstr = M;
				}
				Min.append('<option value="'+Mstr+'">'+Mstr+'</option>');
			}

			Hour.val(DateTime['H']);
			Min.val(DateTime['i']);

			Parent.append(Hour);
			Hour.after(HourStr);
			HourStr.after(Min);
			Min.after(MinStr);


			Parent.change(function(){
				DummyInit();
			});
			Hour.change(function(){
				DummyInit();
			});
			Min.change(function(){
				DummyInit();
			});

			function DummyInit() {
				var HourStr = Hour.val();
				var MinStr = Min.val();
				var DateStr = Date.val();

				if(DateStr == '')
				{
					DateStr = '0000-00-00';
				}

				var DateTimeStr = DateStr+' '+HourStr+':'+MinStr+':00';
				//console.log(Obj);
				Obj.val(DateTimeStr);

			}

		});
	/*
	* 달력
	*/
	$('*[DateYN="Y"]')
		.attr('size', '10')
		.after(function(){
			var DateTime = Html.Parse.DateTime($(this).val());
			$(this).val(DateTime['Date']);

			Html.Input.Datepicker(this);
		});
	/*
	* 달력
	*/
	$('*[CalendarYN="Y"]')
		.attr('size', '10')
		.after(function(){
			var DateTime = Html.Parse.DateTime($(this).val());
			$(this).val(DateTime['Date']);

			Html.Input.Datepicker(this);
		});
	/*
	* 레이어 팝업
	*/
	// $('*[LayerPopUpYN="Y"]')
	// 	.after(function(){
	// 		$('*[LayerPopUpContentsYN="Y"]').hide();
	// 	})
	// 	.css('cursor', 'pointer')
	// 	.click(function() {
	// 		var Obj = $(this);
	// 		var Id = Obj.attr('LayerPopUpId');
	// 		var Title = Obj.attr('LayerPopUpTitle');
	// 		var Contents = $('*[LayerPopUpContentsYN="Y"][LayerPopUpId="'+Id+'"]').html();

	// 		$(Contents).dialog({
	// 			//height: 500,
	// 			title: Title,
	// 			modal: true,
	// 			buttons: {
	// 				'확인': function() {
	// 					$( this ).dialog( "close" );
	// 				}
	// 			}
	// 		});
	// 	});

	/*
	* 오토픽업
	*/
	$('*[PickUpYN="Y"]').after(function() {
			$(this).find('*[PickUpSubYN="Y"]').hide();
		})
		.mouseenter(function() {
			var Obj = $(this);
			var Sub = Obj.find('*[PickUpSubYN="Y"]');

			Sub.css('position', 'absolute');
			Sub.show('fast');

			Obj.mouseleave(function() {
				Sub.hide('fast');
			});
		});

	/*
	* 동영상 플레이어
	*/
	$('*[MoviePlayerYN="Y"]')
		.click(function() {
			var Type = $(this).attr('MoviePlayerType');
			var Src = $(this).attr('href');
			var Width = $(this).attr('width');
			var Height = $(this).attr('height');
			new MoviePlayer.Open(Type, Src, Width, Height);
			return false;
		});
	/*
	* 사진을 지정된곳에 크게 보기
	*/
	$('*[PhotoViewMainYN="Y"]')
		.after(function() {
			var ViewFrame = $(this);
			var Id = ViewFrame.attr('PhotoViewID');
			PhotoView.Init(Id);
		});

	$('*[PhotoViewBtnPrevYN="Y"]')
		.click(function() {
			var Obj = $(this);
			var Id = Obj.attr('PhotoViewID');
			PhotoView.PrevDisplay(Id, Obj);
		});

	$('*[PhotoViewBtnNextYN="Y"]')
		.click(function() {
			var Obj = $(this);
			var Id = Obj.attr('PhotoViewID');
			PhotoView.NextDisplay(Id, Obj);
		});

	$('*[PhotoViewYN="Y"]')
		.mouseover(function() {
			var Obj = $(this);
			var Id = Obj.attr('PhotoViewID');
			var PhotoIdx = Obj.attr('PhotoViewIdx');
			PhotoView.MainDisplay(Id, PhotoIdx);
		});

	/*
	* 웹 에디터다 ㅋㅋㅋㅋ
	*/
	$('textarea[TextAreaEditorYN="Y"]')
		.after(function() {

			var TextArea = $(this);

			TextArea.css('padding', '0');

			var Height = '';
			var Width = '';

			if(TextArea.attr('width'))
			{
				Width = TextArea.width();
			}
			else
			{
				Width = '100%';
			}

			if(TextArea.attr('height'))
			{
				Height = TextArea.height();
			}
			else
			{
				Height = '400px';
			}

			//console.log(TextArea.attr('width'), TextArea.attr('height'));
			//console.log(Height, Width);

			var TextAreaId = TextArea.attr('id');

			if(!TextAreaId) {
				TextAreaId = TextArea.attr('name');
				TextArea.attr('id', TextAreaId);
			}

			tinyMCE.init({
				// General options
				elements : TextAreaId,
				mode : "exact",
				theme : "advanced",
				//theme : "simple",

				//skin : "o2k7",
				//skin_variant : "silver",
				language : "ko",

				height : Height,
				width : Width,

				//IE에서 한글입력 문제 해결을 위해서
				forced_root_block : false,

				//plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",
				//plugins : "autolink,lists,pagebreak,table,advhr,advimage,advlink,emotions,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,xhtmlxtras,wordcount,advlist,autosave",
				plugins : "autolink,lists,pagebreak,table,advhr,advimage,advlink,emotions,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,xhtmlxtras,wordcount,advlist,autosave",

				// Theme options
				/*
				theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
				theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
				*/

				//theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect,|,preview,fullscreen,print",
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,|,preview,fullscreen,print",
				//theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,code,|,forecolor,backcolor,|,cite,abbr,acronym,del,ins,attribs",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,code,|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,media,advhr,|,ltr,rtl",

				theme_advanced_resizing_use_cookie : 0,

				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,

				// Example content CSS (should be your site CSS)
				content_css : "./editor/tiny_mce/css/content.css",
				popup_css_add : "./editor/tiny_mce/css/popup.css",

				// Drop lists for link/image/media/template dialogs
				template_external_list_url : "lists/template_list.js",
				external_link_list_url : "lists/link_list.js",
				external_image_list_url : "lists/image_list.js",
				media_external_list_url : "lists/media_list.js",

				// Style formats
				style_formats : [
					{title : 'Bold text', inline : 'b'},
					{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
					{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
					{title : 'Example 1', inline : 'span', classes : 'example1'},
					{title : 'Example 2', inline : 'span', classes : 'example2'},
					{title : 'Table styles'},
					{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
				],

				// Replace values for the template plugin
				template_replace_values : {
					username : "Some User",
					staffid : "991234"
				}
			});
		});

});

var Html =
{
	Parse:
	{
		DateTime:function(DateTime)
		{
			var Ret = Array();

			if(typeof(DateTime) == 'string')
			{
				if(DateTime.indexOf(' ') != -1)
				{
					var DateTimeSplit = DateTime.split(' ');
					Ret['Date'] = DateTimeSplit[0];
					Ret['Time'] = DateTimeSplit[1];
				}
				else
				{
					Ret['Date'] = DateTime;
					Ret['Time'] = '';
				}

				if(Ret['Date'].indexOf('-') != -1)
				{
					var DateSplit = Ret['Date'].split('-');
					Ret['Y'] = DateSplit[0];
					Ret['m'] = DateSplit[1];
					Ret['d'] = DateSplit[2];
				}

				if(Ret['Time'].indexOf(':') != -1)
				{
					var TimeSplit = Ret['Time'].split(':');
					Ret['H'] = TimeSplit[0];
					Ret['i'] = TimeSplit[1];
					Ret['s'] = TimeSplit[2];
				}

				if(typeof(Ret['Y']) == 'undefined')
				{
					Ret['Y'] = '';
				}
				if(typeof(Ret['m']) == 'undefined')
				{
					Ret['m'] = '';
				}
				if(typeof(Ret['d']) == 'undefined')
				{
					Ret['d'] = '';
				}
				if(typeof(Ret['H']) == 'undefined')
				{
					Ret['H'] = '';
				}
				if(typeof(Ret['i']) == 'undefined')
				{
					Ret['i'] = '';
				}
				if(typeof(Ret['s']) == 'undefined')
				{
					Ret['s'] = '';
				}

			}

			return Ret;
		},
		ValueOfHyphen:function(Value, HyphenStr)
		{
			var Ret = Array();
			if(typeof(HyphenStr) == 'undefined')
			{
				HyphenStr = '-';
			}

			Ret = Value.split(HyphenStr);

			for(var i = 0; i <= 10; i++)
			{
				if(typeof(Ret[i]) == 'undefined')
				{
					Ret[i] = '';
				}
			}

			return Ret
		},
		NameLoop:function(Name)
		{
			var Ret = Array();

			if(Name.indexOf('~') != -1)
			{
				var NameSplit = Name.split('[');
				var NameLoopStr = NameSplit[NameSplit.length - 1];

				if(NameLoopStr.substr(NameLoopStr.length - 2) == ']]')
				{
					NameLoopStr = NameLoopStr.substr(0, (NameLoopStr.length - 2));
					var NameEd = ']';
				}
				else
				{
					NameLoopStr = NameLoopStr.substr(0, (NameLoopStr.length - 1));
					var NameEd = '';
				}

				var NameLoopArr = NameLoopStr.split('~');

				var NameSt = Name.substr(0, Name.lastIndexOf('['));
				var LoopCnt = 0;

				//console.log(NameSplit, NameLoopStr, NameLoopArr, Name);

				for(var i = parseInt(NameLoopArr[0]); i <= parseInt(NameLoopArr[1]); i++)
				{
					Ret[LoopCnt] = NameSt+i+NameEd;
					LoopCnt++;
				}
				//Obj.attr('name', '');
			}
			else
			{
				Ret[0] = Name;
			}

			return Ret;
		}
	},
	Input:
	{
		Verifi:function(Parent)
		{
			// form 별로 동작되는 로직 미완성
			var AlertItem = '';
			var AlertItems = Array();
			var AlertObj = Array();

			var VerifiObjs = $('*[VerifiYN="Y"]');
			//console.log(VerifiObjs);
			//return false;
			var VerifiObjsName = Array();

			$.each(VerifiObjs, function(){
				var VerifiObj = $('*[VerifiYN="Y"][name="'+$(this).attr('name')+'"]');
				var VerifiObjTagName = VerifiObj.get(0).tagName;
				var VerifiObjType = VerifiObj.attr('Type');
				var VerifiObjName = VerifiObj.attr('Name');
				var VerifiObjId = VerifiObj.attr('Id');
				var VerifiObjTitle = VerifiObj.attr('Title');
				var VerifiObjValue = VerifiObj.attr('Value');

				if(VerifiObjType == 'radio')
				{
					VerifiObjValue = '';
				}

				if(VerifiObj.attr('TextAreaEditorYN') == 'Y')
				{
					VerifiObjValue = tinyMCE.get(VerifiObjId).getContent();
				}

				if(typeof(VerifiObjsName[VerifiObjName]) == 'undefined')
				{

					VerifiObjsName[VerifiObjName] = true;

					// checkbox 처리
					var VerifiObjChecked = false;
					if(VerifiObjType == 'radio')
					{
						$.each(VerifiObj, function(RadioKey, RadioObj){
							if($(RadioObj).attr('checked') == 'checked')
							{
								VerifiObjChecked = true;
								//console.log(RadioObj, $(RadioObj).attr('checked'));
							}
						});
					}

					//console.log(VerifiObjChecked);

					if(VerifiObjName != '')
					{
						if(VerifiObjChecked == false && VerifiObjValue == '') // || VerifiObjValue != '')
						{
							//console.log('T', VerifiObjChecked, VerifiObj);
							//console.log(VerifiObjName, VerifiObjChecked, VerifiObjValue);

							if(AlertItem != '')
							{
								AlertItem += ', ';
							}
							AlertItem += VerifiObjTitle;

							AlertObj[AlertObj.length] = VerifiObj;

						}
					}
				}
			});

			//return false;

			if(AlertObj.length > 0)
			{
				$('<div>['+AlertItem+'] 란을 확인해 주세요</div>').dialog({
					//height: 500,
					title: '입력란을 확인해 주세요',
					modal: true,
					buttons: {
							'확인': function() {
								AlertObj[0].focus();
								//console.log(AlertObj[0], AlertObj[1]);
								$( this ).dialog( "close" );
							}
						}
					});
				return false;
			}
			else
			{
				return true;
			}
		},
		Get:{
			Values:function(Obj, HyphenStr)
			{
				if(typeof(HyphenStr) != 'string')
				{
					var HyphenStr = '-';
				}

				var Ret = '';

				if(typeof(Obj) == 'object')
				{
					$.each(Obj, function(key, Item) {
						if(Ret != '')
						{
							Ret += HyphenStr;
						}

						Ret += Item.val();
						//console.log(key, val);
					});

					return Ret;
				}
				else
				{
					return '';
				}
			}
		},
		Datepicker:function(Obj)
		{
			var Obj = $(Obj);

			var minDate = Obj.attr('minDate');

			if(typeof(minDate) != 'string')
			{
				minDate = '';
			}

			Obj.datepicker({
				showOn: "both",
				buttonImage: "/images/admin/calendar_icon.gif",
				buttonImageOnly: true,

				showAnim: 'slideDown',

				minDate: minDate,

				changeMonth: true,
				changeYear: true,
				dateFormat: 'yy-mm-dd',
				yearRange: 'c-90:c+10',
				dayNames: Array('일', '월', '화', '수', '목', '금', '토'),
				dayNamesMin: Array('일', '월', '화', '수', '목', '금', '토'),
				monthNames: Array('1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'),
				monthNamesShort: Array('1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월')
			})
		},
		NumberOnly:function(Obj)
		{
			var Obj = $(Obj);

			Obj.css("ime-mode", "disabled")
			.attr('pattern', '[0-9]*')
			.keypress(function(event)
			{
				if(event.which && event.which != 8 && (event.which < 48 || event.which > 57) )
				{
					event.preventDefault();
				}
			})
			.keyup(function()
			{
				if( $(this).val() != null && $(this).val() != '' )
				{
					$(this).val( $(this).val().replace(/[^0-9]/g, '') );
				}
			})
			.focusout(function()
			{
				if( $(this).val() != null && $(this).val() != '' )
				{
					$(this).val( $(this).val().replace(/[^0-9]/g, '') );
				}
			});
		},
		AutoNextFocus:function(Obj)
		{
			var Obj = $(Obj);
			Obj.keyup(function()
			{

				var Limit = 0;
				if(typeof($(this).attr('maxsize')) != 'undefined')
				{
					Limit = parseInt($(this).attr('maxsize'));
				}
				else if(typeof($(this).attr('size')) != 'undefined')
				{
					Limit = parseInt($(this).attr('size'));
				}

				if( $(this).val() != null && $(this).val() != '' )
				{
					if(Limit > 0 && Limit <= $(this).val().length)
					{
						$(this).next('input').focus();
					}
				}
			});
		}
	}
}


var Cookie = {
	Set:function(Key, Val, Exp)
	{
		//console.log(Key, Val, Exp);
		var todayDate = new Date();
		todayDate.setDate( todayDate.getDate() + Exp );
		//console.log(todayDate.toGMTString());
		//console.log(Key + '=' + escape( Val ) + '; path=/; expires=' + todayDate.toGMTString() + ';');
		document.cookie = Key + '=' + escape( Val ) + '; path=/; expires=' + todayDate.toGMTString() + ';';
	},
	Get:function(Key)
	{
		var nameOfCookie = Key + '=';
		var x = 0;

		while ( x <= document.cookie.length )
		{
			var y = (x+nameOfCookie.length);

			if ( document.cookie.substring( x, y ) == nameOfCookie )
			{
				if ( (endOfCookie=document.cookie.indexOf( ';', y )) == -1 )
				{
					endOfCookie = document.cookie.length;
				}
				return unescape( document.cookie.substring( y, endOfCookie ) );
			}

			x = document.cookie.indexOf( " ", x ) + 1;

			if ( x == 0 )
			{
				break;
			}
		}
		return false;
	}
}

var Popup = {
	PopupEnv:Array(),
	PopupObj:Array(),
	PopupPos:
	{
		Left:20,
		Top:20
	},
	Open:function(Env)
	{
		// 정보 집어넣음
		Popup.PopupEnv[Env.idx] = Env;

		// 쿠키가 없다면 뿌려드림
		if(Cookie.Get('PopupCloseRemind'+Env.idx) != 'YES')
		{

			var Scrollbars = 'no';
			var Height = Env.win_height;
			var Width = Env.win_width;

			if(Env.scroll_yn == 'Y')
			{
				Scrollbars = 'yes'
			}
			else
			{
				Scrollbars = 'no'
			}

			if(Env.type == 'New')
			{
				Popup.PopupObj[Env.idx] = window.open('about:blank', '', 'width='+Width+', height='+Height+', scrollbars='+Scrollbars+'');
				Popup.PopupObj[Env.idx].document.write(Env.contents);

				setTimeout(function() {
					var Child = Popup.PopupObj[Env.idx];
					Child.Popup.PopupEnv = Popup.PopupEnv;
					Child.Popup.PopupObj = Popup.PopupObj;
				}, 1500);
			}
			else if(Env.type == 'Layer')
			{
				Popup.PopupObj[Env.idx] = $('<div>'+Env.contents+'</div>');

				Popup.PopupObj[Env.idx]
					//.height(Height)
					.width(Width)
					.css('z-index', '99999')
					.css('position', 'fixed')
					.css('top', Popup.PopupPos.Left+'px')
					.css('left', Popup.PopupPos.Left+'px');

				Popup.PopupPos.Top = Popup.PopupPos.Top + 20;
				Popup.PopupPos.Left = Popup.PopupPos.Left + 20;

				$('html').append(Popup.PopupObj[Env.idx]);
			}
		}
	},
	Close:function(Idx)
	{
		if(Popup.PopupEnv[Idx].type == 'New')
		{
			self.close();
		}
		else
		{
			Popup.PopupObj[Idx].remove();
		}
	},
	CloseRemind:function(Idx, Day)
	{
		Cookie.Set('PopupCloseRemind'+Idx, 'YES', parseInt(Day));
		Popup.Close(Idx);
	}
}

var PhotoView = {
	Main:Array(),
	MainTitle:Array(),
	MainDate:Array(),

	Items:Array(),
	ItemCnt:Array(),
	ItemCur:Array(),

	// 초기화
	Init:function(Id)
	{
		var Parent = PhotoView;
		Parent.Main[Id]			= $('*[PhotoViewMainYN="Y"][PhotoViewID="'+Id+'"]');
		Parent.MainTitle[Id]	= $('*[PhotoViewMainTitleYN="Y"][PhotoViewID="'+Id+'"]');
		Parent.MainDate[Id]		= $('*[PhotoViewMainDateYN="Y"][PhotoViewID="'+Id+'"]');
		Parent.Items[Id]		= $('*[PhotoViewYN="Y"][PhotoViewID="'+Id+'"]');
		Parent.ItemCnt[Id]		= (Parent.Items[Id].length - 1);
		Parent.ItemCur[Id]		= 0;

		$(Parent.Items[Id]).each(function(key, val) {
			//console.log(key, val);
			$(this).attr('PhotoViewIdx', key);
		});

		Parent.MainDisplay(Id, 0);
	},
	// Main에 뿌려줌
	MainDisplay:function(Id, ItemNum)
	{
		var Parent = PhotoView;

		var Main		= Parent.Main[Id];
		var MainTitle	= Parent.MainTitle[Id];
		var MainDate	= Parent.MainDate[Id];
		var Items		= Parent.Items[Id];
		var ItemCnt		= parseInt(Parent.ItemCnt[Id]);
		var ItemCur		= parseInt(Parent.ItemCur[Id]);

		var Item = Items[ItemNum];

		var PhotoSrc = $(Items[ItemNum]).attr('PhotoSrc');
		var FancyBoxSrc = $(Items[ItemNum]).attr('FancyBoxSrc');
		var PhotoTitle = $(Items[ItemNum]).attr('PhotoTitle');
		var PhotoDate = $(Items[ItemNum]).attr('PhotoDate');

		if(FancyBoxSrc == 'undefined')
		{
			FancyBoxSrc = PhotoSrc;
		}

		//console.log(PhotoSrc);
		Parent.ItemCur[Id] = ItemNum;

		if($(Main).attr('FancyBoxYN') == 'Y')
		{
			var FancyBox = $(''
					+'<a href="'+FancyBoxSrc+'">'
					+'<img src="'+PhotoSrc+'" PhotoViewIdx="'+ItemNum+'">'
					+'</a>'
				+'');

			FancyBox.fancybox({
					'transitionIn'		: 'none',
					'transitionOut'		: 'none',
					'titlePosition' 	: 'over',
					'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
						return '<span id="fancybox-title-over">Image ' + (currentIndex + 2) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
					}
				});

			$(Main).html(FancyBox);
		}
		else
		{
			$(Main).html('<img width="600" height="450" src="'+PhotoSrc+'" PhotoViewIdx="'+ItemNum+'">');
		}


		$(Main).find('img').load(function() {
			$(Main).height($(this).height());
			$(Main).width($(this).width());
		});

		$(MainTitle).html(PhotoTitle);
		$(MainDate).html(PhotoDate);
	},
	NextDisplay:function(Id, Obj)
	{
		var Parent = PhotoView;
		var ItemCnt		= parseInt(Parent.ItemCnt[Id]);
		var ItemCur		= parseInt(Parent.ItemCur[Id]);

		var NextPageUrl	= $(Obj).attr('NextPageUrl');

		//console.log(NextPageUrl, ItemCur, ItemCnt);

		if(ItemCur >= ItemCnt) {
			if(NextPageUrl == '') {
				alert('마지막입니다.');
			} else {
				location.href = NextPageUrl;
			}
		} else {
			Parent.MainDisplay(Id, (ItemCur + 1));
		}
	},
	PrevDisplay:function(Id, Obj)
	{
		var Parent = PhotoView;
		var ItemCnt		= parseInt(Parent.ItemCnt[Id]);
		var ItemCur		= parseInt(Parent.ItemCur[Id]);

		var PrevPageUrl	= $(Obj).attr('PrevPageUrl');

		if(ItemCur <= 0) {
			if(PrevPageUrl == '') {
				alert('처음페이지입니다.');
			} else {
				location.href = PrevPageUrl;
			}
		} else {
			Parent.MainDisplay(Id, (ItemCur - 1));
		}
	}
}

var MoviePlayer = {

	// 레이어 div obj
	Layer:{},
	// BG 레이어 div obj
	BG:{},
	// Center 레이어 div obj
	Center:{},
	// Contents 레이어 div obj
	Contents:{},

	// 플레이어 str
	Player:'',
	// 플레이어 ttoe str
	PlayerType:'',
	// 플레이어 Obj
	PlayerObj:{},

	PlayerWidth:600,
	PlayerHeight:450,

	// 스크린 폭
	ScreenWidth:document.documentElement.clientWidth,
	// 스크린 높이
	ScreenHeight:document.documentElement.clientHeight,

	// 고유 ID생성
	TimeID:Math.round((new Date()).getTime() / 1000),
	Close:function()
	{
		if(MoviePlayer.PlayerType == 'wm')
		{
			MoviePlayer.CloseWM();
		}
		MoviePlayer.Layer.hide();
		MoviePlayer.Layer.remove();
	},
	CloseWM:function()
	{
		if(typeof(MoviePlayer.PlayerObj.controls) != 'undefined')
		{
			MoviePlayer.PlayerObj.controls.stop();
		}
	},
	Open:function(Type, Src)
	{
		//console.log(Type, Src);
		MoviePlayer.PlayerType = Type;
		MoviePlayer.PlayerSrc = Src;

		MoviePlayer.PlayerInit();
		MoviePlayer.LayerInit();
	},
	PlayerInit:function()
	{
		if(MoviePlayer.PlayerType == 'wm')
		{
			MoviePlayer.Player = MoviePlayer.PlayerWM(MoviePlayer.PlayerSrc);
		}
		else if(MoviePlayer.PlayerType == 'swf')
		{
			MoviePlayer.Player = MoviePlayer.PlayerFlash(MoviePlayer.PlayerSrc);
		}
		else if(MoviePlayer.PlayerType == 'flv')
		{
			MoviePlayer.Player = MoviePlayer.PlayerJwPlayer(MoviePlayer.PlayerSrc);
		}
	},
	PlayerJwPlayer:function(Src)
	{
		var Str = '';
		Str = '<div id="'+MoviePlayer.TimeID+'">Wait..</div>											';
		Str += '<script type="text/javascript" src="./player/jw_player_5.10/jwplayer.js"><\/script>		';
		Str += '<script type="text/javascript">															';
		Str += '$("#"+MoviePlayer.TimeID).ready(function(){												';
		Str += '	jwplayer("'+MoviePlayer.TimeID+'").setup({											';
		Str += '		width: '+MoviePlayer.PlayerWidth+',												';
		Str += '		height: '+MoviePlayer.PlayerHeight+',											';
		Str += '		autostart: true,																';
		Str += '		flashplayer: "./player/jw_player_5.10/player.swf",								';
		Str += '		file: "'+Src+'",																';
		Str += '		image: ""																		';
		Str += '	});																					';
		Str += '});																						';
		Str += '<\/script>																				';
		return Str;
	},
	PlayerFlash:function(Src)
	{
		var Str = '';
		Str += '<object id="'+MoviePlayer.TimeID+'" width="'+MoviePlayer.PlayerWidth+'" height="'+MoviePlayer.PlayerHeight+'" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0">';
		Str += '<param name="SRC" value="'+Src+'">';
		Str += '<param name="autosize" value="true"/>';
		Str += '<embed src="'+Src+'" width="'+MoviePlayer.PlayerWidth+'" height="'+MoviePlayer.PlayerHeight+'"></embed>';
		Str += '</object> ';
		return Str;
	},
	PlayerWM:function(Src)
	{
		var Str = '';
		Str += '<object id="'+MoviePlayer.TimeID+'" classid="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6" width="'+MoviePlayer.PlayerWidth+'" height="'+MoviePlayer.PlayerHeight+'">';
		Str += '<param name="URL" value="'+Src+'"/>';
		Str += '<param name="autostart" value="true"/>';
		Str += '<param name="enablecontextmenu" value="0"/>';
		Str += '<embed name="'+MoviePlayer.TimeID+'" src="'+Src+'" width="'+MoviePlayer.PlayerWidth+'" height="'+MoviePlayer.PlayerHeight+'" type=video/mpeg autostart="true"></embed>';
		Str += '</object>';
		return Str;
	},
	LayerInit:function()
	{
		// 전체
		MoviePlayer.Layer = $('<div></div>');
		MoviePlayer.Layer.css('position', 'fixed')
			.css('left', '0px')
			.css('top', '0px')
			.css('z-index', '99999')
			.css('height', '100%')
			.css('width', '100%');

		// 바탕화면
		MoviePlayer.BG = $('<div></div>');
		MoviePlayer.BG.css('position', 'fixed')
			.css('left', '0px')
			.css('top', '0px')
			.css('height', '100%')
			.css('width', '100%')
			.css({'background':'rgba(0, 0, 0, 0.3)'})
			.css({'filter':'progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)'})
			.css({'-ms-filter':'progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)'});

		var PosLeft = (MoviePlayer.ScreenWidth - MoviePlayer.PlayerWidth) / 2;
		var PosTop = (MoviePlayer.ScreenHeight - MoviePlayer.PlayerHeight) / 2;

		// 내용
		MoviePlayer.Contents = $('<div></div>');
		MoviePlayer.Contents.css('position', 'fixed')
			.css('left', PosLeft+'px')
			.css('top', PosTop+'px')
			.css('width', MoviePlayer.PlayerWidth+'px')
			.css('height', MoviePlayer.PlayerHeight+'px')
			.css('padding', '10px')
			//.css('background', '#000');
			.css('background', 'url(./images/user/bbs_images/bg_video.png) repeat');

		var Close = $('<div><img src="./images/user/bbs_images/btn_close.png" alt="VIDEO" width="20" height="19" /></div>')
			.css('position', 'absolute')
			.css('top', '-10px')
			.css('right', '-10px');

		$('body').before(MoviePlayer.Layer);

		MoviePlayer.Layer.append(MoviePlayer.BG).append(MoviePlayer.Contents);
		MoviePlayer.Contents.append(Close).append(MoviePlayer.Player);

		Close.click(function(){
			MoviePlayer.Close();
		});

		// 자동으로 센터로 이동
		MoviePlayer.CenterInit();
		// 플레이어 객채불러옴
		MoviePlayer.PlayerObj = document.getElementById(MoviePlayer.TimeID);
		//MoviePlayer.PlayerObj.StopPlay();
		//console.log(MoviePlayer.PlayerObj.GetVariable('width'));
		//console.log(MoviePlayer.PlayerObj.GetVariable('Width'));

		// 자동 리사이즈
		for(var i = 500; i <= 3000; i += 100) {
			//setTimeout('MoviePlayer.ReSize()', i);
		}
	},
	CenterInit:function()
	{
		var ChildWidth = MoviePlayer.Contents.children().first().width();
		var ChildHeight = MoviePlayer.Contents.children().first().height();
	},
	ReSize:function()
	{
		if(typeof(MoviePlayer.PlayerObj) != 'undefined' && MoviePlayer.PlayerType == 'wm') {
			// 파폭서는 사이즈 못 읽어옴
			if(typeof(MoviePlayer.PlayerObj.currentMedia) != 'undefined')
			{
				var width = MoviePlayer.PlayerObj.currentMedia.imageSourceWidth + 5;
				var height = MoviePlayer.PlayerObj.currentMedia.imageSourceHeight + 75;

				var PosTop = (MoviePlayer.ScreenHeight - height) / 2;
				var PosLeft = (MoviePlayer.ScreenWidth - width) / 2;

				if(width > 400 && height > 300)
				{
					MoviePlayer.Contents.css('top', PosTop+'px').css('left', PosLeft+'px');
					MoviePlayer.PlayerObj.width = width;
					MoviePlayer.PlayerObj.height = height;
				}
			}
		}
		MoviePlayer.CenterInit();
	}
}