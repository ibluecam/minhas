
	
$("#admin_carmodel").chained("#admin_carmake");

	$(function () {
      $('.btnGetAll').click(function () {
		  
  		if ($("[name='idx[]']:checked").length) {
          var chkId = '';
		  var chksender = '';
          $("[name='idx[]']:checked").each(function () {
            chkId += $(this).val() + ",";
			chksender += $(this).attr('data-sender') + ",";
          });
          chkId = chkId.slice(0, -1);
		  chksender = chksender.slice(0, -1);
          
		   if(chkId != 0) {
			
				if (!confirm('Do you want to delete message?'))
				{
					return;
	
				}else{
					window.location.href = "/?c=admin&m=adm_delete_message&idx="+chkId+"&sender_no="+chksender;
				}
			
        	}
		  
        }
        else {
          alert('Please select message(s) to delete.');
        }
      });

      $('.chkSelectAll').click(function () {
        $("[name='idx[]']").prop('checked', $(this).is(':checked'));
      });

      $("[name='idx[]']").click(function () {
        if ($("[name='idx[]']:checked").length == $("[name='idx[]']").length) {
          $('.chkSelectAll').prop('checked', true);

        }
        else {
          $('.chkSelectAll').prop('checked', false);
        }
       });

    });
