$(document).ready(function(){
	/* The following code is executed once the DOM is loaded */
	
	get_message();
	//setInterval(get_message, 5000);
	
	/* This flag will prevent multiple comment submits: */
	var working = false;
	
	/* Listening for the submit event of the form: */
	$('#addCommentForm').submit(function(e){

 		e.preventDefault();
		if(working) return false;
		
		working = true;
		$('#submit').val('Working..');
		$('span.error').remove();
		var comment = $("#body").val();
		/* Sending the form fileds to submit.php: */
		//$(comment).hide().insertAfter('.get_comment').slideDown();
		$.ajax({
			  url: '/?c=admin&m=adm_message_detail',
			  dataType: 'text',
			  type: 'POST',
			  data: $(this).serialize(),
			  success: function( resp ) {
			  	//console.log(resp);
			  	working = false;
				$('#submit').val('Submit');
				$('#body').val('');
				/*var html1 = $("#addCommentContainer").html();
				$("#addCommentContainer").html(resp  + html1).delay(1.5);;*/

				get_message();


			  	//document.getElementById("addCommentContainer").innerHTML=resp+document.getElementById("addCommentContainer").innerHTML;
			 	//$('Hello').html().insertAfter('.get_comment').slideDown();
			    //$( comment).insertBefore( '.get_comment' );
			  },
			  error: function( req, status, err ) {
			    console.log( 'something went wrong', status, err );
			  }
		});

	});





	
});


function get_message(){
	
	var get_Note = $("#get_note_role").val();

	$.ajax({
		  url: '/?c=admin&m=get_message&n='+get_Note,//n = note
		  dataType: 'text',
		  success: function( resp ) {
		    $( '#get_message').html( resp );
		  },
		  error: function( req, status, err ) {
		    console.log( 'something went wrong', status, err );
		  }
	});

}