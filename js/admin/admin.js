
//IWC 설정 초기화
function data_reset()
{
    if(!confirm('설정 내용을 초기화 하시겠습니까?')) return;
    
    location.href = '/?c=iwc&m=reset_site_admin';
}

//관리자 로그인 체크
function login_chk(frm)
{
    if(!validate(frm.member_id, '아이디를 입력하세요.')) return false;
    if(!validate(frm.member_pwd, '비밀번호를 입력하세요.')) return false;
    
    return true;
}

/*------------------------------------------------------------------------------------------------*/

//사이트 메타정보 등록
function reg_sitemeta(frm)
{
    if(!validate(frm.elements['Title'], '사이트 제목을 입력하세요.')) return false;
    /*
    if(!validate(frm.elements['Subject'], '사이트 주제를 입력하세요.')) return false;
    if(!validate(frm.elements['Description'], '사이트 설명을 입력하세요.')) return false;
    if(!validate(frm.elements['Keywords'], '사이트 키워드를 입력하세요.')) return false;
    if(!validate(frm.elements['Copyright'], '사이트 저작권을 입력하세요.')) return false;
    */
    
    return true;
}

/*------------------------------------------------------------------------------------------------*/

//사이트 맵 등록/수정
function reg_sitemap(frm)
{
    var exec_type = frm.exec_type.value;
    
    if(!validate(frm.elements['menu_code'], '메뉴 코드를 입력하세요.')) return false;
    if(!validate(frm.elements['menu_code_name'], '메뉴명을 입력하세요.')) return false;
    if(!validate(frm.elements['menu_type'], '메뉴 구분을 선택하세요.')) return false;
    if(!validate(frm.elements['menu_code_grp'], '메뉴 위치를 선택하세요.')) return false;
    if(!validate(frm.elements['menu_code_level'], '메뉴 레벨을 선택하세요.')) return false;
    
    if(exec_type == 'INSERT')
    {
        if(!validate(frm.elements['chk_menu_code'], '메뉴 코드 중복체크를 하세요.')) return false;
    }
    
    return true;
}

//사이트 맵 삭제
function delete_sitemap(frm)
{
    if(!confirm('삭제하시겠습니까?'))
    {
        return false;
    }
    
    frm.action = '/?c=iwc&m=exec_delete_site_map';
    frm.submit();
}

//사이트 맵 상세.. 
function select_sitemap(frm, seq_no)
{
    frm.exec_type.value = 'UPDATE';
    frm.seq_no.value    = seq_no;
    frm.action = '/?c=iwc&m=view_site_map';
    frm.submit();
}

//사이트 맵(메뉴코드) 중복체크 (AJAX)
function dup_chk_menu_code() 
{
    if(!validate(document.getElementById('menu_code'), '메뉴코드를 입력하세요.')) return;
    
    var menu_code  = document.getElementById('menu_code').value;
     
    var url = '/?c=iwc&m=exec_dup_chk_menu_code'; 
    var sn  = 'menu_code='+ menu_code; 

    Ajax_Call(url, "POST", "true", dup_chk_menu_code_result, sn, false); 
}

//사이트 맵(메뉴코드) 중복체크 결과 (AJAX)
function dup_chk_menu_code_result() 
{
    var result = AJAX.XmlHttp.responseText;  // 결과값 
    
    if(Trim(result) == "FAIL")
    {
        alert('메뉴코드가 중복 되었습니다.');
        document.getElementById('menu_code').value = '';
        document.getElementById('chk_menu_code').value = '';
        return;
    }
    else if(Trim(result) == " SUCCESS");
    {
        alert('사용 가능한 메뉴코드입니다.');
        document.getElementById('menu_code').readOnly = true;
        document.getElementById('chk_menu_code').value = 'ok';
        return;
    }
}  

/*------------------------------------------------------------------------------------------------*/

//사이트 관리 정보 등록
function reg_siteadmin(frm)
{
    //if(!validate(frm.elements['member_id'], '관리자 아이디를 입력하세요.')) return false;
    //if(!validate(frm.elements['member_pwd'], '관리자 비밀번호를 입력하세요.')) return false;
    
    return true;
}

//사이트 관리자 계정 생성 (AJAX) 
function create_admin() 
{
    if(!validate(document.getElementById('member_id'), '관리자 아이디를 입력하세요.')) return;
    if(!validate(document.getElementById('member_pwd'), '관리자 비밀번호를 입력하세요.')) return;
    
    var member_id  = document.getElementById('member_id').value;
    var member_pwd = document.getElementById('member_pwd').value;
    
    var url = '/?c=iwc&m=exec_create_admin_account'; 
    var sn  = 'member_id='+ member_id + '&member_pwd=' + member_pwd; 

    Ajax_Call(url, "POST", "true", create_admin_result, sn, false); 
} 

//사이트 관리자 계정 생성 결과 (AJAX) 
function create_admin_result() 
{
    var result = AJAX.XmlHttp.responseText;  // 결과값 

    if(result != 'FAIL')
    {
        alert('관리자 계정이 생성 되었습니다.');
        return;
    }
    else
    {
        alert('관리자 계정 생성이 실패되었습니다.');
        return;
    }
}

//인츠웍스 관리자 계정 생성 (AJAX) 
function iw_create_admin() 
{
    if(!validate(document.getElementById('iw_member_id'), 'ROOT 관리자 아이디를 입력하세요.')) return;
    if(!validate(document.getElementById('iw_member_pwd'), 'ROOT 관리자 비밀번호를 입력하세요.')) return;
    
    var iw_member_id  = document.getElementById('iw_member_id').value;
    var iw_member_pwd = document.getElementById('iw_member_pwd').value;
    
    var url = '/?c=iwc&m=exec_iw_create_admin_account'; 
    var sn  = 'iw_member_id='+ iw_member_id + '&iw_member_pwd=' + iw_member_pwd;
    
    Ajax_Call(url, "POST", "true", iw_create_admin_result, sn, false); 
} 

//인츠웍스 관리자 계정 생성 결과 (AJAX) 
function iw_create_admin_result() 
{
    var result = AJAX.XmlHttp.responseText;  // 결과값

    if(result != 'FAIL')
    {
        alert('ROOT 관리자 계정이 생성 되었습니다.');
        return;
    }
    else
    {
        alert('ROOT 관리자 계정 생성이 실패되었습니다.');
        return;
    }
} 

/*------------------------------------------------------------------------------------------------*/

//회원등급 설정
function reg_member_grade(frm)
{
    var exec_type = frm.exec_type.value;
    
    if(!validate(frm.elements['grade_no'], '회원등급 선택하세요.')) return false;
    if(!validate(frm.elements['grade_name'], '회원등급명을 입력하세요.')) return false;
    
    return true;
}

//회원등급 삭제
function delete_member_grade(frm, base_url)
{
    if(!confirm('삭제하시겠습니까?'))
    {
        return false;
    }
    
    frm.action = base_url + '&m=exec_delete_member_grade';
    frm.submit();
}

//회원등급 상세.. 
function select_member_grade(frm, seq_no, base_url)
{
    frm.exec_type.value = 'UPDATE';
    frm.seq_no.value    = seq_no;
    frm.action = base_url + '&m=view_member_grade';
    frm.submit();
}
/*------------------------------------------------------------------------------------------------*/










