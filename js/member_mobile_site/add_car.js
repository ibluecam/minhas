
$("#model_list").chained("#make_list");

 $(document).ready(function(e){


 	tiny_mce_load('contents', '300px');

  $("#make_list").change(function(e){
        var car_make = $(this).val();
        $("#car_make").val(car_make.toUpperCase());

        var car_make_class = $("#make_list").find('option:selected').attr("class");

        if(car_make_class == 'add_new_maker'){

              $("#car_make").val('');
              $("#car_make").attr("required", "true");
              
              $("#make_list").removeAttr("required");

              $("#car_make").attr("style", "display: inline");
            
        }else{
            $("#car_make").attr("style", "display: none");
            $("#car_model").attr("style", "display: none");
        }

  });

  $("#model_list").change(function(e){
        var car_make = $(this).val();

        $("#car_model").val(car_make.toUpperCase());

        var car_model_class = $("#model_list").find('option:selected').attr("class");

        if(car_model_class == 'add_new_model'){

              $("#car_model").val('');
              $("#car_model").attr("required", "true");

              $("#model_list").removeAttr("required");

              $("#car_model").attr("style", "display: inline");
            
        }else{
            $("#car_model").attr("style", "display: none");
        }
        

  });


 /*	var saved_car_make = $("#saved_car_make").val();
 	var saved_car_model = $("#car_model").val();
    if(saved_car_model!=''){
        $("#model_list").val(saved_car_model);
        if($("#model_list").val()=='add_new_model'){
            $("#car_model").css("display", "inline");
            $("#car_model").val(saved_car_model);
        }
    }

    $("#car_make").change(function(e){
        var car_make = $(this).val();
        $(this).val(car_make.toUpperCase());

    });

    $("#car_model").change(function(e){
        var car_model = $(this).val();
        if($(this).val()=='add_new_model'){
            $(this).val(''); 
        }
        $(this).val(car_model.toUpperCase());
    });

 	$("#make_list").val(saved_car_make.toUpperCase());
    if(saved_car_make!=''){
        $("#car_make").css("display", "none");
    }
    $("#car_model").css("display", "none");
    $("#car_make").css("display", "none");
 	// Add new maker
	$("#make_list").change(function(e){
	    $("#car_make").val($(this).val());
	    if($(this).val()=='add_new_maker'){

	        $("#car_make").val('');
	        $("#car_make").attr("required", "true");
	        $("#car_make").css("display", "inline");

	    }else{
	    	$("#car_model").attr("required", "false");
	        $("#car_make").css("display", "none");	               
	    }
	     reloadModelList($(this).val());
	});
	$("#model_list").change(function(e){
        $("#car_model").val($(this).val());
        if($(this).val()=='add_new_model'){
            $("#car_model").val('');
            $("#car_model").attr("required", "true");
            $("#car_model").css("display", "inline");
        }else{
        	$("#car_model").attr("required", "false");
            $("#car_model").css("display", "none");
        }
    });
	// function filter model
	function reloadModelList(car_make){
        $.ajax({
            method: "GET",
            url: "/?c=admin&m=adm_get_model_list&car_make="+car_make
            })
            .done(function( data ) {
                $("#model_list").html(data);
                if ( $("#model_list").val()=="add_new_model"){
                    $("#car_model").val('');
                    $("#car_model").css("display", "inline");
                 }else{
                     $("#car_model").css("display", "none");
                }
                
        });
        
    }*/
 });
var hostURL = window.location.protocol + "//" + window.location.host;
if(hostURL=="http://motorbb.dev"){
   document.getElementById("upload_widget_opener").addEventListener("click", function() {

    cloudinary.openUploadWidget({ cloud_name: 'dy79r0ulf', upload_preset: 'xvmizqan', max_image_width: 1024, max_image_height: 768}, 
      function(error, result) { 
        for (i = 0; i < result.length; i++) {
            var public_id         = result[i].public_id; 
            var resource_type     = result[i].resource_type;
            var secure_url        = result[i].secure_url;
            var original_filename = result[i].original_filename;
            var idx = document.getElementById("idx").value;
        
            var data = {
              idx               : idx,
              public_id         : public_id,
              resource_type     : resource_type,
              secure_url        : secure_url,
              original_filename : original_filename,
            }
            $.ajax({
              type: "POST",
              url: "/?c=admin&m=adm_bbs_cloud_upload_exec",
              data: data,
              async: true,
              success: function(data){
                document.getElementById("upload_iframe").contentWindow.append_image(data);    
               }
            });

        }
        
      });
  }, false);
}else{
   document.getElementById("upload_widget_opener").addEventListener("click", function() {

    cloudinary.openUploadWidget({ cloud_name: 'softbloom', upload_preset: 'juefxlp7', show_powered_by: false, max_image_width: 1024, max_image_height: 768}, 
      function(error, result) { 
        for (i = 0; i < result.length; i++) {
            var public_id         = result[i].public_id; 
            var resource_type     = result[i].resource_type;
            var secure_url        = result[i].secure_url;
            var original_filename = result[i].original_filename;
            var idx = document.getElementById("idx").value;
        
            var data = {
              idx               : idx,
              public_id         : public_id,
              resource_type     : resource_type,
              secure_url        : secure_url,
              original_filename : original_filename,
            }
            $.ajax({
              type: "POST",
              url: "/?c=admin&m=adm_bbs_cloud_upload_exec",
              data: data,
              async: true,
              success: function(data){
                document.getElementById("upload_iframe").contentWindow.append_image(data);    
               }
            });

        }
        
      });
  }, false);	
}

window.onload = function () {
    
    setInterval(function() {
        ResizeIframeFromParent('upload_iframe');
    }, 1000);
}
function ResizeIframeFromParent(id) {
    if (jQuery('#'+id).length > 0) {
        var window = document.getElementById(id).contentWindow;
        var prevheight = jQuery('#'+id).attr('height');
        var newheight = Math.max( window.document.body.scrollHeight, window.document.body.offsetHeight, window.document.documentElement.clientHeight, window.document.documentElement.scrollHeight, window.document.documentElement.offsetHeight );
        if (newheight != prevheight && newheight > 0) {
            jQuery('#'+id).attr('height', newheight);
            console.log("Adjusting iframe height for "+id+": " +prevheight+"px => "+newheight+"px");
        }
    }
}
function cancel_modify(){
    window.location = "/?c=admin&m=list_car";
}




// check Chissno


        $('#car_chassis_no').change(function () {


             var Chassis  = $(this).val();

           
            if (!Chassis.match(/^[a-z\d_]{4,20}$/i)) {
                $('.chassis_error').html('');
                return false;
            }
          
            
            if (Chassis.length >= 4 && Chassis.length <= 20)
            {
                $.ajax({
                    url: '/?c=admin&m=check_chassis',
                    type: "POST",
                    async: true,
                    data: {
                        'chass_no': Chassis
                    },
                    success: function (result)
                    {
                      
                        if (result == 1 )
                        {  
                            $('.chassis_error').html('This Chassis No already exits');
                            $( "#car_chassis_no" ).focus();
                             var ss =document.getElementById('chassis_error').innerHTML;
                             $('input[type="submit"]').attr('disabled', 'disabled');  
                            return false;
                            
                        }
                        
                        if (result == 0)
                        {
                            $('.chassis_error').html('');
                            $('input[type="submit"]').removeAttr('disabled');
                           
                            
                        }

                    }
                    
                });
                
                return false;
            }
           

        });
        

      

