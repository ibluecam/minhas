

$(document).ready(function(){


	$(".click_slide_up").on('click',function(){

		$( this ).toggleClass("click_slide_up click_slide_down");

		var get_class = $( this ).attr('class');
		
		if(get_class == 'click_slide_down'){
			$('.content_hide_show').slideDown(300);
		}else{
			$('.content_hide_show').slideUp(300);
		}

	});	


	$(".icon_slide_down_searchbox").on('click', function(){
		var src = $(this).attr('src');
	    var newsrc = (src=='images/public_mobile_site/down_search_box.png') ? 'images/public_mobile_site/up_search_box.png' : 'images/public_mobile_site/down_search_box.png';
	    $(this).attr('src', newsrc );

	    if(src == 'images/public_mobile_site/down_search_box.png'){
	    	$('.content_search_box').slideDown(300);
	    }else{
	    	$('.content_search_box').slideUp(300);
	    }
	    
	});



});