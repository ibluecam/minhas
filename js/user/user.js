//document.onmousedown = function(){
  //document.oncontextmenu=new Function("return false");
//}
//로그인 써머리 체크
function member_summary_login_check(s_login_frm)
{
    if(!validate(s_login_frm.member_id, '아이디를 입력하세요.'))   return false;
    if(!validate(s_login_frm.member_pwd, '비밀번호를 입력하세요.')) return false;
    
    return true;
}

//회원가입시 아이디 체크
function member_id_check(obj, chId, chChar)
{
    chId = chId.toUpperCase();
    var str = chId.split(",");
    var chr = chChar.split(",");
    var vals = obj.value.toUpperCase();
    
    var cStr = new Array('a', 'b','c', 'd','e', 'f','g', 'h','i', 'j','k', 'l','m', 'n','o', 'p','q', 'r','s', 't','u', 'v','w', 'x','y', 'z','0', '1','2', '3','4', '5','6', '7','8', '9');
    
    for(var i=0; i<str.length; i++)
    {
        if(vals.indexOf(str[i]) > -1)
        {
            alert("[" + str[i] + "] 아이디로 사용할 수 없습니다.");
            event.returnValue = false;
            obj.focus();
            return false;
        }
    }

    for(var i=0; i<vals.length; i++)
    {
        var isUse = false;
        
        for(var j=0; j<cStr.length; j++)
        {
            if(vals.charAt(i).toLowerCase() == cStr[j])
            {
                isUse = true;
                break;
            }  
        }
        
        if(!isUse)
        {
            //alert("아이디는 영문자[a~z][A~Z], 숫자[0~9]만을 사용하여 등록할 수 있습니다.");
            alert("아이디는 영문자[a~z][A~Z], 숫자[0~9]만을 사용하여 등록할 수 있습니다.");
            event.returnValue = false;
            obj.focus();
            return false;
            break;
        }
    }

    for(var i=0; i<vals.length; i++)
    {
        for(var j=0; j<chr.length; j++)
        {
            if(vals.charAt(i) == chr[j])
            {
                alert("아이디로 특수문자를 사용할 수 없습니다.");
                event.returnValue = false;
                obj.focus();
                return false;
            }  
        }
    }

    if(Trim(vals).length > 0)
    {    
        var space= " ";
        if(vals.indexOf(space) != -1)
        {
            alert("아이디에 공백이 포함되어 있습니다.");
            event.returnValue = false;
            obj.focus();
            return false;
        }
        
        if(vals.length < 5)
        {
            alert("아이디는 다섯자리 이상으로 등록해야 합니다.");
            event.returnValue = false;
            obj.focus();
            return false;
        }
        
        if(vals.length > 12)
        {
            alert("아이디는 열두자리 이하로 등록해야 합니다.");
            event.returnValue = false;
            obj.focus();
            return false;
        }        
        
        var tmpStr = "abcdefghijklmnopqrstuvwxyz";            
        if (tmpStr.indexOf(vals.charAt(0).toLowerCase()) == -1) {
            alert("아이디는 영문자로 시작해야 합니다.");
            event.returnValue = false;
            obj.focus();
            return false;
        }
    }
    
    return true;
}

//회원가입시 비밀번호 체크
function member_pwd_check(obj)
{
    var vals = obj.value.toUpperCase();
    
    var cStr = new Array('a', 'b','c', 'd','e', 'f','g', 'h','i', 'j','k', 'l','m', 'n','o', 'p','q', 'r','s', 't','u', 'v','w', 'x','y', 'z','0', '1','2', '3','4', '5','6', '7','8', '9');

    for(var i=0; i<vals.length; i++)
    {
        var isUse = false;
        
        for(var j=0; j<cStr.length; j++)
        {
            if(vals.charAt(i).toLowerCase() == cStr[j])
            {
                isUse = true;
                break;
            }  
        }
        
        if(!isUse)
        {
            //alert("비밀번호는 영문자[a~z][A~Z], 숫자[0~9]만을 사용하여 등록할 수 있습니다.");
            alert("You can use alphabets (a to z)(A to Z) and numbers (0 to 9) for setting password.");
            event.returnValue = false;
            obj.focus();
            return false;
            break;
        }
    }

    if(Trim(vals).length > 0)
    {    
        var space= " ";
        if(vals.indexOf(space) != -1)
        {
            alert("Your password include space.");
            event.returnValue = false;
            obj.focus();
            return false;
        }
        
        if(vals.length < 5)
        {
            alert("Please input your password with more than five letters.");
            event.returnValue = false;
            obj.focus();
            return false;
        }
        
        if(vals.length > 12)
        {
            alert("Please input your password with less than twelve letters.");
            event.returnValue = false;
            obj.focus();
            return false;
        }   
    }
    
    return true;
}


//아이디 중복체크 (AJAX)
function dup_chk_member_id(frm, mcd) 
{
    //if(!validate(document.getElementById('member_id'), '아이디를 입력하세요.')) return;
    
    if(!validate(frm.elements['values[member_id]'], '아이디를 입력하세요.')) return;
    
    var check_id   = 'admin,administrator,member,manager';
    var check_char = '!,@,#,$,%,^,&,&,*,(,),_,-,+,=,{,[,},],|,\,:,;,",<,>,.,?,/,~,`';

    if(member_id_check(frm.elements['values[member_id]'], check_id, check_char))
    {
        var member_id  = frm.elements['values[member_id]'].value;
        
        //location.href = '/?c=user&mcd='+ mcd +'&me=dup_chk_member_id&member_id='+ member_id;
        
        var url = '/?c=common&m=dup_chk_member_id'; 
        var sn  = 'member_id='+ member_id; 
        
        Ajax_Call(url, "POST", "true", dup_chk_member_id_result, sn, false);         
    }
}


//아이디 중복체크 결과 (AJAX)
function dup_chk_member_id_result() 
{
    var result = AJAX.XmlHttp.responseText;  // 결과값 
    
    if(result == 'FAIL')
    {
        alert('아이디가 중복 되었습니다.');
        join_frm.elements['values[member_id]'].value = '';
        join_frm.elements['chk_member_id'].value     = '';
        return;
    }
    else
    {
        alert('사용 가능한 아이디 입니다.1');
        join_frm.elements['values[member_id]'].readOnly = true;
        join_frm.elements['chk_member_id'].value        = 'ok';
        return;
    }
}

//아이디 중복체크2 (AJAX)
function dup_chk_member_id2(frm, mcd) 
{
    if(!validate(frm.elements['values[member_id]'], 'Please input your email(ID).')) return;
    
    var member_id  = frm.elements['values[member_id]'].value;
    var url = '/?c=common&m=dup_chk_member_id2';
    var sn  = 'member_id='+ member_id;

    Ajax_Call(url, "POST", "true", dup_chk_member_id_result2, sn, false);
}

//아이디 중복체크 결과 (AJAX)
function dup_chk_member_id_result2()
{
    var result = AJAX.XmlHttp.responseText;  // 결과값 
    
    if(result == 'FAIL')
    {
        alert('Your email(ID) is duplicated.');
        join_frm.elements['values[member_id]'].value = '';
        join_frm.elements['chk_member_id'].value     = '';
        return;
    }
    else if(result == 'FAIL2')
    {
        alert('Please write standard email form.');
        join_frm.elements['values[member_id]'].value = '';
        join_frm.elements['chk_member_id'].value     = '';
        return;
    }
    else
    {
        alert('Your email(ID)is usable.');
        join_frm.elements['values[member_id]'].readOnly = true;
        join_frm.elements['chk_member_id'].value        = 'ok';
        return;
    }
}

// 승인 메일 보내기 함수
function send_verification_mail(frm)
{
    if(frm.elements['chk_member_id'].value != 'ok') {
        alert("please check your email.");
        return;
    }

    var member_id  = frm.elements['values[member_id]'].value;
    var url = '/?c=common&m=send_verification_mail';
    var sn  = 'member_id='+ member_id;

    Ajax_Call(url, "POST", "true", send_verification_mail_result, sn, false);
}

var result = '';

function send_verification_mail_result()
{
    result = AJAX.XmlHttp.responseText;  // 결과값 

    if(result.trim() != '') {
        alert('mail for verification is sended.');
    } else {
        alert('error');
    }
}

function check_verification_mail()
{
    if(join_frm.elements['email_verification_code'].value)
    {
        alert('email verification is complete.');
        join_frm.elements['email_verification_code'].readOnly = true;
        join_frm.elements['chk_email_verification_code'].value = 'ok';
        return;
    }
    else
    {
        alert('verification code are not matched.');
        join_frm.elements['email_verification_code'].value = '';
        join_frm.elements['chk_email_verification_code'].value = '';
        return;
    }
}

//동영상 관련(iframe)
function go_movie(arg)
{
    document.getElementById('movie_iframe').src = '/views/common/video.php?' + arg;
}

//동영상 셋팅
function init_mv()
{    
    document.getElementById('movie_iframe').src = '/views/common/video.php';
}

