

$(document).ready(function(e){

$('.btn_quick_discount').click(function(e) { 
  var idx = $(this).attr("data-idx");
  var car_fob_cost = $('#car_fob_cost_'+idx).val();
  var old_fob_cost = $('#old_fob_cost_'+idx).val();
  $(".msg").html("");
  if(parseInt(old_fob_cost)<parseInt(car_fob_cost)){
    $(".msg").html("FOB must be smaller than Previous FOB!");
    $(".msg").attr("style","color: red");
  }
  if($('#discount_'+idx+':checked').val()=='discount'){
    $("#block_empty_"+idx).css("display","none");
    $(".block_previous_fob_"+idx).css("display","");
    $("#label_previous_fob_"+idx).css("display","block");
    $("#input_previous_fob_"+idx).css("display","block");
  }else{
    $("#block_empty_"+idx).css("display","");
    $(".block_previous_fob_"+idx).css("display","none");
    $("#label_previous_fob_"+idx).css("display","none");
    $("#input_previous_fob_"+idx).css("display","none");
  }
  $.colorbox({
    width:"380px", 
    height:"265px", 
    inline:true, 
    fixed:true,
    href:"#block_car_price_"+idx
  });
});

  $("input[name=normal_discount]").on( "change", function() {
      var idx = $(this).attr("data-idx");
      var car_fob_cost = $('#car_fob_cost_'+idx).val();
      var old_fob_cost = $('#old_fob_cost_'+idx).val();
      var idx = $(this).attr("data-idx");
      if($(this).val()=="discount"){
        $("#block_empty_"+idx).css("display","none");
        $(".block_previous_fob_"+idx).css("display","");
        $("#label_previous_fob_"+idx).css("display","block");
        $("#input_previous_fob_"+idx).css("display","block");

        if(parseInt(old_fob_cost)<parseInt(car_fob_cost) || old_fob_cost==""){
          $(".btn_save_discount_"+idx).attr('disabled','disabled');
          $(".btn_save_discount_"+idx).attr('style','background:#909090;cursor: unset;');
        }

        if(parseInt(old_fob_cost)<parseInt(car_fob_cost)){
          $(".msg").html("FOB must be smaller than Previous FOB!");
          $(".msg").attr("style","color: red");
        }
        
      }else{
        $("#block_empty_"+idx).css("display","");
        $(".block_previous_fob_"+idx).css("display","none");
        $("#label_previous_fob_"+idx).css("display","none");
        $("#input_previous_fob_"+idx).css("display","none");
        $(".btn_save_discount_"+idx).removeAttr('disabled');
        $(".btn_save_discount_"+idx).attr('style','background:#258dc8;cursor: pointer;');
        $(".msg").html("");
      }
  });

  //called when key is pressed in textbox
  $("input[name=car_fob_cost],input[name=old_fob_cost]").keypress(function (e) {
      //if the letter is not digit then display error and don't type anything
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message //
          return false;
      }
  });


  // $('.btn_quick_discount').click(function(e) {   
       // var url = $(this).data('url');
       // var idx = $(this).attr("data-idx");
       // $.colorbox({
            // width:"380", 
            // height:"265", 
            // iframe:true, 
            // href:url,
            // fixed:true
            // onClosed:function(){ 
                //reloadFOB(idx);
                // location.reload();
            // }
        // });
    // });

  $("input[name=car_fob_cost],input[name=old_fob_cost]").on( "keyup", function() {
      var idx = $(this).attr("data-idx");
      var car_fob_cost = $('#car_fob_cost_'+idx).val();
      var old_fob_cost = $('#old_fob_cost_'+idx).val();

      if($("#discount_"+idx+":checked").val()){
        if (parseInt(old_fob_cost)<parseInt(car_fob_cost) || old_fob_cost=="") {

            $(".msg").html("FOB must be smaller than Previous FOB!");
            $(".msg").attr("style","color: red");
            $(".btn_save_discount_"+idx).attr('disabled','disabled');
            $(".btn_save_discount_"+idx).attr('style','background:#909090;cursor: unset;');
        } 
        else{
            $(".msg").html("");
            $(".btn_save_discount_"+idx).removeAttr('disabled');
            $(".btn_save_discount_"+idx).attr('style','background:#258dc8;cursor: pointer;');
        }
      }
  });


  $('#quick_action').attr('disabled','disabled');

  $("#input_select_model").chained("#input_select_make");
  //$("#carmodel").chained("#carmake");
  var searched_model = $("#searched_model").val();
  $("#input_select_model").val(searched_model);

  $(".btn_quick_sale").click(function(e){
    var idx = $(this).attr("data-idx");
    var chassis_no = $(this).attr("data-chassisNo");
    if(confirm("Are you sure want to set this car as Sale?")){
      quick_submit_status(idx, chassis_no, 'sale');
    }
    return false;
  });

  $(".btn_quick_reserve").click(function(e){
    var idx = $(this).attr("data-idx");
    var chassis_no = $(this).attr("data-chassisNo");
    if(confirm("Are you sure want to set this car as Reserved?")){
      quick_submit_status(idx, chassis_no, 'reserved');
    }
    return false;
  });

  $(".btn_quick_soldout").click(function(e){
    var idx = $(this).attr("data-idx");
    var chassis_no = $(this).attr("data-chassisNo");
    if(confirm("Are you sure want to set this car as Soldout?")){
      quick_submit_status(idx, chassis_no, 'soldout');
    }
    return false;
  });

  function quick_submit_status(idx, chassis_no, status){
    $("#quick_input_chassis_no").val(chassis_no);
    $("#quick_input_status").val(status);
    $("#quick_input_idx").val(idx);
    $("#form_quick_status").submit();
  }

  $(".btn_quick_not_publish").click(function(e){
    var idx = $(this).attr("data-idx");
    var value = $(this).attr("data-value");
    if(confirm("Are you sure want to set this car as not publish?")){
      $("#quick_idx_publish").val(idx);
      $("#quick_publish_value").val(value);
      $("#form_quick_set_status").submit();
    }
    return false;
  });

  $(".btn_quick_publish").click(function(e){
    var idx = $(this).attr("data-idx");
    var value = $(this).attr("data-value");
    if(confirm("Are you sure want to set this car as publish?")){
      $("#quick_idx_publish").val(idx);
      $("#quick_publish_value").val(value);
       $("#form_quick_set_status").submit();
    }
    return false;
  });

  $(".btn_apply").attr('disabled','disabled')

  $('#checked_all_list').click(function(e){
      $(".check_seller_list").prop('checked', this.checked);
      var check_all = $(".check_seller_list").prop('checked');

      if(check_all == true){
        $(".btn_apply").removeAttr('disabled');
        $('#quick_action').removeAttr('disabled');
      }else{
        $(".btn_apply").attr('disabled','disabled');
        $('#quick_action').attr('disabled','disabled');
      }
  });

  $(".btn_apply").click(function(e){
    var idx = $(".check_seller_list").val();
    var status = $("#quick_action").val();
    if($('#quick_action option:selected').val()!=''){
     $("#form_quick_set_status").submit();
    }else{
      alert("Please select the action that you need to update?");
    }
    return false;
  });

  $('.check_seller_list').click(function(e){

      if(($('.check_seller_list:checked').length) > 0){

        $(".btn_apply").removeAttr('disabled');
        $('#quick_action').removeAttr('disabled');

      }else{

        $(".btn_apply").attr('disabled','disabled');
        $('#quick_action').attr('disabled','disabled');

        if($('#checked_all_list').prop('checked') == true) {

            $('#checked_all_list').removeAttr('checked');

        }

      }
  });
  $("#btn_reset").click(function(e){
    window.location="/?c=admin";
  });
  $( "#quick_action" ).on( "change", function(e) {
    if($('#quick_action option:selected').val()=='delete'){
      $("#form_quick_set_status").attr("action", "/?c=admin&m=adm_bbs_delete_attach_click_seller");
    }else{
      $("#form_quick_set_status").attr("action", "/?c=admin&m=quick_set_status_by_select_action");
    }
  });


  $(".btn_quick_delete").click(function(e){
    var idx = $(this).attr("data-idx");
    if(confirm("Are you sure want to delete this car?")){
      $(this).attr("href", "/?c=admin&m=adm_bbs_delete_attach_check_seller&idx="+idx);
      return true;
    }
    return false;
  });

  $(".msg_icon_close").click(function(e){
    $("#flash_block").css("display","none");
  });

});


function reloadFOB(idx){
    $.ajax({
        method: "POST",
        url: "/?c=admin&m=adm_get_fob_discount",
        data: {
          idx: idx
        }
    })
    .done(function( data ) {
        $("#car_fob_"+idx).html('$'+$.trim(data));
    });
        
}

// function Discount_reload(){
//    window.location.reload();
// }

/* $(document).on("click","select[name*='carmake']",function() {
	   

   $("#carmodel").empty();
   $("#carmodel").append("<option value>Model</option>");


      var car_make=$(this).val();
var base_url = "<?php echo base_url(); ?>";
      var current_url=$('#base_url').val();
      $.getJSON('/?c=car_model&m=get_car_model&car_make='+car_make,function(json){
          $("select[name*='carmodel']").html("");
          $( "select[name*='carmodel']").append('<option value="">Model</option>');
        $.each(json,function(i,data_json){

          respone_data="<option title='" + data_json.car_model + "' value='" + data_json.car_model + "'>" + data_json.car_model + "</option>";
          $( "select[name*='carmodel']").append(respone_data);

        });

      });


    });
*/

 // select All code

//  $(".check_all").click(function(e){
//         var count_selected = $(".checkcar").prop('checked', this.checked);
//         if(count_selected.length > 0){
//           $(".delete_all a").css('background','');
//           $(".delete_all a").css('border-color','');
//           $(".delete_all a").css('color','#ADB2B5');
//         }else{
//           $(".delete_all a").css('background', '#ee1c24');

//         }
// });


 // count select

  // $(document).on("click","input[type*='checkbox']",function() {
  //   // $('input[type="checkbox"]').click(function(){

  //       if(($('.checkcar:checked').length) > 0){
  //         $(".delete_all a").css('background','red');
  //         $(".delete_all a").css('border-color','');
  //         $(".delete_all a").css('color','white');
  //         $("#lable_selected").html('<span id="count_selected"></span> Car(s) Selected')
  //         $("#count_selected").html($('.checkcar:checked').length);
  //       }
  //       else{
  //         $("#lable_selected").html('');
  //         $(".delete_all a").css('background', '#959595');
  //         $(".delete_all a").css('border-color', '');
  //         $(".delete_all a").css('color', 'white');
  //       }

  //   });

  // delete items

  // $("#delete_btn").click(function(event) {
  //     if($(".checkcar:checked").length<=0){
  //       alert("Please select  cars that you want to delete.");
  //     }else{
  //       if(confirm("Are you sure want to delete selected cars?")){
  //         DeleteFormSeller();
  //       }
  //     }
      
  //   });

  // function DeleteFormSeller(){
  //   $('#car_list').attr('action', "/?c=admin&m=adm_bbs_delete_attach_cloud_check_seller").submit();
  // }

  
    // pagin
// update delete


    //     $("#btnDelete").click(btnDelete_click);

    //      $('#idxs').click(function(event) {
    //   if(this.checked) {
    //       $(':checkbox').each(function() {
    //           this.checked = true;
    //           $('#btnDelete').removeClass('disabled');
    //           $('#btnDelete').removeClass('not_check');
    //           $('#btnDelete').addClass('is_check');
    //       });
    //   }
    //   else {
    //     $(':checkbox').each(function() {
    //           this.checked = false;
    //           $('#btnDelete').addClass('disabled');
    //           $('#btnDelete').addClass('not_check');
    //           $('#btnDelete').removeClass('is_check');
    //       });
    //   }
    // });

        // $('.checkcar').click(function(event) {   
        // if(this.checked) {
        //     this.checked = true;
        //     $('#btnDelete').removeClass('disabled');
        //     $('#btnDelete').removeClass('not_check');
        //     $('#btnDelete').addClass('is_check');
        // }
        // else{
        //    $('#btnDelete').addClass('disabled');
        //    $('#btnDelete').addClass('not_check');
        //    $('#btnDelete').removeClass('is_check');
        // }
        // });
        
        // $("#btn_yes").click(function (){
        //     $("#blanket").css("display", "block");
        //     $(".display_yes").css("background", "none");
        //     var abc ='';
        //     $(".checkcar").each(function (){
        //         if ($(this).is(":checked") ===true)
                  

        //             abc += $(this).attr("data-title") + ";";
        //     });
        //     var ajax = $.ajax({
        //         url: "/?c=admin&m=adm_bbs_delete_attach_cloud_check_seller",
        //         type: 'POST',
        //         data : {
                    
        //             id : abc    
        //         }
        //     });
        //     ajax.done(function (msg){
        //         $("#blanket").css("display", "none");
        //         $("#myModal").modal("hide");
        //         window.location.href = "/?c=admin&m=list_car";
        //     });
        // });


        //   function btnDelete_click(){
        //     if($(this).hasClass("disabled") === false){
        //         if($("#del").html().indexOf("Delete") !== -1){
        //             $("#msgBody").html("Are you sure want to delete selected cars?");
        //             $("#myModalLabel").html("Confirmation");
        //             $('#btnNO').html('NO');
        //             $("#btn_yes").show();
        //             $("#myModal").modal("show");
        //         }
              
        //         else{
        //              window.location.href = "/?c=admin&m=list_car";
        //         }
        //     }
        // }

    

// delete one

//  $('.info_delete').each(function(){
//         $(this).click(function(){
//         $('.check_all').prop('checked', false);
//         $('.checkcar').each(function(){
//         $(this).prop('checked', false);
//       });
//     var a = $(this).parent().parent().parent();
//      a.find('input[type=checkbox]').prop('checked', true);
                                            
//    });
// });

 // $("#btn_yes_one").click(function (){
 //            $("#blanket").css("display", "block");
 //            $(".display_yes").css("background", "none");
 //            var abc ='';
 //            $(".checkcar").each(function (){
 //                if ($(this).is(":checked") ===true)
                
 //                     abc += $(this).attr("data-title") + ";";
 //            });
 //            var ajax = $.ajax({
 //                url: "/?c=admin&m=adm_bbs_delete_attach_cloud_click_seller",
 //                type: 'POST',
 //                data : {
                    
 //                    idxs : abc    
 //                }
 //            });
 //            ajax.done(function (msg){
 //                $("#blanket").css("display", "none");
 //                $("#myModal").modal("hide");
 //                window.location.href = "/?c=admin&m=list_car";
 //            });
 //        });


