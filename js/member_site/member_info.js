 /*$(".buyer-mobile-number, .seller-mobile-number").intlTelInput({
        allowExtensions: true,
        autoFormat: true,
        autoHideDialCode: false,
        autoPlaceholder: false,
        defaultCountry: "auto",
        ipinfoToken: "yolo",
        nationalMode: false,
        numberType: "MOBILE",
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        preferredCountries: ['kr', 'jp'],
        utilsScript: "/lib/libphonenumber/build/utils.js"
      });
      */
      // $(document).ready(function(e){
      //       alert();
      // });

      $("#phone, #mobile").intlTelInput({

        allowExtensions: true,
        //autoFormat: false,
        autoHideDialCode: false,
        autoPlaceholder: false,
        
        //defaultCountry: "auto",
         eoIpLookup: function(callback) {
           $.get('http://ipinfo.io', function() {}, "jsonp").always(function(resp) {
             var countryCode = (resp && resp.country) ? resp.country : "";
             callback(countryCode);
           });
         },
        nationalMode: false,
        numberType: "MOBILE",

        utilsScript: "/intlTelInput/lib/libphonenumber/build/utils.js"
      });


      $(document).ready(function () {
          
          $('#phone').focus(function(){
            //Check val for email
            if($(this).val() == ''){
              $(this).val('+');
            }
            
          }).blur(function(){
            //check for empty input
            if($(this).val() == ''){
              $(this).val('+');
            }
          });

          $('#mobile').focus(function(){
            //Check val for email
            if($(this).val() == ''){
              $(this).val('+');
            }
            
          }).blur(function(){
            //check for empty input
            if($(this).val() == ''){
              $(this).val('+');
            }
          });
        
      });

      function getSelectedBuyerCc(){
        var c = $(".buyer-mobile-number").intlTelInput("getSelectedCountryData");
        return c['iso2'];
      }
      
      function getSelectedSellerCc(){
        var c = $(".seller-mobile-number").intlTelInput("getSelectedCountryData");
        return c['iso2'];
      }
      
      $(function() {
       $("#txtid").focus();
     });
      
      