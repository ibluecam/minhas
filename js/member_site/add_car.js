
$("#model_list").chained("#make_list");

 $(document).ready(function(e){
  $( "#car_fob_cost,#old_fob_cost" ).on( "change", function(e) {
      var fob = $('#car_fob_cost').val();
      var old_fob = $('#old_fob_cost').val();

      if (parseInt(old_fob)<parseInt(fob)) {
          alert("FOB must be smaller than previous FOB!");
          $(this).focus();
          $(this).css('border-color', 'red');
      }else{
        $(this).css('border-color', '');
      }

  });

  $("#car_fob_cost,#old_fob_cost,#manu_year,#car_mileage,#car_cc,#door,#car_seat,#car_length,#car_width,#car_height").keypress(function (e) {
      //if the letter is not digit then display error and don't type anything
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message //
          return false;
      }
  });

  $('.chassis_error').attr('style','display: none');

  if($("#discount:checked").val()){
    $("#input_new_price").attr("style","display:block;width:403px");
    $("#input_filed_fob").attr("style","width:332px");
    $("#input_filed_fob .input_label").attr("style","width:30px");
  }else{
    $("#input_new_price").attr("style","display:none");
    $("#input_filed_fob").attr("style","width:403px");

    $("#input_filed_fob .input_label").css("width","");
  }
  $("#normal").on( "change", function() {
    $("#input_new_price").attr("style","display:none");
    $("#input_filed_fob").attr("style","width:403px");

    $("#input_filed_fob .input_label").css("width","");
  });
  $("#discount").on( "change", function() {
    $("#input_new_price").attr("style","display:block;width:403px;");
    $("#input_filed_fob").attr("style","width:332px");
    $("#input_filed_fob .input_label").attr("style","width:30px");
  });
    // $('.btn_quick_discount').click(function(e) {   
    //    var url = $(this).data('url');
    //    $.colorbox({
    //         width:"340", 
    //         height:"225", 
    //         iframe:true, 
    //         href:url,
    //         fixed:true,
    //         onClosed:function(){ 
    //             reloadFOB();
    //         }
    //     });
    // });

  // BLOCK CONTENTS CAR INFO & CAR IMAGE & PREVIEW 
  $("#car_image").attr("style", "display: none");

  // ACTION STEP
  $(".step_car_image").attr("style", "display: none");
  $(".step_preview").attr("style", "display: none");

  $( ".action_step_car_info" ).on( "click", function() {

      $("#car_info").attr("style", "display: block");
      $("#car_image").attr("style", "display: none");
      
      $(".step_car_info").attr("style", "display: block");
      $(".step_car_image").attr("style", "display: none");
      $(".step_preview").attr("style", "display: none");

  });

  $( ".action_step_car_image" ).on( "click", function() {

      $("#car_info").attr("style", "display: none");
      $("#car_image").attr("style", "display: block");
          
      $(".step_car_info").attr("style", "display: none");
      $(".step_car_image").attr("style", "display: block");
      $(".step_preview").attr("style", "display: none");
       
  });

  $( ".action_step_preview_save" ).on( "click", function() {

    if(validate()==false){

      $("#car_info").attr("style", "display: block");
      $("#car_image").attr("style", "display: none");
      
      $(".step_car_info").attr("style", "display: block");
      $(".step_car_image").attr("style", "display: none");
      $(".step_preview").attr("style", "display: none");
      alert("Please enter all required fields!");

    }
      
  });
  $("#make_list").change(function(e){
        var car_make = $(this).val();
        $("#car_make").val(car_make.toUpperCase());

        var car_make_class = $("#make_list").find('option:selected').attr("class");

        if(car_make_class == 'add_new_maker'){

              $("#car_make").val('');
              $("#car_make").attr("required", "true");
              
              $("#make_list").removeAttr("required");

              $("#car_make").attr("style", "display: inline");
            
        }else{
            $("#car_make").attr("style", "display: none");
            $("#car_model").attr("style", "display: none");
        }

  });

  $("#model_list").change(function(e){
        var car_make = $(this).val();

        $("#car_model").val(car_make.toUpperCase());

        var car_model_class = $("#model_list").find('option:selected').attr("class");

        if(car_model_class == 'add_new_model'){

              $("#car_model").val('');
              $("#car_model").attr("required", "true");

              $("#model_list").removeAttr("required");

              $("#car_model").attr("style", "display: inline");
            
        }else{
            $("#car_model").attr("style", "display: none");
        }
        

  });
});

function getParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var click_step_upload_image = 0;
function step_upload_image() {
    if(getParameter('m')=="addcar"){
        var data = {
            create_mode: 'add_new'
        }
        click_step_upload_image += 1;
        if( click_step_upload_image === 1){
            $.ajax({
                type: "POST",
                url: "/?c=admin&m=addcar_exec",
                data: data,
                success: function(data){
                  if(data!=''){
                    //alert(data);
                      $('#idx').val($.trim(data));
                      $('#upload_iframe').attr('src','/?c=admin&m=adm_upload_image_car&idx='+$.trim(data));
                  }
                }
            
            });
        }
    
    }
}
//var hostURL = window.location.protocol + "//" + window.location.host;
//if(hostURL=="http://motorbb.dev" || hostURL=="http://new.motorbb.com"){

   // document.getElementById("upload_widget_opener").addEventListener("click", function() {
   //  alert('ok');
   //  if(getParameter('m')=="addcar"){
   //    var data = {
   //        create_mode: 'add_new'
   //      }
   //    $.ajax({
   //      type: "POST",
   //      url: "/?c=admin&m=addcar_exec",
   //      data: data,
   //      success: function(data){
   //        if(data!=''){
   //            $('#idx').val($.trim(data));
   //            $('#upload_iframe').attr('src','/?c=admin&m=adm_bbs_cloud_upload&mcd=product&idx='+$.trim(data));
   //        }
   //      }
   //    });
   //  }
    // cloudinary.openUploadWidget({ cloud_name: 'ratha-it', upload_preset: 'lnlzmvqo', max_image_width: 1024, max_image_height: 768}, 
    //   function(error, result) { 
    //     var batch_data = [];
    //     var url_string = '';
    //     var count_url = 0;
    //     var idx = $("#idx").val();

    //     for (i = 0; i < result.length; i++) {
    //         var public_id         = result[i].public_id; 
    //         var resource_type     = result[i].resource_type;
    //         var secure_url        = result[i].secure_url;
    //         var original_filename = result[i].original_filename;

    //         url_string += secure_url + ', ';
    //         count_url ++;
        
    //         var data = {
    //           idx               : idx,
    //           public_id         : public_id,
    //           resource_type     : resource_type,
    //           secure_url        : secure_url,
    //           original_filename : original_filename,
    //         }
    //         $.ajax({
    //           type: "POST",
    //           url: "/?c=admin&m=adm_bbs_cloud_upload_exec",
    //           data: data,
    //           async: true,
    //           success: function(data){
    //             document.getElementById("upload_iframe").contentWindow.append_image(data);    
    //            }
    //         });

    //     }

    //     //GET ACTIVITY
    //     var log_data={
    //         idx: idx,
    //         count_url : count_url,
    //         url_string: url_string
    //     };
    //     $.ajax({
    //         type: "POST",
    //         url : "/?c=admin&m=adm_insert_activity_log_images_for_car",
    //         data: log_data
    //     });
        
    //   });
  //}, false);
//}else{
  //  document.getElementById("upload_widget_opener").addEventListener("click", function() {
  //   if(getParameter('m')=="addcar"){
  //     var data = {
  //         create_mode: 'add_new'
  //       }
  //     $.ajax({
  //       type: "POST",
  //       url: "/?c=admin&m=addcar_exec",
  //       data: data,
  //       success: function(data){
  //         if(data!=''){
  //             $('#idx').val($.trim(data));
  //             $('#upload_iframe').attr('src','/?c=admin&m=adm_bbs_cloud_upload&mcd=product&idx='+$.trim(data));
  //         }else{
            
  //         }
  //       }
  //     });
  //   }
  //   cloudinary.openUploadWidget({ cloud_name: 'softbloom', upload_preset: 'juefxlp7', show_powered_by: false, max_image_width: 1024, max_image_height: 768}, 
  //     function(error, result) { 
  //       var batch_data = [];
  //       var url_string = '';
  //       var count_url = 0;
  //       var idx = $("#idx").val();
  //       for (i = 0; i < result.length; i++) {
  //           var public_id         = result[i].public_id; 
  //           var resource_type     = result[i].resource_type;
  //           var secure_url        = result[i].secure_url;
  //           var original_filename = result[i].original_filename;

  //           url_string += secure_url + ', ';
  //           count_url ++;
        
  //           var data = {
  //             idx               : idx,
  //             public_id         : public_id,
  //             resource_type     : resource_type,
  //             secure_url        : secure_url,
  //             original_filename : original_filename,
  //           }
  //           $.ajax({
  //             type: "POST",
  //             url: "/?c=admin&m=adm_bbs_cloud_upload_exec",
  //             data: data,
  //             async: true,
  //             success: function(data){
  //               document.getElementById("upload_iframe").contentWindow.append_image(data);    
  //              }
  //           });

  //       }

  //       //GET ACTIVITY
  //       var log_data={
  //           idx: idx,
  //           count_url : count_url,
  //           url_string: url_string
  //       };
  //       $.ajax({
  //           type: "POST",
  //           url : "/?c=admin&m=adm_insert_activity_log_images_for_car",
  //           data: log_data
  //       });
        
  //     });
  // }, false);	
//}


// window.onload = function () {
    
//     setInterval(function() {
//         ResizeIframeFromParent('upload_iframe');
//     }, 1000);
// }
// function ResizeIframeFromParent(id) {
//     if (jQuery('#'+id).length > 0) {
//         var window = document.getElementById(id).contentWindow;
//         var prevheight = jQuery('#'+id).attr('height');
//         var newheight = Math.max( window.document.body.scrollHeight, window.document.body.offsetHeight, window.document.documentElement.clientHeight, window.document.documentElement.scrollHeight, window.document.documentElement.offsetHeight );
//         if (newheight != prevheight && newheight > 0) {
//             jQuery('#'+id).attr('height', newheight);
//             console.log("Adjusting iframe height for "+id+": " +prevheight+"px => "+newheight+"px");
//         }
//     }
// }

$('#car_chassis_no').on('input',function(){

    var Chassis  = $(this).val();
    
    if (Chassis.length >= 4 && Chassis.length <= 20){
        $.ajax({
            url: '/?c=admin&m=check_chassis',
            type: "POST",
            async: true,
            data: {
                'chass_no': Chassis
            },
            success: function (result)
            {
                      
                if (result == 1 )
                {  
                    $('.chassis_error').html('already exist');
                    $('.chassis_error').attr('style','display: block');
                    $('input[type="submit"]').prop('title', 'Chassis No already exist, it will be disable button save!');
                    $('input[type="submit"]').attr('disabled', 'disabled');  
                    $('input[type="submit"]').css({cursor:"default"});
                }
                        
                if (result == 0)
                {
                    $('.chassis_error').html('');
                    $('.chassis_error').attr('style','display: none');
                    $('input[type="submit"]').removeAttr('disabled');
                    $('input[type="submit"]').prop('title', '');  
                    $('input[type="submit"]').css({cursor:"pointer"});
                }

            }
                    
        });
                
        return false;
    }
});

function validate(){

    if($('#make_list').val()==''){
      return false;
    }
    else if($('#model_list').val()==''){
      return false;
    }
    else if($('#car_model_year').val()==''){
      return false;
    }
    else if($('#car_mileage').val()==''){
      return false;
    }
    else if($('#car_cc').val()==''){
      return false;
    }
    else if($('#car_chassis_no').val()==''){
      return false;
    }
    else{
      return true;
    }
}

// function reloadFOB(){
//     var idx = $('#idx').val();
//     $.ajax({
//         method: "POST",
//         url: "/?c=admin&m=adm_get_fob_discount",
//         data: {
//           idx: idx
//         }
//     })
//     .done(function( data ) {
//         $("#car_fob_cost").val(data);
//     });
        
// }


