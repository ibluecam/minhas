$(document).ready(function(){



	$(window).resize(function() {
		var width = $(window).width();

		if(width > 1000){
			$(".icon-plus-sidebar-1").attr('style','display: none !important');
			$(".icon-minus-sidebar-1").attr('style','display: none !important');
			$(".list-data-sidebar-1").show();

			$(".icon-plus-sidebar-2").attr('style','display: none !important');
			$(".icon-minus-sidebar-2").attr('style','display: none !important');
			$(".list-data-sidebar-2").show();

			$(".icon-plus-sidebar-3").attr('style','display: none !important');
			$(".icon-minus-sidebar-3").attr('style','display: none !important');
			$(".list-data-sidebar-3").show();




			$(".icon-plus-sidebar-4").attr('style','display: none !important');
			$(".icon-minus-sidebar-4").attr('style','display: none !important');
			$(".data-search-by-country").show();


			
			$(".sidebar-3").attr('style','display: block !important');

			$(".bar_menu").removeClass("warpper_bar_plus_2");
			$(".bar_menu").removeClass("warpper_bar_plus");


			$(".bar_type").removeClass("warpper_bar_plus_type_2");
			$(".bar_type").removeClass("warpper_bar_plus_type");

			$(".head-search-by-car-site").removeClass("warpper_bar_plus_box_2");
			$(".head-search-by-car-site").removeClass("warpper_bar_box");

			$(".head-search-by-country-site").removeClass("warpper_bar_plus_con_2");
			$(".head-search-by-country-site").removeClass("warpper_bar_con");

			$(".home_imageb").attr('style','display:none !important');
			$(".home_image").attr('style','display:block !important');


			$(".imgb").attr('style','display:none !important');
			$(".img").attr('style','display:block !important');


			

		}else{

			 $(".icon-plus-sidebar-1").attr('style','display: block !important');
			 $(".icon-plus-sidebar-2").attr('style','display: block !important');
			 $(".icon-plus-sidebar-3").attr('style','display: block !important');
			
			 $(".icon-plus-sidebar-4").attr('style','display: block !important');

			$(".sidebar-3").attr('style','display:none');
			$(".bar_menu").addClass("warpper_bar_plus");
			$(".bar_type").addClass("warpper_bar_plus_type");
			$(".head-search-by-car-site").addClass("warpper_bar_plus_box");
			$(".head-search-by-country-site").addClass("warpper_bar_plus_con");

			$(".home_imageb").attr('style','display:block !important');
			$(".home_image").attr('style','display:none !important');


			$(".imgb").attr('style','display:block !important');
			$(".img").attr('style','display:none !important');


			


		}


		
		if (width > 1000) {


			$(".group_control_search").attr('style','display:block !important');
			$(".warpper_contant_search").attr('style','display:block !important');
			$(".group_control_small").attr('style','display:none !important');
			$(".warpper_contant_search_small").attr('style','display:none !important');

		}else{


			$(".group_control_search").attr('style','display:none !important');
			$(".warpper_contant_search").attr('style','display:none !important');
			$(".group_control_small").attr('style','display:block !important');
			$(".warpper_contant_search_small").attr('style','display:block !important');
		
			

		}



		if(width > 1028){

			//==== Start block seller profile in detail page =====//

			$(".seller-block-formobile").attr('style','display:none !important');
			$(".seller-block-forpc").attr('style','display:block !important');
			//==== End block seller profile in detail page =====//

		}else{

			//==== Start block seller profile in detail page =====//
			$(".seller-block-forpc").attr('style','display:none !important');
			$(".seller-block-formobile").attr('style','display:block !important');
				
			//==== End block seller profile in detail page =====//

		}


	});


	$(window).load(function () {
		var width = $( ".container .row" ).width();
		if(width > 1000){
		  	$(".icon-plus-sidebar-1").attr('style','display: none !important');
			$(".icon-minus-sidebar-1").attr('style','display: none !important');
			$(".list-data-sidebar-1").show();

			$(".icon-plus-sidebar-2").attr('style','display: none !important');
			$(".icon-minus-sidebar-2").attr('style','display: none !important');
			$(".list-data-sidebar-2").show();

			$(".icon-plus-sidebar-3").attr('style','display: none !important');
			$(".icon-minus-sidebar-3").attr('style','display: none !important');
			$(".list-data-sidebar-3").show();




			$(".icon-plus-sidebar-4").attr('style','display: none !important');
			$(".icon-minus-sidebar-4").attr('style','display: none !important');
			$(".data-search-by-country").show();

			$(".bar_menu").removeClass("warpper_bar_plus_2");
			$(".bar_menu").removeClass("warpper_bar_plus");

			$(".bar_type").removeClass("warpper_bar_plus_type_2");
			$(".bar_type").removeClass("warpper_bar_plus_type");

			$(".head-search-by-car-site").removeClass("warpper_bar_plus_box_2");
			$(".head-search-by-car-site").removeClass("warpper_bar_box");

			$(".head-search-by-country-site").removeClass("warpper_bar_plus_con_2");
			$(".head-search-by-country-site").removeClass("warpper_bar_con");



			$(".home_imageb").attr('style','display:none !important');
			$(".home_image").attr('style','display:block !important');


			$(".imgb").attr('style','display:none !important');
			$(".img").attr('style','display:block !important');


			//==== Start block seller profile in detail page =====//

			//$(".seller-block-formobile").attr('style','display:none !important');
			//$(".seller-block-forpc").attr('style','display:block !important');
			//==== End block seller profile in detail page =====//

		}else{

			
			$(".icon-plus-sidebar-1").attr('style','display: block !important');
			$(".icon-minus-sidebar-1").attr('style','display: none !important');
			$(".list-data-sidebar-1").slideUp(300);	

			$(".icon-plus-sidebar-2").attr('style','display: block !important');
			$(".icon-minus-sidebar-2").attr('style','display: none !important');
			$(".list-data-sidebar-2").slideUp(300);	

			$(".icon-plus-sidebar-3").attr('style','display: block !important');
			$(".icon-minus-sidebar-3").attr('style','display: none !important');
			$(".list-data-sidebar-3").slideUp(300);	

			$(".icon-plus-sidebar-4").attr('style','display: block !important');
			$(".icon-minus-sidebar-4").attr('style','display: none !important');
			$(".data-search-by-country").slideUp(300);	

			$(".sidebar-3").attr('style','display:none');

			$(".bar_menu").addClass("warpper_bar_plus");
			$(".bar_type").addClass("warpper_bar_plus_type");
			$(".head-search-by-car-site").addClass("warpper_bar_plus_box");
			$(".head-search-by-country-site").addClass("warpper_bar_plus_con");


			$(".home_imageb").attr('style','display:block !important');
			$(".home_image").attr('style','display:none !important');


			$(".imgb").attr('style','display:block !important');
			$(".img").attr('style','display:none !important');

			//==== Start block seller profile in detail page =====//
				//$(".seller-block-forpc").attr('style','display:none !important');
				//$(".seller-block-formobile").attr('style','display:block !important');
			//==== End block seller profile in detail page =====//

		}

		if(width > 1028){

			//==== Start block seller profile in detail page =====//

			$(".seller-block-formobile").attr('style','display:none !important');
			$(".seller-block-forpc").attr('style','display:block !important');
			//==== End block seller profile in detail page =====//

		}else{

			//==== Start block seller profile in detail page =====//
			$(".seller-block-forpc").attr('style','display:none !important');
			$(".seller-block-formobile").attr('style','display:block !important');
				
			//==== End block seller profile in detail page =====//
			
		}

		

		if (width > 1000) {


			$(".group_control_search").attr('style','display:block !important');
			$(".warpper_contant_search").attr('style','display:block !important');
			$(".group_control_small").attr('style','display:none !important');
			$(".warpper_contant_search_small").attr('style','display:none !important');


		}else{

			$(".group_control_search").attr('style','display:none !important');
			$(".warpper_contant_search").attr('style','display:none !important');
			$(".group_control_small").attr('style','display:block !important');
			$(".warpper_contant_search_small").attr('style','display:block !important');

		}


	});


		$('.bar_menu').on('click',function(e) {    

			var width=$(window).width(); 
			if (width<=1000){

			  if ($(this).hasClass('warpper_bar_plus')){

			  	 $(".list-data-sidebar-1").slideDown(300);
			     $('.bar_menu').removeClass('warpper_bar_plus'); 
			     $('.bar_menu').addClass('warpper_bar_plus_2'); 
			     $(".list-data-sidebar-2").slideUp(300);	
				 $(".list-data-sidebar-3").slideUp(300);	
				 $(".data-search-by-country").slideUp(300);

				 $(".icon-plus-sidebar-1").attr('style','display: none !important');	
				 $(".icon-minus-sidebar-1").attr('style','display: block !important');
				 $(".icon-plus-sidebar-2").attr('style','display: block !important');
				 $(".icon-plus-sidebar-3").attr('style','display: block !important');
				 $(".icon-plus-sidebar-4").attr('style','display: block !important');
				 e.preventDefault();
			  } else {  
			    $(".list-data-sidebar-1").slideUp(300);
			     $('.bar_menu').removeClass('warpper_bar_plus_2'); 
			     $('.bar_menu').addClass('warpper_bar_plus'); 

			     $(".icon-minus-sidebar-1").attr('style','display: none !important');	
				 $(".icon-plus-sidebar-1").attr('style','display: block !important');
				 $(".icon-plus--sidebar-2").attr('style','display: block !important');
				 $(".icon-plus-sidebar-4").attr('style','display: block !important');
				 e.preventDefault();
			  }
			 	 
			}else{
				return false;
			}
		});

		$('.bar_type').on('click',function(e) {    

			var width=$(window).width(); 
			if (width<=1000){

			  if ($(this).hasClass('warpper_bar_plus_type')){
			  	 $(".list-data-sidebar-2").slideDown(300);
			     $('.bar_type').removeClass('warpper_bar_plus_type'); 
			     $('.bar_type').addClass('warpper_bar_plus_type_2'); 
			     $(".list-data-sidebar-1").slideUp(300);	
				 $(".list-data-sidebar-3").slideUp(300);	
				 $(".data-search-by-country").slideUp(300);

				 $(".icon-plus-sidebar-2").attr('style','display: none !important');	
				 $(".icon-minus-sidebar-2").attr('style','display: block !important');
				 $(".icon-plus-sidebar-1").attr('style','display: block !important');
				 $(".icon-plus-sidebar-3").attr('style','display: block !important');
				 $(".icon-plus-sidebar-4").attr('style','display: block !important');
				 e.preventDefault();

			  } else {  
			    $(".list-data-sidebar-2").slideUp(300);
			     $('.bar_type').removeClass('warpper_bar_plus_type_2'); 
			     $('.bar_type').addClass('warpper_bar_plus_type');  
			     $(".icon-minus-sidebar-2").attr('style','display: none !important');	
			 	 $(".icon-plus-sidebar-2").attr('style','display: block !important');
				 $(".icon-plus-sidebar-3").attr('style','display: block !important');
				 $(".icon-plus-sidebar-4").attr('style','display: block !important');
				 e.preventDefault();
			  }
			  e.preventDefault();
			}else{
				return false;
			}
		});


			$('.head-search-by-car-site').click(function(e) {    

			var width=$(window).width(); 
			if (width<=1000){

			  if ($(this).hasClass('warpper_bar_plus_box')){
			  	 $(".list-data-sidebar-3").slideDown(300);
			     $('.head-search-by-car-site').removeClass('warpper_bar_plus_box'); 
			     $('.head-search-by-car-site').addClass('warpper_bar_plus_box_2'); 
			     $(".list-data-sidebar-2").slideUp(300);
				 $(".list-data-sidebar-1").slideUp(300);	
				 $(".data-search-by-country").slideUp(300);

				 $(".icon-plus-sidebar-3").attr('style','display: none !important');	
				 $(".icon-minus-sidebar-3").attr('style','display: block !important');
				 $(".icon-plus-sidebar-2").attr('style','display: block !important');
				 $(".icon-plus-sidebar-1").attr('style','display: block !important');
				 $(".icon-plus-sidebar-4").attr('style','display: block !important');
				 e.preventDefault();

			  } else {  
			     $(".list-data-sidebar-3").slideUp(300);
			     $('.head-search-by-car-site').removeClass('warpper_bar_plus_box_2'); 
			     $('.head-search-by-car-site').addClass('warpper_bar_plus_box');

				$(".icon-minus-sidebar-3").attr('style','display: none !important');	
				$(".icon-plus-sidebar-3").attr('style','display: block !important');

				$(".icon-minus-sidebar-4").attr('style','display: none !important');
				$(".icon-plus-sidebar-4").attr('style','display: block !important');
				e.preventDefault();  
			  }
			  e.preventDefault();
			}else{
				return false;
			}
		});



		$('.head-search-by-country-site').click(function(e) {    

			var width=$(window).width(); 
			if (width<=1000){

			  if ($(this).hasClass('warpper_bar_plus_con')){
			  	 $(".data-search-by-country").slideDown(300);
			     $('.head-search-by-country-site').removeClass('warpper_bar_plus_con'); 
			     $('.head-search-by-country-site').addClass('warpper_bar_plus_con_2'); 


				$(".list-data-sidebar-1").slideUp(300);
				$(".list-data-sidebar-2").slideUp(300);	
				$(".list-data-sidebar-3").slideUp(300);

				$(".icon-plus-sidebar-4").attr('style','display: none !important');	
				$(".icon-minus-sidebar-4").attr('style','display: block !important');

				$(".icon-minus-sidebar-3").attr('style','display: none !important');
				$(".icon-plus-sidebar-3").attr('style','display: block !important');

				$(".icon-minus-sidebar-2").attr('style','display: none !important');
				$(".icon-plus-sidebar-2").attr('style','display: block !important');

				$(".icon-minus-sidebar-1").attr('style','display: none !important');
				$(".icon-plus-sidebar-1").attr('style','display: block !important');

			  } else {  
			    $(".data-search-by-country").slideUp(300);
			     $('.head-search-by-country-site').removeClass('warpper_bar_plus_con_2'); 
			     $('.head-search-by-country-site').addClass('warpper_bar_plus_con');  
			     $(".icon-minus-sidebar-4").attr('style','display: none !important');	

				$(".icon-plus-sidebar-4").attr('style','display: block !important');
				$(".icon-minus-sidebar-3").attr('style','display: none !important');
				$(".icon-plus-sidebar-3").attr('style','display: block !important');

				$(".icon-minus-sidebar-2").attr('style','display: none !important');
				$(".icon-plus-sidebar-2").attr('style','display: block !important');

				$(".icon-minus-sidebar-1").attr('style','display: none !important');
				$(".icon-plus-sidebar-1").attr('style','display: block !important');
			  }
			    e.preventDefault();
			}else{
				return false;
			}
		});





	 $(".icon-plus-sidebar-1").click(function(){
		$(".list-data-sidebar-1").slideDown(300);

		$(".list-data-sidebar-2").slideUp(300);	
		$(".list-data-sidebar-3").slideUp(300);	
		$(".data-search-by-country").slideUp(300);

		$(".icon-plus-sidebar-1").attr('style','display: none !important');	
		$(".icon-minus-sidebar-1").attr('style','display: block !important');
		$(".icon-plus-sidebar-2").attr('style','display: block !important');
		$(".icon-plus-sidebar-3").attr('style','display: block !important');

		$(".icon-minus-sidebar-4").attr('style','display: none !important');
		$(".icon-plus-sidebar-4").attr('style','display: block !important');
	});


	$(".icon-minus-sidebar-1").click(function(){

		$(".list-data-sidebar-1").slideUp(300);
		$(".data-search-by-country").slideUp(300);

		$(".icon-minus-sidebar-1").attr('style','display: none !important');	
		$(".icon-plus-sidebar-1").attr('style','display: block !important');
		$(".icon-plus--sidebar-2").attr('style','display: block !important');

		$(".icon-minus-sidebar-4").attr('style','display: none !important');
		$(".icon-plus-sidebar-4").attr('style','display: block !important');
		
	});

	$(".icon-plus-sidebar-2").click(function(){
		$(".list-data-sidebar-2").slideDown(300);
	
		$(".list-data-sidebar-1").slideUp(300);	
		$(".list-data-sidebar-3").slideUp(300);	

		$(".data-search-by-country").slideUp(300);

		$(".icon-plus-sidebar-2").attr('style','display: none !important');	
		$(".icon-minus-sidebar-2").attr('style','display: block !important');
		$(".icon-plus-sidebar-1").attr('style','display: block !important');
		$(".icon-plus-sidebar-3").attr('style','display: block !important');

		$(".icon-minus-sidebar-4").attr('style','display: none !important');
		$(".icon-plus-sidebar-4").attr('style','display: block !important');

	});


	$(".icon-minus-sidebar-2").click(function(){
		$(".list-data-sidebar-2").slideUp(300);
		$(".data-search-by-country").slideUp(300);
		$(".icon-minus-sidebar-2").attr('style','display: none !important');	
		$(".icon-plus-sidebar-2").attr('style','display: block !important');

		$(".icon-minus-sidebar-4").attr('style','display: none !important');
		$(".icon-plus-sidebar-4").attr('style','display: block !important');	
		

	});

	$(".icon-plus-sidebar-3").click(function(){
		$(".list-data-sidebar-3").slideDown(300);
	
		$(".list-data-sidebar-2").slideUp(300);
		$(".list-data-sidebar-1").slideUp(300);	

		$(".data-search-by-country").slideUp(300);

		$(".icon-plus-sidebar-3").attr('style','display: none !important');	
		$(".icon-minus-sidebar-3").attr('style','display: block !important');
		$(".icon-plus-sidebar-2").attr('style','display: block !important');
		$(".icon-plus-sidebar-1").attr('style','display: block !important');

		$(".icon-minus-sidebar-4").attr('style','display: none !important');
		$(".icon-plus-sidebar-4").attr('style','display: block !important');

	});


	$(".icon-minus-sidebar-3").click(function(){

		$(".list-data-sidebar-3").slideUp(300);

		$(".data-search-by-country").slideUp(300);

		$(".icon-minus-sidebar-3").attr('style','display: none !important');	
		$(".icon-plus-sidebar-3").attr('style','display: block !important');

		$(".icon-minus-sidebar-4").attr('style','display: none !important');
		$(".icon-plus-sidebar-4").attr('style','display: block !important');
		

	});






	$(".icon-plus-sidebar-4").click(function(){
		$(".data-search-by-country").slideDown(300);

		$(".list-data-sidebar-1").slideUp(300);
		$(".list-data-sidebar-2").slideUp(300);	
		$(".list-data-sidebar-3").slideUp(300);

		$(".icon-plus-sidebar-4").attr('style','display: none !important');	
		$(".icon-minus-sidebar-4").attr('style','display: block !important');

		$(".icon-minus-sidebar-3").attr('style','display: none !important');
		$(".icon-plus-sidebar-3").attr('style','display: block !important');

		$(".icon-minus-sidebar-2").attr('style','display: none !important');
		$(".icon-plus-sidebar-2").attr('style','display: block !important');

		$(".icon-minus-sidebar-1").attr('style','display: none !important');
		$(".icon-plus-sidebar-1").attr('style','display: block !important');



	});


	$(".icon-minus-sidebar-4").click(function(){

		$(".data-search-by-country").slideUp(300);

		$(".icon-minus-sidebar-4").attr('style','display: none !important');	
		$(".icon-plus-sidebar-4").attr('style','display: block !important');

		$(".icon-minus-sidebar-3").attr('style','display: none !important');
		$(".icon-plus-sidebar-3").attr('style','display: block !important');

		$(".icon-minus-sidebar-2").attr('style','display: none !important');
		$(".icon-plus-sidebar-2").attr('style','display: block !important');

		$(".icon-minus-sidebar-1").attr('style','display: none !important');
		$(".icon-plus-sidebar-1").attr('style','display: block !important');

		
	});

	
	$(".clicksearch_small").click(function(){
			$(".group_control_search").attr('style','display:none !important');
			$(".warpper_contant_search").attr('style','display:none !important');
			$(".group_control_small").attr('style','display:block !important');
			$(".warpper_contant_search_small").attr('style','display:block !important');
	});





	$(".smallimg").click(function(a){
		a.preventDefault();
		var ID = $(this).attr("a");
		$("img.imgactive").removeClass('imgactive');
        $("img.imgactive_"+ID).addClass('imgactive');

	});		
	

});

(function(d, s, id) {
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) return;
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=432111436966513";
   fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
