
$("#tel,#mobile").intlTelInput({

        allowExtensions: true,
        //autoFormat: false,
        autoHideDialCode: false,
        autoPlaceholder: false,
        
        defaultCountry: "kh",
         eoIpLookup: function(callback) {
           $.get('http://ipinfo.io', function() {}, "jsonp").always(function(resp) {
             var countryCode = (resp && resp.country) ? resp.country : "";
             callback(countryCode);
           });
         },
        nationalMode: false,
        numberType: "MOBILE",

        utilsScript: "/intlTelInput/lib/libphonenumber/build/utils.js"
});



function slide_up_down(src,src_set,img_down,img_up,block_hide_show_content){

    var newsrc = (src==img_down) ? img_up : img_down;
    $(src_set).attr('src', newsrc );

    if(src == img_down){
	    $(block_hide_show_content).slideDown(300);
    }else{
    	$(block_hide_show_content).slideUp(300);
    }

}

$(document).ready(function(){


	$(".click_slide_up_down_type").on('click',function(){

		var src = $(this).attr('src');
		slide_up_down(src,'.click_slide_up_down_type','/images/public_mobile_site/home/select_drop_down.png','/images/public_mobile_site/home/select_drop_up.png','.content_category_type');

	});	
	


});


