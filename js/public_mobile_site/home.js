$("#series_car").chained("#make_car");

function slide_up_down(src,src_set,img_down,img_up,block_hide_show_content){

    var newsrc = (src==img_down) ? img_up : img_down;
    $(src_set).attr('src', newsrc );

    if(src == img_down){
	    $(block_hide_show_content).slideDown(300);
    }else{
    	$(block_hide_show_content).slideUp(300);
    }

}

$(document).ready(function(){

	$(".click_slide_up_down_country").on('click',function(){
		
		$(".content_category_brand").slideUp(300);
		$(".click_slide_up_down_brand").attr('src', '/images/public_mobile_site/home/select_drop_down.png');
		$(".content_category_type").slideUp(300);
		$(".click_slide_up_down_type").attr('src', '/images/public_mobile_site/home/select_drop_down.png');
		
		var src = $(this).attr('src');
		slide_up_down(src,'.click_slide_up_down_country','/images/public_mobile_site/home/select_drop_down.png','/images/public_mobile_site/home/select_drop_up.png','.category_country_content');

	});	



	$(".click_slide_up_down_brand").on('click',function(){

		$(".category_country_content").slideUp(300);
		$(".click_slide_up_down_country").attr('src', '/images/public_mobile_site/home/select_drop_down.png');
		$(".content_category_type").slideUp(300);
		$(".click_slide_up_down_type").attr('src', '/images/public_mobile_site/home/select_drop_down.png');
		
		var src = $(this).attr('src');
		slide_up_down(src,'.click_slide_up_down_brand','/images/public_mobile_site/home/select_drop_down.png','/images/public_mobile_site/home/select_drop_up.png','.content_category_brand');

	});	



	$(".click_slide_up_down_type").on('click',function(){
		
		$(".content_category_brand").slideUp(300);
		$(".click_slide_up_down_brand").attr('src', '/images/public_mobile_site/home/select_drop_down.png');
		$(".category_country_content").slideUp(300);
		$(".click_slide_up_down_country").attr('src', '/images/public_mobile_site/home/select_drop_down.png');
		
		var src = $(this).attr('src');
		slide_up_down(src,'.click_slide_up_down_type','/images/public_mobile_site/home/select_drop_down.png','/images/public_mobile_site/home/select_drop_up.png','.content_category_type');

	});	
	


});