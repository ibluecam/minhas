

$(document).ready(function(){

	$("form :input").on("keypress", function(e) {
		return e.keyCode != 13;
	});

	$(".click_slide_up").on('click',function(){

		$( this ).toggleClass("click_slide_up click_slide_down");

		var get_class = $( this ).attr('class');
		
		if(get_class == 'click_slide_down'){
			$('.content_hide_show').slideDown(300);
		}else{
			$('.content_hide_show').slideUp(300);
		}

	});	


	$(".icon_slide_down_searchbox").on('click', function(){
		var src = $(this).attr('src');
	    var newsrc = (src=='images/public_mobile_site/down_search_box.png') ? 'images/public_mobile_site/up_search_box.png' : 'images/public_mobile_site/down_search_box.png';
	    $(this).attr('src', newsrc );

	    if(src == 'images/public_mobile_site/down_search_box.png'){
	    	$('.content_search_box').slideDown(300);
	    }else{
	    	$('.content_search_box').slideUp(300);
	    }
	    
	});

	//============= Search Box Car list ===========//
	$(".button_submit").on('click', function(){
          var make = $(".make").val();
          var min_year = $(".min_year").val();
          var max_year = $(".max_year").val();
          var model = $(".model").val();
          var min_price = $(".min_price").val();
          var max_price = $(".max_price").val();
          var country = $(".country").val();
          var keyword = $(".keyword").val();   
          var search_drive_type = $(".search_drive_type").val(); 

          var url = "/?c=user&m=mobile_car_list&car_make="+make+"&car_model="+model+"&car_drive_type="+search_drive_type+"&keyword="+keyword+"&min_model_year="+min_year +"&max_model_year="+max_year+"&min_fob_cost="+min_price+"&max_fob_cost="+max_price+"&country="+country;
          history.pushState({}, '', url);
          location.href=url;

    }); 

	//============= reset textBox search Car list ===========//
    $(".button_reset").on('click', function(){
          var make = $(".make").val('');
          var min_year = $(".min_year").val('');
          var max_year = $(".max_year").val('');
          var model = $(".model").val('');
          var min_price = $(".min_price").val('');
          var max_price = $(".max_price").val('');
          var country = $(".country").val('');
           var search_drive_type = $(".search_drive_type").val(''); 
          var keyword = $(".keyword").val("");
        /*  
          var url = "/?c=user&m=mobile_car_list&car_make="+make+"&car_model="+model+"&keyword="+keyword+"&min_model_year="+min_year +"&max_model_year="+max_year+"&min_fob_cost="+min_price+"&max_fob_cost="+max_price+"&country="+country;
          history.pushState({}, '', url);
          location.href=url;
		*/
    }); 

});