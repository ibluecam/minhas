
$(document).ready(function(){
	var cmatch = false;
	var cformat = false;
	var valid = false;
	var typingTimer;       
	var doneTypingInterval = 900; 
	$('#forgotemail').on('input',function(){
	  clearTimeout(typingTimer);
	  if ($('#forgotemail').val) {
		typingTimer = setTimeout(function(){
			var email = $('#forgotemail').val();
	        if (!email.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,10})+$/)) {
	            $('.error_email').html('Invalid Email Address');
	            return false;
	        }else{
	        	$('.error_email').html("");
	        }
       }, doneTypingInterval);
	  }
    });

	$("#frm_submit").on("submit",function(event){
        var email = $("#forgotemail").val();            
        if(email==""){
            $('.error_email').html("Email is requried!");
            return false;
        }
        if (!email.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,10})+$/)) {
	            $('.error_email').html('Invalid Email Address');
	            return false;
	    }
    });

    $("#btn_validate").on("click",function(event){
            var email = $("#input_password").val();
        	var cemail = $("#input_cpassword").val(); 
        	if(valid==false){
        		if(email==""){
        			$('.error_password_1').html("Password is requried!");
        		}
        		if (cemail==""){
          			$('.error_cpassword_1').html("Confirm password is requried!");
          		}
        		return false;
        	}  
			 $("#submit_reset_password").trigger("click");
    });

     function checkPasswordformat() {
        var password = $("#input_password").val();
        var confirmPassword = $("#input_cpassword").val();
        if (password.length >= 6 && password.length <= 20){
            $('.error_password_1').html("");
            if (confirmPassword==""){
                $('.error_cpassword_1').html("");
            }else{
                cmatch = checkPasswordMatch(); 
            }
            return true;
        }else{
             if (confirmPassword!=""){
                checkPasswordMatch();
             }
             $('.error_password_1').html('Password must be length 6-20 characters!');
        }
    }

    function checkPasswordMatch() {
        var password = $("#input_password").val();
        var confirmPassword = $("#input_cpassword").val();
            if (password == '' && confirmPassword==''){
                return false;
            }else{
                if (password != confirmPassword) {
                    $('.error_cpassword_1').html("Password does not match!");
                }
                else{
                    $('.error_cpassword_1').html("");
                    return true;
                }
            }
     }

    function disable_next(){
        valid = false;
    }
    function enable_next(){
        valid = true;
    }

    function validateAll(){
        if(cformat==true&&cmatch==true){
            enable_next();
        }else{
            disable_next();
        }
    }


    $('#input_password').on('input',function (event) {        
        cformat = checkPasswordformat();
        validateAll();
    });
    $('#input_cpassword').on('input',function (event) {
       cmatch = checkPasswordMatch();
       validateAll();
      
    });

});