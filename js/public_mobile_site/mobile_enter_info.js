
var valid_id=false;
var cmatch = false;
var cformat = false;
var valid_fname = false;
var valid_lname = false;
var valid_tel = false;
var valid = false;

    function checkPasswordformat() {
        var password = $("#input_password").val();
        var confirmPassword = $("#input_cpassword").val();

        
        if (password.length >= 6 && password.length <= 20){
            $('.error_password_1').html("");
            if (confirmPassword==""){
                $('.error_cpassword_1').html("");
            }else{
                cmatch = checkPasswordMatch(); 
            }
            return true;
        }else{
             if (confirmPassword!=""){
                checkPasswordMatch();
             }
             $('.error_password_1').html('Password must be length 6-20 characters!');
        }
    }

    function checkPasswordMatch() {
        var password = $("#input_password").val();
        var confirmPassword = $("#input_cpassword").val();
            if (password == '' && confirmPassword==''){
                return false;
            }else{
                if (password != confirmPassword) {
                    $('.error_cpassword_1').html("Password does not match!");
                }
                else{
                    $('.error_cpassword_1').html("");
                    return true;
                }
            }
        }
    



    var typingTimer;                //timer identifier
    var doneTypingInterval = 500; 

    function checkMemberId(){
        clearTimeout(typingTimer);
        if ($('#member_id').val) {
            valid_id = false;
            typingTimer = setTimeout(function(){
                var memeber_id = $('#member_id').val();
                if (memeber_id.length<4||member_id.length>20) {
                    $('.error_member_id').html('Your ID must be length 4-20 characters!');
                    
                }else{
                    if (!memeber_id.match(/^[a-z\d_]{4,20}$/i)) {
                        $('.error_member_id').html('Only letters, numbers and underscores');
            }else{
                        $.ajaxQueue({
                            url: '?c=admin&m=checkmemember',
                            type: "POST",
                            async: true,
                            data: {
                                'id': memeber_id
                            },
                            success: function (result)
                            {
                                if (result == 1){
                                    valid_id = false;
                                    $('.error_member_id').html('This ID already exist!');
                                }else{
                                    valid_id = true;
                                    validateAll();
                                    $('.error_member_id').html('');
                                }

                            }
                        });
                    }
                }

            }, doneTypingInterval);
        }        
    }
    
    function disable_next(){
        valid = false;
    }
    function enable_next(){
        valid = true;
    }

    function validateAll(){
        if(cformat==true&&cmatch==true&&valid_id==true&&valid_fname==true&&valid_lname==true&&valid_tel==true){
            enable_next();
        }else{
            disable_next();
        }
    }
    $(function () {  
        $('#input_password').on('input',function (event) {        
            cformat = checkPasswordformat();
            validateAll();
        });
        $('#input_cpassword').on('input',function (event) {
           cmatch = checkPasswordMatch();
           validateAll();
          
        });

        $('#member_id').on('input',function(){
            checkMemberId();
            validateAll();
        });

        $(".member_first_name").on('input',function(){
            var member_first_name=$(this).val();
            if(member_first_name==""){
                valid_fname=false;
                $('.error_first_name').html("First name is required!");
            }else{
                valid_fname=true;
                $('.error_first_name').html("");
            }
            validateAll();
        });
        $(".member_last_name").on('input',function(){
            var member_last_name=$(this).val();
            if(member_last_name==""){
                valid_lname = false;
                $('.error_last_name').html("Last name is required!");
            }else{
                valid_lname = true;
                $('.error_last_name').html("");
            }
            validateAll();
        });
        $(".seller-mobile-number").on('input',function(){
            var seller_mobile_number=$(this).val();
            if(seller_mobile_number.length<=4){
                valid_tel = false;
                $('.error_phone').html("Tel is required!");
            }else{
                valid_tel = true;
                $('.error_phone').html("");
            }
            validateAll();
        });
    });
	
     $("#bnt_validate").on("click",function(event){
            var password = $(".password_1").val();
            var confirmPassword =$(".cpassword_1").val();
            var email=$(".email_info").val();
            var member_id=$(".member_id").val();
            var member_first_name=$(".member_first_name").val();
            var member_last_name=$(".member_last_name").val();
            var seller_member_country=$("#seller_member_country").val();
            var seller_mobile_number=$(".seller-mobile-number").val(); 

            if(valid==false){
                if(member_id==""){
                $('.error_member_id').html("Member ID is required!");;
            }
            if(password==""){
                $('.error_password_1').html("Password is required!");
            }
            if(confirmPassword==""){
                $('.error_cpassword_1').html("ConfirmPassword is required!");
            }
           if(member_first_name==""){
                $('.error_first_name').html("First name is required!");
            }
            if(member_last_name==""){
                $('.error_last_name').html("Last name is required!");
            }
            if(seller_mobile_number.length<=4){
                $('.error_phone').html("Tel is required!");
            }
                return false;
            }      
            $(".email_confirm").val(email);
            $(".member_id_confirm").val(member_id);
            $(".password_1_confirm").val(password);
            $(".cpassword_1_confirm").val(confirmPassword);
            $(".member_first_name_confirm").val(member_first_name);
            $(".member_last_name_confirm").val(member_last_name);
            $(".country_confirm").val(seller_member_country);
            $(".phone_no1_confirm").val(seller_mobile_number);
            $(".display_confirm").css({ display: "block" });
            $(".display_enter_info").css({ display: "none" });                
        });

        $(".confirm_back").on("click",function(event){
             $(".display_confirm").css({ display: "none" });
             $(".display_enter_info").css({ display: "block" });
         });
     

	 // $(".buyer-mobile-number, .seller-mobile-number").intlTelInput({
  //       allowExtensions: true,
  //       autoFormat: true,
  //       autoHideDialCode: false,
  //       autoPlaceholder: false,
  //       defaultCountry: "kh",
  //       ipinfoToken: "yolo",
  //       nationalMode: false,
  //       numberType: "MOBILE",
  //       // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
  //       preferredCountries: ['kr', 'jp'],
  //       utilsScript: "/lib/libphonenumber/build/utils.js"
  //     });

$(".buyer-mobile-number, .seller-mobile-number").intlTelInput({

        allowExtensions: true,
        //autoFormat: false,
        autoHideDialCode: false,
        autoPlaceholder: false,
        
        defaultCountry: "kh",
         eoIpLookup: function(callback) {
           $.get('http://ipinfo.io', function() {}, "jsonp").always(function(resp) {
             var countryCode = (resp && resp.country) ? resp.country : "";
             callback(countryCode);
           });
         },
        nationalMode: false,
        numberType: "MOBILE",

        utilsScript: "/intlTelInput/lib/libphonenumber/build/utils.js"
});

      
      function getSelectedBuyerCc(){
        var c = $(".buyer-mobile-number").intlTelInput("getSelectedCountryData");
        return c['iso2'];
      }
      
      function getSelectedSellerCc(){
        var c = $(".seller-mobile-number").intlTelInput("getSelectedCountryData");
        return c['iso2'];
      }
      
      $(function() {
       $("#txtid").focus();
     });
      