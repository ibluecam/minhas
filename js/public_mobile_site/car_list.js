$("#series_car").chained("#make_car");

$("#port_name").chained("#destination_country");

function slide_up_down(src,src_set,img_down,img_up,block_hide_show_content){

    var newsrc = (src==img_down) ? img_up : img_down;
    $(src_set).attr('src', newsrc );

    if(src == img_down){
	    $(block_hide_show_content).slideDown(300);
    }else{
    	$(block_hide_show_content).slideUp(300);
    }

}
function getParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
$(document).ready(function(){


	$(".click_slide_up_down_type").on('click',function(){

		var src = $(this).attr('src');
		slide_up_down(src,'.click_slide_up_down_type','/images/public_mobile_site/home/select_drop_down.png','/images/public_mobile_site/home/select_drop_up.png','.content_category_type');

	});


	$( ".clickpagination" ).on( "click", function() {

 	    var PageID = $(this).attr("id");

 	    var url = window.location.href;
  
        var pieces = url.split("/?");
    
        var pieces1 = pieces[1].split("&");

         $.each(pieces1, function(key, value) {
           
            if(key == pieces1.length - 1){
              valuefull = value;
              value = value.substring(0,4);
              
              if(value == "page"){
                
                var old = pieces = "?"+pieces[1];

                var news = old.replace(valuefull,"page="+PageID);

                location.href=news;
                
              }else{
                location.href=url+"&page="+PageID;
              }

            }
            
         });

	});

      $( ".clickstock_all").on( "click", function() {
          window.location.href="/?c=user&m=mobile_car_list";
      })

      $( ".clickstock" ).on( "click", function() {
          
                  var stockcountry = $(this).attr("id");
                 
                  var url = window.location.href;

                	var pieces = url.split("/?");

                  var split_url_count = pieces[1].split("&");
               
                  var page = pieces[1].split("&");

                  page = page[page.length - 1].split("=");


                  var country = pieces[1].split("&");
                  var country_c = country[country.length - 2].split("=");

                if(split_url_count.length > 2){

                    if(country_c[country_c.length - 2] == "country" && page[page.length - 2] == "page"){
                	
                	      var fullcountry_url = country[country.length - 2];

                	      var old = pieces = "?"+pieces[1];

                	    /* old = old.split('').reverse().join('');
                	     old = old.substring(7);
                	     old = old.split('').reverse().join('');*/

                        var news = old.replace(fullcountry_url,"country="+stockcountry);

                        var page_num = getParameter('page');

                        var newurl = news.replace('&page='+page_num,'');

                        location.href=newurl;
                
                    }else if(country_c[country_c.length - 2] != "country" && page[page.length - 2] == "page"){
                	
                	      var currenturl = "?"+pieces[1];

                	      var old = pieces[1].split("&");

                	      var values = "country="+stockcountry

                	      var news = currenturl.replace(old[old.length - 1],values);

                        var page_num = getParameter('page');

                        var newurl = news.replace('&page='+page_num,'');

                        location.href=newurl;
            
                    }else{

                	      var fullcountry_url = country[country.length - 1];
                	      country_c = country[country.length - 1].split("=");

                	      if(country_c[country_c.length - 2] == "country"){
                		
                		          var old = pieces = "?"+pieces[1];

        	                    var news = old.replace(fullcountry_url,"country="+stockcountry);

        	                    location.href=news;

                	      }else{
                		          location.href=url+"&country="+stockcountry;
                	       }

                    }

              }else{
            	     location.href=url+"&country="+stockcountry;
              }


      });


    $('.tooltip').tooltipster({
        contentAsHTML: true,
        interactive: true,
        theme: 'tooltipster-shadow'
    });

});