  $(document).ready(function(){

         $(window).bind('popstate', function(){
                  window.location.href = window.location.href;
            });
      
        /*** LOAD ONLY CONTENT BELOW SEARCH ***/
        $(document).on("click",".ajax_load_content",function(e) {
            url = $(this).attr('href');
            /*** Save to web history to press back or forward ***/
            history.pushState({}, '', url);


            /*** Loading to container ***/
            $(".detail-data-box-all").load(url);
            return false;
        });

        /*** LOAD SEARCH COMPONENT + CONTENT BELOW ***/
        $(document).on("click",".ajax_load_search_content",function(e) {
            e.preventDefault();

            url = $(this).attr('href');
            //url = url.replace("Pick Up", "Pick-Up"); 
            /*** Save to web history to press back or forward ***/
            history.pushState({}, '', url);
            /*** Append search component if not exist ***/
            if($("#center-quick-search-container").length==0){
                $.get("?c=user&m=get_component_center_search", function(result) {

                    $('.main-data-content-blog').prepend(result);
                });
            }
            /*** Loading to container ***/
            $(".detail-data-box-all").load(url);
            return false;
        });

        /*** LOAD MAIN CONTAINER (REPLACE CONTENT & SEARCH COMPONENT) ***/
        $(document).on("click",".ajax_load_main",function(e) {
            url = $(this).attr('href');
            /*** Save to web history to press back or forward ***/
            history.pushState({}, '', url);
            /*** Loading to container ***/
            $(".main-data-content-blog").load(url);
            return false;
        });

        $(document).ajaxStart(function(){
            $("#wait").css("display", "block");
        });
        $(document).ajaxComplete(function(){
            $("#wait").css("display", "none");
        });
       

       $("#clicksearchdata").click(function(){


             $(document).ajaxStart(function(){
                $("#wait").css("display", "block");
            });
            $(document).ajaxComplete(function(){
                $("#wait").css("display", "none");
            });



            var carmake = $("#carmake").val();
            var carmodel = $("#carmodel").val();
            var keyword = $("#keyword").val();
            var carminyear = $("#carminyear").val();
            var carminprice = $("#carminprice").val();
            var carcountry = $("#carcountry").val();
            var carmaxyear = $("#carmaxyear").val();
            var carmaxrpice = $("#carmaxrpice").val();

            var datastring = "carmake="+carmake+"&carmodel="+carmodel+"&keyword="+keyword+"&carminyear="+carminyear+"&carminprice="+carminprice+"&carcountry="+carcountry+"&carmaxyear="+carmaxyear+"&carmaxrpice="+carmaxrpice + "&page=";

            var urlstring = "?c=user&m=search_boxpageajax";


            $.ajax({
                    url: urlstring,
                    type: "POST",
                    data: datastring,
                    success: function(data){

                        $('.detail-data-box-all').html(data);

                        $('html, body').animate({
                            scrollTop: $(".main-detail-data-area").offset().top
                        }, 1000);
                       
                    }
            });





       });


     $(document).ajaxStart(function(){
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function(){
        $("#wait").css("display", "none");
    });







    });


