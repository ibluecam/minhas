
   
    

  function isNumberKey(evt){
    var charCode = evt.keyCode;
    
	if(evt.ctrlKey == true)
	{
	if(charCode == 67 || charCode == 86)
	{
	 return true;
	}
	}
	if (charCode == 8 || //backspace
                charCode == 46 || //delete
                charCode == 13)   //enter key
    {
        return true;
    }
    else if (charCode >= 37 && charCode <= 40) //arrow keys
    {
        return true;
    }
	
    else if (charCode >= 48 && charCode <= 57) //0-9 on key pad
    {
        if (evt.shiftKey == true)
            return false;

        return true;
    }
    else if (charCode >= 96 && charCode <= 105) //0-9 on num pad
    {
        if (evt.shiftKey == true)
            return false;

        return true;
    }
    else
        return false;
  }

$(document).ready(function(){
    $('#head_chat').click(function(e) {    
       $(".wrapper_iframe").toggleClass("class_none");
    });
    if ($("#destination_country option:selected").text()=="Select country") {
        $("#port_country_to").html();
    }else{
        $("#port_country_to").html($("#destination_country option:selected").text());
        $("#get_port_name").html($("#port_name option:selected").text());
    }
     

    $(window).resize(function() {
        var width = $(window).width();

        if(width > 1000){
            $(".icon-plus-sidebar-1").attr('style','display: none !important');
            $(".icon-minus-sidebar-1").attr('style','display: none !important');
            $(".list-data-sidebar-1").show();

            $(".icon-plus-sidebar-2").attr('style','display: none !important');
            $(".icon-minus-sidebar-2").attr('style','display: none !important');
            $(".list-data-sidebar-2").show();

            $(".icon-plus-sidebar-3").attr('style','display: none !important');
            $(".icon-minus-sidebar-3").attr('style','display: none !important');
            $(".list-data-sidebar-3").show();




            $(".icon-plus-sidebar-4").attr('style','display: none !important');
            $(".icon-minus-sidebar-4").attr('style','display: none !important');
            $(".data-search-by-country").show();


            
            $(".sidebar-3").attr('style','display: block !important');

            $(".bar_menu").removeClass("warpper_bar_plus_2");
            $(".bar_menu").removeClass("warpper_bar_plus");


            $(".bar_type").removeClass("warpper_bar_plus_type_2");
            $(".bar_type").removeClass("warpper_bar_plus_type");

            $(".head-search-by-car-site").removeClass("warpper_bar_plus_box_2");
            $(".head-search-by-car-site").removeClass("warpper_bar_box");

            $(".head-search-by-country-site").removeClass("warpper_bar_plus_con_2");
            $(".head-search-by-country-site").removeClass("warpper_bar_con");

            $(".home_imageb").attr('style','display:none !important');
            $(".home_image").attr('style','display:block !important');


            $(".imgb").attr('style','display:none !important');
            $(".img").attr('style','display:block !important');


            

        }else{

             $(".icon-plus-sidebar-1").attr('style','display: block !important');
             $(".icon-plus-sidebar-2").attr('style','display: block !important');
             $(".icon-plus-sidebar-3").attr('style','display: block !important');
            
             $(".icon-plus-sidebar-4").attr('style','display: block !important');

            $(".sidebar-3").attr('style','display:none');
            $(".bar_menu").addClass("warpper_bar_plus");
            $(".bar_type").addClass("warpper_bar_plus_type");
            $(".head-search-by-car-site").addClass("warpper_bar_plus_box");
            $(".head-search-by-country-site").addClass("warpper_bar_plus_con");

            $(".home_imageb").attr('style','display:block !important');
            $(".home_image").attr('style','display:none !important');


            $(".imgb").attr('style','display:block !important');
            $(".img").attr('style','display:none !important');


            


        }


        
        if (width > 1000) {


            $(".group_control_search").attr('style','display:block !important');
            $(".warpper_contant_search").attr('style','display:block !important');
            $(".group_control_small").attr('style','display:none !important');
            $(".warpper_contant_search_small").attr('style','display:none !important');

        }else{


            $(".group_control_search").attr('style','display:none !important');
            $(".warpper_contant_search").attr('style','display:none !important');
            $(".group_control_small").attr('style','display:block !important');
            $(".warpper_contant_search_small").attr('style','display:block !important');
        
            

        }



        if(width > 1028){

            //==== Start block seller profile in detail page =====//

            $(".seller-block-formobile").attr('style','display:none !important');
            $(".seller-block-forpc").attr('style','display:block !important');
            //==== End block seller profile in detail page =====//

        }else{

            //==== Start block seller profile in detail page =====//
            $(".seller-block-forpc").attr('style','display:none !important');
            $(".seller-block-formobile").attr('style','display:block !important');
                
            //==== End block seller profile in detail page =====//

        }


    });


    $(window).load(function () {
        var width = $( ".container .row" ).width();
        if(width > 1000){
            $(".icon-plus-sidebar-1").attr('style','display: none !important');
            $(".icon-minus-sidebar-1").attr('style','display: none !important');
            $(".list-data-sidebar-1").show();

            $(".icon-plus-sidebar-2").attr('style','display: none !important');
            $(".icon-minus-sidebar-2").attr('style','display: none !important');
            $(".list-data-sidebar-2").show();

            $(".icon-plus-sidebar-3").attr('style','display: none !important');
            $(".icon-minus-sidebar-3").attr('style','display: none !important');
            $(".list-data-sidebar-3").show();




            $(".icon-plus-sidebar-4").attr('style','display: none !important');
            $(".icon-minus-sidebar-4").attr('style','display: none !important');
            $(".data-search-by-country").show();

            $(".bar_menu").removeClass("warpper_bar_plus_2");
            $(".bar_menu").removeClass("warpper_bar_plus");

            $(".bar_type").removeClass("warpper_bar_plus_type_2");
            $(".bar_type").removeClass("warpper_bar_plus_type");

            $(".head-search-by-car-site").removeClass("warpper_bar_plus_box_2");
            $(".head-search-by-car-site").removeClass("warpper_bar_box");

            $(".head-search-by-country-site").removeClass("warpper_bar_plus_con_2");
            $(".head-search-by-country-site").removeClass("warpper_bar_con");



            $(".home_imageb").attr('style','display:none !important');
            $(".home_image").attr('style','display:block !important');


            $(".imgb").attr('style','display:none !important');
            $(".img").attr('style','display:block !important');


            //==== Start block seller profile in detail page =====//

            //$(".seller-block-formobile").attr('style','display:none !important');
            //$(".seller-block-forpc").attr('style','display:block !important');
            //==== End block seller profile in detail page =====//

        }else{

            
            $(".icon-plus-sidebar-1").attr('style','display: block !important');
            $(".icon-minus-sidebar-1").attr('style','display: none !important');
            $(".list-data-sidebar-1").slideUp(300); 

            $(".icon-plus-sidebar-2").attr('style','display: block !important');
            $(".icon-minus-sidebar-2").attr('style','display: none !important');
            $(".list-data-sidebar-2").slideUp(300); 

            $(".icon-plus-sidebar-3").attr('style','display: block !important');
            $(".icon-minus-sidebar-3").attr('style','display: none !important');
            $(".list-data-sidebar-3").slideUp(300); 

            $(".icon-plus-sidebar-4").attr('style','display: block !important');
            $(".icon-minus-sidebar-4").attr('style','display: none !important');
            $(".data-search-by-country").slideUp(300);  

            $(".sidebar-3").attr('style','display:none');

            $(".bar_menu").addClass("warpper_bar_plus");
            $(".bar_type").addClass("warpper_bar_plus_type");
            $(".head-search-by-car-site").addClass("warpper_bar_plus_box");
            $(".head-search-by-country-site").addClass("warpper_bar_plus_con");


            $(".home_imageb").attr('style','display:block !important');
            $(".home_image").attr('style','display:none !important');


            $(".imgb").attr('style','display:block !important');
            $(".img").attr('style','display:none !important');

            //==== Start block seller profile in detail page =====//
                //$(".seller-block-forpc").attr('style','display:none !important');
                //$(".seller-block-formobile").attr('style','display:block !important');
            //==== End block seller profile in detail page =====//

        }

        if(width > 1028){

            //==== Start block seller profile in detail page =====//

            $(".seller-block-formobile").attr('style','display:none !important');
            $(".seller-block-forpc").attr('style','display:block !important');
            //==== End block seller profile in detail page =====//

        }else{

            //==== Start block seller profile in detail page =====//
            $(".seller-block-forpc").attr('style','display:none !important');
            $(".seller-block-formobile").attr('style','display:block !important');
                
            //==== End block seller profile in detail page =====//
            
        }

        

        if (width > 1000) {


            $(".group_control_search").attr('style','display:block !important');
            $(".warpper_contant_search").attr('style','display:block !important');
            $(".group_control_small").attr('style','display:none !important');
            $(".warpper_contant_search_small").attr('style','display:none !important');


        }else{

            $(".group_control_search").attr('style','display:none !important');
            $(".warpper_contant_search").attr('style','display:none !important');
            $(".group_control_small").attr('style','display:block !important');
            $(".warpper_contant_search_small").attr('style','display:block !important');

        }


    });


        $('.bar_menu').on('click',function(e) {    

            var width=$(window).width(); 
            if (width<=1000){

              if ($(this).hasClass('warpper_bar_plus')){

                 $(".list-data-sidebar-1").slideDown(300);
                 $('.bar_menu').removeClass('warpper_bar_plus'); 
                 $('.bar_menu').addClass('warpper_bar_plus_2'); 
                 $(".list-data-sidebar-2").slideUp(300);    
                 $(".list-data-sidebar-3").slideUp(300);    
                 $(".data-search-by-country").slideUp(300);

                 $(".icon-plus-sidebar-1").attr('style','display: none !important');    
                 $(".icon-minus-sidebar-1").attr('style','display: block !important');
                 $(".icon-plus-sidebar-2").attr('style','display: block !important');
                 $(".icon-plus-sidebar-3").attr('style','display: block !important');
                 $(".icon-plus-sidebar-4").attr('style','display: block !important');
                 e.preventDefault();
              } else {  
                $(".list-data-sidebar-1").slideUp(300);
                 $('.bar_menu').removeClass('warpper_bar_plus_2'); 
                 $('.bar_menu').addClass('warpper_bar_plus'); 

                 $(".icon-minus-sidebar-1").attr('style','display: none !important');   
                 $(".icon-plus-sidebar-1").attr('style','display: block !important');
                 $(".icon-plus--sidebar-2").attr('style','display: block !important');
                 $(".icon-plus-sidebar-4").attr('style','display: block !important');
                 e.preventDefault();
              }
                 
            }else{
                return false;
            }
        });

        $('.bar_type').on('click',function(e) {    

            var width=$(window).width(); 
            if (width<=1000){

              if ($(this).hasClass('warpper_bar_plus_type')){
                 $(".list-data-sidebar-2").slideDown(300);
                 $('.bar_type').removeClass('warpper_bar_plus_type'); 
                 $('.bar_type').addClass('warpper_bar_plus_type_2'); 
                 $(".list-data-sidebar-1").slideUp(300);    
                 $(".list-data-sidebar-3").slideUp(300);    
                 $(".data-search-by-country").slideUp(300);

                 $(".icon-plus-sidebar-2").attr('style','display: none !important');    
                 $(".icon-minus-sidebar-2").attr('style','display: block !important');
                 $(".icon-plus-sidebar-1").attr('style','display: block !important');
                 $(".icon-plus-sidebar-3").attr('style','display: block !important');
                 $(".icon-plus-sidebar-4").attr('style','display: block !important');
                 e.preventDefault();

              } else {  
                $(".list-data-sidebar-2").slideUp(300);
                 $('.bar_type').removeClass('warpper_bar_plus_type_2'); 
                 $('.bar_type').addClass('warpper_bar_plus_type');  
                 $(".icon-minus-sidebar-2").attr('style','display: none !important');   
                 $(".icon-plus-sidebar-2").attr('style','display: block !important');
                 $(".icon-plus-sidebar-3").attr('style','display: block !important');
                 $(".icon-plus-sidebar-4").attr('style','display: block !important');
                 e.preventDefault();
              }
              e.preventDefault();
            }else{
                return false;
            }
        });


            $('.head-search-by-car-site').click(function(e) {    

            var width=$(window).width(); 
            if (width<=1000){

              if ($(this).hasClass('warpper_bar_plus_box')){
                 $(".list-data-sidebar-3").slideDown(300);
                 $('.head-search-by-car-site').removeClass('warpper_bar_plus_box'); 
                 $('.head-search-by-car-site').addClass('warpper_bar_plus_box_2'); 
                 $(".list-data-sidebar-2").slideUp(300);
                 $(".list-data-sidebar-1").slideUp(300);    
                 $(".data-search-by-country").slideUp(300);

                 $(".icon-plus-sidebar-3").attr('style','display: none !important');    
                 $(".icon-minus-sidebar-3").attr('style','display: block !important');
                 $(".icon-plus-sidebar-2").attr('style','display: block !important');
                 $(".icon-plus-sidebar-1").attr('style','display: block !important');
                 $(".icon-plus-sidebar-4").attr('style','display: block !important');
                 e.preventDefault();

              } else {  
                 $(".list-data-sidebar-3").slideUp(300);
                 $('.head-search-by-car-site').removeClass('warpper_bar_plus_box_2'); 
                 $('.head-search-by-car-site').addClass('warpper_bar_plus_box');

                $(".icon-minus-sidebar-3").attr('style','display: none !important');    
                $(".icon-plus-sidebar-3").attr('style','display: block !important');

                $(".icon-minus-sidebar-4").attr('style','display: none !important');
                $(".icon-plus-sidebar-4").attr('style','display: block !important');
                e.preventDefault();  
              }
              e.preventDefault();
            }else{
                return false;
            }
        });



        $('.head-search-by-country-site').click(function(e) {    

            var width=$(window).width(); 
            if (width<=1000){

              if ($(this).hasClass('warpper_bar_plus_con')){
                 $(".data-search-by-country").slideDown(300);
                 $('.head-search-by-country-site').removeClass('warpper_bar_plus_con'); 
                 $('.head-search-by-country-site').addClass('warpper_bar_plus_con_2'); 


                $(".list-data-sidebar-1").slideUp(300);
                $(".list-data-sidebar-2").slideUp(300); 
                $(".list-data-sidebar-3").slideUp(300);

                $(".icon-plus-sidebar-4").attr('style','display: none !important'); 
                $(".icon-minus-sidebar-4").attr('style','display: block !important');

                $(".icon-minus-sidebar-3").attr('style','display: none !important');
                $(".icon-plus-sidebar-3").attr('style','display: block !important');

                $(".icon-minus-sidebar-2").attr('style','display: none !important');
                $(".icon-plus-sidebar-2").attr('style','display: block !important');

                $(".icon-minus-sidebar-1").attr('style','display: none !important');
                $(".icon-plus-sidebar-1").attr('style','display: block !important');

              } else {  
                $(".data-search-by-country").slideUp(300);
                 $('.head-search-by-country-site').removeClass('warpper_bar_plus_con_2'); 
                 $('.head-search-by-country-site').addClass('warpper_bar_plus_con');  
                 $(".icon-minus-sidebar-4").attr('style','display: none !important');   

                $(".icon-plus-sidebar-4").attr('style','display: block !important');
                $(".icon-minus-sidebar-3").attr('style','display: none !important');
                $(".icon-plus-sidebar-3").attr('style','display: block !important');

                $(".icon-minus-sidebar-2").attr('style','display: none !important');
                $(".icon-plus-sidebar-2").attr('style','display: block !important');

                $(".icon-minus-sidebar-1").attr('style','display: none !important');
                $(".icon-plus-sidebar-1").attr('style','display: block !important');
              }
                e.preventDefault();
            }else{
                return false;
            }
        });





     $(".icon-plus-sidebar-1").click(function(){
        $(".list-data-sidebar-1").slideDown(300);

        $(".list-data-sidebar-2").slideUp(300); 
        $(".list-data-sidebar-3").slideUp(300); 
        $(".data-search-by-country").slideUp(300);

        $(".icon-plus-sidebar-1").attr('style','display: none !important'); 
        $(".icon-minus-sidebar-1").attr('style','display: block !important');
        $(".icon-plus-sidebar-2").attr('style','display: block !important');
        $(".icon-plus-sidebar-3").attr('style','display: block !important');

        $(".icon-minus-sidebar-4").attr('style','display: none !important');
        $(".icon-plus-sidebar-4").attr('style','display: block !important');
    });


    $(".icon-minus-sidebar-1").click(function(){

        $(".list-data-sidebar-1").slideUp(300);
        $(".data-search-by-country").slideUp(300);

        $(".icon-minus-sidebar-1").attr('style','display: none !important');    
        $(".icon-plus-sidebar-1").attr('style','display: block !important');
        $(".icon-plus--sidebar-2").attr('style','display: block !important');

        $(".icon-minus-sidebar-4").attr('style','display: none !important');
        $(".icon-plus-sidebar-4").attr('style','display: block !important');
        
    });

    $(".icon-plus-sidebar-2").click(function(){
        $(".list-data-sidebar-2").slideDown(300);
    
        $(".list-data-sidebar-1").slideUp(300); 
        $(".list-data-sidebar-3").slideUp(300); 

        $(".data-search-by-country").slideUp(300);

        $(".icon-plus-sidebar-2").attr('style','display: none !important'); 
        $(".icon-minus-sidebar-2").attr('style','display: block !important');
        $(".icon-plus-sidebar-1").attr('style','display: block !important');
        $(".icon-plus-sidebar-3").attr('style','display: block !important');

        $(".icon-minus-sidebar-4").attr('style','display: none !important');
        $(".icon-plus-sidebar-4").attr('style','display: block !important');

    });


    $(".icon-minus-sidebar-2").click(function(){
        $(".list-data-sidebar-2").slideUp(300);
        $(".data-search-by-country").slideUp(300);
        $(".icon-minus-sidebar-2").attr('style','display: none !important');    
        $(".icon-plus-sidebar-2").attr('style','display: block !important');

        $(".icon-minus-sidebar-4").attr('style','display: none !important');
        $(".icon-plus-sidebar-4").attr('style','display: block !important');    
        

    });

    $(".icon-plus-sidebar-3").click(function(){
        $(".list-data-sidebar-3").slideDown(300);
    
        $(".list-data-sidebar-2").slideUp(300);
        $(".list-data-sidebar-1").slideUp(300); 

        $(".data-search-by-country").slideUp(300);

        $(".icon-plus-sidebar-3").attr('style','display: none !important'); 
        $(".icon-minus-sidebar-3").attr('style','display: block !important');
        $(".icon-plus-sidebar-2").attr('style','display: block !important');
        $(".icon-plus-sidebar-1").attr('style','display: block !important');

        $(".icon-minus-sidebar-4").attr('style','display: none !important');
        $(".icon-plus-sidebar-4").attr('style','display: block !important');

    });


    $(".icon-minus-sidebar-3").click(function(){

        $(".list-data-sidebar-3").slideUp(300);

        $(".data-search-by-country").slideUp(300);

        $(".icon-minus-sidebar-3").attr('style','display: none !important');    
        $(".icon-plus-sidebar-3").attr('style','display: block !important');

        $(".icon-minus-sidebar-4").attr('style','display: none !important');
        $(".icon-plus-sidebar-4").attr('style','display: block !important');
        

    });






    $(".icon-plus-sidebar-4").click(function(){
        $(".data-search-by-country").slideDown(300);

        $(".list-data-sidebar-1").slideUp(300);
        $(".list-data-sidebar-2").slideUp(300); 
        $(".list-data-sidebar-3").slideUp(300);

        $(".icon-plus-sidebar-4").attr('style','display: none !important'); 
        $(".icon-minus-sidebar-4").attr('style','display: block !important');

        $(".icon-minus-sidebar-3").attr('style','display: none !important');
        $(".icon-plus-sidebar-3").attr('style','display: block !important');

        $(".icon-minus-sidebar-2").attr('style','display: none !important');
        $(".icon-plus-sidebar-2").attr('style','display: block !important');

        $(".icon-minus-sidebar-1").attr('style','display: none !important');
        $(".icon-plus-sidebar-1").attr('style','display: block !important');



    });


    $(".icon-minus-sidebar-4").click(function(){

        $(".data-search-by-country").slideUp(300);

        $(".icon-minus-sidebar-4").attr('style','display: none !important');    
        $(".icon-plus-sidebar-4").attr('style','display: block !important');

        $(".icon-minus-sidebar-3").attr('style','display: none !important');
        $(".icon-plus-sidebar-3").attr('style','display: block !important');

        $(".icon-minus-sidebar-2").attr('style','display: none !important');
        $(".icon-plus-sidebar-2").attr('style','display: block !important');

        $(".icon-minus-sidebar-1").attr('style','display: none !important');
        $(".icon-plus-sidebar-1").attr('style','display: block !important');

        
    });

    
    $(".clicksearch_small").click(function(){
            $(".group_control_search").attr('style','display:none !important');
            $(".warpper_contant_search").attr('style','display:none !important');
            $(".group_control_small").attr('style','display:block !important');
            $(".warpper_contant_search_small").attr('style','display:block !important');
    });





    $(".smallimg").click(function(a){
        a.preventDefault();
        var ID = $(this).attr("a");
        $("img.imgactive").removeClass('imgactive');
        $("img.imgactive_"+ID).addClass('imgactive');

    });     

    // if(getParameterByName('m') =="cardetail"){
	   //  $("#btn-reset").css('margin-top', '15px');
	   //  $("#btn-reset").css('margin-right', '-8px');
	    // function check FOB = ask or car_length or car_width or car_height is empty or port not avaiable
	//     var idx = $("#idx").val();
	//     if(idx==""){
	//       $("#calculate_price :input").prop("disabled", true);
	//     }
	//     var data = {
	//         idx: idx,
	//     }
	//     $.ajax({
	//         type: "POST",
	//         url:  "/?c=user&m=get_destination_port",
	//         data: data,
	//     })
	//     .done(function( data ){
	//       if(data==0){
	//         $("#calculate_price :input").prop("disabled", true);
	//         $("#total_price_description").html("Calculation of this car is not available. Please contact us for total price.");
	//         $("#total_price_description").css('color', 'red');
	//       }
	//     });
	// }
    // check box insurance
    // $('#insurance').change(function() {

    //   if($('#portname :selected').text() !='Select Port'){
      
    //     if($(this).is(":checked")) {
    //       var insurance = 'on';
    //     }else{
    //       var insurance = 'off';
    //     }
    //     if($("#inspection").is(":checked")){
    //       var inspection = 'on';
    //     }else{
    //       var inspection = 'off';
    //     }

    //       var country_to = $("#des_country").val();
    //       var port_name = $("#portname").val();
    //       var idx = $("#idx").val();                
    //       var data = {
    //           country_to: country_to,
    //           port_name: port_name,
    //           idx: idx,
    //           insurance: insurance,
    //           inspection: inspection,
    //       }
    //       $.ajax({
    //           type: "POST",
    //           url: "/?c=user&m=get_total_price",
    //           data: data,
    //       })
    //       .done(function( data ) {
    //              $("#total_price").html('');
    //              $("#total_price").append(data);
    //       });
    //     }
    // });

// check box inspection

    // $('#inspection').change(function() {
    //   if($('#portname :selected').text() !='Select Port'){

    //     if($(this).is(":checked")) {
    //       var inspection = 'on';
    //     }else{
    //       var inspection = 'off';
    //     }
    //     if($("#insurance").is(":checked")){
    //       var insurance = 'on';
    //     }else{
    //       var insurance = 'off';
    //     }

    //       var country_to = $("#destination_country").val();
    //       var port_name = $("#portname").val();
    //       var idx = $("#idx").val();                
    //       var data = {
    //           country_to: country_to,
    //           port_name: port_name,
    //           idx: idx,
    //           insurance: insurance,
    //           inspection: inspection,
    //       }
    //       $.ajax({
    //           type: "POST",
    //           url: "/?c=user&m=get_total_price",
    //           data: data,
    //       })
    //       .done(function( data ) {
    //              $("#total_price").html('');
    //              $("#total_price").append(data);
    //       });
    //     }
    // });
});

(function(d, s, id) {
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) return;
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=432111436966513";
   fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


   /* $(document).on("click","select[name*='carmake']",function() {
    
       $("#carmodel").empty();
       $("#carmodel").append("<option value>Model</option>");


          var car_make=$(this).val();

          var current_url=$('#base_url').val();
          $.getJSON('?c=car_model&m=get_car_model&car_make='+car_make,function(json){
              $("select[name*='carmodel']").html("");
              $( "select[name*='carmodel']").append('<option value="">Model</option>');
            $.each(json,function(i,data_json){

              respone_data="<option title='" + data_json.car_model + "' value='" + data_json.car_model + "'>" + data_json.car_model + "</option>";
              $( "select[name*='carmodel']").append(respone_data);

            });

          });


        });*/


 jQuery('#carminprice').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g,'');
  });

  jQuery('#carmaxrpice').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g,'');
  });

  

// $(document).on("change","select[name*='des_country']",function() {


//    $("#total_price").html('');
//    $("#total_price").html("-");
//    $("#call_center_port").html('');
//    $("#portname").empty();//To reset cities
//    $("#portname").append("<option value=''>Select Port</option>");


//       var country_to=$(this).val();
    
//       var current_url=$('#base_url').val();
//       $.getJSON('?c=car_model&m=get_port_name&country_to='+country_to,function(json){
//           $("select[name*='portname']").html("");
//           $( "select[name*='portname']").append('<option value="">Select Port</option>');
//         $.each(json,function(i,data_json){

//           respone_data="<option title='" + data_json.port_name + "' value='" + data_json.port_name + "'>" + data_json.port_name + "</option>";
//           $( "select[name*='portname']").append(respone_data);

//         });

//       });


//     });


//============================== Start Destination Port at Page Car Lists=============================//
/*$(document).on("change","select[name*='destination_country']",function() {


   if($(this).val()==""){
       $("#port_name").empty();//To reset cities
       $("#port_name").append("<option value=''>Select port</option>");
       $("#port_name").attr('disabled','true');
   }
      var country_to=$(this).val();
      var current_url=$('#base_url').val();
      $.getJSON('?c=car_model&m=get_port_name&country_to='+country_to,function(json){
         
          $("select[name*='port_name']").html("");
          $( "select[name*='port_name']").append('<option value="">Select port</option>');
        $.each(json,function(i,data_json){
        $("#port_name").removeAttr("disabled")
          respone_data="<option title='" + data_json.port_name + "' value='" + data_json.port_name + "'>" + data_json.port_name + "</option>";
          $( "select[name*='port_name']").append(respone_data);


        });

      });


    });*/

//============================== End Destination Port =============================//

// $(document).on("change","select[name*='portname']",function() {
//   if($("#inspection").is(":checked")) {
//     var inspection = 'on';
//   }else{
//     var inspection = 'off';
//   }
//   if($("#insurance").is(":checked")){
//     var insurance = 'on';
//   }else{
//     var insurance = 'off';
//   }
//   var country_to = $("#des_country").val();
//   var port_name = $("#portname").val();
//   var idx = $("#idx").val();

//   if($('#portname :selected').text() =='Select Port'){

//     $("#total_price").html('-');
//     $("#call_center_port").html('');

//   }else{

//   var data = {
//       country_to: country_to,
//       port_name: port_name,
//       idx: idx,
//       inspection: inspection,
//       insurance: insurance,
//   }
//   $.ajax({
//       type: "POST",
//       url: "/?c=user&m=get_total_price",
//       data: data,
//   })
//   .done(function( data ) {
//           $("#total_price").html('');
//           $("#total_price").append(data);
//           $("#call_center_port").html("Port: "+ $("#des_country option:selected").text() +", "+ port_name);
//   });

//   }

// });


function btn_reset() {
  document.getElementById("frm_total_price").reset();
  $("#portname").html("<option value=''>Select Port</option>");
  $("#total_price").html('');
  $("#total_price").html("-");
  $("#call_center_port").html('');
}


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

