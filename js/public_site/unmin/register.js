
$(document).ready(function(){



// email

        $('.register_email').change(function () {
            var email = $(this).val();
            if (!email.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,10})+$/)) {
                $('.error_email').html('Invalid Email Address');
                $('.error_requried').html("");
                return false;
            }
            else
            {
                $.ajax({
                    url: '?c=admin&m=checkemail',
                    type: "POST",
                    async: true,
                    data: {
                        'id': email
                    },
                    success: function (result)
                    {
                        if (result == 1)
                        {
                            $('.error_email').html('This email already exists');
                            $('input[type="submit"]').attr('disabled', 'disabled');
                            return false;
                        }
                        if (result == 0)
                        {
                            $('.error_email').html('');
                            $('.cerror_email').html('');
                            $('input[type="submit"]').removeAttr('disabled');

                        }

                    }
                });
            }
            if (email == '')
            {
                $('.error_email').html('');
                return false;
            }

        });



        $('.register_confirm_email').change(function () {
            var email = $(this).val();
            if (!email.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,10})+$/)) {
                $('.cerror_email').html('Invalid Email Address');
                return false;
            }
            var password = $(".register_email").val();
            var confirmPassword =$(this).val();
                if (password == '')
                {
                    return true;
                }
                else {
                    if (password != confirmPassword) {
                        $('.cerror_email').html("Email does not match!");
                        return false;
                    }
                    else
                    {
                        $('.cerror_email').html("");
                    }
                }

            if (email == '')
            {
                $('.cerror_email').html('');
                return false;
            }

        });


        $(".register_btn_validate").on("click",function(event){
            var email = $(".register_email").val();
            var cemail =$(".register_confirm_email").val();
            
            if(email!="" && cemail!=""){
                if(email != cemail) {
                    $('.cerror_email').html("Email does not match!");
                    return false;
                }else{
                    if (!email.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,10})+$/)) {
                            $('.error_requried').html('');
                            return false;
                        } 
                     $(".register_btn_search").trigger("click");
                }
            }else{
                $('.error_requried').html("* All fields are required !");
                return false;
            }


        });

         $(".register_email,.register_confirm_email").on("change",function(event){
            var password = $(".register_email").val();
            if(password!=""){
                $('.error_requried').html("");
            }else{
                $('.error_requried').html("* All fields are required !");
                return false;
            }


        });



//email

});
