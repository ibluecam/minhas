  function isNumberKey(evt){
    var charCode = evt.keyCode;
    
	if(evt.ctrlKey == true)
	{
	if(charCode == 67 || charCode == 86)
	{
	 return true;
	}
	}
	if (charCode == 8 || //backspace
                charCode == 46 || //delete
                charCode == 13)   //enter key
    {
        return true;
    }
    else if (charCode >= 37 && charCode <= 40) //arrow keys
    {
        return true;
    }
	
    else if (charCode >= 48 && charCode <= 57) //0-9 on key pad
    {
        if (evt.shiftKey == true)
            return false;

        return true;
    }
    else if (charCode >= 96 && charCode <= 105) //0-9 on num pad
    {
        if (evt.shiftKey == true)
            return false;

        return true;
    }
    else
        return false;
  }