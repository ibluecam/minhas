 $(".buyer-mobile-number, .seller-mobile-number").intlTelInput({
        allowExtensions: true,
        autoFormat: true,
        autoHideDialCode: false,
        autoPlaceholder: false,
        defaultCountry: "auto",
        ipinfoToken: "yolo",
        nationalMode: false,
        numberType: "MOBILE",
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        preferredCountries: ['kr', 'jp'],
        utilsScript: "/lib/libphonenumber/build/utils.js"
      });
      
      // $(document).ready(function(e){
      //       alert();
      // });

      function getSelectedBuyerCc(){
        var c = $(".buyer-mobile-number").intlTelInput("getSelectedCountryData");
        return c['iso2'];
      }
      
      function getSelectedSellerCc(){
        var c = $(".seller-mobile-number").intlTelInput("getSelectedCountryData");
        return c['iso2'];
      }
      
      $(function() {
       $("#txtid").focus();
     });
      
      