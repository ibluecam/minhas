$(document).ready(function(){var cmatch=!1;var cformat=!1;var valid=!1;var typingTimer;var doneTypingInterval=900;$('#forgotemail').on('input',function(){clearTimeout(typingTimer);if($('#forgotemail').val){typingTimer=setTimeout(function(){var email=$('#forgotemail').val();if(!email.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,10})+$/)){$('.error_email').html('Invalid Email Address');return !1}else{$('.error_email').html("")}},doneTypingInterval)}});$("#frm_submit").on("submit",function(event){var email=$("#forgotemail").val();if(email==""){$('.error_email').html("Email is requried!");return !1}
if(!email.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,10})+$/)){$('.error_email').html('Invalid Email Address');return !1}});$("#btn_validate").on("click",function(event){var email=$("#input_password").val();var cemail=$("#input_cpassword").val();if(valid==!1){if(email==""){$('.error_password_1').html("Password is requried!")}
if(cemail==""){$('.error_cpassword_1').html("Confirm password is requried!")}
return !1}
$("#submit_reset_password").trigger("click")});function checkPasswordformat(){var password=$("#input_password").val();var confirmPassword=$("#input_cpassword").val();if(password.length>=6&&password.length<=20){$('.error_password_1').html("");if(confirmPassword==""){$('.error_cpassword_1').html("")}else{cmatch=checkPasswordMatch()}
return !0}else{if(confirmPassword!=""){checkPasswordMatch()}
$('.error_password_1').html('Password must be length 6-20 characters!')}}
function checkPasswordMatch(){var password=$("#input_password").val();var confirmPassword=$("#input_cpassword").val();if(password==''&&confirmPassword==''){return !1}else{if(password!=confirmPassword){$('.error_cpassword_1').html("Password does not match!")}
else{$('.error_cpassword_1').html("");return !0}}}
function disable_next(){valid=!1}
function enable_next(){valid=!0}
function validateAll(){if(cformat==!0&&cmatch==!0){enable_next()}else{disable_next()}}
$('#input_password').on('input',function(event){cformat=checkPasswordformat();validateAll()});$('#input_cpassword').on('input',function(event){cmatch=checkPasswordMatch();validateAll()})})