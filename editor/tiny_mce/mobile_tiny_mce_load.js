// insung cho

function tiny_mce_load(elements, height, width) {



	if(typeof(height) == 'undefined') {

		height = "100%";

	}

	if(typeof(width) == 'undefined') {

		width = "100%";

	}



	tinyMCE.init({

		mode : "textareas",
	    theme : "simple",
	    theme_advanced_buttons1 : "bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,undo,redo,link,unlink",
	    theme_advanced_buttons2 : "",
	    theme_advanced_buttons3 : "",
	    theme_advanced_toolbar_location : "top",
	    theme_advanced_toolbar_align : "left",
	    theme_advanced_statusbar_location : "bottom",
	    plugins : 'inlinepopups',
	    setup : function(ed) {
	        // Add a custom button
	        ed.addButton('mybutton', {
	            title : 'My button',
	            image : 'img/example.gif',
	            onclick : function() {
	                // Add you own code to execute something on click
	                ed.focus();
	                ed.selection.setContent('Hello world!');
	            }
	        });
	    }

	});

}