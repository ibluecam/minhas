<script type="text/javascript">
  function loadContent() {
    var attachments = {};
    attachments['image'] = [];
      <?if(stristr($rows->contents,'tiny_mce') && stristr($rows->contents,'daumeditor')){?>
      attachments['image'].push({
      'attacher': 'image',
      'data': {
        'imageurl': '<?=$dir2.$image_src[1]?>',
        'filename': '<?=$image_src[1]?>',
        'filesize': <?=$file_size?>,
        'imagealign': 'C',
        'originalurl': '<?=$dir2.$thumbnail[0].".".$extension[1]?>',
        'thumburl': '<?=$dir2.$thumbnail[0]."_150.".$extension[1]?>'
      }
    });
      <? }else{ ?>
      attachments['image'].push({
      'attacher': 'image',
      'data': {
        'imageurl': '',
        'filename': '',
        'filesize': 0,
        'imagealign': '',
        'originalurl': '',
        'thumburl': ''
      }
    });
    <? } ?>
    attachments['file'] = [];
    attachments['file'].push({
      'attacher': 'file',
      'data': {
        'attachurl': '',
        'filemime': '',
        'filename': '',
        'filesize': 0
      }
    });
    /* 저장된 컨텐츠를 불러오기 위한 함수 호출 */
    Editor.modify({
      "attachments": function () { /* 저장된 첨부가 있을 경우 배열로 넘김, 위의 부분을 수정하고 아래 부분은 수정없이 사용 */
        var allattachments = [];
        for (var i in attachments) {
          allattachments = allattachments.concat(attachments[i]);
        }
        return allattachments;
      }(),
      "content": document.getElementById("sample_contents_source") /* 내용 문자열, 주어진 필드(textarea) 엘리먼트 */
    });
  }
  loadContent();
</script>
