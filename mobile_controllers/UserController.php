<?php 
include("Controller.php");
include("../mobile_models/User.php");
include("../mobile_models/Car.php");
include("../mobile_models/Conversation.php");
class UserController extends Controller{
	public function getCountNewConversationMessages(){
		/*********START FOR Authentication***********/
		if(isset($_POST['member_no'])) $member_no = $_POST['member_no']; else $member_no = '';
		if(isset($_POST['remember_token'])) $remember_token = $_POST['remember_token']; else $remember_token = '';
		/*********END FOR Authentication***********/
		if(isset($_POST['receiver_no'])) $receiver_no = $_POST['receiver_no']; else $receiver_no = '';
		
		if(User::authenticated($member_no, $remember_token)){
			$new_count = Conversation::countNewConversationMessages($member_no, $receiver_no);

			if(isset($new_count)){
				$this->setData("success", 1);
				$this->setData("new_count", $new_count);
			}else{
				$this->setData("success", 0);
				$this->setData("error_msg", "Cannot get the number of new conversation messages");
			}
		}else{
			$this->setData("success", 0);
			$this->setData("error_msg", "Authentication failed! Incorrect Member ID or Password");
		}
	}
	public function postCancelCarOrder(){
		$where = array();
		$result=array();
		$valid = true;
		/*********START FOR Authentication***********/
		if(isset($_POST['member_no'])) $member_no = $_POST['member_no']; else $member_no = '';
		if(isset($_POST['remember_token'])) $remember_token = $_POST['remember_token']; else $remember_token = '';
		/*********END FOR Authentication***********/
		if(isset($_POST['idx'])) $where['stock_idx'] = $_POST['idx']; 
		if(isset($_POST['member_no'])) $where['customer_no'] = $_POST['member_no'];
		$countWhere['idx'] = $where['stock_idx'];
		if(User::authenticated($member_no, $remember_token)){
			if(Car::countCar($countWhere)<=0){
				$valid = false;
				$this->setData("msg", "The car doesn't exist");
			}elseif(Car::countOrderedCar($where)<=0){
				/****User has already ordered this car****/
				$valid = false;
				$this->setData("msg", 'Unable to cancel car order. You have not ordered this car');
			}
			/*****Check if validation is correct*****/
			if($valid){
				if(Car::cancelCarOrder($where)){
					$this->setData("success", 1);
					$this->setData("msg", 'Your car order cancellation is successful');
				}else{
					$this->setData("success", 0);
					$this->setData("msg", 'Unable to cancel car order! Unknown error occurred');
				}
			}else{
				$this->setData("success", 0);
			}

		}else{
			$this->setData("success", 0);
			$this->setData("msg", 'Authentication failed');
		}
	}
	public function getOrderedCarList(){
		
		if(isset($_POST['member_no'])) $member_no = $_POST['member_no']; else $member_no = '';
		if(isset($_GET['limit_s'])) $limit_s = $_GET['limit_s']; else $limit_s = 0;
		if(isset($_POST['remember_token'])) $remember_token = $_POST['remember_token']; else $remember_token = '';

		$where['iw_customer_inventory.customer_no'] = $member_no;

		if(User::authenticated($member_no, $remember_token)){
			$cars = Car::getOrderedCar($where, $limit_s);
			$this->setData("success", 1);
			$this->setData("cars", $cars);
		}else{
			$this->setData("success", 0);
		}
	}
	public function postOrderCar(){
		$data = array();
		$result=array();
		$valid = true;
		/*********START FOR Authentication***********/
		if(isset($_POST['member_no'])) $member_no = $_POST['member_no']; else $member_no = '';
		if(isset($_POST['remember_token'])) $remember_token = $_POST['remember_token']; else $remember_token = '';
		/*********END FOR Authentication***********/
		if(isset($_POST['idx'])) $data['stock_idx'] = $_POST['idx']; 
		if(isset($_POST['member_no'])) $data['customer_no'] = $_POST['member_no'];
		$countWhere['idx'] = $data['stock_idx'];
		if(User::authenticated($member_no, $remember_token)){
			if(Car::isReserved($data['stock_idx'])){
				/*****This car has already been reserved*****/
				$valid = false;
				$this->setData("msg", 'Unable to order this car! The car has already been reserved');
			}elseif(Car::countOrderedCar($data)>0){
				/****User has already ordered this car****/
				$valid = false;
				$this->setData("msg", 'Unable to order car again! You have already ordered this car');
			}elseif(Car::countCar($countWhere)<=0){
				$valid = false;
				$this->setData("msg", "The car doesn't exist");
			}
			/*****Check if validation is correct*****/
			if($valid){
				if(Car::orderCar($data)){
					$this->setData("success", 1);
					$this->setData("msg", 'Your order is successful! We will contact you soon. Thank you!');
				}else{
					$this->setData("success", 0);
					$this->setData("msg", 'Unable to order car! Unknown error occurred.');
				}
			}else{
				$this->setData("success", 0);
			}

		}else{
			$this->setData("success", 0);
			$this->setData("msg", 'Authentication failed');
		}
	}
	public function getUserDetail(){
		/*********START FOR Authentication***********/
		if(isset($_POST['member_no'])) $member_no = $_POST['member_no']; else $member_no = '';
		if(isset($_POST['remember_token'])) $remember_token = $_POST['remember_token']; else $remember_token = '';
		/*********END FOR Authentication***********/
		$user_where['member_no'] = $member_no;
		if(User::authenticated($member_no, $remember_token)){
			$users = User::getUser($user_where);
			if(count($users)>0){
				$this->setData("success", 1);
				$this->setData("user_info", $users['0']);
			}else{
				$this->setData("success", 0);
				$this->setData("error_msg", "User not found.");
			}
		}else{
			$this->setData("success", 0);
			$this->setData("error_msg", "Authentication failed! Incorrect Member ID or Password");
		}
		
	}
	public function postReservedCar(){
		$data = array();
		$result=array();
		if(isset($_POST['idx'])) $data['stock_idx'] = $_POST['idx'];
		 
		$countWhere['idx'] = $data['stock_idx'];
		if(Car::isOrdered($data['stock_idx'])){
			/*****This Car Reserved by other*****/
			$this->setData("msg", 'This Car Reserved by other');
		}else{
			$this->setData("success", 0);
		}
	}	

	public function getUserlist(){
		/*********START FOR Authentication***********/
		if(isset($_POST['member_no'])) $member_no = $_POST['member_no']; else $member_no = '';
		if(isset($_POST['remember_token'])) $remember_token = $_POST['remember_token']; else $remember_token = '';
		/*********END FOR Authentication***********/
		
		//$user_where['grade_no'] = 10;
		if(User::authenticated($member_no, $remember_token)){
			$users = Conversation::getUsers($member_no, $remember_token);
			if(count($users)>0){
				$this->setData("success", 1);
				$this->setData("user_info", $users);
			}else{
				$this->setData("success", 0);
				$this->setData("error_msg", "Users not found.");
			}
		}else{
			$this->setData("success", 0);
			$this->setData("error_msg", "Authentication failed! Incorrect Member ID or Password");
		}
	}

	/***GET ALL CONVERSATION MESSAGES BY receiver_no***/
	public function getConversationMessages(){
		/*********START FOR Authentication***********/
		if(isset($_POST['member_no'])) $member_no = $_POST['member_no']; else $member_no = '';
		if(isset($_POST['remember_token'])) $remember_token = $_POST['remember_token']; else $remember_token = '';
		/*********END FOR Authentication***********/
		if(isset($_POST['receiver_no'])) $receiver_no = $_POST['receiver_no']; else $receiver_no = '';
		
		if(User::authenticated($member_no, $remember_token)){
			$messages = Conversation::getConversationMessages($member_no, $receiver_no);
			if(count($messages)>0){
				/*** Mark rows of messages as seen ***/
				Conversation::insertSeenDate($member_no, $receiver_no);
				Conversation::resetNewCount($member_no, $receiver_no);
				$this->setData("success", 1);
				$this->setData("conversation_msgs", $messages);
			}else{
				$this->setData("success", 0);
				$this->setData("error_msg", "No messages");
			}
		}else{
			$this->setData("success", 0);
			$this->setData("error_msg", "Authentication failed! Incorrect Member ID or Password");
		}
	}

	/***GET NEW CONVERSATION MESSAGES BY receiver_no, latest_id ***/
	public function getNewConversationMessages(){
		/*********START FOR Authentication***********/
		if(isset($_POST['member_no'])) $member_no = $_POST['member_no']; else $member_no = '';
		if(isset($_POST['remember_token'])) $remember_token = $_POST['remember_token']; else $remember_token = '';
		/*********END FOR Authentication***********/
		if(isset($_POST['receiver_no'])) $receiver_no = $_POST['receiver_no']; else $receiver_no = '';
		if(isset($_POST['latest_id'])) $latest_id = $_POST['latest_id']; else $latest_id = 0;
		if(User::authenticated($member_no, $remember_token)){
			$messages = Conversation::getNewConversationMessages($member_no, $receiver_no, $latest_id);
			if(count($messages)>0){
				/*** Mark rows of messages as seen ***/
				Conversation::insertSeenDate($member_no, $receiver_no);
				Conversation::resetNewCount($member_no, $receiver_no);
				$this->setData("success", 1);
				$this->setData("conversation_msgs", $messages);
			}else{
				$this->setData("success", 0);
				$this->setData("error_msg", "No messages");
			}
		}else{
			$this->setData("success", 0);
			$this->setData("error_msg", "Authentication failed! Incorrect Member ID or Password");
		}
	}

	/***CREATE NEW CONVERSATION MESSAGE by receiver_no, conversation_text***/
	public function createConversationMessage(){
		/*********START FOR Authentication***********/
		if(isset($_POST['member_no'])) $member_no = $_POST['member_no']; else $member_no = '';
		if(isset($_POST['remember_token'])) $remember_token = $_POST['remember_token']; else $remember_token = '';
		/*********END FOR Authentication***********/
		if(isset($_POST['receiver_no'])) $receiver_no = $_POST['receiver_no']; else $receiver_no = '';
		if(isset($_POST['conversation_text'])) $conversation_text = $_POST['conversation_text']; else $conversation_text = '';
		if(User::authenticated($member_no, $remember_token)){
			$result = Conversation::createConversationMessage($member_no, $receiver_no, $conversation_text);
			if($result){
				$this->setData("success", 1);
				Conversation::insertNewCount($member_no, $receiver_no);
			}else{
				$this->setData("success", 0);
				$this->setData("error_msg", "No messages");
			}
		}else{
			$this->setData("success", 0);
			$this->setData("error_msg", "Authentication failed! Incorrect Member ID or Password");
		}
	}
}

?>