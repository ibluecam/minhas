<?php 
include("Controller.php");
include("../mobile_models/User.php");

class PublicController extends Controller{

	public function postRegister(){
		$data = $_POST;
		$fillable = array(
			'member_id',
			'member_pwd',
			'email',
			'member_first_name',
			'member_last_name', 
			'member_country',
			'mobile_no1',
			'mobile_no2'
			);
		foreach($data as $key=>$value){
			if(!in_array($key, $fillable)){
				unset($data[$key]);
			}
		}

		$validation = User::validateRegister($data);
		if($validation['success']==1){
			/****Insert user data to database*****/
			
			if(User::registerUser($data)){
				$result['success']=1;
			}else{
				$result['success']=0;
			}
		}else{
			$result['success']=0;
		}

		$result = $validation;
		
		$this->setData("success", $result['success']);
		unset($result['success']);
		foreach($result as $key => $value){
			$this->setData($key, $value);
		}
		
	}
	public function postLogin(){
		$result=array();
		$rows = User::login($_POST, true);

		if(count($rows)>0){
			$success=1;
			$result['member_no']=$rows->member_no;
			$result['remember_token']=$rows->remember_token;
			$result['member_id']=$rows->member_id;
			$result['member_first_name']=$rows->member_first_name;
			$result['member_last_name']=$rows->member_last_name;
			$result['email']=$rows->email;
			$result['member_country']=$rows->member_country;
		}else{
			$success=0;
			$result['message']="Login failed! Incorrect User ID or Password";
		}
		$this->setData("success", $success);
		$this->setData("user", $result);

	}
	 
	
}

?>