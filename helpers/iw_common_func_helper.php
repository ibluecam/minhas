<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/* ------------------------------------------------
 * 파일명 : iw_common_func.php
 * 개  요 : 공통함수
  ------------------------------------------------ */

function func_get_nav($nav_arr){
	$html = '<div class="nav_bar">';
	foreach ($nav_arr as $key => $value) {
		$nav[] =  "<a href='".$value."'>".$key."</a>";
	}
	
	$html .= implode(' > ', $nav);
	$html .= '</div>';
	return $html;
}
//DB iw_config에서 해당 키의 값을 가져온다.
function func_get_config($db_result, $key, $re_value = '') {
	$value = '';
	if (count($db_result) > 0) {
		foreach ($db_result as $rows) {
			if ($rows->key == $key) {
				$value = $rows->value;
			}
		}
	}

//해당 값이 없이면 $re_value로 대체
	if ($value == '') {
		$value = $re_value;
	}

	return $value;
}

// validate checkbox

function func_get_post_array_checkbox($post_arr){
	$country_values= "";
		foreach ($post_arr as $selected){
			if($country_values != ""){
				$country_values .= "^".$selected;	
			}else{
				$country_values = $selected;
			}
		}
	return $country_values;
}

//POST 배열을 array['key'] = value 형태로 리턴
function func_get_post_array($post_arr) {

	$values = $post_arr;

	//배열의 key 값을 리턴
	$keys = func_get_array_keys($values);

	// var_dump($values);
	// exit;

	$inData = "";
	for ($i = 0; $i < sizeof($keys); $i++) {
		if (is_array($values[$keys[$i]])) {
			$tmpStr = "";
			$tmpArr = $values[$keys[$i]];
			
			for ($j = 0; $j < sizeof($tmpArr); $j++) {
				$tmpStr.= $tmpArr[$j] . "|";
			}
			
			$inData[$keys[$i]] = $tmpStr;
		} else {
			$inData[$keys[$i]] = $values[$keys[$i]];
		}
	}

	return $inData;
}

//배열의 key 값을 리턴
function func_get_array_keys($arr) {
	$keys = '';
	if (count($arr) > 0) {
		$keys = @array_keys($arr);
	}
	return $keys;
}

//배열의 value 값을 리턴
function func_get_array_values($arr) {
	$values = '';
	if (count($arr) > 0) {
		$values = array_values($arr);
	}
	return $values;
}

//HTML 상단 셋팅
function func_setHtmlHeader() {
	$reStr = "";
	$reStr.="<html> \n";
	$reStr.="<head> \n";
	$reStr.="<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n";
	return $reStr;
}

//HTML 하단 세팅
function func_setHtmlFooter() {
	$reStr = "";
	$reStr.="</head> \n";
	$reStr.="<body>  \n";
	$reStr.="</body> \n";
	$reStr.="</html> \n";
	return $reStr;
}

//javascript 상단 셋팅
function func_setJsHeader() {
	return "<script type=\"text/javascript\">\r\n";
}

//javascript 하단 셋팅
function func_setJsFooter() {
	return "</script>\r\n";
}

//javascript alert 함수
function func_jsAlert($msg, $incTag = TRUE) {
	$reStr = '';

	if ($incTag) {
		$reStr = func_setHtmlHeader();
		$reStr .= func_setJsHeader();
	}

	$reStr .= "alert('" . $msg . "');\r\n";

	if ($incTag) {
		$reStr .= func_setJsFooter();
		$reStr .= func_setHtmlFooter();
	}

	return $reStr;
}

//javascript alert & replace 함수
function func_jsAlertReplace($msg, $reUrl, $incTag = TRUE) {
	$reStr = '';

	if ($incTag) {
		$reStr = func_setHtmlHeader();
		$reStr .= func_setJsHeader();
	}

	$reStr .= "alert('" . $msg . "');\r\n";
	$reStr .= "location.replace('" . $reUrl . "');\r\n";

	if ($incTag) {
		$reStr .= func_setJsFooter();
		$reStr .= func_setHtmlFooter();
	}

	return $reStr;
}

function func_jsModalAlert($msg, $reUrl, $incTag = TRUE) {
	$reStr = '';

	if ($incTag) {
		$reStr = func_setHtmlHeader();
		$reStr .= func_setJsHeader();
		$reStr .= "$(document).ready(function(e) { \r\n";
	}

	$reStr .= "modal({ type: 'success', title: 'Success', text: '".$msg."', callback: function(result) {
					location.replace('" . $reUrl . "');
				} } );\r\n";					

	if ($incTag) {
		$reStr .= "}); \r\n";
		$reStr .= func_setJsFooter();
		$reStr .= func_setHtmlFooter();
	}

	return $reStr;
}
//javascript alert & replace 함수
function func_jsAlertParentReplace($msg, $reUrl, $incTag = TRUE) {
	$reStr = '';

	if ($incTag) {
		$reStr = func_setHtmlHeader();
		$reStr .= func_setJsHeader();
	}

	$reStr .= "alert('" . $msg . "');\r\n";
	$reStr .= "parent.location.replace('" . $reUrl . "');\r\n";

	if ($incTag) {
		$reStr .= func_setJsFooter();
		$reStr .= func_setHtmlFooter();
	}

	return $reStr;
}

//javascript alert & history.go() 함수
function func_jsAlertHistoryGo($msg, $history_cnt = '-1', $incTag = TRUE) {
	$reStr = '';

	if ($incTag) {
		$reStr = func_setHtmlHeader();
		$reStr .= func_setJsHeader();
	}

	$reStr .= "alert('" . $msg . "');\r\n";
	$reStr .= "history.go(" . $history_cnt . ");\r\n";

	if ($incTag) {
		$reStr .= func_setJsFooter();
		$reStr .= func_setHtmlFooter();
	}

	return $reStr;
}

//data 파일 셋팅
function func_set_data($thiss, $var, $val) {
	return $thiss->data[$var] = $val;
}

function func_redirect_to_ssl(){
	ob_clean();
	$pageURL = 'https';
    $pageURL .= "://";
    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    if($_SERVER["SERVER_NAME"]=='motorbb.com'){
	    if(isset($_SERVER["HTTPS"])){
	        // if ($_SERVER["HTTPS"] != "on"){
	        //     header("Location: ".$pageURL);
	        // } 
	    }else{
	        //header("Location: ".$pageURL);
	    } 
   	}
}

//암호화
function func_base64_encode($txt) {
	$key = "abcdefghijklmnopqrstuvwxyz";
	$key_len = strlen($key);
	$rot_ptr_set = 9;

	$rot_ptr = $rot_ptr_set;

	$tmp = "";
	$txt = strrev($txt);
	$txt_len = strlen($txt);

	for ($i = 0; $i < $txt_len; $i++) {
		if ($rot_ptr >= $key_len)
			$rot_ptr = 0;
		$tmp .= $txt[$i] ^ $key[$rot_ptr];
		$v = ord($tmp[$i]);
		$tmp[$i] = chr(((($v << 3) & 0xf8) | (($v >> 5) & 0x07)));
		$rot_ptr++;
	}
	$tmp = base64_encode($tmp);
	$tmp = strrev($tmp);
	return $tmp;
}

//복호화
function func_base64_decode($txt) {
	$key = "abcdefghijklmnopqrstuvwxyz";
	$key_len = strlen($key);
	$rot_ptr_set = 9;

	$rot_ptr = $rot_ptr_set;

	$tmp = "";
	$txt = strrev($txt);
	$txt = base64_decode($txt);
	$txt_len = strlen($txt);
	for ($i = 0; $i < $txt_len; $i++) {
		if ($rot_ptr >= $key_len)
			$rot_ptr = 0;
		$v = ord($txt[$i]);
		$txt[$i] = chr(((($v >> 3) & 0x1f) | (($v << 5) & 0xe0)));
		$tmp .= $txt[$i] ^ $key[$rot_ptr];
		$rot_ptr++;
	}

	$tmp = strrev($tmp);
	return $tmp;
}

//세자리마다 콤마 찍기
function func_num_comma($num) {
	if (!is_numeric($num))
		return $num;

	return number_format($num);
}

//값을 비교하여 일치 하면 다른 값을 리턴 한다.
function func_decode($val, $comp, $rep1, $rep2) {
	$tmpStr = explode("|", $comp);
	$reStr = "";

	for ($i = 0; $i < sizeof($tmpStr); $i++) {
		if ($val == $tmpStr[$i]) {
			$reStr = $rep1;
			break;
		} else {
			$reStr = $rep2;
		}
	}

	return $reStr;
}

//문자열을 특정 문자열 길이로 자른다 + (...)
function func_cutstr($msg, $cut_size) {
	$msg = trim(htmlspecialchars_decode(strip_tags($msg)));
	$substr = "";
	if (strlen($msg) == 0)
		return "";

	$substr = mb_substr($msg, 0, $cut_size, 'UTF-8');

	if (strlen($msg) / 2 > $cut_size) {
		return $substr . "<font size=\"1\">..</font>";
	}

	return $substr;
}

//자식글 들여쓰기
function funct_level_str($level, $menu_code = '', $image_name = '') {
	$levelStr = "";
	$img = "";
	if ($image_name == '') {
		$img = "<img src=\"/images/common/bbs/icon_reply.gif\" border=\"0\" alt=\"reply\" />";
	} else {
		$img = "<img src=\"/views/user/bbs/" . $menu_code . "/images/" . $image_name . "\" border=\"0\" alt=\"reply\" />";
	}

	for ($i = 0; $i < $level; $i++) {
		$levelStr.="&nbsp;&nbsp;";
	}

	if ($levelStr != "")
		$levelStr.= $img;

	return $levelStr;
}

//Secret 아이콘 표시
function func_secret_icon($menu_code, $image_name = '') {
	if ($image_name == '') {
		return "<img src=\"/images/common/bbs/icon_secret.gif\" border=\"0\" alt=\"secret\" align=\"absmiddle\" />&nbsp;";
	} else {
		return "<img src=\"/views/user/bbs/" . $menu_code . "/images/" . $image_name . "\" border=\"0\" alt=\"secret\" align=\"absmiddle\" />&nbsp;";
	}
}

//New아이콘 표시
function func_new_icon($conf, $created_dt, $menu_code = '', $image_name = '') {

	$gap = (integer) $conf;
	$img = '';
	if ($image_name == '') {
		$img = "<img class=\"icon_new\" src=\"/images/user/new_icon.gif\" border=\"0\" alt=\"NEW\" align=\"absmiddle\" />";
	} else {
		if(substr($image_name, 0, 1) == '/')
		{
			$img = '<img src="'. $image_name . '" border="0" alt="NEW" align="absmiddle" />';
		}
		else
		{
			$img = "<img src=\"/views/user/bbs/" . $menu_code . "/images/" . $image_name . "\" border=\"0\" alt=\"NEW\" align=\"absmiddle\" />";
		}
	}

	if ((time() - strtotime($created_dt)) < (60 * 60 * 24 * $gap))
	{
		return $img;
	}
	else
	{
		return '';
	}
}

//Hot 아이콘 표시
function func_hot_icon($conf, $visit_cnt, $menu_code = '', $image_name = '') {
	$gap = (integer) $conf;
	$img = "";
	if ($image_name == '') {
		$img = "<img src=\"/images/common/bbs/icon_hot.gif\" border=\"0\" alt=\"HOT\" align=\"absmiddle\" />";
	} else {
		$img = "<img src=\"/views/user/bbs/" . $menu_code . "/images/" . $image_name . "\" border=\"0\" alt=\"HOT\" align=\"absmiddle\" />";
	}
	if ((integer) $visit_cnt >= $gap) {
		return $img;
	}
}

//현재 날짜 + 시간 정보를 리턴 한다.
function func_get_date_time() {
	return date("Y-m-d H:i:s");
}

//날짜 포맷으로 설정
function func_get_date_format($dt = NULL, $format) {
	if ($dt == NULL)
		return "--";
	$time = strtotime($dt);
	return date($format, $time);
}

//설정에 의한 날짜 포맷으로 설정
function func_get_config_date($conf, $dt = NULL) {
	if ($dt == NULL)
		return "--";
	$datestring = $conf;
	$time = strtotime($dt);
	return date($datestring, $time);
}

//해당 포맷으로 현재 날짜 설정
function func_get_today($format = '') {
	if ($format != '') {
		return date($format, time());
	} else {
		return date('Y-m-d H:i:s', time());
	}
}

//날짜를 더한다(시 분 초)
function func_add_date_time($givendate, $day = 0, $mth = 0, $yr = 0) {
	$cd = strtotime($givendate);
	$newdate = date('Y-m-d h:i:s', mktime(date('h', $cd), date('i', $cd), date('s', $cd), date('m', $cd) + $mth, date('d', $cd) + $day, date('Y', $cd) + $yr));
	return $newdate;
}

//날짜를 더한다
function func_add_date($orgDate, $mth) {
	$cd = strtotime($orgDate);
	$retDAY = date('Y-m-d', mktime(0, 0, 0, date('m', $cd) + $mth, date('d', $cd), date('Y', $cd)));
	return $retDAY;
}

//날짜 차이를 구한다.
function func_date_diff($start, $end) {
	return intval(( strtotime($start) - strtotime(func_get_today($end)) ) / 86400);
}

//해당 날짜 시작일과 마지막일을 구한다.(주간)
function func_get_week($inputdate) {
	$yoil = date('w', strtotime($inputdate));
	if ($yoil == '0') {
		$startdate = date("Y-m-d", strtotime($inputdate));
	} else {
		$startdate = date("Y-m-d", strtotime("last Sunday", strtotime($inputdate)));
	}

	if ($yoil == '6') {
		$enddate = date("Y-m-d", strtotime($inputdate));
	} else {
		$enddate = date("Y-m-d", strtotime("next saturday", strtotime($inputdate)));
	}
}

//페이지정보 리턴
function func_get_page_info($rowCount, $viewPage, $viewRows, $localPage) {
	//viewRows가 0이면 전체 페이지이다
	if ($viewRows == 0)
		return NULL;
	if ($localPage == "")
		$localPage = 1;

	$totPage = 0;  // 전체페이지수
	$startPage = 0;  // 시작페이지 번호
	$endPage = 0;  // 끝페이지번호
	$prevPage = 0;  // 이전페이지 번호
	$nextPage = 0;  // 다음페이지 번호
	$startIdx = 0;  // 조회시작Idx
	$endIdx = 0;  // 조회끝 idx

	// var_dump($rowCount);
	// exit;


	// 페이지 수 = 게시글 수 / 페이지당 개시글 수
	$totPage = (int) ($rowCount / $viewRows);

	// var_dump($totPage);
	// exit;
	
	if ($totPage < 1)
		$totPage = 1;
	else
	if (($rowCount % $viewRows) > 0)
		$totPage++;
	
	// 현제페이지가 전체페이지수보다 크면 전체페이지수를 현제페이지수로 지정
	if ($totPage < $localPage) {
		if ($totPage != 0)
			$localPage = $totPage;
		else
			$localPage = 1;
	}
	
	// 시작페이지번호
	$startPage = (((int) (($localPage - 1) / $viewPage)) * $viewPage);
	if ($startPage < 1)
		$startPage = 0;
	$startPage++;
	
	// 끝페이지번호
	$endPage = ($startPage + $viewPage - 1);
	if ($endPage > $totPage)
		$endPage = $totPage;
	
	// 이전페이지번호
	$prevPage = $startPage - $viewPage;
	if ($prevPage < 1)
		$prevPage = -1;
	
	// 다음페이지번호
	$nextPage = $startPage + $viewPage;
	if ($nextPage > $totPage)
		$nextPage = -1;
	
	// 조회시작idx
	$startIdx = ($localPage - 1) * $viewRows + 1;
	
	// 조회끝 idx
	$endIdx = ($startIdx + $viewRows) - 1;
	
	$pageArr = array($totPage, $startPage, $endPage, $prevPage, $nextPage, $startIdx, $endIdx, $localPage, $viewRows, $rowCount);
	
	//pageTemp[0] = $totPage;   // 전체페이지수
	//pageTemp[1] = $startPage; // 시작페이지번호
	//pageTemp[2] = $endPage;   // 끝페이지번호
	//pageTemp[3] = $prevPage;  // 이전페이지번호
	//pageTemp[4] = $nextPage;  // 다음페이지번호
	//pageTemp[5] = $startIdx;  // 조회시작idx
	//pageTemp[6] = $endIdx;	// 조회끝 idx
	//pageTemp[7] = $localPage; // 현재페이지
	//pageTemp[8] = $viewRows;  // 보여줄 로우수
	//pageTemp[9] = $rowCount;  // 전체데이타수
	
	return $pageArr;
}

	//페이지정보 리턴
	function func_wirte_page_info($pageInfo, $sParam, $sUrl, $skin_folder = NULL, $fa = NULL, $pa = NULL, $na = NULL, $la = NULL) {
	if ($pageInfo == NULL)
		return;

	//$fa = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_first.gif\" border=\"0\" alt=\"처음으로\" />";
	//$pa = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_prev.gif\" border=\"0\" alt=\"이전\" />";
	//$na = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_next.gif\" border=\"0\" alt=\"다음\" />";
	//$la = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_last.gif\" border=\"0\" alt=\"마지막\" />";


	if ($fa == NULL || $fa == "") {
		$fa = "<img src=\"/images/common/bbs/arrow_first.gif\" border=\"0\" alt=\"처음으로\" />";
	} else {
		$fa = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $fa . "\" border=\"0\" alt=\"처음으로\" />";
	}
	if ($pa == NULL || $pa == "") {
		$pa = "<img src=\"/images/common/bbs/arrow_prev.gif\" border=\"0\" alt=\"이전\" />";
	} else {
		$pa = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $pa . "\" border=\"0\" alt=\"이전\" />";
	}
	if ($na == NULL || $na == "") {
		$na = "<img src=\"/images/common/bbs/arrow_next.gif\" border=\"0\" alt=\"다음\" />";
	} else {
		$na = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $na . "\" border=\"0\" alt=\"다음\" />";
	}
	if ($la == NULL || $la == "") {
		$la = "<img src=\"/images/common/bbs/arrow_last.gif\" border=\"0\" alt=\"마지막\" />";
	} else {
		$la = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $la . "\" border=\"0\" alt=\"마지막\" />";
	}

	//var_dump($sParam);

	$sParam = func_sPUrlEncode($sParam, "sch_word");

	//var_dump($sParam);
	//exit;

	$tmpStr = "";

	if ($pageInfo[3] == -1) {

	} else {
		$tmpStr.= "<a href=\"$sUrl&cur_page=1$sParam\">" . $fa . "</a><a href=\"$sUrl&cur_page=$pageInfo[3]$sParam\">" . $pa . "</a>&nbsp;";
	}
	for ($i = 0; $i <= ($pageInfo[2] - $pageInfo[1]); $i++) {
		if (($pageInfo[1] + $i) == $pageInfo[7]) {	// 현제 페이지
			$pages = $pageInfo[1] + $i;
			$tmpStr.="<span style=\"color:red;\">$pages&nbsp;</span>";
		} else {
			$pages = $pageInfo[1] + $i;
			$tmpStr.="<a href=\"$sUrl&cur_page=$pages$sParam\">$pages&nbsp;</a>";
		}
	}

	if ($pageInfo[4] == -1) {

	} else {
		$tmpStr.="<a href=\"$sUrl&cur_page=$pageInfo[4]$sParam\">" . $na . "</a><a href=\"$sUrl&cur_page=$pageInfo[0]$sParam\" title=\"다음\">" . $la . "</a>";
	}

	return $tmpStr;
}

/* * *****************************************************************
  - sParam 중 sBbsWord value값만 urlencode를 위해(한글 파라미터 때문)
 * ****************************************************************** */

function func_sPUrlEncode($sParam, $param) {
	$tmpSParam = explode("&", $sParam);

	$val = "";

	for ($i = 1; $i <= count($tmpSParam) - 1; $i++) {
		if (strstr($tmpSParam[$i], $param)) {
			$tmp = explode("=", $tmpSParam[$i]);

			$tmpSParam[$i] = $tmp[0] . "=" . urlencode($tmp[1]);

			$val .= "&" . $tmpSParam[$i];
		} else {
			$val .= "&" . $tmpSParam[$i];
		}
	}

	return $val;
}

/* * ***********************************
  HTML코드로 파싱한다.
 * **************************************** */

function func_html($str) {

	$str = str_replace("&quot;", "\"", $str);
	$str = str_replace("\\'", "'", $str);
	$str = str_replace("&lt;", "<", $str);
	$str = str_replace("&gt;", ">", $str);
	$str = str_replace("&amp;", "&", $str);
	$str = str_replace("\n", "<br/>", $str);

	return $str;
}

//첨부파일 다운로드 String 생성
function func_create_attach_files($bbs_attach_list, $mcd, $del_flag = 'N', $line = 'Y', $image_name = '') {
	$attach_str = '';
	$download_link = '';
	$file_icon = '';

	if (count($bbs_attach_list) > 0) {
		foreach ($bbs_attach_list as $rows) {
			//삭제 아이콘 표시 여부
			if ($del_flag != 'Y') {
				$download_link = "<a href=\"/?c=common&m=download&file_name=$rows->file_name&orig_name=" . urlencode($rows->orig_name) . "&mcd=$mcd\" title=\"파일다운로드\">";
				$file_icon = "<img src=\"/images/common/fileicon/file_" . str_replace(".", "", strtolower($rows->file_ext)) . ".gif\" border=\"0\" align=\"absmiddle\" onerror=\"this.src='/images/common/fileicon/unknown.gif'\" />";
				//줄바꿈 여부
				if ($line != 'N') {
					$attach_str .= $download_link . $file_icon . $rows->orig_name . '&nbsp;(' . $rows->file_size . ')</a><br/>';
				} else {
					$attach_str .= $download_link . $file_icon . $rows->orig_name . '&nbsp;(' . $rows->file_size . ')</a>&nbsp;';
				}
			} else {
				$download_link = "<a href=\"/?c=common&m=download&file_name=$rows->file_name&orig_name=" . urlencode($rows->orig_name) . "&mcd=$mcd\" title=\"파일다운로드\">";
				$file_icon = "<img src=\"/images/common/fileicon/file_" . str_replace(".", "", strtolower($rows->file_ext)) . ".gif\" border=\"0\" align=\"absmiddle\" onerror=\"this.src='/images/common/fileicon/unknown.gif'\" />";
				$attach_str .= $download_link . $file_icon . $rows->orig_name . '&nbsp;(' . $rows->file_size . ')</a>';
				//줄바꿈 여부
				if ($line != 'N') {
					//앨범형 스킨인 경우
					if ($image_name != '') {
						$attach_str .= "&nbsp;<a href=\"#\" onclick=\"delete_bbs_attach('$rows->att_idx'); return false;\" title=\"삭제\"><img src=\"/views/user/bbs/" . $mcd . "/images/" . $image_name . "\" align=\"absmiddle\" alt=\"삭제\" /></a><br/>";
					} else {
						$attach_str .= "&nbsp;<a href=\"#\" onclick=\"delete_bbs_attach('$rows->att_idx'); return false;\" title=\"삭제\"><img src=\"/images/common/bbs/delete_icon.gif\" align=\"absmiddle\" alt=\"삭제\" /></a><br/>";
					}
				} else {
					//앨범형 스킨인 경우
					if ($image_name != '') {
						$attach_str .= "&nbsp;<a href=\"#\" onclick=\"delete_bbs_attach('$rows->att_idx'); return false;\" title=\"삭제\"><img src=\"/views/user/bbs/" . $mcd . "/images/" . $image_name . "\" align=\"absmiddle\" alt=\"삭제\" /></a>&nbsp;";
					} else {
						$attach_str .= "&nbsp;<a href=\"#\" onclick=\"delete_bbs_attach('$rows->att_idx'); return false;\" title=\"삭제\"><img src=\"/images/common/bbs/delete_icon.gif\" align=\"absmiddle\" alt=\"삭제\" /></a>&nbsp;";
					}
				}
			}
		}
	} else {
		if ($del_flag != 'Y')
			$attach_str = '--';
		else
			$attach_str = '';
	}

	return $attach_str;
}

//create attach file by name
function func_create_attach_files_name($bbs_attach_list, $mcd, $del_flag = 'N', $line = 'Y', $image_name = '',$logo= '',$where = '',$del_yn = 'Y') {
	$attach_str = '';
	$download_link = '';
	$file_icon = '';
	
	if (count($bbs_attach_list) > 0) {
		foreach ($bbs_attach_list as $rows) {
			//삭제 아이콘 표시 여부
			$full_part_array = explode('/',$rows->full_path);
			//var_dump($full_part_array);
			$raw_name_file = explode('.',$full_part_array[sizeof($full_part_array)-1]);
			
			//var_dump($raw_name_file);
			//$file_ext = strtolower($raw_name_file[1]);
			$file_ext = $raw_name_file[1];
			$raw_name = $raw_name_file[0].".".$file_ext;
			if($where !=''){
				if($where == $rows->file_type){
					goto emplement_layout;
				}else{
					goto finish_con;
				}
				
			}else{
				
				goto emplement_layout;
			}
			emplement_layout:
			//삭제 아이콘 표시 여부
			$download_link = "<a target=\"_blank\"  class='file_type'  data-file_ext='$rows->file_type' data-bbs_idx='$rows->bbs_idx' 
                           
				 href=\"/?c=common&m=download&file_name=$raw_name&orig_name=" . urlencode($rows->orig_name) . "&mcd=$mcd\" title=\"파일다운로드\">";
			$file_icon = "<a href=\"/?c=common&m=download&file_name=$raw_name&orig_name=" . urlencode($rows->orig_name) . "&mcd=$mcd\" title=\"파일다운로드\" target=\"_blank\" >
			<img src=\"/images/common/fileicon/file_" . $file_ext . ".png\" border=\"0\" align=\"absmiddle\" onerror=\"this.src='/images/common/fileicon/unknown.png'\" />
			</a>";

			
			//줄바꿈 여부
				if($logo =='Y'){
					$attach_str .= $file_icon;
					$attach_str .=	$download_link."<b>".(( $rows->file_type == 'inspect') ? 'Inspection Sheet' :
									(($rows->file_type == 'export') ? 'Export Certification' :
									(($rows->file_type == 'cancel') ? 'Cancellation Certification' :
									(($rows->file_type == 'register') ? 'Car Registeration' :
									(($rows->file_type == 'original') ? 'Original Register' :
									'Invoice')))))."</b>";
				}
				if ($line != 'N') {
					$file_size = explode('(', $rows->file_size);
					$size = implode(' ', $file_size);
					// $attach_str .= $download_link .$rows->orig_name . '&nbsp;' . $rows->file_size . '</a>&nbsp;&nbsp;&nbsp;<br/>';
					$attach_str .= $download_link . '&nbsp;(' . $size . '</a>&nbsp;&nbsp;&nbsp;<br/>';

				    if($_GET['m']=="adm_bbs_modify"){
				    	$attach_str = $download_link . $rows->orig_name . '&nbsp;';
				    }
					//앨범형 스킨인 경우
				} else {
					$attach_str .= $rows->orig_name . '&nbsp;' . $rows->file_size . '&nbsp;';
					
				}
				if ($del_flag == 'Y') {

					//앨범형 스킨인 경우
					if($del_yn == 'Y'){
					
						$attach_str .="&nbsp;<a href=\"#\" onclick=\"delete_bbs_attach_files(".$rows->id.",".$rows->bbs_idx."); return false;\" title=\"삭제\">";	
					}else{
						
						$attach_str .="&nbsp;<a href=\"#\" data-file_ext='$rows->file_type' data-bbs_idx='$rows->bbs_idx' data-mcd='carItem'
						 data-only_attach=Y data-id='$rows->id' data-idx='$rows->bbs_idx'
						 class='del'>";
					}
					if ($image_name != '') {
						$attach_str .= "<img src=\"/views/user/bbs/" . $mcd . "/images/" . $image_name . "\" align=\"absmiddle\" alt=\"삭제\" /></a>&nbsp;&nbsp;";
					} else {
						$attach_str .= "<img src=\"/images/common/bbs/delete_icon.gif\" align=\"absmiddle\" alt=\"삭제\" /></a>&nbsp;&nbsp;";
					}
				
				}
			
			finish_con:
		}
	} else {
		if ($del_flag != 'Y')
			$attach_str = '--';
		else
			$attach_str = '';
	}

	return $attach_str;
}

//이미지 파일 String 생성
function func_create_image_files($raw_name, $file_ext, $orig_name, $mcd, $thumnail_size, $width = '', $height = '') {
	$images_str = '';
	if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $mcd . '/' . $raw_name . '_' . $thumnail_size . $file_ext)) {
		//썸네일 파일이 존재하지 않을경우 원본으로 때려넣음
		$images_str .= "<!-- 썸네일 파일이 존재하지 않습니다 --><a href=\"/?c=common&m=download&file_name=" . $raw_name . $file_ext . "&orig_name=" . $orig_name . "&mcd=" . $mcd . "\" rel=\"lightbox[roadtrip]\" title=\"" . $orig_name . "\">";
		$images_str .= "<img src=\"/?c=common&m=download&file_name=" . $raw_name . $file_ext . "&orig_name=" . $orig_name . "&mcd=" . $mcd . "\" alt=\"" . $orig_name . "\" border=\"0\" style=\"width:90%;\" />";
		$images_str .= "</a>";
	} else {
		if ($raw_name == '' || $file_ext == '' || $orig_name == '') {
			$images_str .= "<div style=\"text-align:center;width:" . $thumnail_size . "px\">No Image</div>";
		} else {
			//가로 세로 크기 고정이 아닐경우
			if ($width == '' || $height == '') {
				$images_str .= "<a href=\"/?c=common&m=download&file_name=" . $raw_name . $file_ext . "&orig_name=" . $orig_name . "&mcd=" . $mcd . "\" rel=\"lightbox[roadtrip]\" title=\"" . $orig_name . "\">";
				$images_str .= "<img src=\"/?c=common&m=download&file_name=" . $raw_name . "_" . $thumnail_size . $file_ext . "&orig_name=" . $orig_name . "&mcd=" . $mcd . "\" alt=\"" . $orig_name . "\" border=\"0\" />";
				$images_str .= "</a>";
			} else {
				$images_str .= "<a href=\"/?c=common&m=download&file_name=" . $raw_name . $file_ext . "&orig_name=" . $orig_name . "&mcd=" . $mcd . "\" rel=\"lightbox[roadtrip]\" title=\"" . $orig_name . "\">";
				$images_str .= "<img src=\"/?c=common&m=download&file_name=" . $raw_name . "_" . $thumnail_size . $file_ext . "&orig_name=" . $orig_name . "&mcd=" . $mcd . "\" alt=\"" . $orig_name . "\" border=\"0\" width=\"" . $width . "\" height=\"" . $height . "\"/>";
				$images_str .= "</a>";
			}
		}
	}

	return $images_str;
}

//이미지 파일 String 생성
function func_create_fancybox($vals) {
	if($vals['size']) {
		$FileThumb = $vals['raw'].'_'.$vals['size'].''.$vals['ext'];
	} else {
		$FileThumb = $vals['raw'].''.$vals['ext'];
	}
	$FileFull = $vals['raw'].''.$vals['ext'];

	$FileOrg = $vals['name'];

	$UrlThumb = '/?c=common&m=download&file_name='.$FileThumb.'&orig_name='.$FileOrg.'&mcd='.$vals['mcd'];
	$UrlFull = '/?c=common&m=download&file_name='.$FileFull.'&orig_name='.$FileOrg.'&mcd='.$vals['mcd'];

	if($vals['title']) {
		$Html = '<a href="'.$UrlFull.'" title="'.$vals['title'].'" rel="fancybox">';
	} else {
		$Html = '<a href="'.$UrlFull.'" title="" rel="fancybox">';
	}
	$Html .= '<img src="'.$UrlThumb.'" alt="'.$FileOrg.'">';
	$Html .= '</a>';

	return $Html;
}


//디렉토리에 존재하는 파일 복사
function func_file_copy($src, $dest) {
	if (is_dir($src)) {
		$map = get_dir_file_info($src);

//값을 배열로 가져온다.
		$keys = func_get_array_keys($map);
		if (is_array($keys)) {
			for ($i = 0; $i < sizeof($keys); $i++) {
				if (sizeof($keys) > 1) {
					if (is_array($map[$keys[$i]])) {
						$tmpArr = $map[$keys[$i]];
						@copy($src . "/" . $tmpArr['name'], $dest . "/" . $tmpArr['name']);
						@chmod($dest . "/" . $tmpArr['name'], 0777);
					}
				} else {
					@copy($src . "/" . $keys[0], $dest . "/" . $keys[0]);
					@chmod($dest . "/" . $keys[0], 0777);
				}
			}
		}
	}
	return;
}

//디렉토리 생성
function func_make_dir($path) {
	if (!(is_dir($path) )) {
		$path_temp = split("/", $path);
		$path_str = '';
		for ($i = 0; $i < count($path_temp); $i++) {
			$path_str .= trim($path_temp[$i]) . "/";

			if (!(is_dir($path_str) )) {
				mkdir($path_str, 0777);
				chmod($path_str, 0777);
			}
		}
	}
}

//현재 URL
function func_get_current_url() {
	return urlencode('http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
//return $_SERVER['REQUEST_URI'];
}

//이전 URL
function func_get_referer_url() {
	return (!isset($_SERVER['HTTP_REFERER']) OR $_SERVER['HTTP_REFERER'] == '') ? '' : trim($_SERVER['HTTP_REFERER']);
}

function func_get_root_path() {
	$sRealPath = realpath('./');

	$sSelfPath = $_SERVER['PHP_SELF'];
	$sSelfPath = substr($sSelfPath, 0, strrpos($sSelfPath, '/'));

	return substr($sRealPath, 0, strlen($sRealPath) - strlen($sSelfPath));
}

//레이아웃 파일의 정보를 읽어 온다.(영역별 설정값)
function func_get_layout_info($file) {
	$fd = read_file('views/user/layout/' . $file . '.php');
	$filePos = 0;
	$areaInfo = array();

	while (true) {
		$areaName = "";
		$contents = "";
		$startPos = strpos($fd, "summary_area", $filePos);
		$endPos = 0;

		if ($startPos === false)
			break;

		if (strpos($fd, "?>", $startPos) !== false) {
			$endPos = strpos($fd, "?>", $startPos);
			$areaName = trim(substr($fd, $startPos, $endPos - $startPos));
		} else {
			break;
		}

		$filePos = $endPos;

		$areaInfo[count($areaInfo)] = $areaName;
	}

	return $areaInfo;
}

//차트 임시저장 경로
function func_get_chart_filepath($pie = NULL) {
	$tmpFile = md5(time() . "FRAMEWORKCHARTHASH");
	$tmpFile.=$pie;

	$graph_temp_directory = 'temp';  // in the webroot (add directory to .htaccess exclude)
	$graph_file_name = $tmpFile.= ".png";
	return $graph_temp_directory . '/' . $graph_file_name;
}

//차트 파일 제거
function func_del_chart_file() {
	$tmp = get_fileNames("temp");
	for ($i = 0; $i < count($tmp); $i++) {
		if (is_File("temp/" . $tmp[$i])) {
			$path = "temp/" . $tmp[$i];
			$ar = stat($path);

//10분이 지난 경우만 삭제
			$gap = time() - $ar['atime'];

			if ($gap > 600) {
				@UnLink($path);
			}
		}
	}
}

//게시판 권한 체크
function func_bbs_certi_check($conf, $var, $session_grade_no) {
	$member_grade_yn = func_get_config($conf, 'member_grade_yn');  //회원권한 설정 여부
	$member_grade_fix = func_get_config($conf, 'member_grade_fix'); //회원권한 고정 여부
	$tmp = explode('|', func_get_config($conf, $var));
	$chk_grade_no = $tmp[0];

	//회원권한을 설정 했다면..
	if ($member_grade_yn != 'N') {
	//회원권한 고정을 하지 않았다면...
		if ($member_grade_fix != 'Y') {
	//설정된 회원권한 보다 현재 권한이 같거나 크다면..
			if($session_grade_no!=''){
			if ((integer) $chk_grade_no >= (integer) $session_grade_no) {
				return true;
			} else {
				return false;
			}
				return false;
				}
		} else {
	//설정된 회원권한과 현재 권한이 같지 않다면
			if ((integer) $chk_grade_no != (integer) $session_grade_no) {
				return false;
			} else {
				return true;
			}
		}
	} else {
		return true;
	}
}

//게시판 카테고리 명을 가져 온다.
function func_get_gategory($cate_code, $cate_name, $cate_cd) {
	$category_code = explode('|', $cate_code);
	$category_name = explode('|', $cate_name);

	if (count($category_code) > 0) {
		for ($i = 0; $i < count($category_code); $i++) {
			if ($category_code[$i] == $cate_cd) {
				return $category_name[$i];
			}
		}
	}
}

//입력된 이메일의 유효성검사
function func_email_check($email) {
	if (!ereg("(^[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+)*@[0-9a-zA-Z-]+(\.[0-9a-zA-Z-]+)*$)", $email)) {
		return FALSE;
	}

	return TRUE;
}

/*
  //id를 5~10자의 영문이나 숫자만 허용
  if(!ereg("[[:alnum:]+]{5,10}",$id)) {
  echo "<script>
  alert('ID는 5~10자의 영문자나 숫자의 조합이어야 합니다!!');
  history.back(-1);
  </script>";
  exit();
  }

  // passwd를 4~8자의 영문이나 숫자만 허용
  if(!ereg("[[:alnum:]+]{4,8}",$passwd)) {
  echo "<script>
  alert('비밀번호는 4~8자의 영문자나 숫자의 조합이어야 합니다!!');
  history.back(-1);
  </script>";
  exit();
  }

  // 입력된 홈페이지의 유효성 검사
  if(ereg("([^[:space:]]+)", $homepage) && (!ereg("http://([0-9a-zA-Z./@~?&=_]+)", $homepage))  ) {
  echo "<script>
  alert('홈페이지 주소가 형식에 맞지 않습니다!!');
  history.back(-1);
  </script>";
  exit();
  }


  //name에 공백(space)이 있는건 허용하지 않음
  if(!ereg("([^[:space:]]+)", $name) || ereg("([[:space:]]+)",$name)) {
  echo "<script>
  alert('이름에 공백이 존재합니다!!\n\n이름을 공백없이 입력하세요!!');
  history.back(-1);
  </script>";
  exit();
  }


  // name이 한글일 경우만 허용
  for($i = 0; $i < strlen($name); $i++) {
  if(ord($name[$i]) <= 0x80) {
  echo "<script>
  alert('이름은 반드시 한글이어야 합니다!!');
  history.back(-1);
  </script>";
  exit();
  }
  }
 */

//퀵서비스 엑셀
function excel_upload() {

	@ini_set("memory_limit", "1024M");

	$this->ci->load->library('excel_reader');

	$this->ci->excel_reader->setOutputEncoding('UTF-8');
	$this->ci->excel_reader->read('/home/intsworks/dev/www/tmp/all_quick.xls');
	$value_rows = 1;
	$sheet_cnt = 0;
	$inData = array();
	//for($i=1; $i<=$this->ci->excel_reader->sheets[0]['numRows']; $i++)
	for ($i = 1; $i <= 100; $i++) {
		$sheet_cnt = 0;
		$inData['s_sido'] = '';
		$inData['s_gugun'] = '';
		$inData['s_dong'] = '';
		$inData['e_sido'] = '';
		$inData['e_gugun'] = '';
		$inData['e_dong'] = '';
		$inData['fee'] = '';

		$tmp1 = '';

		for ($j = 1; $j <= $this->ci->excel_reader->sheets[0]['numCols']; $j++) {
			//시작
			if ($j == 1)
				$inData['s_sido'] = $this->ci->excel_reader->sheets[0]['cells'][$i][$j];
			if ($j == 2)
				$inData['s_gugun'] = $this->ci->excel_reader->sheets[0]['cells'][$i][$j];
			if ($j == 3)
				$inData['s_dong'] = $this->ci->excel_reader->sheets[0]['cells'][$i][$j];

			//$tmp1 .= $this->ci->excel_reader->sheets[0]['cells'][$i][$j].' ';
		}
			//echo $tmp1;

		$value_cols = 0;
		for ($k = 1; $k <= $this->ci->excel_reader->sheets[1]['numRows']; $k++) {

			$tmp2 = '';
			$value_cols++;
			for ($m = 1; $m <= $this->ci->excel_reader->sheets[1]['numCols']; $m++) {
				//종료
				if ($m == 1)
					$inData['e_sido'] = $this->ci->excel_reader->sheets[1]['cells'][$k][$m];
				if ($m == 2)
					$inData['e_gugun'] = $this->ci->excel_reader->sheets[1]['cells'][$k][$m];
				if ($m == 3)
					$inData['e_dong'] = $this->ci->excel_reader->sheets[1]['cells'][$k][$m];
				//$tmp2 .= $this->ci->excel_reader->sheets[1]['cells'][$k][$m].' ';
			}


			//금액
			if ((integer) $sheet_cnt == 0) {
				if (is_numeric($this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols])) {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols] . '000';
				} else {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols];
				}

				if ((integer) $value_cols == 200) {
					$sheet_cnt = 1;
					$value_cols = 0;
				}
			} else if ((integer) $sheet_cnt == 1) {
				if (is_numeric($this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols])) {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols] . '000';
				} else {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols];
				}
				if ((integer) $value_cols == 200) {
					$sheet_cnt = 2;
					$value_cols = 0;
				}
			} else if ((integer) $sheet_cnt == 2) {
				if (is_numeric($this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols])) {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols] . '000';
				} else {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols];
				}
				if ((integer) $value_cols == 200) {
					$sheet_cnt = 3;
					$value_cols = 0;
				}
			} else if ((integer) $sheet_cnt == 3) {
				if (is_numeric($this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols])) {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols] . '000';
				} else {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols];
				}
				if ((integer) $value_cols == 200) {
					$sheet_cnt = 4;
					$value_cols = 0;
				}
			} else if ((integer) $sheet_cnt == 4) {
				if (is_numeric($this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols])) {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols] . '000';
				} else {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols];
				}
				if ((integer) $value_cols == 200) {
					$sheet_cnt = 5;
					$value_cols = 0;
				}
			} else if ((integer) $sheet_cnt == 5) {
				if (is_numeric($this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols])) {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols] . '000';
				} else {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols];
				}
				if ((integer) $value_cols == 200) {
					$sheet_cnt = 6;
					$value_cols = 0;
				}
			} else if ((integer) $sheet_cnt == 6) {
				if (is_numeric($this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols])) {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols] . '000';
				} else {
					$inData['fee'] = $this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols];
				}
				if ((integer) $value_cols == 200) {
					$sheet_cnt = 7;
					$value_cols = 0;
				}
			}
			/*
			  else if((integer)$sheet_cnt == 2) {
			  $inData['fee'] = $this->ci->excel_reader->sheets[4]['cells'][$value_rows][$value_cols].'000';
			  }
			 */
			//$tmp2 .= '$'.$this->ci->excel_reader->sheets[2]['cells'][$value_rows][$value_cols];
			//echo '[s_sido='.$inData['s_sido'].']';
			//echo '[s_gugun'.$inData['s_gugun'].']';
			//echo '[s_dong='.$inData['s_dong'].']';
			//echo '[e_sido='.$inData['e_sido'].']';
			//echo '[e_gugun='.$inData['e_gugun'].']';
			//echo '[e_dong='.$inData['e_dong'].']';
			//echo '[fee='.$inData['fee'].']';
			//$str = '[s_sido='.$inData['s_sido'].']'.'[s_gugun'.$inData['s_gugun'].']'.'[s_dong='.$inData['s_dong'].']'.'[e_sido='.$inData['e_sido'].']'.'[e_gugun='.$inData['e_gugun'].']'.'[e_dong='.$inData['e_dong'].']'.'[fee='.$inData['fee'].']';
			//echo $str."\n";
			//echo $tmp1.'_'.$tmp2."\n";

			$this->ci->bbs->quick_insert($inData);
		}

		$value_rows++;
	}
	echo "성공";
}
//----------validate checkbox value--------------
function func_checkbox_validate($post_value,$val_checkbox){
	$checkbox_array = explode('^',$post_value);
	$chk_status ="";
	if (count($checkbox_array) > 0) {
		for ($i = 0; $i < count($checkbox_array); $i++) {
			if ($checkbox_array[$i] == $val_checkbox) {
				$chk_status = 'checked="checked"';
			}
		}
	}
	return $chk_status;
}

//------------functin currentcy -----------------//

function func_currency_sym($currency_sym ='USD',$symb = ''){
	
	$sym = array('USD'=>'USD','KRW'=>'KRW','JPY'=>'JPY','EUR'=>'EUR');	
	$select_box ='';
	foreach($sym as $key => $csymbol){
		
		if($key == $currency_sym){
			
			$select_box .="<option value=\"".$key."\" selected=\"selected\">".$sym[$currency_sym]."</option>";							
			
		}else{
			
			$select_box .="<option value=\"".$key."\" >".$sym[$key]."</option>";
		}
		
	}
	if($symb !='' && $currency_sym !=''){
		$select_box ='';
		foreach($sym as $key => $csymbol){
			
			if($currency_sym == $key){
				
				$select_box .=$sym[$currency_sym];
			}
		}
	}else{
		if($symb !=''){
		$select_box ='';
		$select_box = 	$sym['USD'];
		}
	}
	return $select_box;
}
//======create date=======================
function func_create_months($months_val=''){
	
	$months = array();
	//$currentMonth = (int)date('m');
	$currentMonth = 1;

	$month_option = '';
	
	for ($x = $currentMonth; $x < $currentMonth + 12; $x++) {
		$months[$x] = date('F', mktime(0, 0, 0, $x, 1));
	}
	
	
	foreach($months as $key => $item){
		if(($months_val == $key) && ($months_val !='')){
			
			$month_option .="<option value=\"".$key."\" selected=\"selected\">".$item."</option>";
		}else{
			
			if($key==1){
				$month_option .="<option value='' selected=\"selected\">- Month -</option>";
			}	
			$month_option .="<option value=\"".$key."\" >".$item."</option>";
		}
	}
	return $month_option;
	
}

function func_create_years($years_val= ''){
	
	$year_option = '';
	$year = range(1990,date("Y"));
	$year_option .="<option value=\"\" selected=\"selected\" >- Year -</option>";
	foreach($year as $item){
		if($years_val == $item){
			
			$year_option .="<option value=\"".$item."\" selected=\"selected\" >".$item."</option>";
		
		}else{
			
			$year_option .="<option value=\"".$item."\" >".$item."</option>";
		}
		
	}
	return $year_option;
	
}
//==========delete image===============
function func_bbs_delete_file($bbs_attach,$image = 'Y',$conf = null){
  if ($bbs_attach->num_rows() > 0) {
  
	  foreach ($bbs_attach->result() as $rows) {
  
		  //첨부파일이 이미지인 경우 생성된 썸네일 까지 삭제
  
		  if($image =='Y'){
			  
				if ($rows->is_image) {
	  
				  @unlink($rows->full_path);   //원본 이미지 삭제
	  
				  for ($i = 1; $i < 6; $i++) {    //썸네일 삭제
				  
					  $thumb_path = $rows->file_path . $rows->raw_name . '_' . func_get_config($conf, 'attach_thumbnail_size' . $i) . $rows->file_ext;
	  
					  if (file_exists($thumb_path)) {
	  
						  @unlink($thumb_path);
					  }
				  }
			  } else {
	  
				  @unlink($rows->full_path);
			  }  
		  }else{
			  
			  @unlink($rows->full_path);
		  }
	  }
  }
	
}

//==========currency exchange===============
function func_profit_calculation($car_idx , $total_buying_price, $buying_currency)
{
/*	echo 'car_idx : '.$car_idx; echo '<br>';
	echo 'buying price : '.$total_buying_price; echo '<br>';
	echo 'currency : '.$buying_currency; echo '<br>';*/

	$sql = "SELECT * FROM iw_payment WHERE car_idx=$car_idx" ;
	$result = mysql_query($sql);
	$total_allocate = 0;

	while ($rows = mysql_fetch_array($result)){

		$from = $rows['currency_type'];
		$to = $buying_currency;

		if($buying_currency == $rows['currency_type']){ //same currency
			
			//echo "same<br>";

			$total_allocate += $rows['car_allocate'];
		
		}else{ //different currency

			$sql_currency = "SELECT * FROM iw_currency WHERE from_currency='".$from."' AND to_currency='".$to."' AND exchange_dt ='".date_format(date_create($rows['allocate_dt']),'Y-m-d')."'";
			$result_currency = mysql_query($sql_currency);
			
			$row = mysql_fetch_array($result_currency);

			if(!$row){ //there is no exchange_rate value in iw_currency
				echo "Please Input Exchange Rate in Accounting<br>";

				goto Stop;
			}else{ // iw_currency has the exchange_rate value
				//echo "row Ture<br>";

				$total_allocate += $rows['car_allocate'] * $row['exchange_rate'];
				
				/*echo 'rate : '.$row['exchange_rate'];
				echo "<br>";
				echo 'total_allocate :'.$total_allocate;
				echo "<br>";*/
			}

		}
	}

/*	echo "total_allocate :".$total_allocate;
	echo "<br>";*/
	$profit = $total_allocate - $total_buying_price;
	echo $profit.' '.$buying_currency;
	

	Stop:
	echo " ";


}

function _func_wirte_page_info($pageInfo, $sParam, $sUrl, $skin_folder = NULL, $fa = NULL, $pa = NULL, $na = NULL, $la = NULL) {
	if ($pageInfo == NULL)
		return;

	//$fa = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_first.gif\" border=\"0\" alt=\"처음으로\" />";
	//$pa = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_prev.gif\" border=\"0\" alt=\"이전\" />";
	//$na = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_next.gif\" border=\"0\" alt=\"다음\" />";
	//$la = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_last.gif\" border=\"0\" alt=\"마지막\" />";


	if ($fa == NULL || $fa == "") {
		$fa = "<img src=\"/images/common/bbs/arrow_first.gif\" border=\"0\" alt=\"처음으로\" />";
	} else {
		$fa = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $fa . "\" border=\"0\" alt=\"처음으로\" />";
	}
	if ($pa == NULL || $pa == "") {
		$pa = "<img src=\"/images/common/bbs/arrow_prev.gif\" border=\"0\" alt=\"이전\" />";
	} else {
		$pa = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $pa . "\" border=\"0\" alt=\"이전\" />";
	}
	if ($na == NULL || $na == "") {
		$na = "<img src=\"/images/common/bbs/arrow_next.gif\" border=\"0\" alt=\"다음\" />";
	} else {
		$na = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $na . "\" border=\"0\" alt=\"다음\" />";
	}
	if ($la == NULL || $la == "") {
		$la = "<img src=\"/images/common/bbs/arrow_last.gif\" border=\"0\" alt=\"마지막\" />";
	} else {
		$la = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $la . "\" border=\"0\" alt=\"마지막\" />";
	}

	//var_dump($sParam);

	$sParam = func_sPUrlEncode($sParam, "sch_word");

	//var_dump($sParam);
	//exit;

	$tmpStr = "";

	if ($pageInfo[3] == -1) {

	} else {
		$tmpStr.= "<a href=\"$sUrl&cus_cur_page=1$sParam\">" . $fa . "</a><a href=\"$sUrl&cus_cur_page=$pageInfo[3]$sParam\">" . $pa . "</a>&nbsp;";
	}
	for ($i = 0; $i <= ($pageInfo[2] - $pageInfo[1]); $i++) {
		if (($pageInfo[1] + $i) == $pageInfo[7]) {	// 현제 페이지
			$pages = $pageInfo[1] + $i;
			$tmpStr.="<span style=\"color:red;\">$pages&nbsp;</span>";
		} else {
			$pages = $pageInfo[1] + $i;
			$tmpStr.="<a href=\"$sUrl&cus_cur_page=$pages$sParam\">$pages&nbsp;</a>";
		}
	}

	if ($pageInfo[4] == -1) {
		
	} else {
		$tmpStr.="<a href=\"$sUrl&cus_cur_page=$pageInfo[4]$sParam\">" . $na . "</a><a href=\"$sUrl&cus_cur_page=$pageInfo[0]$sParam\" title=\"다음\">" . $la . "</a>";
	}

	return $tmpStr;
}
function func_wirte_page_info_($pageInfo, $sParam, $sUrl, $skin_folder = NULL, $fa = NULL, $pa = NULL, $na = NULL, $la = NULL) {
	if ($pageInfo == NULL)
		return;

	//$fa = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_first.gif\" border=\"0\" alt=\"처음으로\" />";
	//$pa = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_prev.gif\" border=\"0\" alt=\"이전\" />";
	//$na = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_next.gif\" border=\"0\" alt=\"다음\" />";
	//$la = "<img src=\"/views/user/bbs/".$skin_folder."/images/arrow_last.gif\" border=\"0\" alt=\"마지막\" />";


	if ($fa == NULL || $fa == "") {
		$fa = "<img src=\"/images/common/bbs/arrow_first.gif\" border=\"0\" alt=\"처음으로\" />";
	} else {
		$fa = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $fa . "\" border=\"0\" alt=\"처음으로\" />";
	}
	if ($pa == NULL || $pa == "") {
		$pa = "<img src=\"/images/common/bbs/arrow_prev.gif\" border=\"0\" alt=\"이전\" />";
	} else {
		$pa = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $pa . "\" border=\"0\" alt=\"이전\" />";
	}
	if ($na == NULL || $na == "") {
		$na = "<img src=\"/images/common/bbs/arrow_next.gif\" border=\"0\" alt=\"다음\" />";
	} else {
		$na = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $na . "\" border=\"0\" alt=\"다음\" />";
	}
	if ($la == NULL || $la == "") {
		$la = "<img src=\"/images/common/bbs/arrow_last.gif\" border=\"0\" alt=\"마지막\" />";
	} else {
		$la = "<img src=\"/views/user/bbs/" . $skin_folder . "/images/" . $la . "\" border=\"0\" alt=\"마지막\" />";
	}

	//var_dump($sParam);

	$sParam = func_sPUrlEncode($sParam, "sch_word_sender");

	//var_dump($sParam);
	//exit;

	$tmpStr = "";

	if ($pageInfo[3] == -1) {

	} else {
		$tmpStr.= "<a href=\"$sUrl&cur_page=1$sParam\">" . $fa . "</a><a href=\"$sUrl&cur_page=$pageInfo[3]$sParam\">" . $pa . "</a>&nbsp;";
	}
	for ($i = 0; $i <= ($pageInfo[2] - $pageInfo[1]); $i++) {
		if (($pageInfo[1] + $i) == $pageInfo[7]) {	// 현제 페이지
			$pages = $pageInfo[1] + $i;
			$tmpStr.="<span style=\"color:red;\">$pages&nbsp;</span>";
		} else {
			$pages = $pageInfo[1] + $i;
			$tmpStr.="<a href=\"$sUrl&cur_page=$pages$sParam\">$pages&nbsp;</a>";
		}
	}

	if ($pageInfo[4] == -1) {
		
	} else {
		$tmpStr.="<a href=\"$sUrl&cur_page=$pageInfo[4]$sParam\">" . $na . "</a><a href=\"$sUrl&cur_page=$pageInfo[0]$sParam\" title=\"다음\">" . $la . "</a>";
	}

	return $tmpStr;
}

function getCountryName($country_cc){

	$sql = mysql_query("SELECT * FROM iw_country_list WHERE cc='$country_cc'") ;
	
	while ($rows = mysql_fetch_array($sql)){
		$country_name = $rows['country_name'];
	}
	return $country_name;
}

?>
