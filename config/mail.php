<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/********************************************
  Default Value : CodeIgniter
  Option        : None
  Description   : The "user agent"
*********************************************/
$config['useragent'] = "n-os";

/********************************************
  Default Value : mail
  Option        : mail, sendmail or smtp
  Description   : The mail sending protocol
*********************************************/
$config['protocol']  = "mail";
//$config['protocol']  = "sendmail";

/******************************************** 
  Default Value : /usr/sbin/sendmail
  Option        : None
  Description   : The Server path to Sendmail
*********************************************/
$config['mailpath'] = "/usr/sbin/sendmail";

/********************************************
  Default Value : No Default
  Option        : None
  Description   : SMTP Server Address 
*********************************************/
$config['smtp_host'] = "";

/********************************************
  Default Value : No Default
  Option        : None
  Description   : SMTP UserName
*********************************************/
$config['smtp_user'] = "";

/********************************************
  Default Value : No Default
  Option        : None
  Description   : SMTP Password
*********************************************/
$config['smtp_pass'] = "";

/********************************************
  Default Value : 25
  Option        : None
  Description   : SMTP Port
*********************************************/
$config['smtp_port'] = "25";

/********************************************
  Default Value : 5
  Option        : None
  Description   : SMTP Timeout(in seconds)
*********************************************/
$config['smtp_timeout'] = "5";

/********************************************
  Default Value : TRUE
  Option        : TRYE or FALSE (boolean)
  Description   : Enable word-wrap
*********************************************/
$config['wordwrap'] = "TRUE";

/******************************************** 
  Default Value : 76
  Option        : 
  Description   : Character count to wrap at
*********************************************/
$config['wrapchars'] = "76";

/********************************************
  Default Value : text
  Option        : text or html
  Description   : Type of mail. if you send HTML email you must send it as a complete web page.
                  Make sure you don't have any relative links or relative image path otherwise they will not work.
*********************************************/
$config['mailtype'] = "text";

/********************************************
  Default Value : utf-8
  Option        : 
  Description   : Character set(utf-8, iso-8859-1, etc)
*********************************************/
$config['charset'] = "utf-8";
//$config['charset'] = 'iso-8859-1';

/********************************************
  Default Value : FALSE
  Option        : TRUE or FALSE (boolean) 
  Description   : Whether to validate the email address
*********************************************/
$config['validate'] = "TRUE";

/********************************************
  Default Value : 3
  Option        : 1,2,3,4,5
  Description   : Email Priority. 1=highest, 5=lowest, 3=normal
*********************************************/
$config['priority'] = "3";

/********************************************
  Default Value : \n
  Option        : "\r\n" or "\n"
  Description   : Newline character. (Use "\r\n" to comply with RFC 822)
*********************************************/
$config['newline'] = "\n";

/********************************************
  Default Value : FALSE
  Option        : TRUE or FALSE (boolean)
  Description   : Enable BCC Batch Mode
*********************************************/
$config['bcc_batch_mode'] = "FALSE";

/********************************************
  Default Value : 200
  Option        : None
  Description   : Number of emails in each BCC batch
*********************************************/
$config['bcc_batch_size'] = "200";
?>
