<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="Title" content="SBT KOREA" />
<meta name="Subject" content="SBT KOREA" />
<meta name="Description" content="SBT KOREA" />
<meta name="Keywords"content="SBT KOREA" />
<meta name="Copyright"content="SBT KOREA" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HOME | BLAUDA</title>
<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT; ?>/common/css/style.css" media="ALL" />
<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT; ?>/css/index.css" media="ALL" />
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto" />
<script type="text/javascript" src="<?php echo WEB_ROOT; ?>/common/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT; ?>/common/js/common.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT; ?>/js/small.nav.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT; ?>/js/slides.min.jquery.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT; ?>/js/jPages.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT; ?>/js/index.js"></script>
</head>

<body id="top">
<div id="wrap"><div id="dvLoading">&nbsp;</div>
	<?php include('common/php/header.php'); ?>
	<div id="container">
		<div id="banner" class="clearfix">
			<div class="slides_container">
				<div class="banner-01">&nbsp;</div>
				<div class="banner-02">&nbsp;</div>
				<div class="banner-03">&nbsp;</div>
			</div>
			<ul class="pagination">
				<li><a href="#">&nbsp;</a></li>
				<li><a href="#">&nbsp;</a></li>
				<li><a href="#">&nbsp;</a></li>
			</ul>
		<!-- /Banner --></div>
		<div id="content">
			<div class="level clearfix">
				<div class="form">
					<div class="line_title">
						<p>Search Vehicles</p>
					</div>
					<form action="<?php echo WEB_ROOT; ?>/?p=product&result=1&search=true" method="post">
						<div id="loading_spinner">&nbsp;</div>
						<table class="tblForm" cellpadding="0" cellspacing="0">
							<tr>
								<td class="tdLeft">Make</td>
								<td class="tdRight">
									<label class="hold-control">
										<select name="maker_name" id="var_maker_name" class="select">
											<option value="*">Any</option>
											<?php $sql = mssql_query("SELECT DISTINCT var_maker_name FROM trn_stock_main ORDER BY var_maker_name ASC"); ?>
											<?php while ($row = mssql_fetch_array($sql)) { ?>
											<option value="<?php echo $row['var_maker_name']; ?>"><?php echo $row['var_maker_name']; ?></option>
											<?php } ?>
										</select>
									</label>
								</td>
							</tr>
							<tr>
								<td class="tdLeft">Model</td>
								<td class="tdRight">
									<label class="hold-control">
										<select name="model_name" id="var_model_name" class="select">
											<option value="*">Any</option>
											<?php $sql = mssql_query("SELECT DISTINCT var_model_name FROM trn_stock_main ORDER BY var_model_name ASC"); ?>
											<?php while ($row = mssql_fetch_array($sql)) { ?>
											<option value="<?php echo $row['var_model_name']; ?>"><?php echo $row['var_model_name']; ?></option>
											<?php } ?>
										</select>
									</label>
								</td>
							</tr>
							<tr>
								<td class="tdLeft">Grade</td>
								<td class="tdRight">
									<label class="hold-control">
										<select name="grade_name" id="var_grade_name" class="select">
											<option value="*">Any</option>
											<?php $sql = mssql_query("SELECT DISTINCT var_grade_name FROM trn_stock_main ORDER BY var_grade_name ASC"); ?>
											<?php while ($row = mssql_fetch_array($sql)) { ?>
											<option value="<?php echo $row['var_grade_name']; ?>"><?php echo $row['var_grade_name']; ?></option>
											<?php } ?>
										</select>
									</label>
								</td>
							</tr>
							<tr>
								<td class="tdLeft">Transmission</td>
								<td class="tdRight">
									<label class="hold-control">
										<select name="drive_mission" id="var_drive_mission" class="select">
											<option value="*">Any</option>
											<?php $sql = mssql_query("SELECT DISTINCT var_drive_mission FROM trn_stock_main ORDER BY var_drive_mission ASC"); ?>
											<?php while ($row = mssql_fetch_array($sql)) { ?>
											<option value="<?php echo $row['var_drive_mission']; ?>"><?php echo $row['var_drive_mission']; ?></option>
											<?php } ?>
										</select>
									</label>
								</td>
							</tr>
							<tr>
								<td class="tdLeft">Chassis Number</td>
								<td class="tdRight">
									<label class="hold-control"><label class="hold-control"><input id="chassis" class="textbox" type="text" name="chassis_number" /></label></label>
								</td>
							</tr>
						</table>
						<div class="btSubmit">
							<input type="image" class="linkimg" src="<?php echo WEB_ROOT; ?>/images/bt_search.png" />
						</div>
					</form>
				<!-- /Form --></div>
				<div class="image">
					<div class="line_title">
						<p>Quick Search</p>
					</div>
					<div class="image-list clearfix">
						<ul>
							<li>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=sedan&result=1&search=quick"><img src="<?php echo WEB_ROOT; ?>/images/cat_01.png" class="linkimg" /></a>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=sedan&result=1&search=quick" class="image-text">Sedan</a>
							</li>
							<li>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=hatchback&result=1&search=quick"><img src="<?php echo WEB_ROOT; ?>/images/cat_02.png" class="linkimg" /></a>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=hatchback&result=1&search=quick" class="image-text">Hatchback</a>
							</li>
							<li>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=suv&result=1&search=quick"><img src="<?php echo WEB_ROOT; ?>/images/cat_03.png" class="linkimg" /></a>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=suv&result=1&search=quick" class="image-text">SUV</a>
							</li>
							<li>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=minibus&result=1&search=quick"><img src="<?php echo WEB_ROOT; ?>/images/cat_04.png" class="linkimg" /></a>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=minibus&result=1&search=quick" class="image-text">Mini Bus</a>
							</li>
							<li>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=bus&result=1&search=quick"><img src="<?php echo WEB_ROOT; ?>/images/cat_05.png" class="linkimg" /></a>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=bus&result=1&search=quick" class="image-text">Bus</a>
							</li>
							<li>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=truck&result=1&search=quick"><img src="<?php echo WEB_ROOT; ?>/images/cat_06.png" class="linkimg" /></a>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=truck&result=1&search=quick" class="image-text">Truck</a>
							</li>
							<li>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=heavymachince&result=1&search=quick"><img src="<?php echo WEB_ROOT; ?>/images/cat_07.png" class="linkimg" /></a>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=heavymachince&result=1&search=quick" class="image-text">Heavy Machince</a>
							</li>
							<li>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=brandnew&result=1&search=quick"><img src="<?php echo WEB_ROOT; ?>/images/cat_08.png" class="linkimg" /></a>
								<a href="<?php echo WEB_ROOT; ?>/?p=product&type=brandnew&result=1&search=quick" class="image-text">Brand New</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		<!-- /content --></div>
		<div class="level bgFull">
			<div class="popular">
				<div class="line_title">
					<p>Popular Model<a href="<?php echo WEB_ROOT; ?>/?p=product&result=1" class="more">More</a></p>
				</div>
				<div id="auto">
					<div class="nav_list clearfix">
						<?php
						$select = "select top 10 (case when min(image.int_sort_id)=0 then max(image.var_image_file) else 'default_186_105.jpg' end) AS img_show, max(main.var_model_name) model, max(main.int_model_y) mode_year, max(main.int_asesprice_to) asesprice, max(main.var_chassis_number) chassis, max(main.int_stock_code) stock_code";
						$from = "from trn_stock_main main LEFT OUTER JOIN trn_stock_image image ON main.int_stock_code IN ( SELECT distinct image.int_stock_code FROM trn_stock_image image where image.int_sort_id=1 and image.int_del_flg=0)";
						$where = "where main.int_stock_sta BETWEEN 3 AND 5 and DATEADD(MONTH, 3, main.dte_insert_ymd) >= {fn NOW() } and main.int_del_flg=0";
						$group = "GROUP BY main.int_visit_cnt";
						
						$popular_model_sql = mssql_query("$select $from $where $group");
						while ($popular_model_row = mssql_fetch_array($popular_model_sql)) {
						?>
						<p class="item">
							<span class="car_name"><?php echo $popular_model_row['model']; ?></span>
							<span class="car_item">
								<a href="<?php echo WEB_ROOT; ?>/?p=detail&id=<?php echo $popular_model_row['stock_code']; ?>&result=1">
									<?php if($popular_model_row['img_show']!="default_186_105.jpg") { ?>
									<img src="http://erp.blauda.com/img/stock/<?php echo $popular_model_row['stock_code']."/".$popular_model_row['img_show']; ?>" class="linkimg" />
									<?php } else { ?>
									<img src="<?php echo WEB_ROOT."/common/images/".$popular_model_row['img_show']; ?>" class="linkimg" />
									<?php } ?>
								</a>
							</span>
							<span class="car_desc">Year <?php echo $popular_model_row['mode_year']; ?>,  FOB  $<?php echo $popular_model_row['asesprice']; ?><br/>NO.<?php echo $popular_model_row['chassis']; ?></span>
							<a href="<?php echo WEB_ROOT; ?>/?p=detail&id=<?php echo $popular_model_row['stock_code']; ?>&result=1" class="detail">Detail >></a>
						</p>
						<?php
						}
						?>
					</div>
				</div>
				<input type='hidden' id='hidden_auto_slide_seconds' value=0 />
			</div>
		<!-- /background color --></div>
		<div id="content_01">
			<div class="line_title">
				<p>Recent Model</p>
			</div>
			<div class="page">
				<table class="imageList" cellpadding="0" cellspacing="0">
					<tbody id="display">
						<?php
						$select = "select (case when min(image.int_sort_id)=0 then max(image.var_image_file) else 'default_186_105.jpg' end) AS img_show, max(main.var_model_name) model, max(main.var_grade_name) grade, max(main.var_stock_memo) memo, max(main.int_model_y) mode_year, max(main.int_asesprice_to) asesprice, max(main.int_stock_code) stock_code";
						$from = "from trn_stock_main main LEFT OUTER JOIN trn_stock_image image ON main.int_stock_code IN ( SELECT distinct image.int_stock_code FROM trn_stock_image image where image.int_sort_id=1 and image.int_del_flg=0)";
						$where = "where main.int_stock_sta BETWEEN 3 AND 5 and DATEADD(MONTH, 3, main.dte_insert_ymd) >= {fn NOW() } and main.int_del_flg=0";
						$group = "GROUP BY main.int_stock_code";
						
						$recent_model_sql = mssql_query("$select $from $where $group");
						while ($recent_model_row = mssql_fetch_array($recent_model_sql)) {
						
						?>
 <!----  code show image car  --->                      
                     
<?php /*

 $sql="select * from isbt.trn_stock_image order by int_stock_code desc limit 4";
//echo $sql;

{
	$link='detail.php?int_stock_code='.$rs['int_stock_code'];
	if($j==1)
	{
		echo '<tr>
				<td valign="top" style="padding-right:13px;">
					<table width="100%">
						<tr>
							<td align="center">
							<a href="http://erp.blauda.com/img/stock/'.$rs['var_image_file'].'"><img src="http://erp.blauda.com/img/stock/'.$rs['var_image_file'].'" width="160" height="110"/></a></td>
						</tr>
						<tr>
							  <td align="center" style="font-family:Verdana; font-size:13px; color:#3f3f3f;">&nbsp;'.$rs['mode_year'].'</td>
						</tr>
					</table>        
				</td>';
	}
	elseif($j<4)
	{
		echo '<td valign="top" style="padding-right:13px;">
					<table width="100%">
						<tr>
							<td align="center">
							<a href="http://erp.blauda.com/img/stock/'.$rs['var_image_file'].'"><img src="http://erp.blauda.com/img/stock/'.$rs['var_image_file'].'" width="160" height="110" /></a></td>
						</tr>
						<tr>
							  <td align="center" style="font-family:Verdana; font-size:13px; color:#3f3f3f;">&nbsp;'.$rs['mode_year'].'</td>
						</tr>
					</table>        
				</td>';
	}
	else
	{
		echo '<td valign="top">
					<table width="100%">
						<tr>
							<td align="center">
							<a href="http://erp.blauda.com/img/stock/'.$rs['var_image_file'].'"><img src="http://erp.blauda.com/img/stock/'.$rs['var_image_file'].'" width="160" height="110"></a></td>
						</tr>
						<tr>
							  <td align="center" style="font-family:Verdana; font-size:13px; color:#3f3f3f;">&nbsp;'.$rs['mode_year'].'</td>
						</tr>
					</table>        
				</td>';
		$j=0;
	}
	$j++;
}
*/
?>                        
                        
 <!----    --->                       
                       
						<tr>
							<td class="tdLeft">
								<div class="image_wrap">
									<a href="<?php echo WEB_ROOT; ?>/?p=detail&id=<?php echo $recent_model_row['stock_code']; ?>
										<?php if($recent_model_row['img_show']!="var_image_file") { ?>
										<img src="http://erp.blauda.com/img/stock/<?php echo $recent_model_row['stock_code']."/".$recent_model_row['img_show']; ?>" class="linkimg" />
										<?php } else { ?>
										<img src="<?php echo WEB_ROOT."/common/images/".$recent_model_row['img_show']; ?>" class="linkimg" />
										<?php } ?>
									</a>
								</div>
							</td>
							<td class="tdRight">
								<p class="page-name"><?php echo $recent_model_row['model']." ".$recent_model_row['mode_year']." ".$recent_model_row['grade']; ?> 2008 Auto <span>$<?php echo $recent_model_row['asesprice'] ?></span></p>
								<p class="page-desc"><?php echo $recent_model_row['memo'] ?></p><a href="<?php echo WEB_ROOT; ?>/?p=detail&id=<?php echo $recent_model_row['stock_code']; ?>&result=1" class="detail">Detail >></a>
							</td>
						</tr>
						<?php
						}
						?>
                        
                        
					</tbody>
				</table>
				<div class="holder"></div>
			</div>
		</div>
	</div>
	<?php include('common/php/footer.php'); ?>
</div>
</body>
</html>