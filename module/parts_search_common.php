<?
// 검색 변수 가져오기
if(isset($_REQUEST['CarSearch']))
{
	$CarSearch = $_REQUEST['CarSearch'];
}
else
{
	$CarSearch = Array();
}

// SqlWhere 구문 만들기
$SqlWheres = Array();
//$SqlWheres['menu_code'] = 'product';

$SqlWhere = '';
//$SqlWhere = ' WHERE `menu_code` = \'product\' ';

foreach($CarSearch as $SKey => $SVal)
{
	if($SVal != '*')
	{
		$SqlWheres[$SKey] = $SVal;
	}
}

$category = '';
if(isset($_REQUEST['category']))
{
	$SqlWheres['category'] = $_REQUEST['category'];
	$category = $_REQUEST['category'];
}

if(count($SqlWheres) > 0)
{
	foreach($SqlWheres as $WhereKey => $WhereVal)
	{
		if($WhereVal != '')
		{
			if($SqlWhere != '')
			{
				$SqlWhere .= ' AND ';
			}

			$SqlWhere .= ' `'.$WhereKey.'` = \''.$WhereVal.'\' ';
		}
	}

	if($SqlWhere != '')
	{
		$SqlWhere = ' WHERE '.$SqlWhere;
	}
}


if($SqlWhere != '')
{
	$SqlWhere = $SqlWhere.' AND `menu_code`=\'parts\' ';
}
else
{
	$SqlWhere = ' WHERE `menu_code`=\'parts\' ';
}

// 제품분류를 얻어오자
$ColumnRes = mysql_query('SHOW COLUMNS FROM `iw_bbs`');
$CarField = Array();
$CarAutoComplete = Array();
$FieldKeyStr = Array();

while($Column = mysql_fetch_object($ColumnRes))
{
	if(strpos($Column->Field, 'car_') === 0)
	{
		//var_dump($Column);
		if(in_array($Column->Field, Array('car_make', 'car_model', 'car_part' /*, 'car_body_type', 'car_model_year', 'car_transmission', 'car_stat' */)) === true)
		{
			$CarField[$Column->Field] = Array();
		}
		$CarAutoComplete[$Column->Field] = Array();

		$FieldKeyStr[$Column->Field] = ucfirst(str_replace('_', ' ', substr($Column->Field, 4)));


		$Query = 'SELECT `'.$Column->Field.'` FROM `iw_bbs` '.$SqlWhere.' GROUP BY `'.$Column->Field.'` ASC';
		//echo $Query;
		$FieldGroupRes = mysql_query($Query);
		if($FieldGroupRes)
		{
			while($FieldGroup = mysql_fetch_object($FieldGroupRes))
			{
				if($FieldGroup->{$Column->Field} != '')
				{
					if(isset($CarField[$Column->Field]))
					{
						$CarField[$Column->Field][] = $FieldGroup->{$Column->Field};
					}
					$CarAutoComplete[$Column->Field][] = $FieldGroup->{$Column->Field};
				}
			}
		}
	}
}

//$FieldValStr = Array();
//$FieldValStr['car_transmission']['auto']	= 'Auto';
//$FieldValStr['car_transmission']['manual']	= 'Manual';

//$FieldValStr['car_sale_yn']['Y']			= 'Yes';
//$FieldValStr['car_sale_yn']['N']			= 'No';

//$FieldValStr['car_stat']['new']				= 'New';
//$FieldValStr['car_stat']['used']			= 'Used';
?>

<script>
//<!--
// 자동완성
$(function() {
	var Values = Array();
	<?
	foreach($CarAutoComplete as $CKey => $CVal)
	{
		echo 'Values[\''.$CKey.'\'] = [';
		echo "\r\n";
		$OptionStr = '';
		foreach($CVal as $Option)
		{
			if($OptionStr != '')
			{
				$OptionStr .= ',';
			}
			$OptionStr .= '\''.str_replace('\'', '\\\'', $Option).'\'';
			$OptionStr .= "\r\n";
		}
		echo $OptionStr;
		echo '];';
		echo "\r\n";
	}
	?>
	$('*[AutocompleteYN="Y"]').after(function() {
		var Name = $(this).attr('name');
		Name = Name.substr(7, Name.length - 8);
		if(typeof(Values[Name]) == 'object')
		{
			$(this).autocomplete({
				source: Values[Name]
			});
		}
	});
	$('*[DupCheckYN="Y"]').keyup(function() {
		var Obj = $(this);
		var Name = Obj.attr('name');
		var Value = Obj.attr('value');
		var DupYN = false;

		Name = Name.substr(7, Name.length - 8);

		$.each(Values[Name], function(key, val){
			if(val == Value)
			{
				DupYN = true;
			}
		});



		if(DupYN)
		{
			Obj
				.css('color', 'red');
		}
		else
		{
			Obj
				.css('color', '');
		}

	});
});
//-->
</script>