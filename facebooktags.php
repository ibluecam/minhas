<?php 
require_once('libraries/scrapwebdata/simple_html_dom.php');
$pageURL = 'http';
if(isset($_SERVER["HTTPS"])){
    if ($_SERVER["HTTPS"] == "on"){
        $pageURL .= "s";
    }
}
$pageURL .= "://";

$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

//echo $pageURL;

$opts = array('http'=>array('header' => "User-Agent:Firefox/1.0\r\n"));
$context = stream_context_create($opts);
$pageURL = str_replace('facebooktags.php', '', $pageURL);

$html = file_get_contents($pageURL,false,$context);
echo $html;