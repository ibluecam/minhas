<?php

/* ------------------------------------------------

 * 파일명 : admin.php

 * 개  요 : 관리자 전체 Controller

  ------------------------------------------------ */
include_once('admin.php');
//require 'controllers/resize_class/resize_class.php';
class Admin_json extends Admin {
    //var $fob_cost_plus = 100;
    function __construct() {
        parent::__construct();
    }
    function test_admin_json(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            echo "authenticated";
    }
    function clear_newsletter(){
        ob_clean();
        $json_data=array();
        if(isset($_SESSION['idx_for_newsletter'])) unset($_SESSION['idx_for_newsletter']);
        $json_data = json_encode($json_data);
        echo $json_data;
    }
    function delete_idx_from_newsletter(){
        ob_clean();
        $json_data=array();
        $exist_idxs = array();
        if(isset($_POST['idx_chk'])) $idxs = $_POST['idx_chk']; else $idxs = array();
        if(!isset($_SESSION['idx_for_newsletter'])) $idx_for_newsletter=''; else $idx_for_newsletter=$_SESSION['idx_for_newsletter'];
        if(!empty($idx_for_newsletter)){
            $exist_idxs = explode("^", $idx_for_newsletter);
        }
        
        if(count($idxs)>0){
            
            foreach($idxs as $idx){
                if(!empty($idx)){
                    
                    if(($key = array_search($idx, $exist_idxs)) !== false) {
                        unset($exist_idxs[$key]);
                    }
                }   
            }
        }
        
        $_SESSION['idx_for_newsletter'] = implode('^', $exist_idxs);
        if(!empty($_SESSION['idx_for_newsletter'])){
            $json_data = explode('^',$_SESSION['idx_for_newsletter']);
        }
        $json_data = json_encode($json_data);
        echo $json_data;
    }


    function insert_idx_for_newsletter(){
        ob_clean();
        $json_data=array();
        $exist_idxs = array();
        if(isset($_POST['idx'])) $idxs = $_POST['idx']; else $idxs = array();
        if(!isset($_SESSION['idx_for_newsletter'])) $idx_for_newsletter=''; else $idx_for_newsletter=$_SESSION['idx_for_newsletter'];
        if(!empty($idx_for_newsletter)){
            $exist_idxs = explode("^", $idx_for_newsletter);
        }
        
        if(count($idxs)>0){
            
            foreach($idxs as $idx){
                if(!empty($idx)){
                    if(!in_array($idx, $exist_idxs)){
                        $exist_idxs[] = $idx;
                    }
                }   
            }
        }
        
        $_SESSION['idx_for_newsletter'] = implode('^', $exist_idxs);
        if(!empty($_SESSION['idx_for_newsletter'])){
            $json_data = explode('^',$_SESSION['idx_for_newsletter']);
        }
        $json_data = json_encode($json_data);
        echo $json_data;

    }

    /**** GET METHOD: country ***/
    function port_list(){
        if ($this->_login_check()&&$this->_isAdminGroup()){
            $port_country = $this->input->get_post('country');
            $ports = $this->shipping->select_port($port_country);
            echo json_encode($ports);
        }
    }
    
    function insert_email_to_list(){

        if ($this->_login_check()&&$this->_isAdminGroup()){
            $insert_result = array();
            $list_name = $this->input->get_post('list');
            $member_nos = $this->input->post('idx');
            $members = $this->member->select_by_member_nos($member_nos);
            $data = array();
            foreach($members as $member){
                $data[] = array('email'=>$member->email, 'name'=>$member->member_first_name.' '.$member->member_last_name);
            }
            
            $insert_result = $this->mo_email->insert_email_to_list($list_name, $data);
            echo json_encode($insert_result);
            
        }
        
    }

    /*** UPDATE CARS FROM SELLERS'S WEB WITH ONE CLICK ***/
    function one_click_scrape(){
        ini_set('max_execution_time', 3600);
        //$this->update_cars_from_primegt();
        //$this->update_cars_from_broomjapan();
    }

    /*** request GET website_name ***/
    function get_number_of_page(){
        ob_clean();
        header('Content-Type: application/json');
        require_once 'libraries/class-scrap-broomjapan.php';
        require_once 'libraries/class-scrap-primegt.php';
        $total_page = 0;
        $website_name = $_GET['website_name'];
        /*** CHANGE FROM HERE ***/
        if($website_name=='broomjapan'){
            $total_page = ScrapBroomjapan::getNumberOfPage();//1
        }elseif($website_name=='primegt'){
            $total_page = ScrapPrimegt::getNumberOfPage();//1
        }
        /*** NO CHANGE FROM HERE ***/
        $result['total_page'] = $total_page;
        echo json_encode($result);
    }

    /*** request GET website_name, page_num ***/
    function get_all_item_from_page(){
        ob_clean();
        header('Content-Type: application/json');
        require_once 'libraries/class-scrap-broomjapan.php';
        require_once 'libraries/class-scrap-primegt.php';
        $website_name = $_GET['website_name'];
        $page_num = $_GET['page_num'];
        /*** CHANGE FROM HERE ***/
        if($website_name=='broomjapan'){
            $items = ScrapBroomjapan::getAllItemsFromPage($page_num);
        }elseif($website_name=='primegt'){
            $items = ScrapPrimegt::getAllItemsFromPage($page_num);
        }
        /*** NO CHANGE FROM HERE ***/
        $result['items'] = $items;
        echo json_encode($result);
    }
    /*** request GET website_name, id ***/
    function get_item_detail_by_id(){
        ob_clean();
        header('Content-Type: application/json');
        require_once 'libraries/class-scrap-broomjapan.php';
        require_once 'libraries/class-scrap-primegt.php';
        $website_name = $_GET['website_name'];
        $id = $_GET['id'];
        /*** CHANGE FROM HERE ***/
        if($website_name=='broomjapan'){
            $item_detail = ScrapBroomjapan::getItemDetailById($id);//3
        }elseif($website_name=='primegt'){
            $item_detail = ScrapPrimegt::getItemDetailById($id);//3
        }
        /*** NO CHANGE FROM HERE ***/
        $result['item_detail'] = $item_detail;
        echo json_encode($result, JSON_UNESCAPED_SLASHES);
    }
    /*** request POST chassis ***/
    function count_by_chassis(){
        ob_clean();
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        $chassis_details = array();
        $chassis = $_POST['chassis'];
        if(!empty($chassis)){
            $chassis_arr = explode('^', $chassis);
            if(count($chassis_arr)>0){
                $chassis_details = $this->bbs->count_by_chassis($chassis_arr);
            }
        }
            
        $result['chassis_details'] = $chassis_details;
        echo json_encode($result);
    }

    function get_seller_detail_by_webname(){
        ob_clean();
        header('Content-Type: application/json');
        $website_name = $_GET['website_name'];
        $seller_detail = $this->mo_webscrape->get_member_detail_by_webname($website_name);//5
        $result['seller_detail'] = $seller_detail;
        echo json_encode($result, JSON_UNESCAPED_SLASHES);
    }
    function scrape_update_soldout(){
        ob_clean();
        header('Content-Type: application/json');
        $valid_token = "eda772653664bf5ec2cf00435bfee74b";

        $chassis =  $_POST['chassis'];
        $token = $_POST['token'];
        $car_owner = $_POST['car_owner'];
        $chassis_arr = explode('^', $chassis);
        if($token == $valid_token){
            $this->bbs->update_status_beside_chassis($chassis_arr, $car_owner, 'soldout');
        }
       
    }
    function scrape_update_all(){
        ob_clean();
        header('Content-Type: application/json');
        $valid_token = "eda772653664bf5ec2cf00435bfee74b";
        /*** GET REQUEST ***/
        $json_data = $_POST['json_data'];
        $json_where = $_POST['json_where'];
        $token = $_POST['token'];
        /*** CONVERT JSON TO ARRAY ***/

        $data = json_decode($json_data, true);
        $where = json_decode($json_where, true);
        /*** PROCESS ***/
        if($token == $valid_token){
            // if(!empty($data['car_fob_cost'])){
            //     if(!empty($data['old_fob_cost'])){
            //         $data['old_fob_cost'] += $this->fob_cost_plus;
            //     }
            //     $data['car_fob_cost'] += $this->fob_cost_plus;
            // }else{
            //     $data['car_fob_cost'] =  0;
            // }
            $this->bbs->adm_scrape_update($data, $where);//6
            $result['status'] = "success";
            $result['message'] = "The car, ".$where['car_chassis_no']." has been updated!";
        }else{
            $result['status'] = "fail";
            $result['message'] = "Invalid token!";
        }
        echo json_encode($result);
    }


   

    function scrape_insert_all(){
        ob_clean();
        header('Content-Type: application/json');
        $valid_token = "eda772653664bf5ec2cf00435bfee74b";
        /*** GET REQUEST ***/
        $json_data = $_POST['json_data'];
        $token = $_POST['token'];
        /*** CONVERT JSON TO ARRAY ***/
        $data = json_decode($json_data, true);

        /*** PROCESS ***/
        if($token == $valid_token){
            // if(!empty($data['car_fob_cost'])){
            //     if(!empty($data['old_fob_cost'])){
            //         $data['old_fob_cost'] += $this->fob_cost_plus;
            //     }
            //     $data['car_fob_cost'] += $this->fob_cost_plus;
            // }else{
            //     $data['car_fob_cost'] =  0;
            // }
            $this->bbs->safe_insert($data);//6
            $result['status'] = "success";
            $result['message'] = "The car, ".$data['car_chassis_no']." has been inserted!";
        }else{
            $result['status'] = "fail";
            $result['message'] = "Invalid token!";
        }
        echo json_encode($result);
    }
    function scrape_upload_images(){
        ob_clean();
        header('Content-type: application/json');

        $chassis_no = $_POST['chassis_no'];
        $imploded_images = $_POST['images'];
        $token = $_POST['token'];
        $valid_token = "eda772653664bf5ec2cf00435bfee74b";
        $images = explode('^', $imploded_images);
        $arr=array();
        $count_image=0;
        if($token==$valid_token){
            $products = $this->bbs->get_detail_by_chassis($chassis_no);

            $product_id = $products[0]->idx;
            $owner = $products[0]->car_owner;
            $data=array();
            $count_image = $this->bbs->count_image(array('bbs_idx'=>$product_id));
            //$cloud_options = array('tags'=>array('item', 'test'));
            //$count_image = $this->bbs->count_image(array('bbs_idx'=>$product_id));
            //if($count_image==0&&!empty($product_id)){
                
               
                if($count_image==0&&!empty($product_id)){
                    $cloud_options = array('tags'=>array('item'));
                    $i=0;
                    foreach($images as $image){
                        // $upload_result = \Cloudinary\Uploader::upload($image, $cloud_options);
                        // /***GET BASE URL***/
                        // $exp_url = explode('/', $upload_result['secure_url']);
                        // $exp_last_key = count($exp_url)-1;
                        // $exp_url[$exp_last_key]='';
                        // unset($exp_url[$exp_last_key-1]);
                        // $data['base_url'] = implode('/', $exp_url);
                        // $data['bbs_idx'] = $product_id;
                        // $data['public_id']          = $upload_result['public_id'];
                        // $data['resource_type']      = $upload_result['resource_type'];
                        // $data['secure_url']         = $data['base_url'].$data['public_id'];
                        // $data['original_filename']  = basename($image);
                        // $data['sort'] = $i;
                        $image_name = md5($image);
                        /*** MAIN IMAGE ***/
                        $content = file_get_contents($image);
                        file_put_contents('uploads/cars/'.$image_name, $content);
                        $mime_image = getimagesize('uploads/cars/'.$image_name); 
                        $ext = '';
                        if($mime_image['mime']=='image/gif'){
                            $ext = '.gif';
                        }elseif($mime_image['mime']=='image/jpeg'){
                            $ext = '.jpg';
                        }elseif($mime_image['mime']=='image/png'){
                            $ext = '.png';
                        }elseif($mime_image['mime']=='image/bmp'){
                            $ext = '.bmp';
                        }
                        if(!empty($ext)){
                            /** THUMBNAIL ***/
                            rename('uploads/cars/'.$image_name, 'uploads/cars/'.$image_name.$ext);
                            $resizeObj = new resize('uploads/cars/'.$image_name.$ext); 
                            $resizeObj->resizeImage(200, 140, 'crop');
                            $resizeObj->saveImage('uploads/cars/thumb/'.$image_name.$ext,100); 
                            $data = array(
                                'base_url'          =>'/uploads/cars/',
                                'bbs_idx'           =>$product_id,
                                'public_id'         =>$image_name.$ext,
                                'resource_type'     =>'image',
                                'secure_url'        =>'',
                                'original_filename' =>$image_name.$ext,
                                'sort'              =>$i
                            );


                            $batch_data[] = $data;
                            $i++;
                        }else{
                            unlink('uploads/cars/'.$image_name);
                        }
                    }
                    $count_image = $this->bbs->insert_image_batch($batch_data);
                        

                }
                //$this->insertImageSQLite($batch_data);
                //$count_image = $this->bbs->count_image(array('bbs_idx'=>$product_id));
            //}
            
        }
        $arr['insert_count'] = $count_image;    
        // //var_dump($data);
        $json = json_encode($arr);
        echo $json;
    }
    private function update_cars_from_broomjapan(){
        require_once 'libraries/class-scrap-broomjapan.php';
        /*** INIT VARIABLE ***/
        $all_items = array(
            'id'=>array(),
            'status'=>array(),
            'old_price'=>array()
        );
        $exist_data = array(
            'chassis_no'=>array(),
            'idx'=>array(),
            'image_count'=>array()
        );
        $item_detail = array();
        $chassis_arr = array();
        $exist_chassis = array();
        $ignore_chassis = array();
        $idx_of_chassis = array();
        $website_name = 'broomjapan';
        /*** 1. Count total page ***/
        $total_page = ScrapBroomjapan::getNumberOfPage();//1

        /*** 2. Loop pages for all items ***/
        for($i=1;$i<=1;$i++){
            $items = ScrapBroomjapan::getAllItemsFromPage($i);//2
            $all_items['id'] = array_merge($all_items['id'], $items['id']);
            $all_items['status'] = array_merge($all_items['status'], $items['status']);
            $all_items['old_price'] = array_merge($all_items['old_price'], $items['old_price']);
        }
        /*** 3. Loop items for item detail ***/
        foreach($all_items['id'] as $key=>$id){
            $scrape_detail = ScrapBroomjapan::getItemDetailById($id);//3
            $scrape_detail['status'] = $all_items['status'][$key];
            $scrape_detail['old_price'] = $all_items['old_price'][$key];
            $item_detail[] = $scrape_detail;
            $chassis_arr[] =  $scrape_detail['chassis_no'];
        }
        //var_dump($item_detail);
        $chassis_details = $this->bbs->count_by_chassis($chassis_arr);//4
        foreach($chassis_details as $detail){
            $exist_chassis[] = $detail->car_chassis_no;
            $ignore_chassis[$detail->car_chassis_no] = $detail->ignore_scan_update;
            $idx_of_chassis[$detail->car_chassis_no] = $detail->idx;
        }
        /*** PRE DEFINE VARIABLE ***/
        $member_detail = $this->mo_webscrape->get_member_detail_by_webname($website_name);//5
        
        foreach($item_detail as $c){
            $c['country'] = $member_detail->member_country;
            $c['car_owner'] = $member_detail->member_no;
            $c['updated_dt'] = date("Y-m-d H:i:s");
            $car = $this->generate_true_scrape_detail($c);
            
            if(in_array($car['car_chassis_no'], $exist_chassis)){
                
                $chassis = $car['car_chassis_no'];
                if($ignore_chassis[$chassis]==0){
                    $where['car_chassis_no'] = $chassis;
                    $car['idx'] = $idx_of_chassis[$chassis];
                    echo "1";
                    $this->bbs->adm_update($car, $where);//6
                    echo "2";
                }
            }else{
                $car_idx = $this->bbs->insert($car);
            }
        }
        
    }
    private function update_cars_from_primegt(){
        require_once 'libraries/class-scrap-primegt.php';
        /*** INIT VARIABLE ***/
        $all_items = array(
            'id'=>array(),
            'status'=>array(),
            'old_price'=>array()
        );
        $exist_data = array(
            'chassis_no'=>array(),
            'idx'=>array(),
            'image_count'=>array()
        );
        $item_detail = array();
        $chassis_arr = array();
        $exist_chassis = array();
        $ignore_chassis = array();
        $idx_of_chassis = array();
        $website_name = 'primegt';
        /*** 1. Count total page ***/
        $total_page = ScrapPrimegt::getNumberOfPage();

        /*** 2. Loop pages for all items ***/
        for($i=1;$i<=1;$i++){
            $items = ScrapPrimegt::getAllItemsFromPage($i);
            $all_items['id'] = array_merge($all_items['id'], $items['id']);
            $all_items['status'] = array_merge($all_items['status'], $items['status']);
            $all_items['old_price'] = array_merge($all_items['old_price'], $items['old_price']);
        }
        /*** 3. Loop items for item detail ***/
        foreach($all_items['id'] as $key=>$id){
            $scrape_detail = ScrapPrimegt::getItemDetailById($id);
            $scrape_detail['status'] = $all_items['status'][$key];
            $scrape_detail['old_price'] = $all_items['old_price'][$key];
            $item_detail[] = $scrape_detail;
            $chassis_arr[] =  $scrape_detail['chassis_no'];
        }
        //var_dump($item_detail);
        $chassis_details = $this->bbs->count_by_chassis($chassis_arr);
        foreach($chassis_details as $detail){
            $exist_chassis[] = $detail->car_chassis_no;
            $ignore_chassis[$detail->car_chassis_no] = $detail->ignore_scan_update;
            $idx_of_chassis[$detail->car_chassis_no] = $detail->idx;
        }
        /*** PRE DEFINE VARIABLE ***/
        $member_detail = $this->mo_webscrape->get_member_detail_by_webname($website_name);
        
        foreach($item_detail as $c){
            $c['country'] = $member_detail->member_country;
            $c['car_owner'] = $member_detail->member_no;
            $c['updated_dt'] = date("Y-m-d H:i:s");
            $car = $this->generate_true_scrape_detail($c);
            
            if(in_array($car['car_chassis_no'], $exist_chassis)){
                $chassis = $car['car_chassis_no'];
                if($ignore_chassis[$chassis]==0){
                    $where['car_chassis_no'] = $chassis;
                    $car['idx'] = $idx_of_chassis[$chassis];
                    echo "1";
                    $this->bbs->adm_update($car, $where);
                    echo "2";
                }
            }else{
                $car_idx = $this->bbs->insert($car);
            }
        }
        
    }
    // private function generate_true_scrape_detail($sdata){
    //     $allowed_drive = array('4WD', '2WD');
    //     $transmission_dic = array('AT'=>"Auto", 'MT'=>'Manual', 'CVT'=>'Auto');
    //     $valid_trans = false;
    //     //$update_mode = $this->ci->input->get_post('update_mode');
    //     //$website_name = $sdata['website_name'];
    //     //$data['idx'] = $sdata['idx'];
    //     $data['car_chassis_no'] = $sdata['chassis_no'];
    //     $data['icon_status'] = $sdata['status'];
    //     $data['car_body_type'] = $sdata['product_type'];
    //     $data['car_make'] = $sdata['make'];
    //     $data['car_color'] = $sdata['exterior_color'];
    //     $data['car_model'] = $sdata['model'];
    //     $data['car_steering'] = $sdata['steering'];
    //     $data['car_drive_type'] = $sdata['drive'];
    //     $data['car_transmission'] = $sdata['transmission'];
    //     $data['car_cc'] = $sdata['engine_volume'];
    //     $data['door'] = $sdata['door'];
    //     $data['car_fuel'] = $sdata['fuel_type'];
    //     $data['car_seat'] = $sdata['number_passenger'];
    //     $data['car_length'] = $sdata['car_length'];
    //     $data['car_width'] = $sdata['car_width'];
    //     $data['car_height'] = $sdata['car_height'];
    //     $data['first_registration_year'] = $sdata['registrationyear'];
    //     if(isset($sdata['registrationmonth'])){
    //         $data['first_registration_month'] = $sdata['registrationmonth'];
    //     }
       
    //     $data['car_mileage'] = $sdata['mileage'];
    //     $car_fob_cost = $sdata['price'];
    //     $data['car_fob_currency'] = $sdata['currency'];

    //     $data['car_model_year'] = $data['first_registration_year'];
    //     $data['manu_year'] = $sdata['manu_year'];
    //     $data['manu_month'] = $sdata['manu_month'];
    //     $data['icon_new_yn'] = $sdata['icon_new_yn'];
    //     $data['old_fob_cost'] = $sdata['old_price'];

       
    //     /****** GET CAR COUNTRY, CAR OWNER, UPDATED DATE ******/
    //     $data['country'] = $sdata['country'];
    //     $data['car_owner'] = $sdata['car_owner'];
    //     $data['updated_dt'] = $sdata['updated_dt'];

    //     if(!in_array($data['car_drive_type'], $allowed_drive)){
    //         $data['car_drive_type']='2WD';
    //     }

    //     foreach($transmission_dic as $key=>$value){
    //         if (strpos($data['car_transmission'],$key) !== false) {
    //             $valid_trans = true;
    //             $data['car_transmission'] = $value;
    //         }
    //     }
    //     if($valid_trans==false){
    //         unset($data['car_transmission']);
    //     }
    //     if(!empty($car_fob_cost)){
    //         $data['old_fob_cost']+=$this->fob_cost_plus;
    //         $data['car_fob_cost'] =  $car_fob_cost+$this->fob_cost_plus;
    //     }else{
    //         $data['car_fob_cost'] =  $car_fob_cost;
    //     }
    //     $data['car_fob_cost_orig'] =  $car_fob_cost;

    //     return $data;
    // }



}

?>