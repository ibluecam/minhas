<?php

/* ------------------------------------------------

 * 파일명 : admin.php

 * 개  요 : 관리자 전체 Controller

  ------------------------------------------------ */
include_once('user.php');
class User_json extends User {
    function __construct() {
        parent::__construct();
        $this->load->model('mo_parse', 'mo_parse');
    }
    
    /**** GET METHOD: make_id ***/
    function json_model_list(){
        $make_id = $this->input->get_post('make_id');
        $where['make_id'] = $make_id;
        $result= $this->mo_bbs->getModel_table($where);
        echo json_encode($result);
    }
    /*** COUNT LATEST UPDATED CAR BY LATEST DATE
        REQUEST GET:
            - latest_date: "2015-07-18 15:46:40"
    ***/
    function count_updated_car(){
        header('Access-Control-Allow-Origin: *');
        $latest_date = $this->input->get_post('latest_date');
        $result = $this->mo_bbs->count_updated_car($latest_date);
        $json = array('count_updated_car'=>$result);
        echo json_encode($json);
    }

    /*** COUNT LATEST UPDATED CAR BY LATEST DATE
        REQUEST GET:
            - latest_date: "2015-07-18 15:46:40"
            - start_from: "20"
    ***/
    function select_updated_car(){
        header('Access-Control-Allow-Origin: *');
        $latest_date = $this->input->get_post('latest_date');
        $start_from = $this->input->get_post('start_from');
        $result = $this->mo_bbs->select_updated_car($latest_date, $start_from);
        $json = array('cars'=>$result);
        echo json_encode($json);
    }

    function insert_to_parse(){
        header('Access-Control-Allow-Origin: *');
        $result = false;
        $data = $_POST;
        $idx = $data['idx'];
        
        //if ($this->_login_check()&&$this->_isAdminGroup()){
            
            if($this->mo_parse->insert_or_update($idx)){
                $result=true;
            }
        //}
        echo json_encode($result);
    }


    function select_distinct_make(){
        $make_list = $this->mo_bbs->select_distinct_make();
        echo json_encode($make_list, JSON_UNESCAPED_SLASHES);
    }
}

?>