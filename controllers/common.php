<?php

/* ------------------------------------------------
 * 파일명 : common.php
 * 개  요 : 공통 Controller
  ------------------------------------------------ */
?>
<?php

class Common extends Controller {

	var $data;

//Constructor
	function __construct() {
		parent::Controller();

		/* ------------------------------------------------------------
		  - Model Load
		  ------------------------------------------------------------ */
		$this->load->model('mo_common', 'common');


		/* ------------------------------------------------------------
		  - Library Load
		  ------------------------------------------------------------ */
		$this->load->library('iwc_common'); //IWC 공통 클래스


		$this->ci = &get_instance();
	}

	//Index
	function index() {

	}

	// 썸네일 자동생성되는거...
	function AutoThumbnail($file_name, $mcd) {
		// 썸네일 인거같아
		if(substr($file_name, 32, 1) == '_') {
			$file_name_exp = explode('_', $file_name);
			list(,$file_name_ext) = explode('.', $file_name_exp[1]);
			$file_name_exp[1] = substr($file_name_exp[1], 0, -(strlen($file_name_ext)+1));

			$FileFullPathOrg = $this->config->item('upload_path') . $mcd . DIRECTORY_SEPARATOR . iconv("UTF-8", "EUC-KR", $file_name_exp[0].'.'.$file_name_ext);
			$FileFullPathThumb = $this->config->item('upload_path') . $mcd . DIRECTORY_SEPARATOR . iconv("UTF-8", "EUC-KR", $file_name);

			// 썸네일 원본발견!! 썸네일 맞넹 ㅋㅋㅋ
			if (file_exists($FileFullPathOrg)) {

				// 근데 썸네일 파일 없음, 만들어야함 -ㅅ-;
				if(!file_exists($FileFullPathThumb)) {

					// 필요한 놈들 로드~~~
					$this->ci = &get_instance();
					$this->ci->load->library('image_lib', NULL); 		//이미지

					$prop = $this->ci->image_lib->get_image_properties($FileFullPathOrg, TRUE);

					//원본이 제작하려고 하는 섬네일보다 큰경우만 실행 한다.
					if (((integer) $prop['width']) > $file_name_exp[1]) {
						$config['image_library'] = 'GD2';
						$config['source_image'] = $FileFullPathOrg;
						$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;
						$config['thumb_marker'] = '_' . $file_name_exp[1];
						$config['quality'] = '100%';
						$config['master_dim'] = 'width';
						$config['width'] = $file_name_exp[1];
						$config['height'] = $file_name_exp[1];

						$this->ci->image_lib->clear();			 		// clear settings
						$this->ci->image_lib->initialize($config); 		// reinitialize
						$this->ci->image_lib->resize();

						// 에러!! 나면 원본복사함
						if($this->ci->image_lib->display_errors()) {
							//echo '에러'.$this->ci->image_lib->display_errors();
							copy($FileFullPathOrg, $FileFullPathThumb);
						}
					} else { // 아니라면 걍 복사
						copy($FileFullPathOrg, $FileFullPathThumb);
					}
				}
			}
		}
	}

	//파일 다운로드
	function dwnld() {

		$file_name = $this->input->get_post('file_name'); //실제저장명
		$orig_name = $this->input->get_post('orig_name'); //원본파일명
		$mcd = $this->input->get_post('mcd');

		//echo $orig_name;
		// 파일 없는데....

		$this->AutoThumbnail($file_name, $mcd);

		if (file_exists($this->config->item('upload_path') . $mcd . DIRECTORY_SEPARATOR . iconv("UTF-8", "EUC-KR", $file_name))) {
			$file_size = filesize($this->config->item('upload_path') . $mcd . DIRECTORY_SEPARATOR . iconv("UTF-8", "EUC-KR", $file_name));
			// 아래 로직 메모리 졸라 처먹음..
			//$file_name = file_get_contents($this->config->item('upload_path').$mcd."/".iconv( "UTF-8", "EUC-KR", $file_name ));
			//force_download(urlencode($orig_name), $file_name);

			header('Content-Type: application/force-download');
			header('Content-Disposition: attachment; filename="' . $orig_name . '"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header("Content-Transfer-Encoding: binary");
			header('Pragma: public');
			header("Content-Length: " . $file_size);
			readfile($this->config->item('upload_path') . $mcd . DIRECTORY_SEPARATOR . iconv("UTF-8", "EUC-KR", $file_name));
			exit;
		} else {
			show_error("파일이 삭제되었거나 존재 하지 않습니다.");
		}
	}
	//파일 다운로드
	function download() {

		$file_name = $this->input->get_post('file_name'); //실제저장명
		$orig_name = $this->input->get_post('orig_name'); //원본파일명
		$mcd = $this->input->get_post('mcd');

		//echo $orig_name;
		// 파일 없는데....

		$this->AutoThumbnail($file_name, $mcd);

		if (file_exists($this->config->item('upload_path') . $mcd . DIRECTORY_SEPARATOR . iconv("UTF-8", "EUC-KR", $file_name))) {
			$file_size = filesize($this->config->item('upload_path') . $mcd . DIRECTORY_SEPARATOR . iconv("UTF-8", "EUC-KR", $file_name));
			// 아래 로직 메모리 졸라 처먹음..
			//$file_name = file_get_contents($this->config->item('upload_path').$mcd."/".iconv( "UTF-8", "EUC-KR", $file_name ));
			//force_download(urlencode($orig_name), $file_name);

			header('Content-Type: application/force-download');
			header('Content-Disposition: attachment; filename="' . $orig_name . '"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header("Content-Transfer-Encoding: binary");
			header('Pragma: public');
			header("Content-Length: " . $file_size);
			readfile($this->config->item('upload_path') . $mcd . DIRECTORY_SEPARATOR . iconv("UTF-8", "EUC-KR", $file_name));
			exit;
		} else {
			
			show_error("파일이 삭제되었거나 존재 하지 않습니다.");
		}
	}
	
	
	//이미지 다운로드
	function image_download() {
		$file_name = $this->input->get_post('file_name'); //실제저장명
		$orig_name = $this->input->get_post('orig_name'); //원본파일명
		$mcd = $this->input->get_post('mcd');

		$this->AutoThumbnail($file_name, $mcd);

		//echo $orig_name;

		if (file_exists($this->config->item('upload_path') . $mcd . DIRECTORY_SEPARATOR . iconv("UTF-8", "EUC-KR", $file_name))) {
			$file_name = file_get_contents($this->config->item('upload_path') . $mcd . "/" . iconv("UTF-8", "EUC-KR", $file_name));
			echo "<img src=\"/?c=common&m=download&file_name=" . $file_name . "&orig_name=" . $orig_name . "&mcd=" . $mcd . "\" border=\"0\" alt=\"" . $orig_name . "\">";
		} else {
			show_error("파일이 삭제되었거나 존재 하지 않습니다.");
		}
	}

	//아이디 중복 체크
	function dup_chk_member_id() {
		$member_id = $this->input->get_post('member_id');

		$cnt = $this->common->select_chk_member_id($member_id);

		//해당 메뉴코드가 존재한다면...
		if ($cnt > 0) {
			echo 'FAIL';
		} else {
			echo 'SUCCESS';
		}
	}

	//아이디 중복 체크 + 이메일 체크
	function dup_chk_member_id2() {
		$member_id = $this->input->get_post('member_id');

		$cnt = $this->common->select_chk_member_id($member_id);

		$this->load->helper('email');

		//해당 메뉴코드가 존재한다면...
		if ($cnt > 0) {
			echo 'FAIL';
		} else if(valid_email($member_id) == false) {
			//이메일 형식 아님
			echo 'FAIL2';
		}else {
			echo 'SUCCESS';
		}
	}

	function send_verification_mail() {

		$member_id = $this->input->get_post('member_id');
	    $generatedKey = sha1(mt_rand(10000,99999).time().$member_id);


		if(false) {
		    $to=$email;
		    $from='noreply@blauda.com';
		    $subject='Verify your Email Address - iblauda.com';
		    $headers = 'From: webmaster@example.com' . "\r\n" .
	    		'Reply-To: webmaster@example.com' . "\r\n" .
	    		'X-Mailer: PHP/' . phpversion();
		    $message=''.$generatedKey;//ttp://example.com/activation.php?email='.$email.'&key='.$generatedKey.'';
		    mail($to,$subject,$message,$headers);

			echo $this->email->print_debugger();

		} else {
			$this->ci->load->library('sendmail');		//메일

			$send_mail = 'ttcopy@blauda.com';
			$send_name = 'BLAUDA CO., LTD.';

			$to_mail = $member_id;											  //받는 이메일
			$subject = 'Verify your Email Address - iblauda.com';	 //제목


			$this->ci->data['generatedKey'] = $generatedKey;

			//이메일 본문
			$contents = $this->ci->load->view("user/member_login/LOGIN/member_certification_code_mail", $this->ci->data, true);

			//메일발송
			$this->ci->sendmail->clear(TRUE);   //모든 이메일 변수들을 초기화  TRUE 시 첨부파일도 초기화
			$this->ci->sendmail->setMailType("html");
			$this->ci->sendmail->setInit();
			$this->ci->sendmail->setFrom($send_mail, $send_name);
			$this->ci->sendmail->setTo($to_mail);
			$this->ci->sendmail->setSubject($subject);
			$this->ci->sendmail->setMessage($contents);
			$mail_result = $this->ci->sendmail->send(); //전송여부 TRUE FALSE 반환

			//var_dump($contents);
			//exit;
		}


	    echo $generatedKey;
	}

	// 요청온 키값에 따라 검사하기
	function verifyEmailAddress($email, $vkey){
	    msConnect();
	    $query=mysql_query("SELECT * FROM users WHERE email='$email' AND activation_key='$vkey'");

	    $fetch=mysql_fetch_array($query);
	    if($fetch['activation_status'] == 'Active'){
	        $output='Your Email has already been verified.';
	    }else{
	        $cnt=mysql_num_rows($query);
	        if($cnt=='1'){
	            $updateQ=mysql_query("UPDATE users SET activation_status='Active' WHERE email='$email' AND activation_key='$vkey'");
	            if($updateQ){
	                $output='Email Verified';
	            }else{
	                $output='System Faced an error while updating your status.';
	            }
	        }else{
	            $output='Unable to verify your email address.';
	        }
	    }
	    mysql_close();
	    return $output;
	}

	//우편번호 찾기
	function zipcode() {
		$dong = $this->input->get_post('dong');
		$fn = $this->input->get_post('fn'); //form name
		$result = '';

		func_set_data($this, 'fn', $fn);
		func_set_data($this, 'dong', $dong);

		if ($dong != '') {
			$result = $this->common->select_zipcode($dong);
		}
		func_set_data($this, 'result', $result);

		$this->load->view('common/zipcode', $this->data, false);
	}

	//쿠키 SET
	function cookie_set() {
		$idx = $this->input->get_post('idx');
		$close_type = $this->input->get_post('close_type');

		$cookie = array(
			'name' => 'POPUP_' . $idx,
			'value' => $idx,
			'expire' => 60 * 60 * 24 * (integer) $close_type,
			'domain' => '',
			'path' => '/',
			'prefix' => 'IW_',
		);

		set_cookie($cookie);

		$tmp = "<html>                                                                   ";
		$tmp .= "<head>                                                                   ";
		$tmp .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />";
		$tmp .= "<script language=\"javascript\"  type=\"text/javascript\">               ";
		$tmp .= "   self.close();                                                         ";
		$tmp .= "</script>                                                                ";
		$tmp .= "</head>                                                                  ";
		$tmp .= "<body></body>                                                            ";
		$tmp .= "</html>                                                                  ";

		echo $tmp;
	}

	//쿠키 GET
	function cookie_get() {
		$idx = $this->input->get_post('idx');

		return get_cookie('IW_POPUP_' . $idx);
	}

	//쿠키 DELETE
	function cookie_delete() {
		$idx = $this->input->get_post('idx');

		delete_cookie('IW_POPUP_' . $idx);
	}

	//전자결제결과 DB 처리 (신용카드, 계좌이체)
	function note_url() {
		$this->load->model('mo_pay', 'payment');

		/*
		 * 공통결제결과 정보
		 */
		$LGD_RESPCODE = "";			  // 응답코드: 0000(성공) 그외 실패
		$LGD_RESPMSG = "";			   // 응답메세지
		$LGD_MID = "";				   // 상점아이디
		$LGD_OID = "";				   // 주문번호
		$LGD_AMOUNT = "";				// 거래금액
		$LGD_TID = "";				   // 데이콤이 부여한 거래번호
		$LGD_PAYTYPE = "";			   // 결제수단코드
		$LGD_PAYDATE = "";			   // 거래일시(승인일시/이체일시)
		$LGD_HASHDATA = "";			  // 해쉬값
		$LGD_FINANCECODE = "";		   // 결제기관코드(카드종류/은행코드/이통사코드)
		$LGD_FINANCENAME = "";		   // 결제기관이름(카드이름/은행이름/이통사이름)
		$LGD_ESCROWYN = "";			  // 에스크로 적용여부
		$LGD_TIMESTAMP = "";			 // 타임스탬프
		$LGD_FINANCEAUTHNUM = "";		// 결제기관 승인번호(신용카드, 계좌이체, 상품권)

		/*
		 * 신용카드 결제결과 정보
		 */
		$LGD_CARDNUM = "";			   // 카드번호(신용카드)
		$LGD_CARDINSTALLMONTH = "";	  // 할부개월수(신용카드)
		$LGD_CARDNOINTYN = "";		   // 무이자할부여부(신용카드) - '1'이면 무이자할부 '0'이면 일반할부
		$LGD_TRANSAMOUNT = "";		   // 환율적용금액(신용카드)
		$LGD_EXCHANGERATE = "";		  // 환율(신용카드)

		/*
		 * 휴대폰
		 */
		$LGD_PAYTELNUM = "";			 // 결제에 이용된전화번호

		/*
		 * 계좌이체, 무통장
		 */
		$LGD_ACCOUNTNUM = "";			// 계좌번호(계좌이체, 무통장입금)
		$LGD_CASTAMOUNT = "";			// 입금총액(무통장입금)
		$LGD_CASCAMOUNT = "";			// 현입금액(무통장입금)
		$LGD_CASFLAG = "";			   // 무통장입금 플래그(무통장입금) - 'R':계좌할당, 'I':입금, 'C':입금취소
		$LGD_CASSEQNO = "";			  // 입금순서(무통장입금)
		$LGD_CASHRECEIPTNUM = "";		// 현금영수증 승인번호
		$LGD_CASHRECEIPTSELFYN = "";	 // 현금영수증자진발급제유무 Y: 자진발급제 적용, 그외 : 미적용
		$LGD_CASHRECEIPTKIND = "";	   // 현금영수증 종류 0: 소득공제용 , 1: 지출증빙용

		/*
		 * OK캐쉬백
		 */
		$LGD_OCBSAVEPOINT = "";		  // OK캐쉬백 적립포인트
		$LGD_OCBTOTALPOINT = "";		 // OK캐쉬백 누적포인트
		$LGD_OCBUSABLEPOINT = "";		// OK캐쉬백 사용가능 포인트

		/*
		 * 구매정보
		 */
		$LGD_BUYER = "";				 // 구매자
		$LGD_PRODUCTINFO = "";		   // 상품명
		$LGD_BUYERID = "";			   // 구매자 ID
		$LGD_BUYERADDRESS = "";		  // 구매자 주소
		$LGD_BUYERPHONE = "";			// 구매자 전화번호
		$LGD_BUYEREMAIL = "";			// 구매자 이메일
		$LGD_BUYERSSN = "";			  // 구매자 주민번호
		$LGD_PRODUCTCODE = "";		   // 상품코드
		$LGD_RECEIVER = "";			  // 수취인
		$LGD_RECEIVERPHONE = "";		 // 수취인 전화번호
		$LGD_DELIVERYINFO = "";		  // 배송지

		$LGD_RESPCODE = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_RESPCODE'));
		$LGD_MID = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_MID'));
		$LGD_OID = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_OID'));
		$LGD_AMOUNT = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_AMOUNT'));
		$LGD_HASHDATA = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_HASHDATA'));
		$LGD_TIMESTAMP = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_TIMESTAMP'));

		//DB 등록
		$inData = array();

		//공통
		$inData['LGD_RESPCODE'] = $LGD_RESPCODE;
		$inData['LGD_RESPMSG'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_RESPMSG'));
		$inData['LGD_MID'] = $LGD_MID;
		$inData['LGD_OID'] = $LGD_OID;
		$inData['LGD_AMOUNT'] = $LGD_AMOUNT;
		$inData['LGD_TID'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_TID'));
		$inData['LGD_PAYTYPE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_PAYTYPE'));
		$inData['LGD_PAYDATE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_PAYDATE'));
		$inData['LGD_HASHDATA'] = $LGD_HASHDATA;
		$inData['LGD_FINANCECODE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_FINANCECODE'));
		$inData['LGD_FINANCENAME'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_FINANCENAME'));
		$inData['LGD_ESCROWYN'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_ESCROWYN'));
		$inData['LGD_TIMESTAMP'] = $LGD_TIMESTAMP;
		$inData['LGD_TRANSAMOUNT'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_TRANSAMOUNT'));
		$inData['LGD_EXCHANGERATE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_EXCHANGERATE'));

		//신용카드
		$inData['LGD_CARDNUM'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CARDNUM'));
		$inData['LGD_CARDINSTALLMONTH'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CARDINSTALLMONTH'));
		$inData['LGD_CARDNOINTYN'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CARDNOINTYN'));
		$inData['LGD_FINANCEAUTHNUM'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_FINANCEAUTHNUM'));

		$inData['LGD_PAYTELNUM'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_PAYTELNUM')); // 결제에 이용된전화번호
		//계좌이체
		$inData['LGD_ACCOUNTNUM'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_ACCOUNTNUM'));
		$inData['LGD_CASTAMOUNT'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASTAMOUNT'));
		$inData['LGD_CASCAMOUNT'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASCAMOUNT'));
		$inData['LGD_CASFLAG'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASFLAG'));
		$inData['LGD_CASSEQNO'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASSEQNO'));
		$inData['LGD_CASHRECEIPTNUM'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASHRECEIPTNUM'));
		$inData['LGD_CASHRECEIPTSELFYN'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASHRECEIPTSELFYN'));
		$inData['LGD_CASHRECEIPTKIND'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASHRECEIPTKIND'));

		$inData['LGD_OCBSAVEPOINT'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_OCBSAVEPOINT'));
		$inData['LGD_OCBTOTALPOINT'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_OCBTOTALPOINT'));
		$inData['LGD_OCBUSABLEPOINT'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_OCBUSABLEPOINT'));

		$inData['LGD_BUYER'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYER'));
		$inData['LGD_PRODUCTINFO'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_PRODUCTINFO'));
		$inData['LGD_BUYERID'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYERID'));
		$inData['LGD_BUYERADDRESS'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYERADDRESS'));
		$inData['LGD_BUYERPHONE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYERPHONE'));
		$inData['LGD_BUYEREMAIL'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYEREMAIL'));
		$inData['LGD_BUYERSSN'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYERSSN'));
		$inData['LGD_PRODUCTCODE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_PRODUCTCODE'));
		$inData['LGD_RECEIVER'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_RECEIVER'));
		$inData['LGD_RECEIVERPHONE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_RECEIVERPHONE'));
		$inData['LGD_DELIVERYINFO'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_DELIVERYINFO'));

		if ($inData['LGD_PRODUCTCODE'] == '0001' || $inData['LGD_PRODUCTCODE'] == '0002' || $inData['LGD_PRODUCTCODE'] == '0003') {
			$inData['re_course_yn'] = $this->input->get_post('re_course_yn');	   //해당 강좌 재수강여부(Y:재수강, N:최초수강)
			$inData['course_status'] = 'Y';										  //수강상태(Y:정상, S:중지, N:완료)
			$inData['course_stop_cnt'] = '0';										  //수강중지횟수
			$inData['start_dt'] = func_get_date_time();						 //시작일
			if ($inData['re_course_yn'] != 'N') { //재수강이면...
				$inData['end_dt'] = func_add_date_time(func_get_date_time(), 30); //종료일
			} else {
				$inData['end_dt'] = func_add_date_time(func_get_date_time(), 60); //종료일
			}
		}

		$inData['created_dt'] = func_get_date_time();						 //등록일

		$LGD_MERTKEY = "bad462dcf36c12c88eb947ff9e1a22d6";  //LG 텔레콤에서 발급한 상점키로 변경해 주시기 바랍니다.

		$LGD_HASHDATA2 = md5($LGD_MID . $LGD_OID . $LGD_AMOUNT . $LGD_RESPCODE . $LGD_TIMESTAMP . $LGD_MERTKEY);

		/*
		 * 상점 처리결과 리턴메세지
		 *
		 * OK   : 상점 처리결과 성공
		 * 그외 : 상점 처리결과 실패
		 *
		 * ※ 주의사항 : 성공시 'OK' 문자이외의 다른문자열이 포함되면 실패처리 되오니 주의하시기 바랍니다.
		 */
		$resultMSG = "결제결과 상점 DB처리(NOTE_URL) 결과값을 입력해 주시기 바랍니다.";

		if ($LGD_HASHDATA2 == $LGD_HASHDATA) {	  //해쉬값 검증이 성공하면
			if ($LGD_RESPCODE == "0000") {			//결제가 성공이면
				/*
				 * 거래성공 결과 상점 처리(DB) 부분
				 * 상점 결과 처리가 정상이면 "OK"
				 */
				//if( 결제성공 상점처리결과 성공 ) resultMSG = "OK";
				$cnt = $this->payment->insert($inData);
				$resultMSG = "OK";
			} else {								 //결제가 실패이면
				/*
				 * 거래실패 결과 상점 처리(DB) 부분
				 * 상점결과 처리가 정상이면 "OK"
				 */
				//if( 결제실패 상점처리결과 성공 ) resultMSG = "OK";
				$cnt = $this->payment->insert($inData);
				$resultMSG = "OK";
			}
		} else {									//해쉬값 검증이 실패이면
			/*
			 * hashdata검증 실패 로그를 처리하시기 바랍니다.
			 */
			$resultMSG = "결제결과 상점 DB처리(NOTE_URL) 해쉬값 검증이 실패하였습니다.";
		}

		//header("Content-Type: text/html; charset=euc-kr");

		echo $resultMSG;
	}

	//전자결제 가상계좌(무통장) 결과 DB처리
	function cas_noteurl() {
		$this->load->model('mo_pay', 'payment');

		/*
		 * [상점 결제결과처리(DB) 페이지]
		 *
		 * 1) 위변조 방지를 위한 hashdata값 검증은 반드시 적용하셔야 합니다.
		 *
		 */

		//DB 등록
		$inData = array();

		$inData['LGD_RESPCODE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_RESPCODE'));			 // 응답코드: 0000(성공) 그외 실패
		$inData['LGD_RESPMSG'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_RESPMSG'));			  // 응답메세지
		$inData['LGD_MID'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_MID'));				  // 상점아이디
		$inData['LGD_OID'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_OID'));				  // 주문번호
		$inData['LGD_AMOUNT'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_AMOUNT'));			   // 거래금액
		$inData['LGD_TID'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_TID'));				  // LG텔레콤이 부여한 거래번호
		$inData['LGD_PAYTYPE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_PAYTYPE'));			  // 결제수단코드
		$inData['LGD_PAYDATE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_PAYDATE'));			  // 거래일시(승인일시/이체일시)
		$inData['LGD_HASHDATA'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_HASHDATA'));			 // 해쉬값
		$inData['LGD_FINANCECODE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_FINANCECODE'));		  // 결제기관코드(은행코드)
		$inData['LGD_FINANCENAME'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_FINANCENAME'));		  // 결제기관이름(은행이름)
		$inData['LGD_ESCROWYN'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_ESCROWYN'));			 // 에스크로 적용여부
		$inData['LGD_TIMESTAMP'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_TIMESTAMP'));			// 타임스탬프
		$inData['LGD_ACCOUNTNUM'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_ACCOUNTNUM'));		   // 계좌번호(무통장입금)
		$inData['LGD_CASTAMOUNT'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASTAMOUNT'));		   // 입금총액(무통장입금)
		$inData['LGD_CASCAMOUNT'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASCAMOUNT'));		   // 현입금액(무통장입금)
		$inData['LGD_CASFLAG'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASFLAG'));
		;			 // 무통장입금 플래그(무통장입금) - 'R':계좌할당, 'I':입금, 'C':입금취소
		$inData['LGD_CASSEQNO'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASSEQNO'));			 // 입금순서(무통장입금)
		$inData['LGD_CASHRECEIPTNUM'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASHRECEIPTNUM'));	   // 현금영수증 승인번호
		$inData['LGD_CASHRECEIPTSELFYN'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASHRECEIPTSELFYN'));	// 현금영수증자진발급제유무 Y: 자진발급제 적용, 그외 : 미적용
		$inData['LGD_CASHRECEIPTKIND'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_CASHRECEIPTKIND'));	  // 현금영수증 종류 0: 소득공제용 , 1: 지출증빙용
		$inData['LGD_PAYER'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_PAYER'));		   // 입금자명

		/*
		 * 구매정보
		 */
		$inData['LGD_BUYER'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYER'));				// 구매자
		$inData['LGD_PRODUCTINFO'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_PRODUCTINFO'));		  // 상품명
		$inData['LGD_BUYERID'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYERID'));			  // 구매자 ID
		$inData['LGD_BUYERADDRESS'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYERADDRESS'));		 // 구매자 주소
		$inData['LGD_BUYERPHONE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYERPHONE'));		   // 구매자 전화번호
		$inData['LGD_BUYEREMAIL'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYEREMAIL'));		   // 구매자 이메일
		$inData['LGD_BUYERSSN'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_BUYERSSN'));			 // 구매자 주민번호
		$inData['LGD_PRODUCTCODE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_PRODUCTCODE'));		  // 상품코드
		$inData['LGD_RECEIVER'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_RECEIVER'));			 // 수취인
		$inData['LGD_RECEIVERPHONE'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_RECEIVERPHONE'));		// 수취인 전화번호
		$inData['LGD_DELIVERYINFO'] = iconv("EUC-KR", "UTF-8", $this->input->get_post('LGD_DELIVERYINFO'));		 // 배송지

		if ($inData['LGD_PRODUCTCODE'] == '0001' || $inData['LGD_PRODUCTCODE'] == '0002' || $inData['LGD_PRODUCTCODE'] == '0003') {
			$inData['re_course_yn'] = $this->input->get_post('re_course_yn');	   //해당 강좌 재수강여부(Y:재수강, N:최초수강)
			$inData['course_status'] = 'Y';										  //수강상태(Y:정상, S:중지, N:완료)
			$inData['course_stop_cnt'] = '0';										  //수강중지횟수
			$inData['start_dt'] = func_get_date_time();						 //시작일
			if ($inData['re_course_yn'] != 'N') { //재수강이면...
				$inData['end_dt'] = func_add_date_time(func_get_date_time(), 30); //종료일
			} else {
				$inData['end_dt'] = func_add_date_time(func_get_date_time(), 60); //종료일
			}
		}

		$inData['created_dt'] = func_get_date_time();						 //등록일

		/*
		 * hashdata 검증을 위한 mertkey는 상점관리자 -> 계약정보 -> 상점정보관리에서 확인하실수 있습니다.
		 * LG텔레콤에서 발급한 상점키로 반드시변경해 주시기 바랍니다.
		 */
		$LGD_MERTKEY = "bad462dcf36c12c88eb947ff9e1a22d6"; //mertkey
		$LGD_HASHDATA2 = md5($inData['LGD_MID'] . $inData['LGD_OID'] . $inData['LGD_AMOUNT'] . $inData['LGD_RESPCODE'] . $inData['LGD_TIMESTAMP'] . $LGD_MERTKEY);

		/*
		 * 상점 처리결과 리턴메세지
		 *
		 * OK  : 상점 처리결과 성공
		 * 그외 : 상점 처리결과 실패
		 *
		 * ※ 주의사항 : 성공시 'OK' 문자이외의 다른문자열이 포함되면 실패처리 되오니 주의하시기 바랍니다.
		 */
		$resultMSG = "결제결과 상점 DB처리(LGD_CASNOTEURL) 결과값을 입력해 주시기 바랍니다.";


		if ($LGD_HASHDATA2 == $inData['LGD_HASHDATA']) { //해쉬값 검증이 성공이면
			if ("0000" == $inData['LGD_RESPCODE']) { //결제가 성공이면
				if ("R" == $inData['LGD_CASFLAG']) {
					/*
					 * 무통장 할당 성공 결과 상점 처리(DB) 부분
					 * 상점 결과 처리가 정상이면 "OK"
					 */
					//if( 무통장 할당 성공 상점처리결과 성공 ) $resultMSG = "OK";
					$cnt = $this->payment->insert($inData);
					$resultMSG = "OK";
				} else if ("I" == $inData['LGD_CASFLAG']) {
					/*
					 * 무통장 입금 성공 결과 상점 처리(DB) 부분
					 * 상점 결과 처리가 정상이면 "OK"
					 */
					//if( 무통장 입금 성공 상점처리결과 성공 ) $resultMSG = "OK";
					$cnt = $this->payment->insert($inData);
					$resultMSG = "OK";
				} else if ("C" == $inData['LGD_CASFLAG']) {
					/*
					 * 무통장 입금취소 성공 결과 상점 처리(DB) 부분
					 * 상점 결과 처리가 정상이면 "OK"
					 */
					//if( 무통장 입금취소 성공 상점처리결과 성공 ) $resultMSG = "OK";
					$cnt = $this->payment->insert($inData);
					$resultMSG = "OK";
				}
			} else { //결제가 실패이면
				/*
				 * 거래실패 결과 상점 처리(DB) 부분
				 * 상점결과 처리가 정상이면 "OK"
				 */
				//if( 결제실패 상점처리결과 성공 ) $resultMSG = "OK";
				$cnt = $this->payment->insert($inData);
				$resultMSG = "OK";
			}
		} else { //해쉬값이 검증이 실패이면
			/*
			 * hashdata검증 실패 로그를 처리하시기 바랍니다.
			 */
			$resultMSG = "결제결과 상점 DB처리(LGD_CASNOTEURL) 해쉬값 검증이 실패하였습니다.";
		}

		echo $resultMSG;
	}

}
?>
