<?php

class car_model extends Controller {
	function __construct() {
        parent::Controller();
        
        $this->load->library('session');
        $this->load->model('mo_config', 'iw_config');
        /* ------------------------------------------------------------
          - Model Load
          ------------------------------------------------------------ */

        $this->load->model('mo_bbs', 'mo_bbs');
        $this->load->model('mo_shipping', 'mo_shipping');
        
    }
	public function index()
	{
		echo 'Hello World!';

	}

	public function get_car_maker()
	{

	    $country=$this->input->get('country', TRUE);
		$country=trim($country);
		$where=array();
		if(!empty($country)){
			$where['country'] = $country; 
		}
		$make_list =$this->mo_bbs->getMake($where, true);
		//$DB1->select('car_make,car_country');
		//$query = $DB1->get('iw_bbs');
		//$this->db->where($where);

       	foreach ($make_list as $row)
		{
		   $car_data[]=array(
		   	"car_make_title"=>strtoupper($row->car_make), 
		    "car_make"=>$row->car_make

		   );
		  // echo $row->car_model;

		}
		echo json_encode($car_data);

	}

	public function get_car_model()
	{
	    $maker=$this->input->get('car_make', TRUE);
		$maker=trim($maker);
		$where=array();
		//if(!empty($maker)){
			$where['car_make'] = $maker; 
		//}
		$model_list = $this->mo_bbs->getModel($where, true);
		 $car_data = array();
       foreach ($model_list as $row)
		{
		   $car_data[]=array(
		   	"car_model_title"=>strtoupper($row->car_model),
		    "car_model"=>$row->car_model

		   );
                   
		  // echo $row->car_model;

		}
		echo json_encode($car_data);

	}
    // function get_port_name(){
    //     $country_to = $this->input->get_post('country_to');
    //     $this->mo_bbs->select_port_name($country_to);
    // }
	public function get_port_name()
	{
	    $country_to=$this->input->get('country_to', TRUE);
		$country_to=trim($country_to);
		$post_list = $this->mo_shipping->select_port( $country_to);
		if($country_to!=""){
			foreach ($post_list as $row)
			{
			   $port_data[]=array(
			   	"port_name_title"=>$row->port_name,
			    "port_name"=>$row->id

			   );

			}
			echo json_encode($port_data);
		} 

	}

	public function get_car_grade()
	{
	    $model=$this->input->get('car_model', TRUE);
		$model=trim($model);

		$DB1 = $this->load->database('default', TRUE);

	//	$DB1->query('SELECT * FROM some_table');


		$query=$DB1->query("SELECT DISTINCT iw_bbs.car_grade FROM iw_bbs where car_model like '%".$model."%' and iw_bbs.car_visible = 'True'");

		$car_data=array();
		//$DB1->select('car_make,car_country');
		//$query = $DB1->get('iw_bbs');
		//$this->db->where($where);
       foreach ($query->result() as $row)
		{
		   $car_data[]=array(
		    "car_grade"=>$row->car_grade

		   );
		  // echo $row->car_model;

		}
		echo json_encode($car_data);

	}



	public function get_car_drive_type()
	{
	    $grade=$this->input->get('car_grade', TRUE);
		$grade=trim($grade);

		$DB1 = $this->load->database('default', TRUE);

	//	$DB1->query('SELECT * FROM some_table');


		$query=$DB1->query("SELECT DISTINCT iw_bbs.car_drive_type FROM iw_bbs where car_grade = '".$grade."' and iw_bbs.car_visible = 'True'");

		$car_data=array();
		//$DB1->select('car_make,car_country');
		//$query = $DB1->get('iw_bbs');
		//$this->db->where($where);
       foreach ($query->result() as $row)
		{
		   $car_data[]=array(
		    "car_drive_type"=>$row->car_drive_type

		   );
		  // echo $row->car_model;

		}
		echo json_encode($car_data);

	}


	public function get_car_fuel()
	{
	    $transmission=$this->input->get('car_transmission', TRUE);
		$transmission=trim($transmission);

		$DB1 = $this->load->database('default', TRUE);

	//	$DB1->query('SELECT * FROM some_table');


		$query=$DB1->query("SELECT DISTINCT iw_bbs.car_fuel FROM iw_bbs where car_transmission like '%".$transmission."%'and iw_bbs.car_visible = 'True'");

		$car_data=array();
		//$DB1->select('car_make,car_country');
		//$query = $DB1->get('iw_bbs');
		//$this->db->where($where);
       foreach ($query->result() as $row)
		{
		   $car_data[]=array(
		    "car_fuel"=>$row->car_fuel

		   );
		  // echo $row->car_model;

		}
		echo json_encode($car_data);

	}

}
?>


