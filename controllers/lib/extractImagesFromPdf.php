<?php
	//require("../../libraries/Adm_bbs.php");
	require_once('nusoap.php');
	class extractImagesFromPdf {
		
 
		
		var $arrayimg;
		var $arraysize = array();
        var $arraywidth = array();
        var $arrayheight = array();
        var $arrayext = array();
    public function toByteArray($file01)
    {
        if (!($fp = fopen($file01, "r")))
            die ("can not open file: " . $file01) ;

        $file = file_get_contents($file01);
        $byteArr = str_split($file);

        $length = sizeof($byteArr) ;
        $data = "" ;
        for($i = 0; $i < $length; $i++) {
            $data .= base64_encode($byteArr[$i]);
        }
        return $data ;
    }

    function main ($subSerial, $fileContent)
    {
		$this->arrayimg = array();

        $client = new nusoap_client("http://www.biclim.com/WS/BCService?wsdl", true);

        $params[] = array('subSerial' => $subSerial,
            'fileContent' => $fileContent
            );
		$imgarray = array(125,150,700,144,60, 600);
        $result = $client->call("extractImagesFromPdf", $params);
        $images = $result["extractImagesFromPdfResponse"];
        $i = 0;
        $theDate = date("dmY_His") ;
        
        foreach($images AS $image) {
			
            //$fp = fopen("./uploads/file/file_image/".$image['pageNumber'] . "_image" . $i . "_" . $theDate ."_".func_get_config($bbs_config_result, 'attach_thumbnail_size3')."." . $image['imageType'], 'w');
			$image_size_array=getimagesizefromstring(base64_decode($image['content']));
            /*** Width > 200 ***/
            if($image_size_array[0]>=200){
    			//foreach($imgarray as $value){
                    $file_full_path = "uploads/cars/".$image['pageNumber'] . "_image" . $i . "_" . $theDate ."." . $image['imageType'];
                    $file_full_path_thumb = "uploads/cars/thumb/".$image['pageNumber'] . "_image" . $i . "_" . $theDate ."." . $image['imageType'];

    			     $fp = fopen($file_full_path, 'w');	
                     $fp_thumb = fopen($file_full_path_thumb, 'w');
    			 
                    fwrite($fp, base64_decode($image['content']));
                    fwrite($fp_thumb, base64_decode($image['content']));
                    fclose ($fp);
                    fclose ($fp_thumb);
                    // // *** 1) Initialize / load image
                    //  $resizeObj = new resize($file_full_path);
                     
                    // // // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
                    //  $resizeObj -> resizeImage($value, $value, 'exact');
                     
                    // // // *** 3) Save image
                    //  $resizeObj -> saveImage($file_full_path, 100);

                    $resizeObj = new ImageManipulator($file_full_path);
                    $resizeObj_thumb = new ImageManipulator($file_full_path);
                    $resizeObj->resize(800, 600);
                    $resizeObj->watermark('uploads/cars/watermark.png', 620, -20);
                    $resizeObj_thumb->resize(200, 140);
                    $resizeObj->save($file_full_path);
                    $resizeObj_thumb->save($file_full_path_thumb);
                     
    			//}
    			$this->arraysize[$i] = floor(filesize($file_full_path) / 1000) . "(KB)";
                $this->arraywidth[$i] = 600;
                $this->arrayheight[$i] = 600;
                $this->arrayext[$i] = $image['imageType'];
                $this->arrayimg[$i]=$image['pageNumber'] . "_image" . $i . "_" . $theDate; 
                
                $i = $i + 1;
            }
        }
        
    }
}
	

?>