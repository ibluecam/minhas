<?php

/* ------------------------------------------------

 * 파일명 : admin.php

 * 개  요 : 관리자 전체 Controller

  ------------------------------------------------ */

class Admin extends Controller {


    var $data;
    var $member_id;
    var $member_name;
    var $admin_login_yn;
    var $iwc_login_yn;
    var $base_url;
    var $grade_no;
    var $frieght_cost_plus    = 250;
    var $inspection_cost_plus = 100;
    var $insurance_cost_plus  = 50;

//Constructor

    function __construct() {

        parent::Controller();


        func_redirect_to_ssl();
//호출된 메뉴 구분을 위해

        func_set_data($this, 'c', $this->input->get_post('c'));

        func_set_data($this, 'm', $this->input->get_post('m'));



//메뉴코드

        func_set_data($this, 'mcd', $this->input->get_post('mcd'));



        /* ------------------------------------------------------------

          - Model Load

          ------------------------------------------------------------ */

        $this->load->model('mo_common', 'common');

        $this->load->model('mo_config', 'iw_config');

        $this->load->model('mo_member', 'member');
        $this->load->model('mo_email', 'mo_email');
        $this->load->model('mo_bbs', 'bbs');

        $this->load->model('mo_file', 'mo_file');

        $this->load->model('mo_customer', 'customer');

        $this->load->model('mo_income', 'income');

        $this->load->model('mo_account', 'account');

        $this->load->model('mo_account_type', 'account_type');

        $this->load->model('mo_shipping', 'shipping');


        $this->load->model('mo_statistics', 'statistics');

        $this->load->model('mo_documents', 'documents');

        $this->load->model('mo_invoice', 'invoice');
        $this->load->model('mo_webscrape', 'mo_webscrape');
        $this->load->model('mo_activity_log','mo_activity_log');
//사이트 관리정보 설정 조회

        func_set_data($this, 'site_admin', $this->iw_config->select('site_admin')->result());

//사이트 맵 조회

        func_set_data($this, 'site_map_list', $this->iw_config->select_site_map()->result());





        /* ------------------------------------------------------------

          - Library Load

          ------------------------------------------------------------ */

        $this->load->library('iwc_common');     //IWC 공통 클래스

        $this->load->library('phpsession');     //php 세션

        $this->load->library('adm_bbs'); //게시판 관리
        
        $this->load->library('adm_negotiate'); 

        $this->load->library('adm_site_meta');     //사이트 메타

        $this->load->library('adm_member');     //회원 관리

        $this->load->library('adm_pay');     //결제 관리

        $this->load->library('adm_statistics');    //접근통계관리

        $this->load->library('adm_documents');    //접근통계관리

        $this->load->library('adm_popup');      //팝업관리

        $this->load->library('adm_customer');      //customer관리

        $this->load->library('adm_income');      //income관리

        $this->load->library('adm_account');

        $this->load->library('adm_invoice');

        $this->load->library('adm_shipping');


//관리자 세션 정보

        $this->member_id = $this->phpsession->get('member_id', 'ADMIN');

        $this->member_no = $this->phpsession->get('member_no', 'ADMIN');

        $this->member_name = $this->phpsession->get('member_name', 'ADMIN');
        $this->business_type = $this->phpsession->get('business_type', 'ADMIN');
        $this->email = $this->phpsession->get('email', 'ADMIN');

        $this->admin_login_yn = $this->phpsession->get('admin_login_yn', 'ADMIN');

        $this->iwc_login_yn = $this->phpsession->get('iwc_login_yn', 'ADMIN');

        $this->base_url = $this->phpsession->get('base_url', 'ADMIN');

        $this->grade_no = $this->phpsession->get('grade_no', 'ADMIN');
        
        $this->member_country = $this->phpsession->get('member_country', 'ADMIN');

        func_set_data($this, 'member_id', $this->member_id);
        func_set_data($this, 'member_country', $this->member_country);
        func_set_data($this, 'member_no', $this->member_no);
        func_set_data($this, 'business_type', $this->business_type);
        func_set_data($this, 'member_name', $this->member_name);
        func_set_data($this, 'grade_no', $this->grade_no);
        func_set_data($this, 'email', $this->email);

        func_set_data($this, 'admin_login_yn', $this->admin_login_yn);

        func_set_data($this, 'iwc_login_yn', $this->iwc_login_yn);

        func_set_data($this, 'base_url', $this->base_url);

        $action_menu = "";
        if(isset($_GET["m"])){

           $action_menu = strtolower($_GET["m"]);

        }else{
           $action_menu = "mobile_list_car";                
        }
        func_set_data($this, 'action_menu',$action_menu);
        
//관리자 HTML INCLUDE

        func_set_data($this, 'inc_top', $this->load->view('admin/inc/inc_top', $this->data, true));   //관리자 top

        func_set_data($this, 'inc_left', $this->load->view('admin/inc/inc_left', $this->data, true));     //관리자 left

        func_set_data($this, 'inc_bottom', $this->load->view('admin/inc/inc_bottom', $this->data, true));  //관리자 bottom



        func_set_data($this, 'title', func_get_config($this->data['site_admin'], 'site_name'));    //사이트 명

        func_set_data($this, 'site_url', func_get_config($this->data['site_admin'], 'site_url'));  //사이트 URL
    }

//Index

    function index() {

//관리자 로그인 처리가 되어 있다면..

        if ($this->_login_check()) {

//관리자 인덱스
//$this->view_site_meta();
//$this->adm_bbs->set_mcd('product');
            $this->adm_bbs->m = 'adm_bbs_list';
            $this->adm_bbs->mcd = 'product';
            if($this->_isAdminGroup()){
                $this->adm_bbs_list();
            }
            else if ($this->business_type=='seller' && $this->_isUserGroup()) {
                $this->list_car();
                //$this->adm_bbs_list(); //OLD SYSTEM
            }
           
            else{
                // $this->buyer_product();//OLD SYSTEM
                $this->buyer_car_list();

              
            }
        }
    }
    function mobile_home(){
        if ($this->_login_check()){
            if($this->_isAdminGroup()){
                redirect('/?c=admin');
            }else{
                if($this->business_type == 'seller'){
                    $this->mobile_list_car();
                }elseif($this->business_type == 'buyer'){
                    $this->mobile_buyer_list_car();
                }
            }
        }
           
    }
    function exec_car_email_compaign(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->exec_car_email_compaign();
    }

    function exec_multiple_car_email_compaign(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->exec_multiple_car_email_compaign();
    }

    function multiple_send_car_offer(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->multiple_send_car_offer();
    }
    
    function send_car_offer(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->send_car_offer();
    }

    function send_recommend_car(){
        if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->send_recommend_car();
    }

    function adm_add_email_bbs(){
        if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->adm_add_email_bbs();
    }

    function adm_add_customer_bbs(){
        if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->adm_add_customer_bbs();
    }

     function add_more_email(){
        if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->add_more_email();
    }


    function send_recommend_car_exec(){
        if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->send_recommend_car_exec();
    }
    function test_member_view(){
        if ($this->_login_check()&&$this->business_type=='seller')
             $this->adm_bbs->test_member_view();
    }
	function test_page(){
        $this->adm_bbs->test_page();
    }

	function member_info(){
        if ($this->_login_check())
             $this->adm_bbs->member_info();
    }

    function adm_member_info_exec(){
         if ($this->_login_check())
             $this->adm_customer->adm_member_info_exec();
    }

    // rado start mobile info

     function mobile_adm_member_info_exec(){
         if ($this->_login_check())
             $this->adm_customer->mobile_adm_member_info_exec();
    }

    // rado end mobile info

    function adm_message_detail(){
        if ($this->_login_check())
             $this->adm_negotiate->adm_message_detail();
    }

    function adm_message_buyer(){
        if ($this->_login_check())
             $this->adm_negotiate->adm_message_buyer();
    }

    function get_message(){
        if ($this->_login_check())
             $this->adm_negotiate->get_message();
    }

    function adm_negotiate(){
        if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_negotiate->adm_negotiate();
    }
	
	function adm_message_read(){
        if ($this->_login_check())
             $this->adm_negotiate->adm_message_read();
    }
	
	function adm_delete_message(){
        if ($this->_login_check())
             $this->adm_negotiate->adm_delete_message();
    }
	
	function member_delete_message(){
        if ($this->_login_check())
             $this->adm_negotiate->member_delete_message();
    }
	function member_message_read(){
        if ($this->_login_check())
             $this->adm_negotiate->member_message_read();
    }
	function member_negotiate(){
        if ($this->_login_check())
             $this->adm_negotiate->member_negotiate();
    }
	
	function addcar(){
        if ($this->_login_check()&&$this->business_type=='seller')
             $this->adm_bbs->addcar();
    }

    function addcar_exec(){
        if ($this->_login_check()&&$this->business_type=='seller')
             $this->adm_bbs->addcar_exec();
    }

    function modify_car(){
        if ($this->_login_check()&&$this->business_type=='seller')
             $this->adm_bbs->modify_car();
    }

    function detail_car(){
        if ($this->_login_check())
             $this->adm_bbs->detail_car();
    }

    function adm_get_fob_discount(){
        if ($this->_login_check()&&$this->business_type=='seller')
             $this->adm_bbs->adm_get_fob_discount();
    }
	
	function modify_car_exec(){
        if ($this->_login_check()&&$this->business_type=='seller')
             $this->adm_bbs->modify_car_exec();
    }
	
	function mobile_addcar(){
        if ($this->_login_check()&&$this->business_type=='seller')
             $this->adm_bbs->mobile_addcar();
    }

    function mobile_modify_car(){
        if ($this->_login_check()&&$this->business_type=='seller')
             $this->adm_bbs->mobile_modify_car();
    }
	
	function mobile_modify_car_exec(){
        if ($this->_login_check()&&$this->business_type=='seller')
             $this->adm_bbs->mobile_modify_car_exec();
    }
    
	function list_car(){
        if ($this->_login_check()&&$this->business_type=='seller' || $this->_login_check()&&$this->business_type=='buyer'){
             $this->adm_bbs->list_car();
        }
            
    }

    // function discount_car(){
    //     if ($this->_login_check()&&$this->business_type=='seller'){
    //          $this->adm_bbs->discount_car();
    //     }
            
    // }

	function mobile_list_car(){

        if ($this->_login_check()&&$this->business_type=='seller')
            $this->adm_bbs->mobile_list_car();
    }
    function mobile_buyer_list_car(){

        if ($this->_login_check()&&$this->business_type=='buyer')
            $this->adm_bbs->mobile_buyer_list_car();
    }
    function mobile_buyer_car_detail(){

        if ($this->_login_check()&&$this->business_type=='buyer')
            $this->adm_bbs->mobile_buyer_car_detail();
    }
    function mobile_info(){
        if ($this->_login_check()&&$this->business_type=='seller')
             $this->adm_bbs->mobile_info();
    }
    function buyer_car_list(){

         if ($this->_login_check()&&$this->business_type=='buyer'){
             $this->adm_bbs->buyer_car_list();
         }
    }
    function mobile_info_buyer(){
        if ($this->_login_check()&&$this->business_type=='buyer')
             $this->adm_bbs->mobile_info_buyer();
    }

    function change_password(){
        if ($this->_login_check())
           $this->adm_bbs->change_password();
    }

    // rado start mobile change password

    function mobile_change_password(){
        if ($this->_login_check())
           $this->adm_bbs->mobile_change_password();
    }

    // rado end mobile change password

    function view_document_buyer(){
        if ($this->_login_check() && $this->business_type=='buyer')
           $this->adm_bbs->view_document_buyer();
    }
      function download_document(){
        if ($this->_login_check())
           $this->adm_bbs->download_document();
    }


    function adm_item_supporter(){
         if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->adm_item_supporter();
    }

    function adm_delete_supporter_car(){
         if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->adm_delete_supporter_car();
    }

    function adm_change_supporter(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_change_supporter();
    }
    function adm_validate_change_supporter(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_validate_change_supporter();
    }
    function adm_add_item_country_supporter(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_add_item_country_supporter();
    }

    function adm_add_item_idx_supporter(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_add_item_idx_supporter();
    }

    function adm_scrape_item(){
         if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_scrape_item();
    }

    function adm_scrape_total_page(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_scrape_total_page();
    }
    function adm_scrape_json_item(){
         if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_scrape_json_item();
    }
    function adm_scrape_json_item_detail(){
         if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_scrape_json_item_detail();
    }
    function adm_json_scrape_count_by_chassis(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_json_scrape_count_by_chassis();
    }
    function adm_json_scrape_update_all(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_json_scrape_update_all();
    }

    function adm_upload_scrape_images(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_upload_scrape_images();
    }
    
    function adm_premium_bbs(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_premium_bbs();

    }
    
    function adm_premium_view(){
         if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_premium_view();
    }
    function user_cloud_upload(){
        if ($this->_login_check()){
            $this->adm_bbs->user_cloud_upload();
        }
    }
    function mobile_user_cloud_upload(){
        if ($this->_login_check()){
            $this->adm_bbs->mobile_user_cloud_upload();
        }
    }
    function user_cloud_upload_exec(){
        if ($this->_login_check()){
            $this->adm_bbs->user_cloud_upload_exec();
        }
    }
    function user_delete_attach_cloud() {

        if ($this->_login_check())
            $this->adm_bbs->user_delete_attach_cloud();
    }
     function mobile_user_delete_attach_cloud() {

        if ($this->_login_check())
            $this->adm_bbs->mobile_user_delete_attach_cloud();
    }
    function adm_cloud_upload(){
        if ($this->_login_check()){
            $this->adm_bbs->adm_cloud_upload();
        }
    }
    function adm_cloud_upload_exec(){
        if ($this->_login_check()){
            $this->adm_bbs->adm_cloud_upload_exec();
        }
    }
    function mobile_adm_cloud_upload_exec(){
        if ($this->_login_check()){
            $this->adm_bbs->mobile_adm_cloud_upload_exec();
        }
    }
    function adm_delete_attach_cloud() {

        if ($this->_login_check())
            $this->adm_bbs->adm_delete_attach_cloud();
    }
    function adm_cloud_upload_list(){
        if ($this->_login_check()){
            $this->adm_bbs->adm_cloud_upload_list();

        }
    }
     function adm_add_premium_bbs(){
         if ($this->_login_check() &&$this->_isAdminGroup())
          $this->adm_bbs->adm_add_premium_bbs();
    }


    function adm_image_uploader(){
        if ($this->_login_check()){
            $this->adm_bbs->adm_image_uploader();
        }
    }
    function adm_image_uploader_exec(){
        if ($this->_login_check()){
            $this->adm_bbs->adm_image_uploader_exec();

        }
    }
    
function  customer_deposit(){
        $sender=  $this->input->get_post('sender');
        $deposit=  $this->input->get_post('deposit');
        $now = new DateTime(date("Y-m-d"));
        $data=array(
            'sender'=>$sender,
            'deposit'=>$deposit,
            'created_dt'=>$now->format("Y-m-d H:i:s")
        );
       $doadd=$this->income->customer_deposit($data);
        if($doadd==TRUE){
            
             echo func_jsAlertReplace('Input Complete!!!!', '/?c=admin&m=adm_income_list' );
        }
          else {
            echo func_jsAlertReplace('fail to deposit.', '/?c=admin&m=adm_income_list');
          }
    }
    
    
    function select_sender(){
        $sch_word=  $this->input->get_post('sch_word_sender');
        $customer_list=  $this->income->select_sender_exce($sch_word);
        if($customer_list==TRUE){
             func_set_data($this, 'admin_contents', $this->load->view('admin/adm/customer/adm_income_list', $this->data, true));
        }
       else {
            echo 'no see';
       }
    }


//사이트&ROOT 관리자 로그인 처리

    function login_exec() {

        $member_id = '';  //관리자 아이디

        $member_pwd = '';  //관리자 패스워드



        $member_id = $this->input->get_post('member_id', TRUE);

        $member_pwd = $this->input->get_post('member_pwd', TRUE);
        $login = $this->member->login($member_id, $member_pwd);

        if ($login->num_rows()>0) {
            $rows = $login->result();
            $row = $rows[0];
            if($row->grade_no != 10){
                $this->phpsession->save('member_id', $row->member_id, 'ADMIN');
                $this->phpsession->save('member_no', $row->member_no, 'ADMIN');
                $this->phpsession->save('grade_no', $row->grade_no, 'ADMIN');
                $this->phpsession->save('admin_login_yn', 'Y', 'ADMIN');
                $this->phpsession->save('iwc_login_yn', 'Y', 'ADMIN');
                $this->phpsession->save('base_url', '/?c=admin', 'ADMIN');
                $this->phpsession->save('session_grade_no', $row->grade_no, 'ADMIN'); //add grade_no
                $this->phpsession->save('business_type', $row->business_type, 'ADMIN');
                $this->phpsession->save('member_country', $row->member_country, 'ADMIN');
                //$this->_visit_statistics();

                redirect('/?c=admin', 'refresh');
            }else{

                echo func_jsAlertReplace('Account not verified! \nPlease verify from your email first.', '/?c=admin', TRUE);
            }
        }else {

            $salt = '%@#key';
            $dm = gmdate('dm', time());
            $md5 = md5($dm.$salt);

            echo func_jsAlertReplace('Incorrect username or password!', '/?c=admin&key='.$md5, TRUE);
        }
    }

//관리자 로그인 세션 체크
    function _isAdminGroup(){
        $grad_no_group = array('1', '2', '3');
        if(in_array($this->grade_no, $grad_no_group)){
            return true;
        }
    }
    
    function _isUserGroup(){
         $grad_no_group = array('11');
        if(in_array($this->grade_no, $grad_no_group)){
            return true;
        }
    }
            
    function _login_check() {

        if ($this->admin_login_yn != 'Y') {

            $salt = '%@#key';
            $dm = gmdate('dm', time());

            $md5 = md5($dm.$salt);
            if($md5==$this->input->get('key')){
                $this->load->view('admin/login', $this->data, false);
            }else{
                redirect('/?c=user&m=public_login');
            }
           
            return false;
        } else {

            return true;
        }
    }

//사이트&ROOT 관리자 로그아웃 처리

    function logout_exec() {

        $this->phpsession->clear(null, 'ADMIN');

        $this->load->view('admin/login', $this->data, false);
    }

    /* ------------------------------------------------------------------------------------------------ */
    function delete_old_files(){
    	if ($this->_login_check()&&$this->_isAdminGroup()){
	    	$dir = 'uploads/product/';
	    	$files=glob($dir."*.*");
	    	foreach($files as $file){
	    		$fileyear = date ("Y", filemtime($file));
	    		if($fileyear<=2013){
	    			unlink($file);
	    			echo $file." ".$fileyear." DELETED!<br/>";
	    		}
	    	}
	    }
    }

    function list_old_files(){
    	if ($this->_login_check()&&$this->_isAdminGroup()){
	    	$dir = 'uploads/product/';
	    	$files=glob($dir."*.*");
	    	foreach($files as $file){
	    		$fileyear = date ("Y", filemtime($file));
	    		if($fileyear<=2013){
	    			echo $file." ".$fileyear;
	    		}
	    	}
	    }
    }

    /* ------------------------------------------------

      - 사이트 메타 테그 관리

      ------------------------------------------------ */

//사이트 메타 테그 HTML

    function view_site_meta() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_site_meta->view_site_meta();
    }

//사이트 메타 테그 EXEC

    function exec_site_meta() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_site_meta->exec_site_meta();
    }

    /* ------------------------------------------------

      - 관리자 게시판 관리

      ------------------------------------------------ */
    function adm_get_model_list(){
        if ($this->_login_check())
            $this->adm_bbs->adm_get_model_list();
    }
/**** SENDGRID FUNCTION ***/
    function recipient_list(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->recipient_list();
    }
    function recipient_email(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->recipient_email();
    }
    function delete_recipient_email(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->delete_recipient_email();
    }
    
/**** END SENDGRID FUNCTION ***/    
    function adm_get_color_options(){
        if ($this->_login_check())
            $this->adm_bbs->adm_get_color_options();
    }
   
            function write_color_popup(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_popup->write_color_popup();
    }
    function modify_color_popup(){
        if ($this->_login_check())
            $this->adm_popup->modify_color_popup();
    }
    
   
    
       
    
            function sender(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_popup->sender();
    }
    function adm_member_list_popup(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_member->adm_member_list_popup();
    }
    function adm_member_list_popup_exec(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_member->adm_member_list_popup_exec();
    }
    function adm_bbs_cloud_upload() {
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_cloud_upload();
    }
	function member_mobile_bbs_cloud_upload() {
        if ($this->_login_check())
            $this->adm_bbs->member_mobile_bbs_cloud_upload();
    }
    function adm_bbs_cloud_upload_exec() {
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_cloud_upload_exec();
    }
    function adm_upload_image_car() {
        if ($this->_login_check())
            $this->adm_bbs->adm_upload_image_car();
    }
    function adm_upload_image_car_exec() {
        if ($this->_login_check())
            $this->adm_bbs->adm_upload_image_car_exec();
    }
    function adm_insert_activity_log_images_for_car() {
        if ($this->_login_check())
            $this->adm_bbs->adm_insert_activity_log_images_for_car();
    }
    function adm_cloud_upload_thumbnail() {
        if ($this->_login_check())
            $this->adm_bbs->adm_cloud_upload_thumbnail();
    }
    function adm_bbs_delete_attach_cloud(){
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_delete_attach_cloud();

    }
    
    function adm_bbs_delete_attach_cloud_check(){
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_delete_attach_cloud_check();
    }

    function adm_bbs_delete_attach_check_seller(){
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_delete_attach_check_seller();
    }
    
    function adm_bbs_delete_attach_cloud_check_seller(){
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_delete_attach_cloud_check_seller();
    }
    function adm_bbs_delete_attach_cloud_click_seller(){
         if ($this->_login_check())
            $this->adm_bbs->adm_bbs_delete_attach_cloud_click_seller();
    }

    function adm_bbs_delete_attach(){
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_delete_attach();

    }

    function adm_bbs_delete_attach_click_seller(){
         if ($this->_login_check())
            $this->adm_bbs->adm_bbs_delete_attach_click_seller();
    }

    function adm_bbs_delete_attach_check(){
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_delete_attach_check();
    }

    function adm_image_upload() {

        if ($this->_login_check())
            $this->adm_bbs->adm_image_upload();
    }

    function adm_image_upload_exec() {

        if ($this->_login_check())
            $this->adm_bbs->adm_image_upload_exec();
    }

    function adm_set_profile_image() {

        if ($this->_login_check())
            $this->adm_bbs->adm_set_profile_image();
    }

    function adm_set_primary_cloud_image(){
        if ($this->_login_check())
            $this->adm_bbs->adm_set_primary_cloud_image();
    }

    function adm_set_all_bbs_image_sort(){
        if ($this->_login_check())
            $this->adm_bbs->adm_set_all_bbs_image_sort();
    }

    function adm_set_primary_image() {

        if ($this->_login_check())
            $this->adm_bbs->adm_set_primary_image();
    }

    function adm_set_primary_photo(){

        if ($this->_login_check())
            $this->adm_bbs->adm_set_primary_photo();
    }

    function adm_bbs_set_image_sort() {
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_set_image_sort();
    }
    function adm_set_image_sort() {

        if ($this->_login_check())
            $this->adm_bbs->adm_set_image_sort();
    }
    function adm_set_all_image_sort() {

        if ($this->_login_check())
            $this->adm_bbs->adm_set_all_image_sort();
    }

    function adm_set_all_image_sort_car() {

        if ($this->_login_check())
            $this->adm_bbs->adm_set_all_image_sort_car();
    }

//게시판 목록 조회

    function adm_bbs_list() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_bbs_list();
    }
    
    function buyer_product(){
        $buyer_link=$this->adm_bbs->mcd='';
        if($this->_login_check() && $this->business_type =='buyer')
           $this->adm_bbs->buyer_product();
    }
    function buyer_bbs_info(){
         if($this->_login_check() && $this->business_type =='buyer')
           $this->adm_bbs->buyer_bbs_info();
    }
    
    function make_list(){
        if($this->_login_check()){
            $this->adm_bbs->make_list();
        }
    }
    
    function adm_json_make(){
        if ($this->_login_check())
           $this->adm_bbs->adm_json_make(); 
    }
    
    function add_make(){
         if ($this->_login_check())
           $this->adm_bbs->add_make(); 
    }
    
    function adm_make_detail(){
         if ($this->_login_check())
            $this->adm_bbs->adm_make_detail();
    }
    
    function add_images_make(){
       if ($this->_login_check())
            $this->adm_bbs->add_images_make(); 
    }
    
    function adm_make_delete(){
          if ($this->_login_check())
            $this->adm_bbs->adm_make_delete(); 
    }
    
    function adm_change_make(){
        if ($this->_login_check())
            $this->adm_bbs->adm_change_make(); 
    }
    
    
    function model_list(){
         if ($this->_login_check())
            $this->adm_bbs->model_list(); 
    }
    function add_model(){
         if ($this->_login_check())
            $this->adm_bbs->add_model();
    }
    function adm_change_model(){
        if ($this->_login_check())
            $this->adm_bbs->adm_change_model();
    }
    
    function add_delete_model(){
       if ($this->_login_check())
            $this->adm_bbs->add_delete_model(); 
    }
            
    
            
    function adm_bbs_file_exec() {
        if ($this->_login_check())
        $this->adm_bbs->adm_bbs_file_exec();
    }
    
  

//게시판 상세 조회

    function adm_bbs_detail() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_detail();
    }

//게시판 등록

    function adm_bbs_write() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_write();
    }

//게시판 등록 처리

    function adm_bbs_write_exec() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_write_exec();
    }

    function delete_file_image() {
        if ($this->_login_check())
            $this->adm_bbs->delete_file_image();
    }

    function checkmemember() {
        $this->adm_bbs->checkmemember();
    }
    
   function check_chassis(){
       $this->adm_bbs->check_chassis();
   }

    function checkemail() {
        $this->adm_bbs->checkemail();
    }
    
    
    function checkpassword() {
        $this->adm_bbs->checkpassword();
    }

    function checkonlyletter() {
        $this->adm_bbs->checkonlyletter();
    }

//게시판 수정
    function adm_get_car_to_allocate_deposit() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_get_car_to_allocate_deposit();
    }
    function adm_customer_option_for_distribute() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_member->adm_customer_option_for_distribute();
    }
    function adm_bbs_update() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_update();
    }

//게시판 수정 처리

    function adm_bbs_update_exec() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_update_exec();
    }
    //New alternative to adm_bbs_excel_exec()
    function adm_bbs_export_excel() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_export_excel();
    }
    
    //New alternative to adm_bbs_excel_exec()
    function adm_bbs_excel_customer(){
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_excel_customer();
    }
    function adm_bbs_excel_staff(){
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_excel_staff();
    }
    
//엑셀 변환 처리//////////////////////////////////////

    function adm_bbs_excel_exec() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_excel_exec();
    }

//shipment 수정

    function adm_bbs_ship_update() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_ship_update();
    }

//shipment 수정 처리

    function adm_bbs_ship_update_exec() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_ship_update_exec();
    }

//popup 수정

    function adm_bbs_ship_popup_update() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_ship_popup_update();
    }

//popup 수정 처리

    function adm_bbs_ship_popup_update_exec() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_ship_popup_update_exec();
    }

     function adm_delete_bl_by_no(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_documents->adm_delete_bl_by_no();
     }
//modify 

    function adm_delete_invoice_by_no(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_invoice->adm_delete_invoice_by_no();
    }
    function adm_bbs_modify() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_modify();
    }

    function adm_bbs_sale_modify(){
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_sale_modify();
    } 

    function adm_delete_attach() {

        if ($this->_login_check())
            $this->adm_bbs->adm_delete_attach();
    }

//modify  exc

    function adm_bbs_modify_exec() {
        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_modify_exec();
    }

//게시판 답변

    function adm_bbs_reply() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_reply();
    }

//게시판 답변 처리

    function adm_bbs_reply_exec() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_reply_exec();
    }

//게시판 삭제

    function adm_bbs_delete() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_delete();
    }

//게시판 선택 삭제

    function adm_bbs_all_delete() {

        if ($this->_login_check())
            $this->adm_bbs->adm_bbs_all_delete();
    }

//게시판 코멘트 등록

    function adm_bbs_comment_write_exec() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_bbs_comment_write_exec();
    }

//게시판 코멘트 삭제

    function adm_bbs_comment_delete() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_bbs_comment_delete();
    }

//설문조사 다운로드

    function adm_survey_entry_download() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_survey_entry_download();
    }

    // reserve popup 수정

    function adm_reserve_popup_update() {

        if ($this->_login_check())
            $this->adm_bbs->adm_reserve_popup_update();
    }

//reserve popup 수정 처리

    function adm_reserve_popup_update_exec() {

        if ($this->_login_check())
            $this->adm_bbs->adm_reserve_popup_update_exec();
    }

    function adm_test_order_email(){
        if ($this->_login_check())
            $this->adm_bbs->adm_test_order_email();
    }
    // reserve cancel exe

    function adm_reserve_cancel_exec() {

        if ($this->_login_check())
            $this->adm_bbs->adm_reserve_cancel_exec();
    }

    /* ------------------------------------------------

      - 회원 정보

      ------------------------------------------------ */

//회원정보 조회

    function adm_member_list() {

        if ($this->_login_check() && ($this->grade_no == '1'))
            $this->adm_member->adm_member_list();
    }

//회원정보 상세 조회
    function adm_member_view() {

        if ($this->_login_check() && ($this->grade_no == '1'||$this->grade_no == '3'))
            $this->adm_member->adm_member_view();
    }

    function adm_member_detail() {

        if ($this->_login_check() && ($this->grade_no == '1'))
            $this->adm_member->adm_member_detail();
    }

    function adm_member_write() {

        if ($this->_login_check() && ($this->grade_no == '1'))
            $this->adm_member->adm_member_write();
    }

    function adm_member_write_exec() {

        if ($this->_login_check() && ($this->grade_no == '1'))
            $this->adm_member->adm_member_write_exec();
    }

//회원정보 수정

    function adm_member_update() {

        if ($this->_login_check() && ($this->grade_no == '1'))
            $this->adm_member->adm_member_update();
    }

//회원정보 삭제

    function adm_member_delete() {

        if ($this->_login_check() && ($this->grade_no == '1'))
            $this->adm_member->adm_member_delete();
    }

//회원정보 선택 삭제

    function adm_member_all_delete() {

        if ($this->_login_check() && ($this->grade_no == '1'))
            $this->adm_member->adm_member_all_delete();
    }

//회원정보 선택 인증

    function adm_member_all_certi() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_member->adm_member_all_certi();
    }

  

    /* ------------------------------------------------

      - 회원 등급

      ------------------------------------------------ */

//회원 등급 HTML

    function view_member_grade() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_member->view_member_grade();
    }

    function adm_add_member() {
        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_member->adm_add_member();
    }

//회원 등급 EXEC

    function exec_member_grade() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_member->exec_member_grade();
    }

//회원 등급 삭제

    function exec_delete_member_grade() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_member->exec_delete_member_grade();
    }

    /* ------------------------------------------------

      - 결제 정보

      ------------------------------------------------ */

//결제정보 조회

    function adm_pay_list() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_pay->adm_pay_list();
    }

//결제정보 상세조회

    function adm_pay_detail() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_pay->adm_pay_detail();
    }

//결제정보 수정

    function adm_pay_update() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_pay->adm_pay_update();
    }

    /* ------------------------------------------------

      - 접근 통계

      ------------------------------------------------ */

//년간통계

    function view_statistics_year() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_statistics->view_statistics_year();
    }

    function adm_vistitors() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_statistics->adm_vistitors();
    }

//월간통계

    function view_statistics_month() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_statistics->view_statistics_month();
    }

//일별통계

    function view_statistics_day() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_statistics->view_statistics_day();
    }

    function calldata() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_statistics->calldata();
    }

//메뉴별통계

    function view_statistics_menu() {

        if ($this->_login_check())
            $this->adm_statistics->view_statistics_menu();
    }

// Search engine  adm_documents_list 

    function adm_documents_list() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_documents->adm_documents_list();
    }
    
// Search engine  adm_documents_view 

    function adm_documents_view() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_documents->adm_documents_view();
    }

// Search engine Document detail 

    function adm_documents_detail() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_documents->adm_documents_detail();
    }

// Search engine Document  modify

    function adm_documents_modify() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_documents->adm_documents_modify();
    }

// Search engine  Document modify_exec

    function adm_documents_modify_exec() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_documents->adm_documents_modify_exec();
    }

// Document delete_file

    function adm_documents_delete_exec() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_documents->adm_documents_delete_exec();
    }

//검색엔진통계

    function view_statistics_referer() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_statistics->view_statistics_referer();
    }

// document register 

    function adm_documents_register() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_documents->adm_documents_register();
    }

// document register 

    function adm_documents_register_exec() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_documents->adm_documents_register_exec();
    }

// invoice register

    function adm_invoice_register() {

        if ($this->_login_check()&&$this->_isAdminGroup()){
            $this->adm_invoice->adm_invoice_register();
        }
    }

    function adm_invoice_register_exec() {

        if ($this->_login_check()&&$this->_isAdminGroup()){
            $this->adm_invoice->adm_invoice_register_exec();
        }
    }

//검색키워드통계

    function view_statistics_keyword() {

        if ($this->_login_check())
            $this->adm_statistics->view_statistics_keyword();
    }

    // Search engine  adm_invoice_list 

    function adm_invoice_list() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_invoice->adm_invoice_list();
    }

    function adm_delete_proforma_invoice() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_invoice->adm_delete_proforma_invoice();
    }

    function download_file_proforma_invoice(){
        
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_invoice->download_file_proforma_invoice();
    }

    function adm_proforma_invoice() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_invoice->adm_proforma_invoice();
    }

    function adm_proforma_invoice_list() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_invoice->adm_proforma_invoice_list();
    }


    function adm_invoice_view() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_invoice->adm_invoice_view();
    }

    function adm_invoice_excel_dwnl() {

        if ($this->_login_check())
            $this->adm_invoice->adm_invoice_excel_dwnl();
    }

    /* ------------------------------------------------

      - 팝업관리

      ------------------------------------------------ */

//팝업 목록 조회

    function adm_popup_list() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_popup->adm_popup_list();
    }

    function adm_popup_list_visitor() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_popup->adm_popup_list_visitor();
    }

//팝업 상세 조회

    function adm_popup_detail() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_popup->adm_popup_detail();
    }

//팝업 등록 HTML

    function adm_popup_write() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_popup->adm_popup_write();
    }

//팝업 등록 exec

    function adm_popup_write_exec() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_popup->adm_popup_write_exec();
    }

//팝업 수정

    function adm_popup_update() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_popup->adm_popup_update();
    }

//팝업 삭제

    function adm_popup_delete() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_popup->adm_popup_delete();
    }

//팝업 미리 보기

    function adm_popup_preview() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_popup->adm_popup_preview();
    }

    /* ------------------------------------------------

      - customer관리

      ------------------------------------------------ */

    function adm_account() {
        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_list();
    }

    function adm_account_write() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_write();
    }

    function adm_account_write_exec() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_write_exec();
    }

    function adm_account_detail() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_detail();
    }

    function adm_account_update() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_update();
    }

    function adm_account_update_exec() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_update_exec();
    }

    function adm_account_rate() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_rate();
    }

    function adm_account_rate_exec() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_rate_exec();
    }

    function adm_account_rate_modify() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_rate_modify();
    }

    function adm_account_rate_modify_exec() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_rate_modify_exec();
    }

    function adm_account_rate_delete() {
        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_rate_delete();
    }

    function adm_account_rate_delete_exec() {
        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_rate_delete_exec();
    }

    function adm_account_all_delete() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_account->adm_account_all_delete();
    }
    function adm_order_list_popup(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_customer->adm_order_list_popup();
    }
    function adm_order_list_popup_exec(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_customer->adm_order_list_popup_exec();
    }
//income 목록 조회

    function adm_income_list() {
        if ($this->_login_check()&&$this->_isAdminGroup())  
            $this->adm_income->adm_income_list();
            //$this->adm_member->adm_member_list();
    }

//customer 목록 조회

    function adm_customer_list() {
        if ($this->_login_check()&&$this->_isAdminGroup())  
            $this->adm_customer->adm_customer_list();
            //$this->adm_member->adm_member_list();
    }    

//customer 세부 내역

    function adm_customer_view() {
        if ($this->_login_check()&&$this->_isAdminGroup())  
            $this->adm_customer->adm_customer_view();
    } 

//customer 등록

    function adm_customer_write() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_customer->adm_customer_write();
    }
    

//customer 등록 처리

    function adm_customer_write_exec() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_customer->adm_customer_write_exec();
    }
    
    function adm_changepassword(){
          if ($this->_login_check()&&$this->_isUserGroup())
            $this->adm_customer->adm_changepassword();   
    }
    
    function adm_changepassword_exec(){
         if ($this->_login_check()&&$this->_isUserGroup())
            $this->adm_customer->adm_changepassword_exec();   
    }
     function user_changepassword_exec(){
         if ($this->_login_check()&&$this->_isUserGroup())
            $this->adm_customer->user_changepassword_exec();   
    }
    
    function adm_my_info(){
        if ($this->_login_check()&&$this->_isUserGroup())
         $this->adm_customer->adm_my_info();  
    }
    
    function adm_my_info_exec(){
        

		if ($this->_login_check()&&$this->_isUserGroup())
            $this->adm_customer->adm_my_info_exec();
					
			
			
    }
    
    function view_info(){
          if ($this->_login_check()&&$this->_isUserGroup())
         $this->adm_customer->view_info();
    }

//customer modify

    function adm_customer_modify() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_customer->adm_customer_modify();
    }

//customer modify exec

    function adm_customer_modify_exec() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_customer->adm_customer_modify_exec();
    }

//customer 선택 삭제

    function adm_customer_all_delete() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_customer->adm_customer_all_delete();
    }
    
    
    function adm_supporter_delete(){
        if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->adm_supporter_all_delete();
    }
   
    function adm_json_premium_bbs(){
        if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->adm_json_premium_bbs();
    }

    function adm_premium_delete(){
        if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->adm_premium_all_delete();

    }
     function adm_support_customer_delete(){
        if ($this->_login_check()&&$this->_isAdminGroup())
             $this->adm_bbs->adm_supporter_all_customer_delete();

    }
//customer delete

    function adm_customer_delete() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_customer->adm_customer_delete();
    }    

// customer popup 수정

    function adm_customer_deposit_popup_update() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_customer->adm_customer_deposit_popup_update();
    }

//customer popup 수정 처리

    function adm_customer_deposit_popup_update_exec() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_income->adm_customer_deposit_popup_update_exec();
    }
    
   

//사이트 접속 통계
    function getALLfromIP($addr, $gu) {

        $query = $this->statistics->select_country_ip(sprintf("%u", ip2long($addr)));

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                if ($gu == "cn") {
                    $ipcountry = $rows->cn;
                } else if ($gu == "cc") {
                    $ipcountry = $rows->cc;
                }
            }
            return $ipcountry;
        } else {
            return "unKnow";
        }

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                if ($rows->scroll_yn == 'Y')
                    $scrollbars = 'yes';
                if ($rows->scroll_yn == 'N')
                    $scrollbars = 'no';
//해당 팝업의 쿠키가 존재하지 않으면...
                if (!get_cookie('IW_POPUP_' . $rows->idx)) {
                    $tmp_str .= "window.open(\"/?c=user&m=popup_view&idx=$rows->idx\", \"\", \"top=$top_interval, left=$left_interval, width=$rows->win_width, height=$rows->win_height, scrollbars=$scrollbars\");\n";
                    $left_interval = (int) $left_interval + (int) $rows->win_width + 10;
                }
            }
        }
    }
    function adm_save_invoice_no() {

        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_invoice->adm_save_invoice_no();
    }
    function adm_save_dhl_no(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_invoice->adm_save_dhl_no();
    }
//     function _visit_statistics() {
//         $inData = array();
//         $is_deny = FALSE;
//         $agent = '';
//         $version = '';

//         if ($this->agent->is_browser()) {
//             $agent = $this->agent->browser();
//             $version = $this->agent->version();
//         } else if ($this->agent->is_robot()) {
//             $agent = $this->agent->robot();
//             $is_deny = TRUE;
//         } else if ($this->agent->is_mobile()) {
//             $agent = $this->agent->mobile();
//         } else {
//             $agent = 'Unidentified User Agent';
//             $is_deny = TRUE;
//         }

//         $ip = $this->input->ip_address();

//         $inData['visit_year'] = date('Y');
//         $inData['visit_month'] = date('m');
//         $inData['visit_day'] = date('d');
//         $inData['visit_time'] = date('H');
//         $inData['visit_dateTime'] = date('Y-m-d H:i:s');
//         $inData['menu_code'] = 'Admin';
//         $inData['visit_ip'] = $this->input->ip_address();
//         $inData['agent'] = $agent;
//         $inData['version'] = $version;
//         $inData['is_robot'] = 'N';
//         $inData['referer'] = '';
//         $inData['search_site'] = ''; //검색 사이트
//         $inData['query_name'] = ''; //검색 사이트 질의 변수명
//         $inData['keyword'] = ''; //검색 키워드
//         $inData['countrycd'] = $this->getALLfromIP($inData['visit_ip'], "cc");
//         $inData['country'] = $this->getALLfromIP($inData['visit_ip'], "cn");


// //referrer 체크
//         if ($this->agent->is_referral()) {
//             $referrer = $this->agent->referrer();
//             $referrer = str_replace('https://', '', $referrer);
//             $referrer = str_replace('http://', '', $referrer);
//             $referrer = str_replace('www.', '', $referrer);
//             $referrer = substr($referrer, 0, strpos($referrer, '/'));
//             $inData['referer'] = $referrer;
//         }
// //검색사이트 referer 체크
//         if ($this->agent->is_referral()) {
//             $http_referer = $this->agent->referrer();

//             if (stripos($http_referer, 'search.naver.com')) { //네이버
//                 $inData['search_site'] = 'naver';
//                 $inData['query_name'] = 'query';
//                 $tmp = substr($http_referer, stripos($http_referer, '&query=') + 7);

// //검색어 뒤에 '&' 파라미터 여부
//                 if (!stripos($tmp, '&')) {
//                     $inData['keyword'] = $tmp;
//                 } else {
//                     $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
//                 }
//             }
//             if (stripos($http_referer, 'search.daum.net')) { //다음
//                 $inData['search_site'] = 'daum';
//                 $inData['query_name'] = 'q';

//                 $tmp = substr($http_referer, stripos($http_referer, '&q=') + 3);

// //검색어 뒤에 '&' 파라미터 여부
//                 if (!stripos($tmp, '&')) {
//                     $inData['keyword'] = $tmp;
//                 } else {
//                     $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
//                 }
//             }
//             if (stripos($http_referer, 'search.nate.com')) { //네이트
//                 $inData['search_site'] = 'nate';
//                 $inData['query_name'] = 'q';

//                 $tmp = substr($http_referer, stripos($http_referer, '&q=') + 3);

// //검색어 뒤에 '&' 파라미터 여부
//                 if (!stripos($tmp, '&')) {
//                     $inData['keyword'] = $tmp;
//                 } else {
//                     $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
//                 }
//             }
//             if (stripos($http_referer, 'search.yahoo.com')) { //야후
//                 $inData['search_site'] = 'yahoo';
//                 $inData['query_name'] = 'p';

//                 $http_referer = str_replace('https://', '', $http_referer);
//                 $http_referer = str_replace('http://', '', $http_referer);
//                 $http_referer = str_replace('www.', '', $http_referer);

//                 $tmp = '';
//                 if (stripos($http_referer, '?p=')) {
//                     $tmp = substr($http_referer, stripos($http_referer, '?p=') + 3);
//                 } else if (stripos($http_referer, '&p=')) {
//                     $tmp = substr($http_referer, stripos($http_referer, '&p=') + 3);
//                 }

// //검색어 뒤에 '&' 파라미터 여부
//                 if (!stripos($tmp, '&')) {
//                     $inData['keyword'] = $tmp;
//                 } else {
//                     $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
//                 }
//             }
//             if (stripos($http_referer, '.google.')) { //구글
//                 $inData['search_site'] = 'google';
//                 $inData['query_name'] = 'q';

//                 $tmp = substr($http_referer, stripos($http_referer, '&q=') + 3);

// //검색어 뒤에 '&' 파라미터 여부
//                 if (!stripos($tmp, '&')) {
//                     $inData['keyword'] = $tmp;
//                 } else {
//                     $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
//                 }
//             }
//             if (stripos($http_referer, 'search.paran.com')) { //파란
//                 $inData['searchEngine'] = 'paran';
//                 $inData['query'] = 'Query';

//                 $tmp = substr($http_referer, stripos($http_referer, '&Query=') + 7);

// //검색어 뒤에 '&' 파라미터 여부
//                 if (!stripos($tmp, '&')) {
//                     $inData['keyword'] = $tmp;
//                 } else {
//                     $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
//                 }
//             }
//             $inData['keyword'] = @iconv('EUC-KR', 'UTF-8', urldecode($inData['keyword']));
//             if (isset($inData['keyword'])) {
//                 $inData['keyword'] = urldecode($inData['keyword']);
//             }
//         }
// //$this->session->set_userdata(ip_address);
// //config/user_agent.php 에 등록되지 않은 browser deny  $this->session->set_userdata($data);
//         if ($is_deny) {
//             $inData['is_robot'] = 'N';
//             $this->statistics->insert($inData);
//             show_error('알수없는 브라우저 형식으로 접근 하였습니다.');
//             exit;
//         } else {
// //			if ($inData['menu_code'] != 'site_main') {


//             $this->statistics->insert($inData);

// //			} else {
// //아이피 중복 체크(하루)
// //				if (get_cookie('IW_VISITOR_' . $this->input->ip_address())) {
// //					$this->statistics->insert($inData);
// //				}
// //			}
//         }
//     }
	
    
	

    function send_ios_push(){
        $url = 'https://api.parse.com/1/push';
        $appId = 'w2pgJ4mOpfEHX8jLXb8VeXZltFDBQk4lOnQjOpXr';
        $restKey = 'ZleiYscGtI4JqZBwHfv0J24PkAhIcv6lWNnF3PPR';

        $deviceToken = '264a22c07f9ec879c50b06a7756253698b942c0edb2bc73436ae5169793655a5';  // using object Id of target Installation.

        $push_payload = json_encode(array(
                "where" => array(
                        "deviceToken" => $deviceToken,
                ),
                "data" => array(
                        "alert" => "This is the alert text."
                )
        ));

        $rest = curl_init();
        curl_setopt($rest,CURLOPT_URL,$url);
        curl_setopt($rest,CURLOPT_PORT,443);
        curl_setopt($rest,CURLOPT_POST,1);
        curl_setopt($rest,CURLOPT_POSTFIELDS,$push_payload);
        curl_setopt($rest,CURLOPT_HTTPHEADER,
                array("X-Parse-Application-Id: " . $appId,
                        "X-Parse-REST-API-Key: " . $restKey,
                        "Content-Type: application/json"));

        $response = curl_exec($rest);
        var_dump($response);


    }

    function adm_customer_distribution(){
         if ($this->_login_check()&&$this->_isAdminGroup()){
            $this->adm_pay->adm_customer_distribution();
        }
    }
    function adm_customer_ditribute_insert_exec(){
         if ($this->_login_check()&&$this->_isAdminGroup()){
            $this->adm_pay->adm_customer_ditribute_insert_exec();
        }
    }

    function adm_shipping_view() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_view();
    }
    function adm_shipping_edit() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_edit();
    }
    function adm_shipping_edit_post() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_edit_post();
    }

    function adm_shipping_charge() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_charge();
    }

    function adm_shipping_charge_exec() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_charge_exec();
    }

    function adm_shipping_charge_modify() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_charge_modify();
    }

    function adm_shipping_charge_modify_exec() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_charge_modify_exec();
    }

    function adm_shipping_charge_delete() {
        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_charge_delete();
    }

    function adm_delete_log_exec() {
        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_bbs->adm_delete_log_exec();
    }

    function adm_shipping_charge_delete_exec() {
        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_charge_delete_exec();
    }


    function adm_member_photo_insert(){
        if ($this->_login_check()&&$this->_isAdminGroup())
            $this->adm_bbs->adm_member_photo_insert();
    }

    function adm_list_log() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_bbs->adm_list_log();
    }

    function adm_shipping_port() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_port();
    }

    function adm_shipping_port_delete_exec() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_shipping->adm_shipping_port_delete_exec();
    }

    function adm_shipping_port_view() {

        if ($this->_login_check() && $this->grade_no == '1') 
            $this->adm_shipping->adm_shipping_port_view();
    }

    function adm_shipping_port_edit() {

        if ($this->_login_check() && $this->grade_no == '1') 
            $this->adm_shipping->adm_shipping_port_edit();
    }

    function adm_shipping_port_edit_exec(){

        if ($this->_login_check() && $this->grade_no == '1') 
            $this->adm_shipping->adm_shipping_port_edit_exec();
    }

    function adm_seller_web_list() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_bbs->adm_seller_web_list();
    }

    function adm_modify_seler_web() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_bbs->adm_modify_seler_web();
    }
    
    function adm_validate_modify_seller_web() {

        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_bbs->adm_validate_modify_seller_web();
    }

    function adm_upload_image_for_send_email() {
        if ($this->_login_check() && $this->grade_no == '1')
            $this->adm_bbs->adm_upload_image_for_send_email();
    }

    //required POST: icon_status, idx
    function quick_set_status(){
        ob_clean();
        $set_status_activity  = true;
        if($_SESSION['ADMIN']['grade_no']=='11' && $_SESSION['ADMIN']['business_type']=='seller'){
            if(isset($_POST['icon_status'])) $icon_status = $_POST['icon_status']; else $icon_status = '';
            if(isset($_POST['idx'])) $idx = $_POST['idx']; else $idx = 0;
            if(isset($_POST['chassis_no'])) $chassis_no = $_POST['chassis_no']; else $chassis_no = 0;
            if(isset($_POST['full_url'])) $full_url = $_POST['full_url']; else $full_url = 0;
            $where['car_owner'] = $_SESSION['ADMIN']['member_no'];
            if(!empty($icon_status)&&!empty($idx)){
                $data['icon_status'] = $icon_status;
                $where['idx'] = $idx;

                $quick_status = $this->bbs->adm_update($data, $where);

                if($quick_status){
                    $set_status_activity  = true;
                }else{
                    $set_status_activity  = false;
                }

                if($icon_status=="sale"){
                    $status = "sale";
                    $this->session->set_flashdata('msg_data', 'Success! The car has been set as Sale.');
                }elseif ($icon_status=="reserved") {
                    $status = "reserve";
                    $this->session->set_flashdata('msg_data', 'Success! The car has been set as Reserved.');
                }elseif ($icon_status=="soldout") {
                    $status = "soldout";
                    $this->session->set_flashdata('msg_data', 'Success! The car has been set as Soldout.');
                }

                $log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'Car idx: '.$idx.' (chassis_no: '.$chassis_no.') has been '.$status.' by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'car_status',
                    'created_dt'=>func_get_date_time()
                );
                $this->mo_activity_log->insert($log_data);

                if($set_status_activity==true){
                    $this->session->set_flashdata('msg_class', 'message_success');
                    redirect($full_url);
                }else{
                    $this->session->set_flashdata('msg_data', 'Modification Failed!');
                    $this->session->set_flashdata('msg_class', 'message_fail');
                    redirect($full_url);
                }
            }
        }
        
    }

    function quick_set_status_by_select_action(){
        ob_clean();

        if($_SESSION['ADMIN']['grade_no']=='11' && $_SESSION['ADMIN']['business_type']=='seller'){
            if(isset($_POST['icon_status'])) $icon_status = $_POST['icon_status']; else $icon_status = '';
            if(isset($_POST['idx'])) $idx = $_POST['idx']; else $idx = 0;
            if(isset($_POST['full_url'])) $full_url = $_POST['full_url']; else $full_url = 0;
            $car_owner = $_SESSION['ADMIN']['member_no'];
            if(!empty($icon_status)&&!empty($idx)){

                if($icon_status == 'not_published'){
                    $data['publish'] = 'N';
                    $this->session->set_flashdata('msg_data', 'Success! The car has been set as Unpublish.');
                }
                
                if($icon_status == 'published'){
                    $data['publish'] = 'Y';
                    $this->session->set_flashdata('msg_data', 'Success! The car has been set as Publish.');
                }

                if($icon_status == 'reserved' || $icon_status == 'soldout' || $icon_status == 'sale'){
                    $data['icon_status'] = $icon_status;
                }

                if($icon_status == 'sale'){
                    $this->session->set_flashdata('msg_data', 'Success! The car has been set as Sale.');
                }

                if($icon_status == 'reserved'){
                    $this->session->set_flashdata('msg_data', 'Success! The car has been set as Reserved.');
                }

                if($icon_status == 'soldout'){
                    $this->session->set_flashdata('msg_data', 'Success! The car has been set as Soldout.');
                }

                if($this->bbs->adm_update_car_seller($data, $idx,$car_owner)){

                    /*** CHECK POST ARRAY ***/
                    if(is_array($idx)){

                        foreach ($idx as $id) {
                            
                            $select_detail = $this->bbs->select_detail_for_seller($id);

                            /***  SAVE LOG ACTIVITY ***/
                            $log_data = array(
                                'author'=>$_SESSION['ADMIN']['member_no'],
                                'message'=>'Car idx: '.$id.' (chassis_no: '.$select_detail[0]->car_chassis_no.') has been set '.$icon_status.' by '.$_SESSION['ADMIN']['member_id'],
                                'log_type'=>'car',
                                'created_dt'=>func_get_date_time()
                            );

                            $this->mo_activity_log->insert($log_data);

                        }

                    }else{

                        $select_detail = $this->bbs->select_detail_for_seller($idx);

                        /***  SAVE LOG ACTIVITY ***/
                        $log_data = array(
                            'author'=>$_SESSION['ADMIN']['member_no'],
                            'message'=>'Car idx: '.$idx.' (chassis_no: '.$select_detail[0]->car_chassis_no.') has been set '.$icon_status.' by '.$_SESSION['ADMIN']['member_id'],
                            'log_type'=>'car',
                            'created_dt'=>func_get_date_time()
                        );

                        $this->mo_activity_log->insert($log_data);
                    }
                    $this->session->set_flashdata('msg_class', 'message_success');
                    redirect($full_url);

                }else{
                    $this->session->set_flashdata('msg_data', 'Modification Failed!');
                    $this->session->set_flashdata('msg_class', 'message_fail');
                    redirect($full_url);

                }
                
            }
        }
        
    }
}

?>