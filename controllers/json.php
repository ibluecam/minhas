<?php

/* ------------------------------------------------
 * 파일명 : json.php
 * 개  요 : 사이트 사용자 전체 Controller
  ------------------------------------------------ */
?>
<?php
require("lib/extractImagesFromPdf.php");
require 'libraries/PHPMailer/PHPMailerAutoload.php';	


class Json extends Controller {
    var $is_login;
    
    function Json() {
        parent::Controller();
		
		//호출된 메뉴 구분을 위해

        func_set_data($this, 'c', $this->input->get_post('c'));

        func_set_data($this, 'm', $this->input->get_post('m'));



//메뉴코드

        func_set_data($this, 'mcd', $this->input->get_post('mcd'));

		

        $this->load->model('mo_common', 'common');

        $this->load->model('mo_config', 'iw_config');

        $this->load->model('mo_member', 'member');

        $this->load->model('mo_bbs', 'bbs');
		
		$this->load->model('mo_email', 'email');
		
		func_set_data($this, 'site_admin', $this->iw_config->select('site_admin')->result());

//사이트 맵 조회

        func_set_data($this, 'site_map_list', $this->iw_config->select_site_map()->result());
		
		
        
        $this->load->library('iwc_common');     //IWC 공통 클래스

        $this->load->library('phpsession');     //php 세션

        $this->load->library('adm_bbs');     //게시판 관리

        $this->load->library('adm_site_meta');     //사이트 메타

        $this->load->library('adm_member'); 
		$this->load->library('iwc_bbs'); 
		
		
        if ($this->phpsession->get('member_id', 'USER') != '') {
            $this->is_login = 'Y';
            func_set_data($this, 'session_member_no', $this->phpsession->get('member_no', 'USER'));
            func_set_data($this, 'session_member_id', $this->phpsession->get('member_id', 'USER'));
            func_set_data($this, 'session_member_first_name', $this->phpsession->get('member_name', 'USER'));
            func_set_data($this, 'session_member_last_name', $this->phpsession->get('member_name', 'USER'));
            //func_set_data($this, 'session_email', $this->phpsession->get('email', 'USER'));
            func_set_data($this, 'session_grade_no', $this->phpsession->get('grade_no', 'USER'));
            //func_set_data($this, 'session_grade_name', $this->phpsession->get('grade_name', 'USER'));
        } else {
            func_set_data($this, 'session_member_no', '');
            func_set_data($this, 'session_member_id', '');
            func_set_data($this, 'session_member_first_name', '');
            func_set_data($this, 'session_member_last_name', '');
            //func_set_data($this, 'session_email', '');
            func_set_data($this, 'session_grade_no', '');
            //func_set_data($this, 'session_grade_name', '');
        }
        func_set_data($this, 'is_login', $this->is_login); //로그인 유무

    }
    public function getCountry(){
        $where=array();
        //Get model rows
        $result = $this->bbs->getCountry();
        //Save rows to data
        func_set_data($this, 'rows', $result);
        //Get content html
        $this->load->view('user/json/getCountry', $this->data, false);
        
    }
    public function getCarCountryList(){
        //$where=array();
        //Get make rows
        $where['country !='] = "";
        $where['category'] = "offer";
        $where['menu_code'] = "product";
        $result = $this->bbs->getLocation($where);
        //Save rows to data
        func_set_data($this, 'rows', $result);
        //Get content html
        $this->load->view('user/json/getLocation', $this->data, false);
        
    }
    public function getMake(){
        $where=array();
        //Get make rows

        $result = $this->bbs->getMake($where);
        //Save rows to data
        func_set_data($this, 'rows', $result);
        //Get content html
        $this->load->view('user/json/getMake', $this->data, false);
        
    }
    public function getModel(){
        $where=array();
        //Get model rows
        $where['car_make'] = $_REQUEST['car_make'];
        //$and =['car_country'] = $_REQUEST['country'];
        $result = $this->bbs->getModel($where);
        //Save rows to data
        func_set_data($this, 'rows', $result);
        //Get content html
        $this->load->view('user/json/getModel', $this->data, false);
        
    }
    public function getCarDetails(){
        $where=array();
        //Get model rows
        $where['idx'] = $_REQUEST['idx'];
        $result = $this->bbs->getCarDetails($where);
        //Save rows to data
        func_set_data($this, 'rows', $result);
        //Get content html
        $this->load->view('user/json/getCarDetails', $this->data, false);
        
    }
    public function getCarListpage(){
        
        //Get model rows

        $gets = $_GET;

        unset($gets['c']);
        unset($gets['m']);
        $param = array();
        $arrage = array();
        $ayear  = array();
        $limit_s  = array();
        $limit_e = 50;
        foreach($gets as $key => $value){

            if( $key == 'pr_max' || $key == 'pr_min' ){
                $arrage[$key] =  $value;
            }else if( $key == 'year_min' || $key == 'year_max'){
                $ayear[$key] =  $value;
            }else if( $key == 'limit_s'){
                $limit_s[$key] =  $value;
            }else $param[$key] =  $value;
        }
         $param=array_filter($param);
		 $arrage=array_filter($arrage); 
		 $ayear=array_filter($ayear);
         $limit_s=array_sum($limit_s);

        $result = $this->bbs->getCarListpage($param,$arrage,$ayear,$limit_s, $limit_e);
        //Save rows to data
        $today = date('Y-m-d h:i:s');
        $today = date_create($today);
        foreach ($result as $key=>$value) {
            $created_dt = date_create($result[$key]->created_dt);
            $interval = date_diff($today, $created_dt);
            /***GET car upload age in days***/
            $days = $interval->format('%a');
            if($days<=3){
                $result[$key]->created_status = 'new';
            }else{
                $result[$key]->created_status = 'old';
            }
            $result[$key]->image_name =$value->base_url."t_item_thumb/".$value->public_id;
        }
               
        func_set_data($this, 'rows', $result);
        //Get content html

        $this->load->view('user/json/getCarListpage', $this->data, false);
        
    }
    public function getCarImages(){
        
        //Get model rows

        $gets = $_GET;
        $idx = 0;

        unset($gets['c']);
        unset($gets['m']);


        $result = $this->bbs->getCarImages($gets);

        //$where=array();
        //Get model rows
        // $where['idx'] = $_REQUEST['idx'];
        // $result = $this->bbs->getCarImages($where);

        foreach ($result as $key=>$value) {
            $result[$key]->image_name ="http://".$_SERVER['HTTP_HOST']."/uploads/product/".$value->raw_name."_60".$value->file_ext;
            $result[$key]->full_image_url ="http://".$_SERVER['HTTP_HOST']."/uploads/product/".$value->raw_name."_600".$value->file_ext;
        }
        //Save rows to data  
        func_set_data($this, 'rows', $result);
        //Get content html
        $this->load->view('user/json/getCarImages', $this->data, false);        
    }
    public function getMinMaxPrice(){
        
        //Get model rows

        $gets = $_GET;
        $offset = 0;
        unset($gets['c']);
        unset($gets['m']);

        $result = $this->bbs->getMinMaxPrice($gets);

        //Save rows to data
        func_set_data($this, 'rows', $result);
        //Get content html

        $this->load->view('user/json/getMinMaxPrice', $this->data, false);
        
    }
	
	public function myjsonpost(){
		$imagepdf = new extractImagesFromPdf();
		$image = $_FILES['pdffile']['name'];
		//$mydata['img']=$image;
		$target = $_FILES['pdffile']['tmp_name'];
 		//$target = $target . basename( $_FILES['pdffile']['name']) ; 
		$subSerial = "";
		//$seccuer = func_get_config($bbs_config_result, 'attach_thumbnail_size3');
		$id = $_POST['id'];
		$fileContent = $imagepdf->toByteArray($target);
		$imagepdf->main($subSerial, $fileContent);
		$mydata['img'] = $imagepdf->arrayimg;
		$mydata['size']  = $imagepdf->arraysize;
        $mydata['width'] = $imagepdf->arraywidth;
        $mydata['height'] = $imagepdf->arrayheight;
        $mydata['ext'] = $imagepdf->arrayext;
		for ($i=0;$i<count($mydata['img']);$i++){
			if ($i==0){
				$primary = 0;
				}
			else {
				$primary = 1;
				}
			

			//$oldpath = str_replace("\\","/",FCPATH);
			$imagepath = "/uploads/cars/".$mydata['img'][$i].".jpg";
			$fielpath = "/uploads/cars/";
			//$rawname = $mydata['img'][$i];
		// $data = array("file_name"=>$mydata['img'][$i].".jpg",
  //           "file_type"=>"image/jpeg",
  //           "file_size"=>$mydata['size'][$i],
  //           "image_width"=>$mydata["width"][$i],
  //           "image_height"=>$mydata["height"][$i],
  //           "image_type"=>"jpeg",
  //           "image_size_str"=>'width="600" height="600"',
  //           "full_path"=>$imagepath,
  //           "orig_name"=>$mydata['img'][$i].".jpg",
  //           "idx"=>$id,"is_image"=>1,
  //           "sort"=>$primary,"file_path"=>$fielpath,
  //           'raw_name'=>$mydata['img'][$i],
  //           'file_ext'=> ".".$mydata['ext'][$i]);	
		
                $data['bbs_idx'] = $id;
                $data['public_id'] = $mydata['img'][$i].".jpg";
                $data['base_url']  = $fielpath;
                $data['resource_type'] = "image";
                $data['created_dt'] = date('Y-m-d H:i:s');
                // if you want to save in db,where here
                
                $this->bbs->insertimagejson($data); 

                $inData['bbs_idx'] = $id; 
                $mydata1['img'][$i]= $data;
                $this->mo_file->resort_image($inData); 	
			}
		
		//$result = $this->bbs->mytestdata();
		//func_set_data($this, 'rows', $result);
		
		$this->load->view('user/json/myjsonpost.php',$mydata1);
		
		}
		
		public function myupdateprimaryimage(){
			$pid = $_POST['pid'];
			$image = $_POST['img'];
			$data = array('sort'=>1);
			$this->bbs->myupdateimagejson($pid,$image,$data); 
			
			}
		
		public function mydeleteimagepdf(){
			
			$image = $_POST['img'];
			$result = $this->bbs->mydeleteimagejson($image);
			if ($result){
				if(file_exists("uploads/file/file_image/".$image)){
					unlink("uploads/file/file_image/".$image);
					
				}
			
			
			}
		
		}
		
    function email_inquiry(){
        ob_clean();
        $customer_email = $this->input->post('customer_email');
        $customer_name = $this->input->post('customer_name');
        $message     = $this->input->post('message');
        $phone     = $this->input->post('customer_phone');
        $country  = $this->input->post('customer_country');
        $link   = $this->input->post('link');

        /*** GET CUSTOMER SUPPORT (SALES TEAM) ***/
        $where['idx'] = $this->input->post('car_idx');
        $members = $this->bbs->select_customer_service($where);
        $sale_email=$members[0]->email;

        
        $json =array();
		
	        
        
        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output
        

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'ssl://bohor.arvixe.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@motorbb.com';                 // SMTP username
        $mail->Password = '~PF!@BUtg)b*';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        $mail->From = 'noreply@motorbb.com';   
        $mail->FromName = $customer_name;
        //$mail->addAddress('aslam0919@blauda.com', 'IBLAUDA'); 
         // Add a recipient
        $mail->addAddress( $sale_email, $customer_name);     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo($customer_email, $customer_email);
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML
        $html = '<html><head></head><body>';
        $html.= '<b>Country: </b>'.$country.'<br/><b>Tel: </b>'.$phone.'<br><b>Message: </b>'.$message.'<br><b>Link: </b><a href="'.$link.'">'.$link.'</a>';
      
        $mail->Subject = "Customer's Inquiry";
        $mail->Body    = $html;
        
        
        $html.= '</body></html>';
        // echo $html;
        //  exit();
		
        if($mail->send()) {
            $json['success'] = 1;
            $json['result_message'] = 'Message has been sent!';
			$json['customer_email'] = $customer_email;
			$json['customer_name'] = $customer_name;
			$json['customer_message'] = $message;
            $json['customer_phone'] = $phone;
            $json['customer_country'] = $country;
			$json['customer_link'] = $link;
            $this->email->send_thank_for_contact($customer_email, $customer_name, $country, $phone, $message, $link);			
        } else {
            $json['success'] = 0;
            $json['result_message'] = 'Massage sending failed!!';
        }
        sleep(1);
        echo json_encode($json);
    }   
}

?>