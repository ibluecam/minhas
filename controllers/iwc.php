<?php



/* ------------------------------------------------

 * 파일명 : iwc.php

 * 개  요 : ROOT Controller

  ------------------------------------------------ */

?>

<?php



class Iwc extends Controller {



	var $data;

	var $member_id;

	var $member_name;

	var $admin_login_yn;

	var $iwc_login_yn;

	var $base_url;



//Constructor

	function __construct() {

		parent::Controller();



//호출된 메뉴 구분을 위해

		func_set_data($this, 'c', $this->input->get_post('c'));

		func_set_data($this, 'm', $this->input->get_post('m'));



//메뉴코드/명

		func_set_data($this, 'mcd', $this->input->get_post('mcd'));

		func_set_data($this, 'mcd_name', $this->input->get_post('mcd_name'));



		/* ------------------------------------------------------------

		  - Model Load

		  ------------------------------------------------------------ */

		$this->load->model('mo_config', 'iw_config');

		$this->load->model('mo_member', 'member');

		$this->load->model('mo_bbs', 'bbs');

		$this->load->model('mo_survey', 'survey');



//사이트 관리정보 설정 조회

		func_set_data($this, 'site_admin', $this->iw_config->select('site_admin')->result());

//사이트 맵 조회

		func_set_data($this, 'site_map_list', $this->iw_config->select_site_map()->result());



		/* ------------------------------------------------------------

		  - IW Library Load

		  ------------------------------------------------------------ */

		$this->load->library('iwc_common');	  //IWC 공통 클래스

		$this->load->library('phpsession');	  //php 세션

		$this->load->library('iwc_site_meta');   //사이트 메타

		$this->load->library('iwc_site_admin');  //사이트 관리설정 정보

		$this->load->library('iwc_site_map');	//사이트 맵

		$this->load->library('iwc_site_design'); //사이트 디자인

		$this->load->library('iwc_bbs');		 //게시판 관리

		$this->load->library('iwc_member');	  //회원 관리

//관리자 세션 정보

		$this->member_id = $this->phpsession->get('member_id', 'ADMIN');

		$this->member_name = $this->phpsession->get('member_name', 'ADMIN');

		$this->email = $this->phpsession->get('email', 'ADMIN');

		$this->admin_login_yn = $this->phpsession->get('admin_login_yn', 'ADMIN');

		$this->iwc_login_yn = $this->phpsession->get('iwc_login_yn', 'ADMIN');

		$this->base_url = $this->phpsession->get('base_url', 'ADMIN');



		func_set_data($this, 'member_id', $this->member_id);

		func_set_data($this, 'member_name', $this->member_name);

		func_set_data($this, 'email', $this->email);

		func_set_data($this, 'admin_login_yn', $this->admin_login_yn);

		func_set_data($this, 'iwc_login_yn', $this->iwc_login_yn);

		func_set_data($this, 'base_url', $this->base_url);



//관리자 HTML INCLUDE

		func_set_data($this, 'inc_top', $this->load->view('admin/inc/inc_top', $this->data, true));	//관리자 top

		func_set_data($this, 'inc_left', $this->load->view('admin/inc/inc_left', $this->data, true));   //관리자 left

		func_set_data($this, 'inc_bottom', $this->load->view('admin/inc/inc_bottom', $this->data, true)); //관리자 bottom



		func_set_data($this, 'customer_name', func_get_config($this->data['site_admin'], 'customer_name'));   //고객(상호명)

		func_set_data($this, 'title', func_get_config($this->data['site_admin'], 'site_name'));   //사이트 명

		func_set_data($this, 'site_url', func_get_config($this->data['site_admin'], 'site_url')); //사이트 URL

	}



//인덱스

	function index() {

//ROOT 관리자 로그인 처리가 되어 있다면...

		if ($this->_login_check()) {

//관리자 인덱스

			$this->view_site_meta();

		}

	}



//ROOT 로그인 세션 체크

	function _login_check() {

		if ($this->iwc_login_yn != 'Y') {

//로그인 페이지

			$this->load->view('admin/login', $this->data, false);



			return false;

		} else {

			return true;

		}

	}



	/* ------------------------------------------------------------------------------------------------ */



	/* ------------------------------------------------

	  - 사이트 메타 테그 관리

	  ------------------------------------------------ */



//사이트 메타 테그 HTML

	function view_site_meta() {

		if ($this->_login_check())

			$this->iwc_site_meta->view_site_meta();

	}



//사이트 메타 테그 EXEC

	function exec_site_meta() {

		if ($this->_login_check())

			$this->iwc_site_meta->exec_site_meta();

	}



	/* ------------------------------------------------

	  - 사이트 맵 관리

	  ------------------------------------------------ */



//사이트 맵 HTML

	function view_site_map() {

		if ($this->_login_check())

			$this->iwc_site_map->view_site_map();

	}



//사이트 맵 등록&수정 EXEC

	function exec_site_map() {

		if ($this->_login_check())

			$this->iwc_site_map->exec_site_map();

	}



//사이트 맵 삭제 EXEC

	function exec_delete_site_map() {

		if ($this->_login_check())

			$this->iwc_site_map->exec_delete_site_map();

	}



//사이트 맵 메뉴코드 중복 체크

	function exec_dup_chk_menu_code() {

		if ($this->_login_check())

			$this->iwc_site_map->exec_dup_chk_menu_code();

	}



	/* ------------------------------------------------

	  - 사이트 관리 정보 설정

	  ------------------------------------------------ */



//사이트 관리정보 설정 HTML

	function view_site_admin() {

		if ($this->_login_check())

			$this->iwc_site_admin->view_site_admin();

	}



//사이트 관리정보 등록&수정 EXEC

	function exec_site_admin() {

		if ($this->_login_check())

			$this->iwc_site_admin->exec_site_admin();

	}



//사이트 관리자 계정 생성 EXEC

	function exec_create_admin_account() {

		if ($this->_login_check())

			$this->iwc_site_admin->exec_create_admin_account();

	}



//ROOT 관리자 계정 생성 EXEC

	function exec_iw_create_admin_account() {

		if ($this->_login_check())

			$this->iwc_site_admin->exec_iw_create_admin_account();

	}



//사이트 DB 데이터 리셋

	function reset_site_admin() {

		if ($this->_login_check())

			$this->iwc_site_admin->reset_site_admin();

	}



	/* ------------------------------------------------

	  - 사이트 디자인 설정

	  ------------------------------------------------ */



//사이트 디자인 서브 HTML

	function view_site_design_sub() {

		if ($this->_login_check())

			$this->iwc_site_design->view_site_design_sub();

	}



//디자인 서브 HTML 소스 가져오기

	function view_site_design_sub_source() {

		if ($this->_login_check())

			$this->iwc_site_design->view_site_design_sub_source();

	}



//사이트 디자인 서브 EXEC

	function exec_site_design_sub() {

		if ($this->_login_check())

			$this->iwc_site_design->exec_site_design_sub();

	}



//사이트 디자인 LEFT 메뉴 HTML

	function view_site_design_left_menu() {

		if ($this->_login_check())

			$this->iwc_site_design->view_site_design_left_menu();

	}



//사이트 디자인 LEFT 메뉴 EXEC

	function exec_site_design_left_menu() {

		if ($this->_login_check())

			$this->iwc_site_design->exec_site_design_left_menu();

	}



	/* ------------------------------------------------

	  - 사이트 게시판 설정

	  ------------------------------------------------ */



//게시판 설정 HTML (메뉴코드 별)

	function view_bbs_config() {

		if ($this->_login_check())

			$this->iwc_bbs->select_bbs_config();

	}



//게시판 설정 exec

	function exec_bbs_config() {

		if ($this->_login_check())

			$this->iwc_bbs->exec_bbs_config();

	}



//게시판 설정 복사

	function copy_bbs_config() {

		if ($this->_login_check())

			$this->iwc_bbs->copy_bbs_config();

	}



	/* ------------------------------------------------

	  - 사이트 일정관리 설정

	  ------------------------------------------------ */



//일정관리 설정 HTML (메뉴코드 별)

	function view_calendar_config() {

		if ($this->_login_check())

			$this->iwc_calendar->select_calendar_config();

	}



//일정관리 설정 exec

	function exec_calendar_config() {

		if ($this->_login_check())

			$this->iwc_calendar->exec_calendar_config();

	}



	/* ------------------------------------------------

	  - 회원 설정

	  ------------------------------------------------ */



//회원가입 설정 HTML

	function view_member_join_config() {

		if ($this->_login_check())

			$this->iwc_member->select_member_join_config();

	}



//회원가입 설정 exec

	function exec_member_join_config() {

		if ($this->_login_check())

			$this->iwc_member->exec_member_join_config();

	}



//회원로그인 설정 HTML

	function view_member_login_config() {

		if ($this->_login_check())

			$this->iwc_member->select_member_login_config();

	}



//회원로그인 설정 exec

	function exec_member_login_config() {

		if ($this->_login_check())

			$this->iwc_member->exec_member_login_config();

	}



//회원 등급 HTML

	function view_member_grade() {

		if ($this->_login_check())

			$this->iwc_member->view_member_grade();

	}



//회원 등급 EXEC

	function exec_member_grade() {

		if ($this->_login_check())

			$this->iwc_member->exec_member_grade();

	}



//회원 등급 삭제

	function exec_delete_member_grade() {

		if ($this->_login_check())

			$this->iwc_member->exec_delete_member_grade();

	}



}

?>
