<?php

/* ------------------------------------------------
 * 파일명 : user.php
 * 개  요 : 사이트 사용자 전체 Controller
  ------------------------------------------------ */
?>
<?php
include('./libraries/hybridauth/Hybrid/Auth.php');
include('./libraries/Encryption.php');
include("./libraries/phpfastcache.php");
include("./libraries/Minify/src/autoload.php");
include("resize_class/resize_class.php");
require 'resize_class/ImageManipulator.php';
require 'controllers/coludinary/main.php';
use MatthiasMullie\Minify;
class User extends Controller {

    var $data;
    var $mcd;     //메뉴코드
    var $menu_type;  //사이트맵 등록시 해당 메뉴코드 사용 모듈 구분
    var $member_join_mcd;  //회원가입 메뉴코드
    var $member_login_mcd; //로그인 메뉴코드
    var $is_login;   //로그인 여부f
    var $upload_version='1.0.37';
    var $cache;
    var $frieght_cost_plus    = 250;
    var $inspection_cost_plus = 100;
    var $insurance_cost_plus  = 50;
    //Constructor

    function __construct() {
        parent::Controller();
        func_redirect_to_ssl();
        $this->cache = phpFastCache();
        $this->load->helper("url");

        $this->load->helper('file');


        $this->load->library("pagination");

        ob_clean();
        $this->load->library('session');



        /* ------------------------------------------------------------
          - Model Load
          ------------------------------------------------------------ */
        $this->load->model('mo_config', 'iw_config');
        $this->load->model('mo_statistics', 'statistics');
        $this->load->model('mo_common', 'common');
        $this->load->model('mo_bbs', 'mo_bbs');
        $this->load->model('mo_message', 'mo_message');
        $this->load->model('mo_member', 'member');
        $this->load->model('mo_customer', 'customer');
        $this->load->model('mo_file', 'mo_file');
        $this->load->model('mo_email', 'mo_email');
        $this->load->model('mo_shipping', 'mo_shipping');

        /* ------------------------------------------------------------
          - Library Load
          ------------------------------------------------------------ */
        $this->load->library('iwc_common'); //IWC 공통 클래스
        $this->load->library('phpsession');
        $this->load->library('Adm_statistics');//php 세션
        $this->load->library('adm_popup');      //팝업관리

        //
        //메뉴코드
        $this->mcd = $this->input->get_post('mcd');
        func_set_data($this, 'mcd', $this->mcd);
        func_set_data($this, 'base_url', '/?c=user'); //기본 URL(메인)
        //로그인이 되었다면..
        $this->_restoreLoginSession();
        if ($this->phpsession->get('member_id', 'ADMIN') != '') {
            $this->is_login = 'Y';
			func_set_data($this, 'session_member_no', $this->phpsession->get('member_no', 'ADMIN'));
            func_set_data($this, 'session_member_id', $this->phpsession->get('member_id', 'ADMIN'));
			func_set_data($this, 'session_member_first_name', $this->phpsession->get('member_first_name', 'ADMIN'));
            func_set_data($this, 'session_email', $this->phpsession->get('email', 'ADMIN'));
            func_set_data($this, 'session_grade_no', $this->phpsession->get('grade_no', 'ADMIN'));
            func_set_data($this, 'session_bussines_type', $this->phpsession->get('business_type', 'ADMIN'));
            func_set_data($this, 'session_member_country', $this->phpsession->get('member_country', 'ADMIN'));
            //func_set_data($this, 'session_grade_name', $this->phpsession->get('grade_name', 'USER'));

        } else {
			func_set_data($this, 'session_member_no', '');
            func_set_data($this, 'session_member_id', '');
            func_set_data($this, 'session_member_first_name', '');
            func_set_data($this, 'session_member_last_name', '');
            func_set_data($this, 'session_email', '');
            func_set_data($this, 'session_grade_no', '');
            func_set_data($this, 'session_bussines_type', '');
            func_set_data($this, 'session_member_country', '');
            //func_set_data($this, 'session_grade_name', '');
        }

        func_set_data($this, 'is_login', $this->is_login); //로그인 유무
        $this->_load_page_var();
		
		

        //func_set_data($this, 'site_admin', $this->iw_config->select('site_admin')->result()); //사이트 관리정보 설정 조회

    }
	


    //index
    function _checkIfMobile(){
        $this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();

        if ($detect->isMobile() && !$detect->isTablet()){
            redirect('/?c=user&m=mobile_home');
             exit;
            
        }

       
    }

    function index() {

       

        $this->_init();
		
        //메뉴코드가 존재하지 않으면 사이트 메인을 호출한다.

        // if ($this->mcd == '') {
        //     $this->_get_site_main();
        // } else {
        //     $this->_get_site_sub();
        // }
        $this->public_home();

        $this->_checkIfMobile();
		
		
    }


  // CHENDA START

    //   public function vehicle() {
    //     $config = array();
    //     $config["base_url"] = base_url() . "/?c=user&m=vehicle";
    //     $config["total_rows"] = $this->mo_bbs->record_count();
    //     $config['use_page_numbers'] = TRUE;
    //     $config['cur_tag_close'] = '</a>';
    //     $config['cur_tag_open'] = '&nbsp;<a class="current">';
    //     $config["per_page"] = 12;
    //     $config["uri_segment"] = 3;
    //     $config['prev_link'] = 'Previous';
    //     $config['next_link'] = 'Next';
    //     $ajax_pagination_config['anchor_class'] = 'page_link';


    //     $this->pagination->initialize($config);
    //     $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    //     $results = $this->mo_bbs->get_all_cars($config["per_page"], $page);
    //     $links = $this->pagination->create_links();
    //     func_set_data($this, 'get_all_car',$results);
    //     func_set_data($this, 'links',explode('&nbsp;',$links));
    //     $this->_load_view_layout('user/vehicle');
    // }


    // CHENDA END

    // ==================== The First Load Ajax ===========================//

     function list_seller_profile(){

        // Additional parameters
        $seller_profile= 'seller';
        $country= $this->input->get_post('seller_country');
        $keyword = $this->input->get_post('keyword');

        if($country !=""){
            $where['member.member_country ='] = $country;
        }else{
             $where['member.business_type ='] = $seller_profile;
        }
        if($keyword !=""){
            $where['keyword_member_name'] = $keyword;
        }else{
            $where['member.business_type ='] = $seller_profile;
        }


        $per_page = 16;

        if(isset($_GET["page"])){

            if ($_GET["page"] != 0) {

                $page=$_GET["page"];
            }else{
                $page=1;
            }



        }else{
            $page=1;
        }
        $start_page=($page-1)*$per_page;

        // $offset = 0;   //You Skip first 20 rows
        // $num_rows = 10; //You take 10 row
        $sellers = $this->customer->customer_select($where, $start_page, $per_page);
        $total_num_row = $sellers->total_count;
        $rows = $sellers->result();

        //.var_dump($rows);
        // Get total num_rows for pagination
        $count_num_row = $sellers->total_count;
        func_set_data($this, 'hide_search_box', true);
        func_set_data($this, 'sellers',$rows);
        func_set_data($this, 'total_count',$count_num_row);
        func_set_data($this, 'perpagepagination', $per_page);
        func_set_data($this, 'getnumpage', $page);

        $country=$this->customer->country_seller();
        func_set_data($this,'country',$country);

        $this->_load_view_layout_ajax('user/content/list-seller-profile');
    }

    function set_image_car_ext(){

        $dir    = 'E:/wamp/www/motorbb_upload_image/uploads/cars/';
        $images = scandir($dir);
        unset($images[0]);
        unset($images[1]);

        $image_count = count($images);

        foreach($images as $image){
            
            $info = new SplFileInfo($image);
            /*** FILE NAME IS AN PUNLISH ID ***/
            $file_name =  basename($image,'.'.$info->getExtension());

            $data = array(
               'public_id' => $file_name.$info->getExtension(),
               'base_url' => '/uploads/cars/'
            );

            echo $file_name.'<br/>';
            
            $this->mo_file->update_path_images($data,$file_name);
        }
    }

    function download_thumb(){

        $dir    = 'uploads/cars/';
        $images = scandir($dir);
        unset($images[0]);
        unset($images[1]);
        foreach($images as $image){
             $resizeObj = new resize('uploads/cars/'.$image); 

            // *** 2) Resize image (options: exact, portrait, landscape, auto, crop) 
            $resizeObj -> resizeImage(200, 140, 'crop'); 

            // *** 3) Save image 
            $resizeObj -> saveImage('uploads/cars/thumb/'.$image, 100); 
            //copy('uploads/cars/'.$image, 'uploads/cars/thumb/'.$image);
        }
    }

    function select_all_car_images(){
        header('Content-Type: application/json');
        $images = $this->mo_bbs->select_all_images();
       
        echo json_encode($images, JSON_UNESCAPED_SLASHES);
        
    }
    function get_all_car_images(){
        $images = $this->mo_bbs->select_all_images();
        foreach($images as $image){
            echo $image->secure_url."<br/>";
        }
    }

    function download_images(){
        $images = $this->mo_bbs->select_all_images();
        foreach($images as $image){
            $content = file_get_contents($image->secure_url);
            $image_mime = image_type_to_mime_type(exif_imagetype($image->secure_url));
            var_dump($image_mime);
            if($image_mime=='image/png'){
                $ext = '.png';
            }else{
                $ext = '.jpg';
            }

            $data = array(
               'public_id' => $image->public_id.$ext,
               'base_url' => '/uploads/cars/'
            );

            $this->mo_file->update_path_images($data,$image->id);

            file_put_contents('uploads/cars/'.$image->public_id.$ext, $content);

        }
        
    }
    
      

    function download_user_profile(){
        $profiles = $this->mo_file->select_all_user_profile();
        foreach($profiles as $profile){
            $content = file_get_contents($profile->secure_url);
            $image_mime = image_type_to_mime_type(exif_imagetype($profile->secure_url));
            var_dump($image_mime);
            if($image_mime=='image/png'){
                $ext = '.png';
            }else{
                $ext = '.jpg';
            }

            $data = array(
               'public_id' => $profile->public_id.$ext,
               'base_url' => '/uploads/users/'
            );

            $this->mo_file->update_path_user_profile($data,$profile->id);

            file_put_contents('uploads/users/'.$profile->public_id.$ext, $content);
        }
        
    }

    function home(){
        
        func_set_data($this, 'test_variable', "output test_variable");
        $get_cars = $this->mo_bbs->get_car_home_page();
		$get_share = $this->mo_bbs->get_car_home_page();
        //echo $this->db->last_query();
         //var_dump($get_cars);
        func_set_data($this, 'get_cars',$get_cars);
		func_set_data($this, 'get_share',$get_share);
        $premium_cars= $this->mo_bbs->get_public_premium_car(array(), 4);
        func_set_data($this, 'premium_cars',$premium_cars);
        $this->_load_view_layout_ajax('user/content/homepages');

    }

     // ==================== The First Load Ajax ===========================//



    //========================= Start Load Page ajax ===========================================//

    function loadpageajax(){

        //$this->session->unset_userdata("sessionkeepdata_pagesearchajax"," ");


        func_set_data($this, 'test_variable', "output test_variable");
        $get_cars = $this->mo_bbs->get_car_home_page();
        func_set_data($this, 'get_cars',$get_cars);
        $premium_cars= $this->mo_bbs->get_premium_car(array(), 4);

        func_set_data($this, 'premium_cars',$premium_cars);
        $this->_load_view_layout_ajax('user/content/homepages');


    }



    //========================= End Load Page ajax ===========================================//





     //========================= Start Load Search Page ajax ===========================================//

     /* function searchpageajax(){


            if(isset($_GET["brand"]) and isset($_GET["page"])){



                $brandbyclick = $_GET["brand"];

                $per_page = 12;

                if ($_GET["page"] != 0) {

                    $page=$_GET["page"];
                }else{
                    $page=1;
                }



                $newdata = array(
                 'sesfunc'  => 'searchpageajax',
                 'brand'     => $brandbyclick,
                 'page' => $page
                );

                $this->session->set_userdata('sessionkeepdata_pagesearchajax',$newdata);





                $start_page=($page-1)*$per_page;
                $searchbrandcars = $this->mo_bbs->searchbybrand($brandbyclick,$start_page,$per_page);


                $countsearchbrandcars = $this->mo_bbs->countsearchbybrand($brandbyclick);

                $countsearchbrandcars = $countsearchbrandcars[0]->countdatabranecar;

                func_set_data($this, 'brandcars', $brandbyclick);

                func_set_data($this, 'searchbrandcars', $searchbrandcars);
                func_set_data($this, 'perpagepagination', $per_page);

                func_set_data($this, 'getnumpage', $page);

                func_set_data($this, 'countsearchbrandcars', $countsearchbrandcars);
                $this->_load_view_layout_ajax('user/content/searchajax');

            }


    }




    function searchbytypeajax(){


        if(isset($_GET["type"]) and isset($_GET["page"])){


            $typebyclick = $_GET["type"];

            $per_page = 12;

            if ($_GET["page"] != 0) {

                $page=$_GET["page"];
            }else{
                $page=1;
            }







            $start_page=($page-1)*$per_page;

            $searchtypecars = $this->mo_bbs->searchbytypecars($typebyclick,$start_page,$per_page);

            $countsearchtypecars = $this->mo_bbs->countsearchbytype($typebyclick);

            $countsearchtypecars = $countsearchtypecars[0]->countdatatypecar;
            func_set_data($this, 'typecars', $typebyclick);

            func_set_data($this, 'searchtypecars', $searchtypecars);
            func_set_data($this, 'perpagepagination', $per_page);

             func_set_data($this, 'getnumpage', $page);
            func_set_data($this, 'countsearchtypecars', $countsearchtypecars);
            $this->_load_view_layout_ajax('user/content/searchbraneajax');

    }
}*/


     //========================= End Load Search Page ajax ===========================================//



	//======================== Show all car with ajax =============================//


		function showallcar(){

            if(isset($_GET["show"])){

                $per_page=12;
                if ($_GET["page"] != 0){
                    $page=$_GET["page"];
                }else{
                    $page=1;
                }




                 $newdata = array(
                     'sesfunc'  => 'showallcar',
                     'show'     => "all",
                     'page' => $page
                );

                $this->session->set_userdata('sessionkeepdata_pagesearchajax',$newdata);


                $start_page=($page-1)*$per_page;
    			$results = $this->mo_bbs->get_all_car_ajax($start_page,$per_page);

                $count_get_all_cars=$this->mo_bbs->count_get_all_car_ajax();
                $count_get_all_cars=$count_get_all_cars[0]->count_get_all_car;

                func_set_data($this, 'countcars',$count_get_all_cars);
    			func_set_data($this, 'get_all_car',$results);
                func_set_data($this, 'perpagepagination',$per_page);
                func_set_data($this, 'getnumpage', $page);
            	$this->_load_view_layout_ajax('user/content/vehicle');
			}

		}



	//======================== Show all car with ajax =============================//


    //========================= Start Load Search box Page ajax ===========================================//

    /*function search_boxpageajax(){


         if(isset($_POST["carmake"]) and isset($_POST["carmodel"]) and isset($_POST["keyword"]) and isset($_POST["searchminyear"]) and isset($_POST["searchminprice"]) and isset($_POST["searchcountry"]) and isset($_POST["searchmaxyear"]) and isset($_POST["searchmaxprice"])){


            $this->session->set_userdata('sessionkeepdata_pagesearchajax',"");

            $carmake = $_POST["carmake"];
            $carmodel = $_POST["carmodel"];
            $keyword = $_POST["keyword"];
            $carminyear = $_POST["searchminyear"];
            $carminprice = $_POST["searchminprice"];
            $carcountry = $_POST["searchcountry"];
            $carmaxyear = $_POST["searchmaxyear"];
            $carmaxrpice = $_POST["searchmaxprice"];

            $per_page = 12;

            if (isset($_GET["page"])) {

                $page=$_GET["page"];
            }else{
                $page=1;
            }



            $newdata = array(
                     'sesfunc'  => 'search_boxpageajax',
                     'carmake'     => $carmake,
                     'carmodel'     => $carmodel,
                     'keyword'     => $keyword,
                     'carminyear'     => $carminyear,
                     'carminprice'     => $carminprice,
                     'carcountry'     => $carcountry,
                     'carmaxyear'     => $carmaxyear,
                     'carmaxrpice'     => $carmaxrpice,
                     'page' => $page
                );

            $this->session->set_userdata('sessionkeepdata_boxsearchajax',$newdata);


            $start_page=($page-1)*$per_page;


            $searchboxcar = $this->mo_bbs->searchboxcar($carmake,$carmodel,$keyword,$carminyear,$carminprice,$carcountry,$carmaxyear,$carmaxrpice,$start_page,$per_page);
            $countsearchboxcars = $this->mo_bbs->countsearchboxcar($carmake,$carmodel,$keyword,$carminyear,$carminprice,$carcountry,$carmaxyear,$carmaxrpice);

            $countsearchboxcars = $countsearchboxcars[0]->countsearhbox;

            func_set_data($this, 'carmake', $carmake);
            func_set_data($this, 'carmodel', $carmodel);
            func_set_data($this, 'keyword', $keyword);
            func_set_data($this, 'carminyear', $carminyear);
            func_set_data($this, 'carminprice', $carminprice);
            func_set_data($this, 'carcountry', $carcountry);
            func_set_data($this, 'carmaxyear', $carmaxyear);
            func_set_data($this, 'carmaxrpice', $carmaxrpice);

            func_set_data($this, 'searchboxcar', $searchboxcar);
            func_set_data($this, 'perpagepagination', $per_page);
             func_set_data($this, 'getnumpage', $page);
            func_set_data($this, 'countsearchboxcars', $countsearchboxcars);
            $this->_load_view_layout('user/content/page_data_searchfrombox');


        }

    }*/

    //========================= End Load Search box Page ajax ===========================================//



    //========================= Search all by country flag ==========================================//


    /*function search_bycountry(){

        if(isset($_GET["CountryID"]) && isset($_GET["page"])){


            $per_page = 12;

            if ($_GET["page"] != 0) {

                $page=$_GET["page"];

            }else{
                $page=1;
            }


            $start_page=($page-1)*$per_page;


            $countryid = $_GET["CountryID"];



            $newdata = array(
                     'sesfunc'  => 'search_bycountry',
                     'CountryID'     => $countryid,
                     'page' => $page
                );

            $this->session->set_userdata('sessionkeepdata_pagesearchajax',$newdata);


            $start_page=($page-1)*$per_page;
            $search_alldata_bycountry_flag = $this->mo_bbs->search_alldata_bycountry_flag($countryid,$start_page,$per_page);
            func_set_data($this, 'searchbycountryflag', $search_alldata_bycountry_flag);

            $countcountrylist = $this->mo_bbs->count_search_alldata_bycountry_flag($countryid);
            $countcountrylist = $countcountrylist[0]->countcountrylist;
            func_set_data($this, 'getcountcountrylist', $countcountrylist);

            func_set_data($this, 'getcountryid', $countryid);

            func_set_data($this, 'perpagepagination', $per_page);
            func_set_data($this, 'getnumpage', $page);


            $this->_load_view_layout_ajax('user/content/page_search_coutry_flag');

        }

    }
*/

    //========================= Search all by country flag ==========================================//


    function _load_public_view($view_path, $arr_ex_js=array(), $arr_ex_css=array()){
		
       
		$titlepage= "$_SERVER[REQUEST_URI]";
		
		$title = "";
		//set page title header
        if(strpos($titlepage,'public_home') !== false){
			$title = " Korea and japan used car - Minhas Trading";
		}elseif(strpos($titlepage,'car_list') !== false){
			$seg = $this->uri->segment(2);
			if(isset($_GET['keyword'])){
				$title = "Search car - Minhas Trading";
			}elseif(isset($_GET["car_make"])){
				$title = ucfirst(strtolower($_GET["car_make"])).' - Minhas Trading';
			}elseif($seg == "car_make"){
				$make = $this->uri->segment(3);
				$title = ucfirst(strtolower($make)).' - Minhas Trading';
			}elseif($seg == "car_body_type"){
				$car_body_type = $this->uri->segment(3);
				$title = ucfirst(strtolower($car_body_type)).' - Minhas Trading';
			}elseif($seg == "country"){
				$country = $this->uri->segment(3);
				if($country == 'jp'){
					$title = "Japan used cars - Minhas Trading";
				}elseif($country == 'kr'){
					$title = "Korea used cars - Minhas Trading";
				}
			}elseif(isset($_GET["car_body_type"])){
				$title = ucfirst($_GET["car_body_type"])." - Minhas Trading";
			}elseif(isset($_GET["country"])){
				if($_GET["country"] == 'jp'){
					$title = "Japanese and Korean Used Car For Sale | Minhas Trading";
				}elseif($_GET["country"] == 'kr'){
					$title = "Korea used cars - Minhas Trading";
				}
			}else{
				$title  = "Cars - Minhas Trading";
			}
		}elseif(strpos($titlepage,'cardetail') !== false){
			$seg = $this->uri->segment(2);
			if(isset($_GET["idx"])){
				$id = $_GET["idx"]; 
				$cardetail = $this->mo_bbs->getCarSpecification($id);
				$title = $cardetail[0]->car_make.' '.$cardetail[0]->car_model." - Minhas Trading";	
			}elseif(!empty($seg)){
				$id = $this->uri->segment(2);
				$cardetail = $this->mo_bbs->getCarSpecification($id);
				$title = $cardetail[0]->car_make.' '.$cardetail[0]->car_model." - Minhas Trading";
			}else{
				$title = "Car detail - Minhas Trading";
			}
		}elseif(strpos($titlepage,'sellerdetail') !== false){
			$seg = $this->uri->segment(2);
			if(isset($_GET["id"])){
				$id = $_GET["id"]; 
				$sellerdetail = $this->mo_bbs->getSellerSpecification($id);
				$title = $sellerdetail[0]->company_name." - Minhas Trading";
			}elseif(!empty($seg)){
				$id = $this->uri->segment(2);
				$sellerdetail = $this->mo_bbs->getSellerSpecification($id);
				$title = $sellerdetail[0]->company_name." - Minhas Trading";
			}
			else{
				$title = "Seller detail - Minhas Trading";
			}
		}elseif(strpos($titlepage,'public_seller') !== false){
			$title = "Sellers - Minhas Trading";
		}elseif(strpos($titlepage,'how_to_buy') !== false){
			$title = "How To Buy - Minhas Trading";
		}elseif(strpos($titlepage,'about_us') !== false){
			$title = "About us - Minhas Trading";
		}elseif(strpos($titlepage,'public_register') !== false){
			$title = "Register - Minhas Trading";
		}elseif(strpos($titlepage,'public_login') !== false){
			$title = "Login - Minhas Trading";
		}elseif(strpos($titlepage,'logout') !== false){
			$title = "Logout - Minhas Trading";
		}else{
            $title ='Japanese and Korean Used Car For Sale | Minhas Trading';
            // $title ='Korea , Japan used cars - motorbb.com';
        }
        /*** GET GOOGLE DATA ***/
        $visitor_count = $this->cache->get("ga_visitor");
        if($visitor_count== null){
            include './libraries/google-api-php-client/visitor.php';
            $analytics = getService();
            $profile = getFirstProfileId($analytics);
            $results = getResults($analytics, $profile);
            $results=$results->rows;
            $this->cache->set('ga_visitor', $results[0][0], 86400); //Cache for 24hours
            $visitor_count = $this->cache->get("ga_visitor");
        }
        // $visitor_count = 20;
        func_set_data($this, 'ga_visitor', $visitor_count);
        /*** END GET GOOGLE DATA ***/
        
		func_set_data($this, 'title_head',$title);
		$total_bbs_count = $this->mo_bbs->user_total_select();
        $total_statistics_cnt = $this->statistics->select_total_cnt();
        $total_statistics = $total_statistics_cnt[0]->total_cnt;
        func_set_data($this, 'total_bbs_count', $total_bbs_count);
        func_set_data($this, 'total_statistics', $total_statistics);      //총 접속수(메인)
        func_set_data($this, 'arr_ex_css', $arr_ex_css);
        func_set_data($this, 'arr_ex_js', $arr_ex_js);
        func_set_data($this, 'page_content', $this->load->view($view_path, $this->data, true));
        $this->load->view('public_site/layout/template.php', $this->data, false);
    }

    function _load_public_mobile_view($view_path, $arr_ex_js=array(), $arr_ex_css=array()){
        
        
        $total_bbs_count = $this->mo_bbs->user_total_select();
        $total_statistics_cnt = $this->statistics->select_total_cnt();
        $total_statistics = $total_statistics_cnt[0]->total_cnt;
        func_set_data($this, 'total_bbs_count', $total_bbs_count);
        func_set_data($this, 'total_statistics', $total_statistics); 
        func_set_data($this, 'arr_ex_css', $arr_ex_css);
        func_set_data($this, 'arr_ex_js', $arr_ex_js);


        //========= make active menu =========//
        $action_menu = "";
        if(isset($_GET["m"])){

            $action_menu = strtolower($_GET["m"]);

        }else{
            $action_menu = "mobile_home";                
        }

        func_set_data($this, 'action_menu',$action_menu);

        //========== Get Country to search box ============//
        // $carcountrytodropdownsearch = $this->mo_bbs->get_car_country();
        // func_set_data($this, 'carcountrytodropdownsearch',$carcountrytodropdownsearch);

        // //========== Get Make to search box ============//
        // $carmaketodropdownsearch = $this->mo_bbs->carmaketodropdownsearch();
        // func_set_data($this, 'carmaketodropdownsearch',$carmaketodropdownsearch);

        // //========== Get Model to search box ============//
        // $carmodeltodropdownsearch = $this->mo_bbs->carmodeltodropdownsearch();
        // func_set_data($this, 'carmodeltodropdownsearch',$carmodeltodropdownsearch);

        func_set_data($this, 'page_content', $this->load->view($view_path, $this->data, true));
        $this->load->view('public_mobile_site/layout/template.php', $this->data, false);
    }



    function public_home(){

        //============= Set PC Version ==============//
            $mobileversion = "/?c=user&m=mobile_home";
            func_set_data($this, 'mobileversion', $mobileversion);
        //============= Set PC Version ==============//

        // $carmaketodropdownsearch = $this->mo_bbs->carmaketodropdownsearch();

        // func_set_data($this, 'carmaketodropdownsearch',$carmaketodropdownsearch);


        // $carmodeltodropdownsearch = $this->mo_bbs->carmodeltodropdownsearch();

        // func_set_data($this, 'carmodeltodropdownsearch',$carmodeltodropdownsearch);

        // $mincaryeartodropdownsearch = $this->mo_bbs->mincaryeartodropdownsearch();
        // func_set_data($this, 'mincaryeartodropdownsearch',$mincaryeartodropdownsearch);


        // $mincarpricetodropdownsearch = $this->mo_bbs->mincarpricetodropdownsearch();
        // func_set_data($this, 'mincarpricetodropdownsearch',$mincarpricetodropdownsearch);



        // $maxcarpricetodropdownsearch = $this->mo_bbs->maxcarpricetodropdownsearch();
        // func_set_data($this, 'maxcarpricetodropdownsearch',$maxcarpricetodropdownsearch);


        // $carcountrytodropdownsearch = $this->mo_bbs->get_car_country();
        // func_set_data($this, 'carcountrytodropdownsearch',$carcountrytodropdownsearch);


		 $get_cars = $this->mo_bbs->get_car_home_page(25);
		$get_share = $get_cars;
        //echo $this->db->last_query();
         //var_dump($get_cars);
        func_set_data($this, 'get_cars',$get_cars);
		func_set_data($this, 'get_share',$get_share);
        $premium_cars= $this->mo_bbs->get_public_premium_car(array(), 5);
        func_set_data($this, 'premium_cars',$premium_cars);
        $arr_js = array('js/public_site/home_slide/wowslider.js?v='.$this->upload_version,'js/public_site/jquery.chained.min.js', 'js/public_site/home_slide/script.js?v='.$this->upload_version,'js/public_site/home.js?v='.$this->upload_version);
        $arr_css = array('css/public_site/home_slide.css?v='.$this->upload_version);
        $this->_load_public_view('public_site/home.php', $arr_js, $arr_css);
    }
	
	function cardetail(){
						
		
		$customer_services = array();
        $frieght_cost=0;
        $inspection_cost=0;
        $total_price=0;
		if(isset($_GET["idx"])){

    		$id = $_GET["idx"];

            //============= Set PC Version ==============//
            $mobileversion = "/?c=user&m=mobile_cardetail&idx=".$id;
            func_set_data($this, 'mobileversion', $mobileversion);
            //============= Set PC Version ==============//

            $newdata = array(
                    'sesfunc'  => 'showdetailcar',
                    'idx'     => $id,
            );

            $this->session->set_userdata('sessionkeepdata_pagesearchajax',$newdata);
    		$related_post = $this->mo_bbs->related_post($id, 6);

            $total_price = '-';

            if(isset($_POST['submit_calculate_price'])){   
                $calculate_price = $this->input->get_post('submit_calculate_price');
                $country_to = $this->input->get_post('destination_country');
                $port_name  = $this->input->get_post('port_name');
                $insurance  = $this->input->get_post('insurance');
                $inspection = $this->input->get_post('inspection');

                if($country_to!='' && $port_name!=''){ 

                    if($insurance!=true){
                        $insurance=false;
                    }
                    if($inspection!=true){
                        $inspection=false;
                    }

                    $data = array(
                            'calculate_price' => $calculate_price,
                            'country_to'  => $country_to,
                            'port_name'     => $port_name,
                            'insurance' => $insurance,
                            'inspection' => $inspection
                        );
                    $this->session->set_userdata($data);

                    $where['iw_shipping_charge.country_to']=$country_to;
                    $where['iw_shipping_charge.port_name'] =$port_name;
                }
                if(isset($_POST['reset'])){
                    $calculate_price = '';
                    $country_to = '';
                    $port_name  = '';
                    $insurance  = $this->input->get_post('insurance');
                    $inspection = $this->input->get_post('inspection');
                    if($insurance!=true){
                        $insurance=false;
                    }
                    if($inspection!=true){
                        $inspection=false;
                    }
                    $data = array(
                           'calculate_price' => $calculate_price,
                           'country_to'  => $country_to,
                           'port_name'     => $port_name,
                           'insurance' => $insurance,
                           'inspection' => $inspection
                       );
                    $this->session->set_userdata($data);
                }

                $calculate_price = $this->session->userdata('calculate_price');
                $get_country_to = $this->session->userdata('country_to');
                $get_port_name = $this->session->userdata('port_name');
                $get_insurance = $this->session->userdata('insurance');
                $get_inspection = $this->session->userdata('inspection');

                func_set_data($this, 'calculate_price', $calculate_price);
                func_set_data($this, 'country_to', $get_country_to);
                func_set_data($this, 'port_name', $get_port_name); 
                func_set_data($this, 'insurance', $get_insurance);
                func_set_data($this, 'inspection', $get_inspection); 
                        
            }
            if($this->session->userdata('country_to')!=''){

                $calculate_price = $this->session->userdata('calculate_price');
                $get_country_to = $this->session->userdata('country_to');
                $get_port_name = $this->session->userdata('port_name');
                $get_insurance = $this->session->userdata('insurance');
                $get_inspection = $this->session->userdata('inspection');

                $where['iw_shipping_charge.country_to']= $get_country_to;
                $where['iw_shipping_charge.port_name'] = $get_port_name;

                $where_country['country_to'] = $get_country_to;
                $list_port_name = $this->mo_bbs->select_port_name($where_country);

                func_set_data($this, 'list_port_name', $list_port_name);
                func_set_data($this, 'calculate_price', $calculate_price);
                func_set_data($this, 'country_to', $get_country_to);
                func_set_data($this, 'port_name', $get_port_name); 
                func_set_data($this, 'insurance', $get_insurance);
                func_set_data($this, 'inspection', $get_inspection); 
                $where['idx'] = $id;
                foreach($where as $key=>$value){
                    if(empty($value)){
                        unset($where[$key]);
                    }
                }
            }
            $where = array();
            
			
			$cardetail = $this->mo_bbs->getCarSpecification($id,$where);
       
            if(isset($calculate_price) && $calculate_price!=''){  


                $total_price = $this->mo_shipping->get_total_price($get_port_name,$cardetail[0]->idx,$get_inspection,$get_insurance);
                    
                if($total_price>0){

                    $total_price = $cardetail[0]->car_fob_currency." ".number_format($total_price);
                    
                }else{

                    $total_price = "ASK";

                }

            }
			func_set_data($this, 'frieght_cost',$frieght_cost);
			func_set_data($this, 'inspection_cost',$inspection_cost);
            func_set_data($this, 'total_price',$total_price);

            $list_country=  $this->mo_bbs->get_country_list();
            $getprimaryImage = $this->mo_bbs->getCarDetailPrimaryImage($id);
			$getDetailImage = $this->mo_bbs->getCarDetailImage($id);
            
            $list_destination = $this->mo_bbs->select_destination();

            $list_port = $this->mo_bbs->select_port_name();

			$member_profile_select = $this->member->member_profile_select($id);
            /*** GET CUSTOMER SUPPORT (SALES TEAM) ***/
            $where['idx'] = $id;
            $members = $this->mo_bbs->select_customer_service($where);
            if(count($members)>0){
                $customer_services[] =  $members[0];
            }else{
                $customer_services = $this->mo_bbs->get_sale_support_default();
            }
            
            $country=$this->mo_bbs->getCountry();
			func_set_data($this, 'member_profile_select',$member_profile_select);
			func_set_data($this, 'cardetail',$cardetail);
			func_set_data($this, 'related_post',$related_post);
			
            func_set_data($this, 'list_country',$list_country);
            func_set_data($this, 'list_destination',$list_destination);

            func_set_data($this, 'list_port',$list_port);

			func_set_data($this, 'getDetailImage',$getDetailImage);
            func_set_data($this, 'getPrimaryImage',$getprimaryImage);
            func_set_data($this, 'customer_service',$customer_services);
            func_set_data($this, 'country_list',$country);
			func_set_data($this, 'hide_sidebar', true);

            //$arr_js = array('/js/tooltipster/jquery.tooltipster.min.js', '/js/public_site/contact_us.js');
            //$arr_css = array('/css/public_site/about_us.css?v='.$this->upload_version,'/js/tooltipster/tooltipster.css');

			$arr_js = array('/js/tooltipster/jquery.tooltipster.min.js', 
                'js/public_site/jquery.chained.min.js',
                '/js/jquery.mousewheel.pack.js',
                '/js/fancybox/jquery.fancybox.pack.js',
                '/js/fancybox/helpers/jquery.fancybox-buttons.js',
                '/js/fancybox/helpers/jquery.fancybox-thumbs.js',
                '/js/public_site/car_detail.js?v='.$this->upload_version,
                '/js/public_site/lightbox.js');
						
			$arr_css = array('/js/fancybox/jquery.fancybox.css', 
                '/js/fancybox/jquery.fancybox.css', 
                '/js/fancybox/helpers/jquery.fancybox-thumbs.css',
                '/css/public_site/car_detail.css?v='.$this->upload_version,
                '/js/tooltipster/tooltipster.css');
			$this->_load_public_view('public_site/car_detail.php', $arr_js, $arr_css);
		}
    }


    function download_image(){

        $getfile = array();
        $i=0;
        $destination= time().'.zip';
        $filelimit = 255;
        if(isset($_POST['download_image'])){
            // if ($_SERVER['SERVER_NAME'] == 'motorbb.dev' || $_SERVER['SERVER_NAME'] == 'new.motorbb.com') {
            //     $host="ratha-it";
            // }
            // else{
            //     $host="softbloom";
            // }
            // $file_folder ='https://res.cloudinary.com/'.$host.'/image/upload/';
            
            $file_folder ='/uploads/cars/';

            ini_set('max_execution_time', 10000);
            ini_set("memory_limit", "6400M");
            $zip = new ZipArchive();    
            $zip->open($destination, ZIPARCHIVE::CREATE);
            $getfile = $_POST["files"];
            $countfile = count($_POST['files']);
            $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';

            foreach($getfile as $file){

                $full_url = $protocol.$_SERVER['SERVER_NAME'].$file_folder.$file;

                if(copy($full_url, "uploads/".basename($full_url))){   
                    if(file_exists("uploads/".basename($full_url))){
                        
                                $zip->addFile("uploads/".basename($full_url));
                        
                    }
                }

                
                
            }

      
            $zip->close();


           if(file_exists($destination)){
                
                ob_clean();
                ob_end_flush();
                header('Content-type: application/zip');
                header('Content-Disposition: attachment; filename="'.$destination.'"');
                readfile($destination);
                // remove zip file is exists in temp path
                unlink($destination);

                foreach (new DirectoryIterator('uploads/') as $fileInfo) {
                    if(!$fileInfo->isDot()) {
                        unlink($fileInfo->getPathname());

                        $myfile = fopen("uploads/newfile.txt", "w") or die("Unable to open file!");
                        $txt = "Hello World\n";
                        fwrite($myfile, $txt);
                        fclose($myfile);

                    }
                }

                
            }


        }



    }

	
	function sellerdetail(){
			
			
			if(isset($_GET["id"])){
				
			$id = $_GET["id"];

            //============= Set mobile Version ==============//
                $mobileversion = $this->replace_device("sellerdetail","mobile_seller_detail");
                func_set_data($this, 'mobileversion', $mobileversion);
            //============= Set mobile Version ==============//

		 	$where['car_owner'] =  $id;

		 	$per_page = 20;

			if(isset($_GET["page"])){

			if ($_GET["page"] != 0) {

				$page=$_GET["page"];
			}else{
				$page=1;
			}

			}else{
				$page=1;
			}

			$start_page=($page-1)*$per_page;


			/*** get total of rows for calculate pagination ***/
			$total_num_row = $this->mo_bbs->user_total_select($where);
			/*** Get car rows ... LIMIT 0, 20 ***/
			$result = $this->mo_bbs->user_select($where, '', $start_page, $per_page);
			/*** Test Output ***/
			//echo "Total Row: ".$total_num_row;
			$sellerdetail = $this->mo_bbs->getSellerSpecification($id);
			$member_profile_detail = $this->member->member_profile_detail($id);
			func_set_data($this, 'member_profile_detail',$member_profile_detail);
			func_set_data($this, 'carlist', $result);
			func_set_data($this, 'countcarlist', $total_num_row);
			func_set_data($this, 'perpagepagination', $per_page);
			func_set_data($this, 'getnumpage', $page);
			func_set_data($this, 'sellerdetail',$sellerdetail);
			$arr_js = array();
			$arr_js = array('/js/public_site/seller_detail.js?v='.$this->upload_version);
			$arr_css = array('/css/public_site/seller_detail.css?v='.$this->upload_version);
        	$this->_load_public_view('public_site/seller_detail.php', $arr_js, $arr_css);
			}
    }
	
	function public_login(){

        if ($this->data['is_login'] == 'Y')
            redirect('/?c=admin', 'refresh'); //로그인 되어 있으d면..
        func_set_data($this, 'return_url', $this->input->get_post('return_url')); //이전 URL

        if($this->input->post('submit')){
            /*** user pressed button login ***/
            $this->_login_exec();
        }
        //============= Set PC Version ==============//
            $mobileversion = $this->replace_device("public_login","mobile_login");
            func_set_data($this, 'mobileversion', $mobileversion);
        //============= Set PC Version ==============//   
		func_set_data($this, 'hide_sidebar', true);
		$arr_js = array();
		$arr_css = array('/css/public_site/login.css');

        $error_type = $this->session->flashdata('error_type');
        func_set_data($this, 'error_type', $error_type);
        $this->_load_public_view('public_site/login.php', $arr_js, $arr_css);
    }

    function forget_password(){
        func_set_data($this, 'hide_sidebar', true);
        $arr_js = array('/js/public_site/reset_password.js?v='.$this->upload_version);
        $arr_css = array('/css/public_site/forget_password.css?v='.$this->upload_version);
        $this->_load_public_view('public_site/forget_password.php', $arr_js, $arr_css);
    }
	
	function public_register(){
        //============= Set PC Version ==============//
            $mobileversion = $this->replace_device("public_register","mobile_register");
            func_set_data($this, 'mobileversion', $mobileversion);
        //============= Set PC Version ==============//   
		func_set_data($this, 'hide_sidebar', true);
		$arr_js = array('/js/public_site/register.js?v='.$this->upload_version);
		$arr_css = array('/css/public_site/register.css?v='.$this->upload_version);
        $this->_load_public_view('public_site/register.php', $arr_js, $arr_css);
    }
	

	
	function complete(){
		func_set_data($this, 'hide_sidebar', true);
		$arr_js = array();
		$arr_css = array('/css/public_site/complete.css?v='.$this->upload_version);
        $this->_load_public_view('public_site/complete.php', $arr_js, $arr_css);
    }
	
    
	
	function enter_info(){
		func_set_data($this, 'hide_sidebar', true);
		$arr_js = array();
        $arr_js = array('/js/public_site/intlTelInput.js','/js/public_site/public_enter_info.js?v='.$this->upload_version);
        $arr_css[] = '/build/css/intlTelInput_enter_info.css?v='.$this->upload_version;
		$arr_css[] = '/css/public_site/enter_info.css?v='.$this->upload_version;
        if (isset($_GET["vcode"])){
            $get_vcode=$_GET["vcode"];
            $vcode= $this->member->existVerificationCode($get_vcode);
            if ($vcode==true){
                $member_info=$this->member->select_VerificationCode($get_vcode);
                $get_info = $this->session->all_userdata();
                $member_id="";
                $member_pwd ="";
                $member_first_name="";
                $member_last_name="";
                $member_country="";
                $phone_no1="";
                if(isset($get_info["member_id"]) && isset($get_info["member_pwd"]) && isset($get_info["member_first_name"]) && isset($get_info["member_last_name"]) && isset($get_info["member_country"]) && isset($get_info["phone_no1"])){
                    $member_id = $get_info["member_id"];
                    $member_pwd = $get_info["member_pwd"];
                    $member_first_name = $get_info["member_first_name"];
                    $member_last_name = $get_info["member_last_name"];
                    $member_country = $get_info["member_country"];
                    $phone_no1 = $get_info["phone_no1"];

                }
                $email_member=$member_info[0]->email;
                $vcode_member=$member_info[0]->verification;
     
                func_set_data($this, 'member_id', $member_id);
                func_set_data($this, 'member_pwd', $member_pwd);
                func_set_data($this, 'member_first_name', $member_first_name);
                func_set_data($this, 'member_last_name', $member_last_name);
                func_set_data($this, 'phone_no1', $phone_no1);
                func_set_data($this, 'email_member', $email_member);
                func_set_data($this, 'vcode_member', $vcode_member);

            $this->_load_public_view('public_site/enter_info.php', $arr_js, $arr_css);
         }
      }
    }

    function enter_info_confirm(){
        func_set_data($this, 'hide_sidebar', true);
        $arr_js = array();
        $arr_css= array();
        $arr_js = array('/intlTelInput/build/js/intlTelInput.js','/js/public_site/public_enter_info.js?v='.$this->upload_version,'/js/public_site/member_info.js?v='.$this->upload_version,'js/jquery.ajaxQueue.min.js');
        $arr_css = array('/intlTelInput/build/css/intlTelInput.css','/css/public_site/enter_info.css?v='.$this->upload_version);
        if (isset($_GET["vcode"])){
            $get_vcode=$_GET["vcode"];
            $vcode= $this->member->existVerificationCode($get_vcode);
            if ($vcode==true){
                $member_info=$this->member->select_VerificationCode($get_vcode);
                $member_id="";
                $member_pwd ="";
                $member_first_name="";
                $member_last_name="";
                $member_country="";
                $phone_no1="";
                $email_member=$member_info[0]->email;
                $vcode_member=$member_info[0]->verification;
                func_set_data($this, 'email_member', $email_member);
                func_set_data($this, 'vcode_member', $vcode_member);

            $this->_load_public_view('public_site/enter_info_confirm.php', $arr_js, $arr_css);
         }
      }
    }



     function reset_password(){
            $resetpass = $this->input->get_post('vcode');
            if($this->member->existVerifyPassword($resetpass)){
            func_set_data($this, 'hide_sidebar', true);
            $arr_js = array();
            $arr_css= array();
            $arr_js = array('/js/public_site/reset_password.js?v='.$this->upload_version);
            $arr_css = array('/css/public_site/reset_password.css?v='.$this->upload_version);
            if(isset($_GET['vcode'])){
                $get_vcode=$_GET['vcode'];
                func_set_data($this, 'vcode_member', $get_vcode);
            }
            $this->_load_public_view('public_site/reset_password.php', $arr_js, $arr_css);
         }else {
            echo func_jsAlertReplace('Verification code is invalid. Account verification failed.', './');
        }
    }

    function reset_success(){
        func_set_data($this, 'hide_sidebar', true);
        $arr_js = array();
        $arr_css= array();
        $get_email=$this->session->userdata("s_email");
        func_set_data($this, 'get_email', $get_email);
        $arr_css = array('/css/public_site/reset_success.css?v='.$this->upload_version);
        $this->_load_public_view('public_site/reset_success.php', $arr_js, $arr_css);

    }

     function reset_success_change(){
        func_set_data($this, 'hide_sidebar', true);
        $arr_js = array();
        $arr_css= array();
        $arr_css = array('/css/public_site/reset_success_change.css?v='.$this->upload_version); 
        $this->_load_public_view('public_site/reset_success_change.php', $arr_js, $arr_css);
    }


    function mobile_reset_password(){
         $resetpass = $this->input->get_post('vcode');
         if($this->member->existVerifyPassword($resetpass)){
            $arr_js = array();
            $arr_css= array();
            if(isset($_GET['vcode'])){
                $get_vcode=$_GET['vcode'];
                func_set_data($this, 'vcode_member', $get_vcode);
            }
            $arr_js = array('/js/public_mobile_site/mobile_reset_password.js?v='.$this->upload_version);
            $arr_css = array('/css/public_mobile_site/mobile_change_pwd.css?v='.$this->upload_version);
            func_set_data($this, 'arr_js',$arr_js);
            func_set_data($this, 'arr_css',$arr_css);
            $this->_load_public_mobile_view('public_mobile_site/mobile_change_pwd');
         }else {
            echo func_jsAlertReplace('Verification code is invalid. Account verification failed.', './');
        }
    }

    function mobile_reset_success(){
        func_set_data($this, 'hide_sidebar', true);
        $arr_js = array();
        $arr_css= array();
        $get_email=$this->session->userdata("s_email");
        func_set_data($this, 'get_email', $get_email);
        $arr_css = array('/css/public_mobile_site/success_reset.css?v='.$this->upload_version);
        func_set_data($this, 'arr_css',$arr_css); 
        $this->_load_public_mobile_view('public_mobile_site/success_reset');

    }

     function mobile_reset_success_change(){
        func_set_data($this, 'hide_sidebar', true);
        $arr_css = array('/css/public_mobile_site/success_password.css?v='.$this->upload_version);
        func_set_data($this, 'arr_css',$arr_css);
        $this->_load_public_mobile_view('public_mobile_site/success_password');
    }



    function car_grid(){
        
        $arr_css = array('/css/public_site/car_list.css?v='.$this->upload_version);
        $this->_load_public_view('public_site/car_grid.php', $arr_js, $arr_css);
    }

    function replace_device($valfine,$valreplace){
        $actual_link = "$_SERVER[REQUEST_URI]";
        return str_replace($valfine,$valreplace,$actual_link);
    }

    function car_list(){

        //============= Set PC Version ==============//
            $mobileversion = $this->replace_device("car_list","mobile_car_list");
            func_set_data($this, 'mobileversion', $mobileversion);
        //============= Set PC Version ==============//

        $arr_js = array('/js/public_site/jquery.chained.min.js','/js/tooltipster/jquery.tooltipster.min.js','/js/public_site/car_list.js?v='.$this->upload_version);

        $arr_css = array('/js/tooltipster/tooltipster.css','/css/public_site/car_list.css?v='.$this->upload_version);

        $where_raw = '';
        $add_arg_mobileversion = "";

        //====================== GET CAR STOCK LIST OR GRID =============//

            $action_stock_car_grid = "";
            $action_stock_car_list = "";

            if(isset($_GET["clist"])){

                if($_GET["clist"] == "cargrid"){
                    
                    $action_stock_car_grid = "block";
                    $action_stock_car_list = "none";

                }else{

                    $action_stock_car_grid = "none";
                    $action_stock_car_list = "block";
                }

            }else{
                $action_stock_car_grid = "block";
                $action_stock_car_list = "none";
            }

            //=============== List Destination Port ============//
            $list_destination = $this->mo_bbs->select_destination();

            //=============== List port country ================//
            $list_port = $this->mo_bbs->select_port_name();


            func_set_data($this, 'action_stock_car_grid', $action_stock_car_grid);
            func_set_data($this, 'action_stock_car_list', $action_stock_car_list);

        //====================== GET CAR STOCK LIST OR GRID =============//


        //===================== GET CAR STOCK COUNTRY ==============//

            $active_stockcountry_jp = ""; 
            $active_stockcountry_kr = ""; 
            $active_stockcountry_th = ""; 
            $active_stockcountry_hk = ""; 

            if(isset($_GET["country"])){

                if($_GET["country"] == "jp"){
                    $all_stock = "";
                    $active_stockcountry_jp = "active_stock";
                    $active_stockcountry_kr = "";
                    $active_stockcountry_th = "";
                    $active_stockcountry_hk = ""; 
                }else if($_GET["country"] == "kr"){
                    $all_stock = "";
                    $active_stockcountry_kr = "active_stock";
                    $active_stockcountry_jp = "";
                    $active_stockcountry_th = "";
                    $active_stockcountry_hk = ""; 
                }else if($_GET["country"] == "th"){
                    $all_stock = "";
                    $active_stockcountry_kr = "";
                    $active_stockcountry_jp = "";
                    $active_stockcountry_th = "active_stock";
                    $active_stockcountry_hk = ""; 
                    }else if($_GET["country"] == "hk"){
                    $all_stock = "";
                    $active_stockcountry_kr = "";
                    $active_stockcountry_jp = "";
                    $active_stockcountry_th = "";
                    $active_stockcountry_hk = "active_stock"; 
                }else{
                    $all_stock = "active_stock";
                    $active_stockcountry_kr = "";
                    $active_stockcountry_jp = "";   
                    $active_stockcountry_th = ""; 
                    $active_stockcountry_hk = ""; 
                }

            }else{
                $all_stock = "active_stock";
                $active_stockcountry_kr = "";
                $active_stockcountry_jp = "";
                $active_stockcountry_th = ""; 
                $active_stockcountry_hk = ""; 
            }

             func_set_data($this, 'all_stock', $all_stock);
             func_set_data($this, 'active_stockcountry_kr', $active_stockcountry_kr);
             func_set_data($this, 'active_stockcountry_jp', $active_stockcountry_jp);
             func_set_data($this, 'active_stockcountry_th', $active_stockcountry_th);
             func_set_data($this, 'active_stockcountry_hk', $active_stockcountry_hk);

        //===================== GET CAR STOCK COUNTRY ==============//




         //====================== Pagination ==============//
            $per_page = 20;

            if(isset($_GET["page"])){

                if ($_GET["page"] != 0) {

                    $page=$_GET["page"];
                }else{
                    $page=1;
                }

            }else{
                $page=1;
            }


            $start_page=($page-1)*$per_page;
            $model_list = array();
        //====================== Pagination ==============//




        
            $where['country'] = $this->input->get_post('country');
            $where['car_make'] = $this->input->get_post('car_make');
            $where['car_model'] = $this->input->get_post('car_model');
            $where['car_model_year'] = $this->input->get_post('car_model_year');
            $where['car_body_type'] = $this->input->get_post('car_body_type');
            $where['car_drive_type'] = $this->input->get_post('car_drive_type');
            $where['car_model_year >='] = $this->input->get_post('min_model_year');
            $where['car_model_year <='] = $this->input->get_post('max_model_year');
            $where['car_fob_cost >='] = $this->input->get_post('min_fob_cost');
            $where['car_fob_cost <='] = $this->input->get_post('max_fob_cost');
            //var_dump($where);
            //$where['car_chassis_no LIKE'] = '%'.$this->input->get_post('keyword').'%';
            
            $keyword = $this->input->get_post('keyword');
            $where_disc = "";
            $discount = $this->input->get_post('disc');
            if($discount == "disc"){
                $where_raw .= "old_fob_cost > car_fob_cost";
            }
            $where['keyword'] = $keyword;

        if(isset($_POST['submit_calculate_price'])){
            
            $calculate_price = $this->input->get_post('submit_calculate_price');
            $country_to = $this->input->get_post('destination_country');
            $port_name  = $this->input->get_post('port_name');
            $insurance  = $this->input->get_post('insurance');
            $inspection = $this->input->get_post('inspection');
            if($country_to!='' && $port_name!=''){ 
                if($insurance!="true"){
                    $insurance=false;
                }
                if($inspection!="true"){
                    $inspection=false;
                }

                $data = array(
                       'calculate_price' => $calculate_price,
                       'country_to'  => $country_to,
                       'port_name'     => $port_name,
                       'insurance' => $insurance,
                       'inspection' => $inspection
                   );
                $this->session->set_userdata($data);
            }else{
                $calculate_price = '';
                $country_to = '';
                $port_name  = '';
                $insurance  = $this->input->get_post('insurance');
                $inspection = $this->input->get_post('inspection');
                if($insurance!=true){
                    $insurance=false;
                }
                if($inspection!=true){
                    $inspection=false;
                }
                $data = array(
                       'calculate_price' => $calculate_price,
                       'country_to'  => $country_to,
                       'port_name'     => $port_name,
                       'insurance' => $insurance,
                       'inspection' => $inspection
                   );
                $this->session->set_userdata($data);
            }

            $calculate_price = $this->session->userdata('calculate_price');
            $get_country_to = $this->session->userdata('country_to');
            $get_port_name = $this->session->userdata('port_name');
            $get_insurance = $this->session->userdata('insurance');
            $get_inspection = $this->session->userdata('inspection');

            func_set_data($this, 'calculate_price', $calculate_price);
            func_set_data($this, 'country_to', $get_country_to);
            func_set_data($this, 'port_name', $get_port_name); 
            func_set_data($this, 'insurance', $get_insurance);
            func_set_data($this, 'inspection', $get_inspection);     
        }
        if(isset($_POST['submit_reset_price'])){
                $calculate_price = '';
                $country_to = '';
                $port_name  = '';
                $insurance  = $this->input->get_post('insurance');
                $inspection = $this->input->get_post('inspection');
                if($insurance!=true){
                    $insurance=false;
                }
                if($inspection!=true){
                    $inspection=false;
                }
                $data = array(
                       'calculate_price' => $calculate_price,
                       'country_to'  => $country_to,
                       'port_name'     => $port_name,
                       'insurance' => $insurance,
                       'inspection' => $inspection
                   );
                $this->session->set_userdata($data);
                $calculate_price = $this->session->userdata('calculate_price');
                $get_country_to = $this->session->userdata('country_to');
                $get_port_name = $this->session->userdata('port_name');
                $get_insurance = $this->session->userdata('insurance');
                $get_inspection = $this->session->userdata('inspection');

                func_set_data($this, 'calculate_price', $calculate_price);
                func_set_data($this, 'country_to', $get_country_to);
                func_set_data($this, 'port_name', $get_port_name); 
                func_set_data($this, 'insurance', $get_insurance);
                func_set_data($this, 'inspection', $get_inspection);
            }

             


        if($this->session->userdata('country_to')!=''){

            $calculate_price = $this->session->userdata('calculate_price');
            $get_country_to = $this->session->userdata('country_to');
            $get_port_name = $this->session->userdata('port_name');
            $get_insurance = $this->session->userdata('insurance');
            $get_inspection = $this->session->userdata('inspection');

            $where_country['id'] = $get_country_to;
            // $list_port_name = $this->mo_bbs->select_port_name($where_country);

            // func_set_data($this, 'list_port_name', $list_port_name);
            func_set_data($this, 'calculate_price', $calculate_price);
            func_set_data($this, 'country_to', $get_country_to);
            func_set_data($this, 'port_name', $get_port_name); 
            func_set_data($this, 'insurance', $get_insurance);
            func_set_data($this, 'inspection', $get_inspection); 
        }
        /*** Check for searching fields that is empty means user didn't input it ***/
        foreach($where as $key=>$value){
            if(empty($value)){
                unset($where[$key]);
            }
        }
        
            $set_url_popular = "";
            if(isset($_GET["car"])){

                $result = $this->mo_bbs->get_public_premium_car($where,$per_page,$start_page);

                $total_num_row = $this->mo_bbs->get_total_premium_car($where=array());

                $set_url_popular = "&car=popular";

           

            }else{

                 /*** get total of rows for calculate pagination ***/
                // var_dump($where_disc);
                $total_num_row = $this->mo_bbs->user_total_select($where,$where_raw);
                /*** Get car rows ... LIMIT 0, 20 ***/
                $result = $this->mo_bbs->user_select($where,$where_raw, $start_page, $per_page);
                /*** Test Output ***/
                //echo "Total Row: ".$total_num_row;
         

            }

            if(isset($get_port_name) && $get_port_name!=''){
                $list_total_price = '';
                foreach ($result as $key => $value) {
                    $list_total_price[$value->idx] = $this->mo_shipping->get_total_price($get_port_name,$value->idx,$get_inspection,$get_insurance);
                }
                func_set_data($this, 'list_total_price',$list_total_price);
            }

         //=============== Select Sale Support Team ============//
        $country = (isset($_GET['country']) && $_GET['country'] !='') ? $_GET['country'] : '';
        $sale_support = $this->mo_bbs->get_sale_support_car_list($country);
        if(count($sale_support)==0){
             $sale_support = $this->mo_bbs->get_sale_support_default();
        }
        //=============== Set Destination Port ============//
        func_set_data($this, 'list_destination',$list_destination);

        //=============== Set Port =============//
        func_set_data($this, 'list_port',$list_port);

        //=============== Set Title to car list page ============//
        
        func_set_data($this, 'model_list', $model_list);
        //func_set_data($this, 'carlisttitle', $carlisttitle);
        //=============== Set Title to car list page ============//
        
        func_set_data($this, 'set_url_popular', $set_url_popular);

        func_set_data($this, 'sale_support', $sale_support); 
        func_set_data($this, 'carlist', $result); 
        func_set_data($this, 'countcarlist', $total_num_row);
        func_set_data($this, 'perpagepagination', $per_page);
        func_set_data($this, 'getnumpage', $page);

        func_set_data($this, 'frieght_cost_plus', $this->frieght_cost_plus);
        func_set_data($this, 'inspection_cost_plus', $this->inspection_cost_plus);
        func_set_data($this, 'insurance_cost_plus', $this->insurance_cost_plus);
       

        $this->_load_public_view('public_site/car_list.php', $arr_js, $arr_css);
    }


    function public_seller(){

        //============= Set mobile Version ==============//
            $mobileversion = $this->replace_device("public_seller","mobile_seller");
            func_set_data($this, 'mobileversion', $mobileversion);
        //============= Set mobile Version ==============//

        $seller_profile= 'seller';
        $country= $this->input->get_post('country');
        $keyword = $this->input->get_post('keyword');

        $queryString="";
        if (isset($_POST['btn_search'])){
            $country=$this->db->escape_str($_POST['country']);
            $keyword=$this->db->escape_str($_POST['keyword']);
            $datasall = array('country'=> $country,'keyword'=> $keyword);
            $queryString =  http_build_query($datasall);

            $link = "/?c=user&m=public_seller&".$queryString."&page=1";
            die("<script>location.href = '".$link. "';</script>");
       }

        if($country !=""){
            $where['member.member_country ='] = $country;
        }else{
             $where['member.business_type ='] = $seller_profile;
        }
        if($keyword !=""){
            $where['keyword_member_name'] = $keyword;
        }else{
            $where['member.business_type ='] = $seller_profile;
        }

        $per_page = 15;

        if(isset($_GET["page"])){

            if ($_GET["page"] != 0) {

                $page=$_GET["page"];
            }else{
                $page=1;
            }

        }else{
            $page=1;
        }
        $start_page=($page-1)*$per_page;

        // $offset = 0;   //You Skip first 20 rows
        // $num_rows = 10; //You take 10 row
        $sellers = $this->customer->customer_select($where, $start_page, $per_page);

        $total_num_row = $sellers->total_count;
        $rows = $sellers->result();


        // Get total num_rows for pagination
        $count_num_row = $sellers->total_count;
        func_set_data($this, 'hide_search_box', true);
        func_set_data($this, 'sellers',$rows);
        func_set_data($this, 'total_count',$count_num_row);
        func_set_data($this, 'perpagepagination', $per_page);
        func_set_data($this, 'getnumpage', $page);

        $country=$this->customer->country_seller();
        func_set_data($this,'country',$country);
        func_set_data($this, 'hide_sidebar', true);
        $arr_js = array();
        $arr_css = array('/css/public_site/seller.css?v='.$this->upload_version);
        $this->_load_public_view('public_site/seller.php', $arr_js, $arr_css);
    }


    function about_us(){
        $arr_js = array('/js/tooltipster/jquery.tooltipster.min.js', '/js/public_site/about_us.js');
        $arr_css = array('/css/public_site/about_us.css?v='.$this->upload_version,'/js/tooltipster/tooltipster.css');
        func_set_data($this, 'hide_sidebar', true);
        $jp='jp';
        $kr='kr';
        $slelect_team_sale_list=$this->member->slelect_team_sale($jp);
        $slelect_team_sale_list_kr=$this->member->slelect_team_sale($kr);
        func_set_data($this, 'slelect_team_sale_list', $slelect_team_sale_list);
        func_set_data($this, 'slelect_team_sale_list_kr', $slelect_team_sale_list_kr);
        //============= Set mobile Version ==============//
            $mobileversion = $this->replace_device("about_us","mobile_about_us");
            func_set_data($this, 'mobileversion', $mobileversion);
        //============= Set mobile Version ==============//
        $this->_load_public_view('public_site/about_us.php', $arr_js, $arr_css);
    }
    function contact_us(){
        $arr_js = array('/js/tooltipster/jquery.tooltipster.min.js', '/js/public_site/contact_us.js');
        $arr_css = array('/css/public_site/contact_us.css?v='.$this->upload_version,'/js/tooltipster/tooltipster.css');
        func_set_data($this, 'hide_sidebar', true);
        $jp='jp';
        $kr='kr';
        $slelect_team_sale_list=$this->member->slelect_team_sale($jp);
        $slelect_team_sale_list_kr=$this->member->slelect_team_sale($kr);
        func_set_data($this, 'slelect_team_sale_list', $slelect_team_sale_list);
        func_set_data($this, 'slelect_team_sale_list_kr', $slelect_team_sale_list_kr);
        //============= Set mobile Version ==============//
            $mobileversion = $this->replace_device("contact_us","mobile_contact_us");
            func_set_data($this, 'mobileversion', $mobileversion);
        //============= Set mobile Version ==============//
        $this->_load_public_view('public_site/contact_us.php', $arr_js, $arr_css);
    }
    function how_to_buy(){
        $arr_js = array('/js/public_site/test.js?v=1.0.7');
        $arr_css = array('/css/public_site/how_to_buy.css?v='.$this->upload_version);
        func_set_data($this, 'hide_sidebar', true);
        //============= Set mobile Version ==============//
            $mobileversion = $this->replace_device("how_to_buy","mobile_how_to_buy");
            func_set_data($this, 'mobileversion', $mobileversion);
        //============= Set mobile Version ==============//

        $this->_load_public_view('public_site/how_to_buy.php', $arr_js, $arr_css);
    }

    function our_customer(){
        $arr_js = array('/js/public_site/testimonialsus.js?v=1.0.9');
        $arr_css = array('/css/public_site/testimonialsus.css?v='.$this->upload_version);
        func_set_data($this, 'hide_sidebar', true);
        //============= Set mobile Version ==============//
        //============= Set mobile Version ==============//

        $this->_load_public_view('public_site/testimonialsus.php', $arr_js, $arr_css);
    }
    
    function site_map(){
        $arr_js = array('/js/public_site/test.js?v=1.0.7');
        $arr_css = array('/css/public_site/site_map.css?v='.$this->upload_version);
        func_set_data($this, 'hide_sidebar', true);
        //============= Set mobile Version ==============//
            $mobileversion = $this->replace_device("site_map","mobile_site_map");
            func_set_data($this, 'mobileversion', $mobileversion);
        //============= Set mobile Version ==============//

        $this->_load_public_view('public_site/site_map.php', $arr_js, $arr_css);
    }

    function siteURL() {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'];
        return $protocol . $domainName;
    }

    function register_form(){
        $is_exist = false;
        $is_verified = false;
        if (isset($_POST['btn_register'])){
            $current_url = $this->siteURL();
            $c_email=$this->input->get_post('email');
            $confirm_email=$this->input->get_post('confirm_email');
            //$html=file_get_contents('email_template/register.html', true);
            $data['email'] = $c_email;
            $data['grade_no'] = 10;
            /*** CHECK IF VERIFIED ***/
            $is_verified = $this->member->check_verify_by_email($c_email);
            /*** CHECK IF EXISTED ***/
            $result=$this->member->validate_email_Register($data);
            if($result['success']==0){
                $is_exist = true;
            }

            /*** CHECK IF ACCOUNT VERIFIED ***/
            if(!$is_verified){
                /*** CHECK IF ACCOUNT EXISTED  ***/
                if(!$is_exist){
                    $lastid = $this->member->insert_member($data);
                    $verificationCode = $this->member->createVerificationCode($lastid);
                    $this->member->insertVerification($lastid, $verificationCode);
                }else{
                    $user_detail = $this->member->getDetailByEmail($c_email);
                    $verificationCode = $user_detail->verification;
                }
                $this->mo_email->send_register($c_email, $verificationCode);
                $data = array('s_email' => $c_email );
                $set_email=$this->session->set_userdata($data);
                redirect('/?c=user&m=email_sent');
            }else{
                echo func_jsAlertReplace('Failed to register! Email already existed.', '/?c=user&m=public_register');
            }

            // if($result['success']==1){
            //    // echo $html;exit(); 
            // }else{
            //     echo func_jsAlertReplace('Email already exists!', 'refresh');
            // }
        }
         
    }
    function email_sent(){
        func_set_data($this, 'hide_sidebar', true);
        $arr_js = array();
        $arr_css = array('/css/public_site/email_sent.css?v='.$this->upload_version);
        $get_email=$this->session->userdata("s_email");
        func_set_data($this, 'get_email', $get_email);
        $this->_load_public_view('public_site/email_sent.php', $arr_js, $arr_css);
    }
	function mobile_email_sent(){
        $arr_js = array();
        $arr_css = array('/css/public_mobile_site/email_sent.css?v='.$this->upload_version);
		func_set_data($this, 'arr_css',$arr_css);
        $get_email=$this->session->userdata("s_email");
        func_set_data($this, 'get_email', $get_email);
        $this->_load_public_mobile_view('public_mobile_site/email_sent', $arr_js, $arr_css);
    }

    function _load_view_layout_ajax($view_path){

        //'X-Requested-With' => string 'XMLHttpRequest' (length=14)
        if(function_exists('getallheaders')){
            $headers = getallheaders();
            if(isset($headers['X-Requested-With'])) $request_header = $headers['X-Requested-With']; else $request_header = '';
        }else{
            $request_header = '';
        }
       //var_dump($view_path);
		//exit();

        $this->_visit_statistics();



        if(strtolower($request_header)=='xmlhttprequest'){

            $contents_area = $this->load->view($view_path, $this->data, true);
            func_set_data($this, 'content', $contents_area);
            $this->load->view('user/config/ajax', $this->data, false);
        }else{
            $this->_load_view_layout($view_path);
        }

    }

	function howtobuy(){
        func_set_data($this, 'hide_search_box', true);
        
        $this->_load_view_layout_ajax('user/howtobuy');
    }

	function aboutus(){
        func_set_data($this, 'hide_search_box', true);
		 $this->_load_view_layout_ajax('user/about-us');

	}
	function register(){
        func_set_data($this, 'hide_search_box', true);
        if ($this->is_login != 'Y'){
		      $this->_load_view_layout_ajax('user/register');
        }else{
            header("Location: /?c=user");
        }


	}
    function minify(){

        /** Minified CSS ***/
        foreach(glob('css/public_site/unmin/*.css') as $filename){
            $sourcePath = $filename;
            $minifier = new Minify\CSS($sourcePath);
            $minifier->minify('css/public_site/'.basename($filename));
        }
        /** Minified JS ***/
        foreach(glob('js/public_site/unmin/*.js') as $filename){
            $sourcePath = $filename;
            $minifier = new Minify\JS($sourcePath);
            $minifier->minify('js/public_site/'.basename($filename));
        }
    }
    function testpages(){

        
    }

    function mobile_home(){


        $arr_js = array('/js/public_mobile_site/home.js?v='.$this->upload_version);
        $arr_css = array('/css/public_mobile_site/home.css?v='.$this->upload_version);
        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);

        $show_search_box = true;
        func_set_data($this, 'show_search_box',$show_search_box);

        $get_cars = $this->mo_bbs->get_car_home_page(25);
        $get_share = $get_cars;
        $premium_cars= $this->mo_bbs->get_public_premium_car(array(), 5);


        func_set_data($this, 'premium_cars',$premium_cars);
        func_set_data($this, 'get_cars',$get_cars);
        func_set_data($this, 'get_share',$get_share);

        //============= Set PC Version ==============//
            $pcversion = "/?c=user&m=public_home";
            func_set_data($this, 'pcversion', $pcversion);
        //============= Set PC Version ==============//

        /*$param = "";
        $action = "";
        if((isset($_GET["m"]) and isset($_GET["car_make"])) or (isset($_GET["m"]) and isset($_GET["car_body_type"]))){

            $action = strtolower($_GET["m"]);
            if(isset($_GET["car_make"])){
                $param = strtoupper($_GET["car_make"]);
            }else{
                $param = strtoupper($_GET["car_body_type"]);   
            }   
            
        }else{
            $action = "public_home";  
            $param = "";              
        }


        func_set_data($this, 'action',$action);
        func_set_data($this, 'param',$param);*/


        //============ Get Brand and type ==========//    
        $get_brand = $this->mo_bbs->get_brand_list();
        func_set_data($this, 'get_brand',$get_brand);

        $getbody_type = $this->mo_bbs->get_bodytype_list();
        func_set_data($this, 'getbody_type',$getbody_type);

        $this->_load_public_mobile_view('public_mobile_site/home',$arr_js,$arr_css);
    }

   

    function mobile_enter_info(){
        func_set_data($this, 'hide_sidebar', true);
        $arr_js = array();
        $arr_css= array();
        $arr_js = array('/intlTelInput/build/js/intlTelInput.js','/js/public_mobile_site/mobile_enter_info.js?v='.$this->upload_version,'js/jquery.ajaxQueue.min.js');
        $arr_css = array('/css/public_mobile_site/enter_info.css?v='.$this->upload_version,'/intlTelInput/build/css/intlTelInput.css');
        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);
        if (isset($_GET["vcode"])){
            $get_vcode=$_GET["vcode"];
            $vcode= $this->member->existVerificationCode($get_vcode);
            if ($vcode==true){
                $member_info=$this->member->select_VerificationCode($get_vcode);
                $member_id="";
                $member_pwd ="";
                $member_first_name="";
                $member_last_name="";
                $member_country="";
                $phone_no1="";
                $email_member=$member_info[0]->email;
                $vcode_member=$member_info[0]->verification;
                func_set_data($this, 'email_member', $email_member);
                func_set_data($this, 'vcode_member', $vcode_member);

           $this->_load_public_mobile_view('public_mobile_site/mobile_enter_info');
         }
      }
    }

    function mobile_success_password(){
        $arr_css = array('/css/public_mobile_site/success_password.css?v='.$this->upload_version);
        func_set_data($this, 'arr_css',$arr_css); 
        $this->_load_public_mobile_view('public_mobile_site/success_password');
    }

    function mobile_complete(){

        $arr_css = array('/css/public_mobile_site/complete.css?v='.$this->upload_version);
        func_set_data($this, 'arr_css',$arr_css);

        $this->_load_public_mobile_view('public_mobile_site/complete');
    }

    function mobile_car_list(){


        $arr_js = array('/js/tooltipster/jquery.tooltipster.min.js', '/js/public_mobile_site/test.js?v=1.0.7','/js/public_mobile_site/car_list.js?v='.$this->upload_version);
        $arr_css = array('/js/tooltipster/tooltipster.css', '/css/public_mobile_site/car_list.css?v='.$this->upload_version);
        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);

        $show_search_box = true;
        func_set_data($this, 'show_search_box',$show_search_box);

        //============= Set PC Version ==============//
            $pcversion = $this->replace_device("mobile_car_list","car_list");
            func_set_data($this, 'pcversion', $pcversion);
        //============= Set PC Version ==============//   

        //===================== GET CAR STOCK COUNTRY ==============//

            $active_stockcountry_jp = ""; 
            $active_stockcountry_kr = ""; 
            $active_stockcountry_th = ""; 
            $active_stockcountry_hk = ""; 

            if(isset($_GET["country"])){

                if($_GET["country"] == "jp"){
                    $all_stock = "";
                    $active_stockcountry_jp = "active_stock";
                    $active_stockcountry_kr = "";
                    $active_stockcountry_th = ""; 
                    $active_stockcountry_hk = ""; 
                }else if($_GET["country"] == "kr"){
                    $all_stock = "";
                    $active_stockcountry_kr = "active_stock";
                    $active_stockcountry_jp = "";
                    $active_stockcountry_th = ""; 
                    $active_stockcountry_hk = ""; 
                }else if($_GET["country"] == "th"){
                    $all_stock = "";
                    $active_stockcountry_kr = "";
                    $active_stockcountry_jp = "";
                    $active_stockcountry_th = "active_stock"; 
                    $active_stockcountry_hk = ""; 
                }else if($_GET["country"] == "hk"){
                    $all_stock = "";
                    $active_stockcountry_kr = "";
                    $active_stockcountry_jp = "";
                    $active_stockcountry_th = ""; 
                    $active_stockcountry_hk = "active_stock"; 
                }else{
                    $all_stock = "active_stock";
                    $active_stockcountry_kr = "";
                    $active_stockcountry_jp = "";  
                    $active_stockcountry_th = "";   
                    $active_stockcountry_hk = ""; 
                }

            }else{
                $all_stock = "active_stock";
                $active_stockcountry_kr = "";
                $active_stockcountry_jp = "";
                $active_stockcountry_th = ""; 
                $active_stockcountry_hk = ""; 
            }

             func_set_data($this, 'all_stock', $all_stock);
             func_set_data($this, 'active_stockcountry_kr', $active_stockcountry_kr);
             func_set_data($this, 'active_stockcountry_jp', $active_stockcountry_jp);
             func_set_data($this, 'active_stockcountry_th', $active_stockcountry_th);
             func_set_data($this, 'active_stockcountry_hk', $active_stockcountry_hk);

        //===================== GET CAR STOCK COUNTRY ==============//


        //=============== List Destination Port ============//
            $list_destination = $this->mo_bbs->select_destination();
            func_set_data($this, 'list_destination', $list_destination);

            //=============== List port country ================//
            $list_port = $this->mo_bbs->select_port_name();
            func_set_data($this, 'list_port', $list_port);

        //====================== Pagination ==============//
            $per_page = 20;

            if(isset($_GET["page"])){

                if ($_GET["page"] != 0) {

                    $page=$_GET["page"];
                }else{
                    $page=1;
                }

            }else{
                $page=1;
            }


            $start_page=($page-1)*$per_page;
      
        //====================== Pagination ==============//




            $where['country'] = $this->input->get_post('country');
            $where['car_make'] = $this->input->get_post('car_make');
            $where['car_model'] = $this->input->get_post('car_model');
            $where['car_model_year'] = $this->input->get_post('car_model_year');
            $where['car_body_type'] = $this->input->get_post('car_body_type');
            $where['car_drive_type'] = $this->input->get_post('car_drive_type');
            $where['car_model_year >='] = $this->input->get_post('min_model_year');
            $where['car_model_year <='] = $this->input->get_post('max_model_year');
            $where['car_fob_cost >='] = $this->input->get_post('min_fob_cost');
            $where['car_fob_cost <='] = $this->input->get_post('max_fob_cost');
            //$where['car_chassis_no LIKE'] = '%'.$this->input->get_post('keyword').'%';

            $where_keyword = '';
            $keyword = $this->input->get_post('keyword');
            $keyword = trim($keyword);
            if(!empty($keyword)){

                $where['keyword'] = $keyword;
            }

            
             if(isset($_POST['submit_calculate_price'])){
            
            $calculate_price = $this->input->get_post('submit_calculate_price');
            $country_to = $this->input->get_post('destination_country');
            $port_name  = $this->input->get_post('port_name');
            $insurance  = $this->input->get_post('insurance');
            $inspection = $this->input->get_post('inspection');
            if($country_to!='' && $port_name!=''){ 
                if($insurance!=true){
                    $insurance=false;
                }
                if($inspection!=true){
                    $inspection=false;
                }

                $data = array(
                       'calculate_price' => $calculate_price,
                       'country_to'  => $country_to,
                       'port_name'     => $port_name,
                       'insurance' => $insurance,
                       'inspection' => $inspection
                   );
                $this->session->set_userdata($data);
            }else{
                $calculate_price = '';
                $country_to = '';
                $port_name  = '';
                $insurance  = $this->input->get_post('insurance');
                $inspection = $this->input->get_post('inspection');
                if($insurance!=true){
                    $insurance=false;
                }
                if($inspection!=true){
                    $inspection=false;
                }
                $data = array(
                       'calculate_price' => $calculate_price,
                       'country_to'  => $country_to,
                       'port_name'     => $port_name,
                       'insurance' => $insurance,
                       'inspection' => $inspection
                   );
                $this->session->set_userdata($data);
            }

            $calculate_price = $this->session->userdata('calculate_price');
            $get_country_to = $this->session->userdata('country_to');
            $get_port_name = $this->session->userdata('port_name');
            $get_insurance = $this->session->userdata('insurance');
            $get_inspection = $this->session->userdata('inspection');

            func_set_data($this, 'calculate_price', $calculate_price);
            func_set_data($this, 'country_to', $get_country_to);
            func_set_data($this, 'port_name', $get_port_name); 
            func_set_data($this, 'insurance', $get_insurance);
            func_set_data($this, 'inspection', $get_inspection);     
        }
        if($this->session->userdata('country_to')!=''){

            $calculate_price = $this->session->userdata('calculate_price');
            $get_country_to = $this->session->userdata('country_to');
            $get_port_name = $this->session->userdata('port_name');
            $get_insurance = $this->session->userdata('insurance');
            $get_inspection = $this->session->userdata('inspection');

            $where_country['country_to'] = $get_country_to;
            $list_port_name = $this->mo_bbs->select_port_name($where_country);

            func_set_data($this, 'list_port_name', $list_port_name);
            func_set_data($this, 'calculate_price', $calculate_price);
            func_set_data($this, 'country_to', $get_country_to);
            func_set_data($this, 'port_name', $get_port_name); 
            func_set_data($this, 'insurance', $get_insurance);
            func_set_data($this, 'inspection', $get_inspection); 
        }
            

             /*** Check for searching fields that is empty means user didn't input it ***/
            foreach($where as $key=>$value){
                if(empty($value)){
                    unset($where[$key]);
                }
            }


        if(isset($_GET["car"])){
            $result = $this->mo_bbs->get_public_premium_car(array(),$per_page,$start_page);

            $total_num_row = $this->mo_bbs->get_total_premium_car($where=array());
        }else{ 
                 /*** get total of rows for calculate pagination ***/
                $total_num_row = $this->mo_bbs->user_total_select($where,$where_keyword);
                /*** Get car rows ... LIMIT 0, 20 ***/
                $result = $this->mo_bbs->user_select($where, $where_keyword, $start_page, $per_page);

                /*** Test Output ***/
                //echo "Total Row: ".$total_num_row;
         }

        if(isset($get_port_name) && $get_port_name!=''){
            $list_total_price = '';
            foreach ($result as $key => $value) {
                $list_total_price[$value->idx] = $this->mo_shipping->get_total_price($get_port_name,$value->idx,$get_inspection,$get_insurance);
            }

            func_set_data($this, 'list_total_price',$list_total_price);
        }

        //=============== Select Sale Support Team ============//
        $country = (isset($_GET['country']) && $_GET['country'] !='') ? $_GET['country'] : '';
        $sale_support = $this->mo_bbs->get_sale_support_car_list($country);

        func_set_data($this, 'sale_support', $sale_support); 

        func_set_data($this, 'perpagepagination', $per_page);
        func_set_data($this, 'getnumpage', $page);
        func_set_data($this, 'countcarlist', $total_num_row);
        func_set_data($this, 'carlist', $result);

        $this->_load_public_mobile_view('public_mobile_site/car_list');


    }
	
	function mobile_cardetail(){
		$total_price = '';
		$customer_services = array();

		if(isset($_GET["idx"])){

    		$id = $_GET["idx"];

            //============= Set PC Version ==============//
                $pcversion = $this->replace_device("mobile_cardetail","cardetail");
                func_set_data($this, 'pcversion', $pcversion);
            //============= Set PC Version ==============//  

            $newdata = array(
                    'sesfunc'  => 'showdetailcar',
                    'idx'     => $id,
            );

            $this->session->set_userdata('sessionkeepdata_pagesearchajax',$newdata);
    		$related_post = $this->mo_bbs->related_post($id, 6);

            if(isset($_POST['submit_calculate_price'])){   
                $calculate_price = $this->input->get_post('submit_calculate_price');
                $country_to = $this->input->get_post('destination_country');
                $port_name  = $this->input->get_post('port_name');
                $insurance  = $this->input->get_post('insurance');
                $inspection = $this->input->get_post('inspection');

                if($country_to!='' && $port_name!=''){ 

                    if($insurance!=true){
                        $insurance=false;
                    }
                    if($inspection!=true){
                        $inspection=false;
                    }

                    $data = array(
                            'calculate_price' => $calculate_price,
                            'country_to'  => $country_to,
                            'port_name'     => $port_name,
                            'insurance' => $insurance,
                            'inspection' => $inspection
                        );
                    $this->session->set_userdata($data);

                    $where['iw_shipping_charge.country_to']=$country_to;
                    $where['iw_shipping_charge.port_name'] =$port_name;
                }
                        
            }
            if($this->session->userdata('country_to')!=''){

                $calculate_price = $this->session->userdata('calculate_price');
                $get_country_to = $this->session->userdata('country_to');
                $get_port_name = $this->session->userdata('port_name');
                $get_insurance = $this->session->userdata('insurance');
                $get_inspection = $this->session->userdata('inspection');

                $where['iw_shipping_charge.country_to']= $get_country_to;
                $where['iw_shipping_charge.port_name'] = $get_port_name;

                $where_country['country_to'] = $get_country_to;
                $list_port_name = $this->mo_bbs->select_port_name($where_country);

                func_set_data($this, 'list_port_name', $list_port_name);
                func_set_data($this, 'calculate_price', $calculate_price);
                func_set_data($this, 'country_to', $get_country_to);
                func_set_data($this, 'port_name', $get_port_name); 
                func_set_data($this, 'insurance', $get_insurance);
                func_set_data($this, 'inspection', $get_inspection); 
                $where['idx'] = $id;
                foreach($where as $key=>$value){
                    if(empty($value)){
                        unset($where[$key]);
                    }
                }
            }
            $where = array();
            
            $cardetail = $this->mo_bbs->getCarSpecification($id,$where);
       
            if(isset($calculate_price) && $calculate_price!=''){  
                
                $total_price = $this->mo_shipping->get_total_price($get_port_name,$cardetail[0]->idx,$get_inspection,$get_insurance);
                    
                if($total_price>0){

                    $total_price = $cardetail[0]->car_fob_currency." ".number_format($total_price);
                    
                 }else{

                    $total_price = "ASK";

                }
                    
            }

            func_set_data($this, 'total_price',$total_price);
            $list_country=  $this->mo_bbs->get_country_list();
            $getprimaryImage = $this->mo_bbs->getCarDetailPrimaryImage($id);
			$getDetailImage = $this->mo_bbs->getCarDetailImage($id);
            
            $list_destination = $this->mo_bbs->select_destination();

            $list_port = $this->mo_bbs->select_port_name();

			$member_profile_select = $this->member->member_profile_select($id);
            /*** GET CUSTOMER SUPPORT (SALES TEAM) ***/
            $where['idx'] = $id;
            $members = $this->mo_bbs->select_customer_service($where);
            if(count($members)>0){
                $customer_services[] =  $members[0];
            }else{
                $customer_services = $this->mo_bbs->get_sale_support_default();
            }
            
            $country=$this->mo_bbs->getCountry();
			func_set_data($this, 'member_profile_select',$member_profile_select);
			func_set_data($this, 'cardetail',$cardetail);
			func_set_data($this, 'related_post',$related_post);
			
            func_set_data($this, 'list_country',$list_country);
            func_set_data($this, 'list_destination',$list_destination);

            func_set_data($this, 'list_port',$list_port);

			func_set_data($this, 'getDetailImage',$getDetailImage);
            func_set_data($this, 'getPrimaryImage',$getprimaryImage);
            func_set_data($this, 'customer_service',$customer_services);
            func_set_data($this, 'country_list',$country);

		$arr_js = array('/js/tooltipster/jquery.tooltipster.min.js', 'js/public_site/jquery.chained.min.js',
            '/js/jquery.mousewheel.pack.js?v=3.1.3',
            '/js/fancybox/jquery.fancybox.pack.js?v=2.1.5',
            '/js/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5',
            '/js/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7',
            '/js/public_mobile_site/car_detail.js?v='.$this->upload_version,
            '/js/public_site/lightbox.js');

		$arr_css = array('/js/tooltipster/tooltipster.css',
            '/js/fancybox/jquery.fancybox.css?v=2.1.5',
            '/js/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7',
            '/css/public_mobile_site/car_detail.css?v='.$this->upload_version);

        $show_search_box = true;
        func_set_data($this, 'show_search_box',$show_search_box);

		func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);
		$this->_load_public_mobile_view('public_mobile_site/car_detail', $arr_js, $arr_css);
    	}
	}

    function mobile_seller(){
        $seller_profile= 'seller';
        $country= $this->input->get_post('country');
        $keyword = $this->input->get_post('keyword');

        //============= Set PC Version ==============//
            $pcversion = $this->replace_device("mobile_seller","public_seller");
            func_set_data($this, 'pcversion', $pcversion);
        //============= Set PC Version ==============//  

        $queryString="";
        if (isset($_POST['btn_search'])){
            $country=$this->db->escape_str($_POST['country']);
            $keyword=$this->db->escape_str($_POST['keyword']);
            $datasa = array('country'=> $country,'keyword'=> $keyword);
            $queryString =  http_build_query($datasa);

            $link = "/?c=user&m=mobile_seller&".$queryString."&page=1";
            die("<script>location.href = '".$link. "';</script>");
       }



        if($country !=""){
            $where['member.member_country ='] = $country;
        }else{
             $where['member.business_type ='] = $seller_profile;
        }
        if($keyword !=""){
            $where['keyword_member_name'] = $keyword;
        }else{
            $where['member.business_type ='] = $seller_profile;
        }

        


        $per_page = 5;

        if(isset($_GET["page"])){

            if ($_GET["page"] != 0) {

                $page=$_GET["page"];
            }else{
                $page=1;
            }



        }else{
            $page=1;
        }
        $start_page=($page-1)*$per_page;

        // $offset = 0;   //You Skip first 20 rows
        // $num_rows = 10; //You take 10 row
        $sellers = $this->customer->customer_select($where, $start_page, $per_page);

        $total_num_row = $sellers->total_count;
        $rows = $sellers->result();


        // Get total num_rows for pagination
        $count_num_row = $sellers->total_count;
        func_set_data($this, 'hide_search_box', true);
        func_set_data($this, 'sellers',$rows);
        func_set_data($this, 'total_count',$count_num_row);
        func_set_data($this, 'perpagepagination', $per_page);
        func_set_data($this, 'getnumpage', $page);

        $country=$this->customer->country_seller();
        func_set_data($this,'country',$country);
        $arr_js = array();
        $arr_css = array('/css/public_mobile_site/seller.css?v='.$this->upload_version);
        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);
        $this->_load_public_mobile_view('public_mobile_site/seller');
    }



    function mobile_how_to_buy(){

        $arr_js = array('/js/public_mobile_site/how_to_buy.js?v='.$this->upload_version);
        $arr_css = array('/css/public_mobile_site/how_to_buy.css?v='.$this->upload_version);
        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);

        //============= Set PC Version ==============//
            $pcversion = $this->replace_device("mobile_how_to_buy","how_to_buy");
            func_set_data($this, 'pcversion', $pcversion);
        //============= Set PC Version ==============//    

        $this->_load_public_mobile_view('public_mobile_site/how_to_buy',$arr_js,$arr_css);

    } 


    function mobile_about_us(){
        
        $arr_js = array('/js/public_mobile_site/about_us.js?v='.$this->upload_version);
        $arr_css = array('/css/public_mobile_site/about_us.css?v='.$this->upload_version);
        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);

        //============= Set PC Version ==============//
            $pcversion = $this->replace_device("mobile_about_us","about_us");
            func_set_data($this, 'pcversion', $pcversion);
        //============= Set PC Version ==============//

        $this->_load_public_mobile_view('public_mobile_site/about_us',$arr_js,$arr_css);

    } 

    function mobile_contact_us(){
        
        $arr_js = array('/js/tooltipster/jquery.tooltipster.min.js','/js/public_mobile_site/contact_us.js?v='.$this->upload_version);
        $arr_css = array('/js/tooltipster/tooltipster.css','/css/public_mobile_site/contact_us.css?v='.$this->upload_version);

        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);

        $jp='jp';
        $kr='kr';
        $slelect_team_sale_list=$this->member->slelect_team_sale($jp);
        $slelect_team_sale_list_kr=$this->member->slelect_team_sale($kr);
        func_set_data($this, 'slelect_team_sale_list', $slelect_team_sale_list);
        func_set_data($this, 'slelect_team_sale_list_kr', $slelect_team_sale_list_kr);

        //============= Set PC Version ==============//
            $pcversion = $this->replace_device("mobile_contact_us","contact_us");
            func_set_data($this, 'pcversion', $pcversion);
        //============= Set PC Version ==============//

        $this->_load_public_mobile_view('public_mobile_site/contact_us',$arr_js,$arr_css);

    } 


    function mobile_seller_detail(){
		if(isset($_GET["id"])){
				
			$id = $_GET["id"];

            //============= Set PC Version ==============//
                $pcversion = $this->replace_device("mobile_seller_detail","sellerdetail");
                func_set_data($this, 'pcversion', $pcversion);
            //============= Set PC Version ==============//  

		 	$where['car_owner'] =  $id;

		 	$per_page = 10;

			if(isset($_GET["page"])){

			if ($_GET["page"] != 0) {

				$page=$_GET["page"];
			}else{
				$page=1;
			}

			}else{
				$page=1;
			}

			$start_page=($page-1)*$per_page;


			/*** get total of rows for calculate pagination ***/
			$total_num_row = $this->mo_bbs->user_total_select($where);
			/*** Get car rows ... LIMIT 0, 20 ***/
			$result = $this->mo_bbs->user_select($where, '', $start_page, $per_page);
			/*** Test Output ***/
			//echo "Total Row: ".$total_num_row;
			$sellerdetail = $this->mo_bbs->getSellerSpecification($id);
			$member_profile_detail = $this->member->member_profile_detail($id);
			func_set_data($this, 'member_profile_detail',$member_profile_detail);
			func_set_data($this, 'carlist', $result);
			func_set_data($this, 'countcarlist', $total_num_row);
			func_set_data($this, 'perpagepagination', $per_page);
			func_set_data($this, 'getnumpage', $page);
			func_set_data($this, 'sellerdetail',$sellerdetail);
		$arr_js = array('/js/public_mobile_site/detail_seller.js');
        $arr_css = array('/css/public_mobile_site/detail_seller.css?v='.$this->upload_version);
        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);
        $this->_load_public_mobile_view('public_mobile_site/detail_seller');
    	}
	}
	
	 function mobile_login(){

        //============= Set PC Version ==============//
            $pcversion = $this->replace_device("mobile_login","public_login");
            func_set_data($this, 'pcversion', $pcversion);
        //============= Set PC Version ==============//  

        $arr_js = array('/js/public_mobile_site/login.js?v='.$this->upload_version);
        $arr_css = array('/css/public_mobile_site/login.css?v='.$this->upload_version);
        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);

        $error_type = $this->session->flashdata('error_type');
        func_set_data($this, 'error_type',$error_type);

        $this->_load_public_mobile_view('public_mobile_site/login');
    }
	
	 function mobile_register(){
        //============= Set PC Version ==============//
            $pcversion = $this->replace_device("mobile_register","public_register");
            func_set_data($this, 'pcversion', $pcversion);
        //============= Set PC Version ==============// 
        $is_exist = false;
        $is_verified = false;
        if (isset($_POST['btn_register'])){
            $current_url = $this->siteURL();
            $c_email=$this->input->get_post('email');
            $confirm_email=$this->input->get_post('confirm_email');
            //$html=file_get_contents('email_template/register.html', true);
            $data['email'] = $c_email;
            $data['grade_no'] = 10;
            /*** CHECK IF VERIFIED ***/
            $is_verified = $this->member->check_verify_by_email($c_email);
            /*** CHECK IF EXISTED ***/
            $result=$this->member->validate_email_Register($data);
            if($result['success']==0){
                $is_exist = true;
            }

            /*** CHECK IF ACCOUNT VERIFIED ***/
            if(!$is_verified){
                /*** CHECK IF ACCOUNT EXISTED  ***/
                if(!$is_exist){
                    $lastid = $this->member->insert_member($data);
                    $verificationCode = $this->member->createVerificationCode($lastid);
                    $this->member->insertVerification($lastid, $verificationCode);
                }else{
                    $user_detail = $this->member->getDetailByEmail($c_email);
                    $verificationCode = $user_detail->verification;
                }
                $this->mo_email->send_register($c_email, $verificationCode);
                $data = array('s_email' => $c_email );
                $set_email=$this->session->set_userdata($data);
                redirect('/?c=user&m=mobile_email_sent');
            }else{
                echo func_jsAlertReplace('Failed to register! Email already existed.', '/?c=user&m=mobile_register');
            }

            // if($result['success']==1){
            //    // echo $html;exit(); 
            // }else{
            //     echo func_jsAlertReplace('Email already exists!', 'refresh');
            // }
        }
         
        $arr_js = array('/js/public_mobile_site/mobile_register.js?v='.$this->upload_version);
        $arr_css = array('/css/public_mobile_site/register.css?v='.$this->upload_version);
        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);
        $this->_load_public_mobile_view('public_mobile_site/register');
    }
    function mobile_forget_password(){
        $arr_js = array('/js/public_mobile_site/mobile_reset_password.js?v='.$this->upload_version);
        $arr_css = array('/css/public_mobile_site/forget_password.css?v='.$this->upload_version);
        func_set_data($this, 'arr_js',$arr_js);
        func_set_data($this, 'arr_css',$arr_css);
        $this->_load_public_mobile_view('public_mobile_site/mobile_forget_password');
    }

	function showdetailcar(){
        ob_clean();
        $idx = $this->input->get_post('idx');

        header("Location: /?c=user&m=cardetail&idx=".$idx);
  //       $customer_services = array();
		// if(isset($_GET["idx"])){

		// 	$id = $_GET["idx"];


  //           $newdata = array(
  //                'sesfunc'  => 'showdetailcar',
  //                'idx'     => $id,
  //           );

  //           $this->session->set_userdata('sessionkeepdata_pagesearchajax',$newdata);
		// 	$related_post = $this->mo_bbs->related_post($id);
			
		// 	$cardetail = $this->mo_bbs->getCarSpecification($id);
  //           $list_country=  $this->mo_bbs->get_country_list();
  //           $getprimaryImage = $this->mo_bbs->getCarDetailPrimaryImage($id);
		// 	$getDetailImage = $this->mo_bbs->getCarDetailImage($id);
  //           $list_destination = $this->mo_bbs->select_destination();
		// 	$member_profile_select = $this->member->member_profile_select($id);
  //           /*** GET CUSTOMER SUPPORT (SALES TEAM) ***/
  //           $where['idx'] = $id;
  //           $members = $this->mo_bbs->select_customer_service($where);
  //           if(count($members)>0){
  //               $customer_services[] =  $members[0];
  //           }
            
  //           $country=$this->mo_bbs->getCountry();
		// 	func_set_data($this, 'member_profile_select',$member_profile_select);
		// 	func_set_data($this, 'cardetail',$cardetail);
		// 	func_set_data($this, 'related_post',$related_post);
			
  //           func_set_data($this, 'list_country',$list_country);
  //           func_set_data($this, 'list_destination',$list_destination);
		// 	func_set_data($this, 'getDetailImage',$getDetailImage);
  //           func_set_data($this, 'getPrimaryImage',$getprimaryImage);
  //           func_set_data($this, 'customer_service',$customer_services);
  //           func_set_data($this, 'country_list',$country);
		// 	$this->_load_view_layout_ajax('user/content/vehicle-detail', $this->data, false);

		// }


	}


   



	function showdetailseller(){

		 $id = $_GET["id"];
		 $where['car_owner'] =  $id;

		 $per_page = 12;

		if(isset($_GET["page"])){

			if ($_GET["page"] != 0) {

				$page=$_GET["page"];
			}else{
				$page=1;
			}

		}else{
				$page=1;
		}

		$start_page=($page-1)*$per_page;


			/*** get total of rows for calculate pagination ***/
			$total_num_row = $this->mo_bbs->user_total_select($where);
			/*** Get car rows ... LIMIT 0, 20 ***/
			$result = $this->mo_bbs->user_select($where, '', $start_page, $per_page);
			/*** Test Output ***/
			//echo "Total Row: ".$total_num_row;
			$sellerdetail = $this->mo_bbs->getSellerSpecification($id);
			$member_profile_detail = $this->member->member_profile_detail($id);
            func_set_data($this, 'hide_search_box', true);
			func_set_data($this, 'member_profile_detail',$member_profile_detail);
			func_set_data($this, 'carlist', $result);
			func_set_data($this, 'countcarlist', $total_num_row);
			func_set_data($this, 'perpagepagination', $per_page);
			func_set_data($this, 'getnumpage', $page);
			func_set_data($this, 'sellerdetail',$sellerdetail);
			$this->_load_view_layout_ajax('user/content/seller-detail', $this->data, false);


	}


	//접속통계 COUNT
    function _visit_statistics_select() {
        //$year = date('Y');
//        $month = date('m');
//        $day = date('d');
//
//        func_set_data($this, 'total_statistics_cnt', $this->statistics->select_total_cnt());      //총 접속수(메인)
//        func_set_data($this, 'today_statistics_cnt', $this->statistics->select_today_cnt($year, $month, $day));  //오늘 접속수(메인)
    }
    function _load_page_var(){
		
        $brandcars = $this->mo_bbs->countgetmake();

        func_set_data($this, 'brandcars',$brandcars);
		func_set_data($this, 'upload_version', $this->upload_version);
          
		 
		//chearado start facebook
		
		//$share_facebook = $this->mo_bbs->share_facebook($id = null);
//		$share_facebook = $share_facebook[0]->public_id;
		
		//func_set_data($this, 'share_facebook',$share_facebook);
		
		//chearado end facebook

        $carmaketodropdownsearch = $this->mo_bbs->carmaketodropdownsearch();

        func_set_data($this, 'carmaketodropdownsearch',$carmaketodropdownsearch);


        $carmodeltodropdownsearch = $this->mo_bbs->carmodeltodropdownsearch();

        func_set_data($this, 'carmodeltodropdownsearch',$carmodeltodropdownsearch);

        // $mincaryeartodropdownsearch = $this->mo_bbs->mincaryeartodropdownsearch();
        // func_set_data($this, 'mincaryeartodropdownsearch',$mincaryeartodropdownsearch);


        // $mincarpricetodropdownsearch = $this->mo_bbs->mincarpricetodropdownsearch();
        // func_set_data($this, 'mincarpricetodropdownsearch',$mincarpricetodropdownsearch);



        // $maxcarpricetodropdownsearch = $this->mo_bbs->maxcarpricetodropdownsearch();
        // func_set_data($this, 'maxcarpricetodropdownsearch',$maxcarpricetodropdownsearch);


        $carcountrytodropdownsearch = $this->mo_bbs->get_car_country();
        func_set_data($this, 'carcountrytodropdownsearch',$carcountrytodropdownsearch);

        $countcarhongkong = $this->mo_bbs->countcarhongkong();
        $countcarhongkong = $countcarhongkong[0]->counthongkongcar;
        func_set_data($this, 'countcarhongkong',$countcarhongkong);

        $countcarthailand = $this->mo_bbs->countcarthailand();
        $countcarthailand = $countcarthailand[0]->countthailandcar;
        func_set_data($this, 'countcarthailand',$countcarthailand);

        $countcarjapan = $this->mo_bbs->countcarjapan();
        $countcarjapan = $countcarjapan[0]->countjapancar;
        func_set_data($this, 'countcarjapan',$countcarjapan);


        $countcarkorea = $this->mo_bbs->countcarkorea();
        $countcarkorea = $countcarkorea[0]->countkoreacar;
        func_set_data($this, 'countcarkorea',$countcarkorea);


        $get_brand = $this->mo_bbs->get_brand_list();
        func_set_data($this, 'get_brand',$get_brand);

        $getbody_type = $this->mo_bbs->get_bodytype_list();
        func_set_data($this, 'getbody_type',$getbody_type);

        //========= make active menu =========//
            $action_menu = "";
            if(isset($_GET["m"])){

                $action_menu = strtolower($_GET["m"]);

            }else{
                $action_menu = "public_home";                
            }

            func_set_data($this, 'action_menu',$action_menu);

        //========= make active menu =========//

        //========= make active side =========// 

            $param = "";
            if((isset($_GET["m"]) and isset($_GET["car_make"])) or (isset($_GET["m"]) and isset($_GET["car_body_type"]))){

                $action = strtolower($_GET["m"]);
                if(isset($_GET["car_make"])){
                    $param = strtoupper($_GET["car_make"]);
                }else{
                    $param = strtoupper($_GET["car_body_type"]);   
                }   
                
            }else{
                $action = "public_home";  
                $param = "";              
            }


            func_set_data($this, 'action',$action);
             func_set_data($this, 'param',$param);


        //========= make active side =========//       
		
		$titlepage= "$_SERVER[REQUEST_URI]";
		
		
		//set page title header
        if($titlepage == "/"){
			$title = "Home - Minhas Trading";
		}elseif(strpos($titlepage,'carlist') !== false){
			$seg = $this->uri->segment(2);
			if(isset($_POST['searchboxcar'])){
				$title = "Search car - Minhas Trading";
			}elseif(isset($_GET["car_make"])){
				$title = ucfirst(strtolower($_GET["car_make"])).' - Minhas Trading';
			}elseif($seg == "car_make"){
				$make = $this->uri->segment(3);
				$title = ucfirst(strtolower($make)).' - Minhas Trading';
			}elseif($seg == "car_body_type"){
				$car_body_type = $this->uri->segment(3);
				$title = ucfirst(strtolower($car_body_type)).' - Minhas Trading';
			}elseif($seg == "country"){
				$country = $this->uri->segment(3);
				if($country == 'jp'){
					$title = "Japanese car - Minhas Trading";
				}elseif($country == 'kr'){
					$title = "Korean car - Minhas Trading";
				}
			}elseif(isset($_GET["car_body_type"])){
				$title = ucfirst($_GET["car_body_type"])." - Minhas Trading";
			}elseif(isset($_GET["country"])){
				if($_GET["country"] == 'jp'){
					$title = "Japanese car - Minhas Trading";
				}elseif($_GET["country"] == 'kr'){
					$title = "Korean car - Minhas Trading";
				}
			}else{
				$title  = "Cars - Minhas Trading";
			}
		}elseif(strpos($titlepage,'showdetailcar') !== false){
			$seg = $this->uri->segment(2);
			if(isset($_GET["idx"])){
				$id = $_GET["idx"]; 
				$cardetail = $this->mo_bbs->getCarSpecification($id);
				$title = $cardetail[0]->car_make.' '.$cardetail[0]->car_model." - Minhas Trading";	
			}elseif(!empty($seg)){
				$id = $this->uri->segment(2);
				$cardetail = $this->mo_bbs->getCarSpecification($id);
				$title = $cardetail[0]->car_make.' '.$cardetail[0]->car_model." - Minhas Trading";
			}else{
				$title = "Car detail - Minhas Trading";
			}
		}elseif(strpos($titlepage,'showdetailseller') !== false){
			$seg = $this->uri->segment(2);
			if(isset($_GET["id"])){
				$id = $_GET["id"]; 
				$sellerdetail = $this->mo_bbs->getSellerSpecification($id);
				$title = $sellerdetail[0]->company_name." - Minhas Trading";
			}elseif(!empty($seg)){
				$id = $this->uri->segment(2);
				$sellerdetail = $this->mo_bbs->getSellerSpecification($id);
				$title = $sellerdetail[0]->company_name." - Minhas Trading";
			}
			else{
				$title = "Seller detail - Minhas Trading";
			}
		}elseif(strpos($titlepage,'list_seller_profile') !== false){
			$title = "Sellers - Minhas Trading";
		}elseif(strpos($titlepage,'howtobuy') !== false){
			$title = "How To Buy - Minhas Trading";
		}elseif(strpos($titlepage,'aboutus') !== false){
			$title = "About us - Minhas Trading";
		}elseif(strpos($titlepage,'register') !== false){
			$title = "Register & Login - Minhas Trading";
		}elseif(strpos($titlepage,'login') !== false){
			$title = "Login - Minhas Trading";
		}elseif(strpos($titlepage,'logout') !== false){
			$title = "Logout - Minhas Trading";
		}else{
            $title ='Minhas Trading';
        }
		
		func_set_data($this, 'title_head',$title); 
		
		
		
		
		$meta_images = "";
        $meta_title = "";
        $meta_desc = "";
        $carpricefb = "";

        $current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		
        if(isset($_GET["idx"])){
            $id = $_GET["idx"]; 
            $getprimaryImage = $this->mo_bbs->getCarDetailPrimaryImage($id);
            $cardetail = $this->mo_bbs->getCarSpecification($id);
            

            if(($cardetail[0]->car_fob_cost != 0) or ($cardetail[0]->car_fob_cost != "0")){
                $carpricefb = "$".$cardetail[0]->car_fob_cost;
            }else{  
                $carpricefb = "ASK";
            }


            $meta_images = $getprimaryImage[0]->secure_url;
            $meta_title = $cardetail[0]->car_model_year . " " . $cardetail[0]->car_make . " " . $cardetail[0]->car_model . " , FOB : " . $carpricefb;
            
            if(strlen($cardetail[0]->contents) > 0){
                $meta_desc = strip_tags($cardetail[0]->contents);    
            }else{
                $meta_desc = "&nbsp;";
            }
            


                     
        }else{
            $meta_images = "https://res.cloudinary.com/softbloom/image/upload/v1435023731/slide/slide3";
            $meta_title = "korea used car,japan used car,used car stock, Minhas Trading, used car, japan used cars,KIA,HYUNDAI, TOYOTA ,HONDA,NISSAN,SUZUKI,DAIHATSU,SONATA,FIT,VITZ";
            $meta_desc = "We have the best Japanese and Korean used-car stocks with the best prices. Over 700 Used Cars in stock. Buy it with confidence.";


        }

        func_set_data($this, 'meta_images',$meta_images);   
        func_set_data($this, 'meta_title',$meta_title); 
        func_set_data($this, 'meta_desc',$meta_desc);
        func_set_data($this, 'current_url',$current_url); 



      

        $year = date('Y');
        $month = date('m');
        $day = date('d');

        $total_bbs_count = $this->mo_bbs->user_total_select();
        $total_statistics_cnt = $this->statistics->select_total_cnt();
       // $today_statistics_cnt = $this->statistics->select_today_cnt($year, $month, $day);
         /***GET CURRENT PAGE URL ***/
        $CI = & get_instance();
        $url = $CI->config->site_url($CI->uri->uri_string());
        //$current_url = $_SERVER['QUERY_STRING'] ? $url . '?' . $_SERVER['QUERY_STRING'] : $url;
        //$total_bbs_count = $total_bbs_count[0]->num_rows;
        $total_statistics_cnt = $total_statistics_cnt[0]->total_cnt;

        /*** SHOULD REQUEST TO FB GRAPH OR NOT ***/
        $allow_fb_graph_js = false;
        $md5_query = md5($_SERVER['QUERY_STRING']);
        $md5_url = $this->cache->get('md5url_'.$md5_query);
        if($md5_url==null) {
            $allow_fb_graph_js = true;
            $md5_url = $this->cache->set('md5url_'.$md5_query, true, 10800);// = 10800 second = 3hours (cache for 3hours)
        }
        func_set_data($this, 'allow_fb_graph_js', $allow_fb_graph_js);
        /*** END SHOULD REQUEST TO FB GRAPH OR NOT ***/
        //$output = $this->cache->get('md5url_'.$md5_query);
        
        func_set_data($this, 'total_bbs_count', $total_bbs_count);
        func_set_data($this, 'total_statistics_cnt', $total_statistics_cnt);      //총 접속수(메인)
        func_set_data($this, 'current_url', $current_url);  //오늘 접속수(메인)


    }
    function _load_view_layout($view_path){

        $contents_area = $this->load->view($view_path, $this->data, true);

        func_set_data($this, 'content', $contents_area);

        $this->load->view('user/config/template', $this->data, false);




    }



    function resend_verification_link(){
        $email = $this->input->get_post('email');
        $userDetail = $this->member->getDetailByEmail($email);
        $this->member->sendVerificationToEmail($userDetail->email, $userDetail->verification, $userDetail->member_id, $userDetail->member_first_name." ".$userDetail->member_last_name);
        echo func_jsAlertReplace('Success! Please verification link has been sent to your email', '/');
    }



    // function login() {
    //     if ($this->data['is_login'] == 'Y')
    //         redirect('/?c=admin', 'refresh'); //로그인 되어 있으d면..
    //     func_set_data($this, 'return_url', $this->input->get_post('return_url')); //이전 URL

    //     if($this->input->post('submit')){
    //         /*** user pressed button login ***/
    //         $this->_login_exec();
    //     }


    //     $this->_load_view_layout_ajax('user/register');
    // }

    function mobile_login_user() {
        if ($this->data['is_login'] == 'Y')
            redirect('/?c=admin&m=mobile_list_car', 'refresh'); //로그인 되어 있으d면..
        func_set_data($this, 'return_url', $this->input->get_post('return_url')); //이전 URL

        if($this->input->post('submit')){
            /*** user pressed button login ***/
            $this->_mobile_login_exec();
        }


        //$this->_load_view_layout_ajax('user/register');
    }

    function _restoreLoginSession(){

        if($this->input->cookie('lgno') && $this->input->cookie('lgsession')){
            /*** User set remember me before ***/
            ob_clean();
            $data = $this->member->getDetailBySession($this->input->cookie('lgno'), $this->input->cookie('lgsession'));
            $this->_saveLoginSession(
                $data->member_no,
                $data->member_id,
				$data->email,
                $data->member_first_name,
                $data->member_last_name,
                $data->grade_no,
                $data->grade_name,
                $data->business_type,
                $row->member_country);
            //$this->phpsession->get('grade_no', 'USER');

        }

    }
    function _saveLoginSession($member_no, $member_id, $email, $member_first_name, $member_last_name, $grade_no, $grade_name,$bussines_type,$member_country){
        /*** Save Login session ***/
        // $this->phpsession->save('member_no', $member_no, 'USER');
        // $this->phpsession->save('member_id', $member_id, 'USER');
        // $this->phpsession->save('member_first_name', $member_first_name, 'USER');
        // $this->phpsession->save('member_last_name', $member_last_name, 'USER');
        // //$this->phpsession->save('email', $row->email, 'USER');
        // $this->phpsession->save('grade_no', $grade_no, 'USER');
        // $this->phpsession->save('grade_name', $grade_name, 'USER');

            $this->phpsession->save('member_id', $member_id, 'ADMIN');
			$this->phpsession->save('email', $email, 'ADMIN');
			$this->phpsession->save('member_first_name', $member_first_name, 'ADMIN');
            $this->phpsession->save('member_no', $member_no, 'ADMIN');
            $this->phpsession->save('grade_no',  $grade_no, 'ADMIN');
            $this->phpsession->save('admin_login_yn', 'Y', 'ADMIN');
            $this->phpsession->save('iwc_login_yn', 'Y', 'ADMIN');
            $this->phpsession->save('base_url', '/?c=admin', 'ADMIN');
            $this->phpsession->save('session_grade_no',$grade_no, 'ADMIN'); //add grade_no
            $this->phpsession->save('business_type',$bussines_type, 'ADMIN');
            $this->phpsession->save('member_country',$member_country, 'ADMIN');
    }
    function _deleteLoginCookie(){
        delete_cookie("lgno");
        delete_cookie("lgsession");
    }
    function _saveLoginCookie($member_no, $remember_token){
        set_cookie("lgno", $member_no, 60 * 60 * 24 * 30);
        set_cookie("lgsession", $remember_token, 60 * 60 * 24 * 30);
    }
    function _login_exec() {
        if ($this->data['is_login'] == 'Y')
            redirect('/?c=admin', 'refresh'); //로그인 되어 있으면..
		$contact = '';
        $member_id = ''; //아이t
        $member_pwd = ''; //패스워드
        $return_url = ''; //이전 URL
		
		$contact = $this->input->get_post('contact', TRUE);
        $member_id = $this->input->get_post('member_id', TRUE);
        $member_pwd = func_base64_encode($this->input->get_post('member_pwd', TRUE));
        $return_url = $this->input->get_post('return_url', TRUE);
        $query = $this->member->member_chk($member_id, $member_pwd);

        $remember_me = $this->input->get_post('remember_me');
       // echo $remember_me;exit();
        /*** If correct ID and Password ***/

        $member_activated = $this->member->_member_activated($member_id);

        if($member_activated->grade_no=="10"){
            func_set_data($this, 'error_type', 'account_not_verified'); //이전 URL
            $this->session->set_flashdata('error_type', 'Your account is not confirmed yet. <br/>Not received Email Confirmation? <a href="/?c=user&m=public_register" class="resend_again">Resend again.</a>');
            redirect('/?c=user&m=public_login');
        }elseif($member_activated->grade_no=="1"||$member_activated->grade_no=="2"||$member_activated->grade_no=="3"){
            $this->session->set_flashdata('error_type', 'Incorrect ID or Password!');
            redirect('/?c=user&m=public_login');
        }else {

            if ($query->num_rows() > 0) {
                if($this->member->isVerified($member_id)){
                    /*** Valid ID and password and Account is verified ***/
                    $row = $query->row();
                    /***** Handle Rememebr Me *****/
                    $remember_me = $this->input->get_post('remember_me');
                    if($remember_me=='on'){
                        if(empty($row->remember_token) || $row->remember_token == NULL){
                            $remember_token = $this->member->generateRememberToken($member_id);
                            $this->member->saveRememberToken($row->member_no, $remember_token);
                        }else{
                            $remember_token = $row->remember_token;
                        }
                        /*** SET COOKIE 1 MONTH***/
                        //echo $row->remember_token;exit();
                        $this->_saveLoginCookie($row->member_no, $row->remember_token);

                    }

                    /*** Save Login session ***/
                    $this->_saveLoginSession(
                        $row->member_no,
                        $row->member_id,
    					$row->email,
                        $row->member_first_name,
                        $row->member_last_name,
                        $row->grade_no,
                        $row->grade_name,
                        $row->business_type,
                        $row->member_country);

                    //로그인 COUNT UPDATE
                    $this->member->login_count_update($member_id);
                    //로그인 DATE UPDATE
                    $this->member->login_date_update($member_id, func_get_date_time());

                    //이전 URL이 없다면
                    if ($return_url == '') {
                        
    					    if(!empty($contact)){
                            $data["idx"] = $this->session->userdata('idx');
                            $data["email"] = $this->phpsession->get('email', 'ADMIN');
    						$data["member_no"] = $this->phpsession->get('member_no', 'ADMIN');
                            $data["message"] = $this->session->userdata('message');
                            $data["total_price"] = $this->session->userdata('total_price');
    						$data["frieght_cost"] = $this->session->userdata('frieght_cost');
    						$data["inspection_cost"] = $this->session->userdata('inspection_cost');
                            $data["member_first_name"] = $this->phpsession->get('member_first_name', 'ADMIN');
                            
                            $this->session->set_userdata('idx', $data['idx']);
                            $this->session->set_userdata('email', $data['email']);
    						$this->session->set_userdata('member_no', $data['member_no']);
                            $this->session->set_userdata('message', $data['message']);
                            $this->session->set_userdata('total_price', $data['total_price']);
    						$this->session->set_userdata('frieght_cost', $data['frieght_cost']);
    						$this->session->set_userdata('inspection_cost', $data['inspection_cost']);
                            $this->session->set_userdata('member_first_name', $data['member_first_name']);
                            redirect('/?c=user&m=contact_email_inquiry&redirect=y');
    						}else{
    						   redirect('/?c=admin', 'refresh');
    						}
                    } else {
                        redirect($return_url, 'refresh');
                    }
                }else{
                    /*** Account not verified ***/
                    $user_email = $this->member->getEmailByMemberId($member_id);
                    func_set_data($this, 'error_type', 'account_not_verified'); //이전 URL
                    func_set_data($this, 'user_email', $user_email); //이전 URL
                    // $this->session->set_flashdata('error_type', 'Your account is not confirmed yet. <br/>Not received Email Confirmation? <a href="/?c=user&m=public_register" class="resend_again">Resend again.</a>');
                    $this->session->set_flashdata('error_type', 'Account not Verified! '.$user_email);
                }
            } else {
                if(!empty($contact)){
                    $this->session->set_flashdata('error_type', 'Incorrect ID or Password!');
                    redirect('/?c=user&m=public_login&contact_form='.$contact);
                }else{
                    $this->session->set_flashdata('error_type', 'Incorrect ID or Password!');
                    redirect('/?c=user&m=public_login');
                }
            }
        }
    }


    function _mobile_login_exec() {
       if ($this->data['is_login'] == 'Y')
            redirect('/?c=admin&m=mobile_home', 'refresh'); //로그인 되어 있으면..

        $member_id = ''; //아이t
        $member_pwd = ''; //패스워드
        $return_url = ''; //이전 URL

        $member_id = $this->input->get_post('member_id', TRUE);
        $member_pwd = func_base64_encode($this->input->get_post('member_pwd', TRUE));
        //echo $member_id ." ". $member_pwd; exit();
        $return_url = $this->input->get_post('return_url', TRUE);

        $query = $this->member->member_chk($member_id, $member_pwd);

        $remember_me = $this->input->get_post('remember_me');
   
        $member_activated = $this->member->_member_activated($member_id);

        if($member_activated->grade_no=="10"){
            func_set_data($this, 'error_type', 'account_not_verified'); //이전 URL
            $this->session->set_flashdata('error_type', 'Your account is not confirmed yet. <br/>Not received Email Confirmation? <a href="/?c=user&m=mobile_register" class="resend_again">Resend again.</a>');
            redirect('/?c=user&m=mobile_login');
        }else{
            /*** If correct ID and Password ***/
            if ($query->num_rows() > 0) {
                if($this->member->isVerified($member_id)){


                    /*** Valid ID and password and Account is verified ***/

                    $row = $query->row();


                    /***** Handle Rememebr Me *****/
                    $remember_me = $this->input->get_post('remember_me');
                    if($remember_me=='on'){
                        if(empty($row->remember_token) || $row->remember_token == NULL){
                            $remember_token = $this->member->generateRememberToken($member_id);
                            $this->member->saveRememberToken($row->member_no, $remember_token);
                        }else{
                            $remember_token = $row->remember_token;
                        }
                        /*** SET COOKIE 1 MONTH***/
                        //echo $row->remember_token;exit();
                        $this->_saveLoginCookie($row->member_no, $row->remember_token);

                    }

                    /*** Save Login session ***/
                    $this->_saveLoginSession(
                        $row->member_no,
                        $row->member_id,
                        $row->email,
                        $row->member_first_name,
                        $row->member_last_name,
                        $row->grade_no,
                        $row->grade_name,
                        $row->business_type,
                        $row->member_country);

                    //로그인 COUNT UPDATE
                    $this->member->login_count_update($member_id);

                    //로그인 DATE UPDATE
                    $this->member->login_date_update($member_id, func_get_date_time());

                    //이전 URL이 없다면
                    if ($return_url == '') {
                        //메인 페이지
                        // if ($_SESSION['ADMIN']['business_type']=="buyer") {
                        //     redirect('/?c=admin&m=mobile_buyer_list_car', 'refresh');
                        // }else{
                        //     redirect('/?c=admin&m=mobile_list_car', 'refresh');
                        // }
                        redirect('/?c=admin&m=mobile_home', 'refresh');
                    } else {
                        redirect($return_url, 'refresh');
                    }
                }else{
                    /*** Account not verified ***/
                    $user_email = $this->member->getEmailByMemberId($member_id);
                    func_set_data($this, 'error_type', 'account_not_verified'); //이전 URL
                    func_set_data($this, 'user_email', $user_email); //이전 URL
                    $this->session->set_flashdata('error_type', 'Account not Verified! '.$user_email);
                }

            }else{
                /*** Wrong id or password ***/
                $this->session->set_flashdata('error_type', 'Incorrect ID or Password!');

                redirect('/?c=user&m=mobile_login');
            }
        }
    }

    function logout_exec() {
        $this->phpsession->clear(null, 'USER');
        $this->_deleteLoginCookie();
        session_destroy();
        redirect('/?c=admin', 'refresh'); //메인 페이지
    }
    function logout_publish_exec() {
        $this->phpsession->clear(null, 'USER');
        $this->_deleteLoginCookie();
        session_destroy();
        redirect('/', 'refresh');
    }
	function mobile_logout_exec() {
        $this->phpsession->clear(null, 'USER');
        $this->_deleteLoginCookie();
        session_destroy();
        redirect('/?c=user&m=mobile_login', 'refresh'); //메인 페이지
    }
    public function testfun(){
        //echo phpinfo();
        //$this->Facebook();

    }

    function _init() {
        //전체 사이트 맵 조회
        $sitemap = $this->iw_config->select_site_map()->result();

        // var_dump($sitemap);
        // exit;

        if (count($sitemap) > 0) {
            foreach ($sitemap as $rows) {

                //회원관련 모듈의 메뉴코드_회원가입
                if (strstr($rows->menu_type, 'member_join')) {
                    $this->member_join_mcd = $rows->menu_code;
                    func_set_data($this, 'member_join_mcd', $this->member_join_mcd);
                }
                //회원관련 모듈의 메뉴코드_로그인
                if (strstr($rows->menu_type, 'member_login')) {
                    $this->member_login_mcd = $rows->menu_code;
                    func_set_data($this, 'member_login_mcd', $this->member_login_mcd);
                }
            }
        }

        if ($this->mcd != '') {
            //해당 메뉴코드별 사용 모듈 조회
            $this->menu_type = $this->_get_menu_code_site_map($this->mcd);
        }

        //사이트 메타 정보
        $this->_get_site_meta();

        //사이트 접속 통계 데이터 등록
        //사이트 접속 통계 데이터 조회
        $this->_visit_statistics_select();

        //방문자 정보 쿠키 셋팅
        $this->_visitor_set_cookie();

        //팝업
        $this->_popup();
    }

    //메인 페이지를 호출한다.
    function _get_site_main() {

        //메인 Layout Summary 정보를 가져온다.
        $summary_info = func_get_layout_info('MAIN_LAYOUT_001'); //레이아웃 고정
        //Summary 설정이 되어 있다면...
        if (count($summary_info) > 0) {
            $library_load_yn = false; //라이브러리 로드 여부

            for ($i = 0; $i < count($summary_info); $i++) {
                //echo '['.$summary_info[$i].']';
                $summary_menu_code = str_replace('summary_area_', '', $summary_info[$i]);
                //func_set_data($this, 'mcd', $summary_menu_code); //메인은 메뉴코드가 존재하지 않기 때문에...
                //해당 메뉴코드별 사용 모듈 조회
                $this->menu_type = $this->_get_menu_code_site_map($summary_menu_code);
                $module = $this->menu_type;

                if (!$library_load_yn) {
                    //해당 module load
                    $this->load->library($module);
                }
                $summary_contents = '';

                if ($module != '') {
                    //$summary_contents = $this->$module->get_summary($summary_menu_code);
                    $summary_contents = $this->bbs->get_summary($summary_menu_code);
                    func_set_data($this, $summary_info[$i], $summary_contents);   //Summary 영역
                } else {
                    show_error('메인 Summary 설정 오류입니다.');
                    exit;
                }

                $library_load_yn = true;
            }
        }
        $grade_no = $this->phpsession->get('grade_no', 'USER');
        $country_list = $this->mo_bbs->getLocation();
        $make_list = $this->mo_bbs->getMake(array(), true);

        func_set_data($this, 'grade_no', $grade_no);
        func_set_data($this, 'country_list', $country_list); //왼쪽 메뉴 영역
        func_set_data($this, 'make_list', $make_list); //왼쪽 메뉴 영역
        func_set_data($this, 'left_menu_area', $this->load->view('user/left_menu/left_menu', $this->data, true)); //왼쪽 메뉴 영역
        $this->load->view('user/layout/MAIN_LAYOUT_001', $this->data, false);
    }

    //서브 페이지를 호출한다.
    function _get_site_sub() {
        $html_header = ''; //상단 HTML
        $html_footer = ''; //하단 HTML
        $contents_area = ''; //컨텐츠 영역
        // var_dump($this->menu_type);
        // exit;
        //존재하지 않는 컨텐츠(메뉴코드)
        if ($this->menu_type == '') {
            echo func_jsAlertReplace('잘못된 호출 URL이거나 존재하지 않는 페이지 입니다.', '/');
            exit;
        }
        //일반 컨텐츠
        else if ($this->menu_type == 'contents') {
            //컨텐츠 상단
            $html_header = $this->load->view('user/contents_top' . '/' . $this->mcd . '/' . $this->mcd, $this->data, true) . "\n";
            $another_file = $this->input->get_post('file'); //같은 메뉴코드 디렉토리에 또 다른 페이지를 불러 올 경우.
            //같은 폴더내 다른 파일일 경우
            if ($another_file != '') {
                $contents_area = $this->load->view('user/contents' . '/' . $this->mcd . '/' . $another_file, $this->data, true) . "\n";
            } else {
                $contents_area = $this->load->view('user/contents' . '/' . $this->mcd . '/' . $this->mcd, $this->data, true) . "\n";
            }
        }
        //기능 컨텐츠
        else if ($this->menu_type != 'contents' && $this->menu_type != 'etc') {
            $module = $this->menu_type;
            $method = $this->input->get_post('me');  //호출 method
            $contents_area = '';
            func_set_data($this, 'method', $method);

            // var_dump($module);
            // exit;

            if ($module != '') {

                //해당 module load
                $this->load->library($module);

                //해당 method
                if ($method == '')
                    $method = 'index';

					$contents_area = $this->$module->$method() . "\n";



                //var_dump($contents_area);
                // exit;
            }
            else {
                show_error('해당 메뉴가 존재하지 않습니다.');
            }
            if (isset($this->data['html_header'])) {
                $html_header = $this->data['html_header'] . "\n";
            }
            if (isset($this->data['html_footer'])) {
                $html_footer = $this->data['html_footer'] . "\n";
            }

        }
        // 서브 Layout Summary 정보를 가져온다.
        $summary_info = func_get_layout_info('SUB_LAYOUT_001'); // 레이아웃 고정
        // Summary 설정이 되어 있다면...
        if (count($summary_info) > 0) {
            $library_load_yn = false; // 라이브러리 로드 여부
            for ($i = 0; $i < count($summary_info); $i++) {
                //echo '['.$summary_info[$i].']';
                $summary_menu_code = str_replace('summary_area_', '', $summary_info[$i]);
                //func_set_data($this, 'mcd', $summary_menu_code); // 메인은 메뉴코드가 존재하지 않기 때문에...
                //해당 메뉴코드별 사용 모듈 조회
                $this->menu_type = $this->_get_menu_code_site_map($summary_menu_code);
                $module = $this->menu_type;

                //echo $module;
                if (!$library_load_yn) {
                    //해당 module load
                    $this->load->library($module);
                }

                $summary_contents = '';

                if ($module != '') {
                    $summary_contents = $this->$module->get_summary($summary_menu_code);
                } else {
                    show_error('서브 Summary 설정 오류입니다.');
                    exit;
                }

                func_set_data($this, $summary_info[$i], $summary_contents);   //Summary 영역

                $library_load_yn = true;
            }
        }

        //$contents_area = $html_header.$contents_area.$html_footer;
        func_set_data($this, 'html_header', $html_header);    //컨텐츠 상단
        func_set_data($this, 'html_footer', $html_footer);    //컨텐츠 하단
        func_set_data($this, 'contents_area', $contents_area);   //컨텐츠 영역
        func_set_data($this, 'left_menu_area', $this->load->view('user/left_menu/left_menu', $this->data, true)); //왼쪽 메뉴 영역

        $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
    }


    //해당 메뉴코드별 사용 모듈 조회
    function _get_menu_code_site_map($mcd) {
        $query = $this->iw_config->select_menu_code_site_map($mcd);

        if ($query->num_rows() > 0) {
            $site_map_detail = $query->row();
            return $site_map_detail->menu_type;
        }

    }

    //사이트 메타 테그
    function _get_site_meta() {
        $result = $this->iw_config->select('site_meta')->result();  //사이트 메타 정보 조회
        $site_meta = '';
        if (func_get_config($result, 'Title') != '') {
            $site_meta .= "<meta name=\"Title\" content=\"" . func_get_config($result, 'Title') . "\" />\r\n";
        }
        if (func_get_config($result, 'Subject') != '') {
            $site_meta .= "<meta name=\"Subject\" content=\"" . func_get_config($result, 'Subject') . "\" />\r\n";
        }
        if (func_get_config($result, 'Description') != '') {
            $site_meta .= "<meta name=\"Description\" content=\"" . func_get_config($result, 'Description') . "\" />\r\n";
        }
        if (func_get_config($result, 'Keywords') != '') {
            $site_meta .= "<meta name=\"Keywords\" content=\"" . func_get_config($result, 'Keywords') . "\" />\r\n";
        }
        if (func_get_config($result, 'Copyright') != '') {
            $site_meta .= "<meta name=\"Copyright\" content=\"" . func_get_config($result, 'Copyright') . "\" />\r\n";
        }

        //사이트 타이틀
        func_set_data($this, 'title', func_get_config($result, 'Title') == '' ? 'Dev' : func_get_config($result, 'Title'));
        func_set_data($this, 'site_meta', $site_meta);
    }

    //사이트 접속 통계
    function getALLfromIP($addr, $gu) {
        $query = $this->statistics->select_country_ip(sprintf("%u", ip2long($addr)));

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                if ($gu == "cn") {
                    $ipcountry = $rows->cn;
                } else if ($gu == "cc") {
                    $ipcountry = $rows->cc;
                }
            }
            return $ipcountry;
        } else {
            return "unKnow";
        }

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                if ($rows->scroll_yn == 'Y')
                    $scrollbars = 'yes';
                if ($rows->scroll_yn == 'N')
                    $scrollbars = 'no';
                //해당 팝업의 쿠키가 존재하지 않으면...
                if (!get_cookie('IW_POPUP_' . $rows->idx)) {
                    $tmp_str .= "window.open(\"/?c=user&m=popup_view&idx=$rows->idx\", \"\", \"top=$top_interval, left=$left_interval, width=$rows->win_width, height=$rows->win_height, scrollbars=$scrollbars\");\n";
                    $left_interval = (int) $left_interval + (int) $rows->win_width + 10;
                }
            }
        }
    }

    function _visit_statistics() {
        $inData = array();
        $is_deny = FALSE;
        $agent = '';
        $version = '';

        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser();
            $version = $this->agent->version();
        } else if ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
            $is_deny = TRUE;
        } else if ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
            $is_deny = TRUE;
        }

        $inData['visit_year'] = date('Y');
        $inData['visit_month'] = date('m');
        $inData['visit_day'] = date('d');
        $inData['visit_time'] = date('H');
        $inData['visit_dateTime'] = date('Y-m-d H:i:s');
        $inData['menu_code'] = $this->mcd == '' ? 'site_main' : $this->mcd;
        $inData['visit_ip'] = $this->input->ip_address();
        $inData['agent'] = $agent;
        $inData['version'] = $version;
        $inData['is_robot'] = 'N';
        $inData['referer'] = '';
        $inData['search_site'] = ''; //검색 사이트
        $inData['query_name'] = ''; //검색 사이트 질의 변수명
        $inData['keyword'] = ''; //검색 키워드
        $inData['countrycd'] = $this->getALLfromIP($inData['visit_ip'], "cc");
        $this->phpsession->save('visitor_cc', $inData['countrycd'], 'USER');

        $inData['country'] = $this->getALLfromIP($inData['visit_ip'], "cn");

        //referrer 체크
        if ($this->agent->is_referral()) {
            $referrer = $this->agent->referrer();
            $referrer = str_replace('https://', '', $referrer);
            $referrer = str_replace('http://', '', $referrer);
            $referrer = str_replace('www.', '', $referrer);
            $referrer = substr($referrer, 0, strpos($referrer, '/'));
            $inData['referer'] = $referrer;
        }
        //검색사이트 referer 체크
        if ($this->agent->is_referral()) {
            $http_referer = $this->agent->referrer();

            if (stripos($http_referer, 'search.naver.com')) { //네이버
                $inData['search_site'] = 'naver';
                $inData['query_name'] = 'query';
                $tmp = substr($http_referer, stripos($http_referer, '&query=') + 7);

                //검색어 뒤에 '&' 파라미터 여부
                if (!stripos($tmp, '&')) {
                    $inData['keyword'] = $tmp;
                } else {
                    $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
                }
            }
            if (stripos($http_referer, 'search.daum.net')) { //다음
                $inData['search_site'] = 'daum';
                $inData['query_name'] = 'q';

                $tmp = substr($http_referer, stripos($http_referer, '&q=') + 3);

                //검색어 뒤에 '&' 파라미터 여부
                if (!stripos($tmp, '&')) {
                    $inData['keyword'] = $tmp;
                } else {
                    $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
                }
            }
            if (stripos($http_referer, 'search.nate.com')) { //네이트
                $inData['search_site'] = 'nate';
                $inData['query_name'] = 'q';

                $tmp = substr($http_referer, stripos($http_referer, '&q=') + 3);

                //검색어 뒤에 '&' 파라미터 여부
                if (!stripos($tmp, '&')) {
                    $inData['keyword'] = $tmp;
                } else {
                    $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
                }
            }
            if (stripos($http_referer, 'search.yahoo.com')) { //야후
                $inData['search_site'] = 'yahoo';
                $inData['query_name'] = 'p';

                $http_referer = str_replace('https://', '', $http_referer);
                $http_referer = str_replace('http://', '', $http_referer);
                $http_referer = str_replace('www.', '', $http_referer);

                $tmp = '';
                if (stripos($http_referer, '?p=')) {
                    $tmp = substr($http_referer, stripos($http_referer, '?p=') + 3);
                } else if (stripos($http_referer, '&p=')) {
                    $tmp = substr($http_referer, stripos($http_referer, '&p=') + 3);
                }

                //검색어 뒤에 '&' 파라미터 여부
                if (!stripos($tmp, '&')) {
                    $inData['keyword'] = $tmp;
                } else {
                    $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
                }
            }
            if (stripos($http_referer, '.google.')) { //구글
                $inData['search_site'] = 'google';
                $inData['query_name'] = 'q';

                $tmp = substr($http_referer, stripos($http_referer, '&q=') + 3);

                //검색어 뒤에 '&' 파라미터 여부
                if (!stripos($tmp, '&')) {
                    $inData['keyword'] = $tmp;
                } else {
                    $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
                }
            }
            if (stripos($http_referer, 'search.paran.com')) { //파란
                $inData['searchEngine'] = 'paran';
                $inData['query'] = 'Query';

                $tmp = substr($http_referer, stripos($http_referer, '&Query=') + 7);

                //검색어 뒤에 '&' 파라미터 여부
                if (!stripos($tmp, '&')) {
                    $inData['keyword'] = $tmp;
                } else {
                    $inData['keyword'] = substr($tmp, 0, stripos($tmp, '&'));
                }
            }
            $inData['keyword'] = @iconv('EUC-KR', 'UTF-8', urldecode($inData['keyword']));
            if (isset($inData['keyword'])) {
                $inData['keyword'] = urldecode($inData['keyword']);
            }
        }

        //config/user_agent.php 에 등록되지 않은 browser deny
        if ($is_deny) {
            $inData['is_robot'] = 'Y';
            $this->statistics->insert($inData);
            show_error('알수없는 브라우저 형식으로 접근 하였습니다.');
            exit;
        } else {
//			if ($inData['menu_code'] != 'site_main') {

            $this->statistics->insert($inData);

//			} else {
//아이피 중복 체크(하루)
//				if (get_cookie('IW_VISITOR_' . $this->input->ip_address())) {
//					$this->statistics->insert($inData);
//				}
//			}
        }
    }



    //방문자 정보 쿠키 셋팅
    function _visitor_set_cookie() {
        $cookie = array(
            'name' => 'VISITOR_' . $this->input->ip_address(),
            'value' => $this->input->ip_address(),
            'expire' => 60 * 60 * 24 * 1,
            'domain' => '',
            'path' => '/',
            'prefix' => 'IW_',
        );

        set_cookie($cookie);
    }

    //팝업 게시 체크
    function _popup() {
        $query = $this->common->select_open_popup(); //팝업 개시 여부 조회 'Y'
        $top_interval = 0; //팝업 간격 (TOP)
        $left_interval = 0; //팝업 간격 (LEFT)
        $scrollbars = 'no';

        $tmp_str = "<script type=\"text/javascript\">\n";

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                if ($rows->scroll_yn == 'Y')
                    $scrollbars = 'yes';
                if ($rows->scroll_yn == 'N')
                    $scrollbars = 'no';
                //해당 팝업의 쿠키가 존재하지 않으면...
                if (!get_cookie('IW_POPUP_' . $rows->idx)) {
                    $tmp_str .= "window.open(\"/?c=user&m=popup_view&idx=$rows->idx\", \"\", \"top=$top_interval, left=$left_interval, width=$rows->win_width, height=$rows->win_height, scrollbars=$scrollbars\");\n";
                    $left_interval = (int) $left_interval + (int) $rows->win_width + 10;
                }
            }
        }

        $tmp_str .= "</script>\n";
        func_set_data($this, 'javascript', $tmp_str);
    }

    //팝업 게시
    function popup_view() {
        $idx = $this->input->get_post('idx');
        $popup_detail_list = $this->common->select_detail_popup($idx); //상세 조회

        func_set_data($this, 'idx', $idx);
        func_set_data($this, 'popup_detail_list', $popup_detail_list);

        $this->load->view('common/popup', $this->data, false);
    }
    function testPage(){

        $contents_area = $this->load->view('user/messages/list', $this->data, true);

        func_set_data($this, 'html_header', "");
        func_set_data($this, 'contents_area', $contents_area);
        func_set_data($this, 'html_footer', "");
        $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
    }
    // function register_exec() {


    //     $inData = array();
    //     $messages = array();
    //     //$password = $this->input->get_post('member_pwd');
    //     $inData['member_id'] = $_POST['member_id'];
    //     $inData['member_pwd'] = $_POST['member_pwd'];
    //     //$inData['cmember_pwd'] = $this->input->post('cmember_pwd');
    //     $inData['email'] = $_POST['email'];
    //     $inData['member_first_name'] = $_POST['member_first_name'];
    //     $inData['member_last_name'] = $_POST['member_last_name'];
    //     $inData['sex'] = $_POST['sex'];
    //     $inData['member_country'] = $_POST['member_country'];
    //     $inData['mobile_no1'] = $_POST['mobile_no1'];
    //     $inData['mobile_no2'] = $_POST['mobile_no2'];
    //     $inData['mobile_no3'] = $_POST['mobile_no3'];
    //     $inData['grade_no'] = 15;

    //     // if ($inData['member_id'] == '' || $inData['sex'] == '' || $inData['member_pwd'] == '' || $inData['email'] == '' || $inData['member_first_name'] == '' || $inData['member_country'] == '') {
    //     //     $messages['messages'][] = "Some of required fields are empty";
    //     // }

    //     // //check value country
    //     // $CC = $inData['member_country'];
    //     // $getcc = mysql_query("SELECT id FROM iw_country_list WHERE cc = '{$CC}'");
    //     // $row_cnt = mysql_num_rows($getcc);

    //     // if ($row_cnt == 0) {
    //     //     $messages['messages'][] = "Not a valid country";
    //     // }
    //     // //end check
    //     // $member_id = $inData['member_id'];
    //     // $getmember_id = mysql_query("SELECT member_no FROM iw_member WHERE member_id = '{$member_id}'");
    //     // $row_member = mysql_num_rows($getmember_id);

    //     // if ($row_member > 0) {
    //     //     $messages['messages'][] = "This member id already exists";

    //     // }
    //     // $first_name = $inData['member_first_name'];
    //     // $last_name = $inData['member_first_name'];

    //     // if (!preg_match('/^[\p{L} ]+$/u', $first_name) == 1 || !preg_match('/^[\p{L} ]+$/u', $last_name)) {
    //     //     $messages['messages'][] = "Last name and first name must be only letters";
    //     // }
    //     // if ($inData['sex'] != 'M' && $inData['sex'] != 'W') {
    //     //     $messages['messages'][] = "Invalid gender";
    //     // }

    //     // if(count($messages)>0){
    //     //     $messages['status']="fail";
    //     //     write_file("logs/errors.txt",  json_encode($messages)."\n", "a+");

    //     // }else{
    //     //     write_file("logs/errors.txt",  "Success\n", "a+");

    //     //     $messages['status']="success";
    //         $inData['member_pwd'] = func_base64_encode($inData['member_pwd']);
    //         $inData['created_dt'] = func_get_date_time();

    //         $insert_id = $this->mo_bbs->registerUser($inData);

    //         $verificationCode = $this->mo_bbs->createVerificaitonCode($insert_id);
    //         $email = $this->mo_bbs->getUserEmail($insert_id);
    //         $this->sendVerificationToEmail($email, $verificationCode);
    //     //}

    //     //var_dump($messages);
    //     echo json_encode($messages);
    //     //echo func_jsAlertReplace('Success! Your account has been created. Please check your email for account verification. Thank you!', './');

    // }
    function getPortOption() {

        if($_POST['country_code'] && $_POST['fob_currency'])
        {
            //echo "<option selected='selected'>".$_POST['country_code']."</option>";
            $country_code=$_POST['country_code'];
            $fob_currency=$_POST['fob_currency'];
            $sql=mysql_query("select a.*, b.country_name from iw_shipping_charge a left outer join iw_country_list b on a.country_to = b.cc where a.country_to='$country_code' and a.currency_type='$fob_currency' ");

            echo "<option value='NoValue' selected='selected'>--Select City--</option>";
            while($row=mysql_fetch_array($sql))
            {
            $port_id=$row['id'];
            $port_name=$row['port_name'];
            echo '<option value="'.$port_id.'">'.$port_name.'</option>';
            }
        }
    }
    function total_price_calculator() {
        //var_dump($_POST);
        if($_POST['country']=='NoValue' || $_POST['port']=='NoValue'){
            echo "<span style='font-size:20px;color:blue;'>Please Choose Your Correct Destination.<span>";
        }else{

            if(number_format($_POST['fob_cost'])== 0){
                echo "<span style='font-size:20px;color:blue;'>Please Ask FOB Cost to Blauda</span>";
            }else{

                $fob_price = $_POST['fob_cost'];
                $port_id = $_POST['port'];

                $sql=mysql_query("select * from iw_shipping_charge where id='$port_id'");

                while($row=mysql_fetch_array($sql))
                {
                $shipping_cost = $row['shipping_cost'];
                $insurance_fee = ($_POST['insurance_option'] == 'Y') ? $row['insurance'] : 0 ;
                $inspection_fee = ($_POST['inspection_option'] == 'Y') ? $row['inspection'] : 0 ;
                }

                $total_price = $fob_price + $shipping_cost + $insurance_fee + $inspection_fee;

                echo "<span style='font-size:25px;color:red;font-weight:bold;'> $total_price USD</span>";
            }

        }
    }
    // function upload_test(){
    //     $this->_load_view_layout('user/uploadtest');
    // }
    // function upload_exec_test(){
    //     $this->_upload_to_cloud('1', 'filetest');
    // }
    /*** return iw_cloud_files.id after upload to cloudinary ***/
    function _upload_to_cloud($member_no, $element_file_name){
        if(isset($_FILES[$element_file_name])){
           if (!empty($_FILES[$element_file_name]['name'])) {
                $default_options = array('tags'=>array("member_image"), 'public_id'=>uniqid($member_no, true));
                $upload_result = \Cloudinary\Uploader::upload($_FILES[$element_file_name]['tmp_name'], $default_options);
                /***GET BASE URL***/
                $exp_url = explode('/', $upload_result['secure_url']);
                $exp_last_key = count($exp_url)-1;
                $exp_url[$exp_last_key]='';
                unset($exp_url[$exp_last_key-1]);
                $data['base_url'] = implode('/', $exp_url);
            
                $data['public_id']          = $upload_result['public_id'];
                $data['file_owner']         = $member_no;
                $data['resource_type']      = $upload_result['resource_type'];
                $data['secure_url']         = $data['base_url'].$data['public_id'];
                $data['original_filename']  = $_FILES[$element_file_name]['name'];
                $insert_id = $this->mo_file->insert($data);
                return $insert_id;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    function sendVerificationToEmail($email, $id, $verificationCode){
        require 'libraries/PHPMailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'bohor.arvixe.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@motorbb.com';                 // SMTP username
        $mail->Password = '~PF!@BUtg)b*';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                // TCP port to connect to

        $mail->From = 'noreply@motorbb.com';
        $mail->FromName = 'Minhas Trading';
        //$mail->addAddress('aslam0919@blauda.com', 'IBLAUDA');     // Add a recipient
        $mail->addAddress($email, 'Minhas Trading');     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('noreply@motorbb.com', 'noreply@motorbb.com');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true); 
        $html = '<html><head></head><body>';
        $html.= 'Hi,<br/><br/>You have recently registered new account at Minhas Trading<br/><br/>Your login ID: '.$id.'<br/><br/>Please verify your email by clicking the link below:<br/><br/><a href="https://motorbb.com/?c=user&m=verifyAccount&vcode='.$verificationCode.'"/>https://motorbb.com/?c=user&m=verifyAccount&vcode='.$verificationCode.'</a><br/><br/>Thank you for joining <a href="https://motorbb.com/">Minhas Trading</a>.';

        $html.= '</body></html>';

        $mail->Subject = 'Minhas Trading: Email Verification';
        $mail->Body    = $html;
        // $config = Array(
        //     'protocol' => 'mail',
        //     'smtp_host' => 'ssl://kangaroo.arvixe.com',
        //     'smtp_port' => '465',
        //     'smtp_user' => 'noreply@motorbb.com',
        //     'smtp_pass' => '~PF!@BUtg)b*',
        //     'charset' => 'utf-8',
        //     'mailtype' => 'html',
        //     'newline' => "\r\n",
        //     'crlf' => "\n",
        //     'wordwrap' => TRUE
        // );
        // $this->load->library('email');
        // $this->email->initialize($config);

        // $this->email->from('noreply@motorbb.com', 'IBLAUDA');
        // $this->email->reply_to("noreply@motorbb.com", "noreply@motorbb.com");
        // $this->email->to($email);
        // $this->email->subject('Motorbb.com: Email Verification');
        // $this->email->message($html);
        // $this->email->send();



        if(!$mail->send()) {
           return false;
        } else {
           return true;
        }

    }
    function testRegister(){
         $this->load->view('user/json/testRegister');
    }
    // public function messagess(){
    //     $where=array();
    //     //Get model rows
    //     $result = $this->mo_message->getMessages();
    //     //Save rows to date
    //     func_set_data($this, 'rows', $result);
    //     //Get content html
    //     $this->load->view('user/json/getMessages', $this->data, false);
    // }
    // Function to Delete Messages record from database.
    function deleteMessageExec() {
        if ($this->is_login == 'Y') {
            $data = $this->input->post('id');
            $where_in = array_values($data);
            $this->mo_message->deleteMessage($where_in);
            redirect('./?c=user&m=messages', 'refresh');
        }else{
            echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
        }
    }
    // function updateReadDate(){
    //     $data = $this->input->post('id');
    //     $where = $data;
    //     $this->mo_message->updateReadDate($where);
    //     //redirect('./?c=user&m=messages', 'refresh');
    // }
    // pagination for inbox
    function messages(){
        if ($this->is_login == 'Y') {
            $this->load->helper('url');
            //pagination init
            $this->load->library('pagination');
            //pagination end init
            $where = array();

            //pagination process
            $total_rows = $this->mo_message->count_messages($where);
            $current_url = current_url()."?c=user&m=messages";
            $per_page = $this->input->get_post("per_page");


            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
            $config['num_links'] = 5;
            $config['base_url'] = $current_url;
            $config['total_rows'] = $total_rows;
            $config['per_page'] = 10;

            $item_perpage = $config['per_page'];
            $total_page =ceil($total_rows/$item_perpage);

            $cur_page =$per_page;
            if ($cur_page == NULL || $cur_page == ''){
                $cur_page = 1;
            }elseif ($cur_page>2) {
                $cur_page = ceil($cur_page/$item_perpage+1);
            }

            $this->pagination->initialize($config);
            $pagination = $this->pagination->create_links();
            $limit_s = $per_page;
            $limit_e = $config['per_page'];
            //pagination end process

            $order_list_query = $this->mo_message->getMessages($limit_s, $limit_e, $where);

            $message_list = $order_list_query;
            func_set_data($this, 'total_rows', $total_rows);
            func_set_data($this, 'cur_page', $cur_page);
            func_set_data($this, 'total_page', $total_page);

            func_set_data($this, 'pagination', $pagination);
            func_set_data($this, 'rows', $message_list);

            func_set_data($this, 'html_header', "");
            func_set_data($this, 'html_footer', "");
            $contents_area = $this->load->view('user/messages/list', $this->data, true);
            func_set_data($this, 'contents_area', $contents_area);
            $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
        }else{
            //echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
        }
    }
    function messagesdetail(){
        $where=array();
        $where['id'] = $_REQUEST['id'];
        if ($this->is_login == 'Y') {

            //Get model rows
            $result = $this->mo_message->messagesdetail($where);
            //Save rows to date
            func_set_data($this, 'rows', $result);
            func_set_data($this, 'html_header', "");
            func_set_data($this, 'html_footer', "");
            if(count($result)>0){
                $this->mo_message->updateReadDate($where);

                $contents_area = $this->load->view('user/messages/list_detail', $this->data, true);
                func_set_data($this, 'contents_area', $contents_area);
            }else{
                func_set_data($this, 'contents_area', "The message does not exist");
            }
            $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
        }else{
            echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
        }
    }

    function deletecarordered(){
        $where=array();
        $user_session = $this->phpsession->get('member_no', 'USER');
        $where['stock_idx'] = $_REQUEST['id'];
        $where['customer_no'] = $user_session;
        if ($this->is_login == 'Y') {
                $this->mo_bbs->deleteCarOrderedDate($where);
                redirect('./?c=user&m=reservedlist', 'refresh');
        }else{
            echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
        }
    }

    function deletecarordereds(){
        $where=array();
        $user_session = $this->phpsession->get('member_no', 'USER');
        $where['stock_idx'] = $_REQUEST['id'];
        $where['customer_no'] = $user_session;
        if ($this->is_login == 'Y') {
                $this->mo_bbs->deleteCarOrderedDate($where);
                redirect('./?c=user&m=orderCarList', 'refresh');
        }else{
            echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
        }
    }

	
	
	function contact_email_inquiry(){
		
		if(isset($_POST['customer_send']) || isset($_GET['redirect'])){

                
                /*** SEND CAR DETAIL TO CUSTOMER MAIL***/
					      
					/*save session contact*/
                    
                    if(isset($_POST['car_idx'])){
                        $idx = $this->input->post('car_idx');
                        $email = $this->input->post('email');
                        $message = $this->input->post('message');
                        $total_price = $this->input->post('total_price');
						$frieght_cost = $this->input->post('frieght_cost');
						$inspection_cost = $this->input->post('inspection_cost');
                       // $sender_email = $this->input->post('sender_email');
                        //exit();
                        
                        $data = array(
                       'idx' => $idx,
                       'email' => $email,
                       'message' => $message,
                       'total_price' => $total_price,
					   'frieght_cost' => $frieght_cost,
					   'inspection_cost' => $inspection_cost
                        );
                        $this->session->set_userdata($data);
                    }
                    
                    $idx = $this->session->userdata('idx');
                    
                    /*save session contact*/
                    
                    
                    $car_detail = '';
                    $detail_html = '';
                    $where['iw_bbs.idx'] = $idx;
                    $results = $this->mo_bbs->get_car_detail_send($where);
                     
                    foreach ($results as $rows) {

                        $car_idx=$rows->idx;
                        $car_image=$rows->base_url.'c_fill,h_89,w_128/'.$rows->fileimgname;
                        if($car_image!=""){
                            $car_image_show=$rows->base_url.'c_fill,h_89,w_128/'.$rows->fileimgname;
                        }else{
                            $car_image_show=$rows->base_url.'c_fill,h_89,w_128/v1435029821/no_image_comzyj';
                        }
                        $car_price=$rows->car_fob_cost;
                        if($car_price!=""){
                           $car_price_fob=number_format($rows->car_fob_cost);
                        }else{
                            $car_price_fob="Ask";
                        }
                        $car_country=$rows->country;
                        if($car_country=="jp"){
                            $car_country="Japan";
                        }else{
                             $car_country="korea";
                        }
						
						if($this->session->userdata('idx')!=''){
                            $idx = $this->session->userdata('idx');
                            $email = $this->phpsession->get('email', 'ADMIN');;
							$member_no = $this->session->userdata('member_no');
                            $message = mysql_real_escape_string($this->session->userdata('message'));
                            $total_price = $this->session->userdata('total_price');
							$frieght_cost = $this->session->userdata('frieght_cost');
							$inspection_cost = $this->session->userdata('inspection_cost');
                            $name = $this->session->userdata('member_first_name');
                        }
                        
						/*** GET CUSTOMER SUPPORT (SALES TEAM) ***/
						// $where['idx'] = $idx;
						// $members = $this->mo_bbs->select_customer_service($where);
                        // if(count($members)>0){
                        //    $sale_email=$members[0]->email;
                        // }else{
                        //    $support_default    = $this->mo_bbs->select_support_default();
                        //    $default            = $support_default->result();
                        //    $default_contact_us = $this->member->select_customer_no($default[0]->support_member_no);
                        //    $sale_email         = $default_contact_us->email;
                        // }

						$sale_email="vuthy@blauda.com";
						
						/*** GET CUSTOMER SUPPORT (SALES TEAM) ***/
                        //$link = "https://motorbb.com/?c=user&m=cardetail&idx=".$idx;
                        $car_title=$rows->car_model_year.' '.$rows->car_make.' '.$rows->car_model;
						$idx=$rows->idx;
						$inspection=$inspection_cost;
						$frieght_cost=$frieght_cost;
						$make=$rows->car_make;
						$model=$rows->car_model;
						$model_year=$rows->car_model_year;
						$chassis=$rows->car_chassis_no;
						if( $rows->icon_new_yn == "Y"){
						$condition = "New";}else{ $condition = "Used";}
						$mileage=$rows->car_mileage;
						$steering=$rows->car_steering;
						$transmission=$rows->car_transmission;
						$fuel=$rows->car_fuel;
						$color=$rows->car_color;
						$manu_date=$rows->manu_year;
						$reg_date=$rows->first_registration_month."-".$rows->first_registration_year;
                        $detail_html.=$this->get_detailcar_send($car_title, $car_price_fob, $total_price,  $car_image_show,$car_country, $idx, $frieght_cost, $inspection, $make,$model,$model_year,$chassis,$condition ,$mileage,$steering,$transmission,$fuel,$color,$manu_date,$reg_date);
						

                    }
                    $car_link = "https://motorbb.com/?c=user&m=cardetail&idx=".$idx;
                    $html = file_get_contents('https://motorbb.com/email_template/car_inquiry.html', true);
                    $html = str_replace('[::CAR_LINK::]', $car_link, $html);
                    $html = str_replace('[::CAR_DETAIL::]', $detail_html, $html);
					$html = str_replace('[::CAR_TITLE::]', $car_title, $html);
                    $html = str_replace('[::EMAIL::]', $email, $html);
					$html = str_replace('[::MESSAGE::]', $message, $html);
                    $html = str_replace('[::NAME::]', $name, $html);
					//echo $html;exit();
                   
                /*** END CAR DETAIL TO CUSTOMER MAIL***/

					
				     if($this->is_login == 'Y'){
                        $to = $this->mo_email->send_contact_seller_car($email, $sale_email, $html);
						$this->mo_bbs->insert_message_negotiate($idx, $member_no, $message);
                        
                        echo func_jsAlertReplace('Message has been sent.', '/?c=user&m=cardetail&idx='.$idx);
						// echo"<script type='text/javascript' src='http://code.jquery.com/jquery-latest.min.js'></script>
						// <script type='text/javascript' src='/js/public_site/jquery.modal.js'></script>
						// <link rel='stylesheet' href='/css/public_site/jquery.modal.css' />";
                        // echo func_jsModalAlert('Message has been sent.', '/?c=user&m=cardetail&idx='.$idx);
                      }else{

                        $this->session->set_userdata('message', $data['message']);
                        $this->session->set_userdata('total_price', $data['total_price']);
						$this->session->set_userdata('frieght_cost', $data['frieght_cost']);
						$this->session->set_userdata('inspection_cost', $data['inspection_cost']);
                        redirect('/?c=user&m=public_login&contact_form=1');
                    }
                

                    
		}

    }


     function get_detailcar_send($car_title,$car_price_fob, $total_price, $car_image_show,$car_country, $idx, $frieght_cost, $inspection, $make,$model,$model_year,$chassis,$condition ,$mileage,$steering,$transmission,$fuel,$color,$manu_date,$reg_date){

        $html='<table style=" margin-top: 20px; width:660px"  border="0" valign="top" cellspacing="0" cellpadding="0">
			<tr>
        	<td style="width:210px;"><img src="'.$car_image_show.'" width="195" height="147" border="0"></td>
		
            <td>
            	<div style="text-align:center;min-height:22px;color: #0772ba;font-size:14px;">Price Break Down</div>
			
			
			<table  border="0" valign="top" cellspacing="0" cellpadding="0" style="width:225px;text-align:center;font-size:12px;padding-right:4px;">
			<tr>	
                <td style="background: #e6f1f8 none repeat scroll 0 0;padding-bottom: 4px;padding-top: 4px;text-align: center;">Fob Price</td>
			</tr>
			</table>
			
			<table  border="0" valign="top" cellspacing="0" cellpadding="0" style="width:225px;text-align:center;font-size:12px;padding-right:4px;">
			<tr>
                <td style="background: #fefefe none repeat scroll 0 0;padding-bottom: 4px;padding-top: 4px;text-align: center;">Freight Cost</td>
			</tr>
			</table>
			
			<table  border="0" valign="top" cellspacing="0" cellpadding="0" style="width:225px;text-align:center;font-size:12px;padding-right:4px;">
			<tr>
                <td style="background: #e6f1f8 none repeat scroll 0 0;padding-bottom: 4px;padding-top: 4px;text-align: center;">Inspection Fee</td>
			</tr>
			</table>
			
			
			<table  border="0" valign="top" cellspacing="0" cellpadding="0" style="width:225px;text-align:center;font-size:12px;padding-right:4px;">
			<tr>
                <td style="background: #fefefe none repeat scroll 0 0;padding-bottom: 4px;padding-top: 4px;text-align: center;">Total Cost</td>
            </tr>
			</table>
			</td>
			
			
            <td style="width:200px;text-align:center;font-size:12px;padding-left:4px;">
            	<div style="text-align:center;min-height:22px;color: #0772ba;font-size:14px;">Original Price</div>
			
			
			<table  border="0" valign="top" cellspacing="0" cellpadding="0" style="width:225px;text-align:center;font-size:12px;padding-right:4px;">
			<tr>
                <td style="background: #e6f1f8 none repeat scroll 0 0;padding-bottom: 4px;padding-top: 4px;text-align: center;">$ '.$car_price_fob.'</td>
            </tr>
			</table>
			
			<table  border="0" valign="top" cellspacing="0" cellpadding="0" style="width:225px;text-align:center;font-size:12px;padding-right:4px;">
			<tr>
				<td style="background: #fefefe none repeat scroll 0 0;padding-bottom: 4px;padding-top: 4px;text-align: center;">$ '.$frieght_cost.'</td>
			</tr>
			</table>
			
            <table  border="0" valign="top" cellspacing="0" cellpadding="0" style="width:225px;text-align:center;font-size:12px;padding-right:4px;">
			<tr>
				<td style="background: #e6f1f8 none repeat scroll 0 0;padding-bottom: 4px;padding-top: 4px;text-align: center;">$ '.$inspection.'</td>
			</tr>
			</table>
			
			
			<table  border="0" valign="top" cellspacing="0" cellpadding="0" style="width:225px;text-align:center;font-size:12px;padding-right:4px;">
			<tr>
                <td style="background: #fefefe none repeat scroll 0 0;padding-bottom: 4px;padding-top: 4px;text-align: center;color: #0772ba;">$ '.$total_price.'</td>
            </tr>
			</table>
			</td>
			</tr>
			</table>
			
			
			<table style=" margin-top: 20px; width:660px;"  border="0" valign="top" cellspacing="0" cellpadding="0">
            <tr>
            <td colspan="2" style="color: #0772ba;font-size: 14px;margin-bottom: 10px;padding-top: 22px;text-align: center;">Car\'s Specification</td>
			</tr>
			
            <tr style="width:100%;">
                <td style="width:330px;margin-right:5px;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:136px;padding-left:24px;background: #e6f1f8;padding-bottom: 4px;padding-top: 4px;">Stock ID</td>
						<td style="font-size:12px;width:136px;padding-right:24px;background: #e6f1f8;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$idx.'</td>
					</tr>
				</table>
				</td>
			
			
                <td style="width:330px;" colspan="2">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:138px;padding-left:24px;background: #e6f1f8;padding-bottom: 4px;padding-top: 4px;">Steering</td>
						<td style="font-size:12px;width:138px;padding-right:24px;background: #e6f1f8;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$steering.'</td>
					</tr>
				</table>
				</td>
			</tr>
				
			<tr style="width:100%;">
                <td style="width:330px;margin-right:5px;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:136px;padding-left:24px;background: #fefefe;padding-bottom: 4px;padding-top: 4px;">Condition</td>
						<td style="font-size:12px;width:136px;padding-right:24px;background: #fefefe;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$condition.'</td>
					</tr>
				</table>
				</td>

                <td style="width:330px;" colspan="2">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:138px;padding-left:24px;background: #fefefe;padding-bottom: 4px;padding-top: 4px;">Transmission</td>
						<td style="font-size:12px;width:138px;padding-right:24px;background: #fefefe;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$transmission.'</td>
                	</tr>
				</table>
				</td>
            </tr>
				
			<tr style="width:100%;">
                <td style="width:330px;margin-right:5px;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:136px;padding-left:24px;background: #e6f1f8;padding-bottom: 4px;padding-top: 4px;">Make</td>
						<td style="font-size:12px;width:136px;padding-right:24px;background: #e6f1f8;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$make.'</td>
                	</tr>
				</table>
				</td>

                <td style="width:330px;" colspan="2">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:138px;padding-left:24px;background: #e6f1f8;padding-bottom: 4px;padding-top: 4px;">Fuel Type</td>
						<td style="font-size:12px;width:138px;padding-right:24px;background: #e6f1f8;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$fuel.'</td>
                	</tr>
				</table>
				</td>
            </tr>
				
			<tr style="width:100%;">
                <td style="width:330px;margin-right:5px;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:136px;padding-left:24px;background: #fefefe;padding-bottom: 4px;padding-top: 4px;">Model</td>
						<td style="font-size:12px;width:136px;padding-right:24px;background: #fefefe;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$model.'</td>
                	</tr>
				</table>
				</td>

                <td style="width:330px;" colspan="2">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:138px;padding-left:24px;background: #fefefe;padding-bottom: 4px;padding-top: 4px;">Location</td>
						<td style="font-size:12px;width:138px;padding-right:24px;background: #fefefe;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$car_country.'</td>
                	</tr>
				</table>
				</td>
            </tr>
				
			<tr style="width:100%;">
                <td style="width:330px;margin-right:5px;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:136px;padding-left:24px;background: #e6f1f8;padding-bottom: 4px;padding-top: 4px;">Chassis No</td>
						<td style="font-size:12px;width:136px;padding-right:24px;background: #e6f1f8;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$chassis.'</td>
                	</tr>
				</table>
				</td>

                <td style="width:330px;" colspan="2">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:138px;padding-left:24px;background: #e6f1f8;padding-bottom: 4px;padding-top: 4px;">Exterior Color</td>
						<td style="font-size:12px;width:138px;padding-right:24px;background: #e6f1f8;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$color.'</td>
                	</tr>
				</table>
				</td>
            </tr>
				
			<tr style="width:100%;">
                <td style="width:330px;margin-right:5px;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:136px;padding-left:24px;background: #fefefe;padding-bottom: 4px;padding-top: 4px;">Model Year</td>
						<td style="font-size:12px;width:136px;padding-right:24px;background: #fefefe;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$model_year.'</td>
                	</tr>
				</table>
				</td>

                <td style="width:330px;" colspan="2">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:138px;padding-left:24px;background: #fefefe;padding-bottom: 4px;padding-top: 4px;">Manufactured Date</td>
						<td style="font-size:12px;width:138px;padding-right:24px;background: #fefefe;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$manu_date.'</td>
                	</tr>
				</table>
				</td>
            </tr>
				
			<tr style="width:100%;">
                <td style="width:330px;margin-right:5px;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:136px;padding-left:24px;background: #e6f1f8;padding-bottom: 4px;padding-top: 4px;">Mileage</td>
						<td style="font-size:12px;width:136px;padding-right:24px;background: #e6f1f8;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$mileage.'</td>
                	</tr>
				</table>
				</td>

                <td style="width:330px;" colspan="2">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="font-size:12px;width:138px;padding-left:24px;background: #e6f1f8;padding-bottom: 4px;padding-top: 4px;">Registration Date</td>
						<td style="font-size:12px;width:138px;padding-right:24px;background: #e6f1f8;text-align:right;padding-bottom: 4px;padding-top: 4px;">'.$reg_date.'</td>
                	</tr>
				</table>
				</td>
			</tr>
               
            </tr>
			
			</table>';
           
                return $html;

    }



    //function registerExec(){
//        $inData = func_get_post_array($this->input->post('values'));
//        $values = $this->input->post('values');
//
//        $mobile_no1 = $values['mobile_no1'];
//        $length = strpos($mobile_no1," ");
//        $firstspace = substr($mobile_no1,1,$length);
//        $inData['mobile_no1'] = $firstspace;
//
//        // $mobile_second=substr(str_replace(' ','',$mobile_no1),$length,$firstspace - $length);
//        $mobile_second=substr(preg_replace('/\D/','',$mobile_no1),$length - 1,$firstspace - ($length - 20));
//
//        $inData['mobile_no2'] = $mobile_second;
//
//        $inData['grade_no'] = 10;
//        $validation = $this->member->validateRegister($inData);
//        if($validation['success']==1){
//            $inData['member_pwd'] = $this->member->func_base64_encode($inData['member_pwd']);
//            $insert_id = $this->member->registerUser($inData);
//            if(!empty($insert_id)){
//                $verificationCode = $this->member->createVerificationCode($insert_id);
//                $email = $this->member->getUserEmail($insert_id);
//                $this->member->sendVerificationToEmail($email, $verificationCode, $inData['member_id'], $inData['member_first_name']." ".$inData['member_last_name']);
//                echo func_jsAlertReplace("Thank you for registration. Please check your email to verify your account", '/');
//            }
//        }else{
//            var_dump($validation);
//            //echo func_jsAlertReplace(implode(", ", $validation), '/?c=user&m=register');
//        }
//    }

	function registerExec(){
        	 /* $data["member_id"] = $this->input->get_post('member_id');
			  $data["email"] = $this->input->get_post('email');
			  $data["member_pwd"] = $this->input->get_post('member_pwd');
			  $data["member_first_name"] = $this->input->get_post('member_first_name');
			  $data["member_last_name"] = $this->input->get_post('member_last_name');
			  $data["member_country"] = $this->input->get_post('member_country');
              $data["phone_no1"] = $this->input->get_post('phone_no1');
              $data["consignee_address"] = $this->input->get_post('consignee_address');
              $data["company_name"] = $this->input->get_post('company_name');
              $data["contact_person_name"] = $this->input->get_post('contact_person_name');*/
              $allowed_ext = array('jpg', 'png', 'jpeg', 'gif');
              $data = $this->input->get_post('values');

              $data["member_pwd"] = func_base64_encode($data["member_pwd"]);

                $phone_no = $data['phone_no1'];
                $phone_no_arr = explode(" ", $phone_no);
                $data["phone_no1"] = $phone_no_arr[0];
                unset($phone_no_arr[0]);
                $data["phone_no2"] = implode('', $phone_no_arr);
                $data["phone_no1"] = preg_replace("/[^0-9]/","",$data["phone_no1"]);
                $data["phone_no2"] = preg_replace("/[^0-9]/","",$data["phone_no2"]);
                $data['draft'] = 'N';
                if(isset($_FILES["filePhoto"]["name"])){
                    $target_file = basename($_FILES["filePhoto"]["name"]);
                }else{
                     $target_file= '';
                }

                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

                // if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {

                //     echo func_jsAlertReplace('Sorry, only JPG, JPEG, PNG & GIF files are allowed.', '/?c=user&m=register');
                // }







              $result=$this->member->validateRegister($data);


              if($result['success']==1){
                    /*** CREATE VCODE ***/
                    
                    
			        $lastid = $this->member->insert_customer($data);
                    $query = $this->member->member_chk( $data["member_id"],  $data["member_pwd"]);
                    /*** SEND VERIFICATION CODE ***/
                    $verificationCode = $this->member->createVerificationCode($lastid);
                    $email = $this->member->getUserEmail($lastid);
                    $this->member->insertVerification($lastid, $verificationCode);
                    $this->sendVerificationToEmail($email, $data['member_id'], $verificationCode);

                    $row = $query->row();
                    /*** Upload Profile image ***/
                    if(in_array($imageFileType, $allowed_ext)) {
                        $data_update['member_id'] =  $row->member_id;
                        $data_update['profile_image_id'] = $this->_upload_to_cloud($row->member_no, 'filePhoto');
                        if($data_update['profile_image_id'])  {
                            $this->member->member_update($data_update);
                        }
                    }
                    
                    $this->_saveLoginSession(
                            $row->member_no,
                            $row->member_id,
                            $row->email,
                            $row->member_first_name,
                            $row->member_last_name,
                            $row->grade_no,
                            $row->grade_name,
                            $row->business_type,
                            $row->member_country);
				        redirect("/?c=user&m=complete");
              }else{
			  	      echo func_jsAlertReplace('Registration Failed!', '/?c=user&m=public_register');
			  }
			//var_dump($lastid);


    }

    function registerupdate(){
             $member_id="";
             $member_pwd ="";
             $member_first_name="";
             $member_last_name="";
             $member_country="";
             $phone_no1="";
            $count = 0;
              $current_url = $this->siteURL();
              $allowed_ext = array('jpg', 'png', 'jpeg', 'gif');
              $data = $this->input->get_post('values');
              $first_name=$data['member_first_name'];
              $email=$data['email'];
              $member_id=$_POST['values']['member_id'];
              $where=array();
              $where['verification']=$_POST['values']['verification'];
              //echo $where['verification']; exit();
                $data["member_pwd"] = func_base64_encode($data["member_pwd"]);
                $phone_no = $data['phone_no1'];
                $phone_no_arr = explode(" ", $phone_no);
                $data["phone_no1"] = $phone_no_arr[0];
                unset($phone_no_arr[0]);
                $data["phone_no2"] = implode('', $phone_no_arr);
                $data["phone_no1"] = preg_replace("/[^0-9]/","",$data["phone_no1"]);
                $data["phone_no2"] = preg_replace("/[^0-9]/","",$data["phone_no2"]);
                $data['draft'] = 'N';
                $data['verification']='';
                if(isset($_FILES["filePhoto"]["name"])){
                    $target_file = basename($_FILES["filePhoto"]["name"]);
                }else{
                     $target_file= '';
                }

                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                $result=$this->member->validateRegister($data);
                  //var_dump($result);exit();
              if($result['success']==1){   
                    $this->member->update_customer($data,$where);
                    $query = $this->member->member_chk( $data["verification"],  $data["member_pwd"]);
                    $login = $this->member->member_chk($member_id, $data["member_pwd"]);
                    $row = $login->row();
                /*** SEND CAR DETAIL TO USER MAIL***/
                    $car_detail = '';
                    $detail_html = '';
                    $results = $this->mo_bbs->get_car_send();
                    foreach ($results as $rows) {
                        $car_idx=$rows->idx;
                        $car_image=$rows->base_url.'c_fill,h_89,w_128/'.$rows->fileimgname;
                        if($car_image!=""){
                            $car_image_show=$rows->base_url.'c_fill,h_89,w_128/'.$rows->fileimgname;
                        }else{
                            $car_image_show=$rows->base_url.'c_fill,h_89,w_128/v1435029821/no_image_comzyj';
                        }
                        $car_price=$rows->car_fob_cost;
                        if($car_price!=""){
                           $car_price_fob=number_format($rows->car_fob_cost);
                        }else{
                            $car_price_fob="Ask";
                        }
                        $car_country=$rows->country;
                        if($car_country=="jp"){
                            $car_country_img=$rows->base_url."c_fill,h_25,w_25/v1435028554/logo/cn/ja";
                        }else{
                             $car_country_img=$rows->base_url."c_fill,h_25,w_25/v1435028555/logo/cn/ko";
                        }
                        $link = $current_url."/?c=user&m=showdetailcar&idx=".$car_idx;
                        $car_title=$rows->car_model_year.' '.$rows->car_make.' '.$rows->car_model;
                        $count ++; 
                        if($count%4==1){
                           $detail_html.='<tr>';
                        }
                          $detail_html.='<td style="border:none;width:0px;">
                              <div style="font-size:6px;color:#fff;">He</div>
                                <div style="width:137px; height:0x;background:#fff;">
                                       <div style="text-align:center;">
                                            <a href="'.$link.'" target="_blank">
                                                <img src="'.$car_image_show.'" style="height:89px;width:128px;"/>
                                            </a>
                                       </div>
                                       <div style="color: rgb(0, 90, 148); font-weight: bold;font-size: 14px; text-align: center;margin-top: 4px;height:35px;">'.$car_title.'</div><br>
                                       <table>
                                           <tr>
                                               <td style="width:100px; text-align:left;font-size: 15px;color#3d3d3d;">Country :</td>
                                               <td><img src="'.$car_country_img.'"></td>
                                           </tr>
                                       </table>
                                       <table>
                                           <tr>
                                               <td style="width:65px; text-align:left;font-size: 15px;color#3d3d3d;">FOB :</td>
                                               <td style="color: red;font-size: 15px;">USD '.$car_price.'</td>
                                           </tr>
                                       </table>
                                       <table style="margin-left:-3px;">
                                           <tr>
                                               <td>
                                                   <div style="width: 140px;margin-bottom: -8px;">
                                                   <img src="https://motorbb.com/email_template/img_car_list/contact_left.jpg"/><a href="'.$link.'"><img src="https://motorbb.com/email_template/img_car_list/contact_right.jpg"/></a>
                                                   </div>
                                               </td>
                                           </tr>
                                       </table>
                                </div>
                            </td>';
                        if($count%4==0){
                          $detail_html.='</tr>';
                        }
                    }

                    $link_detail=$current_url."/?c=user&m=car_list";       
                    $html = file_get_contents('https://motorbb.com/email_template/car_list.html', true);
                    $html = str_replace('[::CAR_DETAIL::]', $detail_html, $html);
                    $html = str_replace('[::EMAIL::]', $email, $html);
                    $html = str_replace('[::NAME::]', $first_name, $html);
                    $html = str_replace('[::LINKDETAIL::]', $link_detail, $html);
                   // echo $html; exit();
                    $this->mo_email->send_register_car_list($email, $html);
                    /*** END CAR DETAIL TO USER MAIL***/

                    /*** Upload Profile image ***/
                    if(in_array($imageFileType, $allowed_ext)) {
                        $data_update['verification'] =  $row->verification;
                        $data_update['profile_image_id'] = $this->_upload_to_cloud($row->member_no, 'filePhoto');
                        if($data_update['profile_image_id'])  {
                            $this->member->member_update($data_update);
                        }
                    }
                    $this->_saveLoginSession(
                            $row->member_no,
                            $row->member_id,
                            $row->email,
                            $row->member_first_name,
                            $row->member_last_name,
                            '11',
                            'User',
                            'buyer',
                            $row->member_country);
                    //$member_no, $member_id, $email, $member_first_name, $member_last_name, $grade_no, $grade_name,$bussines_type
                        redirect("/?c=user&m=complete");

              }else{
                echo func_jsAlertReplace('Registration Failed!', '/?c=user&m=enter_info_confirm&vcode='.$where['verification']);
              }
    }

        function mobile_registerupdate(){
             $member_id="";
             $member_pwd ="";
             $member_first_name="";
             $member_last_name="";
             $member_country="";
             $phone_no1="";
            $count = 0;
              $current_url = $this->siteURL();
              $allowed_ext = array('jpg', 'png', 'jpeg', 'gif');
              $data = $this->input->get_post('values');
             // var_dump($data);exit();
              $first_name=$data['member_first_name'];
              $email=$data['email'];
              $member_id=$_POST['values']['member_id'];
              $where=array();
              $where['verification']=$_POST['values']['verification'];
              //echo $where['verification']; exit();
                $data["member_pwd"] = func_base64_encode($data["member_pwd"]);
                $phone_no = $data['phone_no1'];
                $phone_no_arr = explode(" ", $phone_no);
                $data["phone_no1"] = $phone_no_arr[0];
               // echo $phone_no_arr[0]; exit();
                unset($phone_no_arr[0]);
                $data["phone_no2"] = implode('', $phone_no_arr);
                $data["phone_no1"] = preg_replace("/[^0-9]/","",$data["phone_no1"]);
                $data["phone_no2"] = preg_replace("/[^0-9]/","",$data["phone_no2"]);
                $data['draft'] = 'N';
                $data['verification']='';
                if(isset($_FILES["filePhoto"]["name"])){
                    $target_file = basename($_FILES["filePhoto"]["name"]);
                }else{
                     $target_file= '';
                }

                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                $result=$this->member->validateRegister($data);
                  //var_dump($result);exit();
              if($result['success']==1){   
                    $this->member->update_customer($data,$where);
                    $query = $this->member->member_chk( $data["verification"],  $data["member_pwd"]);
                    $login = $this->member->member_chk($member_id, $data["member_pwd"]);
                    $row = $login->row();
                /*** SEND CAR DETAIL TO USER MAIL***/
                    $car_detail = '';
                    $detail_html = '';
                    $results = $this->mo_bbs->get_car_send();
                    foreach ($results as $rows) {
                        $car_idx=$rows->idx;
                        $car_image=$rows->base_url.'c_fill,h_89,w_128/'.$rows->fileimgname;
                        if($car_image!=""){
                            $car_image_show=$rows->base_url.'c_fill,h_89,w_128/'.$rows->fileimgname;
                        }else{
                            $car_image_show=$rows->base_url.'c_fill,h_89,w_128/v1435029821/no_image_comzyj';
                        }
                        $car_price=$rows->car_fob_cost;
                        if($car_price!=""){
                           $car_price_fob=number_format($rows->car_fob_cost);
                        }else{
                            $car_price_fob="Ask";
                        }
                        $car_country=$rows->country;
                        if($car_country=="jp"){
                            $car_country_img=$rows->base_url."c_fill,h_25,w_25/v1435028554/logo/cn/ja";
                        }else{
                             $car_country_img=$rows->base_url."c_fill,h_25,w_25/v1435028555/logo/cn/ko";
                        }
                        $link = $current_url."/?c=user&m=showdetailcar&idx=".$car_idx;
                        $car_title=$rows->car_model_year.' '.$rows->car_make.' '.$rows->car_model;
                        $count ++; 
                        if($count%4==1){
                           $detail_html.='<tr>';
                        }
                          $detail_html.='<td style="border:none;width:0px;">
                              <div style="font-size:6px;color:#fff;">He</div>
                                <div style="width:137px; height:0x;background:#fff;">
                                       <div style="text-align:center;">
                                            <a href="'.$link.'" target="_blank">
                                                <img src="'.$car_image_show.'" style="height:89px;width:128px;"/>
                                            </a>
                                       </div>
                                       <div style="color: rgb(0, 90, 148); font-weight: bold;font-size: 14px; text-align: center;margin-top: 4px;height:35px;">'.$car_title.'</div><br>
                                       <table>
                                           <tr>
                                               <td style="width:100px; text-align:left;font-size: 15px;color#3d3d3d;">Country :</td>
                                               <td><img src="'.$car_country_img.'"></td>
                                           </tr>
                                       </table>
                                       <table>
                                           <tr>
                                               <td style="width:65px; text-align:left;font-size: 15px;color#3d3d3d;">FOB :</td>
                                               <td style="color: red;font-size: 15px;">USD '.$car_price.'</td>
                                           </tr>
                                       </table>
                                       <table style="margin-left:-3px;">
                                           <tr>
                                               <td>
                                                   <div style="width: 140px;margin-bottom: -8px;">
                                                   <img src="https://motorbb.com/email_template/img_car_list/contact_left.jpg"/><a href="'.$link.'"><img src="https://motorbb.com/email_template/img_car_list/contact_right.jpg"/></a>
                                                   </div>
                                               </td>
                                           </tr>
                                       </table>
                                </div>
                            </td>';
                        if($count%4==0){
                          $detail_html.='</tr>';
                        }
                    }

                    $link_detail=$current_url."/?c=user&m=car_list";       
                    $html = file_get_contents('https://motorbb.com/email_template/car_list.html', true);
                    $html = str_replace('[::CAR_DETAIL::]', $detail_html, $html);
                    $html = str_replace('[::EMAIL::]', $email, $html);
                    $html = str_replace('[::NAME::]', $first_name, $html);
                    $html = str_replace('[::LINKDETAIL::]', $link_detail, $html);
                   // echo $html; exit();
                    $this->mo_email->send_register_car_list($email, $html);
                    /*** END CAR DETAIL TO USER MAIL***/

                    /*** Upload Profile image ***/
                    if(in_array($imageFileType, $allowed_ext)) {
                        $data_update['verification'] =  $row->verification;
                        $data_update['profile_image_id'] = $this->_upload_to_cloud($row->member_no, 'filePhoto');
                        if($data_update['profile_image_id'])  {
                            $this->member->member_update($data_update);
                        }
                    }
                    $this->_saveLoginSession(
                            $row->member_no,
                            $row->member_id,
                            $row->email,
                            $row->member_first_name,
                            $row->member_last_name,
                            '11',
                            'User',
                            'buyer',
                            $row->member_country);

                        redirect("/?c=user&m=mobile_complete");

              }else{
                      echo func_jsAlertReplace('Registration Failed!', '/?c=user&m=mobile_enter_info&vcode='.$where['verification']);
              }
    }
    function verifyAccount(){
        $vcode = $this->input->get_post('vcode');
        if($this->member->existVerificationCode($vcode)){
            $this->member->verifyAccount($vcode);
            echo func_jsAlertReplace('Account verification completed. You may login now.', './');
        }else{
            echo func_jsAlertReplace('Verification code is invalid. Account verification failed.', './');
        }
        //echo $vcode;
    }

    // function register_old(){

    //     func_set_data($this, 'html_header', "");
    //     func_set_data($this, 'html_footer', "");

    //     $contents_area = $this->load->view('user/member/register', $this->data, true);
    //     func_set_data($this, 'contents_area', $contents_area);

    //     $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
    // }

    function resetpassword(){
         func_set_data($this, 'html_header', "");
         func_set_data($this, 'html_footer', "");
        $contents_area = $this->load->view('user/member/resetpassword', $this->data, true);
        func_set_data($this, 'contents_area', $contents_area);

        $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
    }

     function resendid(){
         func_set_data($this, 'html_header', "");
         func_set_data($this, 'html_footer', "");
        $contents_area = $this->load->view('user/member/resendid', $this->data, true);
        func_set_data($this, 'contents_area', $contents_area);

        $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
    }

    function email_resetpassword(){
        $current_url=$this->siteURL();
        $forgotemail = $_POST['forgotemail'];
        $email['email']=$forgotemail;
        $result=$this->member->validate_email_Register($email);
        if($result['success']==0){
            $reseCode=$this->member->random_reset_password($forgotemail);
            $data=array('reset_pass'=>$reseCode);
            $user_id=  $this->member->checkUserID($forgotemail);
            foreach ($user_id as $row){
             $member_id=$row->member_id;
            }
            $html = '<html><head></head><body>';
            $html.= '<br/><br/>
            		Your have resently reset password with your email at Minhas Trading.<br/>
            		Your Member ID: '.$member_id.'<br/><br/>Click the link below to set new password for your account.<br/>
            		<a href="'.$current_url.'/?c=user&m=reset_password&vcode='.$reseCode.'"/>'.$current_url.'/?c=user&m=reset_password&vcode='.$reseCode.'</a><br/><br/>
         			<br/><br/>
            		Thank you.';
            $html.= '</body></html>';
            $this->member->update_password_exc($forgotemail,$data);
            $this->mo_email->sendResetPassword($forgotemail,$html);
            $s_data = array('s_email' => $forgotemail);
            $set_email=$this->session->set_userdata($s_data);
            redirect("/?c=user&m=reset_success",'');
        }else{
             echo func_jsAlertReplace('Your email not register with Minhas Trading!','/?c=user&m=forget_password');
        }

    }
    function email_resend_password(){
        $current_url=$this->siteURL();
        $forgotemail = $_POST['resend_email'];
        $email['email']=$forgotemail;
        $result=$this->member->validate_email_Register($email);
        if($result['success']==0){
            $reseCode=$this->member->random_reset_password($forgotemail);
            $data=array('reset_pass'=>$reseCode);
            $user_id=  $this->member->checkUserID($forgotemail);
            foreach ($user_id as $row){
             $member_id=$row->member_id;
            }
            $html = '<html><head></head><body>';
            $html.= '<br/><br/>
                    Your have resently reset password with your email at Minhas Trading.<br/>
                    Your Member ID: '.$member_id.'<br/><br/>Click the link below to set new password for your account.<br/>
                    <a href="'.$current_url.'/?c=user&m=reset_password&vcode='.$reseCode.'"/>'.$current_url.'/?c=user&m=reset_password&vcode='.$reseCode.'</a><br/><br/>
                    <br/><br/>
                    Thank you.';
            $html.= '</body></html>';
            $this->member->update_password_exc($forgotemail,$data);
            $this->mo_email->sendResetPassword($forgotemail,$html);
            $s_data = array('s_email' => $forgotemail);
            $set_email=$this->session->set_userdata($s_data);
            echo func_jsAlertReplace('Success! Your ID has been sent to your email.','/');
        }else{
             echo func_jsAlertReplace('Your email not register with Minhas Trading!','/?c=user&m=forget_password');
        }

    }

    function reset_password_change(){
         $resetpass = $this->input->get_post('vcode');
         $password = $this->input->get_post('password');
         $data['member_pwd']=func_base64_encode($password);
         if (isset($_POST["bnt_reset"])){
            $vcode= $this->member->existVerifyPassword($resetpass);
            if ($vcode==true){
                $data_update=array('member_pwd'=>$data['member_pwd']);
                $this->member->reset_password_update($data_update,$resetpass);
                redirect('/?c=user&m=reset_success_change','');
            }
        }
    }

     function mobile_email_resetpassword(){
        $current_url=$this->siteURL();
        $forgotemail = $_POST['forgotemail'];
        $email['email']=$forgotemail;
        $result=$this->member->validate_email_Register($email);
        if($result['success']==0){
            $reseCode=$this->member->random_reset_password($forgotemail);
            $data=array('reset_pass'=>$reseCode);
            $user_id=  $this->member->checkUserID($forgotemail);
            foreach ($user_id as $row){
             $member_id=$row->member_id;
            }
            $html = '<html><head></head><body>';
            $html.= '<br/><br/>
                    Your have resently reset password with your email at Minhas Trading.<br/>
                    Your Member ID: '.$member_id.'<br/><br/>Click the link below to set new password for your account.<br/>
                    <a href="'.$current_url.'/?c=user&m=mobile_reset_password&vcode='.$reseCode.'"/>'.$current_url.'/?c=user&m=mobile_reset_password&vcode='.$reseCode.'</a><br/><br/>
                    <br/><br/>
                    Thank you.';
            $html.= '</body></html>';
            $this->member->update_password_exc($forgotemail,$data);
            $this->mo_email->sendResetPassword($forgotemail,$html);
            $s_data = array('s_email' => $forgotemail);
            $set_email=$this->session->set_userdata($s_data);
            redirect("/?c=user&m=mobile_reset_success",'');
        }else{
             echo func_jsAlertReplace('Your email not register with Minhas Trading!','/?c=user&m=mobile_forget_password');
        }

    }
    function mobile_email_resend_password(){
        $current_url=$this->siteURL();
        $forgotemail = $_POST['resend_email'];
        $email['email']=$forgotemail;
        $result=$this->member->validate_email_Register($email);
        if($result['success']==0){
            $reseCode=$this->member->random_reset_password($forgotemail);
            $data=array('reset_pass'=>$reseCode);
            $user_id=  $this->member->checkUserID($forgotemail);
            foreach ($user_id as $row){
             $member_id=$row->member_id;
            }
            $html = '<html><head></head><body>';
            $html.= '<br/><br/>
                    Your have resently reset password with your email at Minhas Trading.<br/>
                    Your Member ID: '.$member_id.'<br/><br/>Click the link below to set new password for your account.<br/>
                    <a href="'.$current_url.'/?c=user&m=mobile_reset_password&vcode='.$reseCode.'"/>'.$current_url.'/?c=user&m=mobile_reset_password&vcode='.$reseCode.'</a><br/><br/>
                    <br/><br/>
                    Thank you.';
            $html.= '</body></html>';
            $this->member->update_password_exc($forgotemail,$data);
            $this->mo_email->sendResetPassword($forgotemail,$html);
            $s_data = array('s_email' => $forgotemail);
            $set_email=$this->session->set_userdata($s_data);
            echo func_jsAlertReplace('Success! Your ID has been sent to your email.','/?c=user&m=mobile_home');
        }else{
            echo func_jsAlertReplace('Your email not register with Minhas Trading!','/?c=user&m=mobile_forget_password');
        }

    }

    function mobile_reset_password_change(){
         $resetpass = $this->input->get_post('vcode');
         $password = $this->input->get_post('password');
         $data['member_pwd']=func_base64_encode($password);
         if (isset($_POST["bnt_reset"])){
            $vcode= $this->member->existVerifyPassword($resetpass);
            if ($vcode==true){
                $data_update=array('member_pwd'=>$data['member_pwd']);
                $this->member->reset_password_update($data_update,$resetpass);
                redirect('/?c=user&m=mobile_reset_success_change','');
            }
        }
    }


    function email_resendid(){
         $forgotemail = $_POST['forgotemail'];
        $reseCode=$this->member->random_reset_password($forgotemail);

        $user_id=  $this->member->checkUserID($forgotemail);
        foreach ($user_id as $row){
            $member_id=$row->member_id;

        }

        $data=array('reset_pass'=>$reseCode);

        //$item_link="http://iblau.com/";

        require 'libraries/PHPMailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'ssl://kangaroo.arvixe.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@iblauda.com';                 // SMTP username
        $mail->Password = '#Kx4Qh_]})eh';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        $mail->From = 'noreply@iblauda.com';
        $mail->FromName = 'IBLAUDA';
        //$mail->addAddress('aslam0919@blauda.com', 'IBLAUDA');     // Add a recipient
        $mail->addAddress($forgotemail, 'IBLAUDA');     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo($forgotemail,$forgotemail);
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML
       $html = '<html><head></head><body>';
        $html.= '<br/><br/>
        		Hello! You have recently requested for forgotten ID at iblauda.com<br/>
        		Your Member ID: '.$member_id.'<br/><br/><br/>
        		<br/><br/>
     			<br/><br/>
        		Thank you.';

		$mail->Subject = 'IBLAUDA.COM: Forget ID';
		$mail->Body    = $html;


        $html.= '</body></html>';
         //echo $html;


         //exit();
        if(!$mail->send()) {
           echo func_jsAlertReplace('Mailer Error:', '/?c=mobile_user&m=resetpassword');
       } else {
           $this->member->update_password_exc($forgotemail,$data);

                echo func_jsAlertReplace(' Success! Your ID has been sent to your email.','./');


        }
    }



    function confirmpassword(){
          $resetpass = $this->input->get_post('vcode');
          if($this->member->existVerifyPassword($resetpass)){
          $this->_load_view_layout('user/member/confirmpassword');
         }
        else {
                    echo func_jsAlertReplace('Verification code is invalid. Account verification failed.', './');
        }

    }


    function verifyPassword(){
             $resetpass = $this->input->get_post('vcode');
            if($this->member->existVerifyPassword($resetpass)){

              // $this->member->changePassword_exc($resetpass);
              //echo func_jsAlertReplace('password verification completed. You may change  now.', './?c=user&mcd=conform'.$resetpass);


            }
                else{
              echo func_jsAlertReplace('Verification code is invalid. Account verification failed.', './');
                    // redirect('/?c=user&mcd', 'refresh');


            }

            //return $resetpass;

    }



    function update_password_verify(){

        $recode = $this->input->get_post('mem_email');
        $newpass=func_base64_encode($this->input->post('newpass'));
        $conformpass=func_base64_encode($this->input->post('conformpass'));
                if($this->member->existVerifyPassword($recode)){
                    if($newpass !=$conformpass){
                     echo func_jsAlertReplace('Your Current password not match.', './');
                    exit('</center></h1>Your Current password not match.</center></h1>');
                }
             else {
                  $this->member->verifyPassword_update($newpass,$conformpass,$recode);
             }
         }
          else {
               echo func_jsAlertReplace('please check email to link modify your password.', './');
          }


    }

    // function reservedlist(){
    //     if ($this->is_login == 'Y') {
    //         $where=array();
    //         //Get model rows
    //         $result = $this->mo_bbs->getReserved();
    //         $result1 = $this->mo_bbs->getOrder();
    //         //Save rows to date
    //         func_set_data($this, 'reserved_rows', $result);
    //         func_set_data($this, 'ordered_rows', $result1);

    //         func_set_data($this, 'html_header', "");
    //         func_set_data($this, 'html_footer', "");
    //         $contents_area = $this->load->view('user/bbs/ordered/list', $this->data, true);
    //         func_set_data($this, 'contents_area', $contents_area);
    //         $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
    //     }else{
    //         echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
    //     }
    // }

    // function orderedlist(){
    //     if ($this->is_login == 'Y') {
    //         $where=array();
    //         //Get model rows
    //         $result = $this->mo_bbs->getOrders(0, 5, $where);
    //         //Save rows to date
    //         func_set_data($this, 'ordered_rows', $result);

    //         func_set_data($this, 'html_header', "");
    //         func_set_data($this, 'html_footer', "");
    //         $contents_area = $this->load->view('user/bbs/ordered/lists', $this->data, true);
    //         func_set_data($this, 'contents_area', $contents_area);
    //         $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
    //     }else{
    //         echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
    //     }
    // }

    public function getImage(){

        //Get model rows
        $gets = $_GET;
        $idx = 0;

        unset($gets['c']);
        unset($gets['m']);


        $result = $this->mo_bbs->getImage($gets);
        foreach ($result as $key=>$value) {
            $result[$key]->image_name ="http://".$_SERVER['HTTP_HOST']."/uploads/product/".$value->raw_name."_150".$value->file_ext;
        }
        //Save rows to data
        func_set_data($this, 'rows', $result);
        //Get content html
        $this->load->view('user/json/getImage', $this->data, false);
    }
    function orderCarList(){
        if ($this->is_login == 'Y') {
            $this->load->helper('url');
            //pagination init
            $this->load->library('pagination');
            //pagination end init
            $where = array();

            //pagination process
            $total_rows = $this->mo_bbs->count_orders($where);
            $current_url = current_url()."?c=user&m=orderCarList";
            $per_page = $this->input->get_post("per_page");


            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
            $config['num_links'] = 5;
            $config['base_url'] = $current_url;
            $config['total_rows'] = $total_rows;
            $config['per_page'] = 15;

            $item_perpage = $config['per_page'];
            $total_page =ceil($total_rows/$item_perpage);

            $cur_page =$per_page;
            if ($cur_page == NULL || $cur_page == ''){
                $cur_page = 1;
            }elseif ($cur_page>2) {
                $cur_page = ceil($cur_page/$item_perpage+1);
            }

            $this->pagination->initialize($config);
            $pagination = $this->pagination->create_links();
            $limit_s = $per_page;
            $limit_e = $config['per_page'];
            //pagination end process

            $order_list_query = $this->mo_bbs->getOrders($limit_s, $limit_e, $where);

            $order_list = $order_list_query;
            func_set_data($this, 'total_rows', $total_rows);
            func_set_data($this, 'cur_page', $cur_page);
            func_set_data($this, 'total_page', $total_page);

            func_set_data($this, 'pagination', $pagination);
            func_set_data($this, 'ordered_rows', $order_list);

            func_set_data($this, 'html_header', "");
            func_set_data($this, 'html_footer', "");
            $contents_area = $this->load->view('user/bbs/ordered/lists', $this->data, true);
            func_set_data($this, 'contents_area', $contents_area);
            $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
        }else{
            echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
        }
    }

    function reservedlist(){
        if ($this->is_login == 'Y') {
            $this->load->helper('url');
            //pagination init
            $this->load->library('pagination');
            //pagination end init
            $where = array();

            //pagination process
            $total_rows = $this->mo_bbs->count_reserved($where);
            $current_url = current_url()."?c=user&m=reservedlist";
            $per_page = $this->input->get_post("per_page");

            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
            $config['num_links'] = 5;
            $config['base_url'] = $current_url;
            $config['total_rows'] = $total_rows;
            $config['per_page'] = 15;

            $item_perpage = $config['per_page'];
            $total_page =ceil($total_rows/$item_perpage);

            $cur_page =$per_page;
            if ($cur_page == NULL || $cur_page == ''){
                $cur_page = 1;
            }elseif ($cur_page>2) {
                $cur_page = ceil($cur_page/$item_perpage+1);
            }

            $this->pagination->initialize($config);
            $pagination = $this->pagination->create_links();
            $limit_s = $per_page;
            $limit_e = $config['per_page'];
            //pagination end process

            $order_list_query = $this->mo_bbs->getOrders($limit_s, $limit_e, $where);

            $order_list = $order_list_query;

            func_set_data($this, 'total_rows', $total_rows);
            func_set_data($this, 'cur_page', $cur_page);
            func_set_data($this, 'total_page', $total_page);

            func_set_data($this, 'pagination', $pagination);

            //Get model rows
            $result = $this->mo_bbs->getReserved($limit_s, $limit_e, $where);
            $result1 = $this->mo_bbs->getOrder();
            //Save rows to date
            func_set_data($this, 'reserved_rows', $result);
            func_set_data($this, 'ordered_rows', $result1);

            func_set_data($this, 'html_header', "");
            func_set_data($this, 'html_footer', "");
            $contents_area = $this->load->view('user/bbs/ordered/list', $this->data, true);
            func_set_data($this, 'contents_area', $contents_area);
            $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
        }else{
            echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
        }
    }
    // function usermessages(){
    //     if ($this->is_login == 'Y') {
    //         $where=array();
    //         //Get model rows
    //         $result = $this->mo_message->getSendMessages();
    //         //Save rows to date
    //         func_set_data($this, 'rows', $result);

    //         func_set_data($this, 'html_header', "");
    //         func_set_data($this, 'html_footer', "");
    //         $contents_area = $this->load->view('user/messages/send_list', $this->data, true);
    //         func_set_data($this, 'contents_area', $contents_area);
    //         $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
    //     }else{
    //         echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
    //     }
    // }
    // pagination for send messages
    function usermessages(){
        if ($this->is_login == 'Y') {
            $this->load->helper('url');
            //pagination init
            $this->load->library('pagination');
            //pagination end init
            $where = array();

            //pagination process
            $total_rows = $this->mo_message->count_SendMessages($where);
            $current_url = current_url()."?c=user&m=usermessages";
            $per_page = $this->input->get_post("per_page");


            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
            $config['num_links'] = 5;
            $config['base_url'] = $current_url;
            $config['total_rows'] = $total_rows;
            $config['per_page'] = 10;

            $item_perpage = $config['per_page'];
            $total_page =ceil($total_rows/$item_perpage);

            $cur_page =$per_page;
            if ($cur_page == NULL || $cur_page == ''){
                $cur_page = 1;
            }elseif ($cur_page>2) {
                $cur_page = ceil($cur_page/$item_perpage+1);
            }

            $this->pagination->initialize($config);
            $pagination = $this->pagination->create_links();
            $limit_s = $per_page;
            $limit_e = $config['per_page'];
            //pagination end process

            $order_list_query = $this->mo_message->getSendMessages($limit_s, $limit_e, $where);

            $message_list = $order_list_query;
            func_set_data($this, 'total_rows', $total_rows);
            func_set_data($this, 'cur_page', $cur_page);
            func_set_data($this, 'total_page', $total_page);

            func_set_data($this, 'pagination', $pagination);
            func_set_data($this, 'rows', $message_list);

            func_set_data($this, 'html_header', "");
            func_set_data($this, 'html_footer', "");
            $contents_area = $this->load->view('user/messages/send_list', $this->data, true);
            func_set_data($this, 'contents_area', $contents_area);
            $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
        }else{
            //echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
        }
    }
    //*****************************************
    function usermessagesdetail(){
        $where=array();
        $where['id'] = $_REQUEST['id'];
        if ($this->is_login == 'Y') {

            //Get model rows
            $result = $this->mo_message->getSendMessageDetail($where);
            //Save rows to date
            func_set_data($this, 'rows', $result);
            func_set_data($this, 'html_header', "");
            func_set_data($this, 'html_footer', "");
            if(count($result)>0){
                $this->mo_message->updateReadDate($where);

                $contents_area = $this->load->view('user/messages/send_list_detail', $this->data, true);
                func_set_data($this, 'contents_area', $contents_area);
            }else{
                func_set_data($this, 'contents_area', "The message does not exist");
            }
            $this->load->view('user/layout/SUB_LAYOUT_001', $this->data, false);
        }else{
            echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
        }
    }

    function deleteusermessage() {
        if ($this->is_login == 'Y') {
            $data = $this->input->post('id');
            $where_in = array_values($data);
            $this->mo_message->deleteMessage($where_in);
            redirect('./?c=user&m=usermessages', 'refresh');
        }else{
            echo func_jsAlertReplace('You need to login first to access this page', '/?c=user&m=login');
        }
    }
    function order_this_car_popup(){
        if ($this->is_login == 'Y') {
            $this->adm_popup->order_this_car_popup();
         }else{
            $this->adm_popup->order_this_car_popup_checklogin();
        }

    }
    // function emails_productinquiry(){

    //     $buyer_email = $_POST['buyer_email'];
    //     $message     = $_POST['message'];
    //     $item_name   = $_POST['item_name'];
    //     $item_link   = $_POST['item_link'];
    //     $item_image  = $_POST['item_image'];

    //     require 'libraries/PHPMailer/PHPMailerAutoload.php';

    //     $mail = new PHPMailer;

    //     //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    //     $mail->isSMTP();                                      // Set mailer to use SMTP
    //     $mail->Host = 'ssl://kangaroo.arvixe.com';  // Specify main and backup SMTP servers
    //     $mail->SMTPAuth = true;                               // Enable SMTP authentication
    //     $mail->Username = 'noreply@iblauda.com';                 // SMTP username
    //     $mail->Password = '#Kx4Qh_]})eh';                           // SMTP password
    //     $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    //     $mail->Port = 465;                                    // TCP port to connect to

    //     $mail->From = 'noreply@iblauda.com';
    //     $mail->FromName = 'IBLAUDA';
    //     $mail->addAddress('aslam0919@blauda.com', 'IBLAUDA');     // Add a recipient
    //     // $mail->addAddress('ellen@example.com');               // Name is optional
    //     $mail->addReplyTo($buyer_email, $buyer_email);
    //     // $mail->addCC('cc@example.com');
    //     // $mail->addBCC('bcc@example.com');

    //     // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //     // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    //     $mail->isHTML(true);                                  // Set email format to HTML
    //     $html = '<html><head></head><body>';
    //     $html.= $message.'<br/><br/><a href="http://iblauda.com/?c=user&mcd=product&me=bbs_detail&idx='.$item_link.'"><img src="'.$item_image.'"></a><br><b>Link to the car: </b><a href="http://iblauda.com/?c=user&mcd=product&me=bbs_detail&idx='.$item_link.'">'.$item_name.'</a>';

    //     $mail->Subject = "Customer's Product Inquiry";
    //     $mail->Body    = $html;


    //     $html.= '</body></html>';
    //     // echo $html;
    //     // exit();
    //     if(!$mail->send()) {
    //         echo func_jsAlertReplace('Mailer Error:', '/?c=user&mcd=product&me=bbs_detail&idx='.$item_link);
    //     } else {
    //         echo func_jsAlertReplace('Message has been sent.', '/?c=user&mcd=product&me=bbs_detail&idx='.$item_link);
    //     }
    // }

    function Facebook()
    {
        $config = array("base_url" => "https://iblauda.com/libraries/hybridauth/",
            "providers" => array (

                "Facebook" => array (
                    "enabled" => true,
                    "keys"    => array ( "id" => "432111436966513", "secret" => "d01457281e704c958bba42e04f37e457" ),
                    "trustForwarded" => false,
                    "scope" => "email, user_about_me, user_birthday, user_hometown"  //optional.
                ),
            ),
            // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
            "debug_mode" => false,
            "debug_file" => "debug.log",
        );

        if(isset($_GET['m']) && $_GET['m']='Facebook')
        {
        $provider = $_GET['m'];
        $type = 'fb';
        try{
            $hybridauth = new Hybrid_Auth( $config );
            $authProvider = $hybridauth->authenticate($provider);
            $user_profile = $authProvider->getUserProfile();
            if($user_profile && isset($user_profile->identifier))
            {
                $member_id = $type.$user_profile->identifier;

                $inData['member_id']        =   $member_id;
                $inData['member_first_name']=   $user_profile->firstName;
                $inData['member_last_name'] =   $user_profile->lastName;
                $inData['grade_no']         =   11;
                $inData['phone_no1']        =   $user_profile->phone;
                $inData['email']            =   $user_profile->email;
                $inData['address']          =   $user_profile->address;
                $inData['sex']              =   $user_profile->gender=="male" ? "M" : "F";
                $inData['s_id']             =   $user_profile->identifier;
                $inData['provider']         =   $_GET['m'];

                $query = $this->member->social_member_chk($member_id);

                if ($query->num_rows() < 1) {

                    $insert_id = $this->member->registerUser($inData);

                    $query = $this->member->social_member_chk($member_id);
                }

                    $row = $query->row();

                    /*** Save Login session ***/
                    $this->_saveLoginSession(
                        $row->member_no,
                        $row->member_id,
                        $row->email,
                        $row->member_first_name,
                        $row->member_last_name,
                        $row->grade_no,
                        $row->grade_name,
                        $row->business_type,
                        $row->member_country);

                    //로그인 COUNT UPDATE

                    $this->member->social_login_count_update($member_id);
                    //로그인 DATE UPDATE
                    $this->member->social_login_date_update(func_get_date_time(), $member_id);
            }

            }
            catch( Exception $e )
            {
                 switch( $e->getCode() )
                 {
                        case 0 : echo "Unspecified error."; break;
                        case 1 : echo "Hybridauth configuration error."; break;
                        case 2 : echo "Provider not properly configured."; break;
                        case 3 : echo "Unknown or disabled provider."; break;
                        case 4 : echo "Missing provider application credentials."; break;
                        case 5 : echo "Authentication failed The user has canceled the authentication or the provider refused the connection.";
                                 break;
                        case 6 : echo "User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.";
                                 $authProvider->logout();
                                 break;
                        case 7 : echo "User not connected to the provider.";
                                 $authProvider->logout();
                                 break;
                        case 8 : echo "Provider does not support this feature."; break;
                }

                echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();

                echo "<hr /><h3>Trace</h3> <pre>" . $e->getTraceAsString() . "</pre>";

            }
        }
        if(isset($_GET['redi_rect']) && $_GET['redirect'] !=''){
            redirect('/', 'refresh');
        }
    }

    function get_component_center_search(){
        $this->load->view('user/config/componentsearch', $this->data, false);
    }


   function carlist(){



        //====================== Pagination ==============//
            $per_page = 20;

            if(isset($_GET["page"])){

                if ($_GET["page"] != 0) {

                    $page=$_GET["page"];
                }else{
                    $page=1;
                }

            }else{
                $page=1;
            }


            $start_page=($page-1)*$per_page;
        $model_list = array();
        //====================== Pagination ==============//

        $queryString = "";

            if(isset($_POST["searchboxcar"])){

                $carmake = $this->db->escape_str($_POST["carmake"]);
                $carmodel = $this->db->escape_str($_POST["carmodel"]);
                $keyword = $this->db->escape_str($_POST["keyword"]);
                $carminyear = $this->db->escape_str($_POST["searchminyear"]);
                $carminprice =$this->db->escape_str($_POST["searchminprice"]);
                $carcountry = $this->db->escape_str($_POST["searchcountry"]);
                $carmaxyear = $this->db->escape_str($_POST["searchmaxyear"]);
                $carmaxrpice = $this->db->escape_str($_POST["searchmaxprice"]);


                $datasa = array('carmake'=> $carmake,
                              'carmodel'=> $carmodel,
                              'keyword'=> $keyword,
                              'carminyear'=> $carminyear,
                              'carminprice'=> $carminprice,
                              'carcountry'=> $carcountry,
                              'carmaxyear'=> $carmaxyear,
                              'carmaxrpice'=> $carmaxrpice
                              );
                $queryString =  http_build_query($datasa);

                $link = "/?c=user&m=carlist&".$queryString."&page=1";

                //die("<script>History.pushState(null, $link);");
                die("<script>location.href = '".$link. "';</script>");
               

          

            }else if(isset($_GET["carmake"]) and isset($_GET["carmodel"]) and isset($_GET["keyword"]) and isset($_GET["carminyear"]) and isset($_GET["carminprice"]) and isset($_GET["carcountry"]) and isset($_GET["carmaxyear"]) and isset($_GET["carmaxrpice"])){


                $carmake = $this->db->escape_str($_GET["carmake"]);
                $carmodel = $this->db->escape_str($_GET["carmodel"]);
                $keyword = $this->db->escape_str($_GET["keyword"]);
                $carminyear = $this->db->escape_str($_GET["carminyear"]);
                $carminprice =$this->db->escape_str($_GET["carminprice"]);
                $carcountry = $this->db->escape_str($_GET["carcountry"]);
                $carmaxyear = $this->db->escape_str($_GET["carmaxyear"]);
                $carmaxrpice = $this->db->escape_str($_GET["carmaxrpice"]);

                $carlisttitle = "TOTAL SEARCH CAR";

                $result = $this->mo_bbs->searchboxcar($carmake,$carmodel,$keyword,$carminyear,$carminprice,$carcountry,$carmaxyear,$carmaxrpice,$start_page,$per_page);
                $countsearchboxcars = $this->mo_bbs->countsearchboxcar($carmake,$carmodel,$keyword,$carminyear,$carminprice,$carcountry,$carmaxyear,$carmaxrpice);
                $total_num_row = $countsearchboxcars[0]->countsearhbox;
                
                $where_model['car_make']=$carmake;
                $model_list = $this->mo_bbs->getModel($where_model, true);


            }else if(isset($_GET["car"])){

                $result = $this->mo_bbs->get_public_premium_car(array(),$per_page,$start_page);

                $total_num_row = $this->mo_bbs->get_total_premium_car($where=array());
            
                $carlisttitle = "More Popular Car";


            }else{

			
    			if(isset($_GET["car_make"])){
    				$carlisttitle=$_GET["car_make"];
    			}
    			else if(isset($_GET["car_body_type"])){
    				$carlisttitle=$_GET["car_body_type"];
    			}
    			else if (isset($_GET["country"])){
    				$carlisttitle = "SEARCH BY COUNTRY";
    			}else{
    				
    				$carlisttitle = "SEARCH BY CAR";
    				
    			}
			


                /***Get given search parameter from url***/
                $where['country'] = $this->input->get_post('country');
                $where['car_make'] = $this->input->get_post('car_make');
                $where['car_model'] = $this->input->get_post('car_model');
                $where['car_model_year'] = $this->input->get_post('car_model_year');
                $where['car_body_type'] = $this->input->get_post('car_body_type');
                $where['car_model_year >='] = $this->input->get_post('min_model_year');
                $where['car_model_year <='] = $this->input->get_post('max_model_year');
                $where['car_fob_cost >='] = $this->input->get_post('min_fob_cost');
                $where['car_fob_cost <='] = $this->input->get_post('max_fob_cost');
                $where['car_chassis_no LIKE'] = '%'.$this->input->get_post('keyword').'%';
                /*** Check for searching fields that is empty means user didn't input it ***/
                foreach($where as $key=>$value){
                    if(empty($value)){
                        unset($where[$key]);
                    }
                }

                /*** get total of rows for calculate pagination ***/
                $total_num_row = $this->mo_bbs->user_total_select($where);
                /*** Get car rows ... LIMIT 0, 20 ***/
                $result = $this->mo_bbs->user_select($where, '', $start_page, $per_page);
                /*** Test Output ***/
                //echo "Total Row: ".$total_num_row;

        }

        //=============== Set Title to car list page ============//
        
        
        func_set_data($this, 'model_list', $model_list);
        func_set_data($this, 'carlisttitle', $carlisttitle);
        //=============== Set Title to car list page ============//

        func_set_data($this, 'carlist', $result);
        func_set_data($this, 'countcarlist', $total_num_row);
        func_set_data($this, 'perpagepagination', $per_page);
        func_set_data($this, 'getnumpage', $page);
        $this->_load_view_layout_ajax('user/content/carlist');
        //var_dump($result);


       
   


    }
    
    function ceiling($number, $significance = 1)
    {
        return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
    }

    // function get_total_price(){
    //     $input['idx'] = $this->input->get_post('idx');
    //     $input['country_to'] = $this->input->get_post('country_to');
    //     $input['port_name']  = $this->input->get_post('port_name');
    //     $insurance  = $this->input->get_post('insurance');
    //     $inspection = $this->input->get_post('inspection');
    //     foreach($input as $key=>$value){
    //         if(!empty($value)){
    //             $where[$key]=$value;
    //         }
    //     }
    //     $row = $this->mo_bbs->calculate_total_price($where);
        
    //         if(empty($row[0]->num_rows) || empty($row[0]->car_fob_cost) || empty($row[0]->country_to) || empty($row[0]->car_width) || empty($row[0]->car_height) || empty($row[0]->car_length) )
    //         {
    //             echo "N/A";
    //         }else{

    //             $frieght_cost = $this->ceiling($row[0]->shipping_cost * ($row[0]->car_width/1000) * ($row[0]->car_height/1000) * ($row[0]->car_length/1000),50);

    //             if($insurance=='on' && $inspection=='on'){
                    
    //                 $total_price = $frieght_cost + $row[0]->insurance + $row[0]->inspection + $row[0]->car_fob_cost;
                   
    //             }elseif($insurance=='off' && $inspection=='off'){
                    
    //                 $total_price = $frieght_cost + $row[0]->car_fob_cost;
                
    //             }
    //             elseif ($insurance=='on' && $inspection=='off') {  

    //                 $total_price = $frieght_cost + $row[0]->insurance + $row[0]->car_fob_cost;
                
    //             }elseif($insurance=='off' && $inspection=='on'){

    //                 $total_price = $frieght_cost + $row[0]->inspection + $row[0]->car_fob_cost;
                
    //             }

    //             echo $row[0]->car_fob_currency." ".number_format($total_price);
                
    //         }
    // }

// function get_destination_port(){
//         $input['idx'] = $this->input->get_post('idx');
//         foreach($input as $key=>$value){
//             if(!empty($value)){
//                 $where[$key]=$value;
//             }
//         }
//         $row = $this->mo_bbs->calculate_total_price($where);
        
//             if(empty($row[0]->num_rows) || empty($row[0]->car_fob_cost) || empty($row[0]->country_to) || empty($row[0]->car_width) || empty($row[0]->car_height) || empty($row[0]->car_length) )
//             {
//                 echo 0;
//             }else{
//                 echo 1;
//             }
//     }

function json_customers(){
        $result = $this->mo_bbs->users_select_customer();
        echo json_encode($result); 
       
}

/********************* JSON FOR ANDROID **********************/
    

    function jsonBodyType()
    {
        echo $this->mo_bbs->jsonBodyType();
    }

    function jsonMake()
    {
        echo $this->mo_bbs->jsonBrand();
    }

    /*function jsonModel()
    {
        
    }

    function jsonCountry()
    {
        
    }*/

    function jsonCarImage()
    {
        echo $this->mo_bbs->jsonCarImage();
    }

    function jsonContact()
    {
        echo $this->mo_bbs->jsonContact();
    }

    function jsonSeller()
    {
        echo $this->mo_bbs->jsonSeller();
    }

    function jsonCar()
    {
        echo $this->mo_bbs->jsonCar();
    }

    function jsonPremium()
    {
        echo $this->mo_bbs->jsonPremium();
    }

    function jsonContactTab()
    {
        echo $this->mo_bbs->jsonContactTab();
    }

    function saveGCM()
    {
        $sql='SELECT COUNT(gcm_regid) as count FROM gcm_users WHERE gcm_regid="'.$_POST['token'].'"';        
        $result=$this->mo_bbs->selectData($sql);
        if($result[0]->count==0)
        {
            $this->db->set('gcm_regid',$_POST['token']);
            $this->db->insert('gcm_users'); 
        }        
    }

    function androidLogIn()
    {
        $username=$_POST['username'];
        $password=func_base64_encode($_POST['password'], TRUE);
        echo $this->mo_bbs->androidLogIn($username,$password);
    }

    /**********************FUNCTION FIRST CREATE SQLite*****************/

    /*function SQLite()
    {
        // ini_set('max_execution_time', 600);
        if(!class_exists('SQLite3'))
          die("SQLite 3 NOT supported.");

        $this->createTable();

        // INSERT BODY TYPE
        $datas=json_decode($this->mo_bbs->jsonBodyType());
        $this->insertData('body_types',$datas);
        
        // INSERT BRAND
        $datas=json_decode($this->mo_bbs->jsonBrand());
        $this->insertData('brands',$datas);

        // INSERT MODEL
        // $datas=json_decode($this->jsonModel());
        // $this->insertData('models',$datas);

        // INSERT SELLER
        $datas=json_decode($this->mo_bbs->jsonSeller());
        $this->insertData('sellers',$datas);

        // INSERT CONTACT
        $datas=json_decode($this->mo_bbs->jsonContact());
        $this->insertData('contacts',$datas);

        // INSERT CAR
        $datas=json_decode($this->mo_bbs->jsonCar());
        $this->insertData('cars',$datas);

        // INSERT CAR IMAGE
        $datas=json_decode($this->mo_bbs->jsonCarImage());
        $this->insertData('car_images',$datas);

        $this->updatePremium();

    }

    function createTable()
    {
        $dbname='sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);

        $query='
        CREATE TABLE IF NOT EXISTS android_metadata (locale TEXT);
        CREATE TABLE IF NOT EXISTS "body_types" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "name" VARCHAR NOT NULL,"title" VARCHAR,"image" VARCHAR NOT NULL , "deleted" INTEGER NOT NULL  DEFAULT 0, "created" DATETIME DEFAULT CURRENT_TIMESTAMP, "modified" DATETIME DEFAULT CURRENT_TIMESTAMP);
        CREATE TABLE IF NOT EXISTS "car_images" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "car_id" INTEGER NOT NULL ,"image" VARCHAR, "sort" INTEGER,"primary" INTEGER, "deleted" INTEGER NOT NULL  DEFAULT 0, "created" DATETIME DEFAULT CURRENT_TIMESTAMP, "modified" DATETIME DEFAULT CURRENT_TIMESTAMP);
        CREATE TABLE IF NOT EXISTS "cars" ("id" INTEGER PRIMARY KEY  NOT NULL,"car_id" INTEGER ,"condition" INTEGER,"type" VARCHAR ,"make" VARCHAR ,"model" VARCHAR ,"chassis_no" VARCHAR,"model_year" INTEGER,"mileage" VARCHAR,"steering" INTEGER,"transmission" INTEGER,"engine_size" VARCHAR,"fuel_type" VARCHAR,"country" INTEGER NOT NULL ,"exterior_color" VARCHAR,"manufactured_year" VARCHAR,"registration_date" VARCHAR,"fob" VARCHAR,"comment" VARCHAR,"seller_id" INTEGER NOT NULL,"contact_id" INTEGER NOT NULL, "premium" INTEGER,"status" VARCHAR,"deleted" INTEGER NOT NULL  DEFAULT (0) ,"created" DATETIME DEFAULT (CURRENT_TIMESTAMP) ,"modified" DATETIME DEFAULT (CURRENT_TIMESTAMP) );
        CREATE TABLE IF NOT EXISTS "contacts" ("id" INTEGER PRIMARY KEY  NOT NULL ,"contact_id" INTEGER,"name" VARCHAR NOT NULL ,"image" VARCHAR,"country" VARCHAR NOT NULL ,"tel" VARCHAR,"email" VARCHAR,"skype_id" VARCHAR,"whatsapp_id" VARCHAR,"viber_id" VARCHAR ,"facebook_id" VARCHAR ,"deleted" INTEGER NOT NULL  DEFAULT (0) ,"created" DATETIME DEFAULT (CURRENT_TIMESTAMP) ,"modified" DATETIME DEFAULT (CURRENT_TIMESTAMP) );
        CREATE TABLE IF NOT EXISTS "brands" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "name" VARCHAR NOT NULL,"image" VARCHAR NOT NULL , "deleted" INTEGER NOT NULL  DEFAULT 0, "created" DATETIME DEFAULT CURRENT_TIMESTAMP, "modified" DATETIME DEFAULT CURRENT_TIMESTAMP);
        CREATE TABLE IF NOT EXISTS "models" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE ,"make_id" INTEGER NOT NULL, "name" VARCHAR NOT NULL , "deleted" INTEGER NOT NULL  DEFAULT 0, "created" DATETIME DEFAULT CURRENT_TIMESTAMP, "modified" DATETIME DEFAULT CURRENT_TIMESTAMP);
        CREATE TABLE IF NOT EXISTS "sellers" ("id" INTEGER PRIMARY KEY  NOT NULL ,"seller_id" INTEGER ,"name" VARCHAR NOT NULL ,"company" VARCHAR NOT NULL ,"image" VARCHAR,"country" VARCHAR NOT NULL  DEFAULT (null) ,"address" VARCHAR,"introduction" VARCHAR,"deleted" INTEGER NOT NULL  DEFAULT (0) ,"created" DATETIME DEFAULT (CURRENT_TIMESTAMP) ,"modified" DATETIME DEFAULT (CURRENT_TIMESTAMP) );';
        
        $handler->query($query);
    }

    function insertData($table,$datas)
    {
        $dbname='sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);
        // print_r($datas);
        foreach ($datas as $data) {
            $columns='(';
            $values='(';        
            foreach ($data as $key => $value) {
                $columns.='`'.$key.'`,';
                $values.='"'.$value.'",';
            }
            $query='INSERT INTO `'.$table.'` '.rtrim($columns, ',').') VALUES'.rtrim($values, ',').');';
            // echo $query.'</br>';
            $handler->query($query);
        }
    }

    function updatePremium()
    {
        $dbname='sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);
        $datas=json_decode($this->mo_bbs->jsonPremium());
        foreach ($datas as $data) {
            $query='UPDATE `cars` SET `premium`=1 WHERE car_id='.$data->car_id;
            // echo $query.'</br>';
            $handler->query($query);
        }
        
    }*/

}

?>
