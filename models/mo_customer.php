<?php



/* ------------------------------------------------

 * 파일명 : me_customer.php

 * 개  요 : customer 관련

  ------------------------------------------------ */

class Mo_customer extends Model {



	function __construct() {

		parent::Model();
		 $this->load->model('mo_email', 'mo_email');


		$this->load->database('default');

	}
	function adm_count_order($where=array()){

		$this->db->select('COUNT(*) as count');
		$this->db->from('iw_customer_inventory');
		$this->db->where($where); 
		$query = $this->db->get();
		
		$result = $query->result();
		return $result['0']->count;
	}
	function adm_order_list($where=array()){
		$this->db->select('iw_customer_inventory.*, iw_member.member_last_name, iw_member.member_first_name');
		$this->db->from('iw_customer_inventory');
		$this->db->join('iw_member', 'iw_customer_inventory.customer_no = iw_member.member_no');
		$this->db->where($where); 
		$query = $this->db->get();
		return $query;
	}


	//ROOT 관리자 체크

	function iw_customer_chk($customer_id, $customer_pwd) {

		$IWCOMMONDB = $this->load->database('iwcommon', TRUE);



/*		$sql = "select *              \r\n";

		$sql .= "  from iwc_customer     \r\n";

		$sql .= " where customer_id = ?  \r\n";

		$sql .= "   and customer_pwd = ? \r\n";

		$sql .= "   and use_yn = 'Y';  \r\n";*/

		$sql = "select *              \r\n";

		$sql .= "  from iw_customer     \r\n";

		$sql .= " where customer_id = ?  \r\n";

		$sql .= "   and customer_pwd = ? \r\n";

		$sql .= "   and certi_yn = 'Y';  \r\n";


		$query = $IWCOMMONDB->query($sql, array($customer_id, $customer_pwd));


		return $query;

	}



	//관리자 체크

	function admin_customer_chk($customer_id, $customer_pwd) {

		$sql = "select *              \r\n";

		$sql .= "  from iw_customer      \r\n";

		$sql .= " where customer_id = ?  \r\n";

		$sql .= "   and customer_pwd = ? \r\n";

		$sql .= "   and certi_yn = 'Y' \r\n";

		$sql .= "   and grade_no = 1;  \r\n"; //관리자 등급 번호는 '1'로 고정



		$query = $this->db->query($sql, array($customer_id, $customer_pwd));



		return $query;

	}



	//관리자 로그인 COUNT UPDATE

	function admin_login_count_update($customer_id) {

		$sql = "update iw_customer                 \r\n";

		$sql .= "   set login_cnt = login_cnt + 1 \r\n";

		$sql .= " where customer_id = ?;            \r\n";



		$this->db->query($sql, array($customer_id));



		return $this->db->affected_rows();

	}



	//관리자 로그인 DATE UPDATE

	function admin_login_date_update($customer_id, $last_login_dt) {

		$sql = "update iw_customer         \r\n";

		$sql .= "   set last_login_dt = ? \r\n";

		$sql .= " where customer_id = ?;    \r\n";



		$this->db->query($sql, array($last_login_dt, $customer_id));



		return $this->db->affected_rows();

	}



	//customer등급 조회

	function customer_grade_select() {

		$sql = "select *               \r\n";

		$sql .= "  from iw_customer_grade \r\n";

		$sql .= " order by grade_no asc \r\n";



		$query = $this->db->query($sql);



		return $query;

	}



	//customer등급 상세 조회

	function customer_grade_select_detail($seq_no) {

		$sql = "select *               \r\n";

		$sql .= "  from iw_customer_grade \r\n";

		$sql .= " where seq_no =?       \r\n";



		$query = $this->db->query($sql, array($seq_no));



		return $query;

	}



	//customer가입시 정의된 기본등급 조회

	function customer_grade_default_yn() {

		$sql = "select grade_no           \r\n";

		$sql .= "  from iw_customer_grade    \r\n";

		$sql .= " where default_yn ='Y'    \r\n";

		$sql .= " order by created_dt desc \r\n";

		$sql .= " limit 0, 1               \r\n";



		$query = $this->db->query($sql);



		if ($query->num_rows() > 0) {

			$rows = $query->row();



			return $rows->grade_no;

		} else {

			return '9999';

		}

	}




	//사용자 체크

	function customer_chk($customer_id, $customer_pwd) {

		$sql = '';

		$sql .= "select a.*                                                                    \n";

		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

		$sql .= "      ,a.created_dt as join_dt                                                \n";

		$sql .= "      ,b.*                                                                    \n";

		$sql .= "  from (                                                                      \n";

		$sql .= "        select * from iw_customer                                               \n";

		$sql .= "         where customer_id = ?                                                  \n";

		$sql .= "           and customer_pwd = ?                                                 \n";

		//$sql .= "           and certi_yn = 'Y'                                                 \n";

		$sql .= "       ) as a                                                                 \n";

		$sql .= " left outer join                                                              \n";

		$sql .= "       (                                                                      \n";

		$sql .= "        select * from iw_customer_grade                                         \n";

		$sql .= "       ) as b                                                                 \n";

		$sql .= " on a.grade_no = b.grade_no;                                                  \n";



		$query = $this->db->query($sql, array($customer_id, $customer_pwd));



		return $query;

	}



	//사용자 로그인 COUNT UPDATE

	function login_count_update($customer_id) {

		$sql = "update iw_customer                 \r\n";

		$sql .= "   set login_cnt = login_cnt + 1 \r\n";

		$sql .= " where customer_id = ?;            \r\n";



		$this->db->query($sql, array($customer_id));



		return $this->db->affected_rows();

	}



	//사용자 로그인 DATE UPDATE

	function login_date_update($customer_id, $last_login_dt) {

		$sql = "update iw_customer         \r\n";

		$sql .= "   set last_login_dt = ? \r\n";

		$sql .= " where customer_id = ?;    \r\n";



		$this->db->query($sql, array($last_login_dt, $customer_id));



		return $this->db->affected_rows();

	}



	//가입여부 체크(이름 + 주민번호)

	function customer_join_check($customer_name, $jumin_no) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where jumin_no = ?  \r\n";

		//$sql .= " where customer_name = ?  \r\n";

		//$sql .= "   and jumin_no = ?     \r\n";



		$query = $this->db->query($sql, array($jumin_no));



		return $query->num_rows();

	}



		//주민등록번호 가입여부 체크

	function customer_jumin_check($jumin_no) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where jumin_no = ?     \r\n";



		$query = $this->db->query($sql, array($jumin_no));



		return $query->num_rows();

	}



	//아이디 찿기

	function customer_account_id($customer_name, $jumin_no) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where customer_name = ?  \r\n";

		$sql .= "   and jumin_no = ?     \r\n";



		$query = $this->db->query($sql, array($customer_name, $jumin_no));



		return $query;

	}

	

	//비밀번호 찿기

	//function customer_account_pwd($customer_id, $customer_name, $jumin_no) {

	function customer_account_pwd($customer_id, $customer_first_name, $customer_last_name) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where customer_id = ?    \r\n";

		$sql .= "   and customer_first_name = ?  \r\n";

		$sql .= "   and customer_last_name = ?  \r\n";

		//$sql .= "   and jumin_no = ?     \r\n";

		

		//$query = $this->db->query($sql, array($customer_id, $customer_name, $jumin_no));

		$query = $this->db->query($sql, array($customer_id, $customer_first_name, $customer_last_name));

		

		return $query;

	}

	

	//customer 조회 목록수

	// function total_customer_select($where) {


	// }

	

	//customer 조회

	function customer_select($where=array(), $offset=0, $num_rows=20) {
		/***INITIAL PARAMETERS***/
		$sql_where = '';
		$sql_select = '';
		$sql_param =array();
		
		/***DEFAULT WHERE CONDITION***/
                $sql_where = "WHERE member.grade_no IN ('10','11') AND (member.draft='N' OR member.email IS NOT NULL) ";

		/***SQL KEYWORD WHERE BINDING***/
        if(isset($where['keyword_member_name'])){
	        	$value = $where['keyword_member_name'];
	        	$sql_where.=" and (member.member_first_name LIKE ? OR member.member_last_name LIKE ?  OR member.company_name LIKE ? OR member.contact_person_name LIKE ? OR member.email LIKE ? OR member.member_id LIKE ?) ";
	        	$sql_param[] = "%".$value."%";
	        	$sql_param[] = "%".$value."%";
	        	$sql_param[] = "%".$value."%";
	        	$sql_param[] = "%".$value."%";
	        	$sql_param[] = "%".$value."%";
	        	$sql_param[] = "%".$value."%";
	        	unset($where['keyword_member_name']);

        }

        /***SQL SEARCH KEYWORD FOR LIST EMAIL AT PAGE SEND RECOMMEND CAR***/
        if(isset($where['keyword_member_name_email'])){
	        	$value = $where['keyword_member_name_email'];
	        	$sql_where.=" and (member.email LIKE ? OR member.member_first_name LIKE ? OR member.member_last_name LIKE ? OR member.company_name LIKE ? OR member.contact_person_name LIKE ?) ";
	        	$sql_param[] = "%".$value."%";
	        	$sql_param[] = "%".$value."%";
	        	$sql_param[] = "%".$value."%";
	        	$sql_param[] = "%".$value."%";
	        	$sql_param[] = "%".$value."%";
	        	unset($where['keyword_member_name_email']);

        }

		/***SQL SEARCH KEYWORD FOR LIST COUNTRY BUYER AT PAGE SEND RECOMMEND CAR***/
        if(isset($where['sch_country'])){
	        	$value = $where['sch_country'];
	        	$sql_where.=" and (member.member_country LIKE ?) ";
	        	$sql_param[] = "%".$value."%";
	        	unset($where['sch_country']);

        }

		/***SQL WHERE BINDING***/
		foreach($where as $key => $value){
            $sql_where.=" AND {$key} ?";
            $sql_param[]=$value;
           // var_dump($sql_param);
        }

        /***SQL SELECT FOR DATA***/
        $sql_data_head = "SELECT 
        			(select count(*)  FROM iw_bbs LEFT JOIN iw_member ON iw_bbs.car_owner=iw_member.member_no where iw_bbs.car_owner=member.member_no and iw_bbs.menu_code='product' and iw_bbs.publish='Y' and iw_bbs.category='offer' and iw_bbs.created_dt >='2013-11-18') AS count_cars,
					country_list.country_name,
					member_grade.grade_name,
					member.grade_no,
					member.member_no,
					member.business_type,
					member.member_country,member.company_name,
					member.member_id as customer_id, 			
					member.email as customer_email, member.contact_person_name as contact_person,			
					concat_ws(' ', member.member_first_name, member.member_last_name) as customer_name,   
					concat_ws('-', member.phone_no1, 	member.phone_no2, member.phone_no3) as customer_phone,
					member.address as customer_address,
					member.created_dt as reg_date,
					concat_ws(' ', sales.member_first_name, sales.member_last_name) as sales_name,cloud.public_id,cloud.base_url";
		/***SQL SELECT FOR COUNT TOTAL***/
		$sql_total_head = "SELECT COUNT(*) as total_count ";
		/***SINGLE SQL BODY FOR BOTH COUNT AND DATA***/
		$sql_body = "
				FROM iw_member as member 
				LEFT JOIN iw_member_grade as member_grade ON member.grade_no = member_grade.grade_no
				LEFT JOIN iw_country_list as country_list ON member.member_country = country_list.cc
				LEFT JOIN iw_cloud_files as cloud ON member.profile_image_id = cloud.id	
				LEFT JOIN iw_member as sales ON member.sales_no = sales.member_no	
				{$sql_where}
				ORDER BY (select count(*)  FROM iw_bbs LEFT JOIN iw_member ON iw_bbs.car_owner=iw_member.member_no where iw_bbs.car_owner=member.member_no) DESC 
		";
		$sql_limit = "LIMIT ?, $num_rows ";
		$sql_param[] = $offset;
		/***QUERY EXECUTION***/
		$query_data = $this->db->query($sql_data_head.$sql_body.$sql_limit, $sql_param);
		
		$query_total = $this->db->query($sql_total_head.$sql_body, $sql_param);
		$result_total = $query_total->result();

		/***RETURN DATA***/
		$query_data->total_count = $result_total[0]->total_count;
		return $query_data;

	}

	function list_country_buyer(){
        $this->db->select('DISTINCT(cc),country_name,phonecode');
        $this->db->from('iw_country_list');
        $this->db->join('iw_member', 'iw_member.member_country = iw_country_list.cc');
        $this->db->where('iw_member.business_type','buyer');
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

	function customer_select_undeleted() {

		$sql = "SELECT CONCAT(ifnull(member_first_name,''),'  ',ifnull(member_last_name,'')) as customer_full_name, member_no FROM iw_member WHERE grade_no= 11 \n";

		$query = $this->db->query($sql);

		return $query;

	}


	function country_seller(){

		$sql="SELECT DISTINCT country_name,member_country From iw_country_list INNER JOIN iw_member ON iw_country_list.cc=iw_member.member_country WHERE business_type='seller'";
		$query=$this->db->query($sql);
		$result=$query->result();
		return $result;
	}


	//customer 전체 조회(관리자)

	function customer_select_all() {

		$sql = '';

		$sql .= "select a.*                                                                    \n";

		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

		$sql .= "      ,a.created_dt as join_dt                                                \n";

		$sql .= "      ,b.*                                                                    \n";

		$sql .= "  from (                                                                      \n";

		$sql .= "        select * from iw_customer                                               \n";

		$sql .= "       ) as a                                                                 \n";

		$sql .= " left outer join                                                              \n";

		$sql .= "       (                                                                      \n";

		$sql .= "        select * from iw_customer_grade                                         \n";

		$sql .= "       ) as b                                                                 \n";

		$sql .= " on a.grade_no = b.grade_no;                                                  \n";



		$query = $this->db->query($sql);



		return $query;

	}



	//customer 상세 조회(관리자)

	function customer_select_detail_adm($customer_no) {

		var_dump("test"); exit();

		$sql = '';

		$sql .= "select a.*                                                                    \n";

		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

		$sql .= "      ,a.created_dt as join_dt                                                \n";

		$sql .= "      ,b.*                                                                    \n";

		$sql .= "  from (                                                                      \n";

		$sql .= "        select * from iw_customer                                               \n";

		$sql .= "         where customer_no = ?                                                  \n";

		$sql .= "       ) as a                                                                 \n";

		$sql .= " left outer join                                                              \n";

		$sql .= "       (                                                                      \n";

		$sql .= "        select * from iw_customer_grade                                         \n";

		$sql .= "       ) as b                                                                 \n";

		$sql .= " on a.grade_no = b.grade_no;                                                  \n";



		$query = $this->db->query($sql, array($customer_no));

		//var_dump($sql); exit();

		return $query;

	}



	//customer 상세 조회(사용자)

	function customer_select_detail($customer_no) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where customer_no = ?    \r\n";

		//var_dump($sql); var_dump($customer_no); exit();

		$query = $this->db->query($sql, $customer_no);

/*		var_dump($this->db );
		exit();*/
		return $query;
	}



	//customer 등록(사용자)

	function customer_insert($data) {
		$data['grade_no'] = 11;
                $data['certi_yn'] ='Y';

		$this->db->insert("iw_member", $data);
		$insert_id = $this->db->insert_id();
		
		return $insert_id;	

	}
        
        function check_member_id($member_id){
              $sql = "SELECT member_id FROM iw_member WHERE member_id = ? LIMIT 1 ";
                $sql_param  = array($member_id);
                $query = $this->db->query($sql, $sql_param);
                $result = $query->result();
                if($query->num_rows() > 0){
                    return TRUE;
                }
              else {
                    return FALSE;
              }
        }
        function validate_password($member_id, $password){
        	$sql_param = array();
        	$sql = "SELECT COUNT(*) as num_rows FROM iw_member WHERE member_id=? AND member_pwd=?";
        	$sql_param[] = $member_id;
        	$sql_param[] = func_base64_encode($password);
        	$query = $this->db->query($sql, $sql_param);
        	$result = $query->result();
        	$count_row = $result[0]->num_rows;
        	if($count_row>0){
        		return true;
        	}
        }
        function member_checkid_password($member_id,$oldpassword){
            $sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
		$sql .= "      ,a.created_dt as join_dt                                                \n";
		$sql .= "      ,b.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "         where member_no = ?                                                  \n";
		$sql .= "           and member_pwd = ?                                                 \n";
		//$sql .= "           and certi_yn = 'Y'                                                 \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member_grade                                         \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.grade_no = b.grade_no;                                                  \n";

		$query = $this->db->query($sql, array($member_id,$oldpassword));
		echo $this->db->last_query();
		return $query;
        }
                
        function update_pass_true($newpassword,$confirmpassword,$member_id){
            
             if ($newpassword ==$confirmpassword) {
         $sqlpassword = mysql_query("UPDATE iw_member SET member_pwd = '{$newpassword}' WHERE member_no = '{$member_id}'");
          echo func_jsAlertReplace('Password has been modified.', '/?c=admin&m=adm_changepassword');
      } else {
             echo func_jsAlertReplace('Unable to change password. New Password and confirm password doesnot match!', '/?c=admin&m=adm_changepassword');
            
            
           }
        }
                
        function view_my_info($member_id){
               
                $sql="select * from iw_member where member_id='".$member_id."' ";
                $query=  mysql_query($sql);
                return $query;
        }

    function user_update_pass_true($newpassword,$member_id){

        $data['member_pwd'] = $newpassword;
        $this->db->where('member_no',$member_id);
        $this->db->update('iw_member',$data);
        
    }
    function update_password($member_id, $new_password){
        //customer 수정(사용자)
        $data['member_pwd'] = $new_password;
        $this->db->where('member_id',$member_id);
        return $this->db->update('iw_member',$data);
    }

	function customer_update($data) {
		$data['draft'] = 'N';
		$this->db->where("member_no", $data['member_no']);
		$this->db->update("iw_member", $data);
		
		if(isset($data['business_type'])){

			if( ($data['business_type'] != "seller") && ($data['business_type'] != "both")){
				
				$this->insert_email_to_recipient($data['member_country'], $data['email'], $data['member_first_name']." ".$data['member_last_name']);
				
			}
		}

	}
	function insert_email_to_recipient($country_cc, $email, $member_name){
		$email_data = array();

		$list_name = $this->get_country_name($country_cc);
		if($list_name){
			$rec_list = $this->mo_email->select_recipient_list($list_name);
			if(count($rec_list)<=0){
				//Insert new rec list
				$this->mo_email->insert_recipient_list($list_name);
			}
			$email_data[] = array('name'=>$member_name,'email'=>$email);
			$this->mo_email->insert_email_to_list($list_name, $email_data);
		}
	}

	function get_country_name($country_cc){
		$this->db->select('country_name');
		$this->db->from('iw_country_list');
		$this->db->where('cc', $country_cc);
		$query = $this->db->get();
		$result = $query->result();
		if(count($result)>0){
			return $result[0]->country_name;
		}
		
	}

	function _customer_update($data) {

		$this->db->where("member_id", $data['member_id']);

		$this->db->update("iw_member", $data);
	}

	//customer 삭제(사용자)

	function customer_delete($customer_no) {

		$this->db->where("customer_no", $customer_no);

		$this->db->delete("iw_customer");

	}



	//비밀번호 비교 체크

	function customer_password_check($customer_no, $password) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where customer_no = ?    \r\n";

		$sql .= "   and customer_pwd  = ?    \r\n";



		$query = $this->db->query($sql, array($customer_no, $password));



		return $query->num_rows();

	}

	 function modal_popup($tittle,$message,$link){
        	

        	echo '
                   <link rel="stylesheet" href="/css/member_site/bootstrap.min.css">
				   <script src="/js/admin/jquery.min.js"></script>
				   <script src="/js/admin/bootstrap.min.js"></script>
                 ';
        	
            echo 
            '
				  <div class="modal-dialog">
				    <div class="modal-content">
				    <div class="modal-header">
				    <div class="bootstrap-dialog-header">
				    <div class="bootstrap-dialog-close-button" style="display: none;">
				    <button class="close">×</button></div><div class="bootstrap-dialog-title" 
				    id="e2985fd5-c4ec-4bbb-aab8-fdcb3d27134b_title">'.$tittle.'</div></div></div>
				    <div class="modal-body"><div class="bootstrap-dialog-body">
				    <div class="bootstrap-dialog-message">
				       '.$message.'
				    </div></div></div>
				    <div class="modal-footer" style="display: block;">
				    <div class="bootstrap-dialog-footer">
				    <div class="bootstrap-dialog-footer-buttons">
				    <a class="btn btn-default" href='.$link.'>Ok</a>
				    </div></div></div>
				    </div></div>
  
            ';
            echo '<script>
                    BootstrapDialog.alert("I want banana!");
                 </script>';
              
        }



	//customer 수정(관리자)

	function customer_update_adm($data) {

		$this->db->where("customer_no", $data['customer_no']);

		$this->db->update("iw_customer", $data);

	}



	//customer 삭제(관리자)

	function customer_delete_adm($customer_no) {

		$this->db->where("member_no", $customer_no);

		$this->db->delete("iw_member");
		
	}



	//customer 인증(관리자)

	function customer_certi_update_adm($customer_no) {

		$this->db->where("customer_no", $customer_no);

		$this->db->set("certi_yn", 'Y');

		$this->db->update("iw_customer");

	}



	/* --------------------------------------------------------------------------------------------------

	 * 게시판 연동

	  ------------------------------------------------------------------------------------------------- */



	//게시판 customer 조회 목록수

	function total_bbs_select($customer_id, $where) {

		$sql = '';

		$sql .= "select a.*                                                                    \n";

		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

		$sql .= "      ,b.*                                                                    \n";

		$sql .= "  from (                                                                      \n";

		$sql .= "        select * from iw_bbs                                                  \n";

		$sql .= "         where customer_id = ?                                                  \n";

		$sql .= "           and notice_yn = 'N'                                                \n";

		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		$sql .= "      order by grp_idx desc, order_no asc                                     \n";

		$sql .= "       ) as a                                                                 \n";

		$sql .= " left outer join                                                              \n";

		$sql .= "       (                                                                      \n";

		$sql .= "        select * from iw_bbs_attach group by idx                              \n";

		$sql .= "       ) as b                                                                 \n";

		$sql .= " on a.idx = b.idx;                                                            \n";



		//파라미터 생성

		$sql_param = array();

		$sql_param[0] = $customer_id; //customer아이디

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}



		$query = $this->db->query($sql, $sql_param);



		return $query->num_rows();

	}



	//게시판 customer 조회

	function bbs_select($customer_id, $notice_yn = 'N', $limit_st, $limit_ed, $where) {

		$sql = '';

		$sql .= "select a.*                                                                    \n";

		$sql .= "      ,a.menu_code as bbs_menu_code                                           \n";

		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

		$sql .= "      ,b.*                                                                    \n";

		$sql .= "      ,a.idx                                                                  \n";

		$sql .= "  from (                                                                      \n";

		$sql .= "        select * from iw_bbs                                                  \n";

		$sql .= "         where customer_id = ?                                                  \n";

		$sql .= "           and notice_yn = ?                                                  \n";

		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		//일반, 공지 구분

		if ($notice_yn != 'Y') {

			$sql .= "      order by grp_idx desc, order_no asc                                 \n";

		} else {

			$sql .= "      order by created_dt desc                                            \n";

		}

		$sql .= "         limit ?, ?                                                           \n";

		$sql .= "       ) as a                                                                 \n";

		$sql .= " left outer join                                                              \n";

		$sql .= "       (                                                                      \n";

		$sql .= "        select * from iw_bbs_attach group by idx                              \n";

		$sql .= "       ) as b                                                                 \n";

		$sql .= " on a.idx = b.idx;                                                            \n";







		//파라미터 생성

		$sql_param = array();

		$sql_param[0] = $customer_id; //customer아이디

		$sql_param[count($sql_param)] = $notice_yn; //일반 공지 구분

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}

		$sql_param[count($sql_param)] = $limit_st;

		$sql_param[count($sql_param)] = $limit_ed;



		$query = $this->db->query($sql, $sql_param);



		return $query;

	}

	// 첨부파일 등록

	function insert_attach($data) {

		$this->db->insert("iw_customer_attach", $data);

		return $this->db->insert_id();

	}

	// 첨부파일 조회

	function select_attach($customer_no, $category = '') {

		if($category != '') {

			$sql = "select * from iw_customer_attach   \n";

			$sql .= "         where customer_no = ?        \n";

			$sql .= "           and category = ?  \n";

			//파라미터 생성

			$sql_param = array();

			$sql_param[0] = $customer_no;

			$sql_param[count($sql_param)] = $category;

		} else {

			$sql = "select * from iw_customer_attach   \n";

			$sql .= "         where customer_no = ?        \n";

			//파라미터 생성

			$sql_param = array();

			$sql_param[0] = $customer_no;

		}

		$query = $this->db->query($sql, $sql_param);



		return $query;

	}

	// 첨부파일 상세 조회

	function select_attach_detail($customer_no, $att_idx) {

		$sql = "select * from iw_customer_attach   \n";

		$sql .= "         where customer_no = ?        \n";

		$sql .= "           and att_idx = ?    \n";



		//파라미터 생성

		$sql_param = array();

		$sql_param[0] = $customer_no;	   //idx

		$sql_param[count($sql_param)] = $att_idx;   //첨부파일 idx



		$query = $this->db->query($sql, $sql_param);



		return $query;

	}

	// 첨부파일 삭제

	function delete_attach($customer_no, $att_idx) {

		$this->db->where("customer_no", $customer_no);

		$this->db->where("att_idx", $att_idx);

		$this->db->delete("iw_customer_attach");

	}
        
    function get_all_persion_sale(){
               
        $this->db->select('member_no,member_first_name');
        $this->db->from('iw_member');
        $where['grade_no <']='10';
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    function get_email_by_customer_no($member_no) {
    	$this->db->select('*');
        $this->db->from('iw_member');
        $this->db->where('member_no', $member_no);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

}

?>