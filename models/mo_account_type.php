<?php



/* ------------------------------------------------

 * 파일명 : me_customer.php

 * 개  요 : customer 관련

  ------------------------------------------------ */

class Mo_account_type extends Model {



	function __construct() {

		parent::Model();



		$this->load->database('default');

	}



	function account_type_list($where=array()) {


		$sql = '';
		$sql .= "   select a.* from iw_account_type a                                     \n";
		$sql .= "   where 1                                          \n";

		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}


		$sql .= " ;                                                  \n";

		//print_r($sql); exit();

		//파라미터 생성

		$sql_param = array();

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}

		$query = $this->db->query($sql, $sql_param);
		return $query;

	}




	// //customer 상세 조회(사용자)

	// function customer_select_detail($customer_id) {

	// 	$sql = "select *                \r\n";

	// 	$sql .= "  from iw_customer        \r\n";

	// 	$sql .= " where customer_id = ?    \r\n";



	// 	$query = $this->db->query($sql, array($customer_id));



	// 	return $query;

	// }



	// //customer 등록(사용자)

	// function customer_insert($data) {

	// 	$this->db->insert("iw_customer", $data);

	// 	$insert_id = $this->db->insert_id();



	// 	return $insert_id;

	// }



	// //customer 수정(사용자)

	// function customer_update($data) {

	// 	$this->db->where("customer_id", $data['customer_id']);

	// 	$this->db->update("iw_customer", $data);

	// }



	// //customer 삭제(사용자)

	// function customer_delete($customer_no) {

	// 	$this->db->where("customer_no", $customer_no);

	// 	$this->db->delete("iw_customer");

	// }



	// function total_bbs_select($customer_id, $where) {

	// 	$sql = '';

	// 	$sql .= "select a.*                                                                    \n";

	// 	$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

	// 	$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

	// 	$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

	// 	$sql .= "      ,b.*                                                                    \n";

	// 	$sql .= "  from (                                                                      \n";

	// 	$sql .= "        select * from iw_bbs                                                  \n";

	// 	$sql .= "         where customer_id = ?                                                  \n";

	// 	$sql .= "           and notice_yn = 'N'                                                \n";

	// 	//검색 조건 생성

	// 	if ($where != NULL) {

	// 		foreach ($where as $key => $value) {

	// 			$findIt = strpos($key, 'IWSEARCHVALS');



	// 			if ($findIt === FALSE) {

	// 				if (strpos($key, 'IWSEARCHKEY') === FALSE) {

	// 					$sql .='  and  ' . $key . ' ? ';

	// 				} else {

	// 					$key = str_replace('IWSEARCHKEY', '', $key);

	// 					$sql .=' and ' . $key;

	// 				}

	// 			}

	// 		}

	// 	}

	// 	$sql .= "      order by grp_idx desc, order_no asc                                     \n";

	// 	$sql .= "       ) as a                                                                 \n";

	// 	$sql .= " left outer join                                                              \n";

	// 	$sql .= "       (                                                                      \n";

	// 	$sql .= "        select * from iw_bbs_attach group by idx                              \n";

	// 	$sql .= "       ) as b                                                                 \n";

	// 	$sql .= " on a.idx = b.idx;                                                            \n";



	// 	//파라미터 생성

	// 	$sql_param = array();

	// 	$sql_param[0] = $customer_id; //customer아이디

	// 	if ($where != NULL) {

	// 		foreach ($where as $key => $value) {

	// 			if ($value == '')

	// 				continue;

	// 			$sql_param[count($sql_param)] = $value;

	// 		}

	// 	}



	// 	$query = $this->db->query($sql, $sql_param);



	// 	return $query->num_rows();

	// }



	// //게시판 customer 조회

	// function bbs_select($customer_id, $notice_yn = 'N', $limit_st, $limit_ed, $where) {

	// 	$sql = '';

	// 	$sql .= "select a.*                                                                    \n";

	// 	$sql .= "      ,a.menu_code as bbs_menu_code                                           \n";

	// 	$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

	// 	$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

	// 	$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

	// 	$sql .= "      ,b.*                                                                    \n";

	// 	$sql .= "      ,a.idx                                                                  \n";

	// 	$sql .= "  from (                                                                      \n";

	// 	$sql .= "        select * from iw_bbs                                                  \n";

	// 	$sql .= "         where customer_id = ?                                                  \n";

	// 	$sql .= "           and notice_yn = ?                                                  \n";

	// 	//검색 조건 생성

	// 	if ($where != NULL) {

	// 		foreach ($where as $key => $value) {

	// 			$findIt = strpos($key, 'IWSEARCHVALS');



	// 			if ($findIt === FALSE) {

	// 				if (strpos($key, 'IWSEARCHKEY') === FALSE) {

	// 					$sql .='  and  ' . $key . ' ? ';

	// 				} else {

	// 					$key = str_replace('IWSEARCHKEY', '', $key);

	// 					$sql .=' and ' . $key;

	// 				}

	// 			}

	// 		}

	// 	}

	// 	//일반, 공지 구분

	// 	if ($notice_yn != 'Y') {

	// 		$sql .= "      order by grp_idx desc, order_no asc                                 \n";

	// 	} else {

	// 		$sql .= "      order by created_dt desc                                            \n";

	// 	}

	// 	$sql .= "         limit ?, ?                                                           \n";

	// 	$sql .= "       ) as a                                                                 \n";

	// 	$sql .= " left outer join                                                              \n";

	// 	$sql .= "       (                                                                      \n";

	// 	$sql .= "        select * from iw_bbs_attach group by idx                              \n";

	// 	$sql .= "       ) as b                                                                 \n";

	// 	$sql .= " on a.idx = b.idx;                                                            \n";







	// 	//파라미터 생성

	// 	$sql_param = array();

	// 	$sql_param[0] = $customer_id; //customer아이디

	// 	$sql_param[count($sql_param)] = $notice_yn; //일반 공지 구분

	// 	if ($where != NULL) {

	// 		foreach ($where as $key => $value) {

	// 			if ($value == '')

	// 				continue;

	// 			$sql_param[count($sql_param)] = $value;

	// 		}

	// 	}

	// 	$sql_param[count($sql_param)] = $limit_st;

	// 	$sql_param[count($sql_param)] = $limit_ed;



	// 	$query = $this->db->query($sql, $sql_param);



	// 	return $query;

	// }

	

}

?>