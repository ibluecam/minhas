<?php



/* ------------------------------------------------

 * 파일명 : mo_message.php

 * 개  요 : message 관련

  ------------------------------------------------ */

class Mo_message_buyer extends Model {



	function __construct() {

		parent::Model();


		$this->load->database('default');

	}
    //Function gat Message for views
	function getMessages($limit_s, $limit_e, $where){

        $this->db->select('iw_messages.*,iw_member.member_no,iw_member.member_id,iw_member.member_first_name');
        $this->db->from('iw_messages');
        $user_session = $this->phpsession->get('member_no', 'USER');
        $this->db->join('iw_member',"iw_member.member_no=iw_messages.receiver AND member_no='{$user_session}'");
        $this->db->order_by("id", "desc");
        $this->db->where($where); 
        $this->db->limit($limit_e, $limit_s);
        $query = $this->db->get();
        $result = $query->result();

        return $result;

    }

    function getcomment(){
        $this->db->select('comments.*');
        $this->db->from('iw_messages');
    }
    //count message at inbox for pagination
    function count_messages($where){
        $this->db->select('COUNT(*) as num_row');
        $this->db->from('iw_messages');
        $user_session = $this->phpsession->get('member_no', 'USER');
        $this->db->join('iw_member',"iw_member.member_no=iw_messages.receiver AND member_no='{$user_session}'");
        $this->db->order_by("id", "desc");
        $this->db->where($where); 
        $query = $this->db->get();

        $result = $query->result();

        return $result[0]->num_row;
    }
    //Function delete Messages
    function deleteMessage($where_in=array()){

        $this->db->where_in('id', $where_in); 
        $result = $this->db->delete('iw_messages');
        return $result;
    }
    public function messagesdetail($where=array()){
        
        $this->db->select('iw_messages.*,iw_member.member_no,iw_member.member_id,iw_member.member_first_name');
        $this->db->from('iw_messages');
        $user_session = $this->phpsession->get('member_no', 'USER');
        $this->db->join('iw_member',"iw_member.member_no=iw_messages.receiver AND member_no='{$user_session}'");
        $this->db->where($where); 
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }
    function updateReadDate($where){

        $read_dt = func_get_date_time();
        $data['read_dt'] = $read_dt;
        $this->db->where($where);
        $result = $this->db->update('iw_messages', $data);
        return $result;
    }
    function getOrderCustomer(){
        $idx=$_GET['idx'];
        $rows = array();
        $this->db->select('customer_no');
        $this->db->from('iw_customer_inventory');
        $this->db->where("stock_idx", $idx);
        $query = $this->db->get();
        
        $result = $query->result(); 
        
        foreach($result as $value){
            $rows[] = $value->customer_no;
        }    
        return $rows; 
    }
    function sendMessage($idx="",$customers=0){
        
        //$customers = $this->getOrderCustomer();

        $subject="Car Order Notification";
        //$body= "We are sorry to inform you that one of your ordered cars has been resereved by other customer.";
        $body='<tr> <td colspan="2" class="contact_message"><p>We are sorry to inform you that one of your ordered cars has been resereved by other customer.</p></td>
                </tr>
                <tr>
                   <td colspan="2" class="contact_message">
                      <p><b>Link to the Car:</b><br>
                    <a href="http://iblau.com/?c=user&mcd=product&me=bbs_detail&idx='.$idx.'&category=offer&sch_create_dt_s=&sch_create_dt_e=&sParam=" target="_blank">
                        http://iblau.com/?c=user&mcd=product&me=bbs_detail&idx='.$idx.'&category=offer&sch_create_dt_s=&sch_create_dt_e=&sParam=
                    </a>
                       </p>
                  </td>
                </tr>';        
        $author= '0';
        $type= "system";
        $read_dt= '0';
        $created_dt= func_get_date_time();
        $author_deleted_dt= '0';
        $receiver_deleted_dt= '0';
        $value_arr = array();

        $sql = "";
        $sql .= "INSERT INTO iw_messages (subject, body, author, receiver, type, read_dt, created_dt, author_deleted_dt,receiver_deleted_dt)
        VALUES ";
        
        for ($i = 0; $i < count($customers); $i++) {
           $value_arr[] .= "('$subject', '$body', '$author', '$customers[$i]', '$type', '$read_dt', '$created_dt', '$author_deleted_dt', '$receiver_deleted_dt')";    
        }

        $sql.= implode(", ", $value_arr);
        $query = $this->db->query($sql);
        return $query;
    }
    function getSendMessages($limit_s, $limit_e, $where){
        
        $this->db->select('iw_messages.*,iw_member.member_no,iw_member.member_id,iw_member.member_first_name');
        $this->db->from('iw_messages');
        $user_session = $this->phpsession->get('member_no', 'USER');
        $this->db->join('iw_member',"iw_member.member_no=iw_messages.author AND member_no='{$user_session}'");
        $this->db->order_by("id", "desc");
        $this->db->where($where); 
        $this->db->limit($limit_e, $limit_s);
        $query = $this->db->get();

        $result = $query->result();

        return $result;

    }
    //count message at inbox for pagination
    function count_SendMessages($where){
        $this->db->select('COUNT(*) as num_row');
        $this->db->from('iw_messages');
        $user_session = $this->phpsession->get('member_no', 'USER');
        $this->db->join('iw_member',"iw_member.member_no=iw_messages.author AND member_no='{$user_session}'");
        $this->db->order_by("id", "desc");
        $this->db->where($where); 
        $query = $this->db->get();

        $result = $query->result();

        return $result[0]->num_row;
    }
    public function getSendMessageDetail($where=array()){
        
        $this->db->select('iw_messages.*,iw_member.member_no,iw_member.member_id,iw_member.member_first_name');
        $this->db->from('iw_messages');
        $user_session = $this->phpsession->get('member_no', 'USER');
        $this->db->join('iw_member',"iw_member.member_no=iw_messages.author AND member_no='{$user_session}'");
        $this->db->where($where); 
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }    
}

?>