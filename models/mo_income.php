<?php



/* ------------------------------------------------

 * 파일명 : me_customer.php

 * 개  요 : customer 관련

  ------------------------------------------------ */

class Mo_income extends Model {



	function __construct() {

		parent::Model();



		$this->load->database('default');

	}



	//ROOT 관리자 체크

	function iw_customer_chk($customer_id, $customer_pwd) {

		$IWCOMMONDB = $this->load->database('iwcommon', TRUE);



/*		$sql = "select *              \r\n";

		$sql .= "  from iwc_customer     \r\n";

		$sql .= " where customer_id = ?  \r\n";

		$sql .= "   and customer_pwd = ? \r\n";

		$sql .= "   and use_yn = 'Y';  \r\n";*/

		$sql = "select *              \r\n";

		$sql .= "  from iw_customer     \r\n";

		$sql .= " where customer_id = ?  \r\n";

		$sql .= "   and customer_pwd = ? \r\n";

		$sql .= "   and certi_yn = 'Y';  \r\n";


		$query = $IWCOMMONDB->query($sql, array($customer_id, $customer_pwd));


		return $query;

	}



	//관리자 체크

	function admin_customer_chk($customer_id, $customer_pwd) {

		$sql = "select *              \r\n";

		$sql .= "  from iw_customer      \r\n";

		$sql .= " where customer_id = ?  \r\n";

		$sql .= "   and customer_pwd = ? \r\n";

		$sql .= "   and certi_yn = 'Y' \r\n";

		$sql .= "   and grade_no = 1;  \r\n"; //관리자 등급 번호는 '1'로 고정



		$query = $this->db->query($sql, array($customer_id, $customer_pwd));



		return $query;

	}

        function  customer_deposit($data){
             $result= $this->db->insert('iw_deposit',$data);
             if($result==TRUE){
                 return TRUE;
             }
          else {
                 return FALSE;
            }
               
           }
           
           
    function get_customer($limit_st, $limit_ed, $where_deposit=array()) {
        
        $this->db->select('*');
		$this->db->from('iw_deposit');
		$this->db->where('deposit >', '0');

		if ($where_deposit != NULL) {
		
		$this->db->where($where_deposit); 
		
		}

		$this->db->limit($limit_ed, $limit_st);
		$this->db->order_by('created_dt','desc');
		$query = $this->db->get();
		$result = $query->result();
		
		return $result; 
          
        }
       
      function total_sender($where_deposit=array()){

      	$this->db->select('COUNT(*) as num_rows');
		$this->db->from('iw_deposit');

		$this->db->where('deposit >', '0');

		if ($where_deposit != NULL) {

		$this->db->where($where_deposit); 

		}

		$query = $this->db->get();
		
		$result = $query->result();

		return $result['0']->num_rows;
   	
    }


        
        
        function select_sender_exce($sch_word_sender){
            $this->db->select('sender');
            $this->db->like('sender',$sch_word_sender);

            $query=  $this->db->get('iw_deposit');

            return $query->result();
        }

        //관리자 로그인 COUNT UPDATE

	function admin_login_count_update($customer_id) {

		$sql = "update iw_customer                 \r\n";

		$sql .= "   set login_cnt = login_cnt + 1 \r\n";

		$sql .= " where customer_id = ?;            \r\n";



		$this->db->query($sql, array($customer_id));



		return $this->db->affected_rows();

	}



	//관리자 로그인 DATE UPDATE

	function admin_login_date_update($customer_id, $last_login_dt) {

		$sql = "update iw_customer         \r\n";

		$sql .= "   set last_login_dt = ? \r\n";

		$sql .= " where customer_id = ?;    \r\n";



		$this->db->query($sql, array($last_login_dt, $customer_id));



		return $this->db->affected_rows();

	}



	//customer등급 조회

	function customer_grade_select() {

		$sql = "select *               \r\n";

		$sql .= "  from iw_customer_grade \r\n";

		$sql .= " order by grade_no asc \r\n";



		$query = $this->db->query($sql);



		return $query;

	}



	//customer등급 상세 조회

	function customer_grade_select_detail($seq_no) {

		$sql = "select *               \r\n";

		$sql .= "  from iw_customer_grade \r\n";

		$sql .= " where seq_no =?       \r\n";



		$query = $this->db->query($sql, array($seq_no));



		return $query;

	}



	//customer가입시 정의된 기본등급 조회

	function customer_grade_default_yn() {

		$sql = "select grade_no           \r\n";

		$sql .= "  from iw_customer_grade    \r\n";

		$sql .= " where default_yn ='Y'    \r\n";

		$sql .= " order by created_dt desc \r\n";

		$sql .= " limit 0, 1               \r\n";



		$query = $this->db->query($sql);



		if ($query->num_rows() > 0) {

			$rows = $query->row();



			return $rows->grade_no;

		} else {

			return '9999';

		}

	}




	//사용자 체크

	function customer_chk($customer_id, $customer_pwd) {

		$sql = '';

		$sql .= "select a.*                                                                    \n";

		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

		$sql .= "      ,a.created_dt as join_dt                                                \n";

		$sql .= "      ,b.*                                                                    \n";

		$sql .= "  from (                                                                      \n";

		$sql .= "        select * from iw_customer                                               \n";

		$sql .= "         where customer_id = ?                                                  \n";

		$sql .= "           and customer_pwd = ?                                                 \n";

		//$sql .= "           and certi_yn = 'Y'                                                 \n";

		$sql .= "       ) as a                                                                 \n";

		$sql .= " left outer join                                                              \n";

		$sql .= "       (                                                                      \n";

		$sql .= "        select * from iw_customer_grade                                         \n";

		$sql .= "       ) as b                                                                 \n";

		$sql .= " on a.grade_no = b.grade_no;                                                  \n";



		$query = $this->db->query($sql, array($customer_id, $customer_pwd));



		return $query;

	}



	//사용자 로그인 COUNT UPDATE

	function login_count_update($customer_id) {

		$sql = "update iw_customer                 \r\n";

		$sql .= "   set login_cnt = login_cnt + 1 \r\n";

		$sql .= " where customer_id = ?;            \r\n";



		$this->db->query($sql, array($customer_id));



		return $this->db->affected_rows();

	}



	//사용자 로그인 DATE UPDATE

	function login_date_update($customer_id, $last_login_dt) {

		$sql = "update iw_customer         \r\n";

		$sql .= "   set last_login_dt = ? \r\n";

		$sql .= " where customer_id = ?;    \r\n";



		$this->db->query($sql, array($last_login_dt, $customer_id));



		return $this->db->affected_rows();

	}



	//가입여부 체크(이름 + 주민번호)

	function customer_join_check($customer_name, $jumin_no) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where jumin_no = ?  \r\n";

		//$sql .= " where customer_name = ?  \r\n";

		//$sql .= "   and jumin_no = ?     \r\n";



		$query = $this->db->query($sql, array($jumin_no));



		return $query->num_rows();

	}



		//주민등록번호 가입여부 체크

	function customer_jumin_check($jumin_no) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where jumin_no = ?     \r\n";



		$query = $this->db->query($sql, array($jumin_no));



		return $query->num_rows();

	}



	//아이디 찿기

	function customer_account_id($customer_name, $jumin_no) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where customer_name = ?  \r\n";

		$sql .= "   and jumin_no = ?     \r\n";



		$query = $this->db->query($sql, array($customer_name, $jumin_no));



		return $query;

	}

	

	//비밀번호 찿기

	//function customer_account_pwd($customer_id, $customer_name, $jumin_no) {

	function customer_account_pwd($customer_id, $customer_first_name, $customer_last_name) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where customer_id = ?    \r\n";

		$sql .= "   and customer_first_name = ?  \r\n";

		$sql .= "   and customer_last_name = ?  \r\n";

		//$sql .= "   and jumin_no = ?     \r\n";

		

		//$query = $this->db->query($sql, array($customer_id, $customer_name, $jumin_no));

		$query = $this->db->query($sql, array($customer_id, $customer_first_name, $customer_last_name));

		

		return $query;

	}

	

	//income 조회 목록수

	function total_income_select($where) {


		$sql = '';

		$sql .= "   select a.member_no as customer_no, concat_ws(' ',a.member_first_name, a.member_last_name) as customer_name, a.*, b.deposit_sum, b.allocate_sum, (b.deposit_sum-b.allocate_sum) as total_deposit, b.last_deposit_dt as last_deposit_dt, b.currency_type as currency_type        \n";
		$sql .= "   from iw_member as a                                     \n";
		$sql .= "   left outer join (select customer_no, currency_type, sum(car_deposit) as deposit_sum, sum(car_allocate) as allocate_sum, max(deposit_dt) as last_deposit_dt from iw_payment group by customer_no,currency_type) as b                \n";
		$sql .= "   on a.member_no = b.customer_no                                    \n";
		$sql .= "   where a.grade_no = '11'                                          \n";
		$sql .= "   and deposit_sum is not null                                          \n";

		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		$sql .= " ORDER BY last_deposit_dt DESC                      \n";
		$sql .= " ;                                                  \n";



		//파라미터 생성

		$sql_param = array();

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}

		

		$query = $this->db->query($sql, $sql_param);

		return $query->num_rows();

	}

	

	//customer 조회

	function income_select($limit_st, $limit_ed, $where) {


		$sql = '';

		$sql .= "   select a.member_no as customer_no,			\n";
		$sql .= "   		a.member_id as customer_id, 			\n";
		$sql .= "   		a.email as customer_email, 			\n";
		$sql .= "			concat_ws(' ',a.member_first_name, a.member_last_name) as customer_name,   \n";
		$sql .= "     		concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as customer_phone,      \n";
		$sql .= "			a.address as customer_address,			\n";			
		$sql .= "           a.*, b.deposit_sum, b.allocate_sum, 			\n";
		$sql .= "			(b.deposit_sum-ifnull(b.allocate_sum,0)) as total_deposit, 			\n";
		$sql .= "   		b.last_deposit_dt as last_deposit_dt, b.currency_type as currency_type       \n";
		$sql .= "   from iw_member as a                                     \n";
		$sql .= "   left outer join (select customer_no, currency_type, sum(car_deposit) as deposit_sum, sum(car_allocate) as allocate_sum, max(deposit_dt) as last_deposit_dt from iw_payment group by customer_no,currency_type) as b                \n";
		$sql .= "   on a.member_no = b.customer_no                                    \n";
		$sql .= "   where a.grade_no = '11'                                          \n";
		$sql .= "   and deposit_sum is not null                                          \n";
		
		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		$sql .= " ORDER BY last_deposit_dt DESC                      \n";
		$sql .= " limit ?,? 										 \n";
		$sql .= " ;                                                  \n";


		//print_r($sql); print_r($where); exit();

		//파라미터 생성

		$sql_param = array();

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}

		$sql_param[count($sql_param)] = $limit_st;

		$sql_param[count($sql_param)] = $limit_ed;



		$query = $this->db->query($sql, $sql_param);

		
		return $query;

	}

	function income_select_undeleted() {

		$sql = "SELECT CONCAT(ifnull(member_first_name,''),'  ',ifnull(member_last_name,'')) as customer_full_name, member_no FROM iw_member WHERE grade_no= 11 \n";

		$query = $this->db->query($sql);

		return $query;

	}


	//customer 전체 조회(관리자)

	function income_select_all() {

		$sql = '';

		$sql .= "select a.*                                                                    \n";

		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

		$sql .= "      ,a.created_dt as join_dt                                                \n";

		$sql .= "      ,b.*                                                                    \n";

		$sql .= "  from (                                                                      \n";

		$sql .= "        select * from iw_customer                                               \n";

		$sql .= "       ) as a                                                                 \n";

		$sql .= " left outer join                                                              \n";

		$sql .= "       (                                                                      \n";

		$sql .= "        select * from iw_customer_grade                                         \n";

		$sql .= "       ) as b                                                                 \n";

		$sql .= " on a.grade_no = b.grade_no;                                                  \n";



		$query = $this->db->query($sql);



		return $query;

	}



	//customer 상세 조회(관리자)

	function income_select_detail_adm($customer_no) {


		$sql = '';

		$sql .= "select a.*                                                                    \n";

		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

		$sql .= "      ,a.created_dt as join_dt                                                \n";

		$sql .= "      ,b.*                                                                    \n";

		$sql .= "  from (                                                                      \n";

		$sql .= "        select * from iw_customer                                               \n";

		$sql .= "         where customer_no = ?                                                  \n";

		$sql .= "       ) as a                                                                 \n";

		$sql .= " left outer join                                                              \n";

		$sql .= "       (                                                                      \n";

		$sql .= "        select * from iw_customer_grade                                         \n";

		$sql .= "       ) as b                                                                 \n";

		$sql .= " on a.grade_no = b.grade_no;                                                  \n";



		$query = $this->db->query($sql, array($customer_no));

		//var_dump($sql); exit();

		return $query;

	}



	//customer 상세 조회(사용자)

	function income_select_detail($customer_no) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where customer_no = ?    \r\n";

		//var_dump($sql); var_dump($customer_no); exit();

		$query = $this->db->query($sql, $customer_no);

/*		var_dump($this->db );
		exit();*/
		return $query;
	}



	//customer 등록(사용자)

	function income_insert($data) {

		$this->db->insert("iw_customer", $data);

		$insert_id = $this->db->insert_id();

		return $insert_id;	

	}



	//customer 수정(사용자)

	function income_update($data) {

		$this->db->where("customer_no", $data['customer_no']);

		$this->db->update("iw_customer", $data);

	}



	//customer 삭제(사용자)

	function income_delete($customer_no) {

		$this->db->where("customer_no", $customer_no);

		$this->db->delete("iw_customer");

	}



	//비밀번호 비교 체크

	function income_password_check($customer_no, $password) {

		$sql = "select *                \r\n";

		$sql .= "  from iw_customer        \r\n";

		$sql .= " where customer_no = ?    \r\n";

		$sql .= "   and customer_pwd  = ?    \r\n";



		$query = $this->db->query($sql, array($customer_no, $password));



		return $query->num_rows();

	}



	//customer 수정(관리자)

	function income_update_adm($data) {

		$this->db->where("customer_no", $data['customer_no']);

		$this->db->update("iw_customer", $data);

	}



	//customer 삭제(관리자)

	function income_delete_adm($customer_no) {

		$this->db->where("customer_no", $customer_no);

		$this->db->delete("iw_customer");

	}



	//customer 인증(관리자)

	function income_certi_update_adm($customer_no) {

		$this->db->where("customer_no", $customer_no);

		$this->db->set("certi_yn", 'Y');

		$this->db->update("iw_customer");

	}



	/* --------------------------------------------------------------------------------------------------

	 * 게시판 연동

	  ------------------------------------------------------------------------------------------------- */



	//게시판 customer 조회 목록수

	function total_bbs_select($customer_id, $where) {

		$sql = '';

		$sql .= "select a.*                                                                    \n";

		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

		$sql .= "      ,b.*                                                                    \n";

		$sql .= "  from (                                                                      \n";

		$sql .= "        select * from iw_bbs                                                  \n";

		$sql .= "         where customer_id = ?                                                  \n";

		$sql .= "           and notice_yn = 'N'                                                \n";

		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		$sql .= "      order by grp_idx desc, order_no asc                                     \n";

		$sql .= "       ) as a                                                                 \n";

		$sql .= " left outer join                                                              \n";

		$sql .= "       (                                                                      \n";

		$sql .= "        select * from iw_bbs_attach group by idx                              \n";

		$sql .= "       ) as b                                                                 \n";

		$sql .= " on a.idx = b.idx;                                                            \n";



		//파라미터 생성

		$sql_param = array();

		$sql_param[0] = $customer_id; //customer아이디

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}



		$query = $this->db->query($sql, $sql_param);



		return $query->num_rows();

	}



	//게시판 customer 조회

	function bbs_select($customer_id, $notice_yn = 'N', $limit_st, $limit_ed, $where) {

		$sql = '';

		$sql .= "select a.*                                                                    \n";

		$sql .= "      ,a.menu_code as bbs_menu_code                                           \n";

		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

		$sql .= "      ,b.*                                                                    \n";

		$sql .= "      ,a.idx                                                                  \n";

		$sql .= "  from (                                                                      \n";

		$sql .= "        select * from iw_bbs                                                  \n";

		$sql .= "         where customer_id = ?                                                  \n";

		$sql .= "           and notice_yn = ?                                                  \n";

		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		//일반, 공지 구분

		if ($notice_yn != 'Y') {

			$sql .= "      order by grp_idx desc, order_no asc                                 \n";

		} else {

			$sql .= "      order by created_dt desc                                            \n";

		}

		$sql .= "         limit ?, ?                                                           \n";

		$sql .= "       ) as a                                                                 \n";

		$sql .= " left outer join                                                              \n";

		$sql .= "       (                                                                      \n";

		$sql .= "        select * from iw_bbs_attach group by idx                              \n";

		$sql .= "       ) as b                                                                 \n";

		$sql .= " on a.idx = b.idx;                                                            \n";







		//파라미터 생성

		$sql_param = array();

		$sql_param[0] = $customer_id; //customer아이디

		$sql_param[count($sql_param)] = $notice_yn; //일반 공지 구분

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}

		$sql_param[count($sql_param)] = $limit_st;

		$sql_param[count($sql_param)] = $limit_ed;



		$query = $this->db->query($sql, $sql_param);



		return $query;

	}

	// 첨부파일 등록

	function insert_attach($data) {

		$this->db->insert("iw_customer_attach", $data);

		return $this->db->insert_id();

	}

	// 첨부파일 조회

	function select_attach($customer_no, $category = '') {

		if($category != '') {

			$sql = "select * from iw_customer_attach   \n";

			$sql .= "         where customer_no = ?        \n";

			$sql .= "           and category = ?  \n";

			//파라미터 생성

			$sql_param = array();

			$sql_param[0] = $customer_no;

			$sql_param[count($sql_param)] = $category;

		} else {

			$sql = "select * from iw_customer_attach   \n";

			$sql .= "         where customer_no = ?        \n";

			//파라미터 생성

			$sql_param = array();

			$sql_param[0] = $customer_no;

		}

		$query = $this->db->query($sql, $sql_param);



		return $query;

	}

	// 첨부파일 상세 조회

	function select_attach_detail($customer_no, $att_idx) {

		$sql = "select * from iw_customer_attach   \n";

		$sql .= "         where customer_no = ?        \n";

		$sql .= "           and att_idx = ?    \n";



		//파라미터 생성

		$sql_param = array();

		$sql_param[0] = $customer_no;	   //idx

		$sql_param[count($sql_param)] = $att_idx;   //첨부파일 idx



		$query = $this->db->query($sql, $sql_param);



		return $query;

	}

	// 첨부파일 삭제

	function delete_attach($customer_no, $att_idx) {

		$this->db->where("customer_no", $customer_no);

		$this->db->where("att_idx", $att_idx);

		$this->db->delete("iw_customer_attach");

	}

	function select_deposit($where){
		$this->db->select('*');
        $this->db->from('iw_deposit');
        $this->db->where($where); 
        $query = $this->db->get();
        return $query->result();
	}

	function sub_deposit_balance($deposit_id, $balance){
		$sql_param = array($balance, $deposit_id);
		$sql = "UPDATE iw_deposit SET deposit = deposit - ? WHERE id = ?";
		$query = $this->db->query($sql, $sql_param);
		return $query;
	}
}

?>