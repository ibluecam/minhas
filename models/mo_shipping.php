<?php



/* ------------------------------------------------

 * 파일명 : me_customer.php

 * 개  요 : customer 관련

  ------------------------------------------------ */

class Mo_shipping extends Model {



	function __construct() {

		parent::Model();



		$this->load->database('default');

	}

	function insert_shipping_charge($data){
		$date = date('Y-m-d H:i:s');
		$data['created_dt'] = $date;
		$data['modified_dt'] = $date;
		$this->db->insert("iw_shipping_charge", $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	/*** If you want to show actual price without commision of each seller pls give param $include_commission as false ***/
	function get_divided_price($port_id, $car_idx, $include_commission=true){
		$div_price = array();
		$div_price['fob_cost'] = 0;
		$div_price['freight_cost'] = 0;
		$div_price['inspection_cost'] = 0;
		$div_price['insurance_cost'] = 0;
		$total_freight = 0;
		$total_price = 0;
		$com_freight_cost = 250;
		$com_inspection_cost = 100;
		$com_insurance_cost = 50;
		$ceil_val = 50; //value to be ceiled
		/*** GET ESSENTIAL DETAIL OF THE CAR ***/
		$this->db->select('bbs.*, member.commission');
		$this->db->from('iw_bbs bbs');
		$this->db->join('iw_member member', 'member.member_no=bbs.car_owner', 'left');
		$this->db->where('bbs.idx', $car_idx);

		$bbs_query = $this->db->get();
		$bbs_result = $bbs_query->result();
		$car_detail = $bbs_result[0];

		/*** GET SHIPPING DETAIL OF THE CAR ***/
		$where_shipping = array('port_to'=>$port_id, 'country_from'=>$car_detail->country);
		$shipping_charges = $this->select_shipping_charge($where_shipping);

		if(count($shipping_charges)>0){
			$shipping_detail = $shipping_charges[0];

			/*** CALCULATE FREIGHT ***/
			if($shipping_detail->freight_cost_by=='model'){
				/*** CHECK IF MODEL IS IN THE LIST OF SET PRICE ***/
				$where_freight = array(
					'sc.port_to'=>$port_id,
					'sc.country_from'=>$car_detail->country,
					'fm.model_name'=>$car_detail->car_model
				);
				$freights_result = $this->select_freight_model($where_freight);
				if(count($freights_result)>0){
					$total_freight = $shipping_detail->cbm_value * $shipping_detail->price_per_cbm; /*** freight cost = CBM * price/CBM ***/
				}
			}elseif($shipping_detail->freight_cost_by=='dimension'){
				/*** CHECK IF DIMENSION IS SET ***/
				if(!empty($car_detail->car_width)&&!empty($car_detail->car_height)&&!empty($car_detail->car_length)){
					$total_freight = $shipping_detail->shipping_cost * ($car_detail->car_width/1000) * ($car_detail->car_height/1000) * ($car_detail->car_length/1000);/*** freight cost = shipping cost * width * height * length (in m)***/
				}
			}elseif($shipping_detail->freight_cost_by=='body_type'){
				/*** CHECK IF BODY TYPE IS IN THE LIST OF SET PRICE ***/
				$where_freight = array(
					'sc.port_to'=>$port_id,
					'sc.country_from'=>$car_detail->country,
					'bt.body_type_name'=>$car_detail->car_body_type
				);
				$freights_result = $this->select_freight_body_type($where_freight);
				if(count($freights_result)>0){
					$total_freight = $shipping_detail->shipping_cost; /*** freight cost = CBM * price/CBM ***/
				}
			}
			/*** CALCULATE TOTAL PRICE ***/
			if($total_freight>0){
				/*** CEILING ***/
				$d_freight = $total_freight /$ceil_val;
				$ceil_freight = ceil($d_freight) * $ceil_val;
				$total_freight = $ceil_freight;
				/*** SAVE DATA TO ARRAY ***/
				$div_price['freight_cost'] = $total_freight;
				$div_price['fob_cost'] = $car_detail->car_fob_cost;
				$div_price['inspection_cost'] = $shipping_detail->inspection_cost;
				$div_price['insurance_cost'] = $shipping_detail->insurance_cost;
				/*** IF THE OWNER GIVE COMMISION TO MOTORBB ***/
				if($include_commission==true&&$car_detail->commission=='1'){
					$div_price['freight_cost'] += $com_freight_cost;
					$div_price['inspection_cost'] += $com_inspection_cost;
					$div_price['insurance_cost'] += $com_insurance_cost;
				}
			}
		}
		return $div_price;
	}

	function get_total_price($port_id, $car_idx, $inspection_cost=true, $insurance_cost=true){
		
		$total_price = 0;
		$div_price = $this->get_divided_price($port_id, $car_idx);
		/*** CALCULATE TOTAL PRICE ***/
		if($div_price['freight_cost']>0){
			$total_price = $div_price['fob_cost'] + $div_price['freight_cost']; 
			if($inspection_cost==true){
				 $total_price += $div_price['inspection_cost'];
			}
			if($insurance_cost==true){
				 $total_price += $div_price['insurance_cost'];
			}
		}
		
		return $total_price;
	}

	function select_freight_body_type($where=array()){
		$this->db->select('*');
		$this->db->from('iw_shipping_charge sc');
		$this->db->join('iw_freight_body_type bt', 'bt.shipping_charge_id = sc.id', 'left');
		$this->db->where($where);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	function insert_freight_model($shipping_charge_id, $model_array=array()) {
		$query = false;
		$sql = '';
		$sql_param = array();
		$sql_value_arr = array();
		$sql_value_str = '';
		if(count($model_array>0)){
			
			foreach($model_array as $model_name){
				$sql_value_arr[] = "(?, ?)";
				$sql_param[] = $shipping_charge_id;
				$sql_param[] = $model_name; 
			}
			$sql_value_str = implode(', ', $sql_value_arr);
			$sql .= 'INSERT INTO iw_freight_model (shipping_charge_id, model_name) VALUES '.$sql_value_str;
			$query = $this->db->query($sql, $sql_param);
			
		}
			
		return $query;
	}
	function insert_freight_body_type($shipping_charge_id, $body_type_array=array()) {
		$query = false;
		$sql = '';
		$sql_param = array();
		$sql_value_arr = array();
		$sql_value_str = '';
		if(count($body_type_array>0)){
			
			foreach($body_type_array as $body_type_name){
				$sql_value_arr[] = "(?, ?)";
				$sql_param[] = $shipping_charge_id;
				$sql_param[] = $body_type_name; 
			}
			$sql_value_str = implode(', ', $sql_value_arr);
			$sql .= 'INSERT INTO iw_freight_body_type (shipping_charge_id, body_type_name) VALUES '.$sql_value_str;
			$query = $this->db->query($sql, $sql_param);
			
		}
			
		return $query;
	}
	function select_freight_model($where=array()){
		$this->db->select('*');
		$this->db->from('iw_shipping_charge sc');
		$this->db->join('iw_freight_model fm', 'fm.shipping_charge_id = sc.id', 'left');
		$this->db->where($where);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/*** if $model_name_arr is not set. all freight_model will be deleted by shipping charge id ***/
	function delete_freight_model($shipping_charge_id, $model_name_arr = array()){
		$this->db->where('shipping_charge_id', $shipping_charge_id);
		if(count($model_name_arr)>0){
			$this->db->where_in('model_name', $model_name_arr);
		}
		$query = $this->db->delete('iw_freight_model'); 
		return $query;
	}
	/*** if $body_type_arr is not set. all freight_model will be deleted by shipping charge id ***/
	function delete_freight_body_type($shipping_charge_id, $body_type_arr = array()){
		$this->db->where('shipping_charge_id', $shipping_charge_id);
		if(count($body_type_arr)>0){
			$this->db->where_in('body_type_name', $body_type_arr);
		}
		$query = $this->db->delete('iw_freight_body_type'); 
		return $query;
	}
	function delete_freight_model_beside($shipping_charge_id, $model_name_arr = array()){
		$this->db->where('shipping_charge_id', $shipping_charge_id);
		$this->db->where_not_in('model_name', $model_name_arr);
		$query = $this->db->delete('iw_freight_model'); 
		return $query;
	}
	function delete_freight_body_type_beside($shipping_charge_id, $body_type_arr = array()){
		$this->db->where('shipping_charge_id', $shipping_charge_id);
		$this->db->where_not_in('body_type_name', $body_type_arr);
		$query = $this->db->delete('iw_freight_body_type'); 
		return $query;
	}
	function select_count_freight_model($shipping_charge_id, $model_name_arr = array()){
		$this->db->select('model_name, COUNT(*) as model_count');
		$this->db->from('iw_freight_model');
		$this->db->where('shipping_charge_id', $shipping_charge_id);
		$this->db->where_in('model_name', $model_name_arr);
		$this->db->group_by('model_name');
		//echo $this->db->last_query();
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	function select_count_freight_body_type($shipping_charge_id, $body_type_arr = array()){
		$this->db->select('body_type_name, COUNT(*) as body_type_count');
		$this->db->from('iw_freight_body_type');
		$this->db->where('shipping_charge_id', $shipping_charge_id);
		if(count($body_type_arr)>0){
			$this->db->where_in('body_type_name', $body_type_arr);
		}
		$this->db->group_by('body_type_name');
		//echo $this->db->last_query();
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	// function insert($data) {
	// 	$this->db->insert("iw_shipping_charge", $data);
	// 	$insert_id = $this->db->insert_id();

	// 	//그룹번호 업데이트
	// 	$this->db->set("grp_idx", $this->db->insert_id());
	// 	$this->db->where("idx", $this->db->insert_id());
	// 	$this->db->update("iw_bbs");

	// 	return $insert_id;
	// }
	//insert currency
	// function insert_shipping_charge($data) {
	// 	$this->db->insert("iw_shipping_charge", $data);
	// 	$insert_id = $this->db->insert_id();

	// 	return $insert_id;
	// }
	
	//insert currency
	function update_shipping_charge($id, $data) {

		$this->db->where("id", $id);
		return $this->db->update("iw_shipping_charge", $data);

	}
	//customer 조회

	function shipping_select($limit_st, $limit_ed, $where) {


		$sql = '';

		$sql .= "   SELECT a.id, b.account_type, b.account_name, a.account_detail, a.currency_type, a.unit_price, a.tax_price, a.tax_flg, a.unit_count, 
								IF(b.account_type='Income',a.unit_total_price,null) AS total_income,
								IF(b.account_type='Expense',a.unit_total_price,null) AS total_expense,
								a.account_date
					FROM iw_account a
					LEFT JOIN iw_account_type b
					ON a.account_id = b.id";
		$sql .= "   where 1 \n";

		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		
		$sql.=" ORDER BY a.account_date DESC						\n" ;
		$sql.=" limit ?,?						\n" ;
		$sql .= " ;                                                  \n";

		//print_r($sql); print_r($where); exit();

		//파라미터 생성

		$sql_param = array();

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}

		$sql_param[count($sql_param)] = $limit_st;

		$sql_param[count($sql_param)] = $limit_ed;
		

		$query = $this->db->query($sql, $sql_param);
		$result = $query->result();

		return $result;

	}
	
	//customer 조회 목록수

	function total_shipping_select($where) {

		$sql = '';

		$sql .= "   select a.*, b.account_name, a.updated_dt \n";
		$sql .= "   from iw_account as a                                     \n";
		$sql .= "   left join iw_account_type as b \n";
		$sql .= "   on a.account_id = b.id                                    \n";
		$sql .= "   where 1 \n";

		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		$sql .= " ;                                                  \n";
		//파라미터 생성

		$sql_param = array();

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}

		

		$query = $this->db->query($sql, $sql_param);
		$result = $query->result();
		//print_r($sql); exit();

		return $result;

	}
	//회원 상세 조회(관리자)
	function shipping_select_detail_adm($id) {
		$sql = '';
		$sql .= "select a.*,b.account_name, b.account_type FROM   
				iw_account a
				LEFT JOIN iw_account_type b ON a.account_id = b.id
				WHERE a.id='$id'
		";

		$query = $this->db->query($sql, array($id));

		return $query;
	}

	function shipping_update_adm($data) {
		
        $sql = "select * from iw_bbs where idx = $data[car_idx]";
        $rs = mysql_query($sql);
        $result = mysql_fetch_array($rs);

        if(empty($result) ){
            //account_id = 1 : Buying Car(Expense) 
            if($data['account_id'] == 1 ){
            	echo "There is no matching car in car list. Please contact an adminstrator.";
            }else{
            	echo "This account type is not releated to Buying Car.<br> So it doesn't include a car index.";
            }
        	
        }else{

            // there is value in iw_account

            $bbs_data  = array();

            $bbs_data['unit_price'] = $data['unit_price'];
            $bbs_data['tax_price'] = $data['tax_price'];
            $bbs_data['unit_price_currency'] = $data['currency_type'];

            $where = $this->db->where("idx", $data['car_idx']);
            
            $this->db->update("iw_bbs", $bbs_data);

        }

        $data['updated_dt'] = func_get_date_time();

		$this->db->where("id", $data['id']);
		$this->db->update("iw_account", $data);

	}

	function shipping_delete_adm($idx) {
		$this->db->where("id", $idx);
		$this->db->delete("iw_account");
	}
	function delete_shipping_charge($id) {
		$this->db->where("id", $id);
		return $this->db->delete("iw_shipping_charge");
	}

    function delete_shipping_port($id) {
		$this->db->where("id", $id);
		return $this->db->delete("iw_port");
	}



	// function select_shipping_charge ($where = ''){
		
	// 	$sql = '';
		
	// 	$sql .= 'SELECT * FROM iw_shipping_charge';
		
	// 	$sql .= ' where id != ? ';
	// 	//검색 조건 생성
 //        if ($where != NULL) {
 //            foreach ($where as $key => $value) {
 //                $findIt = strpos($key, 'IWSEARCHVALS');

 //                if ($findIt === FALSE) {
 //                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
 //                        $sql .='  and  ' . $key . ' ? ';
 //                    } else {
 //                        $key = str_replace('IWSEARCHKEY', '', $key);
 //                        $sql .=' and ' . $key;
 //                    }
 //                }
 //            }
 //        }
	// 	$sql .= ' ORDER by id DESC ';
	// 	//파라미터 생성
 //        $sql_param = array();
 //        $sql_param[0] = ''; //메뉴코드
 //        if ($where != NULL) {
 //            foreach ($where as $key => $value) {
 //                if ($value == '')
 //                    continue;
 //                $sql_param[count($sql_param)] = $value;
 //            }
 //        }

	// 	$query = $this->db->query($sql,$sql_param);
		
	// 	return $query;
	// }
	
	//-----total rate num row---------------
	
	function select_shipping_charge($where=array()){
		$this->db->select('sc.*, cl.country_name as from_country_name, cl_des.country_name as to_country_name, p.country_iso as port_country, p.port_name');
		$this->db->from('iw_shipping_charge sc');
		$this->db->join('iw_country_list cl', "cl.cc=sc.country_from");
		$this->db->join('iw_port p', "sc.port_to=p.id");
		$this->db->join('iw_country_list cl_des', "cl_des.cc=p.country_iso");
		$this->db->where($where);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	
	function select_port($country_iso=null){
		$this->db->select('*');
		$this->db->from('iw_port');
		if($country_iso!=null){
			$this->db->where('country_iso', $country_iso);
		}
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}
	// function total_select_shipping_charge ($where = ''){
		
	// 	$sql = '';
		
	// 	$sql .= 'SELECT * FROM iw_shipping_charge';
		
	// 	$sql .= ' where id != ? ';
	// 	//검색 조건 생성
 //        if ($where != NULL) {
 //            foreach ($where as $key => $value) {
 //                $findIt = strpos($key, 'IWSEARCHVALS');

 //                if ($findIt === FALSE) {
 //                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
 //                        $sql .='  and  ' . $key . ' ? ';
 //                    } else {
 //                        $key = str_replace('IWSEARCHKEY', '', $key);
 //                        $sql .=' and ' . $key;
 //                    }
 //                }
 //            }
 //        }
	// 	$sql .= ' ORDER by id DESC ';
	// 	//파라미터 생성
 //        $sql_param = array();
 //        $sql_param[0] = ''; //메뉴코드
 //        if ($where != NULL) {
 //            foreach ($where as $key => $value) {
 //                if ($value == '')
 //                    continue;
 //                $sql_param[count($sql_param)] = $value;
 //            }
 //        }

	// 	$query = $this->db->query($sql,$sql_param);
		
	// 	return $query->num_rows();
	// }


	//insert buying price when register a car
    // function insert_shipping($data) {
    //     $data['created_dt'] = func_get_date_time();
        
    //     $this->db->insert("iw_shipping_charge", $data);
    //     $insert_id = $this->db->insert_id();

    //     return $insert_id;
    // }



    // function update_shipping($data) {
    //     $sql = "select * from iw_shipping_charge where id = $data[id]";
    //     $rs = mysql_query($sql);
    //     $result = mysql_fetch_array($rs);

    //     if(empty($result) ){
    //         //there is no value in iw_account
    //         $data['created_dt'] = func_get_date_time();

    //         $this->db->insert("iw_shipping_charge", $data);
    //         $insert_id = $this->db->insert_id();

    //         return $insert_id;

    //     }else{
    //         $data['modified_dt'] = func_get_date_time();
    //         $where = $this->db->where("id", $data['id']);
    //         $this->db->update("iw_shipping_charge", $data);

    //         //return $this->db->update("iw_account", $data);
            
    //     }

    // }

    function select_shipping_port($where=array()){
		$this->db->select('iw_port.*,cl.country_name');
		$this->db->from('iw_port');
		$this->db->join('iw_country_list cl','cl.cc=iw_port.country_iso');
		$this->db->where($where);
		$this->db->orderby('cl.country_name','asc');
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function insert_shipping_port($data){
		$this->db->insert("iw_port", $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	function update_shipping_port($id, $data) {

		$this->db->where("id", $id);
		return $this->db->update("iw_port", $data);

	}


}




?>