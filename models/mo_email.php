<?php

/* ------------------------------------------------
 * 파일명 : mo_email.php
 * 개  요 : 회원 관련
  ------------------------------------------------ */
?>
<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/libraries/PHPMailer/PHPMailerAutoload.php';
/*** INCLUDE FOR SENDGRID ***/
require $_SERVER['DOCUMENT_ROOT'].'/libraries/sendgrid/vendor/autoload.php';
require $_SERVER['DOCUMENT_ROOT'].'/libraries/sendgrid/lib/SendGrid.php';
/*** END INCLUDE FOR SENDGRID ***/
include_once $_SERVER['DOCUMENT_ROOT'].'/libraries/class-email-html.php';
        
class Mo_email extends Model{
    var $ci;
	var $sendgrid_user = "sengkean";
    var $sendgrid_key = "Se987654";
	function __construct() {

        parent::Model();
        include_once $_SERVER['DOCUMENT_ROOT'].'/config/sendgrid_config.php';
        $this->sendgrid_user = $sendgrid_user;
        $this->sendgrid_key = $sendgrid_key;
        $this->load->database('default');
    }

    function init_mail(){
        $mail = new PHPMailer;
        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'server.motorbb.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@motorbb.com';                 // SMTP username
        $mail->Password = '~PF!@BUtg)b*';                           // SMTP password
        $mail->SMTPSecure = 'ssl';
        $mail->CharSet = 'UTF-8';
        $mail->Encoding = '8bit';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                // TCP port to connect to

        $mail->From = 'noreply@motorbb.com';  
        $mail->FromName = 'MOTORBB';
       
        // $mail->addAddress('ellen@example.com');               // Name is optional
        //$mail->addReplyTo('noreply@motorbb.com', 'noreply@motorbb.com');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true); 
        return $mail;  
    }


    function init_sendgrid(){
        $sendgrid = new SendGrid('sengkean', 'Se987654');
        return $sendgrid;
    }
/*** SENDGRID FUNCTION ***/
    /*** USING SAMPLE: ***/

    function get_rec_list(){
        //https://api.sendgrid.com/api/newsletter/lists/email/get.json?list=Test&api_user=your_sendgrid_user&api_key=your_sendgrid_key
        $homepage = file_get_contents('https://api.sendgrid.com/api/newsletter/lists/email/get.json?list=Cambodia&api_user=sengkean&api_key=Se987654');
        $array = json_decode($homepage, true);
        return $array;
    }
    function curl_post($url, $posts){
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $posts);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);


        $response = curl_exec( $ch );
        return $response;
    }
    /*** USING SAMPLE: $array_response['inserted']; ***/
    function insert_email_to_list($list_name, $data=array()){
        $url = 'https://api.sendgrid.com/api/newsletter/lists/email/add.json';
        $list_name= urlencode($list_name);
        foreach($data as $d){
            $json_data = json_encode($d);
            $json_data = str_replace('[', '', $json_data);
            $json_data = str_replace(']', '', $json_data);
            $gen_data = urlencode($json_data);
            $data_param_arr[] = 'data[]='.$gen_data;
        }
        $data_param = implode($data_param_arr, '&');

        $myvars = '?list='.$list_name.'&'.$data_param.'&api_user='.$this->sendgrid_user.'&api_key='.$this->sendgrid_key;
        //echo $myvars;
        //$this->url_post($url, $myvars);

        $response = file_get_contents($url.$myvars);
        $array_response = json_decode($response, true);

        return $array_response;
    }

    function insert_recipient_list($list_name){
        $param = '';
        $param .= 'api_user='.$this->sendgrid_user.'&api_key='.$this->sendgrid_key;
        $param .= '&list='.urlencode($list_name);
        $result = file_get_contents('https://api.sendgrid.com/api/newsletter/lists/add.json?'.$param);
        return $result;
    }

    function select_recipient_list($list_name=null){
        $url = 'https://api.sendgrid.com/api/newsletter/lists/get.json?api_user='.$this->sendgrid_user."&api_key=".$this->sendgrid_key;
        if($list_name!=null){
            $url.="&list=".$list_name;
        }
        $rec_list_json = file_get_contents($url);
        $rec_list = json_decode($rec_list_json);
        return $rec_list;
    }
    function select_recipient_email($list_name){
        $rec_list_json = file_get_contents('https://api.sendgrid.com/api/newsletter/lists/email/get.json?api_user='.$this->sendgrid_user."&api_key=".$this->sendgrid_key."&list=".$list_name);
        $rec_list = json_decode($rec_list_json);
        return $rec_list;
    }
    function delete_recipient_email($list_name, $emails){
        $param = '';
        $email_param_arr = array();
        /** CONFIG ***/
        $url = 'https://api.sendgrid.com/api/newsletter/lists/email/delete.json';
        $param .= 'api_user='.$this->sendgrid_user.'&api_key='.$this->sendgrid_key;
        /** END CONFIG ***/
        $param .= '&list='.$list_name;
        foreach($emails as $email){
            $email_param_arr[] = 'email[]='.$email;
        }
        $email_param = implode("&", $email_param_arr);
        $param .= "&".$email_param;

        //$delete_result = file_get_contents('https://api.sendgrid.com/api/newsletter/lists/email/delete.json?api_user='.$this->sendgrid_user."&api_key=".$this->sendgrid_key."&list=".$list_name."&".$email_param);
        $delete_result = $this->curl_post($url, $param);
        
        return json_decode($delete_result);
    }

    function delete_style_element($html){
        $htmlStartPos = strpos($html,"<style>");
        $htmlEndPos = strpos($html,"</style>");

        if ($htmlStartPos && $htmlEndPos) {
            $htmlEndPos += 8; //remove <em> tag aswell
            $len = $htmlEndPos - $htmlStartPos;

            $html = substr_replace($html, '', $htmlStartPos, $len);
        }
        return $html;
    }

    function insert_marketing_email($html,$subject,$name){
        $param = '';
        /** CONFIG ***/
        $url = 'https://api.sendgrid.com/api/newsletter/add.json';
        $param .= 'api_user='.$this->sendgrid_user.'&api_key='.$this->sendgrid_key;
        /** END CONFIG ***/
        
        $plain_text = $this->delete_style_element($html);
        $plain_text = strip_tags($plain_text, '<br><a>');

        $param .= '&html='.urlencode($html);
        $param .= '&identity=Motorbb Offer';
        $param .= '&name='.$name;
        $param .= '&subject='.urlencode($subject);
        $param .= '&text='.urlencode($plain_text);
        echo $this->curl_post($url, $param);

    }

    function insert_recipient_to_marketing_email($recipient_name,$name){
        $param = '';
        /** CONFIG ***/
        $url = 'https://api.sendgrid.com/api/newsletter/recipients/add.json';
        $param .= 'api_user='.$this->sendgrid_user.'&api_key='.$this->sendgrid_key;
        /** END CONFIG ***/

        $param .= '&name='.$name;
        $param .= '&list='.$recipient_name;
        echo $this->curl_post($url, $param);

    }

    function insert_schedule_to_marketing_email($date,$hour,$minute,$name,$send_now){
        $param = '';
        /** CONFIG ***/
        $url = 'https://api.sendgrid.com/api/newsletter/schedule/add.json';
        $param .= 'api_user='.$this->sendgrid_user.'&api_key='.$this->sendgrid_key;
        /** END CONFIG ***/
        $param .= '&name='.$name;

    // "display": "GMT+07:00", 2015-10-02T14:00:00+07:00
    if($send_now=='sechedule_date_time' && $date !=''){
            $param .= '&at='.urlencode($date.'T'.$hour.':'.$minute.':00+07:00');
        }

        echo $this->curl_post($url, $param);

    }

/*** END SENDGRID FUNCTION ***/
    // function init_mailist(){
    //     $mail = new PHPMailer;
    //     //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    //     $mail->isSMTP();                                      // Set mailer to use SMTP
    //     $mail->Host = 'bohor.arvixe.com';  // Specify main and backup SMTP servers
    //     $mail->SMTPAuth = true;                               // Enable SMTP authentication
    //     $mail->Username = 'ads@motorbb.com';                 // SMTP username
    //     $mail->Password = '_l!-#JBb*GRV';                           // SMTP password
    //     $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    //     $mail->Port = 465;                                // TCP port to connect to

    //     $mail->From = 'ads@motorbb.com';  
    //     $mail->FromName = 'MOTORBB';
       
    //     // $mail->addAddress('ellen@example.com');               // Name is optional
    //     //$mail->addReplyTo('noreply@motorbb.com', 'noreply@motorbb.com');
    //     // $mail->addCC('cc@example.com');
    //     // $mail->addBCC('bcc@example.com');

    //     // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //     // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    //     $mail->isHTML(true); 
    //     return $mail;  
    // }

    // function init_mailgun_default(){
    //     $mail = new PHPMailer;
    //     //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    //     $mail->isSMTP();                                      // Set mailer to use SMTP
    //     $mail->Host = 'smtp.mailgun.org';  // Specify main and backup SMTP servers
    //     $mail->SMTPAuth = true;                               // Enable SMTP authentication
    //     $mail->Username = 'postmaster@sandboxbc7fbe362fe1472089fbbb98c1ffee6d.mailgun.org';                 // SMTP username
    //     $mail->Password = '34679e1969ed63d3fe6bc533c1e8ed60';                           // SMTP password
    //     $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    //    //$mail->Port = 465;                                // TCP port to connect to

    //     $mail->From = 'noreply@motorbb.com';  
    //     $mail->FromName = 'MOTORBB';
       
    //     // $mail->addAddress('ellen@example.com');               // Name is optional
    //     //$mail->addReplyTo('noreply@motorbb.com', 'noreply@motorbb.com');
    //     // $mail->addCC('cc@example.com');
    //     // $mail->addBCC('bcc@example.com');

    //     // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //     // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    //     $mail->isHTML(true); 
    //     return $mail;  
    // }
    // function init_mailgun(){
    //     $mail = new PHPMailer;
    //     //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    //     $mail->isSMTP();                                      // Set mailer to use SMTP
    //     $mail->Host = 'smtp.mailgun.org';  // Specify main and backup SMTP servers
    //     $mail->SMTPAuth = true;                               // Enable SMTP authentication
    //     $mail->Username = 'noreply@motorbb.com';                 // SMTP username
    //     $mail->Password = '1d93b11ad44b4244b18be5d5d54b1bbc';                           // SMTP password
    //     $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    //    //$mail->Port = 465;                                // TCP port to connect to

    //     $mail->From = 'noreply@motorbb.com';  
    //     $mail->FromName = 'MOTORBB';
       
    //     // $mail->addAddress('ellen@example.com');               // Name is optional
    //     //$mail->addReplyTo('noreply@motorbb.com', 'noreply@motorbb.com');
    //     // $mail->addCC('cc@example.com');
    //     // $mail->addBCC('bcc@example.com');

    //     // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //     // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    //     $mail->isHTML(true); 
    //     return $mail;  
    // }

    function send_thank_for_contact($receiver_email, $receiver_name='User', $country='', $mobile_no='', $message, $link=''){
        $html = '';
        $mail = $this->init_mail();

        $mail->addAddress($receiver_email, $receiver_name);     // Add a recipient
        $mail->addCustomHeader('X-Mailgun-Campaign-Id', 'fq27d');
        $mail->addReplyTo('info@motorbb.com', 'info@motorbb.com');
        $mail->Subject = 'Thank you for contacting us';
        $html .= '<html><head></head><body>';
        $html .='
            Thank you for contacting us.<br/><br/>
            Your message has been successfully sent. We will contact you as soon as we review your message.<br/><br/>
            Your Inquiry: <br/>
            Country: '.$country.'<br/>
            Mobile Number: '.$mobile_no.'<br/>
            Message: '.$message.'<br/>
            Referent Link: <a href="'.$link.'">'.$link.'</a>

        ';
        $html.= '</body></html>';
        $mail->Body    = $html;
        
        
        sleep(1);
        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }

    function send_multiple_special_offer($c_email, $html){            
            // if(substr($customer_email, -11)=='motorbb.com'){
            //     $mail = $this->init_mail();
            // }else{
        $fail_count = 0;
        $total_email = count($c_email);
        $sendgrid = $this->init_sendgrid();
        /*** SPLIT EMAIL TO 1000 PER POST***/
        $split_emails = array_chunk($c_email, 1000, true);
        /*** GENERATE EMAIL HTML ***/
        //$email_html = new EmailHtml($html);
        //$attachments = $email_html->attachments;
        foreach($split_emails as $customer_email){
            $mail = new SendGrid\Email();
            foreach ($customer_email as $email) {
                // /$mail->addTo($row->customer_email);
                $mail->addSmtpapiTo($email);
            } 

            $mail->setFrom('noreply@motorbb.com')
                    ->setFromName('Motorbb')
                    ->setSubject('Special Offer from Motorbb Car Trader')
                    ->setText('Special Offer')
                    ->setHtml($html);
            /*** Add Unsubscribe Link ***/
            $mail->smtpapi->addFilter('subscriptiontrack', 'enable', 1);
            $mail->smtpapi->addFilter('subscriptiontrack', 'replace', '[unsubscribe_url]');
            // $mail->smtpapi->addFilter('dkim', 'domain', 'motorbb.com');
            // $mail->smtpapi->addFilter('dkim', 'use_from', 1);
            // $mail->smtpapi->addFilter('subscriptiontrack', 'text/plain', 'If you would like to unsubscribe and stop receiving these emails click here: <% %>.');
            // foreach($attachments as $att){
            //     $mail->addAttachment($att['des'], $att['name'], $att['cid']);
            // }
            
            //echo $email_html->html;
           
            try {
                $sendgrid->send($mail);
            } catch(\SendGrid\Exception $e) {
                echo $e->getCode();
                foreach($e->getErrors() as $er) {
                    $fail_count +=1;
                    echo $er;
                }
            }
            // $mail = $this->init_mailgun();
            // $mail->addCustomHeader('X-Mailgun-Campaign-Id', 'fq27d');
            // $mail->Subject = 'Special Offer from Motorbb Car Trader';
            // $mail->Body    = $html;

            // foreach ($customer_email as $row) {

            //     $mail->addAddress($row->customer_email, $row->$customer_email);     // Add a recipient
            //     if(!$mail->send()) {
            //         $fail_count +=1;
            //     }
            //     $mail->clearAddresses();
            // } 
            
        }
        $result['fail_count'] = $fail_count;
        $result['total_email'] = $total_email;
        return $result;
    }

    function send_special_offer($c_email, $html){

        $split_emails = array_chunk($c_email, 1000, true);
        $sendgrid = $this->init_sendgrid();
        foreach($split_emails as $customer_email){
            $mail = new SendGrid\Email();
            //$email_html = new EmailHtml($html);
            //$html = str_get_html('<div id="hello">Hello</div><div id="world">World</div>');
            //echo $html;
            //$attachments = $email_html->attachments;

            foreach ($customer_email as $email) {
                $mail->addSmtpapiTo($email);
            } 

            //$mail->addSmtpapiTo($customer_email);

            $mail->setFrom('noreply@motorbb.com')
                ->setFromName('Motorbb')
                ->setSubject('Special Offer from Motorbb Car Trader')
                ->setText('Special Offer')
                ->setHtml($html);
            /*** Add Unsubscribe Link ***/
            $mail->smtpapi->addFilter('subscriptiontrack', 'enable', 1);
            $mail->smtpapi->addFilter('subscriptiontrack', 'replace', '[unsubscribe_url]');
                //var_dump($email_html->attachments);
            // foreach($attachments as $att){
            //     $mail->addAttachment($att['des'], $att['name'], $att['cid']);
            // }

            //echo $email_html->html;
            //var_dump($email_html->attachments);

            
            try {
                $sendgrid->send($mail);
            } catch(\SendGrid\Exception $e) {
                echo $e->getCode();
                foreach($e->getErrors() as $er) {
                    echo $er;
                }
            }
        }
        // $mail->addAddress($customer_email, $customer_email);     // Add a recipient
        // $mail->addReplyTo('noreply@motorbb.com', 'noreply@motorbb.com');
        // $mail->Subject = 'Special Offer from Motorbb Car Trader';
        // $mail->Body    = $html;
    
        // sleep(1);
        // if(!$mail->send()) {
        //     return false;
        // } else {
        //     return true;
        // }
    }


    // function send_sign_email(){
    //     $mail = new PHPMailer();
    //     $dn = array(
    //         'countryName' => 'KR',
    //         'stateOrProvinceName' => 'Korea',
    //         'localityName' => 'Motorbb',
    //         'organizationName' => 'Motorbb',
    //         'organizationalUnitName' => 'PHPMailer',
    //         'commonName' => 'Motorbb',
    //         'emailAddress' => 'noreply@motorbb.com'
    //     );
    //     $password = 'password';
    //     $certfile = 'certfile.txt';
    //     $keyfile = 'keyfile.txt';

    //     $pk = openssl_pkey_new();
    //     $csr = openssl_csr_new($dn, $pk);
    //     $cert = openssl_csr_sign($csr, null, $pk, 1);
    //     openssl_x509_export($cert, $certout);
    //     file_put_contents($certfile, $certout);
    //     openssl_pkey_export($pk, $pkeyout, $password);
    //     file_put_contents($keyfile, $pkeyout);

    //     // $body = 'Ahoj vsici';
    //     // $mail->AddAddress('xxx@xxx.cz');
    //     // $mail->Subject = "PHPMailer Test";
    //     // $mail->Sign($certfile, $keyfile, $password);
    //     // $mail->MsgHTML($body);
    //     // $mail->IsSMTP();
    //     // $mail->Host = 'localhost';
    //     // $mail->Port = 2500;
    //     // $mail->Send();

    // }

    // function send_register($customer_email,$html,$verificationCode){
    //     $mail = $this->init_mail();
    //     $mail->addAddress($customer_email, $customer_email);     // Add a recipient
    //     $mail->addCustomHeader('X-Mailgun-Campaign-Id', 'fq27d');
    //     $mail->addCustomHeader('Return-Path', 'noreply@motorbb.com');
    //     //$mail->Sign('certfile.txt', 'keyfile.txt', 'password');
    //     $mail->addReplyTo('noreply@motorbb.com', 'noreply@motorbb.com');
         
    //     $mail->Subject = 'Email Confirmation from Motorbb';
    //     $mail->Body    = $html;

    //     sleep(1);
    //     if(!$mail->send()) {
    //         return false;
    //     } else {
    //         return true;
    //     }
    // }
    function send_register($customer_email, $verificationCode=NULL){ 
        $from   = "MOTORBB <noreply@motorbb.com>";
        $message = "";
        $subject  = "Email Confirmation from Motorbb";
        $boundary = str_replace(" ", "", date('l jS \of F Y h i s A'));
        $newline  = "\r\n";
         
        $headers = "From: $from$newline".
                   "MIME-Version: 1.0$newline".
                   "Content-Type: multipart/alternative;".
                   "boundary = \"$boundary\"$newline$newline".
                   "--$boundary$newline".
                   "Content-Type: text/html; charset=ISO-8859-1$newline".
                   "Content-Transfer-Encoding: base64$newline$newline";
        /*** CREATE EMAIL HTML ***/
        $html=file_get_contents('email_template/register.html', true);
        $site_url = $this->siteURL();
        $link = $site_url."/?c=user&m=enter_info_confirm&vcode=".$verificationCode;
        $html = str_replace('[::REGISTER_LINK::]', $link, $html);
        $headers .= rtrim(chunk_split(base64_encode($html)));
        // $plain_text = strip_tags($html, '<br><a>');
        // //Plain text body
        // $headers .= $newline.$newline."--" . $boundary . $newline.$newline;
        // $headers .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
        // $headers .= $plain_text;
        // $headers .= $newline.$newline."--" . $boundary . $newline.$newline;
        mail($customer_email,$subject,$message,$headers);
    }
    function siteURL() {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'];
        return $protocol . $domainName;
    }
    // function send_register($customer_email,$html,$verificationCode){ 
    //     $from   = "MOTORBB <noreply@motorbb.com>";
    //     $message = "";
    //     $subject  = "Email Confirmation from Motorbb";
    //     $boundary = str_replace(" ", "", date('l jS \of F Y h i s A'));
    //     $newline  = "\r\n";
         
    //     $headers = "From: $from$newline".
    //                "MIME-Version: 1.0$newline".
    //                "Content-Type: multipart/alternative;".
    //                "boundary = \"$boundary\"$newline$newline".
    //                "Content-Type: text/html; charset=ISO-8859-1$newline".
    //                "Content-Transfer-Encoding: base64$newline$newline"
    //                ;
         
    //     $headers .= rtrim(chunk_split(base64_encode($html)));

    //     $plain_text = strip_tags($html, '<br><a>');
    //     //Plain text body
    //     // $message .= $newline.$newline . $boundary . $newline.$newline;
    //     // $message .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
    //     // $message .= $plain_text;
    //     // $message .= $newline.$newline . $boundary . $newline.$newline;
    //     mail($customer_email,$subject,$message,$headers);
    // }
    function send_register_car_list($customer_email,$html){
        $mail = $this->init_mail();
        $mail->addAddress($customer_email, $customer_email);     // Add a recipient
        $mail->addCustomHeader('X-Mailgun-Campaign-Id', 'fq27d');
        $mail->addReplyTo('noreply@motorbb.com', 'noreply@motorbb.com');
        $mail->Subject = 'Car from Motorbb Car Trader';
        $mail->Body    = $html;

        sleep(1);
        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }
	
	function send_contact_seller_car($email, $sale_email, $html){
        $mail = $this->init_mail();
        //$mail->addAddress($email, $email); 
		$mail->addAddress($sale_email, $sale_email);     // Add a recipient
        $mail->addCustomHeader('X-Mailgun-Campaign-Id', 'fq27d');
        $mail->addReplyTo($email, $email);
        $mail->Subject = 'Motorbb.com: Inquiry from Customer';
        $mail->Body    = $html;
		
		//var_dump($mail);exit();
		
        sleep(1);
        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }
    function sendResetPassword($customer_email,$html){
        $from   = "MOTORBB <noreply@motorbb.com>";

        $subject  = "Reset password from Motorbb!";
        $boundary = str_replace(" ", "", date('l jS \of F Y h i s A'));
        $newline  = "\r\n";
         
        $headers = "From: $from$newline".
                   "MIME-Version: 1.0$newline".
                   "Content-Type: multipart/alternative;".
                   "boundary = \"$boundary\"$newline$newline".
                   "--$boundary$newline".
                   "Content-Type: text/html; charset=ISO-8859-1$newline".
                   "Content-Transfer-Encoding: base64$newline$newline";
         
        $headers .= rtrim(chunk_split(base64_encode($html)));
         
        mail($customer_email,$subject,"",$headers);
    }
    
    // function sendResetPassword($customer_email,$html){
    //     $mail = $this->init_mail();
    //     $mail->Mailer = "smtp";
    //     $mail->addAddress($customer_email, $customer_email);     // Add a recipient
        
        
        // $mail->DKIM_domain = 'motorbb.com';
        // $mail->DKIM_private = 'id_dsa.key';
        // $mail->DKIM_selector = 'phpmailer';
        // $mail->DKIM_passphrase = 'L#PFQ{&8vBwk';
        // $mail->DKIM_identifier = $mail->From;

    //     $mail->addCustomHeader('X-Mailgun-Campaign-Id', 'fq27d');
    //     $mail->MessageDate = 'Tue, 18 Aug 2015 16:10:20 +0700';
    //     $mail->addReplyTo('noreply@motorbb.com', 'noreply@motorbb.com');
    //     $mail->Subject = 'Reset password from Motorbb!';
    //     $mail->Body    = $html;

    //     sleep(1);
    //     if(!$mail->send()) {
    //         return false;
    //     } else {
    //         return true;
    //     }
    // }
	// function sendOrderNotification($email, $name, $html){
        
 //        $this->init_noreply();
 //        $this->mail->addAddress($email, $name);     // Add a recipient
 //        $header = '<html><head></head><body>';
 //        $html= $header.$html;
      
 //        $this->mail->Subject = 'IBLAUDA.COM: Car Order Notification';
 //        $this->mail->Body    = $html;
        
        
 //        $html.= '</body></html>';
 //        if(!$this->mail->send()) {
 //            return false;
 //        } else {
 //            return true;
 //        }
 //    }
}
?>
