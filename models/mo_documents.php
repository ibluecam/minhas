<?php


/* ------------------------------------------------

 * 파일명 : mo_bbs.php

 * 개  요 : 게시판 관리

  ------------------------------------------------ */


class Mo_documents extends Model {



	function __construct() {

		parent::Model();



		$this->load->database('default');

	}


	function delete_bl_by_no($bl_nos){
		$sql = '';
		$sql_param = array();
		$bind_marks = array();
		
		foreach($bl_nos as $bl_no){
			$sql_param[] = $bl_no;
			$bind_marks[]= "?";
		} 
		$where_in  = implode(', ', $bind_marks);
		$sql .= "UPDATE iw_bbs SET 
					dhl_no=NULL,
					bl_no=NULL,
					vessel_name=NULL,
					discharge=NULL,
					port_leaved_date=NULL,
					consignee_info=NULL,
					icon_status = 'reserved'
				WHERE bl_no IN ({$where_in}) ";

		$query = $this->db->query($sql, $sql_param);
		return $query;
	}
	//게시판 조회 목록수

	function total_select() {

		$sql = '';

		$sql .= "        select * from iw_bbs                                                  \n";

		$sql .= "         where bl_no != ''                                                  \n";
		
		$sql .= "           group by bl_no														\n";
	
		$query = $this->db->query($sql);
		
/*		print_r($sql);
		exit;*/
		/*var_dump($query); exit();*/
		return $query->num_rows();
	}

	//게시판 조회
	function select($bl_no, $limit_st, $limit_ed, $where) {
	
		$sql_param = array();


		$sql = '';

		$sql .= "		select a.bl_no, a.vessel_name, a.discharge, a.port_leaved_date, b.customer_name, c.total_file, count(a.bl_no) as total_car              \n";
		
		$sql .= "		from (iw_bbs a LEFT OUTER JOIN iw_customer b on a.customer_no = b.customer_no)	    	\n";


		$sql .= "       LEFT OUTER JOIN (select bl_no,count(bl_no) as total_file from iw_documents_attach group by bl_no) c  			\n";
		
		$sql .= "       ON a.bl_no = c.bl_no 											\n";

		
		if($bl_no !=''){

		//$sql_param[0] = $bl_no;
		
		$sql .= "         	where a.bl_no = ?                                                  \n";
		
		}else{
		
		$sql .= "         	where a.bl_no != ''                                                  \n";
		
		}


		//검색 조건 생성
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						//$sql .='  and  ' . $key . ' ? '.' AND `car_visible`=\'True\' ';
						$sql .='  and  ' . $key . ' '.'"'. $value .'"'. ' AND `car_visible`=\'True\' ';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key.' AND `car_visible`=\'True\' ';
					}
				}
			}
		}
		
		
		$sql .= "           group by a.bl_no														\n";
		
		$sql .= "           order by a.port_leaved_date desc													\n";
		
		$sql .= "           limit ?, ?  											\n";

		
		//파라미터 생성
		
		$sql_param[count($sql_param)] = $limit_st;
		$sql_param[count($sql_param)] = $limit_ed;	

		$query = $this->db->query($sql, $sql_param);

		//print_r($query->num_rows); exit();
/*		print_r($sql); 
		var_dump($sql_param);

		var_dump($query);

		echo "<pre>";
		var_dump($this->db);
		echo "</pre>"; exit();*/

		return $query;

	}

	//게시판 첨부파일 조회
	function select_attach($idx) {
	
		//파라미터 생성
		$sql_param = array();
		
		$sql = "select * from iw_documents_attach   \n";
		
		$sql .= "         where bl_no = ?        \n";
		
		
		if( is_array($idx)){
			
			if(sizeof($idx) > 1){
			
			foreach($idx as $value){
			
				if(count($sql_param) == 0){
				
					$sql_param[0] = $value; //메뉴코드
				
				}else{
				
					$sql .= "           OR idx = ?  \n";
					
					$sql_param[count($sql_param)] = $value; //메뉴코드
					
				}
			
			}
			
			}else{
				
				$sql_param[0] = $idx[0];    /////////////////////////////////////////////
				
			}
		}else{
		
			$sql_param[0] = $idx;
		
		}

		$query = $this->db->query($sql, $sql_param);
		
		if( is_array($idx)){

			return $query->num_rows;
		}else{
			return $query;
		}
	}

	//게시판 첨부파일 상세 조회
	function select_attach_detail($bl_no, $att_idx) {
		$sql = "select * from iw_documents_attach \n";
		$sql .= "         where bl_no = ?        \n";
		$sql .= "           and att_idx = ?    \n";

		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $bl_no;	   //idx
		$sql_param[count($sql_param)] = $att_idx;   //첨부파일 idx

		$query = $this->db->query($sql, $sql_param);

		return $query;
	}

	//게시판 상세 조회
	function select_detail($idx) {
	
		$sql = '';
		$sql .= "select *                                                                   \n";
		
		$sql .= "from iw_documents                                                                   \n";
		
		$sql .= "where idx = ?                                                                   \n";
		

		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $idx;	   //idx
		
		$query = $this->db->query($sql, $sql_param);

		return $query;
	}

	
	// select document by bl_no
	
	function select_bl($bl_novalu){
	
	$sql = '';
	
	$sql = "SELECT idx FROM iw_documents WHERE bl_no='".$bl_novalu."' \n";
	
		$query = $this->db->query($sql);

		return $query;
	}
	
	
	// select document by bl_no detail
	
	function select_bl_detail($idx){
	
		$sql = '';
		
		$sql .= "select *                                                                   \n";
		
		$sql .= "from iw_documents_bl                                                                   \n";
		
		$sql .= "where idx = ?                                                                   \n";
		
		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $idx;	   //idx
		
		$query = $this->db->query($sql, $sql_param);

		return $query;
	}



	function select_bl_car($bl_no){
	
		$sql = '';
		
		$sql .= "select *                                                                   \n";
		
		$sql .= "from iw_bbs                                                                   \n";
		
		$sql .= "where bl_no = ?                                                                   \n";
		
		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $bl_no;	   //idx
		
		$query = $this->db->query($sql, $sql_param);

		return $query;
	}





	
	//게시판 등록
	function insert($data) {
		$this->db->insert("iw_bbs", $data);
		$insert_id = $this->db->insert_id();

		//그룹번호 업데이트
		$this->db->set("grp_idx", $this->db->insert_id());
		$this->db->where("idx", $this->db->insert_id());
		$this->db->update("iw_bbs");

		return $insert_id;
	}

	//게시판 수정
	function update($data) {
		$this->db->where("bl_no", $data['bl_no']);
		$this->db->update("iw_bbs", $data);
	}

	//게시판 첨부파일 등록
	function insert_attach($data) {
		$this->db->insert("iw_documents_attach", $data);
		return $this->db->insert_id();
	}


	//게시판 첨부파일 삭제
	function delete_attach($bl_no, $att_idx, $menu_code) {
		$this->db->where("bl_no", $bl_no);
		$this->db->where("att_idx", $att_idx);
		$this->db->where("menu_code", $menu_code);
		return $this->db->delete("iw_documents_attach");
	}

}
?>
