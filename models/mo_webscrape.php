<?php

/* ------------------------------------------------
 * 파일명 : me_member.php
 * 개  요 : 회원 관련
  ------------------------------------------------ */
?>
<?php

class Mo_webscrape extends Model {

	function __construct() {
		parent::Model();

		$this->load->database('default');
	}
	public function get_member_detail_by_webname($web_name){
		$where['website_name'] = $web_name;
       	$this->db->select('w.member_no, member.member_country, w.fob_commission');
       	$this->db->from('webscrape as w');
       	$this->db->join('iw_member as member', 'w.member_no = member.member_no');
       	$this->db->where($where);
       	$query = $this->db->get();
       	//echo $this->db->last_query();
       	$result = $query->result();
       	return $result[0];
    }
}
?>
