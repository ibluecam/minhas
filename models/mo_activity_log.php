<?php

/* ------------------------------------------------

mo_activity_log.php

  ------------------------------------------------ */

class Mo_activity_log extends Model {

    function __construct() {

        parent::Model();



        $this->load->database('default');
    }

    function select($where=array()){
        $this->db->select('a.*, m.grade_no, m.member_first_name, m.member_id');
        $this->db->from('iw_activity_log as a');
        $this->db->join('iw_member as m', 'm.member_no = a.author');
        $this->db->where($where);
         
        $query = $this->db->get();
       
        $result = $query->result();

        return $result;
    }

	function insert($data){
        $result = $this->db->insert("iw_activity_log", $data);
        
        return $result;
    }	

	function delete($id,$data) {
        $this->db->where("id", $id);
        return $this->db->update("iw_activity_log", $data);
    }
		

}

?>
