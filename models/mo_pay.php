<?php

/* ------------------------------------------------
 * 파일명 : mo_pay.php
 * 개  요 : 전자결제
  ------------------------------------------------ */
?>
<?php

class Mo_pay extends Model {

	function __construct() {
		parent::Model();

		$this->load->database('default');
	}

//결제결과 정보 등록
	function insert($data) {
		$this->db->insert("iw_pay_lgtelecom", $data);
		$insert_id = $this->db->insert_id();

		return $insert_id;
	}

//재수강 여부를 조회한다.
	function select_re_course_yn($lgd_buyerid, $lgd_productcode) {
		$return_val = '';

		$sql = "select *                     \r\n";
		$sql .= "  from iw_pay_lgtelecom      \r\n";
		$sql .= " where lgd_buyerid = ?       \r\n";
		$sql .= "   and lgd_productcode = ?   \r\n";
		$sql .= " order by created_dt desc    \r\n";
		$sql .= " limit 0, 1;                 \r\n";

		$query = $this->db->query($sql, array($lgd_buyerid, $lgd_productcode));

		if ($query->num_rows() > 0) {
			return $return_val = 'Y';
		} else {
			return $return_val = 'N';
		}
	}

//결제정보 조회 목록수
	function total_payment_select($where) {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,b.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_pay_lgtelecom                                        \n";
		$sql .= "         where seq_no = seq_no                                                \n";
//검색 조건 생성
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						$sql .='  and  ' . $key . ' ? ';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key;
					}
				}
			}
		}
		$sql .= "      order by created_dt desc                                                \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.lgd_buyerid = b.member_id;                                              \n";

//파라미터 생성
		$sql_param = array();
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				if ($value == '')
					continue;
				$sql_param[count($sql_param)] = $value;
			}
		}

		$query = $this->db->query($sql, $sql_param);

		return $query->num_rows();
	}

//결제정보 조회
	function payment_select($limit_st, $limit_ed, $where) {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,b.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_pay_lgtelecom                                        \n";
		$sql .= "         where seq_no = seq_no                                                \n";
//검색 조건 생성
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						$sql .='  and  ' . $key . ' ? ';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key;
					}
				}
			}
		}

		$sql .= "      order by created_dt desc                                                \n";
		$sql .= "         limit ?, ?                                                           \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.lgd_buyerid = b.member_id;                                              \n";

//파라미터 생성
		$sql_param = array();
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				if ($value == '')
					continue;
				$sql_param[count($sql_param)] = $value;
			}
		}
		$sql_param[count($sql_param)] = $limit_st;
		$sql_param[count($sql_param)] = $limit_ed;

		$query = $this->db->query($sql, $sql_param);

		return $query;
	}

//결제정보 상세 조회
	function payment_select_detail($member_id) {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,a.created_dt as pay_dt                                                 \n";
		$sql .= "      ,b.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_pay_lgtelecom                                        \n";
		$sql .= "         where lgd_buyerid = ?                                                \n";
		$sql .= "         order by created_dt desc                                             \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.lgd_buyerid = b.member_id;                                              \n";

		$query = $this->db->query($sql, array($member_id));

		return $query;
	}

//결제정보 상세 조회 (관리자)
	function payment_select_detail_adm($seq_no) {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,a.created_dt as pay_dt                                                 \n";
		$sql .= "      ,b.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_pay_lgtelecom                                        \n";
		$sql .= "         where seq_no = ?                                                     \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.lgd_buyerid = b.member_id;                                              \n";

		$query = $this->db->query($sql, array($seq_no));

		return $query;
	}

//결제 정보 수정
	function payment_update_adm($data) {
		$this->db->where("seq_no", $data['seq_no']);
		$this->db->update("iw_pay_lgtelecom", $data);
	}

	function get_customer_distribution($deposit_id){
		$sql = "SELECT * 
			FROM iw_payment
			WHERE from_deposit_id = ?
		";
		$sql_param = array($deposit_id);
		$query = $this->db->query($sql, $sql_param);
		$result = $query->result();
		return $result;
	}
	function adm_distribute_customer($customer_distribute, $currency_type, $deposit_id){
		$value_arr = array();
		$sql_param = array();
		foreach($customer_distribute as $key=>$value){
			if(!empty($value)){
				$value_arr[]="(?, ?, ?, NOW(), ?)";
				$sql_param[]=$key;
				$sql_param[]=$value;
				$sql_param[]=$currency_type;
				$sql_param[]=$deposit_id;
			}
		}
		$sql_value=implode(", ", $value_arr);
		$sql = "
			INSERT INTO iw_payment (customer_no, car_deposit, currency_type, deposit_dt, from_deposit_id) VALUES 
			$sql_value
		";
		$query = $this->db->query($sql, $sql_param);
		
		return $query;
	}
}
?>