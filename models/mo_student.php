<?php



/* ------------------------------------------------

 * 파일명 : me_customer.php

 * 개  요 : customer 관련

  ------------------------------------------------ */

class Mo_student extends Model {



	function __construct() {

		parent::Model();



		$this->load->database('default');

	}
	
	
	function total_select_student($where) {

		$sql = '';

		$sql .= "SELECT * FROM tblstudent   \n";

		$sql .= " where id is not null   \n";

//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		$sql .= "      order by id desc \n";



//파라미터 생성

		$sql_param = array();



		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}



		$query = $this->db->query($sql, $sql_param);



		return $query->num_rows();

	}



//팝업 조회

	function select_student($limit_st, $limit_ed, $where) {

		$sql = '';

		$sql .= "SELECT *  FROM tblstudent \n";

		$sql .= " where id is not null   \n";

//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		$sql .= "      order by id desc \n";

		$sql .= "         limit ?, ?            \n";



//파라미터 생성

		$sql_param = array();



		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}

		$sql_param[count($sql_param)] = $limit_st;

		$sql_param[count($sql_param)] = $limit_ed;



		$query = $this->db->query($sql, $sql_param);



		return $query;

	}


   	function student_delete_adm($id) {

		$this->db->where("id", $id);

		$this->db->delete("tblstudent");

	}
	
	function student_insert($data) {

		$this->db->insert("tblstudent", $data);

		$insert_id = $this->db->insert_id();

		return $insert_id;	

	}
	
	function student_update($data) {

		$this->db->where("id", $data['id']);

		$this->db->update("tblstudent", $data);

	}
	
	
  function student_select_detail($id) {

		$sql = "select *                \r\n";

		$sql .= "  from tblstudent        \r\n";

		$sql .= " where id = ?    \r\n";

		//var_dump($sql); var_dump($customer_no); exit();

		$query = $this->db->query($sql, $id);

/*		var_dump($this->db );
		exit();*/
		return $query;
	}


	
}

?>