<?php

/* ------------------------------------------------
 * 파일명 : me_member.php
 * 개  요 : 회원 관련
  ------------------------------------------------ */
?>
<?php

class Mo_member extends Model {

	function __construct() {
		parent::Model();
		$this->load->model('mo_customer', 'mo_customer');
		$this->load->database('default');
	}


	function select_by_member_nos($member_nos=array()){
		$this->db->select('email, member_first_name, member_last_name');
		$this->db->from('iw_member');
		$this->db->where_in('member_no', $member_nos);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	function exist_member_id_check($member_id){
		$sql = "select *              \r\n";
		$sql .= "  from iw_member      \r\n";
		$sql .= " where member_id = ?;  \r\n";

		$query = $this->db->query($sql, array($member_id));

		return $query;
	}
	//ROOT 관리자 체크

	function adm_count_member($where){
		$this->db->select('COUNT(*) as num_row');
		$this->db->from('iw_member');
		$this->db->where($where);
		$query = $this->db->get();

		$result = $query->result();

		return $result[0]->num_row;
	}
	function adm_member_select($offset, $limit, $where){
		$query = $this->db->orderby("created_dt", "desc")->get_where('iw_member', $where, $limit, $offset);
		//$this->db->orderby("created_dt", "desc");
		//echo $this->db->last_query();
		return $query;
	}
	//관리자 체크
	function login($member_id, $member_pwd) {
		$member_pwd = func_base64_encode($member_pwd);
		$sql = "select *              \r\n";
		$sql .= "  from iw_member      \r\n";
		$sql .= " where member_id = ?  \r\n";
		$sql .= "   and member_pwd = ? \r\n";
		$sql .= "   and certi_yn = 'Y' \r\n";

		$query = $this->db->query($sql, array($member_id, $member_pwd));


		return $query;
	}

	//관리자 로그인 COUNT UPDATE
	function admin_login_count_update($member_id) {
		$sql = "update iw_member                 \r\n";
		$sql .= "   set login_cnt = login_cnt + 1 \r\n";
		$sql .= " where member_id = ?;            \r\n";

		$this->db->query($sql, array($member_id));

		return $this->db->affected_rows();
	}

	//관리자 로그인 DATE UPDATE
	function admin_login_date_update($member_id, $last_login_dt) {
		$sql = "update iw_member         \r\n";
		$sql .= "   set last_login_dt = ? \r\n";
		$sql .= " where member_id = ?;    \r\n";

		$this->db->query($sql, array($last_login_dt, $member_id));

		return $this->db->affected_rows();
	}

	//회원등급 조회
	function member_grade_select() {
		$sql = "select *               \r\n";
		$sql .= "  from iw_member_grade \r\n";
		$sql .= " order by grade_no asc \r\n";
		$query = $this->db->query($sql);

		return $query;
	}

        function update_password_true($newpassword,$confirmpassword,$member_id){



             if ($newpassword ==$confirmpassword) {
         $sqlpassword = mysql_query("UPDATE iw_member SET member_pwd = '{$newpassword}' WHERE member_id = '{$member_id}'");
          echo func_jsAlertReplace('Password has been modified.', '/?c=mobile_user&m=mobile_acount');
      } else {
             echo func_jsAlertReplace('Your Password not match.', '/?c=mobile_user&m=change_password');
             exit('</center></h1>Your Password not match.</center></h1>');

           }
   }


    function getDetailByEmail($email){
    	$sql_param = array();
		$sql = "select * \r\n";
		$sql .= "  from iw_member        \r\n";
		$sql .= " where email = ? \r\n";
		$sql .= "ORDER BY member_no DESC LIMIT 1";
		$sql_param[] = $email;
		$query = $this->db->query($sql, $sql_param);
		$result = $query->result();
		return $result[0];
    }

    function getEmailByMemberId($member_id){
		$sql_param = array();
		$sql = "select email \r\n";
		$sql .= "  from iw_member        \r\n";
		$sql .= " where (member_id = ? OR email=?) \r\n";
		$sql_param[] = $member_id;
		$sql_param[] = $member_id;
		$query = $this->db->query($sql, $sql_param);
		$result = $query->result();
		return $result[0]->email;

	}




    function member_grade_select_register() {

        $sql = "select *               \r\n";
        $sql .= "  from iw_member_grade where grade_no = 11  \r\n";
        $sql .= " order by grade_no asc \r\n";


        $query = $this->db->query($sql);

        return $query;
    }


	//회원등급 상세 조회
	function member_grade_select_detail($seq_no) {
		$sql = "select *               \r\n";
		$sql .= "  from iw_member_grade \r\n";
		$sql .= " where seq_no =?       \r\n";

		$query = $this->db->query($sql, array($seq_no));

		return $query;
	}

	//회원가입시 정의된 기본등급 조회
	function member_grade_default_yn() {
		$sql = "select grade_no           \r\n";
		$sql .= "  from iw_member_grade    \r\n";
		$sql .= " where default_yn ='Y'    \r\n";
		$sql .= " order by created_dt desc \r\n";
		$sql .= " limit 0, 1               \r\n";

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			$rows = $query->row();

			return $rows->grade_no;
		} else {
			return '9999';
		}
	}

	//회원 등급 등록
	function insert_member_grade($data) {
		$this->db->insert("iw_member_grade", $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//회원 등급 수정
	function update_member_grade($data, $seq_no) {
		$this->db->where("seq_no", $seq_no);
		$this->db->update("iw_member_grade", $data);
	}

	//회원 등급 삭제
	function delete_member_grade($seq_no) {
		$this->db->where("seq_no", $seq_no);
		$this->db->delete("iw_member_grade");
	}
	function isVerified($member_id){
		$sql_param = array();
		$sql = "select COUNT(*) as row_count \r\n";
		$sql .= "  from iw_member        \r\n";
		$sql .= " where (verification IS NULL OR verification='') AND grade_no <> 10 AND (member_id = ? OR email = ?) \r\n";
		$sql_param[] = $member_id;
		$sql_param[] = $member_id;
		$query = $this->db->query($sql, $sql_param);
		$result = $query->result();
		if($result[0]->row_count>0){
			return true;
		}

	}
	//사용자 체크
	function member_chk($member_id, $member_pwd) {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
		$sql .= "      ,a.created_dt as join_dt                                                \n";
		$sql .= "      ,b.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "         where (member_id = ? OR email = ?)                                    \n";
		$sql .= "           and member_pwd = ?                                                 \n";
		//$sql .= "           and certi_yn = 'Y'                                                 \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member_grade                                         \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.grade_no = b.grade_no;                                                  \n";

		$query = $this->db->query($sql, array($member_id, $member_id, $member_pwd));
               //echo $this->db->last_query();
               // exit();
		return $query;
	}



	function _member_activated($member_id){
		$data = array(
			'member_id'=>$member_id,
			'email'=>$member_id
			);
		$this->db->select('grade_no');
		$this->db->from('iw_member');
		$this->db->or_where($data);
		$query = $this->db->get();
		$result = $query->result();
		return $result[0];
	}

	//사용자 로그인 COUNT UPDATE
	function login_count_update($member_id) {
		$sql = "update iw_member                 \r\n";
		$sql .= "   set login_cnt = login_cnt + 1 \r\n";
		$sql .= " where member_id = ?;            \r\n";

		$this->db->query($sql, array($member_id));

		return $this->db->affected_rows();
	}

	//사용자 로그인 DATE UPDATE
	function login_date_update($member_id, $last_login_dt) {
		$sql = "update iw_member         \r\n";
		$sql .= "   set last_login_dt = ? \r\n";
		$sql .= " where member_id = ?;    \r\n";

		$this->db->query($sql, array($last_login_dt, $member_id));

		return $this->db->affected_rows();
	}

	//가입여부 체크(이름 + 주민번호)
	function member_join_check($member_name, $jumin_no) {
		$sql = "select *                \r\n";
		$sql .= "  from iw_member        \r\n";
		$sql .= " where jumin_no = ?  \r\n";
		//$sql .= " where member_name = ?  \r\n";
		//$sql .= "   and jumin_no = ?     \r\n";

		$query = $this->db->query($sql, array($jumin_no));

		return $query->num_rows();
	}

		//주민등록번호 가입여부 체크
	function member_jumin_check($jumin_no) {
		$sql = "select *                \r\n";
		$sql .= "  from iw_member        \r\n";
		$sql .= " where jumin_no = ?     \r\n";

		$query = $this->db->query($sql, array($jumin_no));

		return $query->num_rows();
	}

	//아이디 찿기
	function member_account_id($member_name, $jumin_no) {
		$sql = "select *                \r\n";
		$sql .= "  from iw_member        \r\n";
		$sql .= " where member_name = ?  \r\n";
		$sql .= "   and jumin_no = ?     \r\n";

		$query = $this->db->query($sql, array($member_name, $jumin_no));

		return $query;
	}

	//비밀번호 찿기
	//function member_account_pwd($member_id, $member_name, $jumin_no) {
	function member_account_pwd($member_id, $member_first_name, $member_last_name) {
		$sql = "select *                \r\n";
		$sql .= "  from iw_member        \r\n";
		$sql .= " where member_id = ?    \r\n";
		$sql .= "   and member_first_name = ?  \r\n";
		$sql .= "   and member_last_name = ?  \r\n";
		//$sql .= "   and jumin_no = ?     \r\n";

		//$query = $this->db->query($sql, array($member_id, $member_name, $jumin_no));
		$query = $this->db->query($sql, array($member_id, $member_first_name, $member_last_name));

		return $query;
	}

	//회원 조회 목록수
	function total_member_select($where) {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
		$sql .= "      ,b.*,c.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "         where member_no = member_no                                          \n";
		$sql .= "         	and grade_no in (1,2,3)                                          \n";
		//검색 조건 생성
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						$sql .='  and  ' . $key . ' ? ';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key;
					}
				}
			}
		}
		$sql .= "      order by created_dt desc                                                \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member_grade                                         \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.grade_no = b.grade_no                                                  \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        SELECT cc,country_name FROM iw_country_list                                          \n";
		$sql .= "       ) as c                                                                 \n";
		$sql .= " on a.member_country= c.cc;                                                  \n";

		//파라미터 생성
		$sql_param = array();
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				if ($value == '')
					continue;
				$sql_param[count($sql_param)] = $value;
			}
		}

		$query = $this->db->query($sql, $sql_param);

		return $query->num_rows;
	}

        function update_password_exc($forgotemail,$data){
            $this->db->where('email',$forgotemail);
            $this->db->update('iw_member',$data);
        }

        function random_reset_password($email=''){
        		$md5_email = md5($email);
               $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                $pass = array();
                $alphaLength = strlen($alphabet) - 1;
                for ($i = 0; $i <10; $i++) {
                    $n = rand(0, $alphaLength);
                    $pass[] = $alphabet[$n];
                }
                return $md5_email.implode($pass);

        }


        function changePassword_exc($resetpass){
            $data['reset_pass']='';
            $this->db->where("reset_pass", $resetpass);
            $result = $this->db->update("iw_member", $data);
            //return $result;
          //  var_dump($result);
        }

        function existVerifyPassword($resetpass){
              $sql = "SELECT reset_pass FROM iw_member WHERE reset_pass = ? LIMIT 1 ";
                $sql_param  = array($resetpass);
                $query = $this->db->query($sql, $sql_param);
                $result = $query->result();
                if($query->num_rows() > 0){
                   return TRUE;
                  // var_dump($result);
                }
        }

        function checkUserID($forgotemail){
           $q = $this->db->query("select member_id from iw_member where email='$forgotemail' order by member_no desc limit 1");
                if($q->num_rows() > 0)
                 return $q->result();
             return false;
        }

        function  verifyPassword_update($newpass,$conformpass,$recode){
            if($newpass==$conformpass){
              $sqlpassword ="UPDATE iw_member SET member_pwd = '{$newpass}',reset_pass=NULL WHERE reset_pass = '{$recode}'";
              $this->db->query($sqlpassword);
            echo func_jsAlertReplace('Password has been modified.', './');

           } else {
             echo func_jsAlertReplace('Your Password not match.', './');
             exit('</center></h1>Your Password not match.</center></h1>');

           }

        }

        function  reset_password_update($data=array(),$where=array()){
		        $data['reset_pass'] ='';
		        $this->db->where('reset_pass',$where);
		        $this->db->update("iw_member", $data);
        }




//        function getOldPassword_Mobile($member_id,$oldpassword){
//
//
//            $this->db->where('member_id',$member_id);
//            $this->db->where('member_pwd',$oldpassword);
//            $query=  $this->db->get("iw_member");
//            echo $this->db->last_query();
//            if($query->num_rows()>0){
//                return true;
//            }
//            else {
//                return NULL;
//            }
//
//    }




        //회원 조회
	function member_select($limit_st, $limit_ed, $where) {
		$this->load->library('session');
		$session_grade_no = $this->session->CI->grade_no;
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
		$sql .= "      ,a.created_dt as join_dt                                                \n";
		$sql .= "      ,b.*,c.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "         where member_no = member_no                                          \n";
		$sql .= "         	and grade_no in (1,2,3)                                          \n";
		$sql .= "         	and draft = 'N'                                                   \n";
		//검색 조건 생성
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						$sql .='  and  ' . $key . ' ? ';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key;
					}
				}
			}
		}

		$sql .= "      order by created_dt desc                                                \n";
		$sql .= "         limit ?, ?                                                           \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member_grade                                         \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.grade_no = b.grade_no                                                  \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        SELECT cc,country_name FROM iw_country_list                                          \n";
		$sql .= "       ) as c                                                                 \n";
		$sql .= " on a.member_country= c.cc;                                                  \n";

		//파라미터 생성
		$sql_param = array();
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				if ($value == '')
					continue;
				$sql_param[count($sql_param)] = $value;
			}
		}
		$sql_param[count($sql_param)] = $limit_st;
		$sql_param[count($sql_param)] = $limit_ed;

		$query = $this->db->query($sql, $sql_param);

		return $query;
	}

	//회원 전체 조회(관리자)
	function member_select_all() {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
		$sql .= "      ,a.created_dt as join_dt                                                \n";
		$sql .= "      ,b.*,c.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member_grade                                         \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.grade_no = b.grade_no                                                  \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        SELECT cc,country_name FROM iw_country_list                                          \n";
		$sql .= "       ) as c                                                                 \n";
		$sql .= " on a.member_country= c.cc;                                                  \n";

		$query = $this->db->query($sql);

		return $query;
	}

	//회원 상세 조회(관리자)
	function member_select_detail_adm($member_no) {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
		$sql .= "      ,a.created_dt as join_dt                                                \n";
		$sql .= "      ,b.*,c.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "         where member_no = ?                                                  \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member_grade                                         \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.grade_no = b.grade_no                                                  \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        SELECT cc,country_name FROM iw_country_list                                          \n";
		$sql .= "       ) as c                                                                 \n";
		$sql .= " on a.member_country= c.cc;                                                  \n";

		//$query = $this->db->query($sql, array($member_no));
		$query = $this->db->query($sql, $member_no);
		return $query;
	}

	//회원 상세 조회(사용자)
	function member_select_detail($member_id) {
		$sql = "select *                \r\n";
		$sql .= "  from iw_member        \r\n";
		$sql .= " where member_id = ?    \r\n";

		$query = $this->db->query($sql, array($member_id));

		return $query;
	}
	function email_select_detail($email) {
		$sql = "select *                \r\n";
		$sql .= "  from iw_member        \r\n";
		$sql .= " where email = ?    \r\n";

		$query = $this->db->query($sql, array($email));

		return $query;
	}

	function member_profile_select($id) {
		// $sql  = " select *, cf.secure_url as image, cl.country_name as country   \r\n";
		// $sql .= " from iw_member as me      \r\n";
		// $sql .= " join iw_cloud_files as cf on me.profile_image_id = cf.id   \r\n";
		// $sql .= " join iw_country_list as cl on me.member_country = cl.cc   \r\n";
		// $sql .= " where me.member_no = '$owner'    \r\n";
		// echo $sql;

		$sql="SELECT member.member_no,country_list.country_name as country , country_list.cc, member.contact_person_name, member.company_name, member.address as address,cloud.base_url,cloud.public_id as image, (SELECT COUNT(car_owner) 
			  FROM iw_bbs WHERE iw_bbs.car_owner = (select car_owner from iw_bbs where idx = '$id') and iw_bbs.menu_code='product' and iw_bbs.publish='Y' and iw_bbs.category='offer' and iw_bbs.created_dt >='2013-11-18') as seller_product_count 
			  FROM iw_member as member LEFT JOIN iw_country_list as country_list ON member.member_country = country_list.cc LEFT JOIN iw_cloud_files as cloud ON member.profile_image_id = cloud.id
			  LEFT JOIN iw_member as sales ON member.sales_no = sales.member_no
			  WHERE  member.member_no = (select car_owner from iw_bbs where idx = '$id') ";
		$query = $this->db->query($sql);
		$result=$query->result();
		return $result;
	}

	function slelect_team_sale($mem_country){
		 $this->db->select('member_first_name,phone_no1,phone_no2,mobile_no1,mobile_no2,
		 	email,skype_id,facebook_id,whatsapp_id,viber_id,iw_cloud_files.secure_url,member_country,grade_no');
         $this->db->from('iw_member');
         $this->db->join('iw_cloud_files', 'iw_cloud_files.file_owner = iw_member.member_no');
         $this->db->where("(iw_member.grade_no ='3' or iw_member.grade_no='2') and member_country='$mem_country' ");
         $this->db->order_by("grade_no","ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
	}

	function member_profile_detail($id) {
		// $sql  = " select *, cf.secure_url as image, cl.country_name as country   \r\n";
		// $sql .= " from iw_member as me      \r\n";
		// $sql .= " join iw_cloud_files as cf on me.profile_image_id = cf.id   \r\n";
		// $sql .= " join iw_country_list as cl on me.member_country = cl.cc   \r\n";
		// $sql .= " where me.member_no = '$owner'    \r\n";
		// echo $sql;

		$sql="SELECT member.member_no,country_list.country_name as country , member.contact_person_name, member.company_name, member.address as address,cloud.base_url,cloud.public_id as image FROM iw_member as member LEFT JOIN iw_country_list as country_list ON member.member_country = country_list.cc LEFT JOIN iw_cloud_files as cloud ON member.profile_image_id = cloud.id
			  LEFT JOIN iw_member as sales ON member.sales_no = sales.member_no
			  WHERE  member.member_no = '$id' ";

		$query = $this->db->query($sql);
		$result=$query->result();
		return $result;
	}

	//회원 등록(사용자)
	function member_insert($data) {
		$this->db->insert("iw_member", $data);
		$insert_id = $this->db->insert_id();

		return $insert_id;
	}

	//회원 수정(사용자)
	function member_update($data) {
		$this->db->where("member_id", $data['member_id']);
		$this->db->update("iw_member", $data);
	}

	function member_update_by_vcode($data) {
		$this->db->where("verification", $data['verification']);
		$this->db->update("iw_member", $data);
	}


	//회원 삭제(사용자)
	function member_delete($member_no) {
		$this->db->where("member_no", $member_no);
		$this->db->delete("iw_member");
	}

	//비밀번호 비교 체크
	function member_password_check($member_no, $password) {
		$sql = "select *                \r\n";
		$sql .= "  from iw_member        \r\n";
		$sql .= " where member_no = ?    \r\n";
		$sql .= "   and member_pwd  = ?    \r\n";

		$query = $this->db->query($sql, array($member_no, $password));

		return $query->num_rows();
	}

	//회원 수정(관리자)
	function member_update_adm($data) {
		$data['draft'] = 'N';
		$this->db->where("member_no", $data['member_no']);
		$this->db->update("iw_member", $data);
	}


	//회원 삭제(관리자)
	function member_delete_adm($member_no) {
		$this->db->where("member_no", $member_no);
		$this->db->delete("iw_member");
		
	}

	//회원 인증(관리자)
	function member_certi_update_adm($member_no) {
		$this->db->where("member_no", $member_no);
		$this->db->set("certi_yn", 'Y');
		$this->db->update("iw_member");
	}

	/* --------------------------------------------------------------------------------------------------
	 * 게시판 연동
	  ------------------------------------------------------------------------------------------------- */

	//게시판 회원 조회 목록수
	function total_bbs_select($member_id, $where) {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
		$sql .= "      ,b.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_bbs                                                  \n";
		$sql .= "         where member_id = ?                                                  \n";
		$sql .= "           and notice_yn = 'N'                                                \n";
		//검색 조건 생성
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						$sql .='  and  ' . $key . ' ? ';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key;
					}
				}
			}
		}
		$sql .= "      order by grp_idx desc, order_no asc                                     \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_bbs_attach group by idx                              \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.idx = b.idx;                                                            \n";

		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $member_id; //회원아이디
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				if ($value == '')
					continue;
				$sql_param[count($sql_param)] = $value;
			}
		}

		$query = $this->db->query($sql, $sql_param);

		return $query->num_rows();
	}

	//게시판 회원 조회
	function bbs_select($member_id, $notice_yn = 'N', $limit_st, $limit_ed, $where) {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,a.menu_code as bbs_menu_code                                           \n";
		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
		$sql .= "      ,b.*                                                                    \n";
		$sql .= "      ,a.idx                                                                  \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_bbs                                                  \n";
		$sql .= "         where member_id = ?                                                  \n";
		$sql .= "           and notice_yn = ?                                                  \n";
		//검색 조건 생성
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						$sql .='  and  ' . $key . ' ? ';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key;
					}
				}
			}
		}
		//일반, 공지 구분
		if ($notice_yn != 'Y') {
			$sql .= "      order by grp_idx desc, order_no asc                                 \n";
		} else {
			$sql .= "      order by created_dt desc                                            \n";
		}
		$sql .= "         limit ?, ?                                                           \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_bbs_attach group by idx                              \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.idx = b.idx;                                                            \n";



		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $member_id; //회원아이디
		$sql_param[count($sql_param)] = $notice_yn; //일반 공지 구분
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				if ($value == '')
					continue;
				$sql_param[count($sql_param)] = $value;
			}
		}
		$sql_param[count($sql_param)] = $limit_st;
		$sql_param[count($sql_param)] = $limit_ed;

		$query = $this->db->query($sql, $sql_param);

		return $query;
	}
	// 첨부파일 등록
	function insert_attach($data) {
		$this->db->insert("iw_member_attach", $data);
		return $this->db->insert_id();
	}
	// 첨부파일 조회
	function select_attach($member_no, $category = '') {
		if($category != '') {
			$sql = "select * from iw_member_attach   \n";
			$sql .= "         where member_no = ?        \n";
			$sql .= "           and category = ?  \n";
			//파라미터 생성
			$sql_param = array();
			$sql_param[0] = $member_no;
			$sql_param[count($sql_param)] = $category;
		} else {
			$sql = "select * from iw_member_attach   \n";
			$sql .= "         where member_no = ?        \n";
			//파라미터 생성
			$sql_param = array();
			$sql_param[0] = $member_no;
		}
		$query = $this->db->query($sql, $sql_param);

		return $query;
	}
	// 첨부파일 상세 조회
	function select_attach_detail($member_no, $att_idx) {
		$sql = "select * from iw_member_attach   \n";
		$sql .= "         where member_no = ?        \n";
		$sql .= "           and att_idx = ?    \n";

		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $member_no;	   //idx
		$sql_param[count($sql_param)] = $att_idx;   //첨부파일 idx

		$query = $this->db->query($sql, $sql_param);

		return $query;
	}
	// 첨부파일 삭제
	function delete_attach($member_no, $att_idx) {
		$this->db->where("member_no", $member_no);
		$this->db->where("att_idx", $att_idx);
		$this->db->delete("iw_member_attach");
	}

	function registerUser($data) {
		$data['created_dt'] = date('Y-m-d h:i:s');
        $this->db->insert("iw_member", $data);
        $insert_id = $this->db->insert_id();
        //echo $this->db->last_query(); exit();
        return $insert_id;
    }
	function validateRegister($data){
		$result = array();
		//$where_country['cc'] = $data['member_country'];
		$member_detail = $this->member_select_detail($data['member_id']);
		$member_detail_rows = $member_detail->result();
		//$country_list = $this->selectCountry($where_country);

	//	$member_detail_rows = $member_detail->result();

		/*****Validate member id format*****/
		if(!preg_match("/^[a-z\S\d_]{4,20}$/i", $data['member_id'])){
			$result['error_member_id'] = "Member ID must be alphanumeric and dot characters, from 4 to 20 characters long";
		}
		/*****Check if member id exist*****/
		if(count($member_detail_rows)>0){
			$result['error_member_id'] = "Member ID already existed";
		}
		/*****Check password length*****/
		if(strlen($data['member_pwd'])<6 || strlen($data['member_pwd'])>20){
			$result['error_member_pwd'] = "Password must be 6 to 20 characters long";
		}
		/*****Check confirm password length*****/
		//if(strlen($data['member_password_check'])<5 || strlen($data['member_password_check'])>20){
		//	$result['error_password_check'] = "Password must be 5 to 20 characters long";
		//}
		/*****Check email format*****/
		if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
			$result['error_email'] = "Invalid Email";
		}
		/*****Check mobile phone format*****/
		if (!filter_var($data['phone_no1'])) {
			$result['error_phone_no1'] = "Invalid phone_no1";
		}
		/*****Check member first name*****/
		//if(!preg_match("/^[\p{L} ]+$/u", $data['member_first_name'])){
//			$result['error_member_first_name'] = "First Name must be alphanumeric";
//		}
//		/*****Check member last name*****/
//		if(!preg_match("/^[\p{L} ]+$/u", $data['member_last_name'])){
//			$result['error_member_last_name'] = "Last Name must be alphanumeric";
//		}
		/*****Check member address*****/
		//if(!preg_match("/^[\p{L} ]+$/u", $data['member_address'])){
		//	$result['error_member_address'] = "address must be alphanumeric";
		//}
		/*****Check member company name*****/
		//if(!preg_match("/^[\p{L} ]+$/u", $data['member_company_name'])){
		//	$result['error_member_company_name'] = "company_name must be alphanumeric";
		//}
		/*****Check member contact_person*****/
		//if(!preg_match("/^[\p{L} ]+$/u", $data['member_contact_person'])){
		//	$result['error_member_contact_person'] = "contact_person must be alphanumeric";
		//}
		/*****Check if country valid*****/
		// if(count($country_list)<=0){
		// 	$result['error_member_country'] = "Invalid country";
		// }
		/*****Check if mobile number valid*****/
		// if(!empty($data['mobile_no1'])){
			// $array = explode(' ', $data['mobile_no1']);
			// if(!preg_match("/^[0-9]+$/", $data['mobile_no1'])){
			//    $result['error_mobile_no'] = "Mobile Number must be only numbers";
			// }
		// }
		/*****Give result*****/
		if(count($result)>0){
			$result['success'] = 0;
		}else{
			$result['success'] = 1;
		}

		return $result;
	}
	function validate_email_Register($data){
		$result = array();
		$email = $this->email_select_detail($data['email']);
		$email_detail_rows = $email->result();
		//echo $this->db->last_query();exit();
		if(count($email_detail_rows)>0){
			$result['success'] = 0;
		}else{
			$result['success'] = 1;
		}

		return $result;
	}

	function check_verify_by_email($email){
		$where['email'] = $email;
		$where['grade_no'] = 11;

		$this->db->select('COUNT(*) AS total_user');
		$this->db->from('iw_member');
		$this->db->where($where);
		$query = $this->db->get();
		$result = $query->result();
		$total_user = $result[0]->total_user;
		if($total_user>0){
			return true;
		}
		
	}
	public function selectCountry($where){
		$this->db->select('*');
		$this->db->from('iw_country_list');
		$this->db->where($where);
		$query = $this->db->get();

		$result = $query->result();

		return $result;
	}

	function createVerificationCode($member_no){
        $string = md5($member_no."+".strtotime("now"));
        $data['verification']=$string;
        $this->db->where("member_no", $member_no);
        $result = $this->db->update("iw_member", $data);
        return $string;
    }
    function getUserEmail($member_no){
        $sql = "SELECT email FROM iw_member WHERE member_no = ? LIMIT 1 ";
        $sql_param  = array($member_no);
        $query = $this->db->query($sql, $sql_param);
        $result = $query->result();
        if($query->num_rows() <= 0){
            return '';
        }
        return $result['0']->email;
    }
    // function sendVerificationToEmail($email, $vCode, $user_id, $name){
    //     $html = '<html><head></head><body>';
    //     $html.= 'Hello '.$name.'!<br/><br/>
    //     		Your have recently signed up with your email at iblauda.com.<br/>
    //     		Your Member ID: '.$user_id.'<br/><br/>You need just one more step to complete your registration. Visit the link below to activate your account now.<br/>
    //     		<a href="http://iblauda.com/?c=user&m=verifyAccount&vcode='.$vCode.'"/>http://iblauda.com/?c=user&m=verifyAccount&vcode='.$vCode.'</a><br/><br/>
    //  			Once you have visited it you will be able to login at our site.<br/><br/>
    //     		Thank you for joining <a href="http://iblauda.com/">IBLAUDA.com</a>.';

    //     $html.= '</body></html>';
    //     $config = Array(
    //         'protocol' => 'mail',
    //         'smtp_host' => 'ssl://kangaroo.arvixe.com',
    //         'smtp_port' => '465',
    //         'smtp_user' => 'noreply@iblauda.com',
    //         'smtp_pass' => '#Kx4Qh_]})eh',
    //         'charset' => 'utf-8',
    //         'mailtype' => 'html',
    //         'newline' => "\r\n",
    //         'crlf' => "\n",
    //         'wordwrap' => TRUE
    //     );
    //     $this->load->library('email');
    //     $this->email->initialize($config);

    //     $this->email->from('noreply@iblauda.com', 'IBLAUDA');
    //     $this->email->reply_to("noreply@iblauda.com", "noreply@iblauda.com");
    //     $this->email->to($email);
    //     $this->email->subject('IBLAUDA.com: Account verification');
    //     $this->email->message($html);
    //     $this->email->send();
    //         //echo $this->ci->email->print_debugger();

    // }
    //2
    function sendVerificationToEmail($email, $vCode, $user_id, $name){

        require 'libraries/PHPMailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'ssl://kangaroo.arvixe.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@motorbb.com';                 // SMTP username
        $mail->Password = '~PF!@BUtg)b*';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        $mail->From = 'noreply@motorbb.com';
        $mail->FromName = 'IBLAUDA';
        $mail->addAddress($email, 'Motorbb');     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('noreply@motorbb.com', 'noreply@motorbb.com');
        $mail->isHTML(true);                                  // Set email format to HTML
        $html = '<html><head></head><body>';
        $html.= 'Hello '.$name.'!<br/><br/>
        		Your have recently signed up with your email at iblauda.com.<br/>
        		Your Member ID: '.$user_id.'<br/><br/>You need just one more step to complete your registration. Visit the link below to activate your account now.<br/>
        		<a href="http://iblauda.com/?c=user&m=verifyAccount&vcode='.$vCode.'"/>http://iblauda.com/?c=user&m=verifyAccount&vcode='.$vCode.'</a><br/><br/>
     			Once you have visited it you will be able to login at our site.<br/><br/>
        		Thank you for joining <a href="http://iblauda.com/">IBLAUDA.com</a>.';

        $mail->Subject = "IBLAUDA.com: Account verification";
        $mail->Body    = $html;
        $html.= '</body></html>';
        $mail->send();
    }
    // end send to email
    function verifyAccount($vcode){
        $data['verification']='';
        $data['grade_no'] = 11;
        $this->db->where("verification", $vcode);
        $result = $this->db->update("iw_member", $data);
        return $result;
    }
    function insertVerification($member_no, $vcode){
        $data['verification']=$vcode;
        $this->db->where("member_no", $member_no);
        $result = $this->db->update("iw_member", $data);
        return $result;
    }
    function existVerificationCode($vcode){
        $sql = "SELECT verification FROM iw_member WHERE verification = ? LIMIT 1 ";
        $sql_param  = array($vcode);
        $query = $this->db->query($sql, $sql_param);
        $result = $query->result();
        if($query->num_rows() > 0){
            return TRUE;
        }
    }
    function select_VerificationCode($vcode){
    	$where['verification']=$vcode;
    	$this->db->select('*');
    	$this->db->from('iw_member');
    	$this->db->where($where);
    	$query = $this->db->get();
    	$result= $query->result();
        return $result;
     }
    public function func_base64_encode($txt) {
		$key = "abcdefghijklmnopqrstuvwxyz";
		$key_len = strlen($key);
		$rot_ptr_set = 9;

		$rot_ptr = $rot_ptr_set;

		$tmp = "";
		$txt = strrev($txt);
		$txt_len = strlen($txt);

		for ($i = 0; $i < $txt_len; $i++) {
			if ($rot_ptr >= $key_len)
				$rot_ptr = 0;
			$tmp .= $txt[$i] ^ $key[$rot_ptr];
			$v = ord($tmp[$i]);
			$tmp[$i] = chr(((($v << 3) & 0xf8) | (($v >> 5) & 0x07)));
			$rot_ptr++;
		}
		$tmp = base64_encode($tmp);
		$tmp = strrev($tmp);
		return $tmp;
	}

	/***** SELECT CUSTOMERS THAT HAD ORDERED CAR *****/
	function getInventoryCustomer($stock_idx, $exclude_member=NULL){
		$sql_param =array();
 		$sql = "SELECT inventory.*, member.member_first_name, member.member_last_name, member.email
 			FROM iw_customer_inventory as inventory
 			INNER JOIN iw_member as member ON inventory.customer_no = member.member_no
 			WHERE stock_idx = ?";
 		$sql_param[] = $stock_idx;
 		if($exclude_member!=NULL){
 			$sql.=" AND inventory.customer_no != ?";
 			$sql_param[] = $exclude_member;
 		}

        $query = $this->db->query($sql, $sql_param);
        $result = $query->result();
        return $result;
	}

	function saveRememberToken($member_no, $remember_token){
		$this->db->set('remember_token', $remember_token);
		$this->db->where('member_no', $member_no);
		$this->db->update('iw_member');
	}
	function generateRememberToken($member_id){
		$strtime = strtotime("now");
		$remember_token = crypt($member_id.$strtime, '$2y$07$h7jrovmeotydh46trhfygy$');
        return $remember_token;
	}
	function getDetailBySession($memebr_no, $remember_token){
		$sql_param = array();
		$sql = "select *, iw_member_grade.grade_name";
		$sql .= "  from iw_member        ";
		$sql .= "LEFT JOIN iw_member_grade ON iw_member.grade_no = iw_member_grade.grade_no";
		$sql .= " where member_no = ? AND remember_token = ? ";
		$sql .= "ORDER BY member_no DESC LIMIT 1";

		$sql_param[] = $memebr_no;
		$sql_param[] = $remember_token;
		$query = $this->db->query($sql, $sql_param);
		$result = $query->result();

		return $result[0];
	}
	function social_login_date_update($last_login_dt,$member_id) {
		$sql = "update iw_member         \r\n";
		$sql .= "   set last_login_dt = ? \r\n";
		$sql .= " where member_id = ?;    \r\n";

		$this->db->query($sql, array($last_login_dt, $member_id));

		return $this->db->affected_rows();
	}

	function social_login_count_update($member_id) {
		$sql = "update iw_member                 \r\n";
		$sql .= "   set login_cnt = login_cnt + 1 \r\n";
		$sql .= " where member_id = ?;            \r\n";

		$this->db->query($sql, array($member_id));

		return $this->db->affected_rows();
	}
	function social_member_chk($member_id) {
		$sql = '';
		$sql .= "select a.*                                                                    \n";
		$sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
		$sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
		$sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
		$sql .= "      ,a.created_dt as join_dt                                                \n";
		$sql .= "      ,b.*                                                                    \n";
		$sql .= "  from (                                                                      \n";
		$sql .= "        select * from iw_member                                               \n";
		$sql .= "         where member_id = ?                                                  	   \n";
		$sql .= "       ) as a                                                                 \n";
		$sql .= " left outer join                                                              \n";
		$sql .= "       (                                                                      \n";
		$sql .= "        select * from iw_member_grade                                         \n";
		$sql .= "       ) as b                                                                 \n";
		$sql .= " on a.grade_no = b.grade_no;                                                  \n";

		$query = $this->db->query($sql, array($member_id));

		return $query;
	}

	function select_customer($where=array(), $limit_s=NULL, $limit_e=NULL){
		$where['grade_no'] = 11;

		$this->db->select('*');
		$this->db->from('iw_member');
		if(isset($where['customer_name'])){
			$this->db->where("(member_first_name LIKE '%".$where['customer_name']."%' OR member_last_name LIKE '%".$where['customer_name']."%')");

			unset($where['customer_name']);
		}
		$this->db->where($where);

		$this->db->order_by("member_first_name ASC, member_last_name ASC");
		$query = $this->db->get();
		//echo $this->db->last_query();
		$result = $query->result();

		return $result;
	}
	function select_all_admin_grades() {
		$sql = "select *               \r\n";
		$sql .= "  from iw_member_grade \r\n";
		$sql .= " where member_group = 'admin' \r\n";
		$sql .= " order by grade_no asc \r\n";
		$query = $this->db->query($sql);

		return $query;
	}
	function select_all_user_grades() {
		$sql = "select *               \r\n";
		$sql .= "  from iw_member_grade \r\n";
		$sql .= " where member_group = 'user' \r\n";
		$sql .= " order by grade_no asc \r\n";
		$query = $this->db->query($sql);

		return $query;
	}
	function iscar_owner($idxs) {
		$idx = explode(" ", $idxs);
		$member_no = $_SESSION['ADMIN']['member_no'];
        $this->db->select('COUNT(*) as num_row');
        $this->db->from('iw_member');
        $this->db->where_in('iw_bbs.idx',$idx);
        $this->db->where('iw_bbs.car_owner',$member_no);
        $this->db->join('iw_bbs', 'iw_bbs.car_owner = iw_member.member_no');
        $query = $this->db->get();
        return $query->result();
        // echo $this->db->last_query(); exit();
      //   if(($result[0]->num_row) > 0 && $result[0]->num_row>=count($idxs))
      //   		return true;
	     //    	if(isset($_GET['m']) || $_GET['m']=='adm_reserve_popup_update' || $_GET['m']=='adm_bbs_ship_popup_update')
						// echo "<script>window.close();</script>";
	     //    		echo func_jsAlertReplace("The car does not exist!", "/?c=admin");
	     //    		redirect('/?c=admin', 'refresh');
    }

    
    function adm_select_customer_service($where=array(), $offset=0, $num_rows=NULL){
    	//$where['chat_support'] = 'Y';
    	//$where['grade_no'] = '3';
    	$this->db->select("*, CONCAT_WS('', member_first_name, ' ', member_last_name) as member_name", false);
        $this->db->from('iw_member');
        $this->db->where($where);
        $this->db->where('(grade_no=1 OR grade_no=2 OR grade_no=3)');
        if($num_rows!=NULL){
       		$this->db->limit($num_rows, $offset);
        }
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
  //   function insert_member_image($data) {
  //   	$this->db->insert("iw_member_images", $data);
  //       $insert_id = $this->db->insert_id();
  //       return $insert_id;
  //   }
  //   function delete_member_images($image_id_fk,$member_no_fk) {
  //   	$this->db->where('image_id_fk', $image_id_fk);
  //   	$this->db->where('member_no_fk', $member_no_fk);
		// $this->db->delete('iw_member_images');
  //   }
  //   function select_public_id($public_id) {
  //   	$this->db->select('id');
  //   	$this->db->from('iw_cloud_files');
  //   	$this->db->where('public_id', $public_id);
  //   	$query = $this->db->get();
  //   	$result = $query->result();
  //       return $result[0]->id;
  //   }
    // function member_id_last(){
    // 	$this->db->select('member_no');
    // 	$this->db->from('iw_member');
    // 	$this->db->order_by("member_no", "desc");
    // 	$this->db->limit(1);
    // 	$query = $this->db->get();
    // 	$result = $query->result();
    // 	return $result[0]->member_no;
    // }
    function insert_customer ($data = array()) {
		//var_dump($data);exit();
        $data['created_dt'] = func_get_date_time();
        $data['grade_no'] = '11';
        $data['certi_yn'] = 'Y';
        $this->db->insert("iw_member", $data);
        $insert_id = $this->db->insert_id();
        //echo $this->db->last_query(); exit();
        return $insert_id;
    }
    function update_customer ($data = array(),$where=array()) {
		
        $data['created_dt'] = func_get_date_time();
        $data['grade_no'] = '11';
        $data['certi_yn'] = 'Y';
        $this->db->where($where);
        $this->db->update("iw_member", $data);
        $this->mo_customer->insert_email_to_recipient($data['member_country'], $data['email'], $data['member_first_name']." ".$data['member_last_name']);
       // echo $this->db->last_query(); exit();
    }
    function insert_member($data = array()) {
		//var_dump($data);exit();
        $data['created_dt'] = func_get_date_time();
        //$data['grade_no'] = '3';
        $data['certi_yn'] = 'Y';
        $this->db->insert("iw_member", $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function select_grade_no($grade_no) {
		$this->db->select('member_no,member_first_name');
    	$this->db->from('iw_member');
    	$this->db->where("grade_no <=", $grade_no);
    	$query = $this->db->get();
        return $query;
	}
	function select_member_login(){
		$this->db->select('*');
		$this->db->from('iw_member');
		$this->db->where('member_no', $_SESSION['ADMIN']['member_no']);
		$result = $this->db->get();
		return $result->row();
	}
	function select_customer_no($member_no){
		$this->db->select('member_first_name,member_last_name,email,phone_no1,phone_no2,address');
		$this->db->from('iw_member');
		$this->db->where('member_no', $member_no);
		$result = $this->db->get();
		return $result->row();
	}

	function select_business_type_seller(){
		$this->db->select('member_no,member_id,member_first_name,member_last_name,email,phone_no1,phone_no2,address');
		$this->db->from('iw_member');
		$this->db->where('business_type', 'seller');
		$query = $this->db->get();
		return $query->result();
	}

}
?>
