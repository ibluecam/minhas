<?php


/* ------------------------------------------------

 * 파일명 : mo_bbs.php

 * 개  요 : 게시판 관리

  ------------------------------------------------ */


class Mo_invoice extends Model {



	function __construct() {

		parent::Model();



		$this->load->database('default');

	}
	function delete_invoice_by_no($invoice_nos){
		$sql = '';
		$sql_param = array();
		$bind_marks = array();
		
		foreach($invoice_nos as $invoice_no){
			$sql_param[] = $invoice_no;
			$bind_marks[]= "?";
		} 
		$where_in  = implode(', ', $bind_marks);
		$sql .= "UPDATE iw_bbs SET 
					invoice_no=NULL
				WHERE invoice_no IN ({$where_in}) ";

		$query = $this->db->query($sql, $sql_param);
		return $query;
	}
	function save_dhl_no($dhl_no, $invoice_nos){
		$sql = '';
		$sql_param = array();
		$bind_marks = array();
		$sql_param[] = $dhl_no;
		foreach($invoice_nos as $invoice_no){
			$sql_param[] = $invoice_no;
			$bind_marks[]= "?";
		} 
		$where_in  = implode(', ', $bind_marks);
		$sql .= "UPDATE iw_bbs SET dhl_no=? WHERE invoice_no IN ({$where_in}) ";

		$query = $this->db->query($sql, $sql_param);
		return $query;
	}
	function save_invoice_no($invoice_no, $bl_nos){
		$sql = '';
		$sql_param = array();
		$bind_marks = array();
		$sql_param[] = $invoice_no;
		foreach($bl_nos as $bl_no){
			$sql_param[] = $bl_no;
			$bind_marks[]= "?";
		} 
		$where_in  = implode(', ', $bind_marks);
		$sql .= "UPDATE iw_bbs SET invoice_no=? WHERE bl_no IN ({$where_in}) ";

		$query = $this->db->query($sql, $sql_param);
		return $query;
	}

	//게시판 조회 목록수

	function total_select() {

		$sql = '';

		$sql .= "        select * from iw_bbs                                                  \n";

		$sql .= "         where invoice_no != ''                                                  \n";
		
		$sql .= "           group by invoice_no														\n";
	
		$query = $this->db->query($sql);
		
/*		print_r($sql);
		exit;*/
		/*var_dump($query); exit();*/
		return $query->num_rows();
	}

	//게시판 조회
	function select($invoice_no, $limit_st, $limit_ed, $where) {
	
		$sql_param = array();


		$sql = '';

		$sql .= "		select a.invoice_no, count(a.invoice_no) as total_car              \n";
		
		$sql .= "		from (iw_bbs a LEFT OUTER JOIN iw_customer b on a.customer_no = b.customer_no)	    	\n";
		
		if($invoice_no !=''){

		//$sql_param[0] = $invoice_no;
		
		$sql .= "         	where a.invoice_no = ?                                                  \n";
		
		}else{
		
		$sql .= "         	where a.invoice_no != ''                                                  \n";
		
		}


		//검색 조건 생성
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						//$sql .='  and  ' . $key . ' ? '.' AND `car_visible`=\'True\' ';
						$sql .='  and  ' . $key . ' '.'"'. $value .'"'. ' AND `car_visible`=\'True\' ';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key.' AND `car_visible`=\'True\' ';
					}
				}
			}
		}
		
		
		$sql .= "           group by a.invoice_no														\n";
		
		$sql .= "           order by total_car desc													\n";
		
		$sql .= "           limit ?, ?  											\n";

		
		//파라미터 생성
		
		$sql_param[count($sql_param)] = $limit_st;
		$sql_param[count($sql_param)] = $limit_ed;	

		$query = $this->db->query($sql, $sql_param);

		//print_r($sql); exit();

		return $query;

	}

	//게시판 첨부파일 조회
	function select_attach($idx) {
	
		//파라미터 생성
		$sql_param = array();
		
		$sql = "select * from iw_invoice_attach   \n";
		
		$sql .= "         where invoice_no = ?        \n";
		
		
		if( is_array($idx)){
			
			if(sizeof($idx) > 1){
			
			foreach($idx as $value){
			
				if(count($sql_param) == 0){
				
					$sql_param[0] = $value; //메뉴코드
				
				}else{
				
					$sql .= "           OR idx = ?  \n";
					
					$sql_param[count($sql_param)] = $value; //메뉴코드
					
				}
			
			}
			
			}else{
				
				$sql_param[0] = $idx[0];    /////////////////////////////////////////////
				
			}
		}else{
		
			$sql_param[0] = $idx;
		
		}

		$query = $this->db->query($sql, $sql_param);
		
		if( is_array($idx)){

			return $query->num_rows;
		}else{
			return $query;
		}
	}

	//게시판 첨부파일 상세 조회
	function select_attach_detail($invoice_no, $att_idx) {
		$sql = "select * from iw_invoice_attach \n";
		$sql .= "         where invoice_no = ?        \n";
		$sql .= "           and att_idx = ?    \n";

		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $invoice_no;	   //idx
		$sql_param[count($sql_param)] = $att_idx;   //첨부파일 idx

		$query = $this->db->query($sql, $sql_param);

		return $query;
	}

	//게시판 상세 조회
	function select_detail($idx) {
	
		$sql = '';
		$sql .= "select *                                                                   \n";
		
		$sql .= "from iw_invoice                                                                   \n";
		
		$sql .= "where idx = ?                                                                   \n";
		

		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $idx;	   //idx
		
		$query = $this->db->query($sql, $sql_param);

		return $query;
	}

	
	// select document by invoice_no
	
	function select_invoice($invoice_novalu){
	
	$sql = '';
	
	$sql = "SELECT idx FROM iw_invoice WHERE invoice_no='".$invoice_novalu."' \n";
	
		$query = $this->db->query($sql);

		return $query;
	}
	
	
	// select document by invoice_no detail
	
	function select_invoice_detail($idx){
	
		$sql = '';
		
		$sql .= "select *                                                                   \n";
		
		$sql .= "from iw_invoice_bl                                                                   \n";
		
		$sql .= "where idx = ?                                                                   \n";
		
		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $idx;	   //idx
		
		$query = $this->db->query($sql, $sql_param);

		return $query;
	}



	function select_invoice_car($invoice_no){
	
		$sql = '';
		
		$sql .= "select a.*,b.*                                                                   \n";
		
		$sql .= "from iw_bbs a                                                                   \n";

		$sql .= "left outer join iw_member b                                                   \n";

		$sql .= "on a.customer_no = b.member_no                                                                  \n";
		
		$sql .= "where a.invoice_no = ?                                                                   \n";
		
		//파라미터 생성
		$sql_param = array();
		$sql_param[0] = $invoice_no;	   //idx
		
		$query = $this->db->query($sql, $sql_param);

		return $query;
	}





	
	//게시판 등록
	function insert($data) {
		$this->db->insert("iw_bbs", $data);
		$insert_id = $this->db->insert_id();

		//그룹번호 업데이트
		$this->db->set("grp_idx", $this->db->insert_id());
		$this->db->where("idx", $this->db->insert_id());
		$this->db->update("iw_bbs");

		return $insert_id;
	}

	//게시판 수정
	function update($data) {
		$this->db->where("invoice_no", $data['invoice_no']);
		$this->db->update("iw_bbs", $data);
	}

	//게시판 첨부파일 등록
	function insert_attach($data) {
		$this->db->insert("iw_invoice_attach", $data);
		return $this->db->insert_id();
	}


	//게시판 첨부파일 삭제
	function delete_attach($invoice_no, $att_idx, $menu_code) {
		$this->db->where("invoice_no", $invoice_no);
		$this->db->where("att_idx", $att_idx);
		$this->db->where("menu_code", $menu_code);
		return $this->db->delete("iw_invoice_attach");
	}

	public function check_exist_invoice($idx){
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_proforma_invoice');
        $this->db->where('bbs_idx', $idx);
        
        $query = $this->db->get();
        $result = $query->result();
        return $result[0];
    }

    public function insert_car_invoice_batch($data){

        $sql_param = array();

        $sql_value = '';
        $sql_key = '';


        $sql_value_arr = array();
        $sql_key_arr = array_keys($data[0]);

        foreach($data as $key=>$value){
            $sql_value_sign = array();
            foreach($value as $k=>$v){
                $sql_value_sign[] = '?';
                $sql_param[] = $v;
            }
            $sql_value_arr[] = "(".implode(', ', $sql_value_sign).")";
        }
        $sql_value = implode(', ', $sql_value_arr);
        $sql_key = '(`'.implode('`, `', $sql_key_arr).'`)';
        $sql = "INSERT INTO iw_proforma_invoice $sql_key VALUES $sql_value";

        $this->db->query($sql, $sql_param);
        // echo $this->db->last_query(); exit();
        // return $result;
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function select_invoice_idx($idxs=array(),$discount=array()) {
    	foreach ($idxs as $row) {
    		$idx[] = $row;
    	}
    	foreach ($discount as $row) {
    		$discount_price[] = $row;
    	}

    	$this->db->select('iw_bbs.*,webscrape.member_no,country_list.country_name,proforma_invoice.discount_price');
    	$this->db->from('iw_bbs');
    	$this->db->where_in('iw_bbs.idx', $idx);
    	$this->db->where_in('proforma_invoice.discount_price', $discount_price);
    	$this->db->join('iw_country_list as country_list','country_list.cc=iw_bbs.country','left');
    	$this->db->join('iw_proforma_invoice as proforma_invoice','proforma_invoice.bbs_idx=iw_bbs.idx','left');
    	$this->db->join('webscrape','webscrape.member_no=iw_bbs.car_owner','left');
    	//echo $this->db->_compile_select(); exit();
    	$query = $this->db->get();
    	$result = $query->result();
    	return $result;
    }

    	function select_proforma_invoice($invoice_no, $limit_st, $limit_ed, $where) {
	
		$sql_param = array();


		$sql = '';

		$sql .= "		select p.id, p.bbs_idx, p.discount_price, p.full_path, p.created_dt, m.member_first_name, b.car_make, b.car_model, b.car_model_year              \n";
		
		$sql .= "		from (iw_proforma_invoice p LEFT OUTER JOIN iw_member m on p.customer_no = m.member_no)	    	\n";
		
		$sql .= "		LEFT OUTER JOIN iw_bbs b on b.idx = p.bbs_idx	    	\n";

		if($invoice_no !=''){
		
		$sql .= "         	where p.id = ?                                                  \n";
		
		}else{
		
		$sql .= "         	where p.id != ''                                                  \n";
		
		}

		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						$sql .='  and  ' . $key . ' '.'"'. $value.'"';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key;
					}
				}
			}
		}

		$sql .= "           order by p.created_dt desc 								\n";
		
		$sql .= "           limit ?, ?  											\n";
		
		$sql_param[count($sql_param)] = $limit_st;
		$sql_param[count($sql_param)] = $limit_ed;	

		$query = $this->db->query($sql, $sql_param);

		// print_r($sql); exit();

		return $query->result();

	}

	function delete_proforma_invoice($invoice_nos=array()){
        $id = array();
        foreach ($invoice_nos as $row) { 
            if($row !=''){
        		$id[] = $row;
        	}
        }

        $this->db->where_in('id', $id);
        return $this->db->delete("iw_proforma_invoice");
    }

    function delete_proforma_invoice_file($invoice_nos=array()){
    	$id = array();
        foreach ($invoice_nos as $row) { 
        	if($row !=''){
        		$id[] = $row;
        	}
        }
        $this->db->select('*');
        $this->db->from('iw_proforma_invoice');
        $this->db->where_in('id', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function select_all_proforma_invoice(){
    	$this->db->select('COUNT(*) as count_proforma_invoice');
    	$this->db->from('iw_proforma_invoice');
    	$query = $this->db->get();
    	$result = $query->result();
    	return $result[0]->count_proforma_invoice;
    }

}
?>
