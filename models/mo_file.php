<?php

/* ------------------------------------------------

 * 파일명 : mo_file.php

 * 개  요 : 게시판 관리

  ------------------------------------------------ */

class Mo_file extends Model {

    function __construct() {

        parent::Model();
      
        $this->load->database('default');
        $this->load->model('mo_parse', 'mo_parse');
    }

    function insert($data) {
        $this->db->set("created_dt", 'NOW()', false);
    	$this->db->insert("iw_cloud_files", $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function insert_batch($data) {
    	$value_arr = array();
		$sql_param = array();
        
		foreach($data as $value){
			if(!empty($value)){
				$value_arr[]="(?, ?, ?, ?, 'user', NOW())";
				$sql_param[]=$value['public_id'];
				$sql_param[]=$value['file_owner'];
				$sql_param[]=$value['resource_type'];
				//$sql_param[]=$value['secure_url'];
				//$sql_param[]=$value['original_filename'];
                $sql_param[]=$value['base_url'];
			}
		}
		$sql_value=implode(", ", $value_arr);
		$sql = "
			INSERT INTO iw_cloud_files (public_id,file_owner,resource_type, base_url, category, created_dt) VALUES 
			$sql_value
		";
		$query = $this->db->query($sql, $sql_param);
		
		return $query;
	}
    function select_cphoto($file_owner) {
    	$this->db->select('iw_cloud_files.*,iw_member.profile_image_id');
    	$this->db->from('iw_cloud_files');
    	$this->db->join('iw_member','iw_cloud_files.file_owner=iw_member.member_no');
    	$this->db->where('file_owner', $file_owner);
    	$query = $this->db->get();
        return $query;
    }
    function delete_cloud_file($public_id) {
    	$this->db->where_in('public_id', $public_id);
		$delete_id = $this->db->delete('iw_cloud_files'); 
		return $delete_id;
    }
    function is_owner_file($public_id,$owner_file){
        $this->db->select('file_owner');
        $this->db->from('iw_cloud_files');
        $this->db->where('public_id', $public_id);
        $this->db->where('file_owner', $owner_file);
        $query = $this->db->get();
        return $query;
    }
    function select_owner_file($public_id,$owner_file){
        $this->db->select('iw_cloud_files.*');
        $this->db->from('iw_cloud_files');
        $this->db->where('public_id', $public_id);
        $this->db->where('file_owner', $owner_file);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function set_profile_image($data,$member_no){
        $this->db->where("member_no", $member_no);
        $result = $this->db->update("iw_member", $data);        
        return $result;
    }
    function select_id_bbs_cloud() {
        $this->db->select('id');
        $this->db->from('iw_cloud_bbs_images');
        $this->db->order_by("id", "desc"); 
        $this->db->limit('1');
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->id;

    }
    function select_bbs_cloud($idx) {
        $this->db->select('*');
        $this->db->from('iw_cloud_bbs_images');
        $this->db->where('bbs_idx', $idx);
        $member_no = $_SESSION['ADMIN']['member_no'];
        $grade_no = $_SESSION['ADMIN']['grade_no'];
        if($grade_no==11){
            $this->db->join('iw_bbs','iw_bbs.idx=iw_cloud_bbs_images.bbs_idx');
            $this->db->where('iw_bbs.car_owner', $member_no);
        }
        $this->db->order_by("sort", "asc"); 
        $query = $this->db->get();
        return $query;
    }

    function select_bbs_image_upload($idx) {
        $this->db->select('*');
        $this->db->from('iw_cloud_bbs_images');
        $this->db->where('bbs_idx', $idx);
        $this->db->order_by("sort", "asc"); 
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();
        return $result;
    }
    
//    function select_bbs_cloud_all(){
//         $this->db->select('iw_bbs.idx,iw_cloud_bbs_images.*');
//         $this->db->from('iw_bbs');
//         $this->db->join('iw_cloud_bbs_images','iw_bbs.idx=iw_cloud_bbs_images.bbs_idx');
//         $query = $this->db->get();
//        //echo $this->db->last_query();
//       // exit();
//        return $query;
//    }
    function insert_bbs_batch_images($data) {
        $value_arr = array();
        $sql_param = array();
        foreach($data as $value){
            if(!empty($value)){
                $value_arr[]="(?, ?, ?, ?, ?, ?, ?, NOW())";
                $sql_param[]=$value['public_id'];
                $sql_param[]=$value['bbs_idx'];
                $sql_param[]=$value['resource_type'];
                $sql_param[]=$value['secure_url'];
                $sql_param[]=$value['sort'];
                $sql_param[]=$value['original_filename'];
                $sql_param[]=$value['base_url'];
                
            }
        }
        $sql_value=implode(", ", $value_arr);
        $sql = "
            INSERT INTO iw_cloud_bbs_images (public_id,bbs_idx,resource_type,secure_url, sort, original_filename, base_url, created_dt) VALUES 
            $sql_value
        ";
        $this->db->query($sql, $sql_param);
        $insert_id = $this->db->insert_id();
        if(!empty($value['bbs_idx'])){
            $this->mo_parse->insert_or_update($value['bbs_idx']);
        }
        return $insert_id;
    }
    function delete_bbs_cloud_file($id) {
        $this->db->where_in('public_id', $id);
        $delete_id = $this->db->delete('iw_cloud_bbs_images'); 
        return $delete_id;
    }
    function delete_bbs_file($id) {
        $this->db->where_in('id', $id);
        $delete_id = $this->db->delete('iw_cloud_bbs_images'); 
        return $delete_id;
    }
    function delete_bbs_cloud_file_items($idx){
       
          $this->db->where('bbs_idx',$idx);
          $this->db->delete('iw_cloud_bbs_images');
         //echo $this->db->last_query();
         // exit();
       
    }
    function set_primary_cloud_image($data){
        $data['sort'] = 0;
        $this->db->where("id", $data['id']);
        $result = $this->db->update("iw_cloud_bbs_images", $data);
        if(!empty($data['bbs_idx'])){
            $this->mo_parse->insert_or_update($data['bbs_idx']);
        }
        // echo $this->db->last_query(); exit();

        return $result;
    }
    function sort_all_bbs_images($data, $bbs_idx=''){

        $sql = $this->db->_update_batch('iw_cloud_bbs_images', $data, 'id');
        $result = $this->db->query($sql);
        if(!empty($bbs_idx)){
            $this->mo_parse->insert_or_update($bbs_idx);
        }
        // echo $this->db->last_query();
        return $result;

    }
        /* Set all images sort to correct sort beside one that has the att_idx (primary photo, sort = 0) */
    function resort_except_select($where){
        $sql = "";
        $this->db->query("SET @i := 0;");
        $sql .="UPDATE iw_cloud_bbs_images SET sort = @i:=@i+1 WHERE bbs_idx=? AND id != ? ORDER BY sort;";
        $sql_param = array($where['bbs_idx'], $where['id']);
        $this->db->query($sql, $sql_param);
        if(!empty($where['bbs_idx'])){
            $this->mo_parse->insert_or_update($where['bbs_idx']);
        }
        // echo $this->db->last_query();
    }
        /* Function to refresh or make correct sort of all images of any item */
    function resort_image($where){
        //required idx
        $sql = "";
        $this->db->query("SET @i := -1;");
        $sql .="UPDATE iw_cloud_bbs_images SET sort = @i:=@i+1 WHERE bbs_idx=? AND sort >=0 ORDER BY sort;";
        $sql_param = array($where['bbs_idx']);

        $this->db->query($sql, $sql_param);
        //echo $this->db->last_query();
    }
    function sortMove_image($data){
        //required id, bbs_idx, nav, sort
        $nav = $data['nav'];
        unset($data['nav']);

        $this->resortExceptPrimary($data);
        $nav_sort = $this->getNewSort_image($nav, $data['bbs_idx'], $data['idx'], $data['sort']);
        if(!$nav_sort){
            $this->resort_except_select($data);
        }
        if($nav_sort){
            if($nav_sort->sort == '0'){
                //move
                $this->set_sort($data['bbs_idx'], $nav_sort->att_idx, $data['sort']);
                $this->set_sort($data['bbs_idx'], $data['idx'], $nav_sort->sort);
                //reorder all except select
                $this->resort_except_select($data);
            }else{
                //reorder all > 0
                //$this->resortExceptPrimary($data);
                //refresh sort
                $nav_sort=$this->getNewSort_image($nav, $data['bbs_idx'], $data['idx'], $data['sort']);
                //move
               // echo "from: ". $data['sort']." to: ". $nav_sort->sort;
                $this->set_sort($data['bbs_idx'], $nav_sort->att_idx, $data['sort']);
                $this->set_sort($data['bbs_idx'], $data['idx'], $nav_sort->sort);
            }
        }
    }
    function getNewSort_image($nav, $idx, $bbs_idx, $sort){
        if($nav=='left'){
            $nav_sort = $this->getLeft_sort($idx, $bbs_idx, $sort);
        }else{
            $nav_sort = $this->getRight_sort($idx, $bbs_idx, $sort);
        }
        return $nav_sort;
    }
    function getLeft_sort($idx, $bbs_idx, $sort){

        $sql = "SELECT * FROM iw_cloud_bbs_images WHERE bbs_idx = ? AND sort < ? ORDER BY sort DESC LIMIT 1 ";

        $sql_param  = array($idx, $sort, $bbs_idx);
        $query = $this->db->query($sql, $sql_param);
        $result = $query->result();
        if($query->num_rows()<=0){
            return false;
        }

        return $result['0'];
    }

    function getRight_sort($idx, $bbs_idx, $sort){
        $sql = "SELECT * FROM iw_cloud_bbs_images WHERE bbs_idx = ? AND sort > ? ORDER BY sort ASC LIMIT 1 ";
        $sql_param  = array($idx, $sort, $bbs_idx);
        $query = $this->db->query($sql, $sql_param);
        $result = $query->result();
        if($query->num_rows()<=0){
            return false;
        }

        return $result['0'];
    }
    function set_sort($idx, $bbs_idx, $sort){
        $data['sort'] = $sort;
        $this->db->where("idx", $bbs_idx);
        $this->db->where("bbs_idx", $idx);
        $this->db->update("iw_bbs_attach", $data);
        //echo $this->db->last_query();
    }
    function select_bbs_public_id($bbs_idx) {
        $this->db->select('public_id');
        $this->db->from('iw_cloud_bbs_images');
        $this->db->where('bbs_idx', $bbs_idx);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function select_bbs_public_id_check($bbs_idx){
        $this->db->select('public_id');
        $this->db->from('iw_cloud_bbs_images');
        $this->db->where('public_id', $bbs_idx);
        $query = $this->db->get();
        $result = $query->result();

        return $result; 
    }
        function select_file_public_id($file_owner) {
        $this->db->select('public_id');
        $this->db->from('iw_cloud_files');
        $this->db->where('file_owner', $file_owner);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function update_path_images($data,$public_id){
        $this->db->where("public_id", $public_id);
        $result = $this->db->update("iw_cloud_bbs_images", $data);        
        return $result;
    }

    public function select_all_user_profile(){
        $this->db->select('id, secure_url, public_id');
        $this->db->from('iw_cloud_files');
        $this->db->where('base_url !=', '/uploads/users/');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function update_path_user_profile($data,$id){
        $this->db->where("id", $id);
        $result = $this->db->update("iw_cloud_files", $data);        
        return $result;
    }

    function select_detail_image($where=array()){
        $this->db->select('*');
        $this->db->from('iw_cloud_bbs_images');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

}