<?php



/* ------------------------------------------------

 * 파일명 : me_customer.php

 * 개  요 : customer 관련

  ------------------------------------------------ */

class Mo_account extends Model {



	function __construct() {

		parent::Model();



		$this->load->database('default');

	}

	function insert($data) {
		$this->db->insert("iw_account", $data);
		$insert_id = $this->db->insert_id();

		//그룹번호 업데이트
		$this->db->set("grp_idx", $this->db->insert_id());
		$this->db->where("idx", $this->db->insert_id());
		$this->db->update("iw_bbs");

		return $insert_id;
	}
	//insert currency
	function insert_currency($data) {
		$this->db->insert("iw_currency", $data);
		$insert_id = $this->db->insert_id();

		return $insert_id;
	}
	
	//insert currency
	function update_currency($data) {
		//그룹번호 업데이트
		$this->db->set("exchange_rate", $data['exchange_rate']);
		$this->db->set("modified_dt", $data['modified_dt']);
		$this->db->where("id", $data['id']);
		return $this->db->update("iw_currency");

	}
	//customer 조회

	function account_select($limit_st, $limit_ed, $where) {


		$sql = '';

		$sql .= "   SELECT a.id, b.account_type, b.account_name, a.account_detail, a.currency_type, a.unit_price, a.tax_price, a.tax_flg, a.unit_count, 
								IF(b.account_type='Income',a.unit_total_price,null) AS total_income,
								IF(b.account_type='Expense',a.unit_total_price,null) AS total_expense,
								a.account_date
					FROM iw_account a
					LEFT JOIN iw_account_type b
					ON a.account_id = b.id";
		$sql .= "   where 1 \n";

		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		
		$sql.=" ORDER BY a.account_date DESC						\n" ;
		$sql.=" limit ?,?						\n" ;
		$sql .= " ;                                                  \n";

		//print_r($sql); print_r($where); exit();

		//파라미터 생성

		$sql_param = array();

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}

		$sql_param[count($sql_param)] = $limit_st;

		$sql_param[count($sql_param)] = $limit_ed;
		

		$query = $this->db->query($sql, $sql_param);
		$result = $query->result();

		return $result;

	}
	
	//customer 조회 목록수

	function total_account_select($where) {

		$sql = '';

		$sql .= "   select a.*, b.account_name, a.updated_dt \n";
		$sql .= "   from iw_account as a                                     \n";
		$sql .= "   left join iw_account_type as b \n";
		$sql .= "   on a.account_id = b.id                                    \n";
		$sql .= "   where 1 \n";

		//검색 조건 생성

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				$findIt = strpos($key, 'IWSEARCHVALS');



				if ($findIt === FALSE) {

					if (strpos($key, 'IWSEARCHKEY') === FALSE) {

						$sql .='  and  ' . $key . ' ? ';

					} else {

						$key = str_replace('IWSEARCHKEY', '', $key);

						$sql .=' and ' . $key;

					}

				}

			}

		}

		$sql .= " ;                                                  \n";
		//파라미터 생성

		$sql_param = array();

		if ($where != NULL) {

			foreach ($where as $key => $value) {

				if ($value == '')

					continue;

				$sql_param[count($sql_param)] = $value;

			}

		}

		

		$query = $this->db->query($sql, $sql_param);
		$result = $query->result();
		//print_r($sql); exit();

		return $result;

	}


    function account_total_row($where=array()){
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_account');
        $this->db->where($where);
        $this->db->join("iw_account_type ", "iw_account_type.id = iw_account.account_id", "left");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

	//회원 상세 조회(관리자)
	function account_select_detail_adm($id) {
		$sql = '';
		$sql .= "select a.*,b.account_name, b.account_type FROM   
				iw_account a
				LEFT JOIN iw_account_type b ON a.account_id = b.id
				WHERE a.id='$id'
		";

		$query = $this->db->query($sql, array($id));

		return $query;
	}

	function account_update_adm($data,$bb_idx) {
		
        $sql = "select * from iw_bbs where idx='".$bb_idx."' ";
        $rs = mysql_query($sql);
        $result = mysql_fetch_array($rs);

        if(empty($result) ){
            //account_id = 1 : Buying Car(Expense) 
            if($data['account_id'] == 1 ){
            	echo "There is no matching car in car list. Please contact an adminstrator.";
            }else{
            	echo "This account type is not releated to Buying Car.<br> So it doesn't include a car index.";
            }
        	
        }else{

            // there is value in iw_account

            $bbs_data  = array();

            $bbs_data['unit_price'] = $data['unit_price'];
            $bbs_data['tax_price'] = $data['tax_price'];
            $bbs_data['unit_price_currency'] = $data['currency_type'];

            $where = $this->db->where("idx", $data['car_idx']);
            
            $this->db->update("iw_bbs", $bbs_data);

        }

        $data['updated_dt'] = func_get_date_time();

		$this->db->where("id", $data['id']);
		$this->db->update("iw_account", $data);

	}

	function account_delete_adm($idx) {
		$this->db->where("id", $idx);
		$this->db->delete("iw_account");
	}
	function account_delete_rate($id) {
		$this->db->where("id", $id);
		return $this->db->delete("iw_currency");
	}
	function select_account_rate ($where = ''){
		
		$sql = '';
		
		$sql .= 'SELECT * FROM iw_currency';
		
		$sql .= ' where id != ? ';
		//검색 조건 생성
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $findIt = strpos($key, 'IWSEARCHVALS');

                if ($findIt === FALSE) {
                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
                        $sql .='  and  ' . $key . ' ? ';
                    } else {
                        $key = str_replace('IWSEARCHKEY', '', $key);
                        $sql .=' and ' . $key;
                    }
                }
            }
        }
		$sql .= ' ORDER by exchange_dt DESC ';
		//파라미터 생성
        $sql_param = array();
        $sql_param[0] = ''; //메뉴코드
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                if ($value == '')
                    continue;
                $sql_param[count($sql_param)] = $value;
            }
        }

		$query = $this->db->query($sql,$sql_param);
		
		return $query;
	}
	
	//-----total rate num row---------------
	
	function total_select_account_rate ($where = ''){
		
		$sql = '';
		
		$sql .= 'SELECT * FROM iw_currency';
		
		$sql .= ' where id != ? ';
		//검색 조건 생성
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $findIt = strpos($key, 'IWSEARCHVALS');

                if ($findIt === FALSE) {
                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
                        $sql .='  and  ' . $key . ' ? ';
                    } else {
                        $key = str_replace('IWSEARCHKEY', '', $key);
                        $sql .=' and ' . $key;
                    }
                }
            }
        }
		$sql .= ' ORDER by exchange_dt DESC ';
		//파라미터 생성
        $sql_param = array();
        $sql_param[0] = ''; //메뉴코드
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                if ($value == '')
                    continue;
                $sql_param[count($sql_param)] = $value;
            }
        }

		$query = $this->db->query($sql,$sql_param);
		
		return $query->num_rows();
	}


	//insert buying price when register a car
    function insert_account($data) {
        $data['created_dt'] = func_get_date_time();
        
        $this->db->insert("iw_account", $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }



	    //update buying price when register a car
    function update_account($data) { 
        $sql = "select * from iw_account where car_idx = $data[car_idx]";
        $rs = mysql_query($sql);
        $result = mysql_fetch_array($rs);

        if(empty($result) ){
            //there is no value in iw_account
            $data['created_dt'] = func_get_date_time();

            $this->db->insert("iw_account", $data);
            $insert_id = $this->db->insert_id();

            return $insert_id;

        }else{
        	//var_dump($data); exit();
            // there is value in iw_account
            $data['updated_dt'] = func_get_date_time();
            $where = $this->db->where("car_idx", $data['car_idx']);
            $this->db->update("iw_account", $data);

            //return $this->db->update("iw_account", $data);
            
        }

    }




}




?>