<?php

/* ------------------------------------------------

 * 파일명 : mo_activity.php

 * 개  요 : 게시판 관리

  ------------------------------------------------ */

class Mo_activity extends Model {

    function __construct() {

        parent::Model();



        $this->load->database('default');
    }

    function select($where=array()){
        //$where['YEAR(created_dt >']='2013'; 
       
        $this->db->select('a.*, m.grade_no, m.member_first_name, m.member_id');
        $this->db->from('iw_activity as a');
        $this->db->join('iw_member as m', 'm.member_no = a.author');
        $this->db->where($where);
         
        $query = $this->db->get();
       
        $result = $query->result();

        return $result;
    }

	function insert($author, $activity_msg){
        $data['author'] = $author;
        $data['activity_msg'] = $activity_msg;
        $data['created_dt'] = func_get_date_time(); 
        $result = $this->db->insert("iw_activity", $data);
        
        return $result;
    }	

	function delete($id) {
        $this->db->where("id", $id);
        $this->db->delete("iw_activity");
    }
		

}

?>
