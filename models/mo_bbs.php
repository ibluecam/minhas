<?php

/* ------------------------------------------------

 * 파일명 : mo_bbs.php

 * 개  요 : 게시판 관리

  ------------------------------------------------ */

class Mo_bbs extends Model {

    function __construct() {

        parent::Model();

        $this->load->database('default');
        
        $this->db->query('SET SQL_BIG_SELECTS=1');
        $this->load->model('mo_parse', 'mo_parse');
    }

    public function select_all_images(){
        $this->db->select('id, secure_url, public_id');
        $this->db->from('iw_cloud_bbs_images');
        $this->db->where('public_id NOT LIKE "%.jpg" AND public_id NOT LIKE "%.png"');
        $this->db->order_by('id', 'DESC');
        //$this->db->limit('20');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    public function update_status_beside_chassis($chassis_arr, $car_owner, $icon_status){
        $data=array();
        $data['icon_status'] = $icon_status;
        $this->db->where('car_owner', $car_owner);
        $this->db->where_not_in('car_chassis_no', $chassis_arr);
        $result = $this->db->update('iw_bbs', $data);
        return $result;
    }
    public function get_country_name_by_cc($cc){
        $this->db->select("country_name");
        $this->db->from('iw_country_list');
        $this->db->where('cc', $cc);
        $query = $this->db->get();
        $result = $query->result();
        if(count($result)>0){
            return $result[0]->country_name;
        }
        
    }
    public function select_updated_car($latest_date='', $start_from){
        $this->db->select("iw_bbs.*, cl.country_name as country_name, CONCAT(ci.base_url, 't_item_thumb/', ci.public_id) as thumb_url", false);
        $this->db->from('iw_bbs');
        $this->db->where('iw_bbs.draft =', 'N');
        $this->db->where('iw_bbs.publish =', 'Y');
        $this->db->join('iw_cloud_bbs_images ci', 'ci.bbs_idx = iw_bbs.idx AND ci.sort=0', 'left');
        $this->db->join('iw_country_list as cl', 'iw_bbs.country = cl.cc','inner');
        if(!empty($latest_date)){
            $this->db->where('iw_bbs.updated_dt >', $latest_date);
        }
        $this->db->limit(20, $start_from);
        $query = $this->db->get();
    
        $result = $query->result();
        return $result;
    }
    public function count_updated_car($latest_date=''){
        $this->db->select("count(*) as total_row");
        $this->db->from('iw_bbs');
        $this->db->where('iw_bbs.draft =', 'N');
        $this->db->where('iw_bbs.publish =', 'Y');
        $this->db->join('iw_cloud_bbs_images ci', 'ci.bbs_idx = iw_bbs.idx AND ci.sort=0', 'left');
         $this->db->join('iw_country_list as cl', 'iw_bbs.country = cl.cc','inner');
        if(!empty($latest_date)){
            $this->db->where('iw_bbs.updated_dt >', $latest_date);
        }
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->total_row;
    }

    public function select_by_idxs($idxs=array(), $where=array(), $num_rows=NULL, $limit=0){
        $this->db->select("idx, car_make, car_model, car_model_year, car_chassis_no, cloud_bbs.base_url, cloud_bbs.public_id");
        $this->db->from('iw_bbs as bbs');
        $this->db->join('iw_cloud_bbs_images as cloud_bbs','cloud_bbs.bbs_idx=bbs.idx', 'left','cloud_bbs.sort',0);
        $this->db->group_by('idx');
        if(count($idxs)>0){
            $this->db->where_in('idx', $idxs);
        }
        $this->db->where($where);
        if($num_rows!=NULL){
            $this->db->limit($num_rows, $limit);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        $result = $query->result();
        return $result;
    }

    function select_by_email($more_email=array(),$where=array(), $num_rows=NULL, $limit=0){
        $this->db->select("*");
        $this->db->from('iw_member');
        if(count($more_email)>0){
            $this->db->where_in('member_no', $more_email);
        }
        $this->db->where($where);
        if($num_rows!=NULL){
            $this->db->limit($num_rows, $limit);
        }
        $query = $this->db->get();
        $result = $query->result();
       //echo $this->db->last_query();
        return $result;
    }

    // public function count_car_seller($where){
    //     $this->db->select('COUNT(*) as count_cars');
    //     $this->db->from('iw_bbs');
    //     $this->db->join('iw_member','iw_bbs.car_owner=iw_member.member_no');
    //     $this->db->where('member_no',$where);
    //     $query = $this->db->get();
    //     $result = $query->result();
    //     return $result;
    // }

    public function count_image($where){
        $this->db->select('COUNT(*) as total_rows');
        $this->db->from('iw_cloud_bbs_images');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->total_rows;
    }
    public function insert_image_batch($data){

        $sql_param = array();

        $sql_value = '';
        $sql_key = '';


        $sql_value_arr = array();
        $sql_key_arr = array_keys($data[0]);

        foreach($data as $key=>$value){
            $sql_value_sign = array();
            foreach($value as $k=>$v){
                $sql_value_sign[] = '?';
                $sql_param[] = $v;
            }
            $sql_value_arr[] = "(".implode(', ', $sql_value_sign).")";
        }
        $sql_value = implode(', ', $sql_value_arr);
        $sql_key = '(`'.implode('`, `', $sql_key_arr).'`)';
        $sql = "INSERT INTO iw_cloud_bbs_images $sql_key VALUES $sql_value";

        $result = $this->db->query($sql, $sql_param);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    public function get_detail_by_chassis($chassis_no){

        $this->db->select('idx, car_chassis_no, car_owner');
        $this->db->from('iw_bbs');
        $this->db->where('car_chassis_no', $chassis_no);
        $this->db->group_by('car_chassis_no');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
     public function count_by_chassis($chassis_no){
        $this->db->select("car_chassis_no, bbs.ignore_scan_update, bbs.idx, bbs.icon_status, created_dt, COUNT(*) as num_rows, (SELECT COUNT(*) FROM iw_cloud_bbs_images WHERE bbs_idx = bbs.idx) as image_count");
        $this->db->from('iw_bbs as bbs');
        $this->db->where_in('car_chassis_no', $chassis_no);
        $this->db->group_by('car_chassis_no');

        $query = $this->db->get();

        $result = $query->result();
        return $result;

    }
    function selct_add_more_email($where){

        $this->db->select('*');
        $this->db->from('iw_member');
        $this->db->where($where);
        $query = $this->db->get();
        // echo $this->db->last_query();
        //exit();
        $result = $query->result();

        return $result;
    }

    public function is_car_exist($where){
        $this->db->select("COUNT(*) as car_count");
        $this->db->from('iw_bbs');
        $this->db->where($where);

        $query = $this->db->get();

        $result = $query->result();
        if($result[0]->car_count>0){
            return true;
        }
        
    }


   public function get_all_car_ajax($start_page=null,$per_page=null){

		 $sql = "select b.idx,b.car_owner,b.country,b.country, b.car_chassis_no, b.car_mileage, b.unit_price, b.manu_year,b.car_make, b.menu_code,b.car_fob_currency,b.car_fob_cost,b.car_model,b.car_model_year,
                (select d.country_name from iw_country_list as d where d.cc = b.country limit 1)as fullnamecountry,
                (SELECT c.public_id FROM iw_cloud_bbs_images as c WHERE c.bbs_idx = b.idx  AND c.sort=0 LIMIT 1)AS fileimgname
                FROM iw_bbs AS b WHERE menu_code = 'product' AND publish = 'Y' ORDER BY idx DESC Limit $start_page,$per_page";
        $query = $this->db->query($sql);

		$result = $query->result();
        
        return $result;

   }

   function count_get_all_car_ajax(){
        $sql="SELECT count(*) as count_get_all_car FROM iw_bbs AS b WHERE menu_code = 'product' AND publish = 'Y' ";
        $query = $this->db->query($sql);
        $result=$query->result();
        return $result;
   }



    /* ^^U^^  Watereye lysteav  Search by brand and type =========================*/

/*
    function searchbybrand($where = null,$start_page=null,$per_page=null){
        $sql = "select * FROM iw_bbs WHERE car_make = '$where' AND car_make != '' AND created_dt > '2013' AND menu_code = 'product' AND publish = 'Y' LIMIT $start_page,$per_page ";*/

    function searchbybrand($where = null,$start_page=null,$per_page=null){
        $sql = "select *,
                (SELECT country_name FROM iw_country_list AS b WHERE b.cc = a.country LIMIT 1) AS fullcountryname,
                (SELECT c.secure_url FROM iw_cloud_bbs_images as c WHERE c.bbs_idx = a.idx  AND c.sort=0 LIMIT 1)AS fileimgname
                 FROM iw_bbs as a WHERE a.car_make = '$where' AND a.car_make != '' AND a.created_dt > '2013' AND a.menu_code = 'product' AND a.publish = 'Y' LIMIT $start_page,$per_page " ;
        $query = $this->db->query($sql);
        //var_dump($query);
        $result = $query->result();
        return $result;
    }


     function countsearchbybrand($where = null){
            $sql = "select count(*) as countdatabranecar FROM iw_bbs WHERE car_make = '$where' AND car_make != '' AND created_dt > '2013' AND menu_code = 'product' AND publish = 'Y'";
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
     }


    function searchbytypecars($where = null,$start_page = null,$per_page = null){
        $sql = "select *,
                (SELECT country_name FROM iw_country_list AS b WHERE b.cc = a.country LIMIT 1) AS fullcountryname,
                (SELECT c.secure_url FROM iw_cloud_bbs_images as c WHERE c.bbs_idx = a.idx  AND c.sort=0 LIMIT 1)AS fileimgname
                FROM iw_bbs as a WHERE a.car_body_type = '$where' AND a.car_make != '' AND a.created_dt > '2013' AND a.menu_code = 'product' AND a.publish = 'Y' LIMIT $start_page,$per_page ";

        $query = $this->db->query($sql);

        $result = $query->result();

        return $result;
    }

    function countsearchbytype($where = null){
            $sql = "select count(*) as countdatatypecar FROM iw_bbs WHERE car_body_type = '$where' AND car_body_type != '' AND created_dt > '2011' AND menu_code = 'product' AND publish = 'Y'";
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
     }


 function carmaketodropdownsearch(){
        $sql = "
            SELECT car_make,idx AS id, COUNT(idx) AS count 
              FROM iw_bbs 
              WHERE car_make != '' AND created_dt > '2013' AND menu_code = 'product' AND publish = 'Y'
              GROUP BY car_make 
              ORDER BY count DESC

        ";
        $query = $this->db->query($sql);
        $result = $query->result();


        return $result;
    }

     function search_make_Negotiate(){
         $sql = "
            SELECT iw_bbs.car_make,iw_bbs.idx AS id, COUNT(iw_bbs.idx) AS count 
              FROM iw_bbs INNER JOIN iw_negotiate
              WHERE iw_bbs.idx = iw_negotiate.car_idx AND iw_bbs.car_make != '' AND iw_bbs.created_dt > '2013' AND iw_bbs.menu_code = 'product' AND iw_bbs.publish = 'Y'
              GROUP BY iw_bbs.car_make 
              ORDER BY count DESC

        ";
         $query = $this->db->query($sql);
         $result = $query->result();


         return $result;
     }
	 
	  function search_date_Negotiate(){
         $sql = "
           SELECT DATE_FORMAT(iw_negotiate.message_update_date,('%d/%m/%Y')) as message_date, iw_negotiate.message_update_date FROM iw_negotiate WHERE iw_negotiate.message_update_date != '' GROUP BY message_date DESC
        ";
         $query = $this->db->query($sql);
         $result = $query->result();


         return $result;
     }
	 
	 function search_model_Negotiate(){
        $sql = "
            select iw_bbs.car_model,iw_bbs.car_make
                
        FROM iw_bbs INNER JOIN iw_negotiate
        WHERE iw_bbs.idx = iw_negotiate.car_idx AND iw_bbs.car_make != '' AND iw_bbs.created_dt > '2013' AND iw_bbs.menu_code = 'product' AND iw_bbs.publish = 'Y' GROUP BY iw_bbs.car_model
        ";
        $query = $this->db->query($sql);
        $result = $query->result();


        return $result;
    }



    function carmodeltodropdownsearch(){
        $sql = "
            select car_model,car_make
                
        FROM iw_bbs
        WHERE car_make != '' AND created_dt > '2013' AND menu_code = 'product' AND publish = 'Y' GROUP BY car_model
        ";
        $query = $this->db->query($sql);
        $result = $query->result();


        return $result;
    }



    function maxcaryeartodropdownsearch(){
        $sql = "
                SELECT
                manu_year
                FROM iw_bbs
                WHERE menu_code = 'product' AND publish = 'Y' GROUP BY manu_year ORDER BY manu_year DESC
        ";
        $query = $this->db->query($sql);
        $result = $query->result();


        return $result;
    }



    function mincaryeartodropdownsearch(){

        $sql = "
                SELECT
                manu_year
                FROM iw_bbs
                WHERE menu_code = 'product' AND publish = 'Y' GROUP BY manu_year ORDER BY manu_year ASC
        ";
        $query = $this->db->query($sql);
        $result = $query->result();


        return $result;
    }



    function mincarpricetodropdownsearch(){
        $sql = "
                SELECT
                car_fob_cost
                FROM iw_bbs
                WHERE menu_code = 'product' AND publish = 'Y' GROUP BY car_fob_cost ORDER BY car_fob_cost ASC
        ";
        $query = $this->db->query($sql);
        $result = $query->result();


        return $result;
    }


    function maxcarpricetodropdownsearch(){
        $sql = "
                SELECT
                car_fob_cost
                FROM iw_bbs
                WHERE menu_code = 'product' AND publish = 'Y' GROUP BY car_fob_cost ORDER BY car_fob_cost DESC
        ";
        $query = $this->db->query($sql);
        $result = $query->result();


        return $result;
    }

    function validate_change_supporter($country, $member_no){
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_bbs_supporter');
        $this->db->where('bbs_country', $country);
        $this->db->where('support_member_no', $member_no);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }

    function is_insert_support($countries, $support_member_no){
        if ($countries !='' && $support_member_no !='') {
            $sql_param = array();
            $sql_str_arr= array();
            $sql_str = '';
            foreach($countries as $country){
                $sql_str_arr[]="?";
                $sql_param[] = $country;
            }
            $sql_str = implode(', ', $sql_str_arr);
            $sql = "SELECT count(support_member_no) as support_member_no FROM iw_bbs_supporter WHERE bbs_country IN ($sql_str) AND support_member_no=$support_member_no";
            $query = $this->db->query($sql, $sql_param);
            //echo $this->db->last_query();
            return $query->result();
        }
    }
    function insert_support_default($member_no){
        $data['support_member_no'] = $member_no; 
        /*** GET EXISTING SUPPORT ***/
        $q = $this->select_support_default();
        /*** CHECK SUPPORT ***/
        if ( $q->num_rows() > 0 ){
            $this->db->where('bbs_idx IS NULL');
            $this->db->where('bbs_country IS NULL');
            $this->db->update('iw_bbs_supporter',$data);
        }else{
            $this->db->set('support_member_no', $member_no);
            $this->db->insert('iw_bbs_supporter',$data);
        }
    }

    function select_support_default(){
        $this->db->where('bbs_idx IS NULL');
        $this->db->where('bbs_country IS NULL');
        $q = $this->db->get('iw_bbs_supporter');
        return $q;
    }

    function insert_support_by_country($countries, $support_member_no){
        $sql_param = array();
        $sql_str_arr= array();
        $sql_str = '';
        $result = false;
        foreach($countries as $country){
            $sql_str_arr[]="(?, ?)";
            $sql_param[] = $country;
            $sql_param[] = $support_member_no;
        }
        $sql_str = implode(', ', $sql_str_arr);
        $sql = "INSERT INTO iw_bbs_supporter (bbs_country, support_member_no) VALUES $sql_str";
        
        if(count($countries)>0){
            $result = $this->db->query($sql, $sql_param);
            //echo $this->db->last_query();
        }
        return $result;
    }

    function insert_support_by_car_idx($car_idxs, $support_member_no){
       
        $sql_param = array();
        $sql_str_arr= array();
        $sql_str = '';
        $result = false;
        $this->remove_support_by_idx($car_idxs);
        foreach($car_idxs as $idx){
            $sql_str_arr[]="(?, ?)";
            $sql_param[] = $idx;
            $sql_param[] = $support_member_no;
        }
        $sql_str = implode(', ', $sql_str_arr);
        $sql = "INSERT INTO iw_bbs_supporter (bbs_idx, support_member_no) VALUES $sql_str";
        
        if(count($car_idxs)>0){
            $result = $this->db->query($sql, $sql_param);
            //echo $this->db->last_query();
        }
        return $result;
    }

    function remove_support_by_idx($car_idxs){
        $sql_param = array();
        $where_sql_arr= array();
        $where_sql = '';
        foreach($car_idxs as $idx){
            $where_sql_arr[]="?";
            $sql_param[] = $idx;
            
        }
        $where_sql = implode(', ', $where_sql_arr);
        $sql = "DELETE FROM iw_bbs_supporter WHERE bbs_idx IN ($where_sql)";
        
        if(count($car_idxs)>0){
            $result = $this->db->query($sql, $sql_param);
            //echo $this->db->last_query();
            return $result;
        }
        
    }

    function delete_by_bbs_idx($id){
        $this->db->where_in("bbs_idx",$id);
        $this->db->delete("iw_bbs_supporter");
    }

    function adm_select_bbs_for_support($include_selected=false, $where=array(), $offset=0, $num_rows=NULL){
        $sql_param=array();
        $sql_4='';
        $sql_1 = " SELECT bbs.idx, bbs.car_chassis_no, bbs.car_model_year, bbs.car_make, bbs.car_model, bbs_owner.company_name, bbs_owner.contact_person_name, supporter.member_first_name as support_first_name, supporter.member_last_name as support_last_name, country_list.country_name";
        $sql_2 = " FROM `iw_bbs` bbs 
            LEFT JOIN iw_country_list country_list ON bbs.country = country_list.cc
            LEFT JOIN iw_member bbs_owner ON bbs_owner.member_no = bbs.car_owner
            LEFT JOIN iw_bbs_supporter bbs_supporter ON bbs_supporter.bbs_idx = bbs.idx
            LEFT JOIN iw_member supporter ON supporter.member_no = bbs_supporter.support_member_no 
            ";
        /*** Where query ***/
        $sql_3 = " WHERE bbs.draft='N'";
        if(isset($where['search_keyword'])){
            $sql_3.= ' AND (bbs.car_chassis_no LIKE ? OR bbs.car_stock_no LIKE ?) ';

            $sql_param[]='%'.$where['search_keyword'].'%';
            $sql_param[]='%'.$where['search_keyword'].'%';
            unset($where['search_keyword']);
        }
        if(isset($where['car_make'])){
            $sql_3.= ' AND car_make = ?';
            $sql_param[]=$where['car_make'];
        }
        if(isset($where['car_model'])){
            $sql_3.= ' AND car_model = ?';
            $sql_param[]=$where['car_model'];
        }
        if(isset($where['car_model_year'])){
            $sql_3 .= ' AND car_model_year = ?';
            $sql_param[]=$where['car_model_year'];
        }
        if($include_selected){
            $sql_3.=" AND supporter.member_no IS NOT NULL";
        }
        /*** Limit Query ***/

        if($num_rows!=NULL){
            $sql_4 .= " LIMIT ?, ? ";
            $sql_param[]=$offset;
            $sql_param[]=$num_rows;
        }
        /*** QUERY FOR COUNT DATA ***/
        $sql_count_1 = "SELECT COUNT(*) as count_rows";
        $sql_count = $sql_count_1.$sql_2.$sql_3;
        $query_count = $this->db->query($sql_count, $sql_param);
        //echo $this->db->last_query();
        $result_count=$query_count->result();
        $total_rows = $result_count[0]->count_rows;
        //echo $total_rows;
        /*** END QUERY FOR COUNT DATA ***/
        
        $sql_rows = $sql_1.$sql_2.$sql_3.$sql_4;
        $query_rows = $this->db->query($sql_rows, $sql_param);
        //echo $this->db->last_query();
        $query_rows->total_rows = $total_rows;
        return $query_rows;
        
    }

    function get_total_bbs_support_country(){
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_bbs_supporter');
        $query = $this->db->get();
        return $query->result();
    }

    function get_support_country($selected = false){
        $where=array();
        $where['menu_code']='product';
        $where['publish']='Y';

        $this->db->select("iw_bbs_supporter.id, iw_bbs.country,iw_country_list.country_name, iw_country_list.cc", false);
        $this->db->from('iw_country_list');
        $this->db->join('iw_bbs', 'iw_bbs.country = iw_country_list.cc', 'left');
        $this->db->where("(iw_bbs.country !='')");
        if($selected){
            /*** GET EXISITING SELECTED COUNTRY ***/
            $this->db->join('iw_bbs_supporter', 'iw_bbs_supporter.bbs_country = iw_bbs.country', 'inner');
        }else{
            /*** GET COUNTRY THAT IS NOT SELECTED YET***/
            $this->db->join('iw_bbs_supporter', 'iw_bbs_supporter.bbs_country = iw_bbs.country', 'left');
        }
        $this->db->join('iw_member', 'iw_bbs_supporter.support_member_no = iw_member.member_no', "left");
        $this->db->where($where);

        $this->db->group_by('iw_bbs.country');
        $query = $this->db->get();

        return $query->result();
    }

    function get_bbs_support_country(){
        $where=array();

        $this->db->select("iw_bbs_supporter.id, iw_country_list.country_name, iw_country_list.cc, CONCAT_WS('', iw_member.member_first_name, ' ', iw_member.member_last_name) as support_name", false);
        $this->db->from('iw_country_list');
        $this->db->join('iw_bbs_supporter', 'iw_bbs_supporter.bbs_country = iw_country_list.cc');
        $this->db->join('iw_member', 'iw_bbs_supporter.support_member_no = iw_member.member_no', "left");
        
        $query = $this->db->get();

        return $query->result();
    }

    // function get_bbs_support_country($selected = false){
    //     $where=array();
    //     $where['menu_code']='product';
    //     $where['publish']='Y';

    //     $this->db->select("iw_bbs_supporter.id, iw_bbs.country,iw_country_list.country_name, iw_country_list.cc, CONCAT_WS('', iw_member.member_first_name, ' ', iw_member.member_last_name) as support_name", false);
    //     $this->db->from('iw_bbs');
    //     $this->db->join('iw_country_list', 'iw_bbs.country = iw_country_list.cc');
       
        
    //     if($selected){
    //         /*** GET EXISITING SELECTED COUNTRY ***/
    //         $this->db->join('iw_bbs_supporter', 'iw_bbs_supporter.bbs_country = iw_bbs.country', 'inner');
    //     }else{
    //         /*** GET COUNTRY THAT IS NOT SELECTED YET***/
    //         $this->db->join('iw_bbs_supporter', 'iw_bbs_supporter.bbs_country = iw_bbs.country', 'left');
    //         $this->db->where("(iw_bbs_supporter.bbs_country IS NULL)");
    //     }
    //     $this->db->join('iw_member', 'iw_bbs_supporter.support_member_no = iw_member.member_no', "left");
    //     $this->db->where($where);

    //     $this->db->group_by('iw_bbs.country');
    //     $query = $this->db->get();
    //     //echo $this->db->last_query();

    //     return $query->result();
    // }
    
    function get_car_country(){
          $where=array();
          $where['menu_code']='product';
          $where['publish']='Y';
          $this->db->select('iw_bbs.country,iw_country_list.country_name as com, iw_country_list.cc');
    	  $this->db->from('iw_bbs');
    	  $this->db->join('iw_country_list', 'iw_bbs.country = iw_country_list.cc');
          $this->db->where($where);
          $this->db->distinct();
          $this->db->group_by('iw_bbs.country');
	       $query = $this->db->get();

          //echo $this->db->last_query();
         // exit();
	  return $query->result();
      }

    function searchboxcar($carmake = null,$carmodel = null,$keyword = null,$carminyear = null,$carminprice = null,$carcountry = null,$carmaxyear = null,$carmaxrpice = null,$start_page=null,$per_page=null){

            if(($carmake != "") or ($carmake != null)){
                $a = "and car_make = '$carmake' ";


            }else{
                $a = " ";
            }


            if(($carmodel != "") or ($carmodel != null)){
                $b = "and car_model = '$carmodel' ";
            }else{
                $b = " ";
            }
			
			$i=0;
			$c = "";
            if(($keyword != "") or ($keyword != null)){
				
				
				//$space = substr_count($keyword, ' ');
				$exkeyword = explode(" ",trim($keyword));
				$countkeyword = count($exkeyword);
				//echo $countkeyword ;
				if($countkeyword > 1){ 
				
					if($countkeyword == 2){
					
						$keyword = explode(" ",$keyword);
				
						$element = "";
						foreach($keyword as $element){
							
					
							if (is_numeric($element)) {
							
								$c = " and car_model_year = '$element' ";
								/*$c .= " and car_stock_no = '$element' ";
								$c .= " and car_chassis_no = '$element' ";*/
								
							}else{
								// $element2 = count($element) + count($element);
								
									$c .= "and car_make LIKE '%".$element."%' or car_model LIKE '%".$element."%'";
										
						
							}
							//echo $element."<BR/>";
						
						}
						
					
					}else{
							
							
						$keyword = explode(" ",$keyword);
				
						$element = "";
						foreach($keyword as $element){
					
					
							if (is_numeric($element)) {
							
								$c = " and car_model_year = '$element' ";
								
							}else{
								// $element2 = count($element) + count($element);
								$i = $i + 1;
								
								if($i == 1){
									$c .= "and car_make LIKE '%".$element."%'";
								}else{
									$c .= " and car_model LIKE '%".$element."%'";
								}
									
										
						
							}
							//echo $element."<BR/>";
						
						}
											
					}
				

				
				}else{
			   	
				if (is_numeric($keyword)) {
					$c .= " and car_model_year = '$keyword' or car_stock_no = '$keyword' OR  car_chassis_no like'%".$keyword."%'";
				}else{
					 $c .= " and car_chassis_no = '$keyword' or car_make LIKE '%".$keyword."%' or car_model LIKE '%".$keyword."%'";
				}
			   
				
				}

            }else{
                $c = " "; 
            }


            if(($carminyear != "") or ($carminyear != null)){
                $d = "and car_model_year >= '$carminyear' ";
            }else{
                $d = " ";
            }


            if(($carminprice != "") or ($carminprice != null)){
                $e = "and car_fob_cost >= '$carminprice' ";
            }else{
                 $e = " ";
            }


            if(($carcountry != "") or ($carcountry != null)){
                $f = "and country = '$carcountry' ";
            }else{
                $f = " ";
            }


            if(($carmaxyear != "") or ($carmaxyear != null)){
                $g = "and car_model_year <= '$carmaxyear' ";
            }else{
                $g = " ";
            }

            if(($carmaxrpice != "") or ($carmaxrpice != null)){
                $h = "and car_fob_cost <= '$carmaxrpice' ";
            }else{
                $h = " ";
            }

            $con = $a . $b . $c . $d . $e . $f . $g . $h;
	

            //$con = substr($con,3);
			//echo $con;

            $sql = "select *,
                    (SELECT country_name FROM iw_country_list AS b WHERE b.cc = a.country LIMIT 1) AS country_name,
                    (SELECT c.public_id FROM iw_cloud_bbs_images as c WHERE c.bbs_idx = a.idx  AND c.sort=0 LIMIT 1) AS fileimgname,
                    (SELECT c.base_url FROM iw_cloud_bbs_images as c WHERE c.bbs_idx = a.idx  AND c.sort=0 LIMIT 1) AS base_url FROM iw_bbs as a WHERE a.publish = 'Y' AND a.menu_code = 'product' $con order by a.icon_status ASC, a.created_dt DESC Limit $start_page,$per_page";
            $query = $this->db->query($sql);

            $result = $query->result();

            return $result;


    }

 function countsearchboxcar($carmake = null,$carmodel = null,$keyword = null,$carminyear = null,$carminprice = null,$carcountry = null,$carmaxyear = null,$carmaxrpice = null){

            if(($carmake != "") or ($carmake != null)){
                $a = "and car_make = '$carmake' ";


            }else{
                $a = " ";
            }


            if(($carmodel != "") or ($carmodel != null)){
                $b = "and car_model = '$carmodel' ";
            }else{
                $b = " ";
            }

            if(($keyword != "") or ($keyword != null)){
                $c = "and  car_chassis_no LIKE '%".$keyword."%' Or car_make LIKE '%".$keyword."%'               
               Or car_model LIKE '%".$keyword."%' Or car_model_year LIKE '%".$keyword."%' 
               Or idx LIKE '%".$keyword."%' Or car_body_type LIKE '%".$keyword."%'";
            }else{
                $c = " ";
            }


            if(($carminyear != "") or ($carminyear != null)){
                $d = "and car_model_year = '$carminyear' ";
            }else{
                $d = " ";
            }


            if(($carminprice != "") or ($carminprice != null)){
                $e = "and car_fob_cost = '$carminprice' ";
            }else{
                 $e = " ";
            }


            if(($carcountry != "") or ($carcountry != null)){
                $f = "and country = '$carcountry' ";
            }else{
                $f = " ";
            }


            if(($carmaxyear != "") or ($carmaxyear != null)){
                $g = "and car_model_year = '$carmaxyear' ";
            }else{
                $g = " ";
            }

            if(($carmaxrpice != "") or ($carmaxrpice != null)){
                $h = "and car_fob_cost = '$carmaxrpice' ";
            }else{
                $h = " ";
            }

            $con = $a . $b . $c . $d . $e . $f . $g . $h;

            //$con = substr($con,3);


            $sql = "select count(*) as countsearhbox FROM iw_bbs as a WHERE a.publish = 'Y' AND a.menu_code = 'product' $con ";
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;


    }

    function countcarhongkong(){
        $sql = "select COUNT(*)AS counthongkongcar FROM iw_bbs WHERE country = 'hk' AND menu_code = 'product' AND publish = 'Y'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function countcarthailand(){
        $sql = "select COUNT(*)AS countthailandcar FROM iw_bbs WHERE country = 'th' AND menu_code = 'product' AND publish = 'Y'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function countcarjapan(){
        $sql = "select COUNT(*)AS countjapancar FROM iw_bbs WHERE country = 'jp' AND menu_code = 'product' AND publish = 'Y'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }


     function countcarkorea(){
        $sql = "select COUNT(*)AS countkoreacar FROM iw_bbs WHERE country = 'kr' AND menu_code = 'product' AND publish = 'Y'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }




    function search_alldata_bycountry_flag($wherecountry = null,$start_page=null,$per_page=null){

        $sql = "select *,
                (SELECT country_name FROM iw_country_list AS b WHERE b.cc = a.country LIMIT 1) AS fullcountryname,
                (SELECT c.file_name FROM iw_bbs_attach AS c WHERE c.idx = a.idx LIMIT 1)AS fileimgname
                FROM iw_bbs AS a WHERE country = '$wherecountry' AND menu_code = 'product' AND publish = 'Y' LIMIT $start_page, $per_page";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }


    function count_search_alldata_bycountry_flag($wherecountry = null){

        $sql = "select count(*) as countcountrylist,
                (SELECT country_name FROM iw_country_list AS b WHERE b.cc = a.country LIMIT 1) AS fullcountryname,
                (SELECT c.file_name FROM iw_bbs_attach AS c WHERE c.idx = a.idx LIMIT 1)AS fileimgname
                FROM iw_bbs AS a WHERE country = '$wherecountry' AND menu_code = 'product' AND publish = 'Y'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    
    function car_make_insert($data){
       $car_make=$_POST['values']['car_make'];
       $query = $this->db->get_where('iw_make', array( 'make_name' => $car_make));
       $count = $query->num_rows();
        if ($count <= 0) {
            $this->db->insert('iw_make',$data);
            $insert_id = $this->db->insert_id();
            $make[]=array('name'=>$car_make);
            $this->insertSQLData('brands',$make);
            return $insert_id;
         }else{
            $result = $query->result();
            return $result[0]->id;
         }
    }

     function car_model_insert($data_mode){
       $car_model=$_POST['values']['car_model'];
       $query = $this->db->get_where('iw_model', array( 'model_name' => $car_model));
       $count = $query->num_rows();
       if ($count<=0){
        $this->db->insert('iw_model',$data_mode);
       }
    }



    /* ^^U^^  Watereye lysteav  Search by brand and type =========================*/



    function get_color_options(){
        $sql = "SELECT * FROM iw_bbs_color ORDER BY color_name";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function get_brand_list(){
        $sql = "select id,make_name,icon_name,(SELECT COUNT(*) FROM iw_bbs  WHERE iw_make.make_name=iw_bbs.car_make)AS c_brand FROM iw_make ORDER BY c_brand DESC";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;

    }

    function get_bodytype_list(){
        $sql = "select id,body_title,body_name,icon,(SELECT COUNT(*) FROM iw_bbs AS b WHERE a.body_name = b.car_body_type)AS countbodytype FROM iw_body_type AS a";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;

    }

    function sortMove($data){
        //required idx, att_idx, nav, sort
        $nav = $data['nav'];
        unset($data['nav']);

        $this->resortExceptPrimary($data);
        $nav_sort = $this->getNewSort($nav, $data['idx'], $data['att_idx'], $data['sort']);
        if(!$nav_sort){
            $this->resortExceptSelect($data);
        }
        if($nav_sort){
            if($nav_sort->sort == '0'){
                //move
                $this->setSort($data['idx'], $nav_sort->att_idx, $data['sort']);
                $this->setSort($data['idx'], $data['att_idx'], $nav_sort->sort);
                //reorder all except select
                $this->resortExceptSelect($data);
            }else{
                //reorder all > 0
                //$this->resortExceptPrimary($data);
                //refresh sort
                $nav_sort=$this->getNewSort($nav, $data['idx'], $data['att_idx'], $data['sort']);
                //move
               // echo "from: ". $data['sort']." to: ". $nav_sort->sort;
                $this->setSort($data['idx'], $nav_sort->att_idx, $data['sort']);
                $this->setSort($data['idx'], $data['att_idx'], $nav_sort->sort);
            }
        }
    }

    function getNewSort($nav, $idx, $att_idx, $sort){
        if($nav=='left'){
            $nav_sort = $this->getLeftSort($idx, $att_idx, $sort);
        }else{
            $nav_sort = $this->getRightSort($idx, $att_idx, $sort);
        }
        return $nav_sort;
    }
    function getLeftSort($idx, $att_idx, $sort){

        $sql = "SELECT * FROM iw_bbs_attach WHERE idx = ? AND sort < ? ORDER BY sort DESC LIMIT 1 ";

        $sql_param  = array($idx, $sort, $att_idx);
        $query = $this->db->query($sql, $sql_param);
        //echo $this->db->last_query();
        // echo $this->db->last_query();
        $result = $query->result();
        if($query->num_rows()<=0){
            return false;
        }

        return $result['0'];
    }

    function getRightSort($idx, $att_idx, $sort){
        $sql = "SELECT * FROM iw_bbs_attach WHERE idx = ? AND sort > ? ORDER BY sort ASC LIMIT 1 ";
        $sql_param  = array($idx, $sort, $att_idx);
        $query = $this->db->query($sql, $sql_param);
        $result = $query->result();
        if($query->num_rows()<=0){
            return false;
        }

        return $result['0'];
    }

    function setSort($idx, $att_idx, $sort){
        //required idx, att_idx, sort
        $data['sort'] = $sort;
        $this->db->where("idx", $idx);
        $this->db->where("att_idx", $att_idx);
        $this->db->update("iw_bbs_attach", $data);
        // echo $this->db->last_query();
    }
    /* Function to refresh or make correct sort of all images of any item */
    function resortExceptPrimary($where){
        //required idx
        $sql = "";
        $this->db->query("SET @i := 0;");
        $sql .="UPDATE iw_bbs_attach SET sort = @i:=@i+1 WHERE idx=? AND sort >0 ORDER BY sort;";
        $sql_param = array($where['idx']);

        $this->db->query($sql, $sql_param);
        //echo $this->db->last_query();
    }
    /* Set all images sort to correct sort beside one that has the att_idx (primary photo, sort = 0) */
    function resortExceptSelect($where){
        //required idx, att_idx

        $sql = "";
        $this->db->query("SET @i := 0;");
        $sql .="UPDATE iw_cloud_bbs_images SET sort = @i:=@i+1 WHERE bbs_idx=? AND id != ? ORDER BY sort;";
        $sql_param = array($where['bbs_idx'], $where['id']);
        $this->db->query($sql, $sql_param);
       //echo $this->db->_error_message();
    }
    function count_by_status($where=array()){
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        if(isset($where['icon_status'])){
            unset($where['icon_status']);
        }
        $this->db->select('COUNT(*) as num_rows , icon_status');
        $this->db->from('iw_bbs');
        
        if(!empty($where['car_keyword'])){

            $this->db->where("(car_chassis_no LIKE '".$where['car_keyword']."' OR car_stock_no LIKE '".$where['car_keyword']."' OR car_make LIKE '".$where['car_keyword']."')");
        }
        unset($where['car_keyword']);
        $this->db->where($where);
        $this->db->group_by('icon_status');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function count_by_publish($where=array()){
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        $where['publish'] = 'N';
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_bbs');
        
        if(!empty($where['car_keyword'])){

            $this->db->where("(car_chassis_no LIKE '".$where['car_keyword']."' OR car_stock_no LIKE '".$where['car_keyword']."' OR car_make LIKE '".$where['car_keyword']."')");
        }
        unset($where['car_keyword']);
        $this->db->where($where);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function countPrimaryImage($idx){
        $sql = "SELECT COUNT(*) as num_rows FROM iw_bbs_attach WHERE idx = ? AND `sort`=? ";
        $sql_param  = array($idx, 0);
        $query = $this->db->query($sql, $sql_param);
        $result = $query->result();
        return $result['0']->num_rows;
    }
    function set_primary_image($data){
        $data['sort'] = 0;
        $this->db->where("att_idx", $data['att_idx']);
        $result = $this->db->update("iw_bbs_attach", $data);

        return $result;
    }
    function set_primary_photo($data){
        $data['sort'] = 0;
        $this->db->where("id", $data['id']);
        $result = $this->db->update("iw_cloud_bbs_images", $data);

        return $result;
    }

    function getoldpassword($member_id,$oldpassword)
    {
        $sql = "SELECT member_no FROM iw_member WHERE member_id = ? AND member_pwd = ? LIMIT 1 ";
        $sql_param  = array($member_id, $oldpassword);
        $query = $this->db->query($sql, $sql_param);
        $result = $query->result();
        if($query->num_rows() > 0){
            return TRUE;
        }
        return $result['0'];
    }

    function add_detail_car($data) {

        $this->db->insert("iw_bbs_file", $data);
        $insert_id = $this->db->insert_id();
    }

    function registerUser($data) {

        $this->db->insert("iw_member", $data);
        $insert_id = $this->db->insert_id();
        //echo $this->db->last_query();
        return $insert_id;
    }
    function registerUserTest($data) {

        $this->db->insert("iw_member_test", $data);
        $insert_id = $this->db->insert_id();
        //echo $this->db->last_query();
        return $insert_id;
    }
    function update_supporter($id,$data){

        $this->db->where('id',$id);
        $this->db->update('iw_bbs_supporter',$data);
        // echo $this->db->last_query(); exit();
    }




    function updateuser($data,$idx) {
        $this->db->where("member_id",$idx);
        return $this->db->update("iw_member",$data);
    }
    function update_account($id,$data){

        $this->db->where('member_no',$id);
        $this->db->update('iw_member',$data);
    }

    //게시판 조회 목록수

    function total_select($menu_code, $where, $where2, $member_id = '') {

        $sql = '';

        $sql .= "select a.*                                                                    \n";

        $sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";

        $sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";

        $sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";

        $sql .= "      ,b.* ,c.member_first_name, c.member_last_name                           \n";

        $sql .= "      ,a.idx , e.*                                                                  \n";

        $sql .= "      ,CONCAT(ifnull(c.member_first_name,''),'  ',ifnull(c.member_last_name,'')) as customer_full_name                                                                \n";

        $sql .= "      ,CONCAT(ifnull(d.member_first_name,''),'  ',ifnull(d.member_last_name,'')) as sales_full_name                                                                  \n";

        $sql .= "  from (                                                                      \n";

        $sql .= "        select * from iw_bbs                                                  \n";

        $sql .= "         where menu_code = ?                                                  \n";

        $sql .= "           and notice_yn = 'N'                                                \n";

        //리스트는 한달내 목록만 표시


        $sql .= "           and (CASE WHEN menu_code='product' AND member_id != 'sbtusa'	   \n";
        $sql .= "          				AND car_body_type != 'brandnew'	  					   \n";
        //$sql .= "           	  THEN (DATE_ADD(`created_dt`,INTERVAL 3 MONTH) >= now() )	   \n";
        $sql .= "           	  THEN (created_dt >= '2013-11-18' )	   					   \n";
        $sql .= "           		ELSE 1=1 END)											   \n";


        // and category = ? 들어가는 곳 >>>
        //관리자만 아니면..
        if (isset($this->ci->data['session_grade_no']) && $this->ci->data['session_grade_no'] != 1) {
            $sql .= "           and (open_yn = 'Y' OR open_yn IS NULL)                        \n";
        }

        //검색 조건 생성
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $findIt = strpos($key, 'IWSEARCHVALS');

                if ($findIt === FALSE) {
                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
                        $sql .='  and  ' . $key . ' ? ';
                    } else {
                        $key = str_replace('IWSEARCHKEY', '', $key);
                        $sql .=' and ' . $key;
                    }
                }
            }
        }

        if ($where2 != null) {
            foreach ($where2 as $key => $value) {
                $sql .=' and ' . $value;
            }
        }
        // and category = ? 들어가는 곳 <<<
        //회원 아이디
        if ($member_id != '') {
            $sql .= "           and member_id = ?                                              \n";
        }
        $sql .= "      AND car_visible='True'                                                  \n";
        $sql .= "      order by icon_status DESC, created_dt DESC                               \n";
        $sql .= "       ) as a                                                                 \n";
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select * from iw_bbs_attach group by idx                              \n";
        $sql .= "       ) as b                                                                 \n";
        $sql .= " on a.idx = b.idx                                                             \n";
        //customer join
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select * from iw_member                             				   \n";
        $sql .= "       ) as c                                                                 \n";
        $sql .= " on a.customer_no = c.member_no	                                            \n";

         //sales  join
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select * from iw_member                                               \n";
        $sql .= "       ) as d                                                                \n";
        $sql .= " on a.sales_no = d.member_no                                                \n";

        //iw_payment table join
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select car_idx,sum(car_allocate) as allocate_sum, currency_type from iw_payment Group By car_idx      \n";
        $sql .= "       ) as e                                                                 \n";
        $sql .= " on a.car_stock_no = e.car_idx                                            \n";


        $sql .= " ;                               \n";

        /* print_r($sql); exit(); */
        //파라미터 생성
        $sql_param = array();
        $sql_param[0] = $menu_code; //메뉴코드
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                if ($value == '')
                    continue;
                $sql_param[count($sql_param)] = $value;
            }
        }

        if ($where2 != NULL) {
            foreach ($where2 as $key => $value) {
                if ($value == '')
                    continue;
                $sql_param[count($sql_param)] = $value;
            }
        }

        //회원 아이디
        if ($member_id != '') {
            $sql_param[count($sql_param)] = $member_id;
        }


        $query = $this->db->query($sql, $sql_param);

        //print_r($sql);
        return $query->num_rows();
    }

    // 게시판 설정 읽어옴
    function config($mcd = NULL) {
        $Qry = "SELECT * FROM `iw_bbs_config`";
        if ($mcd != '') {
            $Qry .= " WHERE `bbs_code` = '" . $mcd . "'";
        }
        return $this->db->query($Qry);
    }
    function adm_total_select($where=array()){
        /*****START Default Condition*****/
        if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        $where['created_dt >=']= '2013-11-18';
        /*****END Default Condition*****/
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        if(!empty($car_keyword)){
            $this->db->where("(car_chassis_no LIKE '$car_keyword' OR car_stock_no LIKE '$car_keyword' OR car_make LIKE '$car_keyword')");
        }
        $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }
    function makeSqlWhere($where){
        $sql_where = '';
        foreach($where as $key => $value){
            $pos_ge = strpos($key, " >=");
            $pos_le = strpos($key, " <=");
            $pos_di = strpos($key, " !=");
            $pos_like = strpos($key, " like");
            if($pos_ge !== false){
                $key = str_replace(" >=", "", $key);
                $key = trim($key);
                $sql_where.=" AND {$key} >= ?";
            }elseif($pos_le !== false){
                $key = str_replace(" <=", "", $key);
                $key = trim($key);
                $sql_where.=" AND {$key} <= ?";
            }elseif($pos_like !== false){
                $key = str_replace(" like", "", $key);
                $key = trim($key);
                $sql_where.=" AND {$key} LIKE ?";
            }elseif($pos_di !== false){
                $key = str_replace(" !=", "", $key);
                $key = trim($key);
                $sql_where.=" AND {$key} != ?";
            }else{
                $key = trim($key);
                $sql_where.=" AND {$key} = ?";
            }
        }
        return $sql_where;
    }
    function adm_select_export($where=array(), $order='', $showOnlyAvailable=false, $hideSoldout=false){
        $sql_param = array();
        /***YOU CAN SET DEFAULT WHERE HERE***/
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        $where['bbs.created_dt >=']= '2013-11-18';
        /************************************/
        /**************DO NOT CHANGE*********/
        $sql_where="WHERE 1 ";
        $sql_where.=$this->makeSqlWhere($where);
        if($showOnlyAvailable){
            $sql_where.=" AND (icon_status = 'order' OR icon_status = 'sale') ";
        }
        if($hideSoldout){
            $sql_where.=" AND (icon_status != 'soldout') ";
        }
        $grade_no_session = $_SESSION['ADMIN']['session_grade_no'];
        $member_no_session = $_SESSION['ADMIN']['member_no'];

        if ($grade_no_session == 11) {
            $sql_where.= " AND car_owner = $member_no_session";
        }
        foreach($where as $value){
            $sql_param[]=$value;
        }
        /************************************/
        $sql = "SELECT
                    `bbs`.*,
                    `country_list`.country_name,
                    `sales`.member_first_name as sales_name,
                    `customer`.member_first_name as customer_first_name,
                    `customer`.member_last_name as customer_last_name,
                    `customer`.email as customer_email,
                    sum(payment.car_allocate) as allocate_sum,
                    payment.currency_type as payment_currency_type,
                    sales.member_first_name as sales_first_name,
                    sales.member_last_name as sales_last_name,
                    customer.member_first_name as customer_first_name,
                    customer.member_last_name as customer_last_name
                FROM
                    `iw_bbs` as bbs
                LEFT JOIN iw_payment as payment ON payment.car_idx = bbs.car_stock_no
                LEFT JOIN iw_member as sales ON sales.member_no = bbs.sales_no
                LEFT JOIN iw_member as customer ON customer.member_no = bbs.customer_no
                LEFT JOIN iw_country_list as country_list ON country_list.cc = bbs.country
                $sql_where
                GROUP BY bbs.idx
                $order

        ";


        $query = $this->db->query($sql, $sql_param);
        // echo $this->db->last_query(); exit();
        return $query->result();
    }

    function adm_select_export_customer($where=array(), $order='', $showOnlyAvailable=false, $hideSoldout=false){
        $sql_param = array();
        /***YOU CAN SET DEFAULT WHERE HERE***/
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        $where['bbs.created_dt >=']= '2013-11-18';
        /************************************/
        /**************DO NOT CHANGE*********/
        $sql_where="WHERE 1 ";



        $sql_where.=$this->makeSqlWhere($where);
        if($showOnlyAvailable){
            $sql_where.=" AND (icon_status = 'order' OR icon_status = 'sale') ";
        }
        if($hideSoldout){
            $sql_where.=" AND (icon_status != 'soldout') ";
        }
        $grade_no_session = $_SESSION['ADMIN']['session_grade_no'];
        $member_no_session = $_SESSION['ADMIN']['member_no'];

        if ($grade_no_session == 11) {
            $sql_where.= " AND car_owner = $member_no_session";
        }

        foreach($where as $value){
            $sql_param[]=$value;
        }
        
        /************************************/
        $sql = "SELECT
                    `bbs`.*,
                    `country_list`.country_name,
                    `sales`.member_first_name as sales_name,
                    `customer`.member_first_name as customer_name,
                    sum(payment.car_allocate) as allocate_sum,
                    payment.currency_type as payment_currency_type,
                    sales.member_first_name as sales_first_name,
                    sales.member_last_name as sales_last_name,
                    customer.member_first_name as customer_first_name,
                    customer.member_last_name as customer_last_name
                FROM
                    `iw_bbs` as bbs
                LEFT JOIN iw_payment as payment ON payment.car_idx = bbs.car_stock_no
                LEFT JOIN iw_member as sales ON sales.member_no = bbs.sales_no
                LEFT JOIN iw_member as customer ON customer.member_no = bbs.customer_no
                LEFT JOIN iw_country_list as country_list ON country_list.cc = bbs.country
                
                $sql_where
                GROUP BY bbs.idx
                $order

        ";


        $query = $this->db->query($sql, $sql_param);
        // echo $this->db->last_query(); exit();
        return $query->result();
    }

    // function calculate_total_price($where=array()){     
    //     $this->db->select('COUNT(*) as num_rows,`iw_bbs`.car_width,`iw_bbs`.car_height,`iw_bbs`.car_length,`iw_bbs`.car_fob_cost,`iw_bbs`.car_fob_currency,`iw_shipping_charge`.country_to,`iw_shipping_charge`.port_name,`iw_shipping_charge`.shipping_cost,`iw_shipping_charge`.insurance,`iw_shipping_charge`.inspection');
    //     $this->db->from('iw_bbs');
    //     $this->db->join("iw_shipping_charge", "iw_shipping_charge.country_from=iw_bbs.country", "left");
    //     $this->db->where($where);
    //     $query = $this->db->get();
    //     // echo $this->db->last_query(); exit();
    //     return $query->result();
    // }

    //Alternative to select($menu_code, $notice_yn = 'N', $limit_st, $limit_ed, $where, $where2 = '', $member_id = '') {
    function adm_select($where=array(), $offset=0, $num_rows=20, $order){
        if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }
        /*****START Default Condition*****/
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        /*****END Default Condition*****/
       
        $this->db->select('iw_bbs.*,iw_bbs.car_make as c_make, iw_country_list.country_name, SUM(iw_payment.car_allocate) as allocate_sum, iw_payment.currency_type');
        $this->db->from('iw_bbs');
        $this->db->where($where);
         if(!empty($car_keyword)){
            $this->db->where("(car_chassis_no LIKE '$car_keyword' OR car_stock_no LIKE '$car_keyword')");
        }
        $this->db->limit($num_rows, $offset);
        $this->db->join("iw_payment", "iw_payment.car_idx = iw_bbs.idx", "left");
        $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
        $this->db->join("iw_member ", "iw_member.member_no = iw_bbs.car_owner", "left");
        $this->db->join("iw_cloud_bbs_images ", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", "left");
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by($order);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    function adm_select_not_image($where=array(), $offset=0, $num_rows=20, $order){
        if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }
        /*****START Default Condition*****/
        $where['iw_bbs.menu_code'] = 'product';
        $where['iw_bbs.category'] = 'offer';

        /*****END Default Condition*****/
       
        $this->db->select('iw_bbs.*,iw_bbs.car_make as c_make, iw_country_list.country_name, SUM(iw_payment.car_allocate) as allocate_sum, iw_payment.currency_type');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        $this->db->where('iw_bbs.idx NOT IN (SELECT `bbs_idx` FROM `iw_cloud_bbs_images`)', NULL, FALSE);
         if(!empty($car_keyword)){
            $this->db->where("(car_chassis_no LIKE '$car_keyword' OR car_stock_no LIKE '$car_keyword')");
        }
        $this->db->limit($num_rows, $offset);
        $this->db->join("iw_payment", "iw_payment.car_idx = iw_bbs.idx", "left");
        $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
         $this->db->join("iw_member ", "iw_member.member_no = iw_bbs.car_owner", "left");
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by($order);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function adm_total_select_not_image($where=array()){
        /*****START Default Condition*****/
        if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }
        $where['iw_bbs.menu_code'] = 'product';
        $where['iw_bbs.category'] = 'offer';
        $where['iw_bbs.created_dt >=']= '2013-11-18';
        
        /*****END Default Condition*****/
        $this->db->select('COUNT(iw_bbs.idx) as num_rows');
        $this->db->from('iw_bbs');
        $this->db->where('iw_bbs.idx NOT IN (SELECT `bbs_idx` FROM `iw_cloud_bbs_images`)', NULL, FALSE);
        $this->db->where($where);
        if(!empty($car_keyword)){
            $this->db->where("(car_chassis_no LIKE '$car_keyword' OR car_stock_no LIKE '$car_keyword' OR car_make LIKE '$car_keyword')");
        }

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function adm_sale_select($customer_no,$where=array(), $offset=0, $num_rows=20, $order){
        /*****START Default Condition*****/
        $sql_param = array();
        $car_keyword = '';
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }
        //$where['customer_no']=$customer_no;
        /*****END Default Condition*****/
        $this->db->select('iw_bbs.*,iw_cloud_bbs_images.bbs_idx,iw_cloud_bbs_images.sort,iw_cloud_bbs_images.public_id,iw_cloud_bbs_images.base_url,iw_bbs.car_make as c_make, iw_country_list.country_name, SUM(iw_payment.car_allocate) as allocate_sum, iw_payment.currency_type');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        if(!empty($car_keyword)){
            $this->db->where("(car_chassis_no LIKE '$car_keyword' OR car_stock_no LIKE '$car_keyword' OR car_make LIKE '$car_keyword')");
        }

        $this->db->limit($num_rows, $offset);
        $this->db->join("iw_payment", "iw_payment.car_idx = iw_bbs.idx", "left");
        $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
        $this->db->join("iw_member ", "iw_member.member_no = iw_bbs.car_owner", "left");
        $this->db->join("iw_cloud_bbs_images ", "iw_cloud_bbs_images.bbs_idx  = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", "left");
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by($order);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }



    function adm_seller_select($customer_no,$where=array(), $offset=0, $num_rows, $order){
        /*****START Default Condition*****/
        $sql_param = array();
        $keyword_where = '';
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }
        //$where['customer_no']=$customer_no;

        /*****END Default Condition*****/
        $this->db->select('iw_bbs.*,iw_cloud_bbs_images.bbs_idx,iw_cloud_bbs_images.sort,iw_cloud_bbs_images.public_id,iw_cloud_bbs_images.base_url,iw_bbs.car_make as c_make, iw_country_list.country_name, SUM(iw_payment.car_allocate) as allocate_sum, iw_payment.currency_type');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        // if(!empty($car_keyword)){
        //     $this->db->where("(car_chassis_no LIKE '$car_keyword' OR car_stock_no LIKE '$car_keyword' OR car_make LIKE '$car_keyword')");
        // }
        
        if(!empty($car_keyword)){

            $keyword_arr = explode(' ', $car_keyword);
            $keyword_where .= '(';
            foreach($keyword_arr as $value){
                $keyword_where .= ' CONCAT(car_make, car_model, car_model_year, car_chassis_no) LIKE "%'.$value.'%" AND';
            }
            $keyword_where = trim($keyword_where, 'AND');
            $keyword_where .= ')';
            $this->db->where($keyword_where);
        }

        $this->db->limit($num_rows, $offset);
        $this->db->join("iw_payment", "iw_payment.car_idx = iw_bbs.idx", "left");
        $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
        $this->db->join("iw_member ", "iw_member.member_no = iw_bbs.car_owner", "left");
        $this->db->join("iw_cloud_bbs_images ", "iw_cloud_bbs_images.bbs_idx  = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", "left");
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by($order);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }


     function adm_buyers_select($member_log,$where=array(), $offset=0, $num_rows, $order){
        /*****START Default Condition*****/
        $sql_param = array();
        $car_keyword = '';
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        $where['member_no']=$member_log;
        if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }
        //$where['customer_no']=$customer_no;
        /*****END Default Condition*****/
        $this->db->select('iw_bbs.*,iw_cloud_bbs_images.bbs_idx,iw_cloud_bbs_images.sort,iw_cloud_bbs_images.public_id,iw_cloud_bbs_images.base_url,iw_bbs.car_make as c_make, iw_country_list.country_name, SUM(iw_payment.car_allocate) as allocate_sum, iw_payment.currency_type');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        if(!empty($car_keyword)){
            $this->db->where("(car_chassis_no LIKE '$car_keyword' OR car_stock_no LIKE '$car_keyword' OR car_make LIKE '$car_keyword')");
        }

        $this->db->limit($num_rows, $offset);
        $this->db->join("iw_payment", "iw_payment.car_idx = iw_bbs.idx", "left");
        $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
        $this->db->join("iw_member ", "iw_member.member_no = iw_bbs.customer_no", "left");
        $this->db->join("iw_cloud_bbs_images ", "iw_cloud_bbs_images.bbs_idx  = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", "left");
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by($order);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function adm_buyers_total_select($where=array(),$member_log){
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }
        //$where['created_dt >=']= '2013-11-18';
        $where['customer_no']=$member_log;
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_bbs');
        $this->db->where($where);
         if(!empty($car_keyword)){
            $this->db->where("(car_chassis_no LIKE '$car_keyword' OR car_stock_no LIKE '$car_keyword' OR car_make LIKE '$car_keyword')");
        }
        $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
        $this->db->join("iw_member", "iw_member.member_no = iw_bbs.customer_no", "left");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
            
            
    function adm_buyer_select($where=array(), $offset=0, $num_rows=20,$order,$member_log){
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        $where['member_no']=$member_log;
        $this->db->select('iw_bbs.*, iw_country_list.country_name');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        $this->db->limit($num_rows, $offset);
        $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
        $this->db->join("iw_member", "iw_member.member_no = iw_bbs.customer_no", "left");
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by($order);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    function adm_buyer_total_select($where=array(),$member_log){
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        //$where['created_dt >=']= '2013-11-18';
        $where['customer_no']=$member_log;
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
        $this->db->join("iw_member", "iw_member.member_no = iw_bbs.customer_no", "left");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    
    function  adm_make_select($where=array(), $offset=0, $num_rows=10){
        $this->db->select('iw_make.make_name,iw_make.icon_name,iw_make.id as id_make,iw_model.id,iw_model.model_name,iw_make.make_name as make,
                count(iw_model.make_id) as count_model ');
        $this->db->from('iw_make');
        $this->db->where($where);

        $this->db->join("iw_model", "iw_make.id=iw_model.make_id", 'left');
        $this->db->group_by("iw_make.id");
        $this->db->order_by('iw_make.make_name');
        if($num_rows!=NULL){
            $this->db->limit($num_rows, $offset);
        }
        $query = $this->db->get();
       // echo $this->db->last_query();
        //exit();
        $result = $query->result();
        return $result;
      
    }
    
    function get_total_make($where=array()){
        
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_make');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
     function get_total_model($where=array()){
        
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_make');
        $this->db->where($where);
        $this->db->join("iw_model", "iw_make.id=iw_model.make_id", 'left');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    function insert_make($data,$make_name){
        $value_arr = array();
		$sql_param = array();
		foreach($data as $value){
                    //echo $make_name;
                    //exit();
			if(!empty($value)){
				$value_arr[]="(\"$make_name\",?,?)";
				$sql_param[]=$value['public_id'];
                                $sql_param[]=$value['secure_url'];
			}
	     	}
                    
                
          $sql_value=implode(", ", $value_arr);
		$sql = "
			INSERT INTO iw_make (make_name,public_id,icon_name) VALUES 
			$sql_value
		";
		$query = $this->db->query($sql, $sql_param);
		//echo $this->db->last_query();
                //exit();
		return $query;                 
      
    }
    function insert_model($data){
        $this->db->insert("iw_model", $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
            
    function adm_make_delete($id){
        $this->db->where("id",$id);
        $this->db->delete("iw_make");
        
    }
    function select_make_icon_name($id){
        $this->db->select('icon_name');
        $this->db->from('iw_make');
        $this->db->where('id',$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result; 
    }
    function make_select_id($id){
        $where=array();
        $where['id']=$id;
        $this->db->select('*');
        $this->db->from('iw_make');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function update_make($id,$data){
      $this->db->where('id',$id);
      $this->db->update('iw_make',$data);
   }


   function  adm_model_select($where=array(), $offset=0, $num_rows=10){
        $this->db->select('iw_make.*,iw_model.id as id_model,iw_model.model_name');
        $this->db->from('iw_make');
        $this->db->where($where);
        $this->db->join("iw_model", "iw_make.id=iw_model.make_id");
        $this->db->order_by('iw_make.make_name');
        if($num_rows!=NULL){
            $this->db->limit($num_rows, $offset);
        }
        $query = $this->db->get();
        $result = $query->result();
        return $result;
      
    }  
    function select_make_for_model(){
        $this->db->select('*');
        $this->db->from('iw_make');
        $query = $this->db->get();
        $result = $query->result();
        //echo $this->db->last_query();
       // exit();
        return $result; 
    }
      function select_model_id($id){
        $where=array();
        $where['iw_model.id']=$id;
        $this->db->select('iw_make.*,iw_model.id,iw_model.model_name');
        $this->db->from('iw_make');
        $this->db->where($where);
        $this->db->join("iw_model", "iw_make.id=iw_model.make_id", 'left');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function update_model($id,$data){
       $success_msg='';
       $this->db->where('id',$id);
       $this->db->update('iw_model',$data);
      
    }
    
    function add_delete_model($idx){
        $this->db->where("id",$idx);
        $this->db->delete("iw_model");
    }
    function get_car_send($whereRaw='', $order='icon_status ASC, created_dt DESC'){
        /*****START Default Condition*****/
        $where['iw_bbs.menu_code'] = 'product';
        $where['iw_bbs.publish'] = 'Y';
        $where['iw_bbs.category'] = 'offer';
        $where['iw_bbs.created_dt >=']= '2013-11-18';
        $where['iw_cloud_bbs_images.sort =']= '0';
        /*****END Default Condition*****/
        $this->db->select('iw_bbs.*,iw_cloud_bbs_images.sort, iw_country_list.country_name,iw_cloud_bbs_images.original_filename, iw_cloud_bbs_images.public_id As fileimgname,iw_cloud_bbs_images.base_url As base_url');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        if(!empty($whereRaw)){
            $this->db->where($whereRaw);
        }
        $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
        $this->db->join("iw_country_list", "iw_country_list.cc = iw_bbs.country", 'left');
        $this->db->limit("12");
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by($order);
        $query = $this->db->get();
        // echo $this->db->last_query();
        // exit();
        $result = $query->result();

        return $result;
    }//Alternative to total_select

	function get_car_detail_send($whereRaw='', $order='icon_status ASC, created_dt DESC'){
        /*****START Default Condition*****/
        $where['iw_bbs.menu_code'] = 'product';
        $where['iw_bbs.publish'] = 'Y';
        $where['iw_bbs.category'] = 'offer';
        $where['iw_bbs.created_dt >=']= '2013-11-18';
 
        /*****END Default Condition*****/
        $this->db->select('iw_bbs.*, iw_cloud_bbs_images.sort, iw_country_list.country_name,iw_cloud_bbs_images.original_filename, iw_cloud_bbs_images.public_id As fileimgname,iw_cloud_bbs_images.base_url As base_url');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        if(!empty($whereRaw)){
            $this->db->where($whereRaw);
        }
        $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
        $this->db->join("iw_country_list", "iw_country_list.cc = iw_bbs.country", 'left');
        $this->db->limit("12");
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by($order);
        $query = $this->db->get();
       
        // exit();
        $result = $query->result();

        return $result;
    }
		
	function adm_negotiate_total_select($where=array()){

		$car_keyword = '';
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
		if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }
        $this->db->select('COUNT(negotiate.car_idx) as num_rows');
        $this->db->from('iw_bbs');
		if(!empty($where['support_member_no'])){
            $this->db->select('iw_bbs_supporter.*');
        }
		$this->db->where($where);
		$this->db->where('iw_member.business_type != "both"');
        if(!empty($car_keyword)){
            $this->db->where("(car_make LIKE '$car_keyword' OR country LIKE '$car_keyword' OR car_model LIKE '$car_keyword')");
        }
		
		$this->db->join("(select * from iw_negotiate order by message_id desc, message_update_date desc) as negotiate", "negotiate.car_idx = iw_bbs.idx");
        $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
        $this->db->join("iw_member", "iw_member.member_no = negotiate.sender_no");
		if(!empty($where['support_member_no'])){
           $this->db->join("iw_bbs_supporter", "iw_bbs_supporter.bbs_idx = negotiate.car_idx");
        }
		$this->db->group_by("negotiate.car_idx, negotiate.sender_no");
        $query = $this->db->get();
        $result = $query->num_rows();
        return $result;
    }
	
	
		function get_car_negotiate($where=array(), $offset=0, $num_rows=10,$order){
		
         /*****START Default Condition*****/
        $sql_param = array();
        $car_keyword = '';
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
        /*****END Default Condition*****/
		if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }

       $this->db->select('iw_bbs.*,negotiate.*, iw_member.`member_country`, MAX(message_update_date) as last_seen, count(negotiate.car_idx) as total_smg, negotiate.read = "seen" as seen, iw_member.member_first_name, iw_country_list.country_name, iw_cloud_bbs_images.sort, iw_cloud_bbs_images.original_filename, iw_cloud_bbs_images.public_id As fileimgname,iw_cloud_bbs_images.base_url As base_url');
		if(!empty($where['support_member_no'])){
            $this->db->select('iw_bbs_supporter.*');
        }
        $this->db->from('iw_bbs');
		$this->db->where('iw_member.business_type != "both"');
		$this->db->where($where);
		if(!empty($car_keyword)){
            $this->db->where("(car_make LIKE '$car_keyword'  OR country LIKE '$car_keyword' OR car_model LIKE '$car_keyword')");
        }
        $this->db->limit($num_rows, $offset);
        $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
		$this->db->join("(select * from iw_negotiate order by message_id desc, message_update_date desc) as negotiate", "negotiate.car_idx = iw_bbs.idx");
		$this->db->join("iw_member", "iw_member.member_no = negotiate.sender_no");
		if(!empty($where['support_member_no'])){
           $this->db->join("iw_bbs_supporter", "iw_bbs_supporter.bbs_idx = negotiate.car_idx");
        }
        $this->db->join("iw_country_list", "iw_country_list.cc = iw_member.member_country", "left");
		$this->db->group_by("negotiate.car_idx, negotiate.sender_no");
		$this->db->order_by($order);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
	
	
	
	function member_negotiate_total_select($where=array()){
		$car_keyword = '';
		if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
		$member_no = $_SESSION['ADMIN']['member_no'];
        $this->db->select('COUNT(car_idx) as num_rows');
        $this->db->from('iw_negotiate');
		$this->db->where($where);
		$this->db->where('iw_negotiate.sender_no',$member_no);
		if(!empty($car_keyword)){
            $this->db->where("(car_make LIKE '$car_keyword'  OR country LIKE '$car_keyword' OR car_model LIKE '$car_keyword')");
        }
		$this->db->join("iw_bbs ", "iw_bbs.idx = iw_negotiate.car_idx");
        $this->db->join("iw_bbs_supporter", "iw_bbs_supporter.bbs_idx = iw_negotiate.car_idx", "left");
		$this->db->join("iw_member", "iw_member.member_no = iw_bbs_supporter.support_member_no", "left");
		$this->db->join("iw_country_list", "iw_country_list.cc = iw_member.member_country", "left");
		$this->db->group_by("car_idx, sender_no");
        $query = $this->db->get();
        $result = $query->num_rows();
        return $result;
    }
	
	
		function member_negotiate($where=array(), $offset=0, $num_rows=10,$order){
		
         /*****START Default Condition*****/
        $sql_param = array();
        $car_keyword = '';
        $where['menu_code'] = 'product';
        $where['category'] = 'offer';
		$member_no = $_SESSION['ADMIN']['member_no'];
        /*****END Default Condition*****/
		if(isset($where['car_keyword'])){
            $car_keyword = $where['car_keyword'];
            unset($where['car_keyword']);
        }

       $this->db->select('iw_bbs.*,negotiate.*, iw_member.`member_country`, MAX(message_update_date) as last_seen, count(negotiate.car_idx) as total_smg, negotiate.read = "seen" as seen, iw_member.member_first_name, iw_country_list.country_name, iw_cloud_bbs_images.sort, iw_cloud_bbs_images.original_filename, iw_cloud_bbs_images.public_id As fileimgname,iw_cloud_bbs_images.base_url As base_url');
        $this->db->from('iw_bbs');
		$this->db->where($where);
		$this->db->where('negotiate.sender_no',$member_no);
		if(!empty($car_keyword)){
            $this->db->where("(car_make LIKE '$car_keyword'  OR country LIKE '$car_keyword' OR car_model LIKE '$car_keyword')");
        }
        $this->db->limit($num_rows, $offset);
        $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
		$this->db->join("(select * from iw_negotiate order by message_id desc, message_update_date desc) as negotiate", "negotiate.car_idx = iw_bbs.idx");
		
        $this->db->join("iw_bbs_supporter", "iw_bbs_supporter.bbs_idx = negotiate.car_idx", "left");
	    $this->db->join("iw_member", "iw_member.member_no = iw_bbs_supporter.support_member_no", "left");
        $this->db->join("iw_country_list", "iw_country_list.cc = iw_member.member_country", "left");
		$this->db->group_by("negotiate.car_idx, negotiate.sender_no");
		$this->db->order_by($order);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
	
	
	
	function message_read_exec($car_idx, $sender_no){
        $sql = "update iw_negotiate set iw_negotiate.`read` = 'seen' where iw_negotiate.car_idx = $car_idx and iw_negotiate.sender_no = $sender_no ";
        $this->db->query($sql);
	}
	
	function message_delete_exec($idx_array, $sender_array){
        $sql = "DELETE FROM `iw_negotiate` WHERE `car_idx` in ($idx_array) AND `sender_no` in ($sender_array) ";
        $this->db->query($sql);
		
	}
	
	function insert_message_negotiate($idx, $member_no, $message){
		$member = $_SESSION['ADMIN']['member_no'];
		date_default_timezone_set("Asia/Bangkok");
		$timestamp = date("Y-m-d H:i:s");
        $sql = "INSERT INTO iw_negotiate (`car_idx`, `sender_no`, `message`, `read`, `message_write_date`,`message_update_date`) VALUES ('$idx', '$member', '$message', 'unseen', '$timestamp', '$timestamp')";
        $this->db->query($sql);
		
	}
	
	
    //   function adm_select_support_bbs($where=array(), $offset=0, $num_rows=20){
    //     $where['menu_code'] = 'product';
    //     $where['category'] = 'offer';
    //     /*****END Default Condition*****/
    //     $this->db->select('iw_bbs.*,iw_member.*,,iw_country_list.country_name');
    //     $this->db->from('iw_bbs');
    //     $this->db->where($where);
    //     $this->db->where('bbs_supporter IS NOT NULL');
    //     $this->db->limit($num_rows, $offset);
    //     $this->db->join("iw_member", "iw_bbs.car_owner=iw_member.member_no", "left");
    //     $this->db->join("iw_country_list ", "iw_country_list.cc = iw_bbs.country", "left");
    //     $query = $this->db->get();
    //     $result = $query->result();
    //     return $result;
      
    // }

    //Alternative to select($menu_code, $notice_yn = 'N', $limit_st, $limit_ed, $where, $where2 = '', $member_id = '') {
    function user_select($where=array(), $whereRaw='', $offset=0, $num_rows=20, $order='iw_bbs.icon_status ASC, iw_bbs.created_dt DESC'){
        $calculate_price = $this->session->userdata('calculate_price');
        $port_name = $this->session->userdata('port_name');
        
        /*****START Default Condition*****/
        $where['iw_bbs.menu_code'] = 'product';
        $where['iw_bbs.publish'] = 'Y';
        $where['iw_bbs.category'] = 'offer';
        $where['iw_bbs.created_dt >=']= '2013-11-18';
        if(isset($where['keyword'])){
            $keyword = $where['keyword'];
            unset($where['keyword']);
        }
        $keyword_where = '';
        /*****END Default Condition*****/

        $this->db->select('iw_bbs.*, iw_country_list.country_name,iw_cloud_bbs_images.original_filename, iw_cloud_bbs_images.public_id As fileimgname,iw_cloud_bbs_images.base_url As base_url');
        
        if(isset($_POST['submit_calculate_price']) || isset($calculate_price) && $calculate_price!='' || isset($port_name) && $port_name!=''){ 
            $this->db->select('`iw_shipping_charge`.*');
        }

        $this->db->from('iw_bbs');
        $this->db->where($where);
        if(!empty($whereRaw)){
            $this->db->where($whereRaw);
        }
        // Search by keyword
        
        if(!empty($keyword)){

            $keyword_arr = explode(' ', $keyword);
            $keyword_where .= '(';
            foreach($keyword_arr as $value){
                $keyword_where .= ' CONCAT(car_make, car_model, car_model_year, car_chassis_no) LIKE "%'.$value.'%" AND';
            }
            $keyword_where = trim($keyword_where, 'AND');
            $keyword_where .= ')';
            $this->db->where($keyword_where);
        }
        
        // End Search by keyword
        if(isset($_POST['submit_calculate_price']) || isset($calculate_price) && $calculate_price!='' || isset($port_name) && $port_name!=''){

            $this->db->join("iw_shipping_charge", "iw_shipping_charge.country_from=iw_bbs.country", "left");
        }

        $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
        $this->db->join("iw_country_list", "iw_country_list.cc = iw_bbs.country", 'inner');
        $this->db->limit($num_rows, $offset);
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by($order);
      
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();

        return $result;
    }//Alternative to total_select
    function user_total_select($where=array(), $whereRaw=''){
        /*****START Default Condition*****/
        $where['iw_bbs.menu_code'] = 'product';
        $where['iw_bbs.publish'] = 'Y';
        $where['iw_bbs.category'] = 'offer';
        $where['iw_bbs.created_dt >=']= '2013-11-18';
        if(isset($where['keyword'])){
            $keyword = $where['keyword'];
            unset($where['keyword']);
        }
        $keyword_where = '';
        /*****END Default Condition*****/
        $this->db->select('COUNT(DISTINCT(iw_bbs.idx)) as num_rows');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        if(!empty($whereRaw)){
            $this->db->where($whereRaw);
        }
        // Search by keyword
        
        if(!empty($keyword)){

            $keyword_arr = explode(' ', $keyword);
            $keyword_where .= '(';
            foreach($keyword_arr as $value){
                $keyword_where .= ' CONCAT(car_make, car_model, car_model_year, car_chassis_no) LIKE "%'.$value.'%" AND';
            }
            $keyword_where = rtrim($keyword_where, 'AND');
            $keyword_where .= ')';
            $this->db->where($keyword_where);
        }
        
        // End Search by keyword
        $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx", 'left');

        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();
        $num_rows = $result['0']->num_rows;
        return  $num_rows;
    }
    //게시판 조회
    function select($menu_code, $notice_yn = 'N', $limit_st, $limit_ed, $where, $where2 = '', $member_id = '') {
        // 설청 추출
        $bcfg = $this->config($menu_code)->result();
        //파라미터 생성 (다중 써머리 때문에 여기에 있음)
        $sql_param = array();

        $sql = '';
        $sql .= "select a.*                                                                    \n";
        $sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
        $sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
        $sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
        $sql .= "      ,b.* ,c.member_first_name, c.member_last_name , e.*                    \n";
        $sql .= "      ,a.idx                                                                  \n";

        $sql .= "      ,CONCAT(ifnull(c.member_first_name,''),'  ',ifnull(c.member_last_name,'')) as customer_full_name                                                                \n";
        $sql .= "      ,CONCAT(ifnull(d.member_first_name,''),'  ',ifnull(d.member_last_name,'')) as sales_full_name                                                                  \n";

        $sql .= "  from (                                                                      \n";
        $sql .= "        select * from iw_bbs                                                  \n";



        if (count($bcfg) == 0) { // 다중 써머리
            $sql .= "         where menu_code like '" . $menu_code . "%'                       \n";
        } else {
            $sql .= "         where menu_code = ?                                              \n";
            $sql_param[0] = $menu_code;  //메뉴코드
        }
        $sql .= "           and notice_yn = ?                                                  \n";




        $sql .= "           and (CASE WHEN menu_code='product' AND member_id != 'sbtusa'	   \n";
        $sql .= "          				AND car_body_type != 'brandnew'	  					   \n";
        //$sql .= "           	  THEN (DATE_ADD(`created_dt`,INTERVAL 3 MONTH) >= now() )	   \n";
        $sql .= "           	  THEN (created_dt >= '2013-11-18' )	   					   \n";
        $sql .= "           		ELSE 1=1 END)											   \n";


        //관리자만 아니면..
        if (isset($this->ci->data['session_grade_no']) && $this->ci->data['session_grade_no'] != 1) {
            $sql .= "           and (open_yn = 'Y' OR open_yn IS NULL)                        \n";
        }
        //검색 조건 생성
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $findIt = strpos($key, 'IWSEARCHVALS');

                if ($findIt === FALSE) {
                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
                        $sql .='  and  ' . $key . ' ? ' . ' AND `car_visible`=\'True\' ';
                    } else {
                        $key = str_replace('IWSEARCHKEY', '', $key);
                        $sql .=' and ' . $key . ' AND `car_visible`=\'True\' ';
                    }
                }
            }
        }
        /* 		if ($where != NULL) {
          foreach ($where as $key => $value) {
          $findIt = strpos($key, 'IWSEARCHVALS');
          //var_dump($key); exit();
          $pos = strpos($key,'icon'); //icon이 붙은 체크박스는 'or'


          if($pos !== FALSE)
          {
          //var_dump("test11"); exit();
          $sql .='  OR  ' . $key . ' ? ' ;

          }else{
          //var_dump("test222"); exit();
          if ($findIt === FALSE) {
          if (strpos($key, 'IWSEARCHKEY') === FALSE) {
          $sql .='  and  ' . $key . ' ? ' ;
          } else {
          $key = str_replace('IWSEARCHKEY', '', $key);
          $sql .=' and ' . $key.' ';
          }
          }


          }

          }
          } */

        if ($where2 != null) {
            foreach ($where2 as $key => $value) {
                $sql .=' and ' . $value;
            }
        }


        //회원 아이디
        if ($member_id != '') {
            $sql .= "           and member_id = ?                                                  \n";
        }

        //일반, 공지 구분
        /*if ($notice_yn != 'Y') {
            $sql .= "      order by  created_dt DESC                               \n";
        } else {
            $sql .= "      order by grp_idx desc, order_no asc                                            \n";
        }*/

        $sql .= "         order by icon_status ASC, created_dt DESC                           \n";
        $sql .= "         limit ?, ?                                                           \n";
        $sql .= "       ) as a                                                                 \n";
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        // $sql .= "        select * from iw_bbs_attach group by idx                              \n";
        $sql .= "        select * from iw_bbs_attach WHERE sort=0 group by idx					   \n";
        // $sql .= "        							 order by att_idx asc					   \n";
        $sql .= "       ) as b                                                                 \n";
        $sql .= " on a.idx = b.idx                                                             \n";
        //customer table join
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select * from iw_member                             				   \n";
        $sql .= "       ) as c                                                                 \n";
        $sql .= " on a.customer_no = c.member_no                                            \n";

        //sales  join
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select * from iw_member                                               \n";
        $sql .= "       ) as d                                                                \n";
        $sql .= " on a.sales_no = d.member_no                                                \n";

        //iw_payment table join
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select car_idx,sum(car_allocate) as allocate_sum, currency_type from iw_payment Group By car_idx      \n";
        $sql .= "       ) as e                                                                 \n";
        $sql .= " on a.car_stock_no = e.car_idx                                            \n";

	   //iw_iw_customer_inventory table join
/*        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select * from iw_customer_inventory  \n";
        $sql .= "       ) as e                                                                 \n";
        $sql .= " on a.customer_no = e.customer_no                                            \n";
        $sql .= " ;                               \n";
*/

/*          print_r($sql);
          exit;*/


        $sql_param[count($sql_param)] = $notice_yn;  //일반 공지 구분
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                if ($value == '') {
                    continue;
                }

                $sql_param[count($sql_param)] = $value;
            }
        }

        //회원 아이디
        if ($member_id != '') {
            $sql_param[count($sql_param)] = $member_id;
        }
        if (count($bcfg) == 0) { // 다중 써머리
            $sql_param[count($sql_param)] = 0;
            $sql_param[count($sql_param)] = 10;
        } else {
            $sql_param[count($sql_param)] = $limit_st;
            $sql_param[count($sql_param)] = $limit_ed;
        }

        $query = $this->db->query($sql, $sql_param);
        return  $query;
    }

    //위시리스트 조회
    function select_wishlist($menu_code, $notice_yn = 'N', $limit_st, $limit_ed, $where2 = '', $order_by = '', $member_id = '') {
        // 설청 추출
        $bcfg = $this->config($menu_code)->result();
        //파라미터 생성 (다중 써머리 때문에 여기에 있음)
        $sql_param = array();

        $sql = '';
        $sql .= "select a.*                                                                    \n";
        $sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2, a.phone_no3) as phone_no      \n";
        $sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no3, a.mobile_no3) as mobile_no  \n";
        $sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no              \n";
        $sql .= "      ,b.*                                                                    \n";
        $sql .= "      ,a.idx                                                                  \n";
        $sql .= "  from (                                                                      \n";
        $sql .= "        select * from iw_bbs                                                  \n";

        if (count($bcfg) == 0) { // 다중 써머리
            $sql .= "         where menu_code like '" . $menu_code . "%'                       \n";
        } else {
            $sql .= "         where menu_code = ?                                              \n";
            $sql_param[0] = $menu_code;  //메뉴코드
        }
        $sql .= "           and notice_yn = ?                                                  \n";

        /*
          $sql .= "           and (CASE WHEN menu_code='product' AND member_id != 'sbtusa'	   \n";
          $sql .= "          				AND car_body_type != 'brandnew'	  					   \n";
          $sql .= "           	  THEN (DATE_ADD(`created_dt`,INTERVAL 1 MONTH) >= now() )	   \n";
          $sql .= "           		ELSE 1=1 END)											   \n";
         */

        //관리자만 아니면..
        if (isset($this->ci->data['session_grade_no']) && $this->ci->data['session_grade_no'] != 1) {
            $sql .= "           and (open_yn = 'Y' OR open_yn IS NULL)                        \n";
        }

        if ($where2 != null) {
            foreach ($where2 as $key => $value) {
                $sql .=' and ' . $value;
            }
        }

        //회원 아이디
        if ($member_id != '') {
            $sql .= "           and member_id = ?                                                  \n";
        }

        //일반, 공지 구분
        /*
          if ($notice_yn != 'Y') {
          $sql .= "      order by grp_idx desc, order_no asc                                 \n";
          } else {
          $sql .= "      order by created_dt desc                                            \n";
          }
         */

        if ($order_by != '') {
            $sql .= "      order by " . $order_by . "												\n";
        }

        $sql .= "         limit ?, ?                                                           \n";
        $sql .= "       ) as a                                                                 \n";
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select * from iw_bbs_attach group by idx                              \n";
        $sql .= "       ) as b                                                                 \n";
        $sql .= " on a.idx = b.idx;                                                            \n";

        $sql_param[count($sql_param)] = $notice_yn;  //일반 공지 구분
        //회원 아이디
        if ($member_id != '') {
            $sql_param[count($sql_param)] = $member_id;
        }
        if (count($bcfg) == 0) { // 다중 써머리
            $sql_param[count($sql_param)] = 0;
            $sql_param[count($sql_param)] = 10;
        } else {
            $sql_param[count($sql_param)] = $limit_st;
            $sql_param[count($sql_param)] = $limit_ed;
        }

        $query = $this->db->query($sql, $sql_param);
        /*
          var_dump($sql);
          exit;
         */
        return $query;
    }

    //문의게시판 조회
    function select_board_counsel($where) {

        //파라미터 생성 (다중 써머리 때문에 여기에 있음)
        $sql_param = array();

        $sql = '';
        $sql .= "select * from iw_board_counsel 	\n";

        //검색 조건 생성
        if ($where != NULL) {
            $first_loop = true;
            foreach ($where as $key => $value) {
                if ($first_loop) {
                    $sql .= " where " . $key . " ?";
                } else {
                    $sql .= " or " . $key . " ?";
                }
                $first_loop = false;
            }
        }

        $sql .= " order by board_num desc";

        //파라미터 생성
        $sql_param = array();
        if ($where != NULL) {
            $first_loop = true;
            foreach ($where as $key => $value) {
                if ($value == '')
                    continue;
                if ($first_loop) {
                    $sql_param[0] = $value;
                } else {
                    $sql_param[count($sql_param)] = $value;
                }
                $first_loop = false;
            }
        }

        /*
          //관리자만 아니면..
          if (isset($this->ci->data['session_grade_no']) && $this->ci->data['session_grade_no'] != 1) {
          $sql = "";
          }
         */

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

    //게시판 첨부파일 조회
    function select_attach($idx, $menu_code="") {
        $sql = "select * from iw_bbs_attach   \n";
        $sql .= "         where idx = ?        \n ORDER BY sort";
        // $sql .= "           and menu_code = ?  \n";
        //$sql .= "           order by att_idx desc  \n";
        //파라미터 생성
        $sql_param = array();
        $sql_param[0] = $idx;    //idx
        $sql_param[count($sql_param)] = $menu_code; //메뉴코드

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

    function select_attach_bl($bl_no, $menu_code="") {
        //{$rows->bl_no}
        $sql = "select * from iw_documents_attach where bl_no= ? ";

        $sql_param = array();
        $sql_param[0] = $bl_no;    //idx
        $sql_param[count($sql_param)] = $menu_code; //메뉴코드

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

    function select_attach_files($idx, $file_type = '') {
        $sql = "select * from iw_bbs_file   \n";
        $sql .= "         where bbs_idx = ?        \n";

        //$sql .= "           order by att_idx desc  \n";_s_
        //파라미터 생성
        $sql_param = array();
        $sql_param[0] = $idx;    //idx //메뉴코드

        if ($file_type != '') {
            $sql .= "         AND file_type = ?        \n";
            $sql_param[count($sql_param)] = $file_type;    //idx //메뉴코드
        }

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

    function select_attach_file_detail($idx, $bbs_idx) {
        $sql = "select * from iw_bbs_file   \n";
        $sql .= "         where id = ?        \n";
        $sql .= "         AND bbs_idx = ?        \n";
        //$sql .= "           order by att_idx desc  \n";
        //파라미터 생성
        $sql_param = array();
        $sql_param[0] = $idx;    //idx //메뉴코드
        $sql_param[1] = $bbs_idx;    //idx //메뉴코드

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

    //게시판 첨부파일 상세 조회
    function select_attach_detail($idx, $att_idx, $menu_code) {
        $sql = "select * from iw_bbs_attach   \n";
        $sql .= "         where idx = ?        \n";
        $sql .= "           and att_idx = ?    \n";
        $sql .= "           and menu_code = ?  \n";

        //파라미터 생성
        $sql_param = array();
        $sql_param[0] = $idx;    //idx
        $sql_param[count($sql_param)] = $att_idx;   //첨부파일 idx
        $sql_param[count($sql_param)] = $menu_code; //메뉴코드

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

    //문의 게시판 문의상품 조회
    function select_board_counsel_read($board_num) {
        $sql = "select * from iw_board_counsel   \n";
        $sql .= "         where board_num = ?        \n";

        //파라미터 생성
        $sql_param = array();
        $sql_param[0] = $board_num;    //idx

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

    //문의 게시판 첨부파일 조회
    function select_board_counsel_max_board_num() {
        $sql = "select max(board_num) as board_num from iw_board_counsel   \n";

        $query = $this->db->query($sql);

        return $query;
    }
    function select_car_image($bbs_idx){
        $where['bbs_idx'] = $bbs_idx;
        $this->db->select('public_id,base_url');
        $this->db->from('iw_cloud_bbs_images');
        $this->db->where($where);
        $this->db->order_by('sort', 'asc'); 
        // echo $this->db->last_query();exit();
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function select_car_detail($idx){
        $where['idx'] = $idx;
        $where['draft'] = 'N';
        $this->db->select('iw_bbs.*, iw_country_list.country_name');
        $this->db->from('iw_bbs');
        $this->db->join('iw_country_list', 'iw_bbs.country = iw_country_list.cc', 'left');
        $this->db->where($where);
        $query = $this->db->get();

        $result = $query->result();

        return $result[0];
    }
    //게시판 상세 조회
    function select_detail($idx, $menu_code='') {
        $sql_param = array();
        $sql_param[0] = $idx;    //idx
        $sql = '';
        $sql .= "select a.*,format(a.car_fob_cost,0) as fob,cl.country_name, c.member_id as c_member, c.member_first_name as c_first_name,c.member_id as member_id, c.member_last_name as c_last_name \n";
        $sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2) as phone_no      \n";
        $sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no2) as mobile_no  \n";
        $sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no ,b.*             \n";
        $sql .= "  from (                                                                      \n";
        $sql .= "        select * from iw_bbs                                                  \n";
        $sql .= "         where idx = ?                                                        \n";
        if(!empty($menu_code)){
            $sql .= "           and menu_code = ?                                                  \n";
            $sql_param[count($sql_param)] = $menu_code; //메뉴코드
        }
        $sql .= "       ) as a                                                                 \n";

        //iw_payment table join
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select car_idx,sum(car_allocate) as allocate_sum, currency_type from iw_payment Group By car_idx      \n";
        $sql .= "       ) as b                                                                 \n";
        $sql .= " on a.car_stock_no = b.car_idx                                            \n";
        //iw_member (customer)
        $sql.= "left join iw_member c ON a.customer_no = c.member_no \n";
        $sql.= "left join iw_country_list cl ON a.country = cl.cc \n";

        //파라미터 생성
        $query = $this->db->query($sql, $sql_param);
        //echo $this->db->last_query();
        return $query;
    }
    
    
     function select_detail_for_buyer($idx, $menu_code='',$member_log) {
        $sql_param = array();
        $sql_param[0] = $idx;    //idx
        $sql = '';
        $sql .= "select a.*,format(a.car_fob_cost,0) as fob,cl.country_name, c.member_id as c_member, c.member_first_name as c_first_name,c.member_id as member_id, c.member_last_name as c_last_name \n";
        $sql .= "      ,concat_ws('-', a.phone_no1, a.phone_no2) as phone_no      \n";
        $sql .= "      ,concat_ws('-', a.mobile_no1, a.mobile_no2) as mobile_no  \n";
        $sql .= "      ,concat_ws('-', a.fax_no1, a.fax_no2, a.fax_no3) as fax_no ,b.*             \n";
        $sql .= "  from (                                                                      \n";
        $sql .= "        select * from iw_bbs                                                  \n";
        $sql .= "         where idx = ? and customer_no='$member_log'                                                     \n";
        if(!empty($menu_code)){
            $sql .= "           and menu_code = ?                                                  \n";
            $sql_param[count($sql_param)] = $menu_code; //메뉴코드
        }
        $sql .= "       ) as a                                                                 \n";

        //iw_payment table join
        $sql .= " left outer join                                                              \n";
        $sql .= "       (                                                                      \n";
        $sql .= "        select car_idx,sum(car_allocate) as allocate_sum, currency_type from iw_payment Group By car_idx      \n";
        $sql .= "       ) as b                                                                 \n";
        $sql .= " on a.car_stock_no = b.car_idx                                            \n";
        //iw_member (customer)
        $sql.= "left join iw_member c ON a.customer_no = c.member_no \n";
        $sql.= "left join iw_country_list cl ON a.country = cl.cc \n";

        //파라미터 생성
        $query = $this->db->query($sql, $sql_param);
        //echo $this->db->last_query();
        return $query;
    }

    function select_owner_list(){
          $this->db->select('*');
          $this->db->from('iw_member');
          $query = $this->db->get();
          $result = $query->result();
          return $result;
    }
    

    //조회수 업데이트
    function update_visit_cnt($idx, $menu_code) {
        $sql = " update iw_bbs                    \n";
        $sql .= "    set visit_cnt = ifnull(visit_cnt,0) + 1 \n";
        $sql .= "  where idx = ?                   \n";
        $sql .= "    and menu_code = ?             \n";

        $this->db->query($sql, array($idx, $menu_code));
    }

    //게시판 등록
    function get_max_car_stock_no(){
        $sql = "SELECT MAX(`car_stock_no`) as max_stock_no FROM iw_bbs";

        $query = $this->db->query($sql);
        $result = $query->result();
        return $result['0']->max_stock_no;
    }

    function safe_insert($data){

        $data['created_dt'] = func_get_date_time();
        $data['menu_code'] = 'product';        
        $data['category'] = 'offer';
        $data['draft']  = 'N';
        $data['car_stock_no'] = $this->get_max_car_stock_no()+1;    
        $sql_param = array();
        $sql_key = '';
        $sql_value = '';
        foreach($data as $key=>$value){
            $sql_key .= $key.", ";
            $sql_value .= "?, ";
            $sql_param[] = $value;
        }

        $sql_key=rtrim($sql_key, ', ');
        $sql_value = rtrim($sql_value, ', ');
        $sql_param[] = $data['car_chassis_no'];

        $sql = "INSERT INTO iw_bbs (".$sql_key.")
                    SELECT ".$sql_value." FROM iw_bbs 
                    WHERE
                        NOT EXISTS (
                            SELECT
                                *
                            FROM
                                iw_bbs
                            WHERE
                                car_chassis_no = ?
                        )
                    LIMIT 1";
        $query = $this->db->query($sql, $sql_param);
        //$last_query =  $this->db->last_query();
        //file_put_contents('admin_log.txt', $last_query."\n\n", FILE_APPEND);
        return $query;
        
    }

    function insert($data) {
        $data['menu_code'] = 'product';        //메뉴코드\
        $data['category'] = 'offer';
        $data['draft']  = 'N';
        $data['car_stock_no']= $this->get_max_car_stock_no() + 1;
        $this->db->set('created_dt', 'NOW()', false);
        $result = $this->db->insert("iw_bbs", $data);
        $result=$this->db->insert_id();
        
        /*** PARSE ***/
        $data['idx'] = $result;
        if(!empty($data['idx'])){
            $this->mo_parse->insert_or_update($data['idx']);
        }
       
        return $result;
    }


    // function insert_old($data) {
    //     $this->db->insert("iw_bbs", $data);
    //     $insert_id = $this->db->insert_id();

    //     //그룹번호 업데이트
    //     $this->db->set("car_stock_no", $this->db->insert_id());
    //     $this->db->set("grp_idx", $this->db->insert_id());
    //     $this->db->where("idx", $this->db->insert_id());
    //     $this->db->update("iw_bbs");

    //     return $insert_id;
    // }
    function insert_draft() {
        $country = $_SESSION['ADMIN']['member_country'];
        $data=array("created_dt"=>func_get_date_time(),
                "country"=>$country,
                "draft"=>"Y"
            );
        $data['car_owner'] = $_SESSION['ADMIN']['member_no'];
        $this->db->insert("iw_bbs", $data);
        $insert_id = $this->db->insert_id();

        //그룹번호 업데이트
        // $this->db->set("car_stock_no", $this->db->insert_id());
        // $this->db->set("grp_idx", $this->db->insert_id());
        // $this->db->where("idx", $this->db->insert_id());
        // $this->db->update("iw_bbs");
        // echo $this->db->last_query(); exit();
        return $insert_id;
    }
    //문의 게시판 등록
    function insert_board_counsel($data) {
        $this->db->insert("iw_board_counsel", $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

	//insert order
    function insert_car_order($data) {
        $this->db->insert("iw_customer_inventory", $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    //게시판 코멘트 등록
    function insert_comment($data) {
        $this->db->insert("iw_comment", $data);
        $insert_id = $this->db->insert_id();

        /*
          // load number of comment
          $where['board_idx'] = $data['board_idx'];
          $number_of_comment = count($this->select_comment($where));
         */

        //코멘트 수 업데이트
        $sql = "update iw_board_counsel";
        $sql .= " set comment_count = comment_count + 1";
        $sql .= " where board_idx = ?";

        $this->db->query($sql, array($data['board_idx']));

        return $insert_id;
    }

    //게시판 답변 등록
    function insert_reply($data) {
        //부모글 답변 여부 'Y'
        $sql = " update iw_bbs              \n";
        $sql.="    set reply_yn = 'Y'      \n";
        $sql.="       ,member_id = ?       \n";
        $sql.="  where idx = ?             \n";

        $this->db->query($sql, array($data['member_id'], $data['grp_idx']));

        $this->db->insert("iw_bbs", $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    //게시판 하위글 등록시 정렬 순서 업데이트
    function update_order_no($grp_idx, $order_no) {
        $sql = " update iw_bbs                  \n";
        $sql.="    set order_no = order_no + 1 \n";
        $sql.="  where grp_idx = ?             \n";
        $sql.="    and order_no > ?            \n";

        $this->db->query($sql, array($grp_idx, $order_no));
    }

    //게시판 수정
    function update($data) {
        $this->db->where_in("idx", $data['idx']);
        $grade_no_session = $_SESSION['ADMIN']['session_grade_no'];
        $member_no_session = $_SESSION['ADMIN']['member_no'];

        if ($grade_no_session == 11) {
            $this->db->where("car_owner", $member_no_session);
        }

        $result = $this->db->update("iw_bbs", $data);
        /*** PARSE ***/
        if(!empty($data['idx'])){
           // $this->mo_parse->insert_or_update($data['idx']);
        }
       
        return $result;
    }
    function adm_update($data, $where){
        $this->db->where($where);
        $result=$this->db->update('iw_bbs', $data);
        $idx = 0;
        /*** PARSE ***/

        if(isset($data['idx'])){
            if(!empty($data['idx'])){
                $idx = $data['idx'];
                
            }else{
                if(isset($data['car_chassis_no'])){
                    $car_detail=$this->get_detail_by_chassis($data['car_chassis_no']);
                    $idx = $car_detail[0]->idx;
                }
            }
        }else{
            if(isset($data['car_chassis_no'])){
                $car_detail=$this->get_detail_by_chassis($data['car_chassis_no']);
                $idx = $car_detail[0]->idx;
            }
        }
        if(!empty($idx)){
            $this->mo_parse->insert_or_update($idx);
        }
        //echo $this->db->last_query();
        return $result;
        
    }

    function adm_scrape_update($data, $where){
        $where['icon_status'] = 'sale';
        $where['ignore_scan_update'] = '0';
        $this->db->where($where);
        $result=$this->db->update('iw_bbs', $data);
        /*** PARSE ***/

        if(isset($data['idx'])){
            if(!empty($data['idx'])){
                $idx = $data['idx'];
                
            }else{
                $car_detail=$this->get_detail_by_chassis($data['car_chassis_no']);
                $idx = $car_detail[0]->idx;
            }
        }else{
            $car_detail=$this->get_detail_by_chassis($data['car_chassis_no']);
            $idx = $car_detail[0]->idx;
        }
        if(!empty($idx)){
            $this->mo_parse->insert_or_update($idx);
        }
        //echo $this->db->last_query();
    }

    function adm_update_car_seller($data, $idx, $car_owner){
        $this->db->where('car_owner',$car_owner);
        $this->db->where_in('idx',$idx);
        $result=$this->db->update('iw_bbs', $data);

        return $result;
        
    }

    function users_select_customer(){
        $this->db->select('member_no, email, member_first_name, member_last_name');
        $this->db->from('iw_member');
        $this->db->where('grade_no', '11');
        $this->db->where("member_first_name != '' AND member_first_name != '' ");
        $query = $this->db->get();

        $result = $query->result();

        return $result;
        
    }

	//게시판 수정
    function update_func($table,$where,$data) {

		foreach($where as $key){

       		$this->db->where($key, $data[$key]);

		}
        return $this->db->update($table, $data);
    }

    //게시판 수정
    function update_board_counsel($data) {
        $this->db->where("board_idx", $data['board_idx']);
        $this->db->update("iw_board_counsel", $data);
    }

    //게시판 첨부파일 등록
    function insert_attach($data) {
        $data['menu_code']=$this->select_menu_code_by_idx($data['idx']);
        $this->db->insert("iw_bbs_attach", $data);
        return $this->db->insert_id();
    }
    function insert_iw_cloud_bbs_images($data) {
        $this->db->insert("iw_cloud_bbs_images", $data);
        return $this->db->insert_id();
    }
    function select_menu_code_by_idx($idx){
        $data=array();
        $data['idx'] = $idx;
        $query = $this->db->query("SELECT menu_code FROM iw_bbs WHERE idx=? LIMIT 1;", $data);
        $row = $query->first_row();
        return $row->menu_code;
    }
    //게시판 삭제
    function delete($idx, $menu_code) {
        $this->db->where("idx", $idx);
        //$this->db->where("menu_code", $menu_code);

        $grade_no_session = $_SESSION['ADMIN']['session_grade_no'];
        $member_no_session = $_SESSION['ADMIN']['member_no'];
        if ($grade_no_session == 11) {
            $this->db->where("car_owner", $member_no_session);
        }

        $this->db->delete("iw_bbs");
    }

    function delete_file_image($idx, $file_type) {
        $this->db->where("bbs_idx", $idx);
        $this->db->where("file_type", $file_type);
        $this->db->delete("iw_bbs_file");
    }

    function delete_file_images($idx) {
        $this->db->where("bbs_idx", $idx);
        $this->db->delete("iw_bbs_file");
    }

    //게시판 첨부파일 삭제
    function delete_attach($idx, $att_idx) {
        // $menu_code = $this->select_menu_code_by_idx($idx);
        $this->db->where("idx", $idx);
        $this->db->where("att_idx", $att_idx);
        //$this->db->where("menu_code", $menu_code);
        $this->db->delete("iw_bbs_attach");
    }

    //게시판 첨부파일 삭제
    function delete_attach_file($id, $bbs_idx, $file_type = '') {
        $this->db->where("bbs_idx", $bbs_idx);
        $this->db->where("id", $id);
        if ($file_type != '') {
            $this->db->where("file_type", $file_type);
        }
        return $this->db->delete("iw_bbs_file");
    }

    //게시판 삭제시 하위글 존재 여부
    function delete_child_yn($grp_idx, $menu_code, $depth) {
        $sql = "select * from iw_bbs         \n";
        $sql .= " where menu_code = ?         \n";
        $sql .= "   and grp_idx = ?           \n";
        $sql .= "   and depth > ?;            \n";

        //파라미터 생성
        $sql_param = array();
        $sql_param[0] = $menu_code;
        $sql_param[count($sql_param)] = $grp_idx;
        $sql_param[count($sql_param)] = $depth;

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

    //게시판 삭제
    function delete_board_counsel($board_idx) {
        $this->db->where("board_idx", $board_idx);
        $this->db->delete("iw_board_counsel");
    }

    //게시판 비밀번호 체크
    function password_check($idx, $menu_code, $password) {
        $sql = "select * from iw_bbs  \n";
        $sql .= " where idx = ?        \n";
        $sql .= "   and menu_code = ?  \n";
        $sql .= "   and password = ?;  \n";

        //파라미터 생성
        $sql_param = array();
        $sql_param[0] = $idx;
        $sql_param[count($sql_param)] = $menu_code;
        $sql_param[count($sql_param)] = $password;

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

    /*
      //게시판 코멘트 조회
      function select_comment($idx, $menu_code) {
      $sql = "select * from iw_bbs_comment \n";
      $sql .= " where idx = ?               \n";
      $sql .= "   and menu_code = ?         \n";

      //파라미터 생성
      $sql_param = array();
      $sql_param[0] = $idx;
      $sql_param[count($sql_param)] = $menu_code;

      $query = $this->db->query($sql, $sql_param);

      return $query;
      }
     */

    //게시판 코멘트 조회
    function select_comment($where) {
        $sql = "select * from iw_comment";

        //검색 조건 생성
        if ($where != NULL) {
            $first_loop = true;
            foreach ($where as $key => $value) {
                if ($first_loop) {
                    $sql .= " where " . $key . " ?";
                } else {
                    $sql .= " and " . $key . " ?";
                }
                $first_loop = false;
            }
        }

        $sql .= " order by comment_write_date desc";

        //파라미터 생성
        $sql_param = array();
        if ($where != NULL) {
            $first_loop = true;
            foreach ($where as $key => $value) {
                if ($value == '')
                    continue;
                if ($first_loop) {
                    $sql_param[0] = $value;
                } else {
                    $sql_param[count($sql_param)] = $value;
                }
                $first_loop = false;
            }
        }

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

    //게시판 코멘트 비밀번호 체크
    function select_password($cm_idx, $idx, $menu_code, $password) {
        $sql = "select * from iw_bbs_comment \n";
        $sql .= " where cm_idx = ?            \n";
        $sql .= "   and idx = ?               \n";
        $sql .= "   and menu_code = ?         \n";
        $sql .= "   and password = ?          \n";

        //파라미터 생성
        $sql_param = array();
        $sql_param[0] = $cm_idx;
        $sql_param[count($sql_param)] = $idx;
        $sql_param[count($sql_param)] = $menu_code;
        $sql_param[count($sql_param)] = $password;

        $query = $this->db->query($sql, $sql_param);

        return $query;
    }

	//게시판 코멘트 비밀번호 체크
    function total_select_car_order($where) {
		//파라미터 생성
		$sql_param = array();

        $sql = "select * from iw_customer_inventory \n";
        $sql .= " where order_idx != ''           \n";

        //검색 조건 생성
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $findIt = strpos($key, 'IWSEARCHVALS');

                if ($findIt === FALSE) {
                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
                        $sql .='  and  ' . $key . ' ? ';
                    } else {
                        $key = str_replace('IWSEARCHKEY', '', $key);
                        $sql .=' and ' . $key;
                    }
                }
            }
        }
		$sql_param = array();
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                if ($value == '')
                    continue;
                $sql_param[count($sql_param)] = $value;
            }
        }

        $query = $this->db->query($sql, $sql_param);

         return $query->num_rows();
    }


    //게시판 코멘트 삭제
    function delete_comment($cm_idx, $idx, $menu_code) {
        $this->db->where("cm_idx", $cm_idx);
        $this->db->where("idx", $idx);
        $this->db->where("menu_code", $menu_code);
        $this->db->delete("iw_bbs_comment");

        //코멘트 수 업데이트
        $sql = " update iw_bbs                        \n";
        $sql .= "    set comment_cnt = comment_cnt - 1 \n";
        $sql .= "  where idx = ?                       \n";
        $sql .= "    and menu_code = ?                 \n";

        $this->db->query($sql, array($idx, $menu_code));
    }

    //게시판 코멘트 삭제
    function delete_comment2($comment_idx, $board_idx) {
        $this->db->where("comment_idx", $comment_idx);
        $this->db->delete("iw_comment");

        //코멘트 수 업데이트
        $sql = "update iw_board_counsel";
        $sql .= " set comment_count = comment_count - 1";
        $sql .= " where board_idx = ?";

        $this->db->query($sql, array($board_idx));
    }

    //게시판 SMS 발송 정보 등록
    function insert_sms($data) {
        $this->db->insert("iw_sms_result", $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

	function total_select_reserved($menu_code='', $where='', $where2=''){

		 $sql_param = array();

		 $sql = '';

		 $sql .= "SELECT a.*, b.* 									\n";

		 $sql .= "FROM ( SELECT * FROM iw_bbs ) AS a 				\n";

		 $sql .= "RIGHT OUTER JOIN ( 				\n";

		 $sql .= "SELECT * FROM iw_customer_inventory 				\n";

		 $sql .= "WHERE order_idx != '' 				\n";

		 //검색 조건 생성
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $findIt = strpos($key, 'IWSEARCHVALS');

                if ($findIt === FALSE) {
                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
                        $sql .='  and  ' . $key . ' ? ';
                    } else {
                        $key = str_replace('IWSEARCHKEY', '', $key);
                        $sql .=' and ' . $key;
                    }
                }
            }
        }

		$sql .=")  													\n";

		$sql .="AS b ON b.stock_idx = a.idx						\n";


		$sql_param = array();
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                if ($value == '')
                    continue;
                $sql_param[count($sql_param)] = $value;
            }
        }

		$query = $this->db->query($sql, $sql_param);

        return $query->num_rows();

	}


	function select_reserved($menu_code, $notice_yn = 'N', $limit_st, $limit_ed, $where, $where2 = '', $member_id = ''){

		 // 설청 추출

         $bcfg = $this->config($menu_code)->result();

		 $sql_param = array();

		 $sql = '';

		 $sql .= "SELECT a.*, b.*,c.* 									\n";

		 $sql .= "FROM ( SELECT * FROM iw_bbs ) AS a 				\n";

		 $sql .= "RIGHT OUTER JOIN ( 				\n";

		 $sql .= "SELECT * FROM iw_customer_inventory 				\n";

		 $sql .= "WHERE order_idx != '' 				\n";

		 //검색 조건 생성
        if ($where != NULL) {
            foreach ($where as $key => $value) {
                $findIt = strpos($key, 'IWSEARCHVALS');

                if ($findIt === FALSE) {
                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
                        $sql .='  and  ' . $key . ' ? ';
                    } else {
                        $key = str_replace('IWSEARCHKEY', '', $key);
                        $sql .=' and ' . $key;
                    }
                }
            }
        }
		$sql .="limit ?, ? 											\n";

		$sql .=")  													\n";

		$sql .="AS b ON b.stock_idx = a.idx							\n";

		$sql .="LEFT OUTER JOIN	(									\n";

		$sql .="select * from iw_bbs_attach WHERE sort=0 group by idx						\n";

		$sql .=")													\n";

		$sql .="as c ON b.stock_idx = c.idx								\n";


		$sql_param = array();

		if ($where != NULL) {
            foreach ($where as $key => $value) {
                if ($value == '')
                    continue;
                $sql_param[count($sql_param)] = $value;
            }
        }

		if (count($bcfg) == 0) { // 다중 써머리
            $sql_param[count($sql_param)] = 0;
            $sql_param[count($sql_param)] = 1;
        } else {
            $sql_param[count($sql_param)] = $limit_st;
            $sql_param[count($sql_param)] = $limit_ed;
        }


		$query = $this->db->query($sql, $sql_param);

        return $query;

	}
    function getCountry($where=array()){

        $this->db->select('*');
        $this->db->from('iw_country_list');
        $this->db->where($where);
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }
    function getLocation($where=array()){
        //$where['YEAR(created_dt >']='2013';
        $where['country !='] = "";
        $this->db->select('DISTINCT(country) AS car_country');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        $this->db->where('YEAR(created_dt) >', 2013);

        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }
    function select_distinct_make(){
        $where['make_name !='] = '';

        $this->db->select('(SELECT COUNT(*) FROM iw_bbs WHERE iw_make.make_name = iw_bbs.car_make) as num_rows, iw_make.make_name, iw_make.icon_name as icon_url');
        $this->db->from('iw_make');
        
        $this->db->where($where);
        
        $this->db->group_by("iw_make.make_name");
        $this->db->order_by("num_rows DESC, iw_make.make_name ASC");
        $query = $this->db->get();
       
        $result = $query->result();
        return $result;
    }
    function admGetMake($where=array(), $showOnlyAvailable=false){
        $where['car_make !='] = '';
        $where['menu_code'] = 'product';
        $this->db->select('COUNT(*) as num_rows, car_make');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        if($showOnlyAvailable){
            $this->db->where("(icon_status='sale' OR icon_status='order')", NULL, FALSE);
        }
        $this->db->group_by("car_make");
        $this->db->order_by("num_rows DESC, car_make ASC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();

        return $result;
     }

     function admGetMake_table(){
        $this->db->select('*');
        $this->db->from('iw_make');
        $this->db->where('make_name !=','');
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }
    function admGetMake_table_seller($where=array()){
        $this->db->select('iw_make.id,iw_make.make_name,iw_make.icon_name,iw_make.public_id,COUNT(idx) AS count');
        $this->db->from('iw_make');
        $this->db->join('iw_bbs','iw_bbs.car_make=iw_make.make_name','left');
        $this->db->where($where);
        $this->db->where('make_name !=','');
        $this->db->group_by('make_name'); 
        $this->db->order_by("count","desc");
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    function admGetOwner_list(){
        $this->db->select('member_id,member_no,grade_no,member_first_name,company_name,business_type');
        $this->db->from('iw_member');
        $where['grade_no'] = '11';
        $where['business_type'] = 'seller';
        $this->db->where($where);
        $query = $this->db->get();

       // echo $this->db->last_query();
        $result = $query->result();

        return $result;
    }
    function admGetCountry_list(){
        $this->db->select('cc,country_name,phonecode');
        $this->db->from('iw_country_list');
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();

        return $result;
    }
    function list_country_location_item(){
        $this->db->select('DISTINCT(cc),country_name,phonecode');
        $this->db->from('iw_country_list');
        $this->db->join('iw_bbs', 'iw_bbs.country = iw_country_list.cc');
        $query = $this->db->get();
        // echo $this->db->last_query();
        $result = $query->result();

        return $result;
    }
    function list_country_customer(){
        $this->db->select('DISTINCT(cc),country_name,phonecode');
        $this->db->from('iw_country_list');
        $this->db->join('iw_member', 'iw_member.member_country = iw_country_list.cc');
        $query = $this->db->get();
        // echo $this->db->last_query();
        $result = $query->result();

        return $result;
    }
	
    function getMake($where=array(), $showOnlyAvailable=false){
        $where['car_make !='] = '';
        $where['menu_code'] = 'product';
        $where['publish'] = 'Y';
        $this->db->select('DISTINCT(car_make) AS car_make');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        if($showOnlyAvailable){
            $this->db->where("(icon_status='sale' OR icon_status='order')", NULL, FALSE);
        }
        $this->db->order_by("car_make");
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();
        return $result;
    }

    function countgetmake(){
        $sql = "select COUNT(*) AS countforgetmake, car_make FROM iw_bbs WHERE car_make != '' AND created_dt > '2013' AND menu_code = 'product' AND publish = 'Y' GROUP BY car_make";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }
	/*get for home page edit from chearado*/

	// function get_car_populer(){
 //        $sql = "
 //            select
 //            b.idx,
 //            b.car_country,
	// 		b.country,
	// 		b.car_owner,
 //            b.car_chassis_no,
 //            b.car_mileage,
 //            b.unit_price,
	// 		b.car_fob_cost,
 //            b.car_model_year,
	// 		b.car_model,
	// 		b.car_fob_currency,
 //            b.car_make,
 //            b.visit_cnt,
	// 		(select d.country_name from iw_country_list as d where d.cc = b.country LIMIT 1)as fullnamecountry,
 //            (SELECT c.file_name FROM iw_bbs_attach AS c WHERE b.idx = c.idx LIMIT 1)AS fileimgname
 //            FROM iw_bbs AS b WHERE menu_code = 'product' AND publish = 'Y' ORDER BY visit_cnt DESC LIMIT 4
 //        ";
 //        $query = $this->db->query($sql);
 //        $result = $query->result();
	// 	//var_dump($result[0]->visit_cnt); exit();
 //        return $result;
 //    }

	function get_car_home_page($limit=12){
        $sql = "select
                b.idx,
                b.icon_status,
				b.country,
				b.car_owner,
                b.car_chassis_no,
                b.car_mileage,
                b.unit_price,
				b.car_fob_cost,
                b.car_model_year,
                b.old_fob_cost,
				b.car_model,
				b.car_fob_currency,
                b.car_make,
                b.menu_code,
                b.car_stock_no,
                b.car_transmission,
                b.car_steering,
				(select d.country_name from iw_country_list as d where d.cc = b.country LIMIT 1)as fullnamecountry,
                (SELECT c.public_id FROM iw_cloud_bbs_images AS c WHERE c.bbs_idx = b.idx  AND c.sort=0 LIMIT 1) AS fileimgname,
                (SELECT c.base_url FROM iw_cloud_bbs_images AS c WHERE c.bbs_idx = b.idx  AND c.sort=0 LIMIT 1) AS base_url
                FROM iw_bbs AS b WHERE menu_code = 'product' AND publish = 'Y' ORDER BY icon_status ASC, created_dt DESC LIMIT $limit";

        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function get_available_besides_premium($where=array()){
        /*** Default settings ***/
        //$this->db->where_not_in('idx', '(SELECT bbs_idx FROM iw_bbs_premium)');
        $this->db->where('iw_bbs_premium.bbs_idx IS NULL', '', false);
        $where['iw_bbs.icon_status'] = 'sale';
        $where['iw_bbs.menu_code'] = 'product';
        $where['iw_bbs.category'] = 'offer';
        /*** End Default settings ***/

        $this->db->select('iw_bbs.idx, iw_bbs.car_chassis_no');
        $this->db->from('iw_bbs');
        $this->db->join("iw_bbs_premium", "iw_bbs_premium.bbs_idx = iw_bbs.idx", 'left');
        $this->db->where($where);
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();
        return $result;
    }

	/*End edit from chearado*/
	//SELECT * from iw_bbs WHERE   idx NOT IN (SELECT bbs_idx FROM iw_bbs_premium) and icon_status='sale'
    function get_premium_car($where=array(), $num_row=null, $offset=0){

       /*****START Default Condition*****/
        $where['iw_bbs.menu_code'] = 'product';
        $where['iw_bbs.publish'] = 'Y';
        $where['iw_bbs.category'] = 'offer';
        $where['iw_bbs.created_dt >=']= '2013-11-18';
        // $where['iw_bbs.icon_status'] = 'sale';
        /*****END Default Condition*****/
        $this->db->select('iw_bbs.*,iw_member.company_name as com_name,iw_member.member_no as mem_no,iw_country_list.country_name, iw_bbs_premium.id as premium_id, iw_bbs_premium.premium_sort, iw_cloud_bbs_images.original_filename,iw_cloud_bbs_images.public_id AS fileimgname,iw_cloud_bbs_images.base_url AS base_url');
        $this->db->from('iw_bbs_premium');
        $this->db->where($where);

        $this->db->join("iw_bbs", "iw_bbs_premium.bbs_idx = iw_bbs.idx", 'left');
        $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
        $this->db->join("iw_country_list", "iw_country_list.cc = iw_bbs.country", 'left');
        $this->db->join("iw_member", "iw_member.member_no = iw_bbs.car_owner", 'left');
        if($num_row!=NULL){
            $this->db->limit($num_row, $offset);
        }
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by('iw_bbs_premium.premium_sort DESC, iw_bbs.created_dt DESC, iw_bbs.icon_status ASC');
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();

        return $result;
    }

    function get_public_premium_car($where=array(), $num_row=null, $offset=0){

       /*****START Default Condition*****/
        $where['iw_bbs.menu_code'] = 'product';
        $where['iw_bbs.publish'] = 'Y';
        $where['iw_bbs.category'] = 'offer';
        $where['iw_bbs.created_dt >=']= '2013-11-18';
        // $where['iw_bbs.icon_status'] = 'sale';
        /*****END Default Condition*****/
        $this->db->select('iw_bbs.*,iw_member.company_name as com_name,iw_member.member_no as mem_no,iw_country_list.country_name, iw_bbs_premium.id as premium_id, iw_bbs_premium.premium_sort, iw_cloud_bbs_images.original_filename,iw_cloud_bbs_images.public_id AS fileimgname,iw_cloud_bbs_images.base_url AS base_url');
        $this->db->from('iw_bbs_premium');
        $this->db->where($where);

        $this->db->join("iw_bbs", "iw_bbs_premium.bbs_idx = iw_bbs.idx", 'left');
        $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
        $this->db->join("iw_country_list", "iw_country_list.cc = iw_bbs.country", 'left');
        $this->db->join("iw_member", "iw_member.member_no = iw_bbs.car_owner", 'left');
        if($num_row!=NULL){
            $this->db->limit($num_row, $offset);
        }
        $this->db->group_by("iw_bbs.idx");
        $this->db->order_by('iw_bbs_premium.premium_sort DESC');
        $query = $this->db->get();
       // echo $this->db->last_query();
        $result = $query->result();

        return $result;
    }
    function get_total_premium_car($where=array()){

       /*****START Default Condition*****/
        $where['iw_bbs.menu_code'] = 'product';
        $where['iw_bbs.publish'] = 'Y';
        $where['iw_bbs.category'] = 'offer';
        $where['iw_bbs.created_dt >=']= '2013-11-18';
        // $where['iw_bbs.icon_status'] = 'sale';
        /*****END Default Condition*****/
        $this->db->select('COUNT(DISTINCT(iw_bbs.idx)) as num_rows');
        $this->db->from('iw_bbs_premium');
        $this->db->where($where);

        $this->db->join("iw_bbs", "iw_bbs_premium.bbs_idx = iw_bbs.idx", 'left');
        $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
        $this->db->join("iw_country_list", "iw_country_list.cc = iw_bbs.country", 'left');
        $this->db->join("iw_member", "iw_member.member_no = iw_bbs.car_owner", 'left');

        // $this->db->group_by("iw_bbs.idx");
        $this->db->order_by('iw_bbs_premium.premium_sort DESC, iw_bbs.created_dt DESC');
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();

        return $result[0]->num_rows;
    }


    function premium_select_detail($idx){
        /*****START Default Condition*****/
        $where['iw_bbs.idx']=$idx;
        /*****END Default Condition*****/
        $this->db->select('iw_bbs.*,iw_member.company_name as com_name,iw_country_list.country_name, iw_bbs_premium.id as premium_id, iw_bbs_premium.premium_sort, iw_cloud_bbs_images.original_filename,iw_cloud_bbs_images.secure_url');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        $this->db->join("iw_bbs_premium", "iw_bbs_premium.bbs_idx = iw_bbs.idx", 'left');
        $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
        $this->db->join("iw_country_list", "iw_country_list.cc = iw_bbs.country", 'left');
        $this->db->join("iw_member", "iw_member.member_no = iw_bbs.car_owner", 'left');
        $query = $this->db->get();
        //echo $this->db->last_query();
        //exit();
        $result = $query->result();

        return $result;
    }


    function adm_premium_delete_adm($premium) {

		$this->db->where_in("bbs_idx",$premium);
		$this->db->delete("iw_bbs_premium");

	}

    function adm_supporter_delete_adm($id) {

        $this->db->where_in("id",$id);
        $this->db->delete("iw_bbs_supporter");

    }

    function select_supporter($id){
        $this->db->select('*');
        $this->db->from('iw_bbs_supporter');
        $this->db->where('id',$id);
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }

    function insert_premium_car($data){
        /*****START Default Condition*****/
        $this->db->set('premium_sort', '(SELECT COUNT(id) FROM iw_bbs_premium pb) +1', FALSE);
        $this->db->set('created_dt', 'NOW()', FALSE);
        $this->db->set('updated_dt', 'NOW()', FALSE);
        /*****END Default Condition*****/

        $result = $this->db->insert("iw_bbs_premium", $data);
        //echo $this->db->last_query();
        return $result;
    }


    function getModel($where=array(), $showOnlyAvailable=false){

        //$where['car_make'] = "KIA";
        $where['car_model !='] = '';
        $where['menu_code'] = 'product';
        $where['publish'] = 'Y';
        $this->db->select('DISTINCT(car_model)');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        if($showOnlyAvailable){
            $this->db->where("(icon_status='sale' OR icon_status='order')", NULL, FALSE);
        }
        $this->db->order_by("car_model");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

       function getModel_table($where=array()){
        $this->db->select('*,iw_make.make_name');
        $this->db->from('iw_model');
        $this->db->join('iw_make','iw_model.make_id=iw_make.id');
        $this->db->where($where);
        $this->db->where('model_name !=','');
        $this->db->order_by("model_name");
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }
    
     function select_make_all(){
        $this->db->select('*');
        $this->db->from('iw_make');
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }
    
    function getModel_sale($where_model){
          $where_model=array();
          $this->db->select('iw_model.id ,iw_model.make_id,iw_model.model_name');
    	  $this->db->from('iw_model');
          $this->db->where($where_model);
          $this->db->distinct();
	  $query = $this->db->get();
          echo $this->db->last_query();
	  return $query->result();
    }




    /*** $excluded_cars : array of car_stock_no ***/
    function getCarToAllocateDeposit($where=array(), $excluded_cars){
        $where["a.icon_status !="] = "soldout";
        $where["a.car_visible"] = "True";
        $this->db->select("a.idx, a.car_stock_no, a.car_model, a.car_chassis_no, (a.car_actual_price + a.car_freight_fee) as total_sell_price, a.car_actual_price_currency,
            b.currency_type, b.car_idx, b.allocate_sum, b.currency_type");
        $this->db->from("iw_bbs as a");
        $this->db->join("(SELECT car_idx, SUM(car_allocate) AS allocate_sum, currency_type FROM iw_payment GROUP BY car_idx,currency_type) b", "a.car_stock_no = b.car_idx", "left outer");
        $this->db->where($where);
        $this->db->where_not_in('a.car_stock_no', $excluded_cars);
        $this->db->order_by("a.car_stock_no DESC");
        $query = $this->db->get();
        return $query->result();
    }
    function getCarDetails($where=array()){

        $this->db->select('car_model,manu_year, car_model_year, car_transmission,country,car_city,car_stock_no,car_chassis_no,icon_status,car_make,car_model,car_grade,manu_year,first_registration_month,car_transmission,car_cc,car_mileage,car_fuel,car_color,car_seat,car_body_type,car_drive_type,car_fob_cost,car_fob_currency,car_buying_price,car_buying_currency');
        $this->db->from('iw_bbs');
        $this->db->where($where);
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }
    function getCarListpage($where=array(),$arrage=array(),$ayear=array(),$limit_s=0, $limit_e=10){
        /*** DEFAULT CONDITION ***/
        $sql_param = array();
        $sql_where="WHERE 1 AND draft='N' AND YEAR(iw_bbs.created_dt) >2013 ";
        $where['publish'] = 'Y';
        $sale_order='sale or order';

        if(isset($where['keyword'])){
            $sql_where .= " AND (car_chassis_no LIKE ? OR car_stock_no LIKE ?) ";
            $sql_param[] = '%'.$where['keyword'].'%';
            $sql_param[] = '%'.$where['keyword'].'%';
            unset($where['keyword']);
        }
        foreach($where as $key => $value){
            $sql_where.=" AND `{$key}` = ?";
            $sql_param[]=$value;
        }


        //min price and max price
        $whereArrange = '';
        $round = 0;
        foreach($arrage as $key => $value){
            $round++;
            if($key == 'pr_max'){
                $whereArrange.= (($round==1)?'':' AND ').' car_fob_cost <=' .$value;
            }
            if($key == 'pr_min'){
                $whereArrange.= (($round==1)?'':' AND ').' car_fob_cost >= ' .$value;
            }
        }
        $whereArrange = ($whereArrange!='')?' AND ('.$whereArrange.')':'';

        //min year and max year
        $whereYear = '';
        $round = 0;
        foreach($ayear as $key => $value){
            $round++;
            if($key == 'year_max'){
                $whereYear.= (($round==1)?'':' AND ').' manu_year <=' .$value;
            }
            if($key == 'year_min'){
                $whereYear.= (($round==1)?'':' AND ').' manu_year >= ' .$value;
            }
        }
        $whereYear = ($whereYear!='')?' AND ('.$whereYear.')':'';
        //var_dump($Ayear);

        $sql = "SELECT
                    `iw_bbs`.`idx`,
                    `icon_status`,
                    `public_id`,
                    `base_url`,
                    `sort`,
                    `car_stock_no`,
                    `car_make`,
                    `car_model`,
                    `manu_year`,
                    `country`,
                    `car_city`,
                    `car_fob_cost`,
                    `car_fob_currency`,
                    `iw_bbs`.`created_dt`
                FROM
                    (`iw_bbs`)
                LEFT JOIN `iw_cloud_bbs_images` ON `iw_cloud_bbs_images`.`bbs_idx` = `iw_bbs`.`idx` AND sort=0
                $sql_where $whereArrange $whereYear
                GROUP BY
                    `iw_bbs`.`idx`
                ORDER BY
                    `iw_bbs`.`icon_status` ASC,
                    `iw_bbs`.`created_dt` DESC";
        $sql.=" LIMIT {$limit_s}, {$limit_e}";
        $query = $this->db->query($sql, $sql_param);
        // echo $this->db->last_query();
        return $query->result();
    }
    function sortAllImages($data){

        $sql = $this->db->_update_batch('iw_bbs_attach', $data, 'att_idx');
        $result = $this->db->query($sql);
        return $result;

    }
    function sortAllImages_car($data){

        $sql = $this->db->_update_batch('iw_cloud_bbs_images', $data, 'id');
        $result = $this->db->query($sql);
        return $result;

    }
    function getCarImages($where=array()){

        $this->db->select('att_idx,idx,raw_name,file_ext');
        $this->db->from('iw_bbs_attach');

        $this->db->where($where);
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }


    function getMinMaxPrice($where=array()){

        $this->db->select('
                    idx,
                    car_stock_no,
                    car_make,
                    car_model,
                    manu_year,
                    country,
                    car_city,
                    car_fob_cost,
                    car_fob_currency,
                    car_fob_cost_start,
                    car_fob_cost_end');

        $this->db->from('iw_bbs');
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    function related_post($id, $limit=4){

        $sql= "select b.idx, b.car_owner,b.icon_status, b.car_chassis_no, b.car_mileage, b.car_transmission, b.car_fob_cost, b.car_model_year,b.car_make, b.menu_code, b.category, b.car_model, car_fob_currency, 
                (SELECT c.public_id FROM iw_cloud_bbs_images as c WHERE c.bbs_idx = b.idx  AND c.sort=0 LIMIT 1) AS fileimgname,
				(SELECT m.company_name FROM iw_member as m WHERE m.member_no = b.car_owner LIMIT 1) AS company_name,
                   (SELECT c.base_url FROM iw_cloud_bbs_images AS c WHERE c.bbs_idx = b.idx  AND c.sort=0 LIMIT 1) AS base_url,
                 (select  a.country_name From iw_country_list as a where a.cc = b.country  Limit 1) AS country,b.country as cc
                 FROM iw_bbs AS b where b.car_owner =  (select d.car_owner from iw_bbs as d where d.idx = '$id') and b.menu_code = 'product' and b.publish = 'Y' order by icon_status ASC, created_dt DESC LIMIT $limit";

		$query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }
	
	function share_facebook($id = null){

        $where['bbs_idx'] = $id;
        $where['sort'] = 0;
        $this->db->select("*");
        $this->db->from('iw_cloud_bbs_images');

        $this->db->where($where);
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }

	 function getCarSpecification($id, $where=array()){
        $calculate_price = $this->session->userdata('calculate_price');
        $port_name = $this->session->userdata('port_name');

        $where['idx'] = $id;
		//$where['menu_code'] = 'product';
//        $where['publish'] = 'Y';
        $this->db->select('*,COUNT(*) as num_rows');
        if(isset($_POST['submit_calculate_price']) || isset($calculate_price) && $calculate_price!='' || isset($port_name) && $port_name!=''){ 
            $this->db->select('`iw_shipping_charge`.*,`webscrape`.member_no');
        }
        $this->db->from('iw_bbs');
        $this->db->where($where);
        if(isset($_POST['submit_calculate_price']) || isset($calculate_price) && $calculate_price!='' || isset($port_name) && $port_name!=''){ 
        
            $this->db->join("iw_shipping_charge", "iw_shipping_charge.country_from=iw_bbs.country", "left");
            $this->db->join('webscrape','webscrape.member_no=iw_bbs.car_owner','left');
        }

        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }


    function getCarSpecification_message_detail($car_idx ,$sender_no){
        //$calculate_price = $this->session->userdata('calculate_price');
       
       //  $where['menu_code'] = 'product';
       // $where['publish'] = 'Y';
       //  //$this->db->limit($num_rows, $offset);
       //  $this->db->select('iw_member.*,iw_negotiate.read,iw_bbs_supporter.support_member_no,iw_bbs_supporter.bbs_country');
       //  $this->db->from('iw_bbs');
       //  $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
       //  $this->db->join("iw_negotiate", "iw_negotiate.car_idx=iw_bbs.idx");
       //  $this->db->join("iw_member", "iw_member.member_no=iw_negotiate.sender_no");
       //  $this->db->join("iw_bbs_supporter", "iw_bbs_supporter.bbs_idx=iw_negotiate.car_idx");
       //  $this->db->join("iw_country_list", "iw_country_list.cc = iw_member.member_country");
       //  $this->db->group_by("iw_negotiate.car_idx, iw_negotiate.sender_no");
       //  $this->db->where($where);
        $sql = "SELECT iw_member.*, iw_bbs.*, iw_negotiate.`read`, iw_member.`phone_no2`, iw_member.`member_id`, iw_country_list.`country_name`, iw_cloud_bbs_images.* FROM iw_bbs INNER JOIN iw_negotiate on iw_negotiate.car_idx=iw_bbs.idx
        LEFT JOIN iw_cloud_bbs_images ON iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0 LEFT JOIN iw_member on iw_member.member_no=iw_negotiate.sender_no LEFT JOIN iw_bbs_supporter on iw_bbs_supporter.bbs_idx=iw_negotiate.car_idx 
        LEFT JOIN iw_country_list ON iw_country_list.cc = iw_member.member_country where iw_negotiate.car_idx = '$car_idx' AND iw_negotiate.sender_no = '$sender_no' GROUP BY iw_negotiate.car_idx, iw_negotiate.sender_no";

       

        // echo $this->db->last_query();
        $query = $this->db->query($sql);
        $result = $query->result();
        
       //var_dump($result);exit();

        return $result;
    }

    function get_team_seller($car_idx){
        //$calculate_price = $this->session->userdata('calculate_price');
       
       //  $where['menu_code'] = 'product';
       // $where['publish'] = 'Y';
       //  //$this->db->limit($num_rows, $offset);
       //  $this->db->select('iw_member.*,iw_negotiate.read,iw_bbs_supporter.support_member_no,iw_bbs_supporter.bbs_country');
       //  $this->db->from('iw_bbs');
       //  $this->db->join("iw_cloud_bbs_images", "iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0", 'left');
       //  $this->db->join("iw_negotiate", "iw_negotiate.car_idx=iw_bbs.idx");
       //  $this->db->join("iw_member", "iw_member.member_no=iw_negotiate.sender_no");
       //  $this->db->join("iw_bbs_supporter", "iw_bbs_supporter.bbs_idx=iw_negotiate.car_idx");
       //  $this->db->join("iw_country_list", "iw_country_list.cc = iw_member.member_country");
       //  $this->db->group_by("iw_negotiate.car_idx, iw_negotiate.sender_no");
       //  $this->db->where($where);
        // $sql = "SELECT iw_member.*, iw_bbs.*, iw_negotiate.`read`, iw_bbs_supporter.`support_member_no`, iw_country_list.`country_name`, iw_cloud_bbs_images.* FROM iw_bbs INNER JOIN iw_negotiate on iw_negotiate.car_idx=iw_bbs.idx
        // LEFT JOIN iw_cloud_bbs_images ON iw_cloud_bbs_images.bbs_idx = iw_bbs.idx AND iw_cloud_bbs_images.sort=0 LEFT JOIN iw_member on iw_member.member_no=iw_negotiate.sender_no LEFT JOIN iw_bbs_supporter on iw_bbs_supporter.bbs_idx=iw_negotiate.car_idx 
        // LEFT JOIN iw_country_list ON iw_country_list.cc = iw_member.member_country where iw_negotiate.car_idx = '$car_idx' AND iw_negotiate.sender_no = '$sender_no' GROUP BY iw_negotiate.car_idx, iw_negotiate.sender_no";
        

        $sql ="SELECT iw_member.member_first_name ,iw_member.contact_person_name,iw_member.member_id,iw_member.member_no, iw_country_list.country_name as country from iw_member LEFT JOIN iw_bbs_supporter ON iw_bbs_supporter.support_member_no = iw_member.member_no LEFT JOIN iw_country_list ON iw_country_list.`cc` = iw_member.member_country WHERE iw_bbs_supporter.bbs_idx = $car_idx ";
        // echo $this->db->last_query();
        $query = $this->db->query($sql);
        $result = $query->result();
        
       //svar_dump($result);exit();

        return $result;
    }


    function getCarSpecification_message_detail_count(){
        //$calculate_price = $this->session->userdata('calculate_price');
        // $where['idx'] = $car_idx;
        // $where['car_owner'] = $car_owner;
        //$where['menu_code'] = 'product';
//        $where['publish'] = 'Y';

        
        
        //$this->db->limit($num_rows, $offset);
        $sql ='select count(negotiate.read) as count_read, negotiate.car_idx,negotiate.sender_no from iw_negotiate as negotiate where negotiate.read = "unseen"';
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;

       

        // echo $this->db->last_query();
        //$query = $this->db->get();
        
    }


    function get_country_list(){
        $this->db->select('*');
        $this->db->from('iw_country_list');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function getCarDetailImage($id) {
		$where['bbs_idx'] = $id;
        $this->db->select("*");
        $this->db->from('iw_cloud_bbs_images');

        $this->db->where($where);
        $this->db->order_by('`sort`','ASC');
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }
    function getCarDetailPrimaryImage($id) {
        $where['bbs_idx'] = $id;
        $where['sort'] = 0;
        $this->db->select("*,COUNT(*) as num_rows");
        $this->db->from('iw_cloud_bbs_images');

        $this->db->where($where);
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }

	function list_car_seller($owner){

        $sql= "select b.idx, b.car_owner, b.car_chassis_no, b.car_mileage, b.car_fob_cost, b.car_model_year,b.car_make, b.menu_code, b.category, b.car_model, car_fob_currency, (SELECT c.file_name FROM iw_bbs_attach AS c WHERE c.idx = b.idx AND c.sort = 0 LIMIT 1)AS fileimgname
         FROM iw_bbs AS b where b.car_owner = '$owner' and b.menu_code = 'product' and b.publish = 'Y' order by b.idx DESC";

		$query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

	function getSellerSpecification($id){
        $sql = "SELECT m.*, (SELECT c.country_name FROM iw_country_list AS c WHERE c.cc = m.member_country LIMIT 1)AS country from iw_member AS m where member_no = '$id'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function getOrders($limit_s, $limit_e, $where){
        $this->db->select('*');
        $this->db->from('iw_customer_inventory');
        $user_session = $this->phpsession->get('member_no', 'USER');
        $this->db->join('iw_bbs ',"iw_bbs.idx = iw_customer_inventory.stock_idx AND iw_customer_inventory.customer_no='{$user_session}'");
        $this->db->order_by("order_idx", "desc");
        $this->db->where("cancel_dt IS NULL");
        $this->db->where($where);
        $this->db->limit($limit_e, $limit_s);
        $query = $this->db->get();

        $result = $query->result();

        return $result;

    }
    //count orders car for pagination
    function count_orders($where){
        $this->db->select('COUNT(*) as num_row');
        $this->db->from('iw_customer_inventory');
        $user_session = $this->phpsession->get('member_no', 'USER');
        $this->db->join('iw_bbs ',"iw_bbs.idx = iw_customer_inventory.stock_idx AND iw_customer_inventory.customer_no='{$user_session}'");
        $this->db->order_by("order_idx", "desc");
        $this->db->where("cancel_dt IS NULL");
        $this->db->where($where);
        $query = $this->db->get();

        $result = $query->result();

        return $result[0]->num_row;
    }
    //get car order has limit show for simple page
    function getOrder($where=array()){

        $this->db->select('*');
        $this->db->from('iw_customer_inventory');
        $user_session = $this->phpsession->get('member_no', 'USER');
        $this->db->join('iw_bbs ',"iw_bbs.idx = iw_customer_inventory.stock_idx AND iw_customer_inventory.customer_no='{$user_session}'");
        $this->db->order_by("order_idx", "desc");
        $this->db->where("cancel_dt IS NULL");
        $this->db->where($where);
        $this->db->limit('5');
        $query = $this->db->get();

        $result = $query->result();

        return $result;

    }

    //count reserved car list for pagination
    function count_reserved($where){
        $user_session = $this->phpsession->get('member_no', 'USER');
        $where['customer_no'] = $user_session;
        $this->db->select('COUNT(*) as num_row');
        $this->db->from('iw_bbs');
        $this->db->order_by("idx", "desc");
        $this->db->where($where);
        $query = $this->db->get();

        $result = $query->result();

        return $result[0]->num_row;
    }
    // get car has reserved for pagination
    function getReserved($limit_s, $limit_e, $where){
        $user_session = $this->phpsession->get('member_no', 'USER');
        $where['customer_no'] = $user_session;
        $this->db->select('*');
        $this->db->from('iw_bbs');
        $this->db->order_by("idx", "desc");
        $this->db->where($where);
        $this->db->limit($limit_e, $limit_s);
        $query = $this->db->get();

        $result = $query->result();

        return $result;

    }

    //get a image car for mobile
    function getImage($where=array()) {

        $this->db->select('att_idx,idx,raw_name,file_ext');
        $this->db->from('iw_bbs_attach');

        $this->db->where($where);
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }
    //delete car ordered when car been reserved or want to cnacel
    function deleteCarOrderedDate($where){

        $read_dt = func_get_date_time();
        $data['cancel_dt'] = $read_dt;
        $this->db->where($where);
        $result = $this->db->update('iw_customer_inventory', $data);
        return $result;
    }

	function mytestdata(){
		 $this->db->select('att_idx,idx,raw_name,file_ext');
        $this->db->from('iw_bbs_attach');
        $query = $this->db->get();

        $result = $query->result();

        return $result;



		}



	function insertimagejson($data){
		$this->db->insert("iw_cloud_bbs_images",$data);

        //echo $this->db->last_query();
        //return $insert_id;
		}

/*	 function deleteCarOrderedDate($where){

        $read_dt = func_get_date_time();
        $data['cancel_dt'] = $read_dt;
        $this->db->where($where);
        $result = $this->db->update('iw_customer_inventory', $data);
        return $result;
    }*/

	function myupdateimagejson($pid,$where,$data){
		$this->db->where("idx",$pid);
		$this->db->update("iw_bbs_attach",array('sort'=>0));

		$this->db->where("file_name",$where);
		$result = $this->db->update("iw_bbs_attach",$data);
		}

	function mydeleteimagejson($image){
		$this->db->where("file_name",$image);
		$this->db->delete("iw_bbs_attach");
		return true;

		}

    function reserve_cancel_exec($data,$menu_code,$idxs) {
        $idx = explode(" ", $idxs);
        $this->db->where("menu_code",$menu_code);
        $this->db->where_in("idx",  $idx);
        return $this->db->update("iw_bbs", $data);
    }

    function update_car_shipment_exec($data,$menu_code,$idxs) {
        $idx = explode(" ", $idxs);
        $this->db->where("menu_code",$menu_code);
        $this->db->where_in("idx",  $idx);
        return $this->db->update("iw_bbs", $data);
    }

    function insert_files($data) {
        $member_no_session = $_SESSION['ADMIN']['member_no'];
        $data['file_owner'] = $member_no_session;
        $data['created_dt']=func_get_date_time();
        $this->db->insert("iw_files", $data);
        $this->db->insert_id();

    }

    function get_idx($car_stock_no) {
        $this->db->select('idx');
        $this->db->from('iw_bbs');
        $this->db->where_in('car_stock_no', $car_stock_no);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->idx;
    }


    function select_customer_service($where=array(), $offset=0, $num_rows=NULL){
        //$where['iw_member.chat_support'] = 'Y';

        $this->db->select("iw_member.*, CONCAT_WS('', iw_member.member_first_name, ' ', iw_member.member_last_name) as member_name, iw_member.sex, cl.base_url, cl.public_id, cl.secure_url", false);
        $this->db->from('iw_bbs');
        $this->db->join('iw_bbs_supporter as bbs_support', 'bbs_support.bbs_country = iw_bbs.country OR bbs_support.bbs_idx = iw_bbs.idx
', 'inner');
        $this->db->join('iw_member', 'bbs_support.support_member_no=iw_member.member_no', 'inner');
        $this->db->join('iw_cloud_files cl', 'cl.file_owner=iw_member.member_no', 'left');
        $this->db->where($where);

        $this->db->order_by('bbs_support.bbs_idx DESC');
        if($num_rows!=NULL){
            $this->db->limit($num_rows, $offset);
        }
        $query = $this->db->get();
        // echo $this->db->last_query(); die();
        $result = $query->result();
        return $result;
    }

    function select_destination(){
        $this->db->select('iw_port.*,iw_country_list.country_name');
        $this->db->from('iw_port');
        $this->db->join('iw_country_list', 'iw_country_list.cc=iw_port.country_iso','left');
        $this->db->group_by('country_iso');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function select_port_name(){
        $this->db->select('iw_port.*,iw_country_list.country_name');
        $this->db->from('iw_port');
        $this->db->join('iw_country_list', 'iw_country_list.cc=iw_port.country_iso','left');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function select_detail_for_seller($idx) {
        $this->db->select('*');
        $this->db->from('iw_bbs');
        $this->db->where('idx', $idx);
        $query = $this->db->get();
        $result = $query->result();
        // echo $this->db->last_query();
        return $result;
    }
    public function insert_car_email_batch($data){

        $sql_param = array();

        $sql_value = '';
        $sql_key = '';


        $sql_value_arr = array();
        $sql_key_arr = array_keys($data[0]);

        foreach($data as $key=>$value){
            $sql_value_sign = array();
            foreach($value as $k=>$v){
                $sql_value_sign[] = '?';
                $sql_param[] = $v;
            }
            $sql_value_arr[] = "(".implode(', ', $sql_value_sign).")";
        }
        $sql_value = implode(', ', $sql_value_arr);
        $sql_key = '(`'.implode('`, `', $sql_key_arr).'`)';
        $sql = "INSERT INTO iw_car_email_compaign $sql_key VALUES $sql_value";

        $result = $this->db->query($sql, $sql_param);
        // echo $this->db->last_query(); exit();
        return $result;
    }

    /********************* FUNCTION FOR GCM**********************/
    function getRegisterID()
    {
        $sql='SELECT gcm_regid as regID FROM gcm_users';
        $result=$this->selectData($sql);
        $ids=array();
        for($i=0;$i<count($result);$i++)
        {
            $ids[]=$result[$i]->regID;
        }
        
        return $ids;
    }
    function getCarImage($car_id)
    {
        $sql='SELECT bbs_idx as car_id,CONCAT(iw_cloud_bbs_images.base_url,iw_cloud_bbs_images.public_id,".webp") As image,sort FROM iw_cloud_bbs_images WHERE bbs_idx="'.$car_id.'"';
        $result=$this->selectData($sql);
        return $result;
    }

    /********************* JSON FOR SQLite **********************/
    function selectData($sql)
    {        
        $query = $this->db->query($sql);
        $result=$query->result();
        return $result;
    }

    function jsonBodyType()
    {
        $sql='SELECT body_title as name,body_name as title,CONCAT("https://res.cloudinary.com/softbloom/image/upload/v1435028219/logo/bt/" ,iw_body_type.icon) As image FROM iw_body_type';
        $result=$this->selectData($sql);
        $jsondata= json_encode($result);
        $result = str_replace('\\/', '/', $jsondata);
        $result = str_replace('.png', '.webp', $result);
        $result = str_replace('upload/', 'upload/c_scale,h_64/', $result);
        return $result;
    }

    function jsonBrand()
    {
        $sql='SELECT make_name as name,icon_name As image FROM iw_make';
        $result=$this->selectData($sql);
        $jsondata= json_encode($result);
        $result = str_replace('\\/', '/', $jsondata);
        $result = str_replace('.png', '.webp', $result);
        $result = str_replace('upload/', 'upload/c_scale,w_150/', $result);
        return $result;
    }

    /*function jsonModel()
    {
        $sql='SELECT model_name as name,make_id FROM iw_model';
        $result=$this->selectData($sql);
        $jsondata= json_encode($result);
        echo $jsondata;
    }

    function jsonCountry()
    {
        $sql='SELECT country_name as name,cc FROM iw_country_list';
        $result=$this->selectData($sql);
        $jsondata= json_encode($result);
        echo $jsondata;
    }*/

    function jsonCarImage()
    {
        $sql='SELECT bbs_idx as car_id,CONCAT(iw_cloud_bbs_images.base_url,iw_cloud_bbs_images.public_id,".webp") As image,sort FROM iw_cloud_bbs_images';
        $result=$this->selectData($sql);
        $jsondata= json_encode($result);
        $result = str_replace('\\/', '/', $jsondata);
        return $result;
    }

    function jsonContact()
    {
        $sql='SELECT member_no as contact_id,member_first_name as name,member_id as chat_id,CONCAT(base_url,public_id) as image,member_country as country,CONCAT(phone_no1,phone_no2) as tel,email,skype_id,whatsapp_id,viber_id,facebook_id FROM iw_member,iw_cloud_files WHERE iw_member.member_no=iw_cloud_files.file_owner AND grade_no IN (2,3)';        
        $result=$this->selectData($sql);
        $jsondata= json_encode($result);
        $result = str_replace('\\/', '/', $jsondata);
        return $result;
    }

    function jsonContactTab()
    {
        $member=$_GET['member'];
        $sql='SELECT member_no as contact_id,member_first_name as name,member_id as chat_id,CONCAT(base_url,public_id) as image,member_country as country,CONCAT(phone_no1,phone_no2) as tel,email,skype_id,whatsapp_id,viber_id,facebook_id FROM iw_member,iw_cloud_files WHERE iw_member.member_no=iw_cloud_files.file_owner AND grade_no IN (2,3) AND member_first_name="'.$member.'"';
        $result=$this->selectData($sql);
        $jsondata= json_encode($result);
        $result = str_replace('\\/', '/', $jsondata);
        return $result;
    }

    function jsonSeller()
    {
        $sql='SELECT member_no as seller_id,contact_person_name as name, company_name as company, CONCAT(base_url,public_id) as image, country_name as country,address FROM iw_member,iw_cloud_files,iw_country_list WHERE iw_country_list.cc=iw_member.member_country AND iw_member.member_no=iw_cloud_files.file_owner AND grade_no=11';
        $result=$this->selectData($sql);
        $jsondata= json_encode($result);
        $result = str_replace('\\/', '/', $jsondata);
        return $result;
    }

    function jsonCar()
    {
        $sql='SELECT
         bbs.idx as car_id,
         bbs.car_stock_no as stock_id,
         bbs.icon_new_yn as `condition`,
         bbs.car_body_type as type,
         bbs.car_make as make,
         bbs.car_model as model,
         bbs.car_chassis_no as chassis_no,
         bbs.car_model_year as model_year,
         bbs.car_mileage as mileage,
         bbs.car_steering as steering,
         bbs.car_transmission as transmission,
         bbs.car_cc as engine_size, 
         bbs.car_fuel as fuel_type,
         bbs.country,
         bbs.car_color as exterior_color,
         bbs.manu_year as manufactured_year,
         CONCAT(bbs.first_registration_year,bbs.first_registration_month) as registration_date,
         bbs.car_fob_cost as fob,
         bbs.contents as comment,
         bbs.car_owner as seller_id,
         bbs.icon_status as status,
         bbs.created_dt as created 
        FROM
         `iw_bbs` bbs        
        WHERE
         bbs.draft = "N"';
        $result=$this->selectData($sql);
        
        $jsondata= json_encode($result);
        $result = str_replace('\\/', '/', $jsondata);
        return $result;
    }

    function jsonPremium()
    {
        $sql='SELECT bbs_idx as car_id FROM iw_bbs_premium';
        $result=$this->selectData($sql);
        $jsondata= json_encode($result);
        $result = str_replace('\\/', '/', $jsondata);
        return $result;
    }

    function androidLogIn($username,$password)
    {
        $sql='SELECT member_first_name as name FROM iw_member WHERE member_id="'.$username.'" AND member_pwd="'.$password.'"';
        $result=$this->selectData($sql);
        if(count($result)>0)
        {
            $response=array('status'=>true,'message'=>'Success','name'=>$result[0]->name);
            echo json_encode($response);
            return; 
        }
        $response=array('status'=>false,'message'=>'Failed');
        echo json_encode($response);
    }

    function sqliteCarImage($public_id)
    {
        $sql='SELECT CONCAT(iw_cloud_bbs_images.base_url,iw_cloud_bbs_images.public_id,".webp") As image FROM iw_cloud_bbs_images WHERE iw_cloud_bbs_images.public_id="'.$public_id.'"';
        $result=$this->selectData($sql);        
        return $result;
    }

    function sqliteSellerImage($image_id)
    {
        $sql='SELECT CONCAT(iw_cloud_files.base_url,iw_cloud_files.public_id,".webp") As image FROM iw_cloud_files WHERE iw_cloud_files.id='.$image_id;
        $result=$this->selectData($sql);        
        return $result[0]->image;
    }

    function sqliteCarImageByID($image_id)
    {
        $sql='SELECT CONCAT(iw_cloud_bbs_images.base_url,iw_cloud_bbs_images.public_id,".webp") As image FROM iw_cloud_bbs_images WHERE iw_cloud_bbs_images.id='.$image_id;
        $result=$this->selectData($sql);        
        return $result[0]->image;
    }

    function sqliteReSortImage($primary_id,$car_id)
    {
        $where=' WHERE bbs_idx='.$car_id;
        if($primary_id!=null)
            $where.=' AND iw_cloud_bbs_images.id<>'.$primary_id;
        $sql='SELECT CONCAT(iw_cloud_bbs_images.base_url,iw_cloud_bbs_images.public_id,".webp") AS image FROM iw_cloud_bbs_images '.$where.' ORDER BY sort';
        // echo $sql;
        $result=$this->selectData($sql);        
        return $result;
    }

    function sqliteGetMake($id)
    {
        $sql='SELECT make_name as name FROM iw_make WHERE id='.$id;
        $result=$this->selectData($sql);
        // echo $sql;        
        return $result[0]->name;
    }
    
    function getStockID($id)
    {
        $sql='SELECT car_stock_no as stock_id FROM iw_bbs WHERE idx='.$id;
        $result=$this->selectData($sql);
        return $result;
    }


    /******************** FUNCTION FOR SQLite ********************/    
    function insertSQLData($table,$datas)
    {
        $dbname=dirname(dirname(__FILE__)).'/sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);        
        //print_r($datas);
        foreach ($datas as $data) {
            $columns='(';
            $values='(';        
            foreach ($data as $key => $value) {
                $columns.='`'.$key.'`,';
                $values.='"'.$value.'",';
            }
            $query='INSERT INTO `'.$table.'` '.rtrim($columns, ',').') VALUES'.rtrim($values, ',').');';
            // echo $query.'</br>';
            $handler->query($query);
        }
    }

    function insertPremiumSQLite($car_id)
    {
        $dbname=dirname(dirname(__FILE__)).'/sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);        
        // print_r($datas[0]['car_id']);
        $query='UPDATE `cars` SET `premium`=1 WHERE `car_id`='.$car_id;
        // echo $query.'</br>';
        $handler->query($query);
    }

    function removePremiumSQLite($car_id)
    {
        $dbname=dirname(dirname(__FILE__)).'/sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);        
        // print_r($datas[0]['car_id']);
        $query='UPDATE `cars` SET `premium`=0 WHERE `car_id`='.$car_id;
        // echo $query.'</br>';
        $handler->query($query);
    }

    function updateSQLData($table,$datas)
    {
        $dbname=dirname(dirname(__FILE__)).'/sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);        
        // print_r($datas[0]['car_id']);
        foreach ($datas as $data) {  
            $key_value='';      
            foreach ($data as $key => $value) {
                $key_value.='`'.$key.'`="'.$value.'",';
            }
            $where='';
            if(isset($datas[0]['car_id']))
                $where=' WHERE `car_id`='.$datas[0]['car_id'];
            else
                $where=' WHERE `chassis_no`="'.$datas[0]['chassis_no'].'"';
            $query='UPDATE `'.$table.'` SET '.rtrim($key_value, ',').$where;
            // echo $query.'</br>';
            $handler->query($query);
        }
    }

    function updateSellerSQLData($table,$datas)
    {
        $dbname=dirname(dirname(__FILE__)).'/sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);        
        // print_r($datas[0]['car_id']);
        foreach ($datas as $data) {  
            $key_value='';      
            foreach ($data as $key => $value) {
                $key_value.='`'.$key.'`="'.$value.'",';
            }            
            $query='UPDATE `'.$table.'` SET '.rtrim($key_value, ',').' WHERE `seller_id`='.$datas[0]['seller_id'];
            // echo $query.'</br>';
            $handler->query($query);
        }
    }

    function updateImagePrimay($image,$car_id)
    {
        $dbname=dirname(dirname(__FILE__)).'/sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);        
        // print_r($datas[0]['car_id']);
        $query='UPDATE `car_images` SET [primary]=0 WHERE `car_id`='.$car_id;
        // echo $query.'</br>';        
        $handler->query($query);

        $query='UPDATE `car_images` SET [primary]=1,[sort]=0 WHERE `image`="'.$image.'"';
        // echo $query.'</br>';
        $handler->query($query);
    }

    function updateImageSort($image,$sort)
    {
        $dbname=dirname(dirname(__FILE__)).'/sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);        
        // print_r($datas[0]['car_id']);
        $primary='';
        if($sort==0)
            $primary=' , [primary]=1';
        else
            $primary=' , [primary]=0';            
        $query='UPDATE `car_images` SET [sort]='.$sort.$primary.' WHERE `image`="'.$image.'"';
        // echo $query.'</br>';        
        $handler->query($query);
    }
    function updateMake($oldname,$name,$image)
    {
        $dbname=dirname(dirname(__FILE__)).'/sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);        
        // print_r($datas[0]['car_id']);
        if($image!=null)
            $image=' ,image="'.$image.'"';
        $query='UPDATE `brands` SET name="'.$name.'"'.$image.' WHERE `name`="'.$oldname.'"';
        // echo $query.'</br>';        
        $handler->query($query);
    }

    function deleteSQLData($table,$datas)
    {
        $dbname=dirname(dirname(__FILE__)).'/sqlite/motorbb.sqlite';        
        $handler=new SQLite3($dbname, 0666);        
        // print_r($datas);
        foreach ($datas as $data) {  
            $key_value='';      
            foreach ($data as $key => $value) {
                $key_value.=''.$key.'="'.$value.'",';
            }
            $query='DELETE FROM `'.$table.'` WHERE '.rtrim($key_value, ',');
            // echo $query.'</br>';
            $handler->query($query);
            // echo 'Successfully';
        }
    }

    function get_sale_support_car_list($bbs_country){
        $this->db->select('iw_member.member_first_name,iw_member.email,iw_member.phone_no1,iw_member.phone_no2,iw_member.sex,iw_member.facebook_id,iw_member.whatsapp_id,iw_member.skype_id,iw_cloud_files.base_url, iw_cloud_files.public_id,iw_cloud_files.secure_url');
        $this->db->from('iw_member');
        $this->db->join('iw_bbs_supporter', 'iw_bbs_supporter.support_member_no=iw_member.member_no');
        $this->db->join('iw_cloud_files', 'iw_cloud_files.file_owner=iw_member.member_no', 'left');
        if (!empty($bbs_country)) {
            $this->db->where('iw_bbs_supporter.bbs_country',$bbs_country);
        }else{
            $this->db->where('iw_bbs_supporter.bbs_country is null and iw_bbs_supporter.bbs_country is null');
        }
        $this->db->limit('1');
        // echo $this->db->_compile_select(); 
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function get_sale_support_default(){
        $this->db->select("iw_member.*, CONCAT_WS('', iw_member.member_first_name, ' ', iw_member.member_last_name) as member_name, iw_member.sex, iw_cloud_files.base_url, iw_cloud_files.public_id, iw_cloud_files.secure_url", false);
        $this->db->from('iw_member');
        $this->db->join('iw_bbs_supporter', 'iw_bbs_supporter.support_member_no=iw_member.member_no');
        $this->db->join('iw_cloud_files', 'iw_cloud_files.file_owner=iw_member.member_no', 'left');
        $this->db->where('iw_bbs_supporter.bbs_country is null and iw_bbs_supporter.bbs_country is null');
        $this->db->limit('1');
        // echo $this->db->_compile_select(); 
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_chassis_by_idx($idx){

        $this->db->select('car_chassis_no, car_owner');
        $this->db->from('iw_bbs');
        $this->db->where_in('idx', $idx);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function select_log($limit_st, $limit_ed, $where){

        $sql_param = array();
        $sql = '';

        $sql .= "       select log.*,m.member_id                                                                   \n";
        
        $sql .= "       from (iw_activity_log log LEFT OUTER JOIN iw_member m on log.author = m.member_no)         \n";
       
        $sql .= "        where deleted_dt is null                                                                  \n";

        if ($where != NULL) {
            
            foreach ($where as $key => $value) {
                $findIt = strpos($key, 'IWSEARCHVALS');

                if ($findIt === FALSE) {
                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
                        $sql .='  and  ' . $key . ' '.'"'. $value.'"';
                    } else {
                        $key = str_replace('IWSEARCHKEY', '', $key);
                        $sql .=' and ' . $key;
                    }
                }
            }
        }

        $sql .= "           order by log.created_dt desc                              \n";
        
        $sql .= "           limit ?, ?                                                \n";
        
        $sql_param[count($sql_param)] = $limit_st;
        $sql_param[count($sql_param)] = $limit_ed;  

        $query = $this->db->query($sql, $sql_param);

        return $query->result();
    }

    function select_all_log($where){

        $sql = '';

        $sql .= "       select log.*,m.member_id                                                                   \n";
        
        $sql .= "       from (iw_activity_log log LEFT OUTER JOIN iw_member m on log.author = m.member_no)         \n";
       
        $sql .= "        where deleted_dt is null                                                                  \n";

        if ($where != NULL) {

            foreach ($where as $key => $value) {
                $findIt = strpos($key, 'IWSEARCHVALS');

                if ($findIt === FALSE) {
                    if (strpos($key, 'IWSEARCHKEY') === FALSE) {
                        $sql .='  and  ' . $key . ' '.'"'. $value.'"';
                    } else {
                        $key = str_replace('IWSEARCHKEY', '', $key);
                        $sql .=' and ' . $key;
                    }
                }
            }
        }

        $query = $this->db->query($sql);

        return $query->result();
    }

    function select_seller_web(){
        $this->db->select('webscrape.*,iw_member.member_id');
        $this->db->from('webscrape');
        $this->db->join('iw_member', 'iw_member.member_no=webscrape.member_no', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    function select_seller_web_by_id($member_id){
        $this->db->select('*');
        $this->db->from('webscrape');
        $this->db->where('member_no',$member_id);
        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }

    function update_seller_web($website_name,$data){

        $this->db->where('website_name',$website_name);
        return $this->db->update('webscrape',$data);
    }

    function validate_modify_seller_web($member_no){
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('webscrape');
        $this->db->where('member_no', $member_no);
        $query = $this->db->get();
        return $query->result();
    }

    function get_carOwner(){
        $this->db->select('DISTINCT(iw_bbs.car_owner),m.member_id,m.member_no');
        $this->db->from('iw_bbs');
        $this->db->join('iw_member as m','m.member_no=iw_bbs.car_owner','left');
        $this->db->where('m.member_id is not null');
        $this->db->order_by('m.member_id');
        $query = $this->db->get();
        return $query->result();
    }
}

?>
