<?php
	
class Mo_message_detail_negotiate extends Model 
{
	public function __construct()
	{
		/*
		/	The constructor
		*/
		

		parent::Model();
		$this->load->database('default');

		$this->load->library('session');
	}

	function get_comment($where = array(),$startpage = null,$per_page = null){
		
			//$comments = array();

			/*$sql = "select * FROM iw_negotiate ORDER BY message_id DESC LIMIT 5";
			$this->db->join("iw_negotate.buyer_no", "iw_negotate.buyer_no=iw_bbs.car_owner", "left");
			$query = $this->db->query($sql);
			
			return $query->result();*/
			//$cid = "";

			//$where['car_idx'] = $cid ;
			$this->db->select('*,iw_member.email,iw_member.member_first_name,iw_member.member_id,iw_member.business_type,iw_member.member_first_name, iw_country_list.country_name, iw_negotiate.read');
	        $this->db->join("iw_member", "iw_member.member_no=iw_negotiate.sender_no", "left");
	        $this->db->join("iw_country_list", "iw_country_list.cc=iw_member.member_country", "left");	        
	        $this->db->order_by('message_update_date','DESC');
	        //$this->db->group_by('iw_negotiate.car_idx, iw_negotiate.sender_no');
	        $this->db->from('iw_negotiate');
	        $this->db->where($where);
	       	$this->db->limit($per_page,$startpage);
	        // echo $this->db->last_query();
	        $query = $this->db->get();

	        $result = $query->result();
	        return $result;

	}


	function count_comment($where = array()){
		
			//$comments = array();

			/*$sql = "select * FROM iw_negotiate ORDER BY message_id DESC LIMIT 5";
			$this->db->join("iw_negotate.buyer_no", "iw_negotate.buyer_no=iw_bbs.car_owner", "left");
			$query = $this->db->query($sql);
			
			return $query->result();*/
			//$cid = "";

			//$where['car_idx'] = $cid ;
			$this->db->select('COUNT(*)as commentnum,iw_member.email,iw_member.member_id,iw_member.business_type,iw_member.member_first_name, iw_country_list.country_name');
	        $this->db->join("iw_member", "iw_member.member_no=iw_negotiate.sender_no", "left");
	        $this->db->join("iw_country_list", "iw_country_list.cc=iw_member.member_country", "left");
	        $this->db->from('iw_negotiate');
	        $this->db->order_by('message_update_date','DESC');
	        $this->db->where($where);
	        // echo $this->db->last_query();
	        $query = $this->db->get();

	        $result = $query->result();
	        $result = $result[0]->commentnum;
	        return $result;

	}


	function set_comment($data){

	/*	$dtz = new DateTimeZone("Asia/Bangkok"); //Your timezone
		$now = new DateTime(date("Y-m-d"), $dtz);
		$timestamp = $now->format("Y-m-d H:i:s");

		var_dump($timestamp); exit;*/
		date_default_timezone_set("Asia/Bangkok");
		$timestamp = date("Y-m-d H:i:s");
		
		$data['message_update_date'] = $timestamp; 
		$this->db->insert("iw_negotiate", $data);
		
        $insert_id = $this->db->insert_id();


	}

	
}
