<?php

/* ------------------------------------------------
 * 파일명 : mo_common.php
 * 개  요 : 사이트 공통 model
  ------------------------------------------------ */
?>
<?php

class Mo_common extends Model {

	function __construct() {
		parent::Model();

		$this->load->database('default');
	}

//아이디 중복 체크
	function select_chk_member_id($member_id) {
		$sql = 'select * from iw_member where member_id = ?';
		$query = $this->db->query($sql, array($member_id));

		return $query->num_rows();
	}

//우편번호 조회
	function select_zipcode($dong) {
		$IWCOMMONDB = $this->load->database('iwcommon', TRUE);

		$sql = "select *                                            \r\n";
		$sql .= "      ,concat_ws(' ', sido, gugun, dong) as address \r\n";
		$sql .= "  from iwc_zipcode                                  \r\n";
		$sql .= " where dong like ?                                  \r\n";

		$query = $IWCOMMONDB->query($sql, array('%' . $dong . '%'));

		return $query->result();
	}

//팝업 조회 목록수
	function total_select_popup($where) {
		$sql = '';
		$sql .= "select * from iw_popup   \n";
		$sql .= " where idx is not null   \n";
//검색 조건 생성
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						$sql .='  and  ' . $key . ' ? ';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key;
					}
				}
			}
		}
		$sql .= "      order by created_dt desc \n";

//파라미터 생성
		$sql_param = array();

		if ($where != NULL) {
			foreach ($where as $key => $value) {
				if ($value == '')
					continue;
				$sql_param[count($sql_param)] = $value;
			}
		}

		$query = $this->db->query($sql, $sql_param);

		return $query->num_rows();
	}

//팝업 조회
	function select_popup($limit_st, $limit_ed, $where) {
		$sql = '';
		$sql .= "select * from iw_popup   \n";
		$sql .= " where idx is not null   \n";
//검색 조건 생성
		if ($where != NULL) {
			foreach ($where as $key => $value) {
				$findIt = strpos($key, 'IWSEARCHVALS');

				if ($findIt === FALSE) {
					if (strpos($key, 'IWSEARCHKEY') === FALSE) {
						$sql .='  and  ' . $key . ' ? ';
					} else {
						$key = str_replace('IWSEARCHKEY', '', $key);
						$sql .=' and ' . $key;
					}
				}
			}
		}
		$sql .= "      order by created_dt desc \n";
		$sql .= "         limit ?, ?            \n";

//파라미터 생성
		$sql_param = array();

		if ($where != NULL) {
			foreach ($where as $key => $value) {
				if ($value == '')
					continue;
				$sql_param[count($sql_param)] = $value;
			}
		}
		$sql_param[count($sql_param)] = $limit_st;
		$sql_param[count($sql_param)] = $limit_ed;

		$query = $this->db->query($sql, $sql_param);

		return $query;
	}
	


	//end select visitor

//팝업 상세 조회
	function select_detail_popup($idx) {
		$sql = '';
		$sql .= "select * from iw_popup   \n";
		$sql .= " where idx = ?           \n";

		$query = $this->db->query($sql, array($idx));

		return $query;
	}
        
        function select_premium_list(){
            
           
            $sch_seach=  $this->input->get_post('sch_seach');
              $where = array();
            if ($sch_seach != '') {
          
                $where['car_chassis_no'] =$sch_seach;
            
          }
          
             $this->db->select('*');
                $this->db->from('iw_bbs');
                $where['icon_status']='sale';
                $this->db->where($where);
                $this->db->where('idx NOT IN (SELECT bbs_idx FROM iw_bbs_premium)', NULL, FALSE);
                $query = $this->db->get();
                $result = $query->result();
                return $result;
        }
        
        

//팝업 등록
	function insert_popup($data) {
		$this->db->insert("iw_popup", $data);
		$insert_id = $this->db->insert_id();

		return $insert_id;
	}

//팝업 수정
	function update_popup($data) {
		$this->db->where("idx", $data['idx']);
		$this->db->update("iw_popup", $data);
	}

//팝업 삭제
	function delete_popup($idx) {
		$this->db->where("idx", $idx);
		$this->db->delete("iw_popup");
	}

//팝업 게시 조회
	function select_open_popup() {
		$sql = '';
		$sql .= "select * from iw_popup   \n";
		$sql .= " where open_yn = 'Y'     \n";

		$query = $this->db->query($sql);

		return $query;
	}

}
?>