<?php

/* ------------------------------------------------

mo_activity_log.php

  ------------------------------------------------ */
require_once('libraries/parse-php-sdk/autoload.php');
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
class Mo_parse extends Model {

    function __construct() {

        parent::Model();
        $this->load->database('default');
        if ($_SERVER['SERVER_NAME'] == 'motorbb.com'||$_SERVER['SERVER_NAME'] == 'www.motorbb.com') {
            ParseClient::initialize( 'UEpeIkJAZBs1EbETWEzbseSw0JECKJ9XL2Cf1uUE', 'zXXKFLcvAN599H5M8PvJKCiXPjxgaVdedhWTy4bU', '5bFg6lY9QsV2ZU3HjQlYi1fYDDDBptdCxXPifElU');
            //echo $_SERVER['SERVER_NAME'];
        } else {
            //echo $_SERVER['SERVER_NAME'];
            ParseClient::initialize( '86rGZ9lllJoH42rswvyBTix7fZcTlVED00wlTc7s', 'WyTczPbS8H3oBP7yvXKr6GJPoHpkDLCa0xFAXOxb', 'lGsDGPt3R4mSRlhYlw0YcAgvmiicjt4P8bsir4yn');
        }
        
    }
    function delete_by_idx($idx){
        $query = new ParseQuery("Car");
        $query->equalTo("idx", intval($idx));
        $result = $query->find();
        if(isset($result[0])){
            return $result[0]->destroy();
        }
       
    }
    function select_by_idx($idx){
        $query = new ParseQuery("Car");
        $query->equalTo("idx", intval($idx));
        $result = $query->find();
        return $result;
    }
    // function get_country_name_by_cc($cc){
    //     $this->db->select("country_name");
    //     $this->db->from('iw_country_list');
    //     $this->db->where('cc', $cc);
    //     $query = $this->db->get();
    //     $result = $query->result();
    //     if(count($result)>0){
    //         return $result[0]->country_name;
    //     }
        
    // }
    function select_car_detail_by_idx($idx){
        $this->db->select("iw_bbs.*, cl.country_name, CONCAT(ci.base_url, 't_item_thumb/', ci.public_id) as thumb_url", false);
        $this->db->from('iw_bbs');
        $this->db->join('iw_cloud_bbs_images ci', 'ci.bbs_idx = iw_bbs.idx AND ci.sort=0', 'left');
        $this->db->join('iw_country_list cl', 'cl.cc = iw_bbs.country', 'inner');
        $this->db->where('iw_bbs.idx', $idx);
        $this->db->group_by('iw_bbs.idx');
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();
        if(count($result)>0){
            return $result[0];
        }
        
    }
    function update($idx, $p_data=array()){

        $query = new ParseQuery("Car");

        $query->equalTo("idx", intval($idx));
        $car = $query->first();
        
        //$car->set('make', "set make test");
        
        
        foreach($p_data as $key=>$value){
            //echo $key.": ".$value."<br/>";
            $car->set($key, $value);
        }
        $result = $car->save();

        return $result;
    }
    function insert($p_data){
        
        $parse_obj = new ParseObject("Car");
        
        
        
        foreach($p_data as $key=>$value){
            $parse_obj->set($key, $value);
        }

        try {
            $parse_obj->save();
            return $parse_obj->getObjectId();
            //echo 'New object created with objectId: ' . $parse_obj->getObjectId();
        }catch (ParseException $ex) {  
            // Execute any logic that should take place if the save fails.
            // error is a ParseException object with an error code and message.
            return false;
            //echo 'Failed to create new object, with error message: ' . $ex->getMessage();
        }
        
    }
		
    function insert_or_update($idx){
        $parse_obj = new ParseObject("Car");
        
        /*** INIT DATA ***/
        $car_detail = $this->select_car_detail_by_idx($idx);
       
        $p_data=array(
            'idx'           =>intval($car_detail->idx),
            'stock_no'      =>intval($car_detail->car_stock_no),
            'condition_new' =>$car_detail->icon_new_yn,
            'status'        =>$car_detail->icon_status,
            'car_owner'     =>intval($car_detail->car_owner),
            'make'          =>$car_detail->car_make,
            'model'         =>$car_detail->car_model,
            'model_year'    =>intval($car_detail->car_model_year),
            'chassis_no'    =>$car_detail->car_chassis_no,
            'body_type'     =>$car_detail->car_body_type,
            'mileage'       =>intval($car_detail->car_mileage),
            'steering'      =>$car_detail->car_steering,
            'transmission'  =>$car_detail->car_transmission,
            'engine_size'   =>intval($car_detail->car_cc),
            'drive_type'    =>$car_detail->car_drive_type,
            'fuel_type'     =>$car_detail->car_fuel,
            'country'       =>$car_detail->country,
            'country_name'  =>$car_detail->country_name,
            'ex_color'      =>$car_detail->car_color,
            'price'         =>intval($car_detail->car_fob_cost),
            'old_price'     =>intval($car_detail->old_fob_cost),
            'seat'          =>intval($car_detail->car_seat),
            'door'          =>intval($car_detail->door),
            'manu_month'     =>intval($car_detail->manu_month),
            'manu_year'     =>intval($car_detail->manu_year),
            'reg_month'     =>intval($car_detail->first_registration_month),
            'reg_year'      =>intval($car_detail->first_registration_year),
            'thumb_url'      =>$car_detail->thumb_url
        );

        /*** PARSE ***/

        $saved_car = $this->select_by_idx($idx);
        if(count($saved_car)>0){
            $this->update($idx, $p_data);
        }else{
            $this->insert($p_data);
        }
    }   
}

?>
