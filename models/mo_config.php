<?php

/* ------------------------------------------------
 * 파일명 : mo_config.php
 * 개  요 : 인츠웍스 사이트 관련 설정
  ------------------------------------------------ */
?>
<?php

class Mo_config extends Model {

	function __construct() {
		parent::Model();

		$this->load->database('default');
	}

//사이트 기본 설정 조회
	function select($config_type) {
		$sql = "select *               \r\n";
		$sql .= "  from iw_config       \r\n";
		$sql .= " where config_type = ? \r\n";

		$query = $this->db->query($sql, array($config_type));

		return $query;
	}

//사이트 기본 설정 상세 조회
	function select_detail($config_type, $key) {
		$this->db->select('*');
		$this->db->from('iw_config');
		$this->db->where('config_type', $config_type);
		$this->db->where('key', $key);

		$query = $this->db->get();


		/*
		  $sql  = "select *               \r\n";
		  $sql .= "  from iw_config       \r\n";
		  $sql .= " where config_type = ? \r\n";
		  $sql .= "   and key         = ? \r\n";


		  $query = $this->db->query($sql, array($config_type, $key));
		 */

		return $query;
	}

//사이트 기본 설정 입력
	function insert($data) {
		$this->db->insert('iw_config', $data);

		return $this->db->insert_id();
	}

//사이트 기본 설정 수정
	function update($data) {
		$this->db->set('key', $data['key']);
		$this->db->set('value', $data['value']);
		$this->db->where('key', $data['key']);
		$this->db->where('config_type', $data['config_type']);
		$this->db->update('iw_config', $data);
	}

//사이트 관리자 계정 체크(회원관리가 없을 경우)
	function site_admin_chk() {
		$sql = "select *              \r\n";
		$sql .= "  from iw_member      \r\n";
		$sql .= " where certi_yn = 'Y' \r\n";
		$sql .= "   and grade_no = 1;  \r\n"; //관리자 등급 번호는 '1'로 고정

		$query = $this->db->query($sql);

		return $query;
	}

//인츠웍스 계정 체크
	function iw_site_admin_chk() {
		$IWCOMMONDB = $this->load->database('iwcommon', TRUE);

		$sql = "select *              \r\n";
		$sql .= "  from iwc_member     \r\n";
		$sql .= " where use_yn = 'Y'   \r\n";

		$query = $IWCOMMONDB->query($sql);

		return $query;
	}

//사이트 관리자 계정 등록
	function insert_admin_account($data) {
		$this->db->insert('iw_member', $data);

		return $this->db->insert_id();
	}

//사이트 관리자 계정 수정
	function update_admin_account($member_no, $data) {
		$this->db->set('member_id', $data['member_id']);
		$this->db->set('member_pwd', $data['member_pwd']);
		$this->db->where('member_no', $member_no);
		$this->db->update('iw_member', $data);

		return $this->db->affected_rows();
	}

//사이트 DB 전체 삭제
	function delete_all_site_admin() {
		$sql_01 = "delete from iw_site_map;";
		$sql_02 = "delete from iw_config;";
		$sql_03 = "delete from iw_bbs_config;";
		$sql_04 = "delete from iw_bbs;";
		$sql_05 = "delete from iw_member;";
		$sql_06 = "delete from iw_member_grade;";
		$sql_07 = "delete from iw_statistics;";
		$sql_08 = "delete from iw_pay_lgtelecom;";

		$this->db->query($sql_01);
		$this->db->query($sql_02);
		$this->db->query($sql_03);
		$this->db->query($sql_04);
		$this->db->query($sql_05);
		$this->db->query($sql_06);
		$this->db->query($sql_07);
		$this->db->query($sql_08);
	}

//ROOT 계정 등록
	function insert_iw_account($data) {
		$IWCOMMONDB = $this->load->database('iwcommon', TRUE);

		$IWCOMMONDB->insert('iwc_member', $data);

		return $IWCOMMONDB->insert_id();
	}

//ROOT 계정 수정
	function update_iw_account($member_no, $data) {
		$IWCOMMONDB = $this->load->database('iwcommon', TRUE);

		$IWCOMMONDB->set('member_id', $data['member_id']);
		$IWCOMMONDB->set('member_pwd', $data['member_pwd']);
		$IWCOMMONDB->where('member_no', $member_no);
		$IWCOMMONDB->update('iwc_member', $data);

		return $this->db->affected_rows();
	}

//사이트 맵 조회
	function select_site_map() {
		$sql = 'select * from iw_site_map order by menu_code_order asc, seq_no asc';
		$query = $this->db->query($sql);

		return $query;
	}

//사이트 맵 상세 조회
	function select_detail_site_map($seq_no) {
		$sql = 'select * from iw_site_map where seq_no = ?';
		$query = $this->db->query($sql, array($seq_no));

		return $query;
	}

//사이트 맵 상세 조회 - 메뉴코드별
	function select_menu_code_site_map($menu_code) {
		$sql = 'select * from iw_site_map where menu_code = ?';
		$query = $this->db->query($sql, array($menu_code));

		return $query;
	}

//사이트 맵 등록
	function insert_site_map($data) {
		$this->db->insert('iw_site_map', $data);

		return $this->db->insert_id();
	}

//사이트 맵 수정
	function update_site_map($data, $seq_no) {
		$this->db->where('seq_no', $seq_no);
		$this->db->update('iw_site_map', $data);

		return $this->db->affected_rows();
	}

//사이트 맵 삭제
	function delete_site_map($seq_no) {
		$menu_code_order = '';
		$menu_code = ''; //메뉴코드

		$this->db->trans_begin();

//SELECT
		$sql = 'select * from iw_site_map where seq_no = ?';
		$query = $this->db->query($sql, array($seq_no));

		if ($query->num_rows > 0) {
			foreach ($query->result() as $rows) {
				$menu_code_order = $rows->menu_code_order;
				$menu_code = $rows->menu_code;
			}
		}

//UPDATE
		$sql = '';
		$sql = "update iw_site_map \r\n";
		$sql .= "   set menu_code_order = menu_code_order - 1 \r\n";
		$sql .= " where menu_code_order > ?";

		$this->db->query($sql, array($menu_code_order));

//DELETE
		$sql = '';
		$sql = "delete from iw_site_map \r\n";
		$sql .= " where seq_no = ?";

		$this->db->query($sql, array($seq_no));

//DELETE bbs_config
		$sql = '';
		$sql = "delete from iw_bbs_config \r\n";
		$sql .= " where bbs_code = ?";

		$this->db->query($sql, array($menu_code));

//DELETE bbs 게시판
		$sql = '';
		$sql = "delete from iw_bbs \r\n";
		$sql .= " where menu_code = ?";

		$this->db->query($sql, array($menu_code));

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();

			return false;
		} else {
			$this->db->trans_commit();

			return true;
		}
	}

//부모 메뉴코드를 조회한다.
	function select_parents_menu_code($menu_code_grp) {
		$sql = 'select * from iw_site_map where menu_code = ?';
		$query = $this->db->query($sql, array($menu_code_grp));

		return $query->result();
	}

//메뉴코드 입력시 정렬 순서를 수정한다.
	function update_menu_code_order($parent_menu_code_order, $selected_menu_code_order = NULL) {
		$sql = "update iw_site_map \r\n";
		$sql .= "   set menu_code_order = menu_code_order + 1 \r\n";
		$sql .= " where menu_code_order > ?";
		if ($selected_menu_code_order != NULL) {
			$sql .= "   and menu_code_order < ?";
		}

		$this->db->query($sql, array($parent_menu_code_order, $selected_menu_code_order));

		/*
		  $sql2  = "update iw_site_map \r\n";
		  $sql2 .= "   set menu_code_order = menu_code_order - 1 \r\n";
		  $sql2 .= " where menu_code_order > ?";

		  $this->db->query($sql2, array($selected_menu_code_order));
		 */

		return $this->db->affected_rows();
	}

//사이트 맵(해당 메뉴코드) 중복 체크
	function select_chk_menu_code($menu_code) {
		$sql = 'select * from iw_site_map where menu_code = ?';
		$query = $this->db->query($sql, array($menu_code));

		return $query->num_rows();
	}

//사이트 맵 정렬순서 MAX값
	function select_max_menu_code_order() {
		$menu_code_order = '';
		$sql = 'select max(menu_code_order) + 1 as menu_code_order from iw_site_map;';

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$menu_code_order = $rows->menu_code_order;
			}
		} else {
			$menu_code_order = '1';
		}

		return $menu_code_order;
	}

	/* ------------------------------------------------------------------------------------------------ */

//게시판 설정 정보를 조회한다.
	function select_bbs_config($bbs_code) {
		$sql = 'select * from iw_bbs_config where bbs_code = ?;';

		$query = $this->db->query($sql, array($bbs_code));

		return $query;
	}

//게시판 설정 정보를 상세 조회한다.
	function select_detail_bbs_config($bbs_code, $key) {
		$this->db->select('*');
		$this->db->from('iw_bbs_config');
		$this->db->where('bbs_code', $bbs_code);
		$this->db->where('key', $key);

		$query = $this->db->get();

		return $query;
	}

//게시판 설정 정보 입력
	function insert_bbs_config($data) {
		$this->db->insert('iw_bbs_config', $data);

		return $this->db->insert_id();
	}

//게시판 설정 정보 수정
	function update_bbs_config($data) {
		$this->db->set('key', $data['key']);
		$this->db->set('value', $data['value']);
		$this->db->where('key', $data['key']);
		$this->db->where('bbs_code', $data['bbs_code']);
		$this->db->update('iw_bbs_config', $data);
	}

//게시판 설정 정보 삭제
	function delete_bbs_config($bbs_code) {
//현재 게시판 설정 정보 삭제
		$this->db->where('bbs_code', $bbs_code);
		$this->db->delete('iw_bbs_config');
	}

//게시판 설정에 필요한 회원등급을 조회한다.
	function member_grade_config() {
		$sql = 'select * from iw_member_grade order by grade_no desc;';

		$query = $this->db->query($sql);

		return $query;
	}

	/* ------------------------------------------------------------------------------------------------ */

//회원가입 설정 정보를 조회한다.
	function select_member_join_config() {
		$sql = 'select * from iw_member_join_config;';

		$query = $this->db->query($sql);

		return $query;
	}

//회원가입 설정 정보를 상세 조회한다.
	function select_detail_member_join_config($member_join_code, $key) {
		$this->db->select('*');
		$this->db->from('iw_member_join_config');
		$this->db->where('member_join_code', $member_join_code);
		$this->db->where('key', $key);

		$query = $this->db->get();

		return $query;
	}

//회원가입 설정 정보 입력
	function insert_member_join_config($data) {
		$this->db->insert('iw_member_join_config', $data);

		return $this->db->insert_id();
	}

//회원가입 설정 정보 수정
	function update_member_join_config($data) {
		$this->db->set('key', $data['key']);
		$this->db->set('value', $data['value']);
		$this->db->where('key', $data['key']);
		$this->db->where('member_join_code', $data['member_join_code']);
		$this->db->update('iw_member_join_config', $data);
	}

//회원가입 설정 정보 삭제
	function delete_member_join_config($member_join_code) {
//현재 게시판 설정 정보 삭제
		$this->db->where('member_join_code', $member_join_code);
		$this->db->delete('iw_member_join_config');
	}

	/* ------------------------------------------------------------------------------------------------ */

//회원로그인 설정 정보를 조회한다.
	function select_member_login_config() {
		$sql = 'select * from iw_member_login_config;';

		$query = $this->db->query($sql);

		return $query;
	}

//회원로그인 설정 정보를 상세 조회한다.
	function select_detail_member_login_config($member_login_code, $key) {
		$this->db->select('*');
		$this->db->from('iw_member_login_config');
		$this->db->where('member_login_code', $member_login_code);
		$this->db->where('key', $key);

		$query = $this->db->get();

		return $query;
	}

//회원로그인 설정 정보 입력
	function insert_member_login_config($data) {
		$this->db->insert('iw_member_login_config', $data);

		return $this->db->insert_id();
	}

//회원로그인 설정 정보 수정
	function update_member_login_config($data) {
		$this->db->set('key', $data['key']);
		$this->db->set('value', $data['value']);
		$this->db->where('key', $data['key']);
		$this->db->where('member_login_code', $data['member_login_code']);
		$this->db->update('iw_member_login_config', $data);
	}

//회원로그인 설정 정보 삭제
	function delete_member_login_config($member_login_code) {
//현재 게시판 설정 정보 삭제
		$this->db->where('member_login_code', $member_login_code);
		$this->db->delete('iw_member_login_config');
	}

}
?>