<?php

/* ------------------------------------------------
 * 파일명 : mo_statistics.php
 * 개  요 : 접속 통계
  ------------------------------------------------ */
?>
<?php

class Mo_statistics extends Model {

	function __construct() {
		parent::Model();

		$this->load->database('default');
	}




//접속통계 등록
	function insert($data) {
		$this->db->insert('iw_statistics', $data);

		return $this->db->insert_id();
	}

//년도 그룹을 조회한다.
	function select_year_group() {
		$sql = '';
		$sql = " select visit_year from iw_statistics \n";
		$sql .= "  where menu_code = 'site_main'       \n";
		$sql .= "  group by visit_year                 \n";
		$sql .= "  order by visit_year desc;           \n";

		$query = $this->db->query($sql);

		return $query->result();
	}

//년간 접근통계 조회
	function select_year($year) {
		$sql = '';
		$sql = " select visit_month, count(visit_ip) cnt ";
		$sql .="   from iw_statistics  ";
		$sql .="  where visit_year = ? ";
		$sql .="    and menu_code = 'site_main' ";
		$sql .="  group by visit_month  ";
		$sql .="  order by visit_month; ";

		$query = $this->db->query($sql, array($year));

		return $query->result();
	}

//월간 그룹을 조회한다.
	function select_month_group() {
		$sql = " select visit_year, visit_month from iw_statistics ";
		$sql.="    where menu_code = 'site_main' ";
		$sql.=" group by visit_year, visit_month ";
		$sql.=" order by visit_year desc, visit_month desc ";

		$query = $this->db->query($sql);

		return $query->result();
	}

//월간 접근통계 조회
	function select_month($year, $month) {
		$sql = '';
		$sql = " select visit_day, count(visit_ip) cnt ";
		$sql .="   from iw_statistics ";
		$sql .="  where visit_year = ? ";
		$sql .="    and visit_month = ? ";
		$sql .="    and menu_code = 'site_main' ";
		$sql .=" group by visit_day ";
		$sql .=" order by visit_day ";

		$query = $this->db->query($sql, array($year, $month));

		return $query->result();
	}

//일별 그룹을 조회한다.
	function select_time_group() {
		$sql = '';
		$sql = " select visit_year, visit_month, visit_day from iw_statistics ";
		$sql .="  where menu_code = 'site_main' ";
		$sql .="  order by visit_month desc, visit_day desc ";
		$sql .="  limit 1";

		$query = $this->db->query($sql);
		return $query->result();
	}

//일별 접근통계 조회
	function select_time($sDate, $eDate) {
		$sql = " select visit_time, count(visit_ip) cnt ";
		$sql.="   from iw_statistics ";
		$sql.="  where visit_datetime between ? and ? ";
		$sql.="    and menu_code = 'site_main' ";
		$sql.=" group by visit_time ";
		$sql.=" order by visit_time ";

		$query = $this->db->query($sql, array($sDate, $eDate . " 23:59:59"));
		return $query->result();
	}

//메뉴별 접속통계
	function select_menu($sDate, $eDate) {
		$sql = " select a.menu_code_name, b.menu_code, b.cnt ";
		$sql.="    from (select * from iw_site_map where menu_code_level > 0) as a";
		$sql.=" LEFT OUTER JOIN ";
		$sql.=" (select menu_code, count(visit_ip) cnt ";
		$sql.="   from iw_statistics ";
		$sql.="  where visit_datetime between ? and ? ";
		$sql.=" group by menu_code ) as b ";
		$sql.=" on a.menu_code = b.menu_code ";
		$sql.=" order by b.cnt desc ";

		$query = $this->db->query($sql, array($sDate, $eDate . " 23:59:59"));
		return $query->result();
	}

//접근경로통계
	function select_referer($sDate, $eDate) {
		$sql = " select a.referer, a.cnt from ( ";
		$sql.=" select referer, count(visit_ip) cnt ";
		$sql.="   from iw_statistics ";
		$sql.="  where visit_datetime between ? and ? ";
		$sql.=" group by referer ) as a";
		$sql.=" where a.referer <> '' ";
		$sql.=" order by a.cnt desc ";

		$query = $this->db->query($sql, array($sDate, $eDate . " 23:59:59"));
		return $query->result();
	}

//검색키워드별 접속 통계
	function select_keyword($sDate, $eDate, $searchEngine) {
		$sql = "SELECT A.keyword                             \n";
		$sql .= "      ,A.keyword_cnt                         \n";
		$sql .= "      ,A.search_site                         \n";
		$sql .= "      ,A.query_name                          \n";
		$sql .= "  FROM (                                     \n";
		$sql .= "        SELECT keyword                       \n";
		$sql .= "              ,search_site                   \n";
		$sql .= "              ,query_name                    \n";
		$sql .= "              ,COUNT(keyword) AS keyword_cnt \n";
		$sql .= "          FROM iw_statistics                 \n";
		$sql .= "         WHERE visit_datetime BETWEEN ? AND ?\n";
		$sql .= "           AND keyword <> ''                 \n";
		if ($searchEngine != '' && $searchEngine != NULL) {
			$sql .= "           AND search_site = ?           \n";
		}
		$sql .= "         GROUP BY keyword                    \n";
		$sql .= "       ) AS A                                \n";
		$sql .= " ORDER BY A.keyword_cnt DESC;                \n";

		$query = '';

		if ($searchEngine != '' && $searchEngine != NULL) {
			$query = $this->db->query($sql, array($sDate . ' 00:00:00', $eDate . ' 23:59:59', $searchEngine));
		} else {
			$query = $this->db->query($sql, array($sDate . ' 00:00:00', $eDate . ' 23:59:59'));
		}

		return $query->result();
	}

//총 접속 통계수
	function select_total_cnt() {
		$sql = '';
		$sql = " select COUNT(*) as total_cnt ";
		$sql .="   from iw_statistics  ";
		$sql .="  where menu_code = 'site_main' ";

		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}

//오늘 접속 통계수
	function select_today_cnt($year, $month, $day) {
		$sql = '';
		$sql = " select COUNT(*) as total_cnt";
		$sql .="   from iw_statistics  ";
		$sql .="  where menu_code = 'site_main' ";
		$sql .="    and visit_year  = ? ";
		$sql .="    and visit_month = ? ";
		$sql .="    and visit_day   = ? ";

		$query = $this->db->query($sql, array($year, $month, $day));

		$result=$query->result();
		return $result['0']->total_cnt;

	}

	function select_country_ip($ip) {
		$sql = '';
		$sql = " select cc, cn FROM ip_table \n";
		$sql .= "  WHERE $ip BETWEEN start AND end\n";

		$query = $this->db->query($sql);

		return $query;

	}

	function select_count_ip() {
		$sql = '';
		$sql = " select count(cn) as count_ip FROM ip_table \n";
		$sql .= "  WHERE $ip BETWEEN start AND end\n";

		$query = $this->db->query($sql);
		$result = $query->result();

		return $result;

	}

	function select_user_cc(){
		$ip_address = $this->input->ip_address();
		$ip = ip2long($ip_address);
		$sql = '';
		$sql = " select cc, cn FROM ip_table \n";
		$sql .= "  WHERE $ip BETWEEN start AND end\n";

		$query = $this->db->query($sql);
		$result = $query->result();
		if(count($result)>0){
			return $result[0]->cc;
		}else{
			return '';
		}

	}
}
?>
