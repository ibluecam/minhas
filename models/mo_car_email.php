<?php

/* ------------------------------------------------

 * 파일명 : mo_bbs.php

 * 개  요 : 게시판 관리

  ------------------------------------------------ */

class Mo_car_email extends Model {

    function __construct() {

        parent::Model();



        $this->load->database('default');
    }
    /*
        @param: is_sent
            NULL: select all
            0: select only email that was not yet sent
            1: select only email that was sent
    */
    public function select_all($is_sent=NULL){
        $this->db->select("*");
        $this->db->from('iw_car_email_compaign');
        if($is_sent!==NULL){
            $this->db->where('is_sent', $is_sent);
        }
        $query = $this->db->get();
       
        $result = $query->result();
        return $result;
    }

    public function mark_sent($id, $is_sent=1){
        $data['is_sent'] = $is_sent;
        $this->db->where('id', $id);
        $result = $this->db->update("iw_car_email_compaign", $data);
        
        return $result;
    }

    public function delete_compaign($compaign_id=array()){
        $id = array();
        foreach ($compaign_id as $row) { 
            $id[] = $row['id'];
        }
        $this->db->where_in('id', $id);
        $result = $this->db->delete("iw_car_email_compaign");
        
        return $result;
    }

    function delete_compaign_by_bbs_idx($bbs_idx){
        $this->db->where('bbs_idx', $bbs_idx);
        $result = $this->db->delete("iw_car_email_compaign");
        return $result;
    }

    public function delete_multiple_compaign($compaign_id=array()){
        $id = array();
        foreach ($compaign_id as $row) { 
            $id[] = $row['id'];
        }
        $this->db->where_in('id', $id);
        $result = $this->db->delete("iw_car_email_compaign");

        return $result;
    }

    public function delete_dll_compaign(){
        $this->db->where('id is not null');
        $result = $this->db->delete("iw_car_email_compaign");
        return $result;
    }

    // public function select_by_id($id, $is_sent=NULL){
    //     $row = array();
    //     $this->db->select("*");
    //     $this->db->from('iw_car_email_compaign');
    //     $this->db->where('id', $id);
    //     if($is_sent!==NULL){
    //         $this->db->where('is_sent', $is_sent);
    //     }
    //     $query = $this->db->get();
        
    //     if($query->num_rows>0){
    //         $result = $query->result();
    //         $row = $result[0];
    //     }
       
    //     return $row;
    // }

    public function select_by_id($compaign_id=array(), $is_sent=NULL){
        $ids = array();
        foreach ($compaign_id as $row) { 
            $ids[] = $row['id'];
        }

        $this->db->select("*");
        $this->db->from('iw_car_email_compaign');
        $this->db->where_in('id', $ids);

        if($is_sent!==NULL){
            $this->db->where('is_sent', $is_sent);
        }

        $this->db->order_by("bbs_idx", "asc");
        $query = $this->db->get();
        $result = $query->result();
       
        return $result;
    }

    public function select_by_bbs_idx($bbs_idx){
        $this->db->select("*");
        $this->db->from('iw_car_email_compaign');
        $this->db->where('bbs_idx', $bbs_idx);
        $this->db->order_by("bbs_idx", "asc");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }



    public function check_exist($idx, $email){
        $this->db->select('COUNT(*) as num_rows');
        $this->db->from('iw_car_email_compaign');
        $this->db->where('bbs_idx', $idx);
        $this->db->where('customer_email', $email);
        
        $query = $this->db->get();
        $result = $query->result();
        return $result[0];
    }

    public function select_by_multiple_id($compaign_id=array(), $is_sent=NULL){
        $ids = array();
        foreach ($compaign_id as $row) { 
            $ids[] = $row['id'];
        }

        $this->db->select("iw_car_email_compaign.*,iw_bbs.car_fob_cost,iw_bbs.country,iw_bbs.car_model_year,iw_bbs.car_make,iw_bbs.car_model");
        $this->db->from('iw_car_email_compaign');
        $this->db->where_in('id', $ids);

        if($is_sent!==NULL){
            $this->db->where('is_sent', $is_sent);
        }

        $this->db->join("iw_bbs", "iw_bbs.idx = iw_car_email_compaign.bbs_idx");
        // echo $this->db->_compile_select();  exit();
        $this->db->group_by('iw_bbs.idx');
        $query = $this->db->get();
        $result = $query->result();
       
        return $result;
    }

    public function select_multiple_email_by_id($compaign_id=array(), $is_sent=NULL){
        $ids = array();
        foreach ($compaign_id as $row) { 
            $ids[] = $row['id'];
        }

        $this->db->select('customer_email, COUNT(customer_email) as num_rows,iw_member.member_first_name');
        $this->db->from('iw_car_email_compaign');
        $this->db->where_in('id', $ids);

        if($is_sent!==NULL){
            $this->db->where('is_sent', $is_sent);
        }
        $this->db->join("iw_member", "iw_member.email = iw_car_email_compaign.customer_email");
        $this->db->group_by('customer_email'); 
        $query = $this->db->get();
        $result = $query->result();
       
        return $result;
    }

    public function select_car_by_multiple_idx($compaign_id=array()){
        $idx = array();
        foreach ($compaign_id as $value) { 
            $idx[] = $value;
        }

        $this->db->select("idx,car_fob_cost,old_fob_cost,country,car_model_year,car_make,car_model,CONCAT(base_url,'t_item_thumb/',public_id) as car_thumb", false);
        $this->db->from('iw_bbs');
        $this->db->join('iw_cloud_bbs_images', 'bbs_idx=iw_bbs.idx', "left");
        $this->db->group_by('iw_bbs.idx');
        $this->db->where_in('idx', $idx);

        // echo $this->db->_compile_select();  exit();
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();
       
        return $result;
    }

}

?>




