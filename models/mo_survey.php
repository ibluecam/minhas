<?php

/* ------------------------------------------------
 * 파일명 : mo_survey.php
 * 개  요 : 설문조사 관리
  ------------------------------------------------ */
?>
<?php

class Mo_survey extends Model {

	function __construct() {
		parent::Model();
		$this->load->database('default');
	}

	// ID중복 체크
	function chk_entry_id($bbs_idx, $bbs_code, $member_id) {
		$Res = $this->db->query("SELECT COUNT(`idx`) AS `cnt` FROM `iw_survey_entry`
			WHERE `bbs_idx` = '".$bbs_idx."'
				AND `menu_code` = '".$bbs_code."'
				AND `member_id` = '".$member_id."'
			");
		list($Cnt) = $Res->result();
		if($Cnt->cnt == 0) {
			return false;
		} else {
			return true;
		}
	}

	// IP중복 체크
	function chk_entry_ip($bbs_idx, $bbs_code, $ip) {
		$Res = $this->db->query("SELECT COUNT(`idx`) AS `cnt` FROM `iw_survey_entry`
			WHERE `bbs_idx` = '".$bbs_idx."'
				AND `menu_code` = '".$bbs_code."'
				AND `writer_ip` = '".$ip."'
			");
		list($Cnt) = $Res->result();
		if($Cnt->cnt == 0) {
			return false;
		} else {
			return true;
		}
	}

	// 설문조사 삭제
	function config_delete($bbs_idx, $bbs_code, $no = '') {
		if($no != '') {
			$this->db->where("no", $no);
		}
		$this->db->where("bbs_idx", $bbs_idx);
		$this->db->where("bbs_code", $bbs_code);
		$this->db->delete("iw_survey_config");
		if($no != '') {
			$this->db->query("UPDATE `iw_survey_config`
				SET `no` = (`no` - 1)
				WHERE `bbs_idx` = '".$bbs_idx."'
					AND `bbs_code` = '".$bbs_code."'
					AND `no` > ".$no."
				");
		}
	}

	// 설문조사 설정 저장
	function config_insert($bbs_idx, $menu_code, $config) {
		if(is_array($config)) {

			$this->config_delete($bbs_idx, $menu_code); // 기존정보 삭제

			if(isset($config['field']) && is_array($config['field'])) {
				foreach($config['field'] as $Ckey => $Cval) {
					//var_dump($Ckey);
					foreach($Cval as $Vkey => $Vval) {
						$Insert['bbs_code'] = $menu_code;
						$Insert['bbs_idx'] = $bbs_idx;
						$Insert['column'] = 'field';
						$Insert['no'] = $Ckey;
						$Insert['key'] = $Vkey;
						$Insert['value'] = $Vval;
						//var_dump($Insert);
						$this->db->insert('iw_survey_config', $Insert);
						$insert_id = $this->db->insert_id();
						unset($Insert, $Vkey, $Vval);
					}
				}
			}
			if(isset($config['custom']) && is_array($config['custom'])) {
				foreach($config['custom'] as $Ckey => $Cval) {
					//var_dump($Ckey);
					foreach($Cval as $Vkey => $Vval) {
						$Insert['bbs_code'] = $menu_code;
						$Insert['bbs_idx'] = $bbs_idx;
						$Insert['column'] = 'custom';
						$Insert['no'] = $Ckey;
						$Insert['key'] = $Vkey;
						$Insert['value'] = $Vval;
						//var_dump($Insert);
						$this->db->insert('iw_survey_config', $Insert);
						$insert_id = $this->db->insert_id();
						unset($Insert, $Vkey, $Vval);
					}
				}
			}

			unset($Ckey, $Cval);
		} else {
			return false;
		}
	}

	// 설문조사 설정
	function config($bbs_idx, $menu_code = '', $cnt = 0) {
		$Qry = "SELECT * FROM `iw_survey_config`
			 WHERE `bbs_idx` = '" . $bbs_idx . "'";
		if($menu_code != '') {
			$Qry .= " AND `bbs_code` = '".$menu_code."'";
		}
		$Qry .= " ORDER BY `no` ASC, `column` ASC";

		$SurveyRes = $this->db->query($Qry)->result();


		if(isset($SurveyRes)) {
			$Survey = Array();
			foreach($SurveyRes as $Rkey => $Rval) {
				$Survey[$Rval->column][$Rval->no]->{$Rval->key} = $Rval->value;

				// DB 잘못들어간데이타 보상시켜줌
				if(!isset($Survey[$Rval->column][$Rval->no]->no)) {
					$Survey[$Rval->column][$Rval->no]->no = $Rval->no;
				}
				if(!isset($Survey[$Rval->column][$Rval->no]->title)) {
					$Survey[$Rval->column][$Rval->no]->title = '';
				}
				if(!isset($Survey[$Rval->column][$Rval->no]->cont)) {
					$Survey[$Rval->column][$Rval->no]->cont = '';
				}
				if(!isset($Survey[$Rval->column][$Rval->no]->type)) {
					$Survey[$Rval->column][$Rval->no]->type = '';
				}
				if(!isset($Survey[$Rval->column][$Rval->no]->quest)) {
					$Survey[$Rval->column][$Rval->no]->quest = '';
				}

				unset($Rkey, $Rval);
			}

			// dummy
			if(!isset($Survey['custom'])) {
				$Survey['custom'] = Array();
			}
			if(!isset($Survey['field'])) {
				$Survey['field'] = Array();
			}

			if(5 > count($Survey['custom'])) {
				for($i = count($Survey['custom']); $i < 5; $i++) {
					$Survey['custom'][count($Survey['custom'])+1]->title = '';
				}
			}


			//var_dump($Survey['custom']);

			// 더 많은 항목 생성시 더미 생성
			for($i = count($Survey['field']); $i < $cnt; $i++) {
				$Survey['field'][count($Survey['field'])+1]->no = count($Survey)+1;
				$Survey['field'][count($Survey['field'])]->title = '';
				$Survey['field'][count($Survey['field'])]->cont = '';
				$Survey['field'][count($Survey['field'])]->type = '';
				$Survey['field'][count($Survey['field'])]->quest = '';
			}
			return $Survey;
		} else {
			return false;
		}
	}

	// 설문조사 저장
	function insert($Insert) {
		$Insert['writer_ip'] = $_SERVER['REMOTE_ADDR'];
		$Insert['created_dt'] = date('Y-m-d H:i:s');

		$this->db->insert('iw_survey_entry', $Insert);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	// 설문조사 결과
	function select_entry($bbs_idx, $menu_code = '') {
		$Res = $this->db->query("SELECT *
			FROM `iw_survey_entry`
			WHERE `bbs_idx` = '".$bbs_idx."'
				AND `menu_code` = '".$menu_code."';");
		return $Res;
	}

	// 설문조사 결과
	function select($bbs_idx, $menu_code = '') {
		$Cfg = $this->config($bbs_idx, $menu_code);

		// 카운트
		list($ResCnt) = $this->db->query("SELECT COUNT(`idx`) AS `cnt`
			FROM `iw_survey_entry`
			WHERE `bbs_idx` = '".$bbs_idx."';")->result();

		// 항목별로 그룹바이 시켜드림
		if(isset($Cfg['field'])) {
			foreach($Cfg['field'] as $Rkey => &$Rval) {
				$group = $this->db->query("SELECT COUNT(`idx`) AS `cnt`, `field".$Rkey."` AS `title`
					FROM `iw_survey_entry`
					WHERE `bbs_idx` = '".$bbs_idx."'
					GROUP BY `field".$Rkey."`
					ORDER BY `cnt` DESC, `field".$Rkey."` ASC;");

				$Rval->result = $group->result();
				$Rval->result_cnt = $ResCnt->cnt;
			}
		}
		return $Cfg;
	}

}