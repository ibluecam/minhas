<?php
function statistics($xdata, $ydata, $flag)
{
    include_once "jpgraph/jpgraph.php";
    include_once "jpgraph/jpgraph_line.php";
    include_once "jpgraph/jpgraph_bar.php";
    include_once "jpgraph/jpgraph_scatter.php";
    include_once "jpgraph/jpgraph_regstat.php";
    
    
    $sflag = "";
    if($flag=="MONTH") $sflag = "월";
    if($flag=="DAY")   $sflag = "일";
    if($flag=="HOUR")  $sflag = "시";

    // Create the graph
    $g = new Graph(800,350, "auto");
    $g->SetMargin(50,25,25,35);
    $g->title->SetMargin(7);
    $g->title->SetAlign('center');
    
    //$g->title->Set($title);
    $g->title->SetFont(FF_GULIM,FS_ITALIC, 9);
    $g->title->SetColor("#4D75C5");
    $g->SetMarginColor('#EFF0F2');

    $maxYdata = 0;
    for($i=0; $i<count($ydata); $i++)
    {
        if($ydata[$i] > $maxYdata)
            $maxYdata = $ydata[$i];  
    }
     
    $step = 1;    
    if($maxYdata > 10)
    {
        $step = $maxYdata / 10;
    }
    else
    {
        $maxYdata = 10;  
    }

    if($flag=="DAY")
    {
        $g->SetScale('linlin' ,0 , $maxYdata ,0 ,31); 
        $g->xaxis->scale->ticks->Set(1); 
        $g->yaxis->scale->ticks->Set($step);
    }
    elseif($flag=="MONTH")
    {
        $g->SetScale('linlin' ,0 ,$maxYdata ,0 ,12); 
        $g->xaxis->scale->ticks->Set(1); 
        $g->yaxis->scale->ticks->Set($step);
    }
    elseif($flag=="HOUR")
    {
        $g->SetScale('linlin' ,0 ,$maxYdata ,0 ,23); 
        $g->xaxis->scale->ticks->Set(1); 
        $g->yaxis->scale->ticks->Set($step); 
    }


    $g->SetFrame(true,'#6B6D6B',1); 
    
    // We want 1 decimal for the X-label
    $g->xaxis->SetFont(FF_GULIM,FS_BOLD,9);
    $g->xaxis->SetColor("black", "#082560");
    $g->xaxis->SetLabelFormat('%1.0f'.$sflag);
    $g->xaxis->HideFirstTickLabel(false);

    // We want 1 decimal for the Y-label
    $g->yaxis->SetFont(FF_GULIM,FS_NORMAL,9);
    $g->yaxis->SetLabelFormat('%1.0f명');
    $g->yaxis->SetColor("black", "#082560");
    $g->yaxis->HideFirstTickLabel(false);
    
    $splot = new ScatterPlot($ydata,$xdata);
    
    $splot->mark->SetFillColor('#0FC514@0.3');
    $splot->mark->SetColor('#0FC514@0.5');
    $splot->value->show();
    $splot->value->SetColor("red");
    $splot->value->SetFont(FF_GULIM, FS_BOLD, 8);
    $splot->value->SetFormat("%1.0f");
    $splot->value->SetMargin(5);
    $splot->value->SetAlign('top');
    $splot->value->SetAlign('center');
    
    // And a line plot to stroke the smooth curve we got
    // from the original control points
    if(count($xdata) > 1)
    {
        //$spline = new Spline($xdata,$ydata);
        //list($newx,$newy) = $spline->Get(60);
        $lplot = new LinePlot($ydata,$xdata);
//        $lplot = new LinePlot($newy,$newx);
        //$lplot->mark->SetType(MARK_UTRIANGLE);
    
        $lplot->SetColor('#969896');
        $g->Add($lplot);
    }

    $bplot = new BarPlot($ydata,$xdata);
    
    // Adjust fill color
    $bplot->SetFillColor('orange');
    $bplot->SetAbsWidth(2);
    $bplot->SetAlign('');
    //$bplot->value->Show();
    $g->Add($splot);

    $g->Add($bplot);
    
    // Add the plots to the graph and stroke

    return $g;
}


function statisticsPie($data, $regends, $flag)
{
    include_once ("jpgraph/jpgraph.php");
    include_once ("jpgraph/jpgraph_pie.php");
    include_once ("jpgraph/jpgraph_pie3d.php");

    if($flag=="DAY") $flag="일";
    if($flag=="MONTH") $flag="월";
    if($flag=="HOUR") $flag="시";
    
    $maxYdata = 0;
    $maxYpos = 0;
    $countAll = 0;
    for($i=0; $i<count($data); $i++)
    {
        $countAll = $countAll + $data[$i];
        if($data[$i] > $maxYdata)
        {
            $maxYdata = $data[$i];  
            $maxYpos = $i;
        }
    }

    $regends2 = NULL;
    for($i=0; $i<count($regends); $i++)
    {
        $regends2[$i] = $regends[$i].$flag."(".$data[$i]."명) (".round(($data[$i] / $countAll * 100),1)."%%)";
        $regends[$i] = $regends[$i].$flag."(".$data[$i]."명)  ";
        //$regends2[$i] = $regends[$i].$flag."(".$data[$i]."명) ";
    }

    $graph = new PieGraph(800,450);

    $graph->legend->SetFont(FF_GULIM,FS_BOLD,9);
    $graph->legend->SetColumns(1);
    $graph->legend->SetLineSpacing(2);
    $graph->legend->SetPos(0.05,0.4,'right','center'); 
    $graph->SetColor('#EFF0F2');
    $graph->SetFrame(true,'#6B6D6B',1); 
    
    $p1 = new PiePlot($data);
    $p1->ExplodeSlice($maxYpos);
    $p1->value->SetFont(FF_GULIM,FS_BOLD, 9);
    $p1->value->SetFormat('%2.1f%%');  
    $p1->SetSize(0.3);
    $p1->SetCenter(0.5,0.50);
    $p1->SetGuideLines(true, false);
    $p1->SetGuideLinesAdjust(1);
    //$p1->value->Show();            
    //$p1->SetLegends($regends);
    $p1->SetLabels($regends2);
    $p1->SetLabelType(PIE_VALUE_PER);
    $p1->SetLabelPos(1.0); 

    $graph->Add($p1);
    return $graph;
}

function surveyPie($data, $regends)
{
    include_once ("jpgraph/jpgraph.php");
    include_once ("jpgraph/jpgraph_pie.php");
    include_once ("jpgraph/jpgraph_pie3d.php");

    $maxYdata = 0;
    $maxYpos = 0;
    $countAll = 0;
    for($i=0; $i<count($data); $i++)
    {
        $countAll = $countAll + $data[$i];
        if($data[$i] > $maxYdata)
        {
            $maxYdata = $data[$i];  
            $maxYpos = $i;
        }
    }

    for($i=0; $i<count($regends); $i++)
    {
        $regends[$i]= func_cutstr($regends[$i], 30);
    }


    $regends2 = NULL;
    for($i=0; $i<count($regends); $i++)
    {
        if($countAll==0)
        {
            //$regends2[$i] = $regends[$i]."(".$data[$i]."명) (".round(($data[$i] / $countAll * 100),1)."%%)";
            $regends2[$i] ="0%%";
            $regends[$i] = "0(0명)  ";
        }
        else
        {
            //$regends2[$i] = $regends[$i]."(".$data[$i]."명) (".round(($data[$i] / $countAll * 100),1)."%%)";
            $regends2[$i] = round(($data[$i] / $countAll * 100),1)."%%";
            $regends[$i] = $regends[$i]."(".$data[$i]."명)  ";
        }
    }
    
    $reCount = count($regends);
    if($reCount > 6)
    {
        $reCount = $reCount - 6;
        $reCount = $reCount * 20;   
    }
    else
    {
        $reCount = 0;   
    }
    

    //$graph = new PieGraph(640,130 + $reCount, "auto");
    $graph = new PieGraph(613,160 + $reCount, "auto");
    $graph->SetFrame(true,'',0);
    $graph->legend->SetFont(FF_GULIM,FS_BOLD,9);
    $graph->legend->SetColumns(1);
    $graph->legend->SetLineSpacing(5);
    $graph->legend->SetFrameWeight(0);
    $graph->legend->SetPos(0.4,0.5,'left','center');
    $graph->legend->SetShadow(false); 
    $graph->legend->SetFillColor("#EFF0F2");
    //$graph->legend->SetPos(200, 20, 'left', 'center');
    $graph->SetColor('#EFF0F2');
    $graph->SetFrame(true,'#6B6D6B',1); 
    
    $p1 = new PiePlot($data);
    //$p1->ExplodeSlice($maxYpos);
    $p1->value->SetFont(FF_ARIAL,FS_NORMAL, 9);
    $p1->value->SetFormat('%2.1f%%');  
    $p1->SetSize(60);
    $p1->SetCenter(0.17,0.50);
    $p1->SetGuideLines(true, true);
    $p1->SetGuideLinesAdjust(0.5);
    
    $p1->value->Show();            
    $p1->SetLegends($regends);
    
    $p1->SetLabels($regends2, 0.6);
    $p1->SetLabelType(PIE_VALUE_PER);
    //$p1->SetLabelPos(1.0); 

    $graph->Add($p1);
    return $graph;
}

function menuSatistics($datax=NULL, $datay=NULL)
{
    include_once ("jpgraph/jpgraph.php");
    include_once ("jpgraph/jpgraph_bar.php");
    
    //$datay=array(2,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,120,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3);
    //$datax=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul");
    
    // Size of graph
    $width = 800; 
    $height= 20 * count($datax) + 20;
    
    // Set the basic parameters of the graph 
    $graph = new Graph($width,$height,'auto');
    $graph->SetScale("textlin");
    
    $top = 10;
    $bottom = 20;
    $left = 150;
    $right = 10;
    $graph->Set90AndMargin($left,$right,$top,$bottom);
    $graph->SetFrame(true,'#6B6D6B',1); 
     
    $graph->xaxis->SetPos('min');
    
    // Nice shadow
    //$graph->SetShadow();
    
    // Setup title
    $graph->SetMarginColor('#EFF0F2');
    // Setup X-axis
    $graph->xaxis->SetTickLabels($datax);
    $graph->xaxis->SetFont(FF_GULIM,FS_BOLD,9);
    
    // Some extra margin looks nicer
    $graph->xaxis->SetLabelMargin(5);
    
    // Label align for X-axis
    $graph->xaxis->SetLabelAlign('right','center');
    
    // Add some grace to y-axis so the bars doesn't go
    // all the way to the end of the plot area
    $graph->yaxis->scale->SetGrace(13);
    
    // Setup the Y-axis to be displayed in the bottom of the 
    // graph. We also finetune the exact layout of the title,
    // ticks and labels to make them look nice.
    $graph->yaxis->SetPos('max');

    // First make the labels look right
    $graph->yaxis->SetLabelAlign('center','top');
    $graph->yaxis->SetLabelFormat('%d');
    $graph->yaxis->SetLabelSide(SIDE_RIGHT);
    $graph->yaxis->SetTextTickInterval(1,1); 
    
    // The fix the tick marks
    $graph->yaxis->SetTickSide(SIDE_LEFT);
    // To center the title use :
    //$graph->yaxis->SetTitle('Turnaround 2002','center');
    //$graph->yaxis->title->Align('center');
    
    
    $graph->yaxis->SetFont(FF_GULIM,FS_NORMAL, 8);
    // If you want the labels at an angle other than 0 or 90
    // you need to use TTF fonts
    $graph->yaxis->Hide();

    // Now create a bar pot
    $bplot = new BarPlot($datay);
    $bplot->SetFillColor("orange");
    $bplot->SetAbsWidth(10);
    //$bplot->SetShadow();
    
    //You can change the width of the bars if you like
    //$bplot->SetWidth(0.5);
    
    // We want to display the value of each bar at the top
    $bplot->value->Show();
    $bplot->value->SetFont(FF_GULIM,FS_BOLD,9);
    $bplot->value->SetAlign('left','center');
    $bplot->value->SetColor("black","darkred");
    $bplot->value->SetFormat('%1.0f (명)');
    
    // Add the bar to the graph
    $graph->Add($bplot);
    return $graph;
}

function refererSatistics($datax=NULL, $datay=NULL)
{
    include_once ("jpgraph/jpgraph.php");
    include_once ("jpgraph/jpgraph_bar.php");
    
    //$datay=array(2,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,120,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3);
    //$datax=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul");
    
    // Size of graph
    $width = 800; 
    $height= 20 * count($datax) + 20;
    
    // Set the basic parameters of the graph 
    $graph = new Graph($width,$height,'auto');
    $graph->SetScale("textlin");
    
    $top = 10;
    $bottom = 20;
    $left = 150;
    $right = 10;
    $graph->Set90AndMargin($left,$right,$top,$bottom);
    $graph->SetFrame(true,'#6B6D6B',1); 
     
    $graph->xaxis->SetPos('min');
    
    // Nice shadow
    //$graph->SetShadow();
    
    // Setup title
    $graph->SetMarginColor('#EFF0F2');
    // Setup X-axis
    $graph->xaxis->SetTickLabels($datax);
    $graph->xaxis->SetFont(FF_GULIM,FS_BOLD,9);
    
    // Some extra margin looks nicer
    $graph->xaxis->SetLabelMargin(5);
    
    // Label align for X-axis
    $graph->xaxis->SetLabelAlign('right','center');
    
    // Add some grace to y-axis so the bars doesn't go
    // all the way to the end of the plot area
    $graph->yaxis->scale->SetGrace(13);
    
    // Setup the Y-axis to be displayed in the bottom of the 
    // graph. We also finetune the exact layout of the title,
    // ticks and labels to make them look nice.
    $graph->yaxis->SetPos('max');

    // First make the labels look right
    $graph->yaxis->SetLabelAlign('center','top');
    $graph->yaxis->SetLabelFormat('%d');
    $graph->yaxis->SetLabelSide(SIDE_RIGHT);
    $graph->yaxis->SetTextTickInterval(1,1); 
    
    // The fix the tick marks
    $graph->yaxis->SetTickSide(SIDE_LEFT);
    // To center the title use :
    //$graph->yaxis->SetTitle('Turnaround 2002','center');
    //$graph->yaxis->title->Align('center');
    
    
    $graph->yaxis->SetFont(FF_GULIM,FS_NORMAL, 8);
    // If you want the labels at an angle other than 0 or 90
    // you need to use TTF fonts
    $graph->yaxis->Hide();

    // Now create a bar pot
    $bplot = new BarPlot($datay);
    $bplot->SetFillColor("orange");
    $bplot->SetAbsWidth(10);
    //$bplot->SetShadow();
    
    //You can change the width of the bars if you like
    //$bplot->SetWidth(0.5);
    
    // We want to display the value of each bar at the top
    $bplot->value->Show();
    $bplot->value->SetFont(FF_GULIM,FS_BOLD,9);
    $bplot->value->SetAlign('left','center');
    $bplot->value->SetColor("black","darkred");
    $bplot->value->SetFormat('%1.0f (명)');
    
    // Add the bar to the graph
    $graph->Add($bplot);
    return $graph;
}

function keywordSatistics($datax=NULL, $datay=NULL)
{
    include_once ("jpgraph/jpgraph.php");
    include_once ("jpgraph/jpgraph_bar.php");
    
    //$datay=array(2,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,120,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3,3,5,8,12,6,3);
    //$datax=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul","Feb","Mar","Apr","May","Jun","Jul");
    
    // Size of graph
    $width=760; 
    $height=20 * count($datax) + 20;
    
    // Set the basic parameters of the graph 
    $graph = new Graph($width,$height,'auto');
    $graph->SetScale("textlin");
    
    $top = 10;
    $bottom = 20;
    $left = 150;
    $right = 10;
    $graph->Set90AndMargin($left,$right,$top,$bottom);
    $graph->SetFrame(true,'#6B6D6B',1); 
     
    $graph->xaxis->SetPos('min');
    
    // Nice shadow
    //$graph->SetShadow();
    
    // Setup title
    $graph->SetMarginColor('#EFF0F2');
    // Setup X-axis
    $graph->xaxis->SetTickLabels($datax);
    $graph->xaxis->SetFont(FF_GULIM,FS_BOLD,9);
    
    // Some extra margin looks nicer
    $graph->xaxis->SetLabelMargin(5);
    
    // Label align for X-axis
    $graph->xaxis->SetLabelAlign('right','center');
    
    // Add some grace to y-axis so the bars doesn't go
    // all the way to the end of the plot area
    $graph->yaxis->scale->SetGrace(13);
    
    // Setup the Y-axis to be displayed in the bottom of the 
    // graph. We also finetune the exact layout of the title,
    // ticks and labels to make them look nice.
    $graph->yaxis->SetPos('max');

    // First make the labels look right
    $graph->yaxis->SetLabelAlign('center','top');
    $graph->yaxis->SetLabelFormat('%d');
    $graph->yaxis->SetLabelSide(SIDE_RIGHT);
    $graph->yaxis->SetTextTickInterval(1,1); 
    
    // The fix the tick marks
    $graph->yaxis->SetTickSide(SIDE_LEFT);
    // To center the title use :
    //$graph->yaxis->SetTitle('Turnaround 2002','center');
    //$graph->yaxis->title->Align('center');
    
    
    $graph->yaxis->SetFont(FF_GULIM,FS_NORMAL, 8);
    // If you want the labels at an angle other than 0 or 90
    // you need to use TTF fonts
    $graph->yaxis->Hide();

    // Now create a bar pot
    $bplot = new BarPlot($datay);
    $bplot->SetFillColor("orange");
    $bplot->SetAbsWidth(10);
    //$bplot->SetShadow();
    
    //You can change the width of the bars if you like
    //$bplot->SetWidth(0.5);
    
    // We want to display the value of each bar at the top
    $bplot->value->Show();
    $bplot->value->SetFont(FF_GULIM,FS_BOLD,9);
    $bplot->value->SetAlign('left','center');
    $bplot->value->SetColor("black","darkred");
    $bplot->value->SetFormat('%1.0f (명)');
    
    // Add the bar to the graph
    $graph->Add($bplot);
    return $graph;
}

//관리자 메인 주간 방문자 통계
function adminMainMonth($xdata, $ydata )
{
    include_once "jpgraph/jpgraph.php";
    include_once "jpgraph/jpgraph_line.php";
    include_once "jpgraph/jpgraph_bar.php";
    include_once "jpgraph/jpgraph_scatter.php";
    include_once "jpgraph/jpgraph_regstat.php";
    
    $sflag="일";

    // Create the graph
    $g = new Graph(780,200, "auto");
    $g->SetMargin(50,25,25,35);
    $g->title->SetMargin(7);
    $g->title->SetAlign('center');
    
    //$g->title->Set($title);
    $g->title->SetFont(FF_GULIM,FS_ITALIC,9);
    $g->title->SetColor("#4D75C5");
    $g->SetMarginColor('#EFF0F2');

    $maxYdata = 0;
    for($i=0; $i<count($ydata); $i++)
    {
        if($ydata[$i] > $maxYdata)
            $maxYdata = $ydata[$i];  
    }
     
    $step = 1;    
    if($maxYdata > 10)
    {
        $step = $maxYdata / 10;
    }
    else
    {
        $maxYdata = 10;  
    }

    $g->SetScale('linlin' ,0 , $maxYdata ,0 ,31); 
    $g->xaxis->scale->ticks->Set(1); 
    $g->yaxis->scale->ticks->Set($step);

    $g->SetFrame(true,'#6B6D6B',1); 
    
    // We want 1 decimal for the X-label
    $g->xaxis->SetFont(FF_GULIM,FS_BOLD,9);
    $g->xaxis->SetColor("black", "#082560");
    $g->xaxis->SetLabelFormat('%1.0f'.$sflag);
    $g->xaxis->HideFirstTickLabel(false);

    // We want 1 decimal for the Y-label
    $g->yaxis->SetFont(FF_GULIM,FS_NORMAL,9);
    $g->yaxis->SetLabelFormat('%1.0f명');
    $g->yaxis->SetColor("black", "#082560");
    $g->yaxis->HideFirstTickLabel(false);
    
    $splot = new ScatterPlot($ydata,$xdata);
    
    $splot->mark->SetFillColor('#0FC514@0.3');
    $splot->mark->SetColor('#0FC514@0.5');
    $splot->value->show();
    $splot->value->SetColor("red");
    $splot->value->SetFont(FF_GULIM, FS_BOLD, 8);
    $splot->value->SetFormat("%1.0f");
    $splot->value->SetMargin(5);
    $splot->value->SetAlign('top');
    $splot->value->SetAlign('center');
    
    // And a line plot to stroke the smooth curve we got
    // from the original control points
    if(count($xdata) > 1)
    {
        //$spline = new Spline($xdata,$ydata);
        //list($newx,$newy) = $spline->Get(60);
        $lplot = new LinePlot($ydata,$xdata);
//        $lplot = new LinePlot($newy,$newx);
        //$lplot->mark->SetType(MARK_UTRIANGLE);
    
        $lplot->SetColor('#969896');
        $g->Add($lplot);
    }

    $bplot = new BarPlot($ydata,$xdata);
    
    // Adjust fill color
    $bplot->SetFillColor('orange');
    $bplot->SetAbsWidth(2);
    $bplot->SetAlign('');
    //$bplot->value->Show();
    $g->Add($splot);

    $g->Add($bplot);
    
    // Add the plots to the graph and stroke

    return $g;
}

?>
