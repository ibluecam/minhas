<?php 

class Helper{
	public static function func_base64_encode($txt) {
		$key = "abcdefghijklmnopqrstuvwxyz";
		$key_len = strlen($key);
		$rot_ptr_set = 9;

		$rot_ptr = $rot_ptr_set;

		$tmp = "";
		$txt = strrev($txt);
		$txt_len = strlen($txt);

		for ($i = 0; $i < $txt_len; $i++) {
			if ($rot_ptr >= $key_len)
				$rot_ptr = 0;
			$tmp .= $txt[$i] ^ $key[$rot_ptr];
			$v = ord($tmp[$i]);
			$tmp[$i] = chr(((($v << 3) & 0xf8) | (($v >> 5) & 0x07)));
			$rot_ptr++;
		}
		$tmp = base64_encode($tmp);
		$tmp = strrev($tmp);
		return $tmp;
	}
	public static function makeSqlWhere($where){
        $sql_where = '';
        foreach($where as $key => $value){
            $pos_ge = strpos($key, " >=");
            $pos_le = strpos($key, " <=");
            $pos_di = strpos($key, " !=");
            $pos_like = strpos($key, " like");
            if($pos_ge !== false){
                $key = str_replace(" >=", "", $key);
                $key = trim($key);
                $sql_where.=" AND {$key} >= '$value'";
            }elseif($pos_le !== false){
                $key = str_replace(" <=", "", $key);
                $key = trim($key);
                $sql_where.=" AND {$key} <= '$value'";
            }elseif($pos_like !== false){
                $key = str_replace(" like", "", $key);
                $key = trim($key);
                $sql_where.=" AND {$key} LIKE '%$value%'";
            }elseif($pos_di !== false){
                $key = str_replace(" !=", "", $key);
                $key = trim($key);
                $sql_where.=" AND {$key} != '$value'";
            }else{
                $key = trim($key);
                $sql_where.=" AND {$key} = '$value'";
            }
        }
        return $sql_where;
    }
}

?>