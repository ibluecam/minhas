<?php 
include_once("Helper.php");
include_once("Database.php");
include_once("User.php");
class Conversation{

	public static function getUsers($auth_member_no, $remember_token){
		include("../config/database_mobile.php");
		$rows = array();
		$isChatSupport = User::isChatSupport($auth_member_no, $remember_token);
		$where='';
		if(!$isChatSupport){
			$where.=" AND u.chat_support = 'Y' ";
		}
		$sql = "SELECT u.member_no, u.member_first_name, u.member_last_name, u.member_no, c.new_count1, c.new_count2, c.user1, c.user2 FROM iw_member as u
				LEFT JOIN iw_conversation as c ON c.user1 = u.member_no OR c.user2 = u.member_no
				WHERE u.member_no != $auth_member_no $where
				GROUP BY u.member_no
		";
	
		$con->set_charset("utf8");
		$result=$con->query($sql);
		while($row = $result->fetch_object()){
			if($row->new_count2==NULL) $row->new_count2=0;
			if($row->new_count1==NULL) $row->new_count1=0;
			if($row->user1!=NULL && $row->user2!=NULL){
				if($row->user1==$auth_member_no){
					/***COUNT oppsite side***/
					$row->new_count = $row->new_count2;
				}else{
					$row->new_count = $row->new_count1;
				}
			}else{
				$row->new_count = 0;
			}
			$rows[] = $row;
		}
		return $rows;
	}

	public static function countNewConversationMessages($auth_member_no, $receiver_no){
		include("../config/database_mobile.php");
		$sql = "SELECT COUNT(*) as new_count
				FROM iw_conversation_msg as cm
				LEFT JOIN iw_conversation as c ON c.id = cm.conversation_id
				WHERE ((c.user1 = $auth_member_no AND c.user2 = $receiver_no) OR (c.user1 = $receiver_no AND c.user2 = $auth_member_no)) AND cm.seen_dt IS NULL";
		$result = $con->query($sql);
		$row = $result->fetch_object();
		return $row->new_count;
	}
	public static function createConversationMessage($auth_member_no, $receiver_no, $conversation_text){
		include("../config/database_mobile.php");
		/***PREVENT SQL INJECTION***/
		$receiver_no=$con->real_escape_string($receiver_no);
		$auth_member_no=$con->real_escape_string($auth_member_no);
		$conversation_text=$con->real_escape_string($conversation_text);
		
		$conversation_id = Conversation::_createConversation($auth_member_no, $receiver_no);
		$sql = "INSERT INTO iw_conversation_msg 
					(author, conversation_id, conversation_text, created_dt) 
				VALUES
					({$auth_member_no}, {$conversation_id}, '{$conversation_text}', NOW())
			";
		$result = $con->query($sql);
		return $result;
	}
	/***Create first conversation if not exist***/
	public static function _createConversation($auth_member_no, $receiver_no){
		include("../config/database_mobile.php");
		$conversation = Conversation::_getConversation($auth_member_no, $receiver_no);
		if(count($conversation)<=0){
			$receiver_no=$con->real_escape_string($receiver_no);
			$auth_member_no=$con->real_escape_string($auth_member_no);

			$sql = "INSERT INTO iw_conversation (user1, user2, created_dt) VALUES ({$auth_member_no}, {$receiver_no}, NOW()) ";
			$result = $con->query($sql);
			return $con->insert_id;
		}else{
			return $conversation->id;
		}
	}
	/***Get conversation in the past***/
	public static function _getConversation($auth_member_no, $receiver_no){
		include("../config/database_mobile.php");
		$sql = "SELECT iw_conversation.* FROM iw_conversation WHERE (user1 = $auth_member_no AND user2 = $receiver_no) OR (user1 = $receiver_no AND user2 = $auth_member_no)";
		$result = $con->query($sql);
		$row = $result->fetch_object();
		return $row;
	}

	public static function getConversationMessages($auth_member_no, $receiver_no){
		include("../config/database_mobile.php");
		
		$receiver_no=$con->real_escape_string($receiver_no);
		$auth_member_no=$con->real_escape_string($auth_member_no);
		$rows = array();
		
		$sql = "SELECT cm.id as cm_id, cm.conversation_text, cm.author, cm.created_dt, u.member_first_name as member_name, c.user1, c.user2 FROM iw_conversation as c
			INNER JOIN iw_conversation_msg as cm ON cm.conversation_id = c.id
			INNER JOIN iw_member as u ON cm.author = u.member_no
			WHERE (c.user1 = {$auth_member_no} AND c.user2 = {$receiver_no}) OR (c.user1 = {$receiver_no} AND c.user2 = {$auth_member_no})
			ORDER BY cm.id DESC
			LIMIT 30
		";
		
		$con->set_charset("utf8");

		$result=$con->query($sql);
		while($row = $result->fetch_object()){
			$rows[] = $row;
		}
		$rows = array_reverse($rows);
		return $rows;
	}
	public static function getNewConversationMessages($auth_member_no, $receiver_no, $latest_id=0){
		include("../config/database_mobile.php");

		$receiver_no=$con->real_escape_string($receiver_no);
		$auth_member_no=$con->real_escape_string($auth_member_no);
		$latest_id = $con->real_escape_string($latest_id);
		$rows = array();
		
		$sql = "SELECT cm.id as cm_id, cm.conversation_text, cm.author, cm.created_dt, u.member_first_name as member_name, c.user1, c.user2 FROM iw_conversation as c
			INNER JOIN iw_conversation_msg as cm ON cm.conversation_id = c.id
			INNER JOIN iw_member as u ON cm.author = u.member_no
			WHERE ((c.user1 = {$auth_member_no} AND c.user2 = {$receiver_no}) OR (c.user1 = {$receiver_no} AND c.user2 = {$auth_member_no})) AND cm.id > {$latest_id}
		";
		
		$con->set_charset("utf8");

		$result=$con->query($sql);
		while($row = $result->fetch_object()){
			$rows[] = $row;
		}
		return $rows;
	}

	public static function insertSeenDate($auth_member_no, $receiver_no){
		include("../config/database_mobile.php");
		/***PREVENT SQL INJECTION***/
		$receiver_no=$con->real_escape_string($receiver_no);
		$auth_member_no=$con->real_escape_string($auth_member_no);
		
		$sql = "UPDATE iw_conversation_msg as cm
				INNER JOIN iw_conversation as c ON cm.conversation_id = c.id
				SET cm.seen_dt = NOW()
				WHERE ((c.user1=$auth_member_no AND c.user2=$receiver_no) OR (c.user1=$receiver_no AND c.user2=$auth_member_no))
					AND cm.author != $auth_member_no 
					AND cm.seen_dt IS NULL
			";
		$result = $con->query($sql);
		return $result;
	}

	public static function insertNewCount($auth_member_no, $receiver_no){
		include("../config/database_mobile.php");
		/***PREVENT SQL INJECTION***/
		$receiver_no=$con->real_escape_string($receiver_no);
		$auth_member_no=$con->real_escape_string($auth_member_no);
		
		$sql = "UPDATE iw_conversation as cm
				SET cm.new_count1 = CASE
					WHEN (cm.user1=$auth_member_no AND cm.user2=$receiver_no) THEN cm.new_count1 + 1 ELSE cm.new_count1
				END,
				cm.new_count2 = CASE
					WHEN (cm.user1=$receiver_no AND cm.user2=$auth_member_no) THEN cm.new_count2 + 1 ELSE cm.new_count2
				END
				WHERE (cm.user1=$auth_member_no AND cm.user2=$receiver_no) OR (cm.user1=$receiver_no AND cm.user2=$auth_member_no) 
			";
	
		$result = $con->query($sql);
		return $result;
	}

	public static function resetNewCount($auth_member_no, $receiver_no){
		include("../config/database_mobile.php");
		/***PREVENT SQL INJECTION***/
		$receiver_no=$con->real_escape_string($receiver_no);
		$auth_member_no=$con->real_escape_string($auth_member_no);
		
		$sql = "UPDATE iw_conversation as cm
				SET cm.new_count1 = CASE
					WHEN (cm.user1=$receiver_no AND cm.user2=$auth_member_no) THEN 0 ELSE cm.new_count1
				END,
				cm.new_count2 = CASE
					WHEN (cm.user1=$auth_member_no AND cm.user2=$receiver_no) THEN 0 ELSE cm.new_count2
				END
				WHERE (cm.user1=$auth_member_no AND cm.user2=$receiver_no) OR (cm.user1=$receiver_no AND cm.user2=$auth_member_no) 
			";
	
		$result = $con->query($sql);
		return $result;
	}
}

?>