<?php 

class Database{
	public static function insert($table, $data){
		include("../config/database_mobile.php");
		
		foreach($data as $key=>$value){
			$data[$key] = $con->real_escape_string($value);
		}

		$array_fields = array_keys($data);
		$array_values = array_values($data);
		$sql_fields = implode('`, `', $array_fields);
		$sql_values = implode("', '", $array_values);

		if(count($data)>0){
			$sql = "Insert Into {$table}(`".$sql_fields."`) Values('".$sql_values."' )";
		    return $con->query($sql);
		}
		return false;	
	}
	
}

?>