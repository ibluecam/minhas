<?php 
include_once("Helper.php");
include_once("Database.php");

class Car{
	
	public static function getOrderedCar($where, $limit_s=0, $limit_e=20){
		include("../config/database_mobile.php");
		$rows = array();
		$limit_s = $con->real_escape_string($limit_s);
		$limit_e = $con->real_escape_string($limit_e);

		foreach($where as $key=>$value){
			$where[$key] = $con->real_escape_string($value);
		}

		$sql_where = "WHERE 1 ";
		$sql_where.= Helper::makeSqlWhere($where);

		$sql = "SELECT iw_customer_inventory.order_dt, iw_bbs.*, iw_bbs_attach.raw_name, iw_bbs_attach.file_ext
			FROM iw_customer_inventory 
			LEFT JOIN iw_bbs ON iw_bbs.idx = iw_customer_inventory.stock_idx 
			LEFT JOIN iw_bbs_attach ON iw_bbs_attach.idx = iw_customer_inventory.stock_idx AND iw_bbs_attach.sort = 0
			{$sql_where}
			GROUP BY iw_bbs.idx
			";
		//$sql.=" LIMIT {$limit_s}, {$limit_e}";
		$member_no = $_POST['member_no'];

		$result=$con->query($sql);
		while($row = $result->fetch_object()){
			$row->image_name = "http://".$_SERVER['HTTP_HOST']."/uploads/product/".$row->raw_name."_150".$row->file_ext;
			$row->msg = (($row->idx==$row->idx AND $row->icon_status=="reserved" AND $row->customer_no!=$member_no) ? "This Car Reserved by other" : " " );

			$rows[] = $row;
		}

		return $rows;
	}

	public static function countCar($where){
		include("../config/database_mobile.php");
		$rows = array();
		foreach($where as $key=>$value){
			$where[$key] = $con->real_escape_string($value);
		}
		$sql_where = "WHERE 1 ";
		$sql_where.= Helper::makeSqlWhere($where);
		$sql = "SELECT COUNT(*) as num_rows FROM iw_bbs
			{$sql_where}";
		$result=$con->query($sql);
		$row = $result->fetch_object();	
		return $row->num_rows;
	}

	public static function orderCar($data){
		$data['order_dt']= date('Y-m-d H:i:s');
		$data['order_status']="order";
		return Database::insert("iw_customer_inventory", $data);
	}

	public static function countOrderedCar($where){
		include("../config/database_mobile.php");
		$rows = array();
		$num_rows = 0;
		foreach($where as $key=>$value){
			$where[$key] = $con->real_escape_string($value);
		}

		$sql_where = "WHERE 1 ";
		$sql_where.= Helper::makeSqlWhere($where);
		$sql = "SELECT COUNT(*) as num_rows FROM iw_customer_inventory {$sql_where}";
		$result=$con->query($sql);
		if($result){
			$rows = $result->fetch_object();
			$num_rows = $rows->num_rows;
		}
		return $num_rows;
	}
	public static function isReserved($idx){
		include("../config/database_mobile.php");
		$rows = array();
		$num_rows = 0;
		$idx = $con->real_escape_string($idx);
		$sql = "SELECT COUNT(*) as num_rows FROM iw_bbs WHERE idx='{$idx}' AND `icon_status` <> 'sale' AND `icon_status` <> 'order' ";
		$result=$con->query($sql);
		if($result){
			$rows = $result->fetch_object();
			$num_rows = $rows->num_rows;
			if($num_rows > 0){
				return true;
			}
		}
		return false;
	}
	public static function isOrdered($idx){
		include("../config/database_mobile.php");
		$rows = array();
		$num_rows = 0;
		
		if(isset($_POST['customer_no'])) $customer_no = $_POST['customer_no'];
		$idx = $con->real_escape_string($idx);
		$customer_no = $con->real_escape_string($customer_no);
		$sql = "SELECT COUNT(*) as num_rows FROM iw_bbs WHERE `idx`='{$idx}' AND `icon_status`='reserved' AND `customer_no`!='{$customer_no}' ";
		$result=$con->query($sql);
		if($result){
			$rows = $result->fetch_object();
			$num_rows = $rows->num_rows;
			if($num_rows > 0){
				return true;
			}
		}
		return false;
	}

	public static function cancelCarOrder($where){
		include("../config/database_mobile.php");
		$rows = array();
		$num_rows = 0;
		foreach($where as $key=>$value){
			$where[$key] = $con->real_escape_string($value);
		}

		$sql_where = "WHERE 1 ";
		$sql_where.= Helper::makeSqlWhere($where);
		$sql = "DELETE FROM iw_customer_inventory {$sql_where}";
		$result=$con->query($sql);
		if($result){
			return true;
		}
	}
}

?>