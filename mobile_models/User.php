<?php 
include_once("Helper.php");
include_once("Database.php");
class User{
	public static function isChatSupport($member_no, $remember_token){
		include("../config/database_mobile.php");
		$row = array();
		$member_no = $con->real_escape_string($member_no);
		$remember_token = $con->real_escape_string($remember_token);
		$sql = "SELECT chat_support FROM iw_member WHERE member_no = $member_no AND remember_token='$remember_token' ";
		$result=$con->query($sql);
		$row = $result->fetch_object();
		if($row->chat_support=='Y'){
			return true;
		}
	}
	public static function getGradeNo($member_no, $remember_token){
		include("../config/database_mobile.php");
		$row = array();
		$member_no = $con->real_escape_string($member_no);
		$remember_token = $con->real_escape_string($remember_token);
		$sql = "SELECT grade_no FROM iw_member WHERE member_no = $member_no AND remember_token='$remember_token' ";
		$result=$con->query($sql);
		$row = $result->fetch_object();
		return $row->grade_no;
	}
	
	public static function getUser($where, $select_fields=array('iw_member.*')){
		include("../config/database_mobile.php");
		$rows = array();
		foreach($where as $key=>$value){
			$where[$key] = $con->real_escape_string($value);
		}
		$sql_where = "WHERE 1 ";
		$sql_where.= Helper::makeSqlWhere($where);
		$select_fields_sql = implode(", ", $select_fields);
		$sql = "SELECT {$select_fields_sql}, iw_country_list.country_name FROM iw_member
			LEFT JOIN iw_country_list ON iw_country_list.cc = iw_member.member_country
			{$sql_where}";
		$con->set_charset("utf8");
		$result=$con->query($sql);
		while($row = $result->fetch_object()){
			unset($row->member_pwd);
			$rows[] = $row;
		}
		return $rows;
	}
	public static function registerUser($data){
		include("../config/database_mobile.php");
		/*****START DEFAULT PARAMETERS*****/
		$data['grade_no'] = 11;
		$data['verification'] = User::createVerificationCode($data['member_id']);
		$data['created_dt'] = date('Y-m-d h:i:s');
		/*****END DEFAULT PARAMETERS*****/
		if(isset($data['member_pwd'])){
			$data['member_pwd'] = Helper::func_base64_encode($data['member_pwd']);
		}
		unset($data['cmember_pwd']);
		/*****START SECURE FROM MYSQL INJECTION*****/
		foreach($data as $key=>$value){
			$data[$key] = $con->real_escape_string($value);
		}
		/*****END SECURE FROM MYSQL INJECTION*****/
		$array_fields = array_keys($data);
		$array_values = array_values($data);
		$sql_fields = implode('`, `', $array_fields);
		$sql_values = implode("', '", $array_values);

		if(isset($data['member_id']) && isset($data['member_pwd']) && isset($data['email']) && isset($data['member_first_name']) && isset($data['member_last_name']) && isset($data['member_country']) && isset($data['mobile_no1']) && isset($data['mobile_no2'])){
			$sql = "Insert Into iw_member(`".$sql_fields."`) Values('".$sql_values."' )";
			User::sendVerificationEmail($data['email'], $data['verification'], $data['member_id'], $data['member_first_name']." ".$data['member_last_name']);
		    return $con->query($sql);
		}
		return false;
	}
	public static function login($data, $remember_me){
		include("../config/database_mobile.php");
		$rows = array();
		$member_id=$con->real_escape_string($data['member_id']);
		$member_password=$con->real_escape_string($data['member_pwd']);
		$encoded_password = Helper::func_base64_encode($member_password);
		$sql = "SELECT * FROM iw_member WHERE member_id='{$member_id}' AND member_pwd='{$encoded_password}'";
		$con->set_charset("utf8");
		$result=$con->query($sql);
		$rows = $result->fetch_object();
		if(count($rows)>0){
			/***** Insert remember token only when there is no previous token else get previous token*****/
			if(empty($rows->remember_token)){
				$generatedRememberToken = User::generateRememberToken($rows->member_id);
				User::saveRememberToken($rows->member_no, $generatedRememberToken);
				$result=$con->query($sql);
				$rows = $result->fetch_object();
			}
		}

		if(!empty($member_id) && !empty($member_password)){
			return $rows;
		}else{
			return array();
		}
		
	}
	public static function saveRememberToken($member_no, $remember_token){
		include("../config/database_mobile.php");
		$sql = "UPDATE iw_member SET remember_token = '{$remember_token}' WHERE member_no='{$member_no}'";
		$result=$con->query($sql);
	}
	public static function validateRegister($data){
		$result = array();
		
		/*****Validate member id format*****/
		if(!preg_match("/^[a-z\d_]{4,20}$/i", $data['member_id'])){
			$code['error_member_id'] = 1;
			$result['error_member_id'] = "Member ID must be alphanumeric and dot characters, from 4 to 20 characters long";
		}
		/*****Check if member id exist*****/
		if(count(User::selectMemberByMemberId($data['member_id']))>0){
			$code['error_member_id'] = 1;
			$result['error_member_id'] = "Member ID already existed";
		}
		/*****Check password length*****/
		if(strlen($data['member_pwd'])<5 || strlen($data['member_pwd'])>20){
			$code['error_member_pwd'] = 3;
			$result['error_member_pwd'] = "Password must be 5 to 20 characters long";
		}
		/*****Check email format or if existed *****/
		if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
			$code['error_email'] = 4;
			$result['error_email'] = "Invalid Email";
		}elseif(User::existedEmail($data['email'])){
			$code['error_email'] = 42;
			$result['error_email'] = "Email already existed";
		}
		/*****Check member first name*****/
		if(!preg_match("/^[\p{L} ]+$/u", $data['member_first_name'])){
			$code['error_member_first_name'] = 5;
			$result['error_member_first_name'] = "First Name must be alphanumeric";
		}
		/*****Check member last name*****/
		if(!preg_match("/^[\p{L} ]+$/u", $data['member_last_name'])){
			$code['error_member_last_name'] = 6;
			$result['error_member_last_name'] = "Last Name must be alphanumeric";
		}
		/*****Check if gender is valid*****/
		// if(!in_array($data['sex'], array('M', 'W'))){
		// 	$code['error_sex'] = 7;
		// 	$result['error_sex'] = "Invalid gender";
		// }
		/*****Check if country valid*****/
		if(count(User::selectCountryByCc($data['member_country']))<=0){
			$code['error_member_country'] = 8;
			$result['error_member_country'] = "Invalid country";
		}
		/*****Check if mobile number valid*****/
		if(!empty($data['mobile_no1'])||!empty($data['mobile_no2'])){
			if(!preg_match("/^[0-9]+$/", $data['mobile_no1'])
				||!preg_match("/^[0-9]+$/", $data['mobile_no2'])
				){
				$code['error_mobile_no'] = 9;
			   	$result['error_mobile_no'] = "Mobile Number must be only numbers";
			} 
		}
		$first_error = current(array_keys($result));
		$i=0;
		
		foreach($result as $key=>$value){
			$i++;
			if($first_error==$key){
				$result['error_msg']=$result[$first_error];
				$result['error_code']=$code[$first_error]; 
			}
		}
		
		/*****Give result*****/
		if(count($result)>0){
			$result['success'] = 0;
		}else{
			$result['success'] = 1;
		}
		
		return $result;
	}
	public static function existedEmail($email){
		include("../config/database_mobile.php");
		$email = $con->real_escape_string($email);
		$sql = "SELECT COUNT(*) as num_rows FROM iw_member WHERE email='{$email}'";
		$result=$con->query($sql);
		$rows = $result->fetch_object();
		if($rows->num_rows>0){
			return true;
		}
	}
	public static function selectCountryByCc($cc){
		include("../config/database_mobile.php");
		$sql = "SELECT * FROM iw_country_list WHERE cc='{$cc}'";
		$result=$con->query($sql);
		$rows = $result->fetch_object();
		return $rows;
	}
	public static function selectMemberByMemberId($member_id){
		include("../config/database_mobile.php");
		$member_id = $con->real_escape_string($member_id);
		$sql = "SELECT * FROM iw_member WHERE member_id='{$member_id}'";
		$result=$con->query($sql);
		$rows = $result->fetch_object();
		return $rows;
	}
	public static function generateRememberToken($member_id){
		$strtime = strtotime("now");
		$remember_token = crypt($member_id.$strtime, '$2y$07$h7jrovmeotydh46trhfygy$');
        return $remember_token;
	}	

	public static function authenticated($member_no, $remember_token){
		include("../config/database_mobile.php");
		$member_no = $con->real_escape_string($member_no);
		$remember_token= $con->real_escape_string($remember_token);
		$sql = "SELECT * FROM iw_member WHERE remember_token='{$remember_token}' AND member_no='$member_no'";
	
		$result = $con->query($sql);
		$rows = $result->fetch_object();
		if(count($rows)>0){
			return true;
		}
	}

	public static function createVerificationCode($member_id){
        $string = md5($member_id."+".strtotime("now"));
        $data['verification']=$string;
        //$this->db->where("member_no", $member_no);
        //$result = $this->db->update("iw_member", $data);
        return $string;
    }

    public static function sendVerificationEmail($email, $vCode, $user_id, $name){
    	require '../libraries/PHPMailer/PHPMailerAutoload.php';

		$mail = new PHPMailer;

		//$mail->SMTPDebug = 3;                               // Enable verbose debug output

		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'ssl://kangaroo.arvixe.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'noreply@iblauda.com';                 // SMTP username
		$mail->Password = '#Kx4Qh_]})eh';                           // SMTP password
		$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 465;                                    // TCP port to connect to

		$mail->From = 'noreply@iblauda.com';
		$mail->FromName = 'IBLAUDA';
		$mail->addAddress($email, $name);     // Add a recipient
		// $mail->addAddress('ellen@example.com');               // Name is optional
		$mail->addReplyTo('noreply@iblauda.com', 'noreply@iblauda.com');
		// $mail->addCC('cc@example.com');
		// $mail->addBCC('bcc@example.com');

		// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		$mail->isHTML(true);                                  // Set email format to HTML
		$html = '<html><head></head><body>';
        $html.= 'Hello '.$name.'!<br/><br/>
        		Your have recently signed up with your email at iblauda.com.<br/>
        		Your Member ID: '.$user_id.'<br/><br/>You need just one more step to complete your registration. Visit the link below to activate your account now.<br/>
        		<a href="http://iblauda.com/?c=user&m=verifyAccount&vcode='.$vCode.'"/>http://iblauda.com/?c=user&m=verifyAccount&vcode='.$vCode.'</a><br/><br/>
     			Once you have visited it you will be able to login at our site.<br/><br/>
        		Thank you for joining <a href="http://iblauda.com/">IBLAUDA.com</a>.';
      
		$mail->Subject = 'IBLAUDA.com: Account verification';
		$mail->Body    = $html;
		
		
        $html.= '</body></html>';
		if(!$mail->send()) {
		    return false;
		} else {
		    return true;
		}
    }
}

?>