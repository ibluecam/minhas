
                                <div class="sidebar-1">

                                    <nav class="top-bar top-sidebar-1" data-topbar role="navigation">
                    
                                      <section class="top-bar-section">
                                        <!-- Right Nav Section -->
                                         <div class="bar_menu warpper_bar_plus">
                                            <ul class="left">
                                                <li class="sidebar-search-by-brand">
                                                <div class="detail-data-left-search-by-brand"><img src="images/user/detail_data_left.png"></div>
                                                <p class="title-search-by-brand">SEARCH BY BRAND</p></li>
                                                <li><i class="fa fa-plus icon-plus-sidebar-1 add-class-icon-plus"></i> <i class="fa fa-minus icon-minus-sidebar-1 add-class-icon-minus"></i></li>
                                            </ul>
                                          </div>
                                      </section>
                                    </nav>
                                    <div class="list-data-sidebar-1">

                                        <ul class="side-nav">
                                           <?php foreach($get_brand as $make){ ?>
                                              <li><a href="?c=user&m=carlist&car_make=<?php echo $make->make_name; ?>&page=1" class="searchbycarbrand_<?php echo $make->id; ?>" id="<?php echo $make->id; ?>" a="<?php echo $make->make_name; ?>">
                                              <img class="margin_imag" src="<?php echo $make->icon_name; ?>"/><?php echo $make->make_name;  ?> <span>(<?php echo $make->c_brand; ?>)</span></a></li>
                                            <?php }; ?>
                                          </ul>
                                    </div>
                                 </div><!-- End sidebar 1-->

                                 <div class="sidebar-2">                 
                                      <nav class="top-bar top-sidebar-1" data-topbar role="navigation">
                                          <section class="top-bar-section">
                                            <!-- Right Nav Section -->
                                          <div class="bar_type">
                                            <ul class="left">
                                              <li class="sidebar-search-by-brand">
                                                <div class="detail-data-left-search-by-brand"><img src="images/user/detail_data_left.png"></div>
                                                <p class="title-search-by-brand">SEARCH BY TYPE</p> 
                                               </li>
                                              <li> 
                                                  <i class="fa fa-plus icon-plus-sidebar-2 add-class-icon-plus"></i> 
                                                  <i class="fa fa-minus icon-minus-sidebar-2 add-class-icon-minus"></i>
                                              </li>
                                            </ul>
                                          </div>
                                          </section>
                                      </nav>
                                      
                                      <div class="list-data-sidebar-2">

                                          <ul class="side-nav">

                                            <?php foreach($getbody_type as $getbodytype){ ?>
                                              <li><a href="?c=user&m=carlist&car_body_type=<?php echo $getbodytype->body_name; ?>&page=1" class="searchbycartype_<?php echo $getbodytype->id; ?>" id="<?php echo $getbodytype->id; ?>" a="<?php echo $getbodytype->body_name; ?>">
                                              <img class="margin_imag" src="https://res.cloudinary.com/softbloom/image/upload/v1435028219/logo/bt/<?php echo $getbodytype->icon; ?>"/><?php echo $getbodytype->body_title;  ?> <span>(<?php echo $getbodytype->countbodytype; ?>)</span></a></li>
                                            <?php }; ?>

                                            </ul>
                                      </div>

                                 </div><!-- End sidebar 2-->

                                 <div class="sidebar-3" style="margin-bottom:30px;">
                                   <div id="fb-root">
                                      <div class="fb-page" data-href="https://www.facebook.com/sb.motorbb" data-height="410" data-width="263" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true">
                                          <div class="fb-xfbml-parse-ignore">
                                              <blockquote cite="https://www.facebook.com/sb.motorbb">
                                                <a href="https://www.facebook.com/sb.motorbb" id="getpagefb"></a>
                                              </blockquote>
                                          </div>
                                      </div>
                                   </div>
                                 </div>
 