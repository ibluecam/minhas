<?php include('header.php'); ?>

<!-- Slideshow -->
        <div class="row rw-slideshow">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <!--<ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
     <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>-->

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="https://res.cloudinary.com/softbloom/image/upload/v1435023718/slide/slide" alt="Hello">
      <div class="carousel-caption">

      </div>
    </div>

    <div class="item">
      <img src="https://res.cloudinary.com/softbloom/image/upload/v1435023724/slide/slide2" alt="Hello2">
      <div class="carousel-caption">

      </div>
    </div>

     <div class="item">
      <img src="https://res.cloudinary.com/softbloom/image/upload/v1435023731/slide/slide3" alt="Hello2">
      <div class="carousel-caption">
      </div>
    </div>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

        </div><!-- End Slideshow -->


      </header>


      <div class="content">



            <div class="row main-content-data">
              <div class="large-11 large-centered columns top-slideshow-site">
                    <div class="row">
                        <div class="large-12 columns top-slideshow-site-1">

                             <div class="large-4 columns sidebar-left-site"> <!-- Sidebar -->

                                  <?php include('sidebar.php'); ?>

                             </div><!-- End Sidebar -->


                            <div class="large-8 columns main-data-content-blog"><!-- Content -->
                                    <!-- Component Search -->
                                        <?php
                                        if(isset($hide_search_box)){
                                           if($hide_search_box==false){
                                            include('componentsearch.php');
                                          }
                                        }
                                       else{
                                            include('componentsearch.php');
                                        }
                                        ?>
                                    <!-- End Component Search -->














	<?php //=========================================== Content include ========================================== ?>

  								<div class="detail-data-box-all">
  									<?php echo $content; ?>

                    

                  </div>
  	<?php //=========================================== Content ========================================== ?>



						</div> <!-- End Content-->
                        </div>
                    </div>

              </div>
            </div>
        </div>


  <?php include('footer.php'); ?>



