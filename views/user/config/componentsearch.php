       <div id="center-quick-search-container">                             
                                   <input type="hidden" value="<?php if(isset($carmodeltodropdownsearchs['car_model'])) echo $carmodeltodropdownsearchs['car_model']; ?>" id="searched_model">
                                     <div class="large-7 columns search-by-car-site">

                                        <div class="large-12 columns head-search-by-car-site">
                                      	  <div class="detail-data-left-search"><img src="images/user/detail_data_left.png"></div>
                                          <p class="title-search-by-car">SEARCH BY CARS</p>
                                         </div>
                                        <i class="fa fa-plus icon-plus-sidebar-3 add-class-icon-plus-3"></i> 
                                        <i class="fa fa-minus icon-minus-sidebar-3 add-class-icon-minus-3"></i>

                                        <div class="list-data-sidebar-3">
                                        <form action="/?c=user&m=carlist" method="POST">    
                                        <div class="large-7 columns select-form-search-by-car">

                                             <select class="styleforform" id="carmake" name="carmake">
                                              <option  value="">Make</option>
                                              <?php if(count($carmaketodropdownsearch) > 0){ foreach($carmaketodropdownsearch as $carmaketodropdownsearchs){ ?>
                                                <option value="<?php echo $carmaketodropdownsearchs->car_make; ?>"<?php if(isset($_GET['carmake'])){ if($_GET['carmake']==$carmaketodropdownsearchs->car_make){ echo "selected";}}?> >
                                                <?php echo $carmaketodropdownsearchs->car_make; ?></option>
                                              <?php }} ?>
                                            </select>

                                            <select style="overflow:auto" class="styleforform" id="carmodel" name="carmodel">
                                                <option value="">Model</option>
                                                <?php if(isset($model_list)){ foreach ($model_list as $model) {  ?>
                                                      <option <?php if(isset($_GET['carmodel'])){ if($_GET['carmodel']==$model->car_model){ ?> Selected <?php } } ?>  value="<?php echo $model->car_model; ?>"><?php echo $model->car_model; ?></option>
                                                 <?php } }  ?>
                                            </select>
                                           
                                    

                                            <input class="styleforform_text" type="text" value="<?php echo $keyword= (isset($_POST['keyword'])!="" ?  $_POST['keyword']:''); ?>" name="keyword" id="keyword" placeholder="Keywords" />

                                         
                                        </div>

                                        <div class="large-3 columns text1-form-search-by-car">

                                            <select class="styleforform" id="carminyear" name="searchminyear">
                                              <option value="">Min Year</option>
                                             <?php 
                                								for($i=date("Y"); $i>=1990; $i--){
                                              ?>
                                            <option <?php if(isset($_GET['carminyear'])){ if($_GET['carminyear']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                              <?php
                                							  }
								                              ?>
                                            </select>
                                         
                                            <input class="styleforform_text" type="text" id="carminprice" value="<?php echo $searchminprice= (isset($_POST['searchminprice'])!="" ?  $_POST['searchminprice']:''); ?>" name="searchminprice" placeholder="Min Price" >

                                             <select class="styleforform" name="searchcountry" id="carcountry">
                                              <option value="">Country</option>
                                              <?php foreach($carcountrytodropdownsearch as $carcountrytodropdownsearchs){ ?>
                                              <option <?php if(isset($_GET['carcountry'])){ if($_GET['carcountry']==$carcountrytodropdownsearchs->cc){ ?> Selected <?php } } ?> value="<?php echo $carcountrytodropdownsearchs->cc; ?>"><?php echo $carcountrytodropdownsearchs->com; ?></option>
                                              <?php } ?>
                                            </select>
                                        </div>

                                        <div class="large-3 columns text2-form-search-by-car">
                                             <select class="styleforform" name="searchmaxyear" id="carmaxyear">
                                              <option value="">Max Year</option>
                                              <?php 
                                      						for($i=date("Y"); $i>=1990; $i--){
                                              ?>

                                                <option <?php if(isset($_GET['carmaxyear'])){ if($_GET['carmaxyear']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>

                                              <?php 
                                      						}
                                    					?>

                                              

                                            </select>

                 
                                            <input class="styleforform_text" type="text" value="<?php echo $searchmaxprice= (isset($_POST['searchmaxprice'])!="" ?  $_POST['searchmaxprice']:''); ?>" name="searchmaxprice" id="carmaxrpice" placeholder="Max Price">

                                            <!--<a class="button btn-search" id="clicksearchdata">Search</a>-->
                                            <input style="width: 102px; height: 28px" type="submit" class="button btn-search" value="Search" name="searchboxcar">
                                        </div>
                                        </form>
                                    </div>
                                  </div>

                                    <div class="large-5 columns search-by-country-site">
                                     
                                      <div class="large-12 columns head-search-by-country-site">
                                        <div class="detail-data-left-search-2"><img src="images/user/detail_data_left.png"></div>
                                        <p class="title-search-by-country">SEARCH BY COUNTRY</p> 
                                      </div>

                                        <i class="fa fa-plus icon-plus-sidebar-4 add-class-icon-plus-4"></i> 
                                        <i class="fa fa-minus icon-minus-sidebar-4 add-class-icon-minus-4"></i>

                                        <div class="large-12 columns data-search-by-country">
                                            <div class="large-6 columns country-flag-1">
                                               <a href="?c=user&m=carlist&country=jp&page=1" id="jp"><img src="https://res.cloudinary.com/softbloom/image/upload/v1435028554/logo/cn/ja">
                                               JAPAN</a>
                                                <p class="number-search-by-country"><?php echo $countcarjapan; ?> cars</p>
                                            </div>
                                            <div class="large-6 columns country-flag-2">
                                              <a href="?c=user&m=carlist&country=kr&page=1" id="kr"><img src="https://res.cloudinary.com/softbloom/image/upload/v1435028555/logo/cn/ko">
                                                KOREA</a>
                                                <p class="number-search-by-country"><?php echo $countcarkorea; ?> cars</p>   
                                            </div>
                                        </div> 
                                    </div>



                                    
                                    
</div>   

