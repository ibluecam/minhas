<div class="large-12 columns main-detail-data"> 
    <div class="large-12 columns detail-data">
    <div class="detail-data-left"><img src="images/user/detail_data_left.png"></div>
        <p>TOYOTA</p>
        <a href="#"><div class="detail-data-right"><p1>SEE MORE</p1> &nbsp;&nbsp;<img src="images/user/see_more.png"></div></a>
    </div> 



    <div class="large-12 columns detail-data-box"><!-- line 1 -->

      <?php foreach ($get_all_car as $row) { ?>
         <div class="large-3 columns main-item-car">
             <a href=""><img src="<?php echo ($row->fileimgname!='' ? "uploads/product/".$row->fileimgname : "/images/user/no_image.png"); ?>" class="home_image" /></a>
            <div class="large-12 columns link-item-cars">
              <a href="#"><?php echo $row->manu_year ." ". $row->car_make; ?></a>
            </div>
            <div class="large-6 columns title-item-car">
                  <p>Country :</p>
                  <p>ChassisNo :</p>
                  <p>Mileage :</p>
            </div>
            <div class="large-6 columns data-detail-item-car">
                  <p><?php echo ($row->car_country!='' ? $row->car_country: 'N/A'); ?></p>
                  <p><?php echo ($row->car_chassis_no!='' ? $row->car_chassis_no: 'N/A'); ?></p> 
                  <p><?php echo ($row->car_mileage!='' ? $row->car_mileage: 'N/A'); ?></p>
            </div>

            <div class="large-12 columns detail-price-item-car">
                <p>FOB : <?php echo number_format($row->unit_price) ?></p>
            </div>
        </div>
      <?php } ?>

    </div><!-- End line 1 -->

     <div class="large-12 columns paginater-site">
         <div id="pagination">
              <ul class="tsc_pagination">
                <!-- Show pagination links -->
                <?php foreach ($links as $pagelink) {
                    echo "<li>".$pagelink."</li>";
                } ?>
              </ul>
          </div>
      </div>
   <!-- blog pagination -->

      <!-- <div class="large-12 columns paginater-site">
          <ul class="pagination" role="menubar" aria-label="Pagination">
            <li class="arrow unavailable" aria-disabled="true"><a href="">&laquo; Previous</a></li>
            <li class="current"><a href="">1</a></li>
            <li><a href="">2</a></li>
            <li><a href="">3</a></li>
            <li><a href="">4</a></li>
            <li class="unavailable" aria-disabled="true"><a href="">&hellip;</a></li>
            <li><a href="">12</a></li>
            <li><a href="">13</a></li>
            <li class="arrow"><a href="">Next &raquo;</a></li>
          </ul>
      </div> -->
    <!-- end blog pagination -->
</div>
