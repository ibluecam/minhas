

  <?php //=========================================== Content include ========================================== ?>







                                <div class="large-12 columns main-about-data-area">
                                    <div class="large-12 columns detail-aboutus-data">
                                    <div class="detail-aboutus-data-left-area"><img src="images/user/detail_data_left.png"></div>
                                        <p class="title-aboutus-by-about">ABOUT US</p>
                                    </div>

                                    <!-- Images for about us -->
                                    <div class="img-info-aboutus">
                                    	<img src="images/user/ww.jpg" />
                                    </div>  <!-- Images for about us -->
 
 									<div class="large-12 columns" style="padding:0">
									<div class="large-8 columns" style="padding:0">
                                    <div class="large-12 columns blog-aboutus-data-1">
                                    	<h4 class="aboutus-title-1">Company Information</h4>
                                    	<p class="para-about-info-1">
                                        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MotorBB is automobile trading companies whose headquarter is located in Kawasaki, Japan.We are exporting cars from South Korea and Japan to Kenya, Myanmar, Republic of Dominica and Paraguay. Once a customer buys our automobile, it is sent to the customer in the shortest time possible through our smooth and prompt procedures.  
 We support customer who wants to buy our cars 24 hours / 365 days. We perform thorough inspection for each automobile, so you never have to worry the quality and condition of the automobile, and will be surely satisfied with your automobile. We never stop making efforts for providing our customers with better services, and keep pursuing higher customer satisfaction!
 											<div class="about_us_certificate">See our certificate from Japanese Government:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="images/user/aboutus/icon_pdf.png" border="0">&nbsp;&nbsp; 
                                            <a href="uploads/file/certificate of Motorbb from Japanese government.pdf" target="_blank">Click to download.</a></div>
                                        </p>
                                    </div>


                                     <div class="large-12 columns blog-aboutus-data-2">
                                     
                                    	<h4 class="aboutus-title-2">Company Outline</h4>
                                    	
                                    	<!--block description start-->
                                        <div class="columns detail-block-about-us">
                                        
                                        <div class="large-12 columns comment-about-us">
	                                        
	                                        <div class="large-12 columns spec">
	                                           <div class="large-6 columns left">Company Name:</div>
	                                           <div class="large-6 columns right">MOTORBB CO., LTD</div>
	                                       	</div>
                                       
	                                        <div style=" background-color: #e6f1f8;" class="large-12 columns spec">
		                                        <div class="large-6 columns left blue">Headquarter (Japan):</div>
		                                        <div class="large-6 columns right blue">Ark Hills Front Tower17F, 2-23-1 Akasaka, Minato-ku, Tokyo, Japan</div>
	                                        </div>

	                                      
	                                       
	                                        <div style=" background-color: #e6f1f8;" class="large-12 columns spec">
		                                        <div class="large-6 columns left blue">Branch (Cambodia):</div>
		                                        <div class="large-6 columns right blue">#B26, St.1019 Sangkat Toek Thlar, Khann Sensok Phnom Penh, Cambodia</div>
		                                    </div>
	                                       
	                                        <div class="large-12 columns spec">
		                                        <div class="large-6 columns left ">PhoneNumber (Japan):</div>
		                                        <div class="large-6 columns right phone_aboutus ">
                                                <img src="images/user/icon_skype_aboutus.png" class="aboutus_image" sty />
                                                &nbsp;+(81)-3-5575-7557</div>
	                                        </div>

	                                       
	                                       
	                                        <div class="large-12 columns spec">
		                                        <div class="large-6 columns left">PhoneNumber (Cambodia):</div>
		                                        <div class="large-6 columns right phone_aboutus">
                                                <img src="images/user/icon_skype_aboutus.png" class="aboutus_image" sty />
                                                &nbsp;+855-16-4040-32</div>
	                                        </div>
	                                       
	                                        <div class="large-12 columns spec">
		                                        <div class="large-6 columns left blue">Foundation:</div>
		                                        <div class="large-6 columns right blue">June/2015</div>
	                                        </div>
	                                       
	                                       
	                                       
	                                        <div class="large-12 columns spec">
		                                        <div class="large-6 columns left blue">Business description:</div>
		                                        <div class="large-6 columns right blue">Web Service</div>
	                                        </div>
											
											<div class="large-12 columns spec">
		                                        <div class="large-6 columns left blue">Email : <a href='mailto:info@motorbb.com'>info@motorbb.com</a></div>
		                                        <div class="large-6 columns right blue">Skype : <a href='skype:kenta.motorbb?add'>KENTA</a></div>
	                                        </div>
                                       
                                      
                                     
                                       
                                     
                                    
                                         </div>
                                     	 </div>
                                    	<!--block description end-->

                                    </div>

									</div>
                                    <div class="large-4 columns block-img-aboutus">
                                    <img src="/images/user/aboutus/aboutus1.jpg"/>
                                    <img src="/images/user/aboutus/aboutus2.jpg"/>
                                    </div>
                                    </div>




                                    <div class="large-12 columns">
	                                    <div class="large-12 columns title-team-list">
	                                    	<h5>Sales Team</h5>
	                                    </div>
	                                    <div class="large-12 columns title-team-profile">
	                                    	<p>English</p>
	                                    </div>

	                               	<div class="large-12 columns main-list-team-profile">
                                    	<div class="large-4 columns list-team-profile">
                                    			<div class="large-6 columns photo-profile">
                                    				<img src="images/user/sell/kenta.png">
                                    			</div>

                                    			<div class="large-6 columns profile-item">
                                    					<p class="name">Mr.Kenta</p>
                                    					<p class="phone">+855-16-4040-32</p>
                                    					<p class="email">kenta@motorbb.com</p>
                                    					<p class="media"><a href="skype:kenta.motorbb?add"><img src="images/user/skype.png"></a> <a href="https://www.facebook.com/kenta.motorbb"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>
                                    			</div>
                                    	</div>
                                    	<div class="large-4 columns list-team-profile">
                                    			<div class="large-6 columns photo-profile">
                                    				<img src="images/user/sell/Muoykea.png">
                                    			</div>

                                    			<div class="large-6 columns profile-item">
                                    					<p class="name"> Ms.Muoykea</p>
                                    					<p class="phone">+855-81-3131-56</p>
                                    					<p class="email">Muoykea@motorbb.com</p>
                                    					<p class="media"><a href="skype:muoykea.motorbb?add"><img src="images/user/skype.png"></a> <a href="https://www.facebook.com/profile.php?id=100007498877979"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>

                                    			</div>
                                    	</div>	
                                    	<div class="large-4 columns list-team-profile">
                                    			<div class="large-6 columns photo-profile">
                                    				<img src="images/user/sell/thorng.png">
                                    			</div>

                                    			<div class="large-6 columns profile-item">
                                    					<p class="name">Ms.Thorng</p>
                                    					<p class="phone">+855-81-3131-06</p>
                                    					<p class="email">thorng@motorbb.com</p>
                                    					<p class="media"><a href="skype:thorng.motorbb?add"><img src="images/user/skype.png"></a> <a href="https://www.facebook.com/profile.php?id=100006621225249"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>

                                    			</div>
                                    	</div>
                                   	</div>

                                   	<div class="large-12 columns main-list-team-profile">
                                    	<div class="large-4 columns list-team-profile">
                                    			<div class="large-6 columns photo-profile">
                                    				<img src="images/user/sell/heap.png">
                                    			</div>

                                    			<div class="large-6 columns profile-item">
                                    					<p class="name">Ms.Heap</p>
                                    					<p class="phone">+855-81-3131-94</p>
                                    					<p class="email">heap@motorbb.com</p>
                                    					<p class="media"><a href="skype:heap.motorbb?add"><img src="images/user/skype.png"></a> <a href="https://www.facebook.com/profile.php?id=100009187458777"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>

                                    			</div>
                                    	</div>
                                    	<div class="large-4 columns list-team-profile">
                                    			<div class="large-6 columns photo-profile">
                                    				<img src="images/user/sell/veasna.png">
                                    			</div>

                                    			<div class="large-6 columns profile-item">
                                    					<p class="name">Ms.Veasna</p>
                                    					<p class="phone">+855-81-3131-53</p>
                                    					<p class="email">veasna@motorbb.com</p>
                                    					<p class="media"><a href="skype:veasna.motorbb?add"><img src="images/user/skype.png"></a> <a href="https://www.facebook.com/profile.php?id=100005065065280"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>

                                    			</div>
                                    	</div>	
                                    	<div class="large-4 columns list-team-profile">
                                    			<div class="large-6 columns photo-profile">
                                    				<img src="images/user/sell/thim.png">
                                    			</div>

                                    			<div class="large-6 columns profile-item">
                                    					<p class="name">Ms.Thim</p>
                                    					<p class="phone">+855-81-3131-05</p>
                                    					<p class="email">thim@motorbb.com</p>
                                    					<p class="media"><a href="skype:thim.motorbb?add"><img src="images/user/skype.png"></a> <a href="https://www.facebook.com/thim.motorbb"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>

                                    			</div>
                                    	</div>
                                    </div>



                                    <div class="large-12 columns main-list-team-profile">
                                        <div class="large-4 columns list-team-profile">
                                                <div class="large-6 columns photo-profile">
                                                    <img src="images/user/sell/moni.png">
                                                </div>

                                                <div class="large-6 columns profile-item">
                                                        <p class="name">Ms.Moni</p>
                                                        <p class="phone">+855-81-3131-64</p>
                                                        <p class="email">moni@motorbb.com</p>
                                                        <p class="media"><a href="#"><img src="images/user/skype.png"></a> <a href="#"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>

                                                </div>
                                        </div>

                                         <div class="large-4 columns list-team-profile">
                                                <div class="large-6 columns photo-profile">
                                                    <img src="images/user/sell/may.png">
                                                </div>

                                                <div class="large-6 columns profile-item">
                                                        <p class="name">Ms.May</p>
                                                        <p class="phone">+855-96-299-8906</p>
                                                        <p class="email">may@motorbb.com</p>
                                                        <p class="media"><a href="#"><img src="images/user/skype.png"></a> <a href="#"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>

                                                </div>
                                        </div>


                                    </div>




                                    </div>



                                   <div class="large-12 columns">
	                              
	                                    <div class="large-12 columns title-team-profile">
	                                    	<p>Spanish</p>
	                                    </div>

	                               	<div class="large-12 columns main-list-team-profile">
                                    	<div class="large-4 columns list-team-profile">
                                    			<div class="large-6 columns photo-profile">
                                    				<img src="images/user/sell/diego.png">
                                    			</div>

                                    			<div class="large-6 columns profile-item">
                                    					<p class="name">Diego</p>
                                    					<p class="phone">(+51)1-397-5721</p>
                                    					<p class="email">diego@blauda.com</p>
                                    					<p class="media"><a href="#"><img src="images/user/skype.png"></a> <a href="#"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>

                                    			</div>
                                    	</div>
										
										<div class="large-4 columns list-team-profile">
                                    			<div class="large-6 columns photo-profile">
                                    				<img src="images/user/sell/victor.png">
                                    			</div>

                                    			<div class="large-6 columns profile-item">
                                    					<p class="name">Victor</p>
                                    					<p class="phone">(+51)1-397-5721</p>
                                    					<p class="email">victor@blauda.com</p>
                                    					<p class="media"><a href="#"><img src="images/user/skype.png"></a> <a href="#"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>

                                    			</div>
                                    	</div>
										
										
										<div class="large-4 columns list-team-profile">
                                    			<div class="large-6 columns photo-profile">
                                    				<img src="images/user/sell/jesus.png">
                                    			</div>

                                    			<div class="large-6 columns profile-item">
                                    					<p class="name">Jesus</p>
                                    					<p class="phone">(+51)1-397-5721</p>
                                    					<p class="email">jesus@blauda.com</p>
                                    					<p class="media"><a href="#"><img src="images/user/skype.png"></a> <a href="#"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>

                                    			</div>
                                    	</div>
                                    	
                                    
                                   	</div>
									
									
									
									
								 
					 
									
									
									 

                                   
                                    </div>





                                </div>






  <?php //=========================================== Content ========================================== ?>

