
  <?php //=========================================== Content include ========================================== ?>
  

                            <div class="large-12 columns main-detail-data detail-data-box-all"> 
                           	 <div class="large-12 columns detail-data">
                             	<div class="detail-data-left"><img src="images/user/detail_data_left.png"></div>
                                 <p>HOW TO BUY</p>
                             </div>
                             
                             <div class="large-12 columns blog-contents">
                             <p class="legend">Importing from Japan is not as difficult as you think! How to receive your vehicle?<br/>
							<span style="color:#0772ba; font-size:15px; font-weight:600">5 Easy Steps to Buy Japanese Used Car</span></p>
                            
                            <!--STEP 1-->
                            <p style="font-size:15px"><span style="color:#0772ba">STEP 1</span> : ORDER</p>
                            <div class="large-12 columns wrap-step">
                            <div class="large-3 columns" style="padding:0%">
                            <img src="images/user/howtobuy/step1.jpg" />
                            </div>
                            <div class="large-9 columns" style="padding:0%">
                            <li>Choose your car</li>
                            <li>Receive the Invoice</li>
                            <li>Search the Local Clearing Agent.</li>
                            </div>
                            </div>
                            
                            <!--STEP 2-->
                            <p style="font-size:15px"><span style="color:#0772ba">STEP 2</span> : PAYMENT</p>
                            <div class="large-12 columns wrap-step">
                            <div class="large-3 columns" style="padding:0%">
                            <img src="images/user/howtobuy/step2.jpg" />
                            </div>
                            <div class="large-9 columns" style="padding:0%">
                            <p style="line-height:28px">Go to your  Bank(*1)  to make payment(*2)  by <span style="color:#ff4242">Telegraphic transfer</span>. And send a copy of bank receipt  to MotorBB  by E-mail.<br/>
                            (*1) Bank DETAILS<br/>
                           Take the Invoice to your local bank and ask a banker to arrange the payment by "telegraphic transfer". You can basically use any of the banks who accept the foreign currency tranfer.</p>
                           <div class="large-12 columns" style="padding:0%;">
                           <div class="large-6 columns" style="padding:0%; padding-top:5%">
                           <p style="line-height:28px">(*2) Make payment"Invoice Price" at the Bank.<br/>
                           Make payment "Additional Price" at the Port.</p>
                           </div>
                           <div class="large-6 columns">
                           <img src="images/user/howtobuy/bank-acc.jpg" />
                           </div>
                           </div>




                            <p style="font-size:17px; font-weight:700; text-align:center">Bank Account</p>
                            <p style="font-size:17px; font-weight:700; text-align:center; color:#0772ba">Company Name  :  MOTORBB CO., LTD</p><br/>
                            <div class="large-12 columns wrap-step">
                            <div class="large-4 columns left">
                            <p class="p_about_text_left">
                            Account Number<br/>
                            Bank Name<br/>
                            Branch<br/>
                            Swift Code<br/>
                            Bank Address
                            </p></div>
                            <div class="large-8 columns right">
                            <p class="p_about_text_right">
                            0194171<br/>
                            The Bank of Tokyo-Mitsubishi UFJ,Ltd; 005<br/>
                            Yokohama Ekimae Branch; 251<br/>
                            BOTKJPJT<br/>
                            220-0004,1-11-20,Kitasaiwai,<br/>
                            Nishi-Ku,Yokohama,Japan
                            </p>
                            <div class="div_about_img"><img src="/images/user/MUFG.png" /></div>
                            
                            </div>
                            </div>
                            





                           <img src="images/user/howtobuy/step2-img2.jpg" />
                           <p>(*3) Ask the Clearing Agent how much does it cost to pick up the car from the port.</p>
                            </div>
                            </div>
                            
                            <!--STEP 3-->
                            <p style="font-size:15px"><span style="color:#0772ba">STEP 3</span> : SHIPMENT</p>
                            <div class="large-12 columns wrap-step">
                            <div class="large-3 columns" style="padding:0%">
                            <img src="images/user/howtobuy/step3.jpg" />
                            </div>
                            <div class="large-9 columns" style="padding:0%">
                            <p style="line-height:28px">Wait for the “ B/L copy” (*3) by E-mail and “B/L original”(*4) by DHL.
(*3) B/L  (Bill of Landing) Copy is certificate to show that your car was perfectly shipped out.
Within 1-2 days after shipping out from Japan, we will send you “Notice for Shipment” Including <span style="color:#ff4242">Bill of Landing Copy(PDF)</span> by E-mail.</p>
							<img style="float:right" src="images/user/howtobuy/pdf.png" />
                            <br/><p>(*4) It is needed at the port of your country in order to receive your vehicle from the port.</p>
                            <img style="float:left" src="images/user/howtobuy/step-img.png" />
                            </div>
                            </div>
                            
                            <!--STEP 4-->
                            <p style="font-size:15px"><span style="color:#0772ba">STEP 4</span> : CLEARING</p>
                            <div class="large-12 columns wrap-step">
                            <div class="large-3 columns" style="padding:0%">
                            <img src="images/user/howtobuy/step4.jpg" />
                            </div>
                            <div class="large-9 columns" style="padding:0%">
                            <p style="line-height:28px">Go to  your local agent and pass the B/L original.</p>
                            <img src="images/user/howtobuy/step4-img1.png" />
                            </div>
                            </div>
                            
                            <!--STEP 5-->
                            <p style="font-size:15px"><span style="color:#0772ba">STEP 5</span> : RECEIVING</p>
                            <div class="large-12 columns wrap-step">
                            <div class="large-3 columns" style="padding:0%">
                            <img src="images/user/howtobuy/step5.jpg" />
                            </div>
                            <div class="large-9 columns" style="padding:0%">
                            <p style="line-height:28px"> Pick up your car at your port. Clearing Agent will help you to receive it. 
 And if you register  your car in your local authority, you can drive your car from that day.</p>
 
                            </div>
                            </div>
                            
                            

                            
                             </div> 
                             </div>





									
                                
                                   




  <?php //=========================================== Content ========================================== ?>







                         