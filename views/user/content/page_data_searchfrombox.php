
<div class="large-12 columns main-detail-data-area">
    <div class="large-12 columns detail-data">
    <div class="detail-data-left-area"><img src="img/user/detail_data_left.png"></div>
        <p>TOTAL SEARCH CAR</p>
    </div>

    <div class="large-12 columns detail-data-box-search-area"><!-- line 1 -->

    <?php foreach($searchboxcar as $searchboxcar){ ?>
    <?php

          $BbsLinkRsv = './?c=user';
          $BbsLinkRsv .= '&m=cardetail';
          $BbsLinkRsv .= '&idx='.$searchboxcar->idx;
		  
		  $img_row = "https://res.cloudinary.com/softbloom/image/upload/thumb/".$searchboxcar->fileimgname;
      $img_rowb = "https://res.cloudinary.com/softbloom/image/upload/c_fill,h_500,w_700/".$searchboxcar->fileimgname;
        ?>

        <div class="large-3 columns main-item-car-area">
             <a href="?c=user&m=showdetailcar&idx=<?php echo $searchboxcar->idx; ?>" id="<?php echo $searchboxcar->idx; ?>">
             
              <?php if ($searchboxcar->fileimgname != ''){ ?>
               <img class="img" alt="<?php echo $searchboxcar->car_make ." ". $searchboxcar->car_model; ?>" src="<?php echo $img_row; ?>"/>
              <img style="display: none;" class="imgb" alt="<?php echo $searchboxcar->car_make ." ". $searchboxcar->car_model; ?>" src="<?php echo $img_rowb; ?>"/>
                      <?php }else{ ?>
               <img class="home_image_default" alt="default" src="https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj"/>
                      <?php } ?>
             
                      <?php if($searchboxcar->icon_status=="soldout"){
                           echo' <img class="sold_out" src="/images/user/sold-out.png"/>';
                        }

                       if($searchboxcar->icon_status=="reserved"){
                           echo' <img class="sold_out" src="/images/user/reserved.png"/>';
                        }
                        
                        ?>

              </a>

            <div class="large-12 columns link-item-cars">

              <?php

					$carmakesub = $searchboxcar->car_model_year ." ".$searchboxcar->car_make ." ". $searchboxcar->car_model;

          $chassisno = substr($searchboxcar->car_chassis_no, -6);
			  ?>

              <a href="?c=user&m=showdetailcar&idx=<?php echo $searchboxcar->idx; ?>" id="<?php echo $searchboxcar->idx; ?>"><?php echo  $carmakesub; ?></a>

            </div>
            <div class="large-5 columns title-item-car">
                  <p>Country :</p>
                  <p>ChassisNo :</p>
                  <p>Mileage :</p>
            </div>
            <div class="large-7 columns data-detail-item-car">
                  <p><?php echo ($searchboxcar->fullcountryname !="" ? $searchboxcar->fullcountryname  : 'N/A'); ?></p>
                  <p><?php echo  ($chassisno !='' ? $chassisno : 'N/A');?></p>
                  <p><?php echo ($searchboxcar->car_mileage !='' && $searchboxcar->car_mileage > 0 ?  number_format($searchboxcar->car_mileage)." Km" : 'N/A'); ?></p>
            </div>

            <div class="large-12 columns detail-price-item-car">
                <p>FOB : <?php echo ($searchboxcar->car_fob_cost !=0 ? number_format($searchboxcar->car_fob_cost) ." ".$searchboxcar->car_fob_currency : 'ASK'); ?></p>
            </div>
        </div>

      <?php } ?>



    </div><!-- End line 1 -->


    <?php


if($countsearchboxcars > 12){

      $totalpage = ceil($countsearchboxcars / $perpagepagination);


    ?>
      <div class="large-12 columns paginater-site">
              <ul class="pagination" role="menubar" aria-label="Pagination">
                <li><a class="clickpagination" id="1">&laquo;</a></li>
                <li class="arrow <?php if($getnumpage == 1){ ?> unavailable <?php } ?> " <?php if($getnumpage == 1){ ?> aria-disabled="true" <?php } ?> ><a class="clickpagination" id="<?php if($getnumpage == 1){ $numpage = $getnumpage; }else{ $numpage = $getnumpage - 1; } echo $numpage; ?>">Previous</a></li>

                <?php
                    if(($getnumpage-2) <= 0){
                        $currentpagenumber = 1;
                    }else{
                         $currentpagenumber = $getnumpage-2;
                    }

                    if(($getnumpage+2) > $totalpage){
                        $totalcurrent = $totalpage;
                    }else{
                        $totalcurrent = $getnumpage+2;
                    }

                ?>

                <?php for($i=$currentpagenumber; $i<=$totalcurrent; $i++){  ?>

                     <li <?php if($i == $getnumpage){ ?> class="current" <?php } ?>><a class="clickpagination valuepagination_<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i;?></a></li>
                <?php } ?>
                <li class="arrow <?php if($getnumpage >= $totalpage){ ?> unavailable <?php } ?> " <?php if($getnumpage >= $totalpage){ ?> aria-disabled="true" <?php } ?> ><a class="clickpagination" id="<?php if($getnumpage >= $totalpage){ $numpage = $getnumpage; }else{ $numpage = $getnumpage + 1; } echo $numpage; ?>">Next</a></li>

                <li><a class="clickpagination" id="<?php echo $totalpage; ?>">&raquo;</a></li>
             </ul>
      </div>

       <div class="brandId">
            <input type="hidden" id="txtcarmake" value="<?php echo $carmake; ?>">
            <input type="hidden" id="txtcarmodel" value="<?php echo $carmodel; ?>">
            <input type="hidden" id="txtkeyword" value="<?php echo $keyword; ?>">
            <input type="hidden" id="txtcarminyear" value="<?php echo $carminyear; ?>">
            <input type="hidden" id="txtcarminprice" value="<?php echo $carminprice; ?>">
            <input type="hidden" id="txtcarcountry" value="<?php echo $carcountry; ?>">
            <input type="hidden" id="txtcarmaxyear" value="<?php echo $carmaxyear; ?>">
            <input type="hidden" id="txtcarmaxrpice" value="<?php echo $carmaxrpice; ?>">
      </div>

<?php } ?>

<script type="text/javascript">

  $(document).ready(function(){


      // $(".clickpagination").click(function(){

      //       var ID = $(this).attr('id');
      //       var carmake = $("#txtcarmake").val();
      //       var carmodel = $("#txtcarmodel").val();
      //       var keyword = $("#txtkeyword").val();
      //       var carminyear = $("#txtcarminyear").val();
      //       var carminprice = $("#txtcarminprice").val();
      //       var carcountry = $("#txtcarcountry").val();
      //       var carmaxyear = $("#txtcarmaxyear").val();
      //       var carmaxrpice = $("#txtcarmaxrpice").val();
      //       var datastring = "carmake="+carmake+"&carmodel="+carmodel+"&keyword="+keyword+"&carminyear="+carminyear+"&carminprice="+carminprice+"&carcountry="+carcountry+"&carmaxyear="+carmaxyear+"&carmaxrpice="+carmaxrpice + "&page="+ ID;
      //        var urlstring="?c=user&m=search_boxpageajax";
      //    $.ajax({
      //      data: datastring,
      //      url: urlstring,
      //      type:'POST',
      //      success: function(data){
      //           $('.detail-data-box-all').html(data);

      //         }
      //     });
      // });




   $('.clickcardetial').click(function(a){

        a.preventDefault();


        var IDX = $(this).attr('id');

        var datastring = "idx=" + IDX;


        //alert(datastring + " " + IDX + " " + owner);

        $.ajax({
               url: "?c=user&m=showdetailcar",
               type: 'GET',

               data: datastring,
               success: function(data){
                 $('.detail-data-box-all').html(data);
               }
        });

      });

  });
</script>


</div>





