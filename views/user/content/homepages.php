  <?php //=========================================== Content include ========================================== ?>


                                <div class="large-12 columns main-detail-data">
                                    <div class="large-12 columns detail-data">
                                    <div class="detail-data-left"><img src="images/user/detail_data_left.png"></div>
                                        <p>MOST POPULAR CARS</p>
                                        <a rel="canonical" href="?c=user&m=carlist&car=popular&page=1"><div class="detail-data-right"><p1>SEE MORE</p1> &nbsp;&nbsp;<img src="images/user/see_more.png"></div></a>
                                    </div>

                                    <div class="large-12 columns detail-data-box"><!-- line 1 -->

										<?php
                      
											foreach($premium_cars as $row){

										?>
                                         <?php
											$BbsLinkRsv = './?c=user';
											$BbsLinkRsv .= '&m=cardetail';
											$BbsLinkRsv .= '&idx='.$row->idx;
					$img_row = $row->base_url . "thumb/".$row->fileimgname;
          $img_rowb = $row->base_url . "c_fill,h_500,w_700/".$row->fileimgname;


										?>

                                        	<div class="large-3 columns main-item-car">

                                            
                                              <?php  if ($row->icon_status=="soldout") { ?>

                                                   <a rel="canonical" href="?c=user&m=showdetailcar&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>">
                       
					   <?php if ($row->fileimgname != ''){ ?>
               <img class="home_image" alt="<?php echo $row->car_make ." ". $row->car_model; ?>" src="<?php echo $img_row; ?>" />
               <img class="home_imageb" style="display: none;" alt="<?php echo $row->car_make ." ". $row->car_model; ?>" src="<?php echo $img_rowb; ?>" />
                      <?php }else{ ?>
               <img class="home_image_default" alt="default" src="https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj"/>
                      <?php } ?>
                      
                      
                      
            <img class="sold_out" src="https://res.cloudinary.com/softbloom/image/upload/v1435031658/sold-out_xfi7yz"/>  
                                                   </a>

                                             <?php   }elseif ($row->icon_status=="reserved") { ?>

                                                     <a rel="canonical" href="?c=user&m=showdetailcar&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>">
                                                        <img  alt="default" src="<?php echo ($row->fileimgname !='' ? $img_row : "https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj"); ?>" class="home_image" />
                                                        <img style="display: none;" alt="default" src="<?php echo ($row->fileimgname !='' ? $img_rowb : "https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj"); ?>" class="home_imageb" />
                                                        <img class="sold_out" src="https://res.cloudinary.com/softbloom/image/upload/v1435031658/reserved_oisi6d"/>  
                                                     </a>

                                             <?php   }else{ ?>

                                                    <a rel="canonical" href="?c=user&m=showdetailcar&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>" >

                                                        <img  alt="default" src="<?php echo ($row->fileimgname !='' ? $img_row : "https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj"); ?>" class="home_image" />
                                                        <img style="display: none;" alt="default" src="<?php echo ($row->fileimgname !='' ? $img_rowb : "https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj"); ?>" class="home_imageb" />
                                                    </a>

                                            <?php  } ?>
                                            


                                            <div class="large-12 columns link-item-cars">
                                            <?php

                                              $carmodelyear = "";
                                              //echo $row->car_model_year; 
                                              if($row->car_model_year != 0){
                                    
                                                $carmodelyear = $row->car_model_year;
                                              }

												$carmakestr2 = $carmodelyear ." ".$row->car_make ." ". $row->car_model;

                                                $chassisno = substr($row->car_chassis_no, -6);
											?>

                                              <a rel="canonical" href="?c=user&m=showdetailcar&idx=<?php echo $row->idx; ?>" class="clickcardetial" id="<?php echo $row->idx; ?>"><?php echo $carmakestr2; ?></a>
                                            </div>


                                            <div class="large-12 columns detail-item-car">
                                                  
                                                  <table class="table-detail-item" width="100%">
                                                      <tr>
                                                        <td WIDTH="40%" class="td1">Country :</td>
                                                        <td WIDTH="60%" class="td2"><?php echo ($row->country_name !='' ? $row->country_name : 'N/A'); ?></td>
                                                      </tr>
                                                      <tr>
                                                        <td WIDTH="40%" class="td1">ChassisNo :</td>
                                                        <td WIDTH="60%" class="td2"><?php echo  ($chassisno !='' ? $chassisno : 'N/A');?></td>
                                                      </tr>
                                                      <tr>
                                                        <td WIDTH="40%" class="td1">Mileage :</td>
                                                        <td WIDTH="60%" class="td2"><?php echo ($row->car_mileage !='' && $row->car_mileage !=0 ? $row->car_mileage ." km" : 'N/A'); ?></td>
                                                      </tr>
                                                      <tr>
                                                        <td WIDTH="40%" class="td1">FOB :</td>
                                                        <td WIDTH="60%" class="td2 fob-item-car"><?php echo ($row->car_fob_cost !=0 ? number_format($row->car_fob_cost) ." ".$row->car_fob_currency : 'ASK'); ?></td>
                                                      </tr>
                                                  </table>

                                            </div>
                                            



                                        </div>
										<?php 


                    } //End Foreach ?>

                    
                                    </div><!-- End line 1 -->
                                </div>
                                <div class="large-12 columns main-detail-data">
                                    <div class="large-12 columns detail-data">
                                    <div class="detail-data-left"><img src="images/user/detail_data_left.png"></div>
                                        <p>NEW & UPDATED CARS</p>
                                        <a rel="canonical" href="?c=user&m=carlist&page=1"><div class="detail-data-right"><p1>SEE MORE</p1> &nbsp;&nbsp;<img src="images/user/see_more.png"></div></a>
                                    </div>

                                    <div class="large-12 columns detail-data-box"><!-- line 1 -->
              <?php
                      $count = 1;
											foreach($get_cars as $row){

                      if ($count%4 == 1)
                      {  
                           echo "<div class='main-block-item-car'>";
                      }
												
						$img_row = $row->base_url . "thumb/" . $row->fileimgname;

            $img_rowb = $row->base_url . "c_fill,h_768,w_1024/" . $row->fileimgname;
						
										?>
                            	<div class="large-3 columns main-item-car">

                             <a rel="canonical" href="?c=user&m=showdetailcar&idx=<?php echo $row->idx; ?>" class="clickcardetial" id="<?php echo $row->idx; ?>">
                              
							  <?php if ($row->fileimgname != ''){ ?>
               <img class="home_image" alt="<?php echo $row->car_make ." ". $row->car_model; ?>" src="<?php echo $img_row; ?>" />
                    <img class="home_imageb" style="display: none; " alt="<?php echo $row->car_make ." ". $row->car_model; ?>" src="<?php echo $img_rowb; ?>"/>
                      <?php }else{ ?>
               <img class="home_image_default" alt="default" src="https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj"/>
                      <?php } ?>
                                                
												<?php if($row->icon_status=="soldout"){
                                                   echo' <img class="sold_out" src="https://res.cloudinary.com/softbloom/image/upload/v1435031658/sold-out_xfi7yz"/>';
                                                }

                                               if($row->icon_status=="reserved"){
                                                   echo' <img class="sold_out" src="https://res.cloudinary.com/softbloom/image/upload/v1435031658/reserved_oisi6d"/>';
                                                }
                                                
                                                ?>


                                            </a>

                                            <div class="large-12 columns link-item-cars">
                                            <?php

                                              $carmodelyear = "";
                                              //echo $row->car_model_year; 
                                              if($row->car_model_year != 0){
                                    
                                                $carmodelyear = $row->car_model_year;
                                              }

                                            	$carmakestr2 = $carmodelyear ." ".$row->car_make ." ". $row->car_model;

                                                $chassisno = substr($row->car_chassis_no, -6);
											?>

                                              <a rel="canonical" href="?c=user&m=showdetailcar&idx=<?php echo $row->idx; ?>" class="clickcardetial" id="<?php echo $row->idx; ?>"><?php echo $carmakestr2; ?></a>
                                            </div>
                                              
                                            <div class="large-12 columns detail-item-car">
                                                  
                                                  <table class="table-detail-item" width="100%">
                                                      <tr>
                                                        <td WIDTH="40%" class="td1">Country :</td>
                                                        <td WIDTH="60%" class="td2"><?php echo ($row->fullnamecountry!='' ? $row->fullnamecountry: 'N/A'); ?></td>
                                                      </tr>
                                                      <tr>
                                                        <td WIDTH="40%" class="td1">ChassisNo :</td>
                                                        <td WIDTH="60%" class="td2"><?php echo  ($chassisno !='' ? $chassisno : 'N/A');?></td>
                                                      </tr>
                                                      <tr>
                                                        <td WIDTH="40%" class="td1">Mileage :</td>
                                                        <td WIDTH="60%" class="td2"><?php echo ($row->car_mileage !='' && $row->car_mileage !=0 ? number_format($row->car_mileage) ." km" : 'N/A'); ?></td>
                                                      </tr>
                                                      <tr>
                                                        <td WIDTH="40%" class="td1">FOB :</td>
                                                        <td WIDTH="60%" class="td2 fob-item-car"><?php echo ($row->car_fob_cost !=0 ? number_format($row->car_fob_cost) ." ".$row->car_fob_currency : 'ASK'); ?></td>
                                                      </tr>
                                                  </table>

                                            </div>




                                        </div>

									<?php 


                      if ($count%4 == 0)
                        {
                            echo "</div>";
                        }
                        $count++; 

                    } //End Foreach

                  ?>
                  <?php if ($count%4 != 1) echo "</div>"; ?>

                                    </div><!-- End line 1 -->

                                </div>









  <?php //=========================================== Content ========================================== ?>








<script>
	$(document).ready(function(){


			// ================ Ajax Click Show all car ============================//


			$('.clickshowallcar').click(function(a){

				a.preventDefault();
				var datastring="show=all&page=0";
				 $.ajax({
				 		   data:datastring,
						   url:"?c=user&m=showallcar",
						   type:'GET',
						   success: function(data){
							   $('.detail-data-box-all').html(data);
						   }
					 });

			});

		// ================ Ajax Click Show all car ============================//



	//============================= Click to view detail item with ajax ===========================//



			// $('.clickcardetial').click(function(a){

			// 	a.preventDefault();

			// 	var IDX = $(this).attr('id');
			

			// 	var datastring = "idx=" + IDX;


			// 	//alert(datastring + " " + IDX + " " + owner);

			// 	$.ajax({
			// 			   url: "?c=user&m=showdetailcar",
			// 			   type: 'GET',
			// 			   data: datastring,
			// 			   success: function(data){
			// 				   $('.detail-data-box-all').html(data);
			// 			   }
			// 	});

			// });



	//============================= Click to view detail item with ajax ===========================//



	});
</script>
