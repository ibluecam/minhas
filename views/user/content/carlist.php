
<div id="gotopagesearch" class="large-12 columns main-detail-data-area">
    <div class="large-12 columns detail-data">
    <div class="detail-data-left-area"><img src="img/user/detail_data_left.png"></div>
        <p class="titlebycar"><?php echo strtoUpper($carlisttitle); ?></p>
    </div>

    <div class="large-12 columns detail-data-box-search-area"><!-- line 1 -->

    <?php
    $count = 1;
     foreach($carlist as $row){ 

        if ($count%4 == 1){  
             echo "<div class='main-block-item-car'>";
        }

          $BbsLinkRsv = './?c=user';
          $BbsLinkRsv .= '&m=cardetail';
          $BbsLinkRsv .= '&idx='.$row->idx;
		  
		  $img_row = $row->base_url . "thumb/".$row->fileimgname;
      $img_rowb = $row->base_url . "c_fill,h_500,w_700/".$row->fileimgname;
      ?>

        <div class="large-3 columns main-item-car-area">

             <a rel="canonical" href="?c=user&m=showdetailcar&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>">
             
             <?php if ($row->fileimgname != ''){ ?>
               <img class="img" alt="<?php echo $row->car_make ." ". $row->car_model; ?>" src="<?php echo $img_row; ?>"/>
               <img style="display: none;" class="imgb" alt="<?php echo $row->car_make ." ". $row->car_model; ?>" src="<?php echo $img_rowb; ?>"/>
                      <?php }else{ ?>
               <img class="home_image_default" alt="default" src="https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj"/>
                      <?php } ?>

                      <?php if($row->icon_status=="soldout"){       
                          echo' <img class="sold_out" src="https://res.cloudinary.com/softbloom/image/upload/v1435031658/sold-out_xfi7yz"/>';
                        }

                       if($row->icon_status=="reserved"){
                           echo' <img class="sold_out" src="https://res.cloudinary.com/softbloom/image/upload/v1435031658/reserved_oisi6d"/>';
                        }
                        
                        ?>

                      </a>

            <div class="large-12 columns link-item-cars">

              <?php

                       $carmodelyear = "";
                        if($row->car_model_year != 0 and $row->car_model_year != ""){
              
                          $carmodelyear = $row->car_model_year;
                        }


                        $carmakestr2 = $carmodelyear ." ".$row->car_make ." ". $row->car_model;

                        $chassisno = substr($row->car_chassis_no, -6);
			         ?>

              <a rel="canonical" href="?c=user&m=showdetailcar&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>"><?php echo $carmakestr2; ?></a>

            </div>


            <div class="large-12 columns detail-item-car">
                                                  
                  <table class="table-detail-item" width="100%">
                      <tr>
                        <td WIDTH="40%" class="td1">Country :</td>
                        <td WIDTH="60%" class="td2"><?php echo ($row->country_name!='' ? $row->country_name : 'N/A'); ?></td>
                      </tr>
                      <tr>
                        <td WIDTH="40%" class="td1">ChassisNo :</td>
                        <td WIDTH="60%" class="td2"><?php echo  ($chassisno !='' ? $chassisno : 'N/A');?></td>
                      </tr>
                      <tr>
                        <td WIDTH="40%" class="td1">Mileage :</td>
                        <td WIDTH="60%" class="td2"><?php echo ($row->car_mileage !='' && $row->car_mileage > 0 ? number_format($row->car_mileage) ." km" : 'N/A'); ?></td>
                      </tr>
                      <tr>
                        <td WIDTH="40%" class="td1">FOB :</td>
                        <td WIDTH="60%" class="td2 fob-item-car"><?php echo ($row->car_fob_cost !=0 ? number_format($row->car_fob_cost) ." ".$row->car_fob_currency : 'ASK'); ?></td>
                      </tr>
                  </table>

            </div>


        </div>

      <?php 

          if ($count%4 == 0){
              echo "</div>";
          }
          $count++; 

        } //End Foreach

      ?>

      <?php if ($count%4 != 1) echo "</div>"; ?>

    </div><!-- End line 1 -->



 <?php

if($countcarlist > $perpagepagination){
      $totalpage = ceil($countcarlist / $perpagepagination);
	  

    ?>

<div class="brandId">
      <input type="hidden" id="txttype" value="<?php echo $carlist[0]->car_make; ?>">
</div>
<div class="large-12 columns paginater-site">
        <ul class="pagination" role="menubar" aria-label="Pagination">
          <li><a class="clickpagination" id="1">&laquo;</a></li>
          <li class="arrow <?php if($getnumpage == 1){ ?> unavailable <?php } ?> " <?php if($getnumpage == 1){ ?> aria-disabled="true" <?php } ?> ><a class="clickpagination" id="<?php if($getnumpage == 1){ $numpage = $getnumpage; }else{ $numpage = $getnumpage - 1; } echo $numpage; ?>">Previous</a></li>

          <?php

              if(($getnumpage-2) <= 0){
                  $currentpagenumber = 1;
              }else{
                  $currentpagenumber = $getnumpage-2;
              }

              if(($getnumpage+2) > $totalpage){
                  $totalcurrent = $totalpage;
              }else{
                  $totalcurrent = $getnumpage+2;
              }

          ?>
          <!--  href="<?php echo  current_url().ltrim($_SERVER['REQUEST_URI'], '/'); ?>&page=<?php echo $i; ?>"-->
          <?php $n=1;for($i=$currentpagenumber; $i<=$totalcurrent; $i++){  ?>

               <li <?php if($i == $getnumpage){ ?> class="current" <?php } ?>><a class="clickpagination valuepagination_<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i;?></a></li>
          <?php } ?>
          <li class="arrow <?php if($getnumpage >= $totalpage){ ?> unavailable <?php } ?> " <?php if($getnumpage >= $totalpage){ ?> aria-disabled="true" <?php } ?> ><a class="clickpagination" id="<?php if($getnumpage >= $totalpage){ $numpage = $getnumpage; }else{ $numpage = $getnumpage + 1; } echo $numpage; ?>">Next</a></li>
          <li><a class="clickpagination" id="<?php echo $totalpage; ?>">&raquo;</a></li>
       </ul>
</div>
<?php } ?>



</div> 

<script type="text/javascript">

  $(document).ready(function(){

    $(".clickpagination").click(function(){

        var ID = $(this).attr('id');
        var urlcutresult = "";
        var url = window.location.href;

        //alert(url);

        var pieces = url.split("/?");



       
        var pieces1 = pieces[1].split("&");

        /*for(var i = 1; i<=pieces1.length ; i++){

             pieces1 = pieces1[10];

         }*/
         $.each(pieces1, function(key, value) {
           
            if(key == pieces1.length - 1){
              valuefull = value;
              value = value.substring(0,4);

              if(value == "page"){
                

                var old = pieces = "?"+pieces[1];

                var news = old.replace(valuefull,"page="+ID);

                location.href=news;
                

              }else{
                location.href=url+"&page="+ID;
              }

            }
            
         });

         //alert(pieces1);


        //urlcut = pieces[2];

        //var ID = $(this).attr('id');
       // var TYPECARS=$("#txttype").val();


        //var datastring = "&page="+ID;
         //pieces = "?"+pieces[1] + "&page="+ID;

        /*if(typeof(urlcut) === "undefined"){
           urlcutresult = "";
        }else{
            urlcutresult = urlcut;
        }*/






      });

    });

</script>

