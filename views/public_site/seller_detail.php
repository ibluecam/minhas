
<div class="detail_wrapper">
	<?php
		foreach( $sellerdetail as $rows){
	?>
    <div class="title_detail"><p class="bigger"><?php echo ($rows->company_name !="" ? $rows->company_name : "N/A"); ?></p></div>
    <?php } ?>
    
	<!---- seller info start ---->
    <div class="seller_info_wrapper">
    	<!---- seller info left start ---->
        <div class="seller_info_left">
        	<div class="imgload">
            <?php
				foreach ($member_profile_detail as $img_rows){
					$img_row = $img_rows->base_url.$img_rows->image;
					if($img_rows->image != ""){
            ?>
            	<img src="<?php echo $img_row; ?>" />
            <?php }else{ ?>
         <img src="/images/user/no_image_seller.jpg" />
			<?php
                  }
				}
            ?>
            </div>
            
            <!--<div class="small_img">
            	<img src="/images/public_site/detail_img_small.jpg" />
                <img src="/images/public_site/detail_img_small.jpg" />
                <img src="/images/public_site/detail_img_small.jpg" />
                <img src="/images/public_site/detail_img_small.jpg" />
                <img src="/images/public_site/detail_img_small.jpg" />
                <img src="/images/public_site/detail_img_small.jpg" />
                <img src="/images/public_site/detail_img_small.jpg" />
                <img src="/images/public_site/detail_img_small.jpg" />
                <img src="/images/public_site/detail_img_small.jpg" />
                <img src="/images/public_site/detail_img_small.jpg" />
            </div>-->
            
       </div>
       <!---- seller info left end ---->
        
        <!---- seller info right start ---->
        <div class="seller_info_right">
        <?php
             foreach( $sellerdetail as $rows){
        ?>
        	 <div class="spec_content">
				
            	
                <div class="spec">
                	<div class="left">Country</div>
                    <div class="right"><?= $rows->country; ?></div>
                </div>
                <div class="spec">
                	<div class="left blue">Contact Person</div>
                    <div class="right blue"><?php echo ($rows->contact_person_name !="" ? $rows->contact_person_name : "N/A"); ?></div>
                </div>
                <div class="spec address">
                	<div class="left">Address</div>
                    <div class="right"><?= $rows->address; ?></div>
                </div>
                
               
            </div>
            
            <div class="introduction">
            	<p class="bigger">Introduction</p>
                
            	<p class="smaller intro_text"><?= $rows->user_memo; ?></p>
            </div>
            	
		<?php } ?>
        
        </div>
        <!---- seller info right end ---->
        
     </div>
	<!---- seller info end ---->
    
    
    <!---- carlist's seller start ---->
    <div class="carlist_seller_wrapper">
    <?php
		foreach( $sellerdetail as $rows){
	?>
    	<div class="title_detail"><p class="bigger">Car from <?php echo ($rows->company_name !="" ? $rows->company_name : "N/A"); ?></p></div>
    <?php
		}
	?>
        <div class="listcar_wrapper">
        
        	 <!---- listcar box start ---->
             <?php
  					foreach( $carlist as $rows){
					$img_row = $rows->base_url."thumb/".$rows->fileimgname;			
					$img_rowb = $rows->base_url.$rows->fileimgname;
			 ?>
            <div class="listcar">
            	<div class="carsource_wrapper">
                    <a href="/?c=user&m=cardetail&idx=<?php echo $rows->idx; ?>">
                    <?php if ($rows->fileimgname != ''){ ?>
                    	<img class="carload" alt="<?php echo $rows->car_make ." ". $rows->car_model." ". $rows->car_model_year; ?>" title="<?php echo $rows->car_make ." ". $rows->car_model." ". $rows->car_model_year;; ?>" src="<?php echo $img_row; ?>" />
                    <?php }else{ ?>
                    	<img class="carload" alt="<?php echo $rows->car_make ." ". $rows->car_model." ". $rows->car_model_year;; ?>" title="<?php echo $rows->car_make ." ". $rows->car_model." ". $rows->car_model_year;; ?>" src="/images/user/no_image.png"/>
                    <?php } ?>
                    <?php if($rows->icon_status=="soldout"){
                              echo' <img class="sold_out" src="/images/user/sold-out.png"/>';
                          }
                          if($rows->icon_status=="reserved"){
                              echo' <img class="sold_out" src="/images/user/reserved.png"/>';
                          }
                    ?>
                    </a>
                    <?php 
						$carmakestr2 = $rows->car_model_year ." ".$rows->car_make ." ". $rows->car_model;
                    ?>
                    <div class="title"><a href="/?c=user&m=cardetail&idx=<?php echo $rows->idx; ?>"><?php echo $carmakestr2; ?></a></div>
                    <div class="cardata_wrapper">
                        <div class="smaller">Country: 
                          <img class="cc" src="/images/flags/32/<?=strtoupper($rows->country)?>.png">
                        </div>
                        <div class="smaller">FOB:<span><?php echo ($rows->car_fob_cost !=0 ? $rows->car_fob_currency ." ".number_format($rows->car_fob_cost) : 'ASK'); ?></span></div>
                    </div>
            	</div>
                
                <a class="contact_sell" href="/?c=user&m=cardetail&idx=<?php echo $rows->idx; ?>">Contact</a>
                
                
            </div>
            <?php } ?>

            <!---- listcar box end ---->
            
        </div>
        
    </div>
    <!---- carlist's seller end ---->
    
</div>


<?php

if($countcarlist > $perpagepagination){
      $totalpage = ceil($countcarlist / $perpagepagination);

    ?>

<div class="brandId">
      <input type="hidden" id="txttype" value="<?php echo $carlist[0]->car_make; ?>">
</div>
<div class="paginater-site">
        <ul class="pagination" role="menubar" aria-label="Pagination">
          <li><a class="clickpagination" id="1">&laquo;</a></li>
          <li class="arrow <?php if($getnumpage == 1){ ?> unavailable <?php } ?> " <?php if($getnumpage == 1){ ?> aria-disabled="true" <?php } ?> ><a class="clickpagination" id="<?php if($getnumpage == 1){ $numpage = $getnumpage; }else{ $numpage = $getnumpage - 1; } echo $numpage; ?>">Previous</a></li>

          <?php

              if(($getnumpage-2) <= 0){
                  $currentpagenumber = 1;
              }else{
                  $currentpagenumber = $getnumpage-2;
              }

              if(($getnumpage+2) > $totalpage){
                  $totalcurrent = $totalpage;
              }else{
                  $totalcurrent = $getnumpage+2;
              }

          ?>

          <?php for($i=$currentpagenumber; $i<=$totalcurrent; $i++){  ?>
               <li<?php if($i == $getnumpage){ ?> class="current" <?php } ?>><a class="clickpagination valuepagination_<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i;?></a></li>
          <?php } ?>
          <li class="arrow <?php if($getnumpage >= $totalpage){ ?> unavailable <?php } ?> " <?php if($getnumpage >= $totalpage){ ?> aria-disabled="true" <?php } ?> ><a class="clickpagination" id="<?php if($getnumpage >= $totalpage){ $numpage = $getnumpage; }else{ $numpage = $getnumpage + 1; } echo $numpage; ?>">Next</a></li>
          <li><a class="clickpagination" id="<?php echo $totalpage; ?>">&raquo;</a></li>
       </ul>
</div>
<?php } ?>






                                </div>
									




  <?php //=========================================== Content ========================================== ?>
  

