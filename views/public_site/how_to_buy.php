<div class="main_content_how_to_buy">
	
	<div class="title_content_how_to_buy">
		<p>Importing from Japan is not as difficult as you think! How to receive your vehicle?</p>
		<p class="step_of_buy">5 Easy Steps to Buy Japanese and Korean Used Cars</p>
	</div>

	<div class="desc_content_how_to_buy">
		<div class="row_content">
			
			<div class="row_left_content">
				<p><span>STEP 1</span> : ORDER</p>
				<img src="../images/public_site/howtobuy/step1.jpg">
			</div>
			
			<div class="row_right_content">
				<p>&rarr; <span>Choose</span> the car which you want to buy. Our stock search engine will help you search through our stocks.</p>
				<p>&rarr; <span>Receive</span> the invoice from our company and check the price.</p>
				<p>&rarr; If it is the <span>first time</span> for you to buy a car by online, you should find a <span>Clearing Agent.</span></p>
				<p class="last_desc">It will help you a lot.</p>
			</div><div class="clear"></div>
 
		</div>


		<div class="row_content">
			
			<div class="row_left_content">
				<p><span>STEP 2</span> : PAYMENT</p>
				<img src="../images/public_site/howtobuy/step2.jpg">
			</div>
			
			<div class="row_right_content">
				
				<p>
				Go to your  bank(*1)  to make <span class="add_line">payment</span>(*2)    by <span class="add_color">Telegraphic transfer</span>. And send a copy of bank 
receipt  to MotorBB  by E-mail.
				</p>

				<div class="bank_details">

					<p>(*1) Bank details</p>
					<p>Take the invoice to your local bank and ask a banker to arrange the payment by "telegraphic transfer". 
You can basically use any of the banks who accept the foreign currency transfer.</p>
				</div>

				

				<div class="banking_acc_detail">

					<div class="text_acc_detail">
						<p>(*2) Make payment"Invoice Price" at the Bank. Make payment 
"Additional Price" at the Port.</p>
					</div>

					<div class="img_acc_detail">
						<img src="../images/public_site/howtobuy/bank-acc.jpg">
					</div><div class="clear"></div>

				</div>


				<div class="main_address_bank_account">
					<p class="title_address_bank_account">Bank Account</p>
					<p class="name_address_bank_account">Company Name  :  MOTORBB CO., LTD</p>

					<div class="address_content_bank_account">

						<div class="left_content_bank_account">
							<p>ACCOUNT NUMBER</p>
							<p>BANK NAME</p>
							<p>BRANCH</p>
							<p>SWIFT CODE</p>
							<p>BANK ADDRESS</p>
						</div>

						<div class="center_content_bank_account">
							<p>0194171</p>
							<p>The Bank of Tokyo-Mitsubishi UFJ,Ltd; 005</p>
							<p>YOKOHAMA Ekimae Branch; 251</p>
							<p>BOTKJPJT</p>
							<p>220-0004,1-11-20,Kitasaiwai,</p>
							<p>Nishi-Ku,Yokohama,Japan</p>
						</div>

						<div class="right_content_bank_account">
							<img src="../images/public_site/howtobuy/MUFG.png">
						</div><div class="clear"></div>

					</div>	

				</div>


				<div class="mian_shipping_how_to_buy">
					
					<div class="img_shipping_how_to_buy">
						<img src="../images/public_site/howtobuy/step2-img2.jpg">
					</div>

					<div class="desc_shipping_how_to_buy">
						<p>(*3) Ask the Clearing Agent how much does it cost to pick up the car from the port.</p>
					</div>

				</div>



			</div><div class="clear"></div>
 
		</div>





		<div class="row_content">
			
			<div class="row_left_content">
				<p><span>STEP 3</span> : SHIPMENT</p>
				<img src="../images/public_site/howtobuy/step3.jpg">
			</div>
			
			<div class="row_right_content">
				<p>Wait for the &ldquo;B/L copy&ldquo; (*3) by E-mail and &ldquo;B/L original&ldquo;(*4) by DHL.</p>
				<p>(*3) B/L  (Bill of Landing) Copy is certificate to show that your car was perfectly shipped out.</p>
				<p>Within 1-2 days after shipping out from Japan, we will send you &ldquo;Notice for Shipment&ldquo; Including </p>
				<p><span class="bill_of_landing">Bill of Landing Copy(PDF)</span> by E-mail.</p>
				
				<div class="img_bill_of_landing">
					<img src="../images/public_site/howtobuy/pdf.png">
				</div>

				<div class="text_bill_of_landing">
					<p>(*4) It is needed at the port of your country in order to receive your vehicle from the port.</p>
				</div>

				<div class="img_shipping_of_landing">
					<img src="../images/public_site/howtobuy/shipinglanding.png">
				</div>

			</div><div class="clear"></div>
 
		</div>


		<div class="row_content">
			
			<div class="row_left_content">
				<p><span>STEP 4</span> : CLEARING</p>
				<img src="../images/public_site/howtobuy/step4.jpg">
			</div>
			
			<div class="row_right_content">
				<p>Go to  your local agent and pass the B/L original.</p>
				
				<div class="img_clearing">
					<img src="../images/public_site/howtobuy/step4-img1.png">
				</div>

			</div><div class="clear"></div>
 
		</div>



		<div class="row_content" id="last_row_content">
			
			<div class="row_left_content">
				<p><span>STEP 5</span> : RECIEVING</p>
				<img src="../images/public_site/howtobuy/step5.jpg">
			</div>
			
			<div class="row_right_content">
				<p>Pick up your car at your port. Clearing Agent will help you to receive it.</p>
				<p>And if you register  your car in your local authority, you can drive your car from that day.</p>
			
			</div><div class="clear"></div>
 
		</div>




	</div>

	

</div>