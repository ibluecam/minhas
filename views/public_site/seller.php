<div class="wrapper_seller">
	<div class="seller_side_bar">
		<div class="seller_left"></div>
		<div class="seller_right">
			<span>SELLER</span>
		</div>
	</div>
	<div class="wrapper_seller_search">
	 <form action="?c=user&m=public_seller"  method="post">
		<select name="country">
			<option value="">Country</option>
             <?php foreach($country as $rows){ ?>
              <option name="country"  value="<?php echo $rows->member_country ;?>" <?php if(isset($_GET['country'])){ if($_GET['country'] == $rows->member_country){ ?> selected <?php } }?> ><?php echo $rows->country_name;  ?></option>
             <?php } ?>
		</select>
		<input type="text" id="keyword" name="keyword" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}?>" placeholder="Keyword..."/>
		<input type="submit" name="btn_search" value="Search"/>
	<form>
	</div>

	<div class="wrapper_seller_list">
	 <?php 
	 	$block = 1;
	 	foreach($sellers as $row) { 
	 	$row_img=$row->base_url.$row->public_id;
		
		if($block%3 == 1){
			echo '<div class="wrapper_box">';
		}	 

	 ?>
		 <div class="box_seller">
		 	<div class="box_seller_head">
            <?php $company = strtoupper($row->company_name); ?>
		 		<span><?php echo ($company !="" ? $company : $company); ?></span>
		 	</div>
		 	<div class="box_seller_center">
			  	<div class="box_seller_image">
			  		<?php 
	                  if($row->public_id !=""){ ?>
	                  <img  src="<?php echo $row_img; ?>"/>
		              <?php  }else{ ?>
		              <img src="<?php echo "/images/user/no_image_seller.jpg";?>"/>
	              	<?php } ?>
			 	</div>
			 	<div class="seller_contact">
			 			<div class="float_left"><span>Contact Person : </span></div>
			 			<div class="contact_person"><p style="word-break: break-all;"><?php echo ($row->contact_person !="" ? $row->contact_person : "N/A" ); ?></p></div>
			 			<div class="clear"></div>

			 			<div class="float_left"><span>Country : </span></div>
			 			<div class="contact_addr_country"><p><?php echo ($row->country_name!='' ? $row->country_name : 'N/A'); ?></p></div>
			 			<div class="clear"></div>

			 			<div class="float_left"><span>Address : </span></div>
			 			<div class="contact_addr_country"><p><?php echo ($row->customer_address!='' ? substr($row->customer_address,0,75)  : 'N/A' ); ?></p></div>
			 			<div class="clear"></div>
			 	</div>
			 	<div class="clear"></div>
			 </div>
			 <div class="box_seller_bottom">
			 	<div class="seller_bottom_left">
			 		<p>Total products : <span><?php echo number_format($row->count_cars); ?></span></p>
			 	</div>
	    		<div class="seller_bottom_right">
		 			<input type="button" value="View Profile" onClick="javascript:location.href = '/?c=user&m=sellerdetail&id=<?php echo $row->member_no; ?>';"/>
		 		</div> 
			 	<div class="clear"></div>
			 </div>
		 </div>

	   <?php 
	   	if($block%3 == 0){
			echo '</div><div class="clear"></div>';
		}
		$block++;
	   }
	   	if($block%3 != 1){
			echo '</div><div class="clear"></div>';
		}
	   ?>

	
 <?php

if($total_count > $perpagepagination){
      $totalpage = ceil($total_count / $perpagepagination);
    ?>
	<div class="pagination">
		<ul class="paginnav">
			<?php if($getnumpage > 1){ ?>
				<li><a href="?c=user&m=public_seller&country=<?php if(isset($_GET['country'])){ echo $_GET['country'];}else{ echo "";} ?>&keyword=<?php  if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}?>&page=1">&laquo;</a></li>
				<li><a href="?c=user&m=public_seller&country=<?php if(isset($_GET['country'])){ echo $_GET['country'];}else{ echo "";} ?>&keyword=<?php  if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}  ?>&page=<?php if($getnumpage == 1){ $numpage = $getnumpage; }else{ $numpage = $getnumpage - 1; } echo $numpage; ?>">Previous</a></li>
			<?php } ?>
			<?php

              if(($getnumpage-2) <= 0){
                  $currentpagenumber = 1;
              }else{
                  $currentpagenumber = $getnumpage-2;
              }

              if(($getnumpage+2) > $totalpage){
                  $totalcurrent = $totalpage;
              }else{
                  $totalcurrent = $getnumpage+2;
              }

          ?>
          <?php $n=1;for($i=$currentpagenumber; $i<=$totalcurrent; $i++){  ?>
               <li <?php if($i == $getnumpage){ ?> class="current" <?php } ?>><a href="?c=user&m=public_seller&country=<?php if(isset($_GET['country'])){ echo $_GET['country'];}else{ echo "";} ?>&keyword=<?php  if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}  ?>&page=<?php echo $i; ?>"><?php echo $i;?></a></li>
          <?php } ?>

          <?php if($getnumpage != $totalpage){ ?>
			<li><a href="?c=user&m=public_seller&country=<?php if(isset($_GET['country'])){ echo $_GET['country'];}else{ echo "";} ?>&keyword=<?php  if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}  ?>&page=<?php if($getnumpage >= $totalpage){ $numpage = $getnumpage; }else{ $numpage = $getnumpage + 1; } echo $numpage; ?>">Next</a></li>
			<li><a href="?c=user&m=public_seller&country=<?php if(isset($_GET['country'])){ echo $_GET['country'];}else{ echo "";} ?>&keyword=<?php  if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}  ?>&page=<?php echo $totalpage; ?>">&raquo;</a></li>
		  <?php } ?>
		  
		</ul>
	</div><div class="clear"></div>
<?php } ?>

</div>
