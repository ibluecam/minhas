<div class="step_wrapper">
	<img src="images/public_site/email_sent/first_step.png">
</div>

<div class="confirm_msg_wrapper">
	<div class="confirm_msg_container">
		<div class="step_title">Registration Almost Completed</div>
		<div class="send_status_container">
			<img src="images/public_site/email_sent/sent_status.png"/>
		</div>
		<div class="sent_desc">
			Email Confirmation was sent to your email:<br/>
			<b><?php echo $get_email; ?></b><br/><br/>
			Please <b>check your inbox</b> and <br/>find this button in our email to continue your registration.<br/><br/>
		</div>
		<div class="email_sample">
			<img src="images/public_site/email_sent/email_sample.jpg"/>
		</div>
		<div class="special_note">
			* If you cannot find our email in Inbox, it might be in <b>spam</b> or <b>junk-mail.</b></br>
			<!--Still not received? <a href="#">Resend</a>-->
		</div>
	</div>
	

</div>