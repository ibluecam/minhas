<div class="about_us_map">
	<img src="../images/public_site/motorbb_map.png">
</div>

<div class="main_content_about_us">

	<div class="header_about_us">
		
		<div class="header_left_about_us">
			
		</div>

		<div class="header_right_about_us">
			<p>About us</p>
		</div><div class="clear"></div>

	</div>


	<div class="main_content_about_us">
		
		<div class="main_content_left_about_us">
			
			<div class="desc_content_about_us">
				<h4 class="title_content_about_us">Company Information</h4>
				<p class="text_content_about_us">Motorbb is an online used car information providing company whose headquarter is located in Kawasaki, Japan. We are exporting cars from South Korea and Japan to Asia, Africa, Dominican Republic and Paraguay.</p>
				<p>Once a customer buys a car from our website, we take care of all the details for you, including shipping and documentation. Our sales team provides 24/7 service in order to achieve the highest customer satisfaction. We commit to provide convenient, quick and professional services to worldwide customers.</p>
				
				<div class="main_note_For_certificate">

					<!-- <p class="note_For_certificate">See our certificate from Japanese Government:</p> --> 
					<div class="note_For_download_certificate">
						<img class="icon_download" src="../images/public_site/icon_pdf.png" /> 
						<a href="uploads/file/certificate of Motorbb from Japanese government.pdf" target="_blank">Click To Download</a>
					</div><div class="clear"></div>

				</div>

			</div>


			<div class="company_outline_about_us">
				<h4 class="title_content_about_us">Company Outline</h4>

				<div class="company_outline_about_us_table">
					
					<table width="100%" class="company_outline_table">
						<tr>
							<td class="company_outline_table_data_left">Company Name: </td>
							<td class="company_outline_table_data_right">Motorbb Co., LTD</td>
						</tr>

						<tr class="row_color">
							<td class="company_outline_table_data_left">Headquarter (Japan): </td>
							<td class="company_outline_table_data_right">Ark Hills Front Tower17F, 2-23-1 Akasaka, Minato-ku, Tokyo, Japan</td>
						</tr>

						<tr>
							<td class="company_outline_table_data_left">Branch (South Korea): </td>
							<td class="company_outline_table_data_right">E-1906B, Songdo Smart Valley, 30 Songdomirae-ro, Yeonsu-gu, Incheon,Korea</td>
						</tr>

						<tr class="row_color">
							<td class="company_outline_table_data_left">Branch (Cambodia): </td>
							<td class="company_outline_table_data_right">#B26, St.1019 Sangkat Toek Thlar, Khann Sensok Phnom Penh, Cambodia</td>
						</tr>

						<tr>
							<td class="company_outline_table_data_left">PhoneNumber (Japan): </td>
							<td class="company_outline_table_data_right"><p><img src="../images/public_site/icon_skype_aboutus.png" class="about_us_skype">+81-45-290-9480 / 81</p></td>
						</tr>

						<tr class="row_color">
							<td class="company_outline_table_data_left">PhoneNumber (South Korea): </td>
							<td class="company_outline_table_data_right"><p><img src="../images/public_site/icon_skype_aboutus.png" class="about_us_skype">+(82) 32-264-5500</p></td>
						</tr>

						<tr>
							<td class="company_outline_table_data_left">PhoneNumber (Cambodia): </td>
							<td class="company_outline_table_data_right"><p><img src="../images/public_site/icon_skype_aboutus.png" class="about_us_skype">+855-16-4040-32</p></td>
						</tr>

						<tr class="row_color">
							<td class="company_outline_table_data_left">Foundation: </td>
							<td class="company_outline_table_data_right">April/2015</td>
						</tr>

						<tr>
							<td class="company_outline_table_data_left">Number of employees: </td>
							<td class="company_outline_table_data_right">7(As of the end of May 2015)</td>
						</tr>

						<tr class="row_color">
							<td class="company_outline_table_data_left">Business description: </td>
							<td class="company_outline_table_data_right">Export of automobiles</td>
						</tr>

					</table>

				</div>

			</div>

		</div>

		<div class="main_content_right_about_us">
			<div class="img_about_us">
				<img src="../images/public_site/aboutus1.jpg">
			</div>

			<div class="img_about_us">
				<img src="../images/public_site/aboutus2.jpg">
			</div>
		</div><div class="clear"></div>

	</div>





	


</div>

<div id="content_seller_team" class="main_content_about_us">
	<div class="header_about_us">
		
		<div class="header_left_about_us">
			
		</div>

		<div class="header_right_about_us">
			<p>The Team for Japanese Export</p>
		</div><div class="clear"></div>

	</div>

	<div class="main_seller_team">

		
		<div class="row_seller_team">
			
             <?php
              if(count($slelect_team_sale_list>0)){
              	foreach ($slelect_team_sale_list as $row_jp) {
              
            
            ?>
          
			<div class="item_seller_team">

				<div class="item_images_seller_team">
					<!-- <img src="../images/user/sell/may.png"> -->
					<img src="<?php echo $row_jp->secure_url; ?>">
				</div>

				<div class="item_info_seller_team">
					<h4 class="name"><?php echo $row_jp->member_first_name; ?></h4>
					<p class="phone">
						 <?php
						   
						   	 echo "+".$row_jp->phone_no1.'-'.$row_jp->phone_no2;
						   
						  	//var_dump($row_jp);
						  ?>
						
					</p>
					<p class="email"><?php echo $row_jp->email; ?></p>
					<p class="media">
						<?php 
							if(!empty($row_jp->skype_id)){
								echo '<a class="tooltip" title="Skype ID: &lt;b&gt;'.$row_jp->skype_id.'&lt;/b&gt;" href="skype:'.$row_jp->skype_id.'?add"><img src="images/user/skype.png"></a>';
							} 
							if(!empty($row_jp->facebook_id)){
								echo '<a class="tooltip" title="Facebook: &lt;b&gt;https://facebook.com/'.$row_jp->facebook_id.'&lt;/b&gt;" target="_blank" href="https://facebook.com/'.$row_jp->facebook_id.'"><img src="images/user/facebook.png"></a> ';
							}
							if(!empty($row_jp->viber_id)){
								echo '<a class="tooltip" title="This is my divs tooltip message!" href="'.$row_jp->viber_id.'"><img src="images/user/1.png"></a>';
							}
							if(!empty($row_jp->whatsapp_id)){
								echo '<a class="tooltip" title="This is my divs tooltip message!" href="'.$row_jp->whatsapp_id.'"><img src="images/user/4(1).png"></a>';
							}
						?>
						 
						
						 
						
					</p>
				</div><div class="clear"></div>

			</div>
			 <?php 
            	}
             }
        ?>
			<div class="clear"></div>


       

		</div>



		<!-- Line -->

		
	</div>


</div>



<div id="content_seller_team_korean" class="main_content_about_us">
	<div class="header_about_us">
		
		<div class="header_left_about_us">
			
		</div>

		<div class="header_right_about_us">
			<p>The Team for Korean Export</p>
		</div><div class="clear"></div>

	</div>


		<div class="main_seller_team">
		
			<div class="row_seller_team">

				<?php
				  if(count($slelect_team_sale_list_kr>0)){
				  	var_dump($slelect_team_sale_list_kr);
				  	foreach ($slelect_team_sale_list_kr as $row_kr) {
				  		
				 
				?>
				
				<div class="item_seller_team">

					<div class="item_images_seller_team">
						<img src="<?php echo $row_kr->secure_url; ?>">
					</div>

					<div class="item_info_seller_team">
						<h4 class="name"><?php echo $row_kr->member_first_name; ?></h4>
					    <p class="phone">
					    	 <?php
						   
						   	 echo "(+".$row_kr->phone_no1.')'.$row_kr->phone_no2;
						   
						  
						  ?>
					    </p>
					    <p class="email"><?php echo $row_kr->email; ?></p>
						<p class="media">
							<?php 
								if(!empty($row_jp->skype_id)){
									echo '<a href="skype:'.$row_jp->skype_id.'?add"><img src="images/user/skype.png"></a>';
								} 
								if(!empty($row_jp->facebook_id)){
									echo '<a target="_blank" href="https://facebook.com/'.$row_jp->facebook_id.'"><img src="images/user/facebook.png"></a> ';
								}
								if(!empty($row_jp->viber_id)){
									echo '<a href="'.$row_jp->viber_id.'"><img src="images/user/1.png"></a>';
								}
								if(!empty($row_jp->whatsapp_id)){
									echo '<a href="'.$row_jp->whatsapp_id.'"><img src="images/user/4(1).png"></a>';
								}
							?>
						</p>
					</div><div class="clear"></div>

				</div>
              

              <?php
                 	}
				  }
              ?>


				<!-- <div class="item_seller_team">

					<div class="item_images_seller_team">
						<img src="../images/user/sell/victor.png">
					</div>

					<div class="item_info_seller_team">
						<h4 class="name">Mr.Victor</h4>
						<p class="phone">(+51)1-397-5721</p>
						<p class="email">victor@blauda.com</p>
						<p class="media"><a href="#"><img src="images/user/skype.png"></a> <a href="#"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>
					</div><div class="clear"></div>

				</div> -->


<!-- 
				<div class="item_seller_team">

					<div class="item_images_seller_team">
						<img src="../images/user/sell/jesus.png">
					</div>

					<div class="item_info_seller_team">
						<h4 class="name">Mr.Jesus</h4>
						<p class="phone">(+51)1-397-5721</p>
						<p class="email">jesus@blauda.com</p>
						<p class="media"><a href="#"><img src="images/user/skype.png"></a> <a href="#"><img src="images/user/facebook.png"></a> <a href="#"><img src="images/user/1.png"></a> <a href="#"><img src="images/user/4(1).png"></a></p>
					</div><div class="clear"></div>

				</div><div class="clear"></div>
 -->
			</div>

		</div>


</div>
