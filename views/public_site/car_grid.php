
<div class="main_calculate_price">

	<div class="header_calculate_price">

		<div class="head_left_calculate_price">
			
		</div>

		<div class="head_right_calculate_price">
			<p>Total Price Calculator</p>
		</div><div class="clear"></div>

	</div>

	<div class="content_calculate_price">
		
		<div class="content_left_calculate_price">

			<div class="note_price_calculate">
				<p>*** Total Price = Freight + Insurance + Inspection</p>	
			</div>

			<div class="main_price_calculate_form">
				
				<div class="price_calculate_form_line1">

					<div class="price_calculate_form_one" id="label_price_calculate_form">
						<p>Destination Port</p>
					</div>

					<div class="price_calculate_form">
						<select>
							<option>Select country</option>
						</select>
					</div>

					<div class="price_calculate_form">
						<select>
							<option>Select port</option>
						</select>
					</div><div class="clear"></div>

				</div>

				<div class="price_calculate_form_line1" id="price_calculate_form_line2">

					<div class="price_calculate_form_checkbox" id="form_checkbox">
						<input type="checkbox" name=""><label>Insurance</label>
						<input type="checkbox" name=""><label>Inspection</label> 
					</div>
                    <div class="clear">
               </div>

				</div>

				<div class="price_calculate_form_line1" id="price_calculate_form_line3">
                
					<div class="price_calculate_form_submit" id="form_button">
						<input type="submit" class="calculate" name="submit" value="Calculate"> 
					</div>

					<div class="clear"></div>

				</div>


			</div>
		
		</div>

		<div class="content_right_calculate_price">
			<div class="seller_profile">
				<div class="seller_profile_photo">
					<img src="../images/public_site/call-center-m.png">
				</div>
				<div class="seller_profile_desc">
					<p class="seller_profile_name">KENTA</p>
					<p class="seller_profile_phone">+855-16404032</p>
					<p class="seller_profile_email">info@motorbb.com</p>
				</div>
				<div class="seller_profile_media">

					<div class="profile_media_logo" id="profile_media_logo1">
						<img src="../images/public_site/fb.png">	
					</div>

					<div class="profile_media_logo">
						<img src="../images/public_site/viber.png">	
					</div>

					<div class="profile_media_logo">
						<img src="../images/public_site/what's_up.png">	
					</div><div class="clear"></div>

				</div>
			</div>
		</div><div class="clear"></div>

		
	</div>


	<div class="footer_calculate_price">
		<div class="footer_calculate_price_note">
			<p>Contact and let Motorbb's staffs check car's avilability</p>
			<p>and <span>secure your money.</span></p>
		</div>	
	</div>



</div>



<div class="main_content_cargrid">

	<div class="header_cargrid">

		<div class="header_left_cargrid">
			<div class="header_cargrid_stock">
				<ul>
					<li class="active"><a href="#">Japan Stocks</a></li>
					<li><a href="#">Korea Stocks</a></li>
				</ul>
			</div><div class="clear"></div>
		</div>

		<div class="header_right_cargrid">

			<div class="header_right_cargrid_tool">

				<div class="header_right_tool_grid">
					<a href="#"><img src="../images/public_site/toolgrid.png"> Grid View</a>
				</div>

				<div class="header_right_tool_list">
					<a href="#"><img src="../images/public_site/toollist.png"> List View</a>
				</div>

				<div class="header_right_total_product">
					<p>Total Products Found : <span>000</span></p>
				</div><div class="clear"></div>

			</div>

		</div><div class="clear"></div>

	</div>

	<div class="content_cargrid">

		<div class="content_row_cargrid">
			
			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div>

			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div>


			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div>

			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div>

			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div><div class="clear"></div>



		</div>	

	</div>



	<!-- Line -->

		<div class="content_cargrid">

		<div class="content_row_cargrid">
			
			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>
			</div>

			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div>


			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div>

			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div>

			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div><div class="clear"></div>



		</div>	

	</div>


	<!-- Line -->

		<div class="content_cargrid">

		<div class="content_row_cargrid">
			
			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>
			</div>

			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div>


			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div>

			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>

				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div>

			<!-- Line -->

			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<img src="../images/public_site/image_item.png">
				</div>

				<div class="item_cargrid_title">
					<p>2010 HYUNDAI SOTANA</p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price">US $100,00</p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>
					<p class="total_price">ASK</p>
				</div>
				
				<div class="item_cargrid_contact">
					<a href="#">Contact</a>
				</div>

			</div><div class="clear"></div>



		</div>	

	</div>


	
</div>

<div class="pagination">
		<ul class="paginnav">
			<li><a href="#"><<</a></li>
			<li><a href="#">Previous</a></li>
			<li class="active"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">Next</a></li>
			<li><a href="#">>></a></li>
		</ul>
	</div><div class="clear"></div>
