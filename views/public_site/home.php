
<!-- start home slide -->
<div class="wrapper_slide">
<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
	<div class="ws_images"><ul>
			<li><a href="https://play.google.com/store/apps/details?id=com.motorbb" target="_blank"><img src="/images/public_site/home_site/newslide1.jpg" id="wows1_0"/></a></li>
			<li><img src="/images/public_site/home_site/newslide2.jpg" id="wows1_1"/></li>
			<li><img src="/images/public_site/home_site/newslide4.jpg" id="wows1_2"/></li>
		</ul></div>
	<div class="ws_shadow"></div>
</div>
</div>
<!-- end home slide -->

<!-- start home search -->
<div class="wrapper_home_search">
  <form action="/?c=user&m=car_list" method="post">
	<div class="search_title">
		<div class="title_left">
			<img src="/images/public_site/icon_search.png">
			<span>SEARCH CARS</span>
		</div>
	</div>

	<div class="wrapper_box_search">
		<div class="box">

			<select id="mark" name="carmake" class="make select_long">
				<option value="">Make</option>
				<?php if(count($carmaketodropdownsearch) > 0){ foreach($carmaketodropdownsearch as $carmaketodropdownsearchs){ ?>
                <option value="<?php echo $carmaketodropdownsearchs->car_make; ?>"<?php if(isset($_GET['car_make'])){ if($_GET['car_make']==$carmaketodropdownsearchs->car_make){ echo "selected";}}?> >
                <?php echo $carmaketodropdownsearchs->car_make; ?></option>
              <?php }} ?>
		    </select>

		    <select id="carminyear" name="searchminyear" class="min_year select_short select_margin">
				<option value="">Min Year</option>
				
				<?php for($i=date("Y"); $i>=1990; $i--){ ?>
	            	<option <?php if(isset($_GET['min_model_year'])){ if($_GET['min_model_year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
	            <?php }  ?>

		    </select>

		    <select name="searchmaxyear" class="max_year select_short">
				<option value="">Max Year</option>
				<?php for($i=date("Y"); $i>=1990; $i--){ ?>
        			<option <?php if(isset($_GET['max_model_year'])){ if($_GET['max_model_year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
      			<?php } ?>
		    </select>

		</div>

		<div class="box">
			<select id="series" name="carmodel" class="model select_long">
				<option value="">Model</option>
				<?php foreach($carmodeltodropdownsearch as $carmodeltodropdownsearchs){ ?>
                <option class="<?php echo $carmodeltodropdownsearchs->car_make; ?>" value="<?php echo $carmodeltodropdownsearchs->car_model; ?>"<?php if(isset($_GET['car_model'])){ if($_GET['car_model']==$carmodeltodropdownsearchs->car_model){ echo "selected";}}?> >
                <?php echo $carmodeltodropdownsearchs->car_model; ?></option>
              <?php } ?>
			</select>
			<input type="text" onkeydown="return isNumberKey(event)" name="searchminprice" value="<?php if(isset($_GET['min_fob_cost'])){ echo $_GET['min_fob_cost']; } ?>" placeholder="Min Price" class="min_price text_box_short select_margin"/>
			<input type="text" onkeydown="return isNumberKey(event)" name="searchmaxprice" value="<?php if(isset($_GET['max_fob_cost'])){ echo $_GET['max_fob_cost']; } ?>" id="carmaxrpice" placeholder="Max Price" class="max_price text_box_short "/>
		</div>

		<div class="box">
			<!-- <select name="searchcountry" id="carcountry" class="country select_long">
				<option value="">Country</option>
				<?php foreach($carcountrytodropdownsearch as $carcountrytodropdownsearchs){ ?>
                  <option <?php if(isset($_GET['country'])){ if($_GET['country']==$carcountrytodropdownsearchs->cc){ ?> Selected <?php } } ?> value="<?php echo $carcountrytodropdownsearchs->cc; ?>"><?php echo $carcountrytodropdownsearchs->com; ?></option>
                <?php } ?>
		    </select> -->
		   	<select name="search_drive_type" id="car_drive_type" class="search_drive_type select_long">
				<option value="">Drive Type</option>
				<?php foreach(Array('2WD', '4WD') as $Val ) { ?>
                	<option value="<?= $Val ?>"><?= $Val ?></option>
                <?php } ?>
		    </select>
		    <input type="text" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];} ?>" name="keyword" id="keyword" placeholder="Keyword" class="keyword text_box_long"/>
		</div>


		
	</div>
	<div class="wrapper_search-bottom">
		<div class="bottom_left">
		<!-- 	<input type="text" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];} ?>" name="keyword" id="keyword" placeholder="Keyword" class="keyword text_box_long"/>
		<div class="calculate_form_checkbox calculate_form_checkbox_color">
			<input type="checkbox" class="discount" value="0" name="discount"><label>Promotion</label>
		</div> -->
		</div>

		<div class="bottom_right">
			<input type="button" name="searchboxcar" id="button_search" class="btn_search" value="Search"/>
			<input type="button" onclick="javascript:location.href = '/?c=user&m=public_home';" class="btn_reset" value="Reset"/>
		</div>
		<div class="discount_total_products">Total products : <span class="discount_total_number">
			
                <?php echo $total_bbs_count ?>
		</span> </div>
		
	</div>
 </form>

</div>
<!-- end home search -->

<div class="wrapper_content_list">
	<div class="side_bar">
		<div class="border_side_bar">
			<img src="/images/public_site/home_bar_left.png"/>
			<div class="text_title">
				<span>MOST POPULAR CARS</span>
			</div>
		</div>

		<div class="arrow_bar">
			<a rel="canonical" href="?c=user&m=car_list&car=popular"><img src="/images/public_site/arrow_bar.png" border="0"/></a>
		</div>
	</div>
	
	<div class="list_cars">
    	<?php
		
			foreach($premium_cars as $row){

		?>
        <?php
			$BbsLinkRsv = './?c=user';
			$BbsLinkRsv .= '&m=cardetail';
			$BbsLinkRsv .= '&idx='.$row->idx;
			$img_row = $row->base_url . "thumb/".$row->fileimgname;
          	$img_rowb = $row->base_url . "".$row->fileimgname;
		?>
        <?php

		  $carmodelyear = "";
		  //echo $row->car_model_year; 
		  if($row->car_model_year != 0){

			$carmodelyear = $row->car_model_year;
		  }

			$carmakestr2 = $carmodelyear ." ".$row->car_make ."<br/>". $row->car_model;

			$chassisno = substr($row->car_chassis_no, -6);
		?>

                                              
		<div class="cars_box">
			<div class="image_box">
			<?php 
			if($row->old_fob_cost > $row->car_fob_cost){ 
			?>
			<div class="bg_discount_top">
				<div class="discount_save">Save</div>
				<div class="discount_fob"><?php echo ($row->old_fob_cost > $row->car_fob_cost ? '$'.number_format($row->old_fob_cost - $row->car_fob_cost) : ''); ?></div>
			</div>
			<?php } ?>
            	<a rel="canonical" href="?c=user&m=cardetail&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>">
				<img class="image" alt="<?php echo $row->car_make ." ". $row->car_model ." ". $row->car_model_year;?>" title="<?php echo $row->car_make ." ". $row->car_model ." ". $row->car_model_year;; ?>"  src="<?php echo ($row->fileimgname !='' ? $img_row : "/images/user/no_image.png"); ?> " border="0"/>
				<?php if($row->icon_status=="soldout"){
                   echo' <img class="sold_out" src="/images/user/sold-out.png"/>';
                }
               if($row->icon_status=="reserved"){
                   echo' <img class="sold_out" src="/images/user/reserved.png"/>';
                }
               ?>
				</a>               
			</div>
			<div class="cars_text">
				<div class="car_stock_no">Stock ID : <?php echo $row->car_stock_no; ?></div>
				<div class="cars_title">
					<span>
						<a href="?c=user&m=cardetail&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>">
							<?php echo $carmakestr2; ?>
						</a>
                    </span>
				</div>
				<div class="fob_discount">
					<?php echo ($row->old_fob_cost > $row->car_fob_cost ? number_format($row->old_fob_cost).' USD' : ''); ?>
				</div>
				<div class="car_price">
					<span><?php echo ($row->car_fob_cost !=0 ? number_format($row->car_fob_cost) ." ".$row->car_fob_currency : 'ASK'); ?></span>
				</div>
			</div>
		</div>
        <?php } ?>
		<div style="clear:both"></div>
	</div>




	<div class="side_bar">
		<div class="border_side_bar">
			<img src="/images/public_site/home_bar_left.png"/>
			<div class="text_title">
				<span>NEW & UPDATED CARS</span>
			</div>
		</div>

		<div class="arrow_bar">
			<a rel="canonical" href="?c=user&m=car_list"><img src="/images/public_site/arrow_bar.png" border="0"/></a>
		</div>
	</div>

	<div class="list_cars">
    	<?php
			  $count = 1;
			  //var_dump($get_cars);
				foreach($get_cars as $row){
	
			  if ($count%5 == 1)
			  {  
				   echo "<div class='main-block-item-car'>";
			  }
			   $carmodelyear = "";
		  //echo $row->car_model_year; 
		  if($row->car_model_year != 0){

			$carmodelyear = $row->car_model_year;
		  }
										
				$img_row = $row->base_url . "thumb/" . $row->fileimgname;

            	$carmakestr2 = $carmodelyear ." ".$row->car_make ."<br/>". $row->car_model;
		?>
		<div class="cars_box">
			<?php 
			if($row->old_fob_cost > $row->car_fob_cost){ 
			?>
			<div class="bg_discount_top">
				<div class="discount_save">Save</div>
				<div class="discount_fob"><?php echo ($row->old_fob_cost > $row->car_fob_cost ? '$'.number_format($row->old_fob_cost - $row->car_fob_cost) : ''); ?></div>
			</div>
			<?php
			}
			?>
			<div class="image_box">
            	<a rel="canonical" href="?c=user&m=cardetail&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>">
				<img class="image" alt="<?php echo $row->car_make ." ". $row->car_model." ". $row->car_model_year;; ?>" title="<?php echo $row->car_make ." ". $row->car_model." ". $row->car_model_year;; ?>" src="<?php echo ($row->fileimgname !='' ? $img_row : "/images/user/no_image.png"); ?>" border="0"/>
				</a>
				
			</div>
			<div class="cars_text">
				<div class="car_stock_no">Stock ID : <?php echo $row->car_stock_no; ?></div>
				<div class="cars_title">
					<span>
                    	<a href="?c=user&m=cardetail&idx=<?php echo $row->idx; ?>"
                    	id="<?php echo $row->idx; ?>"><?php echo $carmakestr2; ?></a>
                    </span>
				</div>
				<div class="fob_discount"><?php echo ($row->old_fob_cost > $row->car_fob_cost ? number_format($row->old_fob_cost).' USD' : ''); ?></div>
				<div class="car_price">
					<span><?php echo ($row->car_fob_cost !=0 ? number_format($row->car_fob_cost) ." ".$row->car_fob_currency : 'ASK'); ?></span>
				</div>
			</div>
		</div>
        <?php 
			
			if ($count%5 == 0)
			  {  
				   echo "</div>";
			  }
			  
			  
			  $count++;
		
		} 
		
		
		?>
        
        <?php if ($count%5 != 1) echo "</div>"; ?>
		<div style="clear:both"></div>
	</div>

	<div class="bottom_ads" style="width: 100%;">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Adsence Home -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-2405391128747022"
		     data-ad-slot="4863711394"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>
</div>
