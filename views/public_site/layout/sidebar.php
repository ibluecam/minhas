<!-- <div class="sidebar_country">
	<div class="sidebar_by_country bg_title">BY COUNTRY</div>
    <div class="wrapp_country">
        <div class="sidebar_japan">
        	<a href="?c=user&m=car_list&country=jp&page=1">
                <div class="sidebar_japan_img"><img src="/images/flags/32/JP.png" border="0"></div>
                <div class="sidebar_japan_text">JAPAN (<?php echo $countcarjapan; ?>)</div>
            </a>
        </div>
        <div style="clear: both;"></div>
        <div class="sidebar_japan_img_text">
        	<a href="?c=user&m=car_list&country=kr&page=1">
                <div class="sidebar_japan_img"><img src="/images/flags/32/KR.png" border="0"></div>
                <div class="sidebar_japan_text">KOREA (<?php echo $countcarkorea; ?>)</div>
            </a>
        </div>
        <div style="clear: both;"></div>
        <div class="sidebar_japan_img_text">
            <a href="?c=user&m=car_list&country=th&page=1">
                <div class="sidebar_japan_img"><img src="/images/flags/32/TH.png" border="0"></div>
                <div class="sidebar_japan_text">THAILAND (<?php echo $countcarthailand; ?>)</div>
            </a>
        </div>
        <div style="clear: both;"></div>
        <div class="sidebar_japan_img_text">
            <a href="?c=user&m=car_list&country=hk&page=1">
                <div class="sidebar_japan_img"><img src="/images/flags/32/HK.png" border="0"></div>
                <div class="sidebar_japan_text">HONG KONG (<?php echo $countcarhongkong; ?>)</div>
            </a>
        </div>
    </div>
</div> -->


<!--<div class="sidebar_search_cars">
	<div class="wrapp_sidebar_search_cars">
    	<div class="sidebar_search">SEARCH CARS</div>
        <form action="/?c=user&m=car_list" method="post">
        <div class="select_make">
            <select align="absmiddle" class="sidebar_search_cars_make" id="sidebar_mark" name="carmake">
            	<option value="">Make</option>
                <?php if(count($carmaketodropdownsearch) > 0){ foreach($carmaketodropdownsearch as $carmaketodropdownsearchs){ ?>
                <option <?php if(isset($_GET['car_make'])){ if($_GET['car_make']==$carmaketodropdownsearchs->car_make){ echo "selected";}}?> value="<?php echo $carmaketodropdownsearchs->car_make; ?>">
                <?php echo $carmaketodropdownsearchs->car_make; ?></option>
              <?php }} ?>
            </select>
        </div>
        <div class="select_make">
            <select align="absmiddle" class="sidebar_search_cars_make" id="sidebar_series" name="carmodel">
            	<option value="">Model</option>
                <?php foreach($carmodeltodropdownsearch as $carmodeltodropdownsearchs){ ?>
                <option class="<?php echo $carmodeltodropdownsearchs->car_make; ?>" value="<?php echo $carmodeltodropdownsearchs->car_model; ?>"<?php if(isset($_GET['car_model'])){ if($_GET['car_model']==$carmodeltodropdownsearchs->car_model){ echo "selected";}}?> >
                <?php echo $carmodeltodropdownsearchs->car_model; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="select_make">
            <select align="absmiddle" class="sidebar_search_cars_make"  name="searchcountry" id="carcountry">
            	<option value="">Country</option>
                <?php foreach($carcountrytodropdownsearch as $carcountrytodropdownsearchs){ ?>
                  <option <?php if(isset($_GET['country'])){ if($_GET['country']==$carcountrytodropdownsearchs->cc){ ?> Selected <?php } } ?> value="<?php echo $carcountrytodropdownsearchs->cc; ?>"><?php echo $carcountrytodropdownsearchs->com; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="select_make">
            <select align="absmiddle" class="sidebar_search_cars_min_year_left" id="carminyear" name="searchminyear">
            	<option value="">Min Year</option>
                <?php for($i=date("Y"); $i>=1990; $i--){ ?>
                    <option <?php if(isset($_GET['min_model_year'])){ if($_GET['min_model_year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php }  ?>
            </select>
            <select align="absmiddle" class="sidebar_search_cars_min_year" name="searchmaxyear">
            	<option value="">Max Year</option>
                <?php for($i=date("Y"); $i>=1990; $i--){ ?>
                    <option <?php if(isset($_GET['max_model_year'])){ if($_GET['max_model_year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="select_make">
            <input type="text" onkeydown="return isNumberKey(event)" name="searchminprice" value="<?php if(isset($_GET['min_fob_cost'])){ echo $_GET['min_fob_cost']; } ?>" class="sidebar_search_cars_max_year_left" placeholder="Min Price">
            <input type="text" onkeydown="return isNumberKey(event)" name="searchmaxprice" class="sidebar_search_cars_max_year" value="<?php if(isset($_GET['max_fob_cost'])){ echo $_GET['max_fob_cost']; } ?>" placeholder="Max Price">
        </div>
        <div class="select_make">
            <input type="text" name="keyword" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];} ?>" class="sidebar_search_long" placeholder="Keyword">
        </div>
        <div class="select_make">
            <input type="submit" name="searchboxcar" class="sidebar_btn_search" value="Search">
            <input type="submit" class="sidebar_btn_reset" value="Reset">
        </div>
        
    </form>
    </div>
    <div class="clear"></div>
</div>-->


<div class="sidebar_brand">
	<div class="sidebar_by_country bg_title">BY BRAND</div>
    <div class="wrapp_country">
    	<?php foreach($get_brand as $make){ ?>
        <div class="sidebar_brand_img_text <?php if(($action == "car_list") and ($param == $make->make_name)){ ?>active<?php } ?>">
        	<a href="?c=user&m=car_list&car_make=<?php echo $make->make_name; ?>&page=1">
            	<?php
                  if( ($make->icon_name=='' || $make->icon_name==NULL) &&  $make->c_brand <=0 ){
                    echo "";
                  }
                  else{
                ?>
                <div class="sidebar_japan_img" id="<?php echo $make->id; ?>" a="<?php echo $make->make_name; ?>">
                    <img src="<?php echo $make->icon_name; ?>" width="40" height="28" border="0">
                </div>
                <div class="sidebar_brand_text"><?php echo strtoupper($make->make_name);  ?> (<?php echo $make->c_brand; ?>)</div>
                <?php
                 }
                ?>
            </a>
        </div>
        <?php }; ?>
        <div class="clear"></div>
    </div>
</div>

<div class="sidebar_brand">
	<div class="sidebar_by_country bg_title">BY TYPE</div>
    <div class="wrapp_country">
    	<?php foreach($getbody_type as $getbodytype){ ?>
        <div class="sidebar_type_img_text <?php if(($action == "car_list") and ($param == strtoupper($getbodytype->body_name))){ ?>active<?php } ?>">
        	<a href="?c=user&m=car_list&car_body_type=<?php echo $getbodytype->body_name; ?>&page=1">
                <div class="sidebar_japan_img" id="<?php echo $getbodytype->id; ?>" a="<?php echo $getbodytype->body_name; ?>">
                	<img src="/images/user/compo/bt/<?php echo $getbodytype->icon; ?>.png" class="type_img" width="50" height="21" border="0">
                </div>
                <div class="sidebar_brand_text">
					<?php echo strtoupper($getbodytype->body_title);  ?> (<?php echo $getbodytype->countbodytype; ?>)
                </div>
            </a>
        </div>
        <?php }; ?>
        <div class="clear"></div>
    </div>
</div>

<div class="sidebar_fabook">
	<div id="fb-root">
      <div class="fb-page" data-href="https://www.facebook.com/sb.motorbb" data-height="410" data-width="263" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true">
          <div class="fb-xfbml-parse-ignore">
              <blockquote cite="https://www.facebook.com/sb.motorbb">
                <a href="https://www.facebook.com/sb.motorbb" id="getpagefb"></a>
              </blockquote>
          </div>
      </div>
   </div>
</div>
