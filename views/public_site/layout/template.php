<!doctype html>
<html class="no-js" lang="en">
  	<head>
    <meta charset="utf-8" />
    <meta content='width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=1;' name='viewport' />
	  
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="Expires" content="-1" />
      <meta http-equiv="Pragma" content="no-cache" />
      <meta http-equiv="Cache-Control" content="no-cache" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
 	    <meta property="fb:app_id" content="1464028390517067">
      <meta property="og:type" content="website">
      <meta property="og:url" content="<?php  echo $current_url; ?>">	
      <meta property="og:title" content="<?php echo $meta_title; ?>">
      
      <meta property="og:description" content="<?php echo $meta_desc; ?>">
      <meta property="og:author" content="No. 1 B2B Website For Car Trading">
      <meta property="og:image" content="<?php echo $meta_images; ?>">
      <meta property="og:image:url" content="<?php echo $meta_images; ?>">
      <meta property="og:image:width" content="500">
      <meta property="og:image:height" content="280"> 
      <meta property="title" content="<?php echo $meta_title; ?>">
      <meta property="description" content="<?php echo $meta_desc; ?>">
      <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="/css/public_site/global.css?v=<?= $upload_version ?>" />
      <link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
      <?php 
          /*** OUTPUT EXTERNAL CSS LINK ***/
          foreach($arr_ex_css as $ex_css){ 
              echo '<link rel="stylesheet" href="'.$ex_css.'" />';
          } 
      ?>
      <link rel="stylesheet" href="/css/public_site/lightbox.css?v=<?= $upload_version ?>" />
      <script src="/js/jquery-1.11.3.min.js?v=<?= $upload_version ?>"></script>
   	  <script type="text/javascript">
    		  <?php if($allow_fb_graph_js){ ?>
    		  $.post(
      			'https://graph.facebook.com',
      			{
      			  id: '<?php echo $current_url; ?>',
      			  scrape: true
      			},
      			function(response){
      			  //console.log(response);
      			}
    		  ); 
          <?php } ?>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-52212332-1', 'auto');
          ga('send', 'pageview');
    	  </script>
<!--End chearado start share facebook--> 
        
        <title><?php echo $title_head; ?></title>
        
  	</head>
  	
  
 

  	
  	<body>
  		<div style="display: none;"><!--SEO KEY-->
          <h1>Used cars in Korea</h1>
          <h1>Used cars in japan</h1>
           	
          <h1>Korea used cars</h1>
          <h1>Car in Korea</h1>
           	
          <p>Used cars in Korea,Used cars in japan,Selling used cars online in Korea,Used cars,New cars,Cars in Korea,Cars for sale, Minhas Trading</p>
      </div><!--/SEO KEY-->
  		<?php 
  			include('header.php'); 
        include('content.php');
  			include('footer.php'); 
  		?>

      <script type="text/javascript" src="/js/public_site/global.js?v=<?= $upload_version ?>"></script>

      <?php
          /*** OUTPUT EXTERNAL JS LINK ***/
          foreach($arr_ex_js as $ex_js){
              echo '<script type="text/javascript" src="'.$ex_js.'"></script>';
          }

      ?>

	</body>
</html>