
<div id="search_car_list" class="wrapper_car_list_search">
  <form action="/?c=user&m=car_list" method="post">
	<div class="search_title">
		<div class="title_left">
			<img src="/images/public_site/icon_search.png">
			<span>SEARCH CARS</span>
		</div>
	</div>

	<div class="wrapper_box_search">
		<div class="box">

			<select id="sidebar_mark" name="carmake" class="make select_long">
				<option value="">Make</option>
				<?php if(count($carmaketodropdownsearch) > 0){ foreach($carmaketodropdownsearch as $carmaketodropdownsearchs){ ?>
                <option value="<?php echo $carmaketodropdownsearchs->car_make; ?>"<?php if(isset($_GET['car_make'])){ if($_GET['car_make']==$carmaketodropdownsearchs->car_make){ echo "selected";}}?> >
                <?php echo $carmaketodropdownsearchs->car_make; ?></option>
              <?php }} ?>
		    </select>

		    <select id="carminyear" name="searchminyear" class="min_year select_short select_margin">
				<option value="">Min Year</option>
				
				<?php for($i=date("Y"); $i>=1990; $i--){ ?>
	            	<option <?php if(isset($_GET['min_model_year'])){ if($_GET['min_model_year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
	            <?php }  ?>

		    </select>

		    <select name="searchmaxyear" class="max_year select_short">
				<option value="">Max Year</option>
				<?php for($i=date("Y"); $i>=1990; $i--){ ?>
        			<option <?php if(isset($_GET['max_model_year'])){ if($_GET['max_model_year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
      			<?php } ?>
		    </select>
		</div>

		<div class="box">
			<select id="sidebar_series" name="carmodel" class="model select_long">
				<option value="">Model</option>
				<?php foreach($carmodeltodropdownsearch as $carmodeltodropdownsearchs){ ?>
                <option class="<?php echo $carmodeltodropdownsearchs->car_make; ?>" value="<?php echo $carmodeltodropdownsearchs->car_model; ?>"<?php if(isset($_GET['car_model'])){ if($_GET['car_model']==$carmodeltodropdownsearchs->car_model){ echo "selected";}}?> >
                <?php echo $carmodeltodropdownsearchs->car_model; ?></option>
              <?php } ?>
			</select>
			<input type="text" onkeydown="return isNumberKey(event)" name="searchminprice" value="<?php if(isset($_GET['min_fob_cost'])){ echo $_GET['min_fob_cost']; } ?>" placeholder="Min Price" class="min_price text_box_short select_margin"/>
			<input type="text" onkeydown="return isNumberKey(event)" name="searchmaxprice" value="<?php if(isset($_GET['max_fob_cost'])){ echo $_GET['max_fob_cost']; } ?>" id="carmaxrpice" placeholder="Max Price" class="max_price text_box_short "/>
		</div>

		<div class="box">
			<!-- <select name="searchcountry" id="carcountry" class="country select_long">
				<option value="">Country</option>
				<?php foreach($carcountrytodropdownsearch as $carcountrytodropdownsearchs){ ?>
                  <option <?php if(isset($_GET['country'])){ if($_GET['country']==$carcountrytodropdownsearchs->cc){ ?> Selected <?php } } ?> value="<?php echo $carcountrytodropdownsearchs->cc; ?>"><?php echo $carcountrytodropdownsearchs->com; ?></option>
                <?php } ?>
		    </select> -->

	    	<select name="search_drive_type" id="car_drive_type" class="search_drive_type select_long">
					<option value="">Drive Type</option>
				<?php foreach(Array('2WD', '4WD') as $Val ) { ?>
	            	<option <?php if(isset($_GET['car_drive_type'])){ if($_GET['car_drive_type']==$Val){ ?> Selected <?php } } ?> value="<?= $Val ?>"><?= $Val ?></option>
	            <?php } ?>
		    </select>
		   <input type="text" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];} ?>" name="keyword" id="keyword" placeholder="Keyword" class="keyword text_box_long"/>
		</div>

		
	</div>

	<div class="wrapper_search-bottom">
		<div class="bottom_left">
			<!--  <input type="text" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];} ?>" name="keyword" id="keyword" placeholder="Keyword" class="keyword text_box_long"/> -->
			 <!--<div><span>Product Total : <?php echo $countcarlist;?></span></div> -->
		<!-- <div class="calculate_form_checkbox calculate_form_checkbox_color">
			<input type="checkbox" class="discount" value="0" name="discount"><label>Promotion</label>
		</div> -->
		</div>
		<div class="bottom_right">
			<input type="button" name="searchboxcar" id="button_search" class="btn_search" value="Search"/>
			<input type="button" onclick="javascript:location.href = '/?c=user&m=car_list&country=jp';" class="btn_reset" value="Reset"/>
		</div>
	</div>

	<!-- <div class="total_product_item">Product Total : <span><?php echo $countcarlist;?></span></div> -->
	<div class="discount_total_products">Total products : <span class="discount_total_number"><?php echo $countcarlist;?></span> </div>
		
 </form>

</div>

<div class="main_calculate_price">

	<div class="header_calculate_price">

		<div class="head_left_calculate_price">
			
		</div>

		<div class="head_right_calculate_price">
			<p>Total Price Calculator</p>
		</div><div class="clear"></div>

	</div>

	<div class="content_calculate_price">
		
		<div class="content_left_calculate_price">

			<form name="calculate_price" method="post" enctype="multipart/form-data">

				<!--<div class="note_price_calculate">
					<p>*** Total Price = Freight + Insurance + Inspection</p>	
				</div>-->

				<div class="main_price_calculate_form">
					
					<div class="price_calculate_form_line1">


						<!-- Start Block Country -->

						<div class="block_calculate_country">
							<div class="price_calculate_form_one" id="label_price_calculate_form">
								<p>Destination Country</p>
							</div>

							<div class="price_calculate_form" id="price_calculate_country">
								<select name="destination_country" id="destination_country">
									<option value="">Select country</option>

									<? foreach ($list_destination as $destination) { ?>

									<option value="<?= $destination->country_iso ?>" <?php if(isset($country_to) && $country_to==$destination->country_iso){ echo "selected";}?>><?= $destination->country_name ?></option>							
									<? } ?>

								</select>

								<div class="inspection_title" id="">
									Insurance
								</div>
								<div class="calculate_form_checkbox_left">
									
									
									<!-- <input type="checkbox" <?php if(isset($insurance)&&$insurance=='on' ) echo "checked"; ?> name="insurance"><label>Insurance</label> -->
									<input type="radio" class="radio_yes" value="true" name="insurance" <?php if(isset($insurance)&&$insurance==true ) echo "checked"; ?>> Yes &nbsp;&nbsp;&nbsp;&nbsp;
   									<input type="radio" class="radio_no" value="false" name="insurance" <?php if(isset($insurance)&&$insurance==false ) echo "checked"; ?> > No
									
								</div><div class="clear"></div>

							</div><div class="clear"></div>
						</div><!-- End Block Country -->





						<!-- Start Block Port -->
						<div class="block_calculate_port">
							<div class="price_calculate_form_one" id="label_price_calculate_form">
								<p>Destination Port</p>
							</div>

							<div class="price_calculate_form" id="price_calculate_port">
								<select name="port_name" id="port_name">
									<option value="">Select port</option>
									<?php
										foreach ($list_port as $row_list_port) {
									?>
									<option <?php if(isset($port_name) && $port_name==$row_list_port->id){ echo "selected";}?> class="<?= $row_list_port->country_iso ?>" value="<?= $row_list_port->id ?>"><?= $row_list_port->port_name ?></option>
									<?php  } ?>
								</select>
								<div class="inspection_title" id="">
									Inspection
								</div>
								<div class="calculate_form_checkbox_left">
									<!-- <input type="checkbox" <?php if(isset($inspection)&&$inspection=='on' ) echo "checked"; ?> name="inspection"><label>Inspection</label> -->
									<input type="radio" class="radio_yes" value="true" <?php if(isset($inspection)&&$inspection==true ) echo "checked"; ?> name="inspection"> Yes &nbsp;&nbsp;&nbsp;&nbsp;
   									<input type="radio" class="radio_no" value="false" <?php if(isset($inspection)&&$inspection==false ) echo "checked"; ?>  name="inspection"> No
								</div><div class="clear"></div>

							</div>

							<div class="clear"></div>

						</div><!-- End Block Port -->
							
					</div>

					<!--<div class="price_calculate_form_line1" id="price_calculate_form_line2">

						<div class="price_calculate_form_checkbox" id="form_checkbox">
							<input type="checkbox" <?php if(isset($insurance)&&$insurance=='on' ) echo "checked"; ?> name="insurance"><label>Insurance</label>
							<input type="checkbox" <?php if(isset($inspection)&&$inspection=='on' ) echo "checked"; ?> name="inspection"><label>Inspection</label> 
						</div>
	                    <div class="clear"></div>

					</div>-->

					<div class="price_calculate_form_line1" id="price_calculate_form_line3">
	                
						<div class="price_calculate_form_submit" id="form_button">
							<input type="submit" class="calculate" name="submit_calculate_price" value="Calculate"> &nbsp;
							<input type="submit" class="reset_list" name="submit_reset_price" value="Reset"> 
						</div>

						<div class="clear"></div>

					</div>


				</div>

			</form>

		</div>

		<div class="content_right_calculate_price">
			<div class="seller_profile">
				<?
				foreach ($sale_support as $support) {
				?>
				<div class="seller_profile_photo">
					
					<?php if($support->secure_url !="") { ?>	

						<img src="<?= $support->base_url.$support->public_id ?>"/>

				    <?php }else{ ?>

						<?php if($support->sex=='M'){ ?>
							
							<img src="/images/user/call-center-m.png"/>
						
						<?php } else{ ?>
							
							<img src="/imagesuser/call-center.png"/>
						
						<?php } ?>

					<?php } ?>
				
				</div>
				<div class="seller_profile_desc">
					<div class="seller_profile_name"><?= $support->member_first_name ?></div>
					<div class="seller_profile_phone">+<?= $support->phone_no1 ?>-<?= $support->phone_no2 ?></div>
					<div class="seller_profile_email"><a href="mailto:<?= $support->email ?>"><?= $support->email ?></a></div>
				</div>
				<div class="seller_profile_media">
				<?php if(!empty($support->facebook_id)) { ?>
                    <a class="tooltip" title="Facebook: &lt;b&gt;<?= $support->facebook_id?>&lt;/b&gt;" target="_blank" href="<?php echo $support->facebook_id; ?>"><img src="/images/public_site/fb.png" border="0" /></a>
                <?php } 
                    if(!empty($support->skype_id)) {
                ?>
                    <a class="tooltip" title="Skype ID: &lt;b&gt;<?= $support->skype_id?>&lt;/b&gt;" href="skype:'<?php echo $support->skype_id; ?>'?add"><img src="/images/user/skype.png" /></a>
                <?php }
                    if(!empty($support->whatsapp_id)) {
                ?>
                    <a class="tooltip" title="Viber: &lt;b&gt;<?= $support->whatsapp_id?>&lt;/b&gt;" href="#"><img src="/images/public_site/what's_up.png" style="height: 30px;" /></a>
                <?php } ?>


				</div>
			<?
			}
			?>
			</div>
		</div><div class="clear"></div>

		
	</div>


	<!--<div class="footer_calculate_price">
		<div class="footer_calculate_price_note">
			<p>Contact and let Motorbb's staffs check car's avilability</p>
			<p>and <span>secure your money.</span></p>
		</div>	
	</div>-->



</div>



<div class="main_content_cargrid">

	<div class="header_cargrid">

		<div class="header_left_cargrid">
			<div class="header_cargrid_stock">
				<ul>
					<li <?php if($all_stock){ ?>class="mobile_car_list_japan_stocks_a <?php echo $all_stock; ?>"<?php } ?>><a href="/?c=user&m=car_list<?php echo $set_url_popular; ?>">All Stocks</a></li>
					<li <?php if($active_stockcountry_jp){ ?>class="mobile_car_list_japan_stocks_a <?php echo $active_stockcountry_jp; ?>"<?php } ?>><a class="clickstock" id="jp">Japan Stocks</a></li>
					<li <?php if($active_stockcountry_kr){ ?>class="mobile_car_list_korea_stocks_a <?php echo $active_stockcountry_kr; ?>"<?php } ?>><a class="clickstock" id="kr">Korea Stocks</a></li>
					<!-- <li <?php if($active_stockcountry_th){ ?>class="mobile_car_list_thailand_stocks_a <?php echo $active_stockcountry_th; ?>"<?php } ?>><a class="clickstock" id="th">Thailand</a></li>
					<li <?php if($active_stockcountry_hk){ ?>class="mobile_car_list_hongkong_stocks_a <?php echo $active_stockcountry_hk; ?>"<?php } ?>><a class="clickstock" id="hk">Hong Kong</a></li> -->
					<!--<li <?php if($active_stockcountry_jp){ ?>class="<?php echo $active_stockcountry_jp; ?>"<?php } ?>><a href="/?c=user&m=car_list&country=jp" id="jp">Japan Stocks</a></li>
					<li <?php if($active_stockcountry_kr){ ?>class="<?php echo $active_stockcountry_kr; ?>"<?php } ?>><a href="/?c=user&m=car_list&country=kr" id="kr">Korea Stocks</a></li>-->
				</ul>
			</div><div class="clear"></div>
		</div>

		<div class="header_right_cargrid">

			<div class="header_right_cargrid_tool">

				<div class="header_right_tool_grid">
				<!-- <a class="changelist <?php if($action_stock_car_grid == "block"){ ?>active<?php } ?>" id="cargrid"><img src="../images/public_site/toolgrid.png"> Grid View</a> -->
				</div>

				<div class="header_right_tool_list">
					<!-- <a class="changelist <?php if($action_stock_car_list == "block"){ ?>active<?php } ?>" id="carlist"><img src="../images/public_site/toollist.png"> List View</a> -->
				</div>

				<div class="header_right_total_product">
					<p>Total Products Found : <span><?php echo $countcarlist; ?></span></p>
				</div><div class="clear"></div>

			</div>

		</div><div class="clear"></div>

	</div>


	<div class="content_itemlist" id="car_list">

		<table class="tablelist_item" border="0" width="100%">
			<tr>
				<th width="425px">Car Information</th>
				<th width="90px"><div>Year</div>
					<!--<div class="wrapp_car_list_year">
						
						<div class="car_list_poin_small"><img src="images/public_site/poin_small.png"></div>
						<div class="car_list_link"> <img src="images/public_site/car_list_link.png"></div>
						<div class="car_list_poin_big"> <img src="images/public_site/poin_big.png"></div>
					</div>-->
				</th>
				<!--<th width="90px">
					<div>Mileage</div>
					<div class="wrapp_car_list_year">
					<div class="car_list_poin_small"><img src="images/public_site/poin_small.png"></div>
					<div class="car_list_link"> <img src="images/public_site/car_list_link.png"></div>
					<div class="car_list_poin_big"> <img src="images/public_site/poin_big.png"></div>
					</div>
				</th>-->
				<th width="90px">
					<div class="th_fob">FOB</div>
					<!--<div class="wrapp_car_list_right">
					<div class="car_list_poin_small"><img src="images/public_site/poin_small.png"></div>
					<div class="car_list_link"> <img src="images/public_site/car_list_link.png"></div>
					<div class="car_list_poin_big"> <img src="images/public_site/poin_big.png"></div>
					</div>-->
				</th>
				<th width="90px"><div class="th_total">Total</div></th>
			</tr>



			<?php

		     	foreach($carlist as $row){ 

			        $BbsLinkRsv = './?c=user';
			        $BbsLinkRsv .= '&m=cardetail';
			        $BbsLinkRsv .= '&idx='.$row->idx;
					  
					$img_row = $row->base_url . "thumb/".$row->fileimgname;


					//======== Car title ========//
					$carmodelyear = "";
                    if($row->car_model_year != 0 and $row->car_model_year != ""){
          
                      $carmodelyear = $row->car_model_year;
                    }


                    $carmakestr2 = $row->car_make ." ". $row->car_model;

                    
					//======== Car title ========//

                   	//======== Car steering ========//
                    	$steering = $row->car_steering;
                    //======== Car Steering ========//

                    //======== Car Condition =======//
                    	$carcondition = "";
                    	if($row->icon_new_yn == "Y"){
							$carcondition = "New";
						}else{ 
							$carcondition = "Used";
						}

					// car_stock_no
					$car_mileage = "N/A";

					if(($row->car_mileage != 0) and ($row->car_mileage != "")){
          
                      $car_mileage = number_format($row->car_mileage)." km";
                    }

                    // car_cc
					$car_cc = "N/A |";

					if(($row->car_cc != 0) and ($row->car_cc != "")){
          
                      $car_cc = $row->car_cc." cc |";
                    }

                    // car_stock_no
					$car_stock_no = "N/A";

					if(($row->car_stock_no != 0) and ($row->car_stock_no != "")){
          
                      $car_stock_no = $row->car_stock_no;
                    }


                     // car_chassis_no
					$car_chassis_no = "N/A";
					if($row->country == "jp"){
						
						if($row->car_chassis_no != ""){
          
	                      $car_chassis_no =  substr($row->car_chassis_no, 0,6);

	                    }

					}else{

						if($row->car_chassis_no != ""){
          
	                      $car_chassis_no =  substr(strrev($row->car_chassis_no) , 0 ,6);

	                      $car_chassis_no = strrev($car_chassis_no);
						}

                    }

                  
                    // car_transmission
					$car_transmission = "N/A";

					if($row->car_transmission != ""){
          
                      $car_transmission = $row->car_transmission;

                    }
                    // car_steering
					$car_steering = "";

                    if($row->car_steering == "LHD"){ 
                    	$steering = "Left hand";
                    }else{ 
                    	$steering = "Right hand";
                    }

			      
      		?>

			<!-- Line -->
			<tr>

				<td>
					
					<div class="list_item_photo">
						<a href="/?c=user&m=cardetail&idx=<?php echo $row->idx; ?>">
						<?php if ($row->fileimgname != ''){ ?>
			            
			               <img class="imgs" title="<?php echo $row->car_make ." ". $row->car_model." ". $row->car_model_year; ?>" alt="<?php echo $row->car_make ." ". $row->car_model." ". $row->car_model_year; ?>" src="<?php echo $img_row; ?>"/>
			     		
			     		<?php }else{ ?>
			            
			               <img class="home_image_defaults" alt="default" src="/images/user/no_image.png"/>
                      	
                      	<?php } ?>
						
						<?php 
							if($row->icon_status=="soldout"){
			                         echo' <img class="sold_out_list" src="/images/user/sold-out.png"/>';
			                }
							if($row->icon_status=="reserved"){
			                         echo' <img class="sold_out_list" src="/images/user/reserved.png"/>';
			                }
						?>

                      	</a>
					</div>

					<div class="list_item_desc">
						<p class="title"><a href="/?c=user&m=cardetail&idx=<?php echo $row->idx; ?>"><?php echo $carmakestr2; ?></a></p>
						<p><span>ChassisNo : </span> <?php echo $car_chassis_no; ?> | <span>Condition : </span><?php echo $carcondition; ?></p>
						<p><span>Mileage : </span> <?php echo $car_mileage; ?> | <span>Transmission : </span> <?php echo $car_transmission; ?></p>
						<p class="body_capital"><span>Steering : </span> <?php echo $car_steering; ?> | <span>Body Type : </span> <?php echo ($row->car_body_type !='' ? $row->car_body_type : 'N/A'); ?></p>
						<p><span>Stock ID :</span> <?php echo $row->car_stock_no; ?></p>
						<!--<p><span><div style="float:left;width: 25px;"><img src="images/public_site/icon_lefthand.png"></div></span><span><div style="overflow: hidden;padding-top: 1px;color: #009045;"><?php echo $steering; ?></div></span></p>-->

						<!-- <p><span>Mileage :</span> <?php echo ($row->car_mileage !='' && $row->car_mileage > 0 ? number_format($row->car_mileage) ." km" : 'N/A'); ?> | <span>Transmission :</span> <?php echo ($row->car_transmission !='' || $row->car_transmission > 0 ? $row->car_transmission : 'N/A'); ?></p>
						<p><span>Steering :</span> <?php echo $steering; ?> | <span>Body Type :</span> <?php echo ($row->car_body_type !='' ? $row->car_body_type : 'N/A'); ?></p> -->
					</div><div class="clear"></div>

				</td>
				<td>
					<div class="list_item_year">
						<p><?php echo $carmodelyear; ?></p>
					</div>
				</td>

				<!--<td>
					<div class="list_item_year">
						<p><?php echo $car_mileage; ?></p>
					</div>
				</td>-->

				<td>
					<?php if($row->old_fob_cost > $row->car_fob_cost){ ?>
					<?php 
						if($row->old_fob_cost > $row->car_fob_cost){ 
						?>
						<div class="bg_discount_top_list">
							<div class="discount_save_list">Save</div>
							<div class="discount_fob_list"><?php echo ($row->old_fob_cost !=0 ? '$'.number_format($row->old_fob_cost - $row->car_fob_cost) : ''); ?></div>
						</div>
					<?php } ?>
					<div class="fob_discount_list">
						<?php echo ($row->old_fob_cost > $row->car_fob_cost ? number_format($row->old_fob_cost).' USD' : ''); ?>
					</div>
					<?php } ?>
					<div class="list_item_fobprice">
						
						<p class="price"><?php echo ($row->car_fob_cost !=0 ? $row->car_fob_currency." ".number_format($row->car_fob_cost) : 'ASK'); ?></p>
					</div>
				</td>

				<td>
					<div class="list_item_totalprice">

						<!--<p style="color:#0772ba; margin-bottom:0px;">CIF + Inspection</p>-->
                        <p class="get_port_name"></p>

						<?php
						if(isset($calculate_price) && $calculate_price!='' || isset($get_port_name) && $get_port_name!=''){

                			echo '<p class="total_price">';
                			
	                        
	                            
	                            if ($list_total_price[$row->idx]>0) {
	                            	
	                            	echo $row->car_fob_currency." ".number_format($list_total_price[$row->idx]);
	                            
	                            }else{

	                            	echo "ASK";

	                            }
	                             
	                        
	                        
	                        echo '</p>';
	                    }else{
	                    	echo '<p class="total_price">ASK</p>';
	                    }
	                    ?>
					
						<!--<div class="item_cargrid_contact">
							<a href="/?c=user&m=cardetail&idx=<?php echo $row->idx; ?>">Negotiate</a>
						</div>-->
					
					</div>
				</td>

			</tr>

		<?php 
  
        	} //End Foreach

      	?>

			

			
		</table>


</div>



	<div style="display:none;" class="content_cargrid" id="car_grid">



		<?php
				//var_dump($carlist); 
			$count_add_div = 1;
	     	foreach($carlist as $row){ 

		        $BbsLinkRsv = './?c=user';
		        $BbsLinkRsv .= '&m=cardetail';
		        $BbsLinkRsv .= '&idx='.$row->idx;
				  
				$img_row = $row->base_url . "thumb/".$row->fileimgname;


				//======== Car title ========//
				$carmodelyear = "";
                if($row->car_model_year != 0 and $row->car_model_year != ""){
      
                  $carmodelyear = $row->car_model_year;
                }


                $carmakestr2 = $carmodelyear . " " . $row->car_make ." ". $row->car_model;

                
				//======== Car title ========//

				//======== Car Chesisses =====//

                	$chassisno = substr($row->car_chassis_no, -6);

				//======== Car Chesisses =======//


               	//======== Car steering ========//
                	$steering = $row->car_steering;
                //======== Car Steering ========//

                //======== Car Condition =======//
                	$carcondition = "";
                	if($row->icon_new_yn == "Y"){
						$carcondition = "New";
					}else{ 
						$carcondition = "Used";
					}


					if($count_add_div%5 == 1){
						echo '<div class="content_row_cargrid">';
					}
			      
      		?>
			
			<div class="item_cargrid">

				<div class="item_cargrid_photo">
					<a href="/?c=user&m=cardetail&idx=<?php echo $row->idx; ?>">
					<?php if ($row->fileimgname != ''){ ?>
			            
			               <img class="imgs" title="<?php echo $row->car_make ." ". $row->car_model." ". $row->car_model_year;?>"  alt="<?php echo $row->car_make ." ". $row->car_model." ". $row->car_model_year;?>" src="<?php echo $img_row; ?>" border="0"/>
			     		
			     		<?php }else{ ?>
			            
			               <img class="home_image_defaults" alt="default" src="/images/user/no_image.png" border="0" />
                      	
                     <?php } ?>

                     <?php 
							if($row->icon_status=="soldout"){
			                           echo' <img class="sold_out_grid" src="/images/user/sold-out.png"/>';
			                }
							if($row->icon_status=="reserved"){
			                           echo' <img class="sold_out_grid" src="/images/user/reserved.png" border="0"/>';
			                }
					?>

                     </a>
				</div>

				<div class="item_cargrid_title">
					<p><a href="/?c=user&m=cardetail&idx=<?php echo $row->idx; ?>"><?php echo $carmakestr2; ?></a></p>
				</div>

				<div class="item_cargrid_price">
					<p>FOB</p>
					<p class="fob_price"><?php echo ($row->car_fob_cost !=0 ? $row->car_fob_currency." ".number_format($row->car_fob_cost) : 'ASK'); ?></p>
				</div>

				<div class="item_cargrid_total_price">
					<p>Total Price</p>

					<?php

                		if(isset($_POST['submit_calculate_price']) || isset($calculate_price) && $calculate_price!=''){  

                			echo '<p class="total_price">';

	                        if(empty($row->car_fob_cost) || empty($row->country_to) || empty($row->car_width) || empty($row->car_height) || empty($row->car_length) )
	                        {
	                          echo $total_price = "ASK";
	                        }else{

	                            $frieght_cost = ceiling($row->shipping_cost * ($row->car_width/1000) * ($row->car_height/1000) * ($row->car_length/1000),50);

	                            if($insurance=='on' && $inspection=='on'){
	                                
	                               $total_price = $frieght_cost + $row->insurance + $row->inspection + $row->car_fob_cost + 500;
	                               
	                            }elseif($insurance=='off' && $inspection=='off'){
	                                
	                                $total_price = $frieght_cost + $row->car_fob_cost + 500;
	                            
	                            }
	                            elseif ($insurance=='on' && $inspection=='off') {  

	                                $total_price = $frieght_cost + $row->insurance + $row->car_fob_cost + 500;
	                            
	                            }elseif($insurance=='off' && $inspection=='on'){

	                                $total_price = $frieght_cost + $row->inspection + $row->car_fob_cost + 500;
	                            
	                            }

	                            echo $row->car_fob_currency." ".number_format($total_price);
	                            
	                        }

	                        echo '</p>';

	                    }else{
	                    	echo '<p class="total_price">ASK</p>';
	                    }
	                    ?>
				</div>

				<div class="item_cargrid_contact">
                
					<a href="/?c=user&m=cardetail&idx=<?php echo $row->idx; ?>">Contact</a>

				</div>

			</div>

			<!-- Line -->




			<?php 

				if($count_add_div%5 == 0){
					echo '<div class="clear"></div>';
				}

				if($count_add_div%5 == 0){
					echo '</div>';
				}

				$count_add_div++; 

			} 

				if($count_add_div%5 != 1){
					echo '<div class="clear"></div>';
				}

				if($count_add_div%5 != 1){

					echo '</div>';

				}

			?>



	</div>



	


 <?php

if($countcarlist > $perpagepagination){
      $totalpage = ceil($countcarlist / $perpagepagination);


    ?>
	<div class="pagination">
		<ul class="paginnav">
			<?php if($getnumpage > 1){ ?>
				<li><a class="clickpagination" id="1">&laquo;</a></li>
				<li><a class="clickpagination" id="<?php if($getnumpage == 1){ $numpage = $getnumpage; }else{ $numpage = $getnumpage - 1; } echo $numpage; ?>">Previous</a></li>
			<?php } ?>

			<?php

              if(($getnumpage-5) <= 0){
                  $currentpagenumber = 1;
              }else{
                  $currentpagenumber = $getnumpage-5;
              }

              if(($getnumpage+5) > $totalpage){
                  $totalcurrent = $totalpage;
              }else{
                  $totalcurrent = $getnumpage+5;
              }

          ?>
        
          <?php $n=1;for($i=$currentpagenumber; $i<=$totalcurrent; $i++){  ?>

               <li <?php if($i == $getnumpage){ ?> class="current" <?php } ?>><a class="<?php if($i == $getnumpage){ ?>not_click<?php } ?> clickpagination valuepagination_<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i;?></a></li>
          <?php } ?>

        <?php if($getnumpage != $totalpage){ ?>
			<li><a class="clickpagination" id="<?php if($getnumpage >= $totalpage){ $numpage = $getnumpage; }else{ $numpage = $getnumpage + 1; } echo $numpage; ?>">Next</a></li>
			<li><a class="clickpagination" id="<?php echo $totalpage; ?>">&raquo;</a></li>
		<?php } ?>

		</ul>
	</div><div class="clear"></div>
<?php } ?>

</div>
