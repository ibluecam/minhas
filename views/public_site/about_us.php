<div class="about_us_map">
	<img src="../images/public_site/motorbb_map.png">
</div>

<div class="main_content_about_us">

	<div class="header_about_us">
		
		<div class="header_left_about_us">
			
		</div>

		<div class="header_right_about_us">
			<p>About us</p>
		</div><div class="clear"></div>

	</div>


	<div class="main_content_about_us">
		
		<div class="main_content_left_about_us">
			
			<div class="desc_content_about_us">
				<h4 class="title_content_about_us">Company Information</h4>
				<p class="text_content_about_us">Motorbb is an online used car information providing company whose headquarter is located in Kawasaki, Japan. We are exporting cars from South Korea and Japan to Asia, Africa, Dominican Republic and Paraguay.</p>
				<p>Once a customer buys a car from our website, we take care of all the details for you, including shipping and documentation. Our sales team provides 24/7 service in order to achieve the highest customer satisfaction. We commit to provide convenient, quick and professional services to worldwide customers.</p>
				
				<div class="main_note_For_certificate">

					<!-- <p class="note_For_certificate">See our certificate from Japanese Government:</p> -->
					<p class="note_For_certificate">Company Licenses</p> 
					<div class="note_For_download_certificate">
						<img class="icon_download" src="../images/public_site/icon_pdf.png" /> 
						<a href="uploads/file/certificate of Motorbb from Japanese government.pdf" target="_blank">Click To Download</a>
					</div><div class="clear"></div>

				</div>

			</div>


			<div class="company_outline_about_us">
				<h4 class="title_content_about_us">Company Outline</h4>

				<div class="company_outline_about_us_table">
					
					<table width="100%" class="company_outline_table">
						<tr>
							<td class="company_outline_table_data_left">Company Name: </td>
							<td class="company_outline_table_data_right">Motorbb Co., LTD</td>
						</tr>

						<tr class="row_color">
							<td class="company_outline_table_data_left">Headquarter (Japan): </td>
							<td class="company_outline_table_data_right">Ark Hills Front Tower17F, 2-23-1 Akasaka, Minato-ku, Tokyo, Japan</td>
						</tr>

						<tr>
							<td class="company_outline_table_data_left">Branch (South Korea): </td>
							<td class="company_outline_table_data_right">E-1906B, Songdo Smart Valley, 30 Songdomirae-ro, Yeonsu-gu, Incheon,Korea</td>
						</tr>

						<tr class="row_color">
							<td class="company_outline_table_data_left">Branch (Cambodia): </td>
							<td class="company_outline_table_data_right">#B26, St.1019 Sangkat Toek Thlar, Khann Sensok Phnom Penh, Cambodia</td>
						</tr>

						<tr>
							<td class="company_outline_table_data_left">PhoneNumber (Japan): </td>
							<td class="company_outline_table_data_right"><p>+(81) 80-4095-0379</p></td>
						</tr>

						<tr class="row_color">
							<td class="company_outline_table_data_left">PhoneNumber (South Korea): </td>
							<td class="company_outline_table_data_right"><p>+(82) 32-264-5500</p></td>
						</tr>

						<tr>
							<td class="company_outline_table_data_left">PhoneNumber (Cambodia): </td>
							<td class="company_outline_table_data_right"><p>+(855) 16-4040-32</p></td>
						</tr>

						<tr class="row_color">
							<td class="company_outline_table_data_left">Foundation: </td>
							<td class="company_outline_table_data_right">April/2015</td>
						</tr>

						

						<tr class="row_color">
							<td class="company_outline_table_data_left">Business description: </td>
							<td class="company_outline_table_data_right">Export of automobiles</td>
						</tr>

					</table>

				</div>

			</div>

		</div>

		<div class="main_content_right_about_us">
			<div class="img_about_us">
				<img src="../images/public_site/aboutus1.jpg">
			</div>

			<div class="img_about_us">
				<img src="../images/public_site/aboutus2.jpg">
			</div>
		</div><div class="clear"></div>

	</div>





	


</div>

<br/><br/>
