
  <?php //=========================================== Content include ========================================== ?>


<div class="detail_wrapper">
<!---- car detail left start ---->

<div class="detail_content_left">
	<div class="title_detail">
		<?php
           if($cardetail[0]->num_rows>0){
                 foreach( $cardetail as $rows){
					$carmodelyear = "";
                    if($rows->car_model_year != 0){
						$carmodelyear = $rows->car_model_year;
                    }
        ?>
        <p class="bigger"><?php echo $carmodelyear."&nbsp;".$rows->car_make."&nbsp;".$rows->car_model; ?></p>
        <?php 
			} 
		} ?>
    </div>
    
    <!---- car info start ---->
    <div class="car_info_wrapper">
    	<!---- car info left start ---->
        <div class="car_info_left">
        	<div class="imgload" id="carSelected">
            	
                <?php                 
				$i = 0;          
               foreach ($getPrimaryImage as $img_rows){
                    if($getPrimaryImage[0]->num_rows > 0){
                ?>
      <a class="fancybox" data-fancybox-group="gallery"></a>
      <a class="clickimg"><img class="imgchange"  src="<?php echo $img_rows->base_url.$img_rows->public_id?>">
   <?php if($rows->icon_status=="soldout"){
      	echo' <img class="sold_out" src="/images/user/sold-out.png"/>';
         }

         if($rows->icon_status=="reserved"){
        echo' <img class="sold_out" src="/images/user/reserved.png"/>';
         }
	?>
             
     </a>
           
               <?php
               }else{
               ?>
              <?php if($cardetail[0]->num_rows>0){ ?>
          <img class="imgchange" src="/images/user/no_image.png"/>
              <?php }else{ ?>
              <p>The car does not exit.</p>

                                    <?php
								   }
										 }
									 break;
								  $i++;
									 }
									 ?>
            </div>
            
            
            
             <!--block image small start-->
            <div class="small_img">
            <form action="/?c=user&m=download_image" name="zips" method="post">
             <?php
			 
				$i=0;
				foreach($getDetailImage as $img_rows){
				$i++;
				$file = $img_rows->public_id;
				?>
                <div class="small-img carAcessory2" style="opacity: 1;">
                
                    <a class="fancybox" accesskey="<?php echo $img_rows->base_url.$img_rows->public_id; ?>" data-fancybox-group="gallery" href="#"> </a>
                       <a  class="example-image-link cantrig_<?php echo $i; ?>" href="<?php echo $img_rows->base_url.$img_rows->public_id; ?>" data-lightbox="example-set" data-title="<a  class='a_download bigger' href='<?php echo $img_rows->base_url.$img_rows->public_id; ?>'>Download</a>"><img class="smallimg imgactive_<?php echo $i; ?> <?php if($i == 1){ ?> imgactive <?php } ?>" a="<?php echo $i; ?>" id="new" src="<?php echo $img_rows->base_url.'thumb/'.$img_rows->public_id; ?>" border="0"></a>
                      
                   <input type="hidden" name="files[]" value="<?php echo $file; ?>" />
                   
                </div>
            <?php } ?>
            <input class="download_img" type="submit" name="download_image" value="" />
			</form>
	                
            </div><!--block image small end-->
            <!--Start chearado facebook-->   

			   <?php if($cardetail[0]->num_rows>0){ ?>                                             
              <div id="btnfb" class="fb-share-button" data-href="<?php echo $current_url; ?>" 
              data-layout="button_count"></div>
                <?php } ?>
            <!--End chearado facebook-->
            <?php if(strip_tags($cardetail[0]->contents)!=''){ ?>
                <div class="detail_box" id="car_description">
                    <div class="box_title">Car Description</div>
                    <div class="box_content"><?= $cardetail[0]->contents; ?></div>
                </div>
            <?php } ?>                
                
        </div>
        <!---- car info left end ---->
        
        
         <!---- car info right start ---->
        <div class="car_info_right">
        
         <!---- car specification start ---->
        <div class="spec_wrapper">
        	
            <?php
				foreach( $cardetail as $rows){
			?>
        	<div class="spec_content_left">
            
            	<div class="spec">
                	<div class="left smaller">Stock ID</div>
                    <div class="right smaller"><?php echo ($rows->car_stock_no !='' && $rows->car_stock_no > 0 ? $rows->car_stock_no : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Condition</div>
                    <div class="right blue smaller">
					<?php if( $rows->icon_new_yn == "Y"){
						echo "New";}else{ echo "Used";}
					?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Make</div>
                    <div class="right smaller"><?php echo ($rows->car_make !='' || $rows->car_make > 0 ? $rows->car_make : 'N/A');?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Model</div>
                    <div class="right blue smaller"><?php echo ($rows->car_model !='' || $rows->car_model > 0 ? $rows->car_model : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Chassis No</div>
                    <div class="right smaller"> <?php echo ($rows->car_chassis_no !='' || $rows->car_chassis_no > 0 ? $rows->car_chassis_no : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Model Year</div>
                    <div class="right blue smaller"><?php echo ($rows->car_model_year !='' && $rows->car_model_year > 0 ? $rows->car_model_year : 'N/A');?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Body Type</div>
                    <div class="right smaller"><?php echo ($rows->car_body_type !='' || $rows->car_body_type > 0 ? $rows->car_body_type : 'N/A');  ?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Mileage</div>
                    <div class="right blue smaller"><?php echo ($rows->car_mileage !='' && $rows->car_mileage > 0 ? number_format($rows->car_mileage) ." km" : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Steering</div>
                    <div class="right smaller"><?php
                                       if($rows->car_steering == "LHD"){ $steering = "Left Hand Drive";}else{ $steering = "Right Hand Drive";}
                                        echo ($steering !='' || $steering > 0 ? $steering : 'N/A'); ?></div>
                </div>
                
                
           
            
            
            
            	<div class="spec">
                	<div class="left blue smaller">Transmission</div>
                    <div class="right blue smaller"><?php echo ($rows->car_transmission !='' || $rows->car_transmission > 0 ? $rows->car_transmission : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Engine Size</div>
                    <div class="right smaller"><?php echo ($rows->car_cc !='' && $rows->car_cc > 0 ? number_format($rows->car_cc) ." CC" : 'N/A'); ?></div>
                </div>
                
                 <div class="spec">
                	<div class="left blue smaller">Drive Type</div>
                    <div class="right blue smaller"><?php echo ($rows->car_drive_type !='' || $rows->car_drive_type > 0 ? $rows->car_drive_type : 'N/A');  ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Fuel Type</div>
                    <div class="right smaller"><?php echo ($rows->car_fuel !='' || $rows->car_fuel > 0 ? $rows->car_fuel : 'N/A');  ?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Location</div>
                    <div class="right blue smaller">
					<?php
                          foreach ($list_country as $list ){
                             if($list->cc==$rows->country){
                    ?>
					<?php echo ($rows->country !='' || $rows->country > 0 ? $list->country_name .",": 'N/A'); ?>
                    <?php echo ($rows->car_city !='' || $rows->car_city > 0 ? $rows->car_city: 'N/A'); ?>
                    <?
                               }
                          }
                    ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Exterior Color</div>
                    <div class="right smaller"><?php echo ($rows->car_color !='' || $rows->car_color > 0 ? $rows->car_color : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                    <div class="left smaller">Seats</div>
                    <div class="right smaller"><?php echo ($rows->car_seat !='' || $rows->car_seat > 0 ? $rows->car_seat : 'N/A'); ?></div>
                </div>

                <div class="spec">
                    <div class="left smaller">Doors</div>
                    <div class="right smaller"><?php echo ($rows->door !='' || $rows->door > 0 ? $rows->door : 'N/A'); ?></div>
                </div>

                <div class="spec">
                	<div class="left blue smaller">Manufactured Date</div>
                    <div class="right blue smaller"><?php echo ($rows->manu_year !='' && $rows->manu_year > 0 ? $rows->manu_year : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Registration Date</div>
                    <div class="right smaller"><?php echo ($rows->first_registration_year !='' && $rows->first_registration_year > 0 ? $rows->first_registration_year : 'N/A'); ?></div>
                </div>
                
                
                
                
                
             </div>
             <?php } ?>
        </div>
        <!---- car specification end ---->
        
        
        <!--seller info -->
	<div class="contact_seller">
        <div class="content_contact_seller">
        <?php 
		foreach( $customer_service as $rows){ 
			
		?>

            <div class="seller_img">

                <?php if($rows->secure_url !="") { ?>
                    
                    <img src="<?= $rows->secure_url ?>"/>
               
                <?php }else{ ?>

                    <?php if($rows->sex =='F') { ?>
                        
                        <img src="/images/user/call-center.png"/>
                    
                    <?php }else{ ?>
                        
                        <img src="/images/user/call-center-m.png" border="0" />
                    
                    <?php } ?>

                <?php } ?>

            </div>
        
            <div class="smaller text_seller">
            	<span class="bigger"><?= $rows->member_name ?></span><br/><br/>
               + <?= $rows->phone_no1 ?>-<?= $rows->phone_no2 ?><br/><br/>
               <a href="mailto:<?= $rows->email ?>" style="font-size: 14px;text-decoration: none;"><?= $rows->email ?></a><br/><br/>
            </div>
         <div class="social_wrapper">
                <?php if(!empty($rows->facebook_id)) { ?>
                    <a class="tooltip" title="Facebook: &lt;b&gt;<?= $rows->facebook_id?>&lt;/b&gt;" href="<?php echo $rows->facebook_id; ?>"><img src="/images/public_site/fb.png" border="0" /></a>
                <?php } 
                    if(!empty($rows->skype_id)) {
                ?>
                    <a class="tooltip" title="Skype ID: &lt;b&gt;<?= $rows->skype_id?>&lt;/b&gt;" href="skype:'<?php echo $rows->skype_id; ?>'?add"><img src="/images/user/skype.png" /></a>
                <?php }
                    if(!empty($rows->whatsapp_id)) {
                ?>
                    <a class="tooltip" title="Viber: &lt;b&gt;<?= $rows->whatsapp_id?>&lt;/b&gt;" href="#"><img src="/images/public_site/what's_up.png" /></a>
                <?php } ?>

            </div>
		<?php } ?>
        </div>
        
        
        <div class="Divfooter smaller">
        	Contact and let Minhas Trading's staffs check car's availability and <br/><span style="color:#fc3e40">secure your money.</span>
       </div>
       
    </div>
    <!--seller info -->
    
    
     <!--seller info -->
	<div class="seller_info">
    	<div class="title bigger">Seller's Info</div>
        <?php
			foreach( $member_profile_select as $rows){
			$img_row = $rows->base_url.$rows->image;
		?>
        <div class="content_seller">
        <?php if ($rows->image != ''){ ?>
        	
            <img class="img_seller" src="<?php echo $img_row; ?>"  border="0" />
        <?php }else{ ?>
        	<img class="img_seller" src="/images/user/no_image_big.jpg"/>
        <?php } ?>
        <div class="text_right">
            <span class="bigger"><?php echo ($rows->company_name !="" ? $rows->company_name : "N/A"); ?></span><br/>
            
            <div style="overflow:auto; margin:5px 0px;" class="smaller"> 
                <img style="float:left; height:20px;" src="/images/flags/32/<?=strtoupper($rows->cc)?>.png" />
            <span style="margin-top:5px; float:left; margin-left:10px;"><?php echo ($rows->country !="" ? $rows->country : "N/A"); ?></span></div>
            <div class="smaller" style="line-height:20px;">Total products : <span style="color:red;font-weight: bold;"><?php echo ($rows->seller_product_count !="" ? $rows->seller_product_count : "N/A"); ?></span></div>
         </div>
        </div>
        <?php } ?>
        
    </div>
    <!--seller info -->
       
        
        
        </div>
        <!---- car info right end ---->
        
        </div>
		<!---- car info end ---->
        
        
       
        
        
    
    
</div>
<!---- car detail left end ---->

<!---- car detail right start ---->
<div class="detail_content_right">
	
		<!---- Total Price Calculator start ---->
        	<div class="wrapper_total_price">
            <?php
				foreach( $cardetail as $rows_fob){
			?>
            <p class="smaller title_fob" style="text-align: center;">FOB Price: 
            <strike><?php if($rows_fob->old_fob_cost > $rows_fob->car_fob_cost){ echo '$ '.$rows_fob->old_fob_cost;} ?></strike>
            <span style="color:red; margin-left:15px; font-size:20px; font-weight:bold">
            <?php
                                                $price = number_format($rows_fob->car_fob_cost);
                                                if(empty($price))
                                                {
                                                $price = 'ASK';
                                                }
                                                else
                                                {
                                                $price = "$ ".$price;
                                                }
                                                ?>
                                                <?= $price ?>
            <span>
            <?php 
            if($rows_fob->old_fob_cost > $rows_fob->car_fob_cost){ 
            ?>
            <div class="bg_special_offer">
                <div class="special_offer_text">Special Offer</div>
            </div>
            <?php
            }
            ?>
            </p>
            <?php } ?>
        	<p class="bigger total-price">Total Price Calculator</p>
            <!-- <span class="smaller text_insure">*** Total Price = Freight + Insurance + Inspection</span> -->
            <p class="smaller destination">Destination</p>
            <form name="calculate_price" id="calculate_price" method="post" enctype="multipart/form-data">
            <input type="hidden" name="idx" id="idx" value="<?= $_GET['idx'] ?>">
                <select name="destination_country" id="destination_country" class="form_selection smaller">
                <option value="">Select country</option>
                     <? 
                    foreach ($list_destination as $destination) {
                    ?>
                    <option value="<?= $destination->country_iso ?>" <?php if(isset($country_to) && $country_to==$destination->country_iso){ echo "selected";}?>><?= $destination->country_name ?></option>
                    <?
                    }
                    ?>
                </select>
                <select name="port_name" id="port_name" class="form_selection smaller">
                	<option value="">Select port</option>
                    <?php
                        foreach ($list_port as $row_list_port) {
                    ?>
                    
                    <option class="<?= $row_list_port->country_iso; ?>" value="<?= $row_list_port->id ?>" <?php if(isset($port_name) && $port_name==$row_list_port->id){ echo "selected";}?>><?= $row_list_port->port_name ?></option>
                    
                    <?php  }  ?>
                </select>
                
                <div class="check_boxed_wrapper" style="margin-left: 0px;text-align: left;">
                    <label style="float: left; margin-left: 17px; margin-right: 14px;font-weight:bold;font-size: 14px;">Insurance</label>
                    <input type="radio" class="radio_yes" value="true" name="insurance" <?php if(isset($insurance)&&$insurance==true ) echo "checked"; ?>> Yes
                    <input type="radio" class="radio_no" value="false" name="insurance" <?php if(isset($insurance)&&$insurance==false ) echo "checked"; ?> > No
                </div>
                <div class="check_boxed_wrapper" style="margin-left: 0px;text-align: left;">
                    <label style="float: left; margin-left: 17px; margin-right: 10px;font-weight:bold;font-size: 14px;">Inspection</label>
                    <input type="radio" class="radio_yes" value="true" <?php if(isset($inspection)&&$inspection==true ) echo "checked"; ?> name="inspection"> Yes
                    <input type="radio" class="radio_no" value="false" <?php if(isset($inspection)&&$inspection==false ) echo "checked"; ?>  name="inspection"> No
                </div>
                
                <input class="calculate button btn-reset" border="0" type="submit" name="submit_calculate_price" value="Calculate">
                
                <?
                if(isset($port_name) || isset($country_to) && $country_to==$destination->id){
                ?>
                <div class="smaller" style="margin-top:10px;">
                	Selected : <span style="color:#009045" id="port_country_to"></span>
                    <a style="margin-left:50px;">Nearest Port : <span style="color:#009045" id="get_port_name"></span></a>
                </div>
                <?
                }
                ?>
           
            <div id="call_center_port" class="call_center_port"></div>
            <div class="est_price">
            	Total Price :<span style="color:#eb1c24; float: right;" id="total_price" class="usd">
                <?= $total_price ?>
            </div>
             </form>
             </div>
             <!---- Total Price Calculator end ---->
             
    
    
     <!--contact seller-->
    
           <div style="width:295px; text-align:left; border:1px solid #cacaca; margin-top:20px">
                <form action="/?c=user&m=contact_email_inquiry" method="post">
                <p class="bigger title_contact_seller">CONTACT OUR SELE TEAM</p>
                <input type="hidden" name="car_idx" value="<?= $_GET['idx']; ?>"/>
                <input type="hidden" name="total_price" value="<?= $total_price ?>"/>
                <input type="hidden" name="frieght_cost" value="<?= $frieght_cost ?>"/>
                <input type="hidden" name="inspection_cost" value="<?= $inspection_cost ?>"/>
                <div style="padding:0px 10px 20px 10px; margin-top:10px">
                <!--<label style="font-size:14px">Your Email</label>
                <div class="smaller" style="margin-top:5px;">
                	<input type="text" value="" name="sender_email" style="width:100%; height:30px"/>
                </div><br/>-->
               <label style="font-size:14px;font-weight:bold;">Your Email</label>
               <input style="width: 99%;padding:10px;margin-bottom:15px;margin-top:10px;" type="text" class="smaller" name="email" required>
               
               <label style="font-size:14px;font-weight:bold;">Your Message</label>

               <textarea style="padding:10px; margin-top:10px; line-height:20px;" name="message" class="smaller" cols="33" rows="7" placeholder="" required></textarea>
               
               
               <input name="customer_send" type="submit" class="send" value="Submit" />
               </div>
               </form>
         </div>
            
        <!--contact seller-->
        
    
</div>

<!---- car detail right end ---->
</div>


<!---- More car start ---->
    <div class="more_car_wrapper">
    <?php
		$i = 0;
		foreach( $related_post as $rows){
	?>
    	<div class="title_more_car"><p class="bigger">More cars from <?php echo $rows->company_name; ?><a href="?c=user&m=sellerdetail&id=<?php echo $rows->car_owner; ?>">Show All</a></p></div>
    <?php
		break;
			$i++;
		}
	?>
        <div class="listcar_wrapper">
        
            <?php
				foreach( $related_post as $rows){
					$img_row = $rows->base_url."thumb/".$rows->fileimgname;
					$img_rowb = $rows->base_url.$rows->fileimgname;
			?>
                                        
            <div class="more_listcar">
            	<div class="carsource_wrapper">
                
                <a href="?c=user&m=cardetail&idx=<?php echo $rows->idx; ?>">
                <?php if ($rows->fileimgname != ''){ ?>
                	<img class="carload" title="<?php echo $rows->car_make ."<br/>". $rows->car_model." ". $rows->car_model_year;?>"  alt="<?php echo $rows->car_make ." ". $rows->car_model." ". $rows->car_model_year;?>" src="<?php echo $img_row; ?>"/>
                <?php }else{ ?>
                	<img class="carload" src="/images/user/no_image.png"/>
                <?php } ?>
                <?php 
				if($rows->icon_status=="soldout"){
                           echo' <img class="sold_out" src="/images/user/sold-out.png"/>';
                }
				if($rows->icon_status=="reserved"){
                           echo' <img class="sold_out" src="/images/user/reserved.png"/>';
                }
				?>
                </a>
                
                 <?php

                          $carmodelyear = "";
                          if($rows->car_model_year != 0){
                            $carmodelyear = $rows->car_model_year;
                          }

						  $carmakestr2 = $rows->car_model_year ." ".$rows->car_make ."<br/>". $rows->car_model;						
				?>
                    <div class="title"><a href="?c=user&m=cardetail&idx=<?php echo $rows->idx; ?>"><?php echo $carmakestr2; ?></a></div>
                    <div class="cardata_wrapper">
                        <div class="smaller">Country: 

                            <img class="cc" src="/images/flags/32/<?=strtoupper($rows->cc)?>.png">

                        <?php 
						// if($rows->country == "Japan"){
						// 	 $country = "<img class='cc' src='/images/public_site/country/icon_japan.png'>";
						// }elseif($rows->country == "South Korea"){ 
						// 	$country = "<img class='cc' src='/images/public_site/country/icon_korea.png'>";
      //                   }elseif($rows->country == "Thailand"){ 
      //                       $country = "<img class='cc' src='/images/public_site/country/icon_thailand.png'>";
						// }else{ $country = "N/A"; }
						
						
						// echo $country;
						?>
                        </div>
                        <div class="smaller">FOB:<span><?php echo ($rows->car_fob_cost !=0 ? $rows->car_fob_currency ." ".number_format($rows->car_fob_cost) : 'ASK'); ?></span></div>
                    </div>
            	</div>
                
                
                	<a class="contact_sell" href="/?c=user&m=cardetail&idx=<?php echo $rows->idx; ?>">Details</a>
              
            </div>
            
             <?php
				}
			?>
            
            
            
            </div>            
        </div>
    </div>
<!---- More car end ---->


 <?php //=========================================== Content ========================================== ?>

