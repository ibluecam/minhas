<?php
  echo $navigation_bar; 
  echo $sidebar;
?>
<div class="block_right" style="padding-top: 0">
  <form id="form_quick_status" action="/?c=admin&m=quick_set_status" method="post">
    <input id="quick_input_chassis_no" type="hidden" name="chassis_no" value=""/>
    <input id="quick_input_status" type="hidden" name="icon_status" value=""/>
    <input id="quick_input_idx" type="hidden" name="idx" value=""/>
    <input type="hidden" value="<?= $full_url; ?>" name="full_url"/> 
  </form>
  <!-- SELLER(access_type=true) & BUYER(access_type=false)  -->
  <?php if($access_type==true) {?>  
  <div class="<?=$msg_class?>" id="flash_block">
      <?=(!empty($msg_data) ? $msg_data.'<a href="#" class="msg_icon_close" title="Close">×</a>' : '' )?>
  </div>
  <?php } ?>
  <div class="page_title" style="border: none;">
    MY CAR LIST
  </div>
  <!--- SEARCH CONTAINER -->
  <div class="search_container">
    <form action="" method="get">
      <input type="hidden" name="c" value="admin">
      <input type="hidden" name="m" value="list_car">
      <select id="input_select_make" name="carmake" style="width: 150px" class="input_select">
        <option value="">Make</option>
          <?php 
            foreach($select_make_all as $make){                                   
          ?> 
            <option <?php if($make->car_make==$searched['carmake']) echo "selected"; ?> value="<?php echo $make->car_make; ?>"><?php echo $make->car_make; ?></option>
          <?php
            } 
          ?>
      </select>
      <input type="hidden" value="<?php echo $searched['carmodel'];?>" id="searched_model"/>
      <select name="carmodel" id="input_select_model" style="width: 150px" class="input_select">
        <option value="">Model</option>
        <?php foreach ($select_model_all as $model) {  ?>               
          <option class="<?php echo $model->car_make; ?>" value="<?php echo $model->car_model; ?>"><?php echo $model->car_model; ?></option>
        <?php } ?>
      </select>
      <select name="year" id="input_select_year" style="width: 100px" class="input_select">
        <?php $currentYear = date('Y'); ?>
        <option value="">Year</option>
        <?php for($i=$currentYear;$i>=1990;$i--){ ?>
          <option <?php if($i==$searched['year']) echo "selected"; ?> value="<?= $i ?>"><?= $i ?></option>
        <?php } ?>
      </select>
      
      <input name="sch_word" value="<?php if(isset($searched['sch_word'])) echo $searched['sch_word']; ?>" style="width: 150px" class="input_text" placeholder="Keyword" type="text"/>
      <input type="submit" class="btn_search" value="Search" />
      <input type="button" class="btn_search" id="btn_reset" value="Reset" />
    </form>
  </div>
  <!-- END SEARCH CONTAINER --> 
  
  <!-- SELLER(access_type=true) & BUYER(access_type=false)  -->
  <?php if($access_type==true) {?>  

  <form id="form_quick_set_status" action="/?c=admin&m=quick_set_status_by_select_action" method="post">
  <input type="hidden" value="<?= $full_url; ?>" name="full_url"/> 
  <input type="hidden" value="" id="quick_idx_publish" name="idx"/> 
  <input type="hidden" value="" id="quick_publish_value" name="icon_status"/>
  
  <div style="overflow:auto;padding: 20px 0 0 0;">
    <div class="action_container" style="float:left; ">
      <select style="width: 150px" name="icon_status" class="input_select"  id="quick_action">
        <option value="">Action</option>
        <option value="sale">Set as Sale</option>
        <option value="reserved">Set as Reserved</option>
        <option value="soldout">Set as Soldout</option>
        <option value="not_published">Unpublish</option>
        <option value="delete">Delete</option>
      </select>
      <input type="button" class="btn_apply" value="Apply" />
    </div>
    <div style="padding: 5px 0;float:right;">
      <a <?php if(!isset($_GET['sch_icon_status']) && !isset($_GET['sch_publish'])) { echo 'class="active"'; }?> href="<?php echo $quick_link['all']; ?>">All (<?php if(isset($quick_count['all'])) echo $quick_count['all']; else echo '0';?>)</a><span class="sep_line"> | </span> 
      <a <?php if(isset($_GET['sch_icon_status']) && $_GET['sch_icon_status']=='reserved') { echo 'class="active"'; }?> href="<?php echo $quick_link['reserved']; ?>">Reserved (<?php if(isset($quick_count['reserved'])) echo $quick_count['reserved']; else echo '0';?>)</a><span class="sep_line"> | </span> 
      <a <?php if(isset($_GET['sch_icon_status']) && $_GET['sch_icon_status']=='soldout') { echo 'class="active"'; }?> href="<?php echo $quick_link['soldout']; ?>">Soldout (<?php if(isset($quick_count['soldout'])) echo $quick_count['soldout']; else echo '0';?>)</a><span class="sep_line"> | </span> 
      <a <?php if(isset($_GET['sch_publish']) && $_GET['sch_publish']=='not_published') { echo 'class="active"'; }?> href="<?php echo $quick_link['not_published']; ?>">Not Published (<?php if(isset($quick_count_pub['not_published'])) echo $quick_count_pub['not_published']; else echo '0';?>)</a>
    </div>
  </div>
  <?php } ?>
  <div class="car_list_container">
    <table class="car_list">
      <tr>
        <!-- SELLER(access_type=true) & BUYER(access_type=false)  -->
        <?php if($access_type==true) {?>
        <th><input id="checked_all_list" type="checkbox"/></th>
        <?php } ?>
        <th>Image</th>
        <th>Car Name</th>
        <th>FOB</th>
        <th>Date</th>
      </tr>
      
      
      <?php
        if(count($bbs_list_sale)>0){ 
          foreach ($bbs_list_sale as $row) {
      ?>
        <tr>
          <!-- SELLER(access_type=true) & BUYER(access_type=false)  -->
          <?php if($access_type==true) {?>
          <td>
          <input value="<?=$row->idx?>" name="idx[]" class="check_seller_list" type="checkbox"/>
          </td>
          </form>
          <?php } ?>
          <td style="width:120px;">
          <a href="/?c=admin&m=detail_car&mcd=product&idx=<?= $row->idx ?>">
            <?php 
              if(!empty($row->public_id)){ 
                $src=$row->base_url . "thumb/".$row->public_id;
              }else{
                $src="/images/user/no_image.png";
              }
            ?>
            <div class="thumb_container" style="position:relative;width:120px;">
              <img width="120" src="<?= $src ?>"/>
              <?php if($row->icon_status=='soldout'){ ?>
                <img class="car_status_badge" src="/images/member_site/soldout.png"/>
              <?php }elseif($row->icon_status=="reserved"){ ?>
                <img class="car_status_badge" src="/images/member_site/reserved-small.png"/>
              <?php }elseif($row->icon_status!="sale"){ ?>
                <img class="car_status_badge" src="/images/member_site/reserved-small.png"/>
              <?php } ?>
            </div>
          </a>    
          </td>
          <td>
            <div class="model"><a href="/?c=admin&m=detail_car&mcd=product&idx=<?= $row->idx ?>"><?= $row->car_model_year." ".$row->car_make." ".$row->car_model; ?></a></div>
            <div class="car_short_des">
              Chassis No: <?= $row->car_chassis_no ?><br/>
              Mileage:  <?= $row->car_mileage ?> Km
            </div>
            <!-- SELLER(access_type=true) & BUYER(access_type=false)  -->
            <?php if($access_type==true) {?>  

            <div class="below_action">
              <a href="/?c=admin&m=modify_car&mcd=product&idx=<?=$row->idx?>" title="Edit this car">Edit</a><span class="sep_line"> | </span>
              <a style="color:#E00202" title="Delete this car" class="btn_quick_delete" data-idx="<?=$row->idx?>" href="#">Delete</a><span class="sep_line"> | </span>
              <a href="#" data-idx="<?=$row->idx?>" data-url="/?c=admin&amp;m=discount_car&idx=<?=$row->idx?>&car_fob_cost=<?=$row->car_fob_cost?>&old_fob_cost=<?=$row->old_fob_cost?>" class="btn_quick_discount">Discount</a>
              <span class="sep_line"> | </span>  

              <?php if(isset($_GET['sch_icon_status']) && $_GET['sch_icon_status']=='soldout') {?>

                <a class="btn_quick_sale" title="Set car as Sale" data-chassisNo="<?=$row->car_chassis_no?>" data-idx="<?=$row->idx?>" href="#">Sale</a> <span class="sep_line"> | </span><a class="btn_quick_reserve" title="Set car as Reserved" data-chassisNo="<?=$row->car_chassis_no?>" data-idx="<?=$row->idx?>" href="#">Reserve</a>
              
              <?php }elseif (isset($_GET['sch_icon_status']) && $_GET['sch_icon_status']=='reserved') {?>
               
                <a class="btn_quick_sale" title="Set car as Sale" data-chassisNo="<?=$row->car_chassis_no?>" data-idx="<?=$row->idx?>" href="#">Sale</a> <span class="sep_line"> | </span><a class="btn_quick_soldout" title="Set car as Soldout" data-chassisNo="<?=$row->car_chassis_no?>" data-idx="<?=$row->idx?>" href="#">Soldout</a>
              
              <?php }else{ ?>              
                
                <?php if($row->icon_status=='soldout') {?>
                  
                  <a class="btn_quick_sale" title="Set car as Sale" data-chassisNo="<?=$row->car_chassis_no?>" data-idx="<?=$row->idx?>" href="#">Sale</a> <span class="sep_line"> | </span><a class="btn_quick_reserve" title="Set car as Reserved" data-chassisNo="<?=$row->car_chassis_no?>" data-idx="<?=$row->idx?>" href="#">Reserve</a>
                
                <?php }elseif($row->icon_status=='reserved') {?>
                  
                  <a class="btn_quick_sale" title="Set car as Sale" data-chassisNo="<?=$row->car_chassis_no?>" data-idx="<?=$row->idx?>" href="#">Sale</a> <span class="sep_line"> | </span><a class="btn_quick_soldout" title="Set car as Soldout" data-chassisNo="<?=$row->car_chassis_no?>" data-idx="<?=$row->idx?>" href="#">Soldout</a>
                
                <?php }else{ ?>
                  
                  <a class="btn_quick_reserve" title="Set car as Reserved" data-chassisNo="<?=$row->car_chassis_no?>" data-idx="<?=$row->idx?>" href="#">Reserve</a> <span class="sep_line"> | </span><a class="btn_quick_soldout" title="Set car as Soldout" data-chassisNo="<?=$row->car_chassis_no?>" data-idx="<?=$row->idx?>" href="#">Soldout</a>
                
                <?php } ?>
              
              <?php } ?>
              
            </div>


            <!-- BLOCK_FOB -->

            <div style="display:none;">

              <div id="block_car_price_<?=$row->idx?>">
                  <h3 class="title">DISCOUNT</h3>
                  <p class="msg" id="msg"></p>
                  <form id="block_discount" name="block_discount" action="" method="post" enctype="multipart/form-data">
                    <div class="select_container">
                    <input type="hidden" name="idx" id="idx" value="<?=$row->idx?>">
                    <table class="tbl_input_discount" style="height:120px;">
                      <tr>
                        <td style="width: 106px"><b>FOB TYPE</b></td>
                        <td>
                          <label><input type="radio" data-idx="<?=$row->idx?>" value="normal" <?php if(!empty($row->car_fob_cost) || empty($row->old_fob_cost)) echo 'checked';?> name="normal_discount" id="normal_<?=$row->idx?>"> Normal</label>&nbsp;&nbsp;&nbsp;
                          <label><input type="radio" data-idx="<?=$row->idx?>" value="discount" <?php if(!empty($row->old_fob_cost)) echo 'checked';?> name="normal_discount" id="discount_<?=$row->idx?>"> Discount</label>
                        </td>
                      </tr>
                      <tr class="block_previous_fob_<?=$row->idx?>">
                        <td style="width: 106px"><b id="label_previous_fob_<?=$row->idx?>">Previous FOB</b></td>
                        <td>
                          <label id="input_previous_fob_<?=$row->idx?>">
                          <input type="text" data-idx="<?=$row->idx?>" class="old_fob_cost" id="old_fob_cost_<?=$row->idx?>" name="old_fob_cost" value="<?=(!empty($row->old_fob_cost) ? $row->old_fob_cost : '')?>"> USD</label>
                        </td>
                      </tr>
                      <tr>
                        <td style="width: 106px"><b> FOB</b></td>
                        <td>
                          <label><input type="text" class="car_fob_cost" id="car_fob_cost_<?=$row->idx?>" data-idx="<?=$row->idx?>" name="car_fob_cost" value="<?=(!empty($row->car_fob_cost) ? $row->car_fob_cost : '')?>"> USD</label>
                        </td>
                      </tr>
                      <tr class="block_empty" id="block_empty_<?=$row->idx?>">
                        <td colspan="2"></td>
                      </tr>
                    </table>
                    </div>
                    <div class="btn_container">
                      <input class="btn_save_discount_<?=$row->idx?>" id="btn_save_discount" type="submit" value="Save" name="btn_save_discount">

                    </div>
                  </form>
              </div>

            </div>

            <!-- END BLOCK FOB -->

            <?php } ?>

          </td>
          <td>
              <?php

              if(!empty($row->old_fob_cost)){
                echo "<del>$".number_format($row->old_fob_cost)."</del><br/>";
              }

              ?>

              <span class="car_fob" id="car_fob_<?=$row->idx?>">
              <?php 
                if(!empty($row->car_fob_cost)){
                  echo "$".number_format($row->car_fob_cost);
                }else{
                  echo "ASK PRICE";
                }
              ?>
              
            </span>
          </td>
          <td class="publish_dt" style="text-align:center;">
          
          <?= date('Y-m-d',strtotime($row->created_dt)) ?>
          <!-- SELLER(access_type=true) & BUYER(access_type=false)  -->
          <?php if($access_type==false) {?>

          <?= ($row->publish=='Y') ? '<br/>Published<br/>' : '<br/><span style="color:#EE002F">Not Published</span><br/>'?>
          <?php }else{ ?>
          <?= ($row->publish=='Y') ? '<br/>Published<br/><a class="btn_quick_not_publish" data-value="not_published" data-idx='.$row->idx.' href="#">Unpublish</a>' : '<br/><span style="color:#EE002F">Not Published</span><br/><a class="btn_quick_publish" data-value="published" data-idx='.$row->idx.' href="#">Publish</a>'?>
          <?php } ?>
          </td>
        </tr>
        
      <?php
          }
      ?>
        <tr>
        <!-- SELLER(access_type=true) & BUYER(access_type=false)  -->
          <?=($access_type==true) ? '<td colspan="5">' : '<td colspan="4">' ?> 
            <center>
              <?php
                echo $pagination;
              ?>
            </center>
          </td>
        </tr>
      <?php
        }else{
      ?>
        <tr><td colspan="5"><div style="padding:30px 0; text-align:center;">Sorry there is no product found for your query.</div></td></tr>
      <?php 
        }
      ?>
      
    </table>
  </div>

  <!-- </form> -->

</div>