<div class="blog-addcar">
   <div class="title-info">
      <p>View Car List</p>
   </div>
   <div class="view-info">
         <?php
                if(count($bbs_info_buyer_detail)>0){
                foreach ($bbs_info_buyer_detail->result() as $rows){
 
                ?>
         <table class="table-info" border="1" width="100%">
            <thead>
            </thead>
            <tbody>
              
               <tr>
                  <td class="info-field-title" width="15%"><label>Car Condition</label></td>
                  <td class="info-field-radio" width="35%">
                    <label>
                     <?php
                              if($rows->icon_new_yn=='Y'){
                                echo 'New';
                              }elseif($rows->icon_new_yn=='N'){
                                echo 'Used';
                              }
                            ?>
                 </label>
                  </td>
                  <td class="info-field-title" width="15%"><label>Car Dimension (mm)</label></td>
                  <td class="info-field-text" width="35%">
                      <label>
                                    <?php
                                       if($rows->car_length=='' || $rows->car_length==0 || $rows->car_length==NULL){
                                           echo '';
                                       }
                                       else{
                                           echo number_format($rows->car_length) .' x';
                                       }
                                    ?>
                                </label>
                                <label>
                                    <?php
                                       if($rows->car_width=='' || $rows->car_width==0 || $rows->car_width==NULL){
                                           echo '';
                                       }
                                       else{
                                           echo number_format($rows->car_width) .' x';
                                       }
                                    ?>
                                </label>
                                <label>
                                    <?php
                                       if($rows->car_height=='' || $rows->car_height==0 || $rows->car_height==NULL){
                                           echo '';
                                       }
                                       else{
                                           echo number_format($rows->car_height);
                                       }
                                    ?>
                              
                    </label>
                  </td>
               </tr>
               <tr>
                  <td class="info-field-title" width="15%"><label>Location</label></td>
                  <td class="info-field-select" width="35%">
                    <label>
                      <?= $rows->country_name.($rows->car_city!=''? '&nbsp;/ City : &nbsp;'.$rows->car_city:''); ?>
                    </label>
                  </td>
                  <td class="info-field-title" width="15%"><label>Steering</label></td>
                  <td class="info-field-radio" width="35%">
                    <label> <?php 
                              if($rows->car_steering=='' || $rows->car_steering=='0'){
                                  echo '';
                              }
                            else {
                            ?>
                            <p>
                                <?php
                                  echo ($rows->car_steering =='LHD') ? 'Left-hand driving' : 'Right-hand driving' ;
                                ?>
                            </p>
                            <?php 
                              }
                            ?></label>
                  </td>
               </tr>
               <tr>
                  <td class="info-field-title" width="15%"><label>Status</label></td>
                  <td class="info-field-select" width="35%">
                    <label><?=$rows->icon_status?></label>
                  </td>
                  <td class="info-field-title" width="15%"><label>Chassis No</label></td>
                  <td class="info-field-text" width="35%">
                     <label><?php echo $rows->car_chassis_no; ?></label>
                  </td>
               </tr>
               <tr>
                  <td class="info-field-title" width="15%"><label>Make</label></td>
                  <td class="info-field-select" width="35%">
                     <label><?php echo $rows->car_make; ?></label>
                  </td>
                  <td class="info-field-title" width="15%"><label>Grade</label></td>
                  <td class="info-field-text" width="35%">
                   <label><?php echo $rows->car_grade; ?></label>
                  </td>
               </tr>
               <tr>
                  <td class="info-field-title" width="15%"><label>Model</label></td>
                  <td class="info-field-select" width="35%">
                     <?php 
                               if($rows->car_model=='' ||  $rows->car_model=='0'){
                                   echo '';
                               }
                            else {
                            ?>
                             <label><?php echo $rows->car_model; ?><?php } ?></label>
                  </td>
                  <td class="info-field-title" width="15%"><label>Engine Size</label></td>
                  <td class="info-field-text" width="35%">
                     <?php
                              if($rows->car_cc=='' || $rows->car_cc=='0'){
                                  echo '';
                              }
                            else {
                            ?>
                             <label><?php echo $rows->car_cc; ?> CC</label><?php } ?> 
                  </td>
               </tr>
               <tr>
                  <td class="info-field-title" width="15%"><label>Model Year</label></td>
                  <td class="info-field-text" width="35%">
                    <?php 
                               if($rows->car_model_year=='' || $rows->car_model_year=='0'){
                                   echo '';
                               }
                              else {
                            ?>
                            <label><?php echo $rows->car_model_year; ?> Year (only number)<?php } ?></label>
                  </td>
                  <td class="info-field-title" width="15%"><label>Manufactured</label></td>
                  <td class="info-field-text" width="35%">
                      <?php  
                            
                              if($rows->manu_year=='' || $rows->manu_year=='0'){
                                  echo '';
                              }
                             else {
                            ?>
                           <label><?php echo $rows->manu_year; ?> Year (only number)</label><?php } ?>
                  </td>
               </tr>
               <tr>
                  <td class="info-field-title" width="15%"><label>Registration Date</label></td>
                  <td class="info-field-select" width="35%">
                     <label> <?php
                              if($rows->first_registration_month =='0'  && $rows->first_registration_year =='0'){
                               echo '';
                              }
                              else if($rows->first_registration_month =='0' && $rows->first_registration_year !='0') {
                                echo $rows->first_registration_year.' Year';
                              }
                               else if($rows->first_registration_month !='0' && $rows->first_registration_year =='0') {
                                echo date('F', mktime(0, 0, 0, $rows->first_registration_month, 1));
                              }
                              else{
                                echo date('F', mktime(0, 0, 0, $rows->first_registration_month, 1)).' / '.$rows->first_registration_year.' Year';
                              }
                             
                           ?></label>
                  </td>
                  <td class="info-field-title" width="15%"><label>Seats</label></td>
                  <td class="info-field-text" width="35%">
                     <?php
                              if($rows->car_seat=='' || $rows->car_seat=='0'){
                                  echo '';
                              }
                             else {
                            ?>
                            <label></label><?= $rows->car_seat?> SEATS </label><?php } ?>
                  </td>
               </tr>
               <tr>
                  <td class="info-field-title" width="15%"><label>Mileage</label></td>
                  <td class="info-field-text" width="35%">
                      <?php 
                           if($rows->car_mileage=='' || $rows->car_mileage=='0'){
                               echo '';
                           }
                         else {
                        ?>
                            <label></label><?php echo $rows->car_mileage; ?> Km <?php } ?></label>
                  </td>
                  <td class="info-field-title" width="15%"><label>Color</label></td>
                  <td class="info-field-select" width="35%">
                    <?php 
                              if($rows->car_color=='' || $rows->car_color=='0'){
                                  echo '';
                              }
                             else {
                            ?>
                         <label></label><?= $rows->car_color ?></label><?php } ?>
                  </td>
               </tr>
               <tr>
                  <td class="info-field-title" width="15%"><label>Body Type</label></td>
                  <td class="info-field-select" width="35%">
                      <?php 
                              if( $rows->car_body_type=='' ||  $rows->car_body_type=='0' ){
                                  echo '';
                              }
                              else {
                           ?>
                            <label></label><?= $rows->car_body_type ?></label><?php } ?>
                  </td>
                  <td class="info-field-title" width="15%"><label>Fuel</label></td>
                  <td class="info-field-select" width="35%">
                    <?php
                            if($rows->car_fuel=='' || $rows->car_fuel=='0'){
                                echo '';
                            }
                         else {
                         ?>
                        <label><?= $rows->car_fuel ?></label><?php } ?>
                  </td>
               </tr>
               <tr>
                  <td class="info-field-title" width="15%"><label>Transmission</label></td>
                  <td class="info-field-select" width="35%">
                    <?php 
                            if($rows->car_transmission=='' || $rows->car_transmission=='0'){
                                echo '';
                            }
                        else {
                                    
                             ?>
                            <label><?= $rows->car_transmission ?></label><?php } ?>
                  </td>
                  <td class="info-field-title" width="15%"><label>Drive Type</label></td>
                  <td class="info-field-select" width="35%">
                    <label>2WD</label>
                  </td>
               </tr>
               <tr>
                  <td class="info-field-title" width="15%"><label>FOB Cost</label></td>
                  <td class="info-field-select" width="35%">
                    <?php if($rows->fob=='' ||$rows->fob=='0'  ){
                                        echo 'Ask Price';}else {
        echo $rows->fob;?> <?=func_currency_sym($rows->car_fob_currency,'Y') ?><?php }?>
                  </td>
                  <td colspan="2" width="15%"></td>
               </tr>

               <tr>
                  <td class="info-field-title" width="15%"><label>Files</label></td>
                  <td class="info-field-select" width="35%">
                     <?=func_create_attach_files_name($bbs_attach_files,'carItem','N','Y','','Y','','N')?>
                  </td>
                  <td colspan="2" width="15%"></td>
               </tr>


               <tr id="tr-description">
                  <td class="info-field-title" id="description-field" width="15%"><label>Description</label></td>
                  <td colspan="3" cols="20" class="info-field-data" width="35%">
                     <label><?=$rows->contents;?></label>
                  </td>
               </tr>
               <tr id="tr-images">
                  <td class="info-field-title" width="15%"><label>Images</label></td>
                  <td colspan="3" cols="20" class="info-field-data" width="35%">

                    <form action="/?c=admin&m=download_document" name="zips" method="post">
             <?php
       
        $i=0;
        foreach($getDetailImage as $img_rows){
        $i++;
        $file = $img_rows->public_id;
        ?>
                <!-- <div class="small-img carAcessory2" style="opacity: 1;"> -->
                   <div class="light_left">
                        <a class="fancybox" accesskey="<?php echo $img_rows->base_url.$img_rows->public_id; ?>" data-fancybox-group="gallery" href="#"> </a>
                       <a class="example-image-link cantrig_<?php echo $i; ?>" href="<?php echo $img_rows->base_url."c_fill,h_768,w_1024/".$img_rows->public_id; ?>" data-lightbox="example-set" data-title="<a class='a_download bigger' href='<?php echo $img_rows->base_url."c_fill,h_768,w_1024/".$img_rows->public_id; ?>'>Download</a>"><img class="smallimg imgactive_<?php echo $i; ?> <?php if($i == 1){ ?> imgactive <?php } ?>" a="<?php echo $i; ?>" id="new" src="<?php echo $img_rows->base_url.'c_fill,h_66,w_86/'.$img_rows->public_id; ?>" border="0"></a>
                      
                   <input type="hidden" name="files[]" value="<?php echo $file; ?>" />
                   </div>
                  
                   
              <!--   </div> -->
            <?php } ?>
            <input class="download_img_buyer" type="submit" name="download_image" value="" />
      </form>

                  
                  </td>
               </tr>
            </tbody>
            <tfoot>
            </tfoot>
         </table>
       <?php
        }
     }
       ?>
   </div>
</div>