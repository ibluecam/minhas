<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/css/admin/admin.css" />
     
<style>
div {
    box-sizing: initial;
}
#block_discount{
  width: 350px;
  padding-left: 15px;
}
.title{ 
  padding: 20px 0 5px 28px;
}
.select_container select{
  -moz-appearance: tabpanels;
  background-color: transparent;
  box-shadow: none;
  cursor: auto;
  display: block;
  font-size: 100%;
  height: 30px;
  line-height: normal;
  margin: 0;
  padding: 0 10px;
  width: 180px;
}
.btn_container{
  border: solid 0px #000; 
  padding-top: 10px; 
  width: 297px;
}
#btn_save_discount{
  background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #258dc8 0%, #258dc8 100%) repeat scroll 0 0;
    border: medium none;
    border-radius: 5px;
    color: white;
    cursor: pointer;
    min-width: 100px;
    padding: 5px;
    text-align: center;
    float: right;
}
.msg{
  margin: -3px 0 0 27px;
  position: absolute;
}
#old_fob_cost{
    box-shadow: none;
    cursor: auto;
    font-size: 100%;
    height: 27px;
    line-height: normal;
    margin: 0;
    padding: 0 10px;
    width: 165px;
}
#car_fob_cost{
    box-shadow: none;
    cursor: auto;
    font-size: 100%;
    height: 27px;
    line-height: normal;
    margin: 0;
    padding: 0 10px;
    width: 165px;
}
.success{
  color: blue;
}
.fail{
  color: red ;
}
b{
  padding: 0px 10px;
}
table tr{
  height: 40px;
}
</style>
</head>
<body>
    <h3 class="title">DISCOUNT</h3>
    <p class="msg" id="msg"></p>
    <form id="block_discount" name="block_discount" action="?c=admin&m=list_car" method="post" enctype="multipart/form-data">
      <div class="select_container">
      <input type="hidden" name="idx" id="idx" value="<?=$_GET['idx']?>">
      <table style="height:120px;">
        <tr>
          <td style="width: 106px"><b>FOB TYPE</b></td>
          <td>
            <label><input type="radio" value="normal" <?php if(!empty($_GET['car_fob_cost']) || empty($_GET['old_fob_cost'])) echo 'checked';?> name="normal_discount" id="normal"> Normal</label>&nbsp;&nbsp;&nbsp;
            <label><input type="radio" value="discount" <?php if(!empty($_GET['old_fob_cost'])) echo 'checked';?> name="normal_discount" id="discount"> Discount</label>
          </td>
        </tr>
        <tr class="block_previous_fob">
          <td style="width: 106px"><b id="label_previous_fob">Previous FOB</b></td>
          <td>
            <label id="input_previous_fob"><input type="text" id="old_fob_cost" name="old_fob_cost" value="<?=(!empty($_GET['old_fob_cost']) ? $_GET['old_fob_cost'] : '')?>"> USD</label>
          </td>
        </tr>
        <tr>
          <td style="width: 106px"><b> FOB</b></td>
          <td>
            <input type="text" id="car_fob_cost" name="car_fob_cost" value="<?=(!empty($_GET['car_fob_cost']) ? $_GET['car_fob_cost'] : '')?>"> USD
          </td>
        </tr>
        <tr class="block_empty">
          <td colspan="2"></td>
        </tr>
      </table>
      </div>
      <div class="btn_container">
        <input id="btn_save_discount" type="submit" value="Save" name="btn_save_discount">

      </div>
    </form>
</body>
</html>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>

$(document).ready(function() {

  if($("#discount:checked").val()){ 
    $(".block_empty").css("display","none");
    $(".block_previous_fob").css("display","");
    $("#label_previous_fob").css("display","block");
    $("#input_previous_fob").css("display","block");
  }else{
    $(".block_empty").css("display","");
    $(".block_previous_fob").css("display","none");
    $("#label_previous_fob").css("display","none");
    $("#input_previous_fob").css("display","none");
  }

  $("#normal").on( "change", function() {
    $(".block_empty").css("display","");
    $(".block_previous_fob").css("display","none");
    $("#label_previous_fob").css("display","none");
    $("#input_previous_fob").css("display","none");
  });
  $("#discount").on( "change", function() {
    $(".block_empty").css("display","none");
    $(".block_previous_fob").css("display","");
    $("#label_previous_fob").css("display","block");
    $("#input_previous_fob").css("display","block");
  });

  //called when key is pressed in textbox
  $("#car_fob_cost,#old_fob_cost").keypress(function (e) {
      //if the letter is not digit then display error and don't type anything
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message //
          return false;
      }
  });

   $( "#car_fob_cost,#old_fob_cost" ).on( "keyup", function(e) {
      var car_fob_cost = $('#car_fob_cost').val();
      var old_fob_cost = $('#old_fob_cost').val();

      if($("#discount:checked").val()){
        if (parseInt(old_fob_cost)<parseInt(car_fob_cost)) {
            $(".msg" ).removeClass( "success" );
            $(".msg").html("New FOB must be smaller than Previous FOB!");
            $(".msg").attr("style","color: red");
            $('#btn_save_discount').attr('disabled','disabled');
        } 
        else{
            $("#msg").html("");
            $('#btn_save_discount').removeAttr('disabled');
        }
      }
  });

  $( "#btn_save_discount" ).on( "click", function(e) {
      // window.parent.Discount_reload();
      // parent.$.colorbox.close();
      // reload_discount();
      // return false;      
  });

});
function reload_discount(){
  parent.location.reload();
  // window.parent.location.href = window.parent.location.href.replace("#",'');
}

</script>