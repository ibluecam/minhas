<div class="outer">
    <!-- Navigation Bar -->
    
    <?php echo $navigation_bar; ?>

    <div class="inner add_info">
        <!-- SIDEBAR MENU-->
        
            <?php echo $sidebar; ?>

        <!-- ADD & MODIFY INFORMATION -->
        <div class="block_right" style="padding-top: 0">
            <!-- START BLOCK TITLE & QUICK ACTION -->
            <div class="page_title">
              <?php if($bbs_detail_list > 0){ ?>
              <?= $bbs_detail_list[0]->car_model_year." ".$bbs_detail_list[0]->car_make." ".$bbs_detail_list[0]->car_model; ?>
              <?php } ?>
            </div>

            <div class="block_action_button">
                <div class="step_car_info">
                    <a href="/?c=admin"><div class="action_back">BACK</div></a>
                    <!-- SELLER(access_type=true) & BUYER(access_type=false)  -->
                    <?php if($access_type==true) {?>
                    <a href="/?c=admin&m=modify_car&mcd=product&idx=<?=$bbs_detail_list[0]->idx?>"><div class="action_edit">EDIT</div></a>     
                    <?php } ?>
                </div>
            </div>

            <?php

            if (count($bbs_detail_list) > 0) {
                foreach ($bbs_detail_list as $rows) {
            ?>

            <div>
                <div class="container_preview">

                    <div class="input_row">
                    
                        <?php if (count($bbs_cloud_list) > 0) { ?>
                        <div class="block_image">

                        <form action="/?c=admin&m=download_document" name="zips" method="post">

                            <div id="big_images_car">
                                <img src="<?=$bbs_cloud_list[0]->base_url.$bbs_cloud_list[0]->public_id?>">
                            </div>

                            <?php 
                            $i=0;
                            foreach ($bbs_cloud_list as $img_rows) {
                            $i++;
                            $file = $img_rows->public_id;
                            ?>
                            <div id="images_car">
                                <li>
                                    <a class="fancybox" accesskey="<?php echo $img_rows->base_url.$img_rows->public_id; ?>" data-fancybox-group="gallery" href="#"> </a>
                                    <a class="example-image-link cantrig_<?php echo $i; ?>" href="<?php echo $img_rows->base_url.$img_rows->public_id; ?>" data-lightbox="example-set" data-title="<a class='a_download bigger' href='<?php echo $img_rows->base_url.$img_rows->public_id; ?>'>Download</a>"><img class="smallimg imgactive_<?php echo $i; ?> <?php if($i == 1){ ?> imgactive <?php } ?>" a="<?php echo $i; ?>" id="new" src="<?php echo $img_rows->base_url.'thumb/'.$img_rows->public_id; ?>" border="0"></a>
                                    <input type="hidden" name="files[]" value="<?php echo $file; ?>" />
                                </li>
                            </div>
                            <?php } ?>

                            <input class="download_img_buyer" type="submit" name="download_image" value="" />
                            </form>

                        </div>
                        <?php } else { ?>
                        <div class="block_image">
                            <div id="big_images_car">
                        <img src="/images/user/no_image_big.jpg">
                            </div>
                        </div>
                        <?php }?>

                        <!-- SELLER(access_type=true) & BUYER(access_type=false)  -->
                        <?php if($access_type==false) {?>

                        <div class="input_field_container">
                            
                            <div class="input_control_container">
                                <div class="file_title">Files</div>
                                <div class="file_name_icon">
                                <?=func_create_attach_files_name($bbs_attach_files,'carItem','N','Y','','Y','','N')?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="block_preview_data">
                            <div class="field_container">
                                <div class="input_label_preview">Location </div>
                                <div class="input_control_container_preview">
                                    <label>
                                    <?php 
                                    foreach($country_list as $country){
                                        if($rows->country == $country->cc){
                                            echo $country->country_name;
                                        }
                                    }
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">City </div>
                                <div class="input_control_container_preview">
                                    <label><?= $rows->car_city ?></label>
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">Condition </div>
                                <div class="input_control_container_preview">
                                    <label>
                                        <?php
                                        if($rows->icon_new_yn=='N'){
                                            echo 'Used';
                                        }else{
                                            echo 'New';
                                        }
                                        ?>
                                    </label>
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">Availability </div>
                                <div class="input_control_container_preview">
                                    <label><?= $rows->icon_status ?></label>
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">Steering </div>
                                <div class="input_control_container_preview">
                                    <label>
                                        <?php
                                        if($rows->car_steering=='LHD'){
                                            echo 'Left';
                                        }else{
                                            echo 'Right';
                                        }
                                        ?>
                                    </label>
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">Make </div>
                                <div class="input_control_container_preview">
                                    <label><?= $rows->car_make ?></label>      
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">Model </div>
                                <div class="input_control_container_preview">                        
                                    <label><?= $rows->car_model ?></label>                                    
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">Model Year </div>
                                <div class="input_control_container_preview">                                
                                    <label><label><?= $rows->car_model_year ?></label>  </label> 
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">Register Month </div>
                                <div class="input_control_container_preview">
                                    <label><?= $rows->first_registration_year ?></label>    
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">Register Year </div>
                                <div class="input_control_container_preview">
                                    <label><?= $rows->first_registration_year ?></label>  
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">Mileage </div>
                                <div class="input_control_container_preview">
                                    <label><?= ($rows->car_mileage!=0) ? $rows->car_mileage. ' Km' : '' ?></label>  
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">Engine Size </div>
                                <div class="input_control_container_preview">
                                    <label><?= ($rows->car_cc!=0) ? $rows->car_cc. ' CC' : '' ?></label>  
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">Chassis No </div>
                                <div class="input_control_container_preview">
                                    <label><?=  $rows->car_chassis_no ?></label>  
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">Body Type </div>
                                <div class="input_control_container_preview">
                                    <label>
                                    <?php
                                    foreach($bodytype_list as $bodytype){
                                        if($rows->car_body_type == $bodytype->body_name){
                                            echo $bodytype->body_title;
                                        }
                                    }
                                    ?>
                                    </label>
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">Transmission </div>
                                <div class="input_control_container_preview">
                                    <label><?= $rows->car_transmission ?></label>
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">Fule Type </div>
                                <div class="input_control_container_preview">
                                    <label><?= $rows->car_fuel ?></label>
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">FOB </div>
                                <div class="input_control_container_preview">
                                    <label><?= ($rows->car_fob_cost!=0) ? $rows->car_fob_cost .' USD' : '' ?></label>
                                </div>
                            </div>
                            <?php if($rows->old_fob_cost!=0) {?>
                            <div class="field_container">
                                <div class="input_label_preview">Previous FOB </div>
                                <div class="input_control_container_preview">
                                    <label><?= ($rows->old_fob_cost!=0) ? $rows->old_fob_cost .' USD' : '' ?></label>
                                </div>
                            </div> 
                            <?php } ?>
                            <div class="field_container bg_field_container"> 
                                <div class="input_label_preview">Car Dimension </div>
                                <div class="input_control_container_preview">
                                    <label><?= ($rows->car_length!=0) ? $rows->car_length : '' ?></label>
                                    <label><?= ($rows->car_width !=0) ? ' x '. $rows->car_width .' x ' : '' ?></label>
                                    <label><?= ($rows->car_height !=0) ? $rows->car_height. ' mm' : '' ?></label>
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">Grade </div>
                                <div class="input_control_container_preview">
                                    <label><?= $rows->car_grade ?></label>
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">Manu Year </div>
                                <div class="input_control_container_preview">
                                    <label><?= ($rows->manu_year!=0) ? $rows->manu_year : '' ?></label>
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">Door </div>
                                <div class="input_control_container_preview">
                                    <label><?= ($rows->door!=0) ? $rows->door : '' ?></label>
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">Seats </div>
                                <div class="input_control_container_preview">
                                    <label><?= ($rows->car_seat!=0) ? $rows->car_seat : '' ?></label>
                                </div>
                            </div>
                            <div class="field_container">
                                <div class="input_label_preview">Color </div>
                                <div class="input_control_container_preview">
                                    <label><?= $rows->car_color ?></label>
                                </div>
                            </div>
                            <div class="field_container bg_field_container">
                                <div class="input_label_preview">Drivr Type </div>
                                <div class="input_control_container_preview">
                                    <label><?= $rows->car_drive_type ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <?php

                }
            } 
            ?>

        </div>
    </div>
</div>