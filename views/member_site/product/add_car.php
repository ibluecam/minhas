<div class="outer">
    <!-- Navigation Bar -->
    
    <?php echo $navigation_bar; ?>

    <div class="inner add_info">
        <!-- SIDEBAR MENU-->
        
            <?php echo $sidebar; ?>

        <!-- ADD & MODIFY INFORMATION -->
        <div class="block_right" style="padding-top: 0">
            <!-- START BLOCK TITLE & QUICK ACTION -->
            <div class="page_title">
              Post Car
            </div>

            <form action="?c=admin&m=addcar_exec" enctype="multipart/form-data" method="post" id="adm_frm_car_info" name="adm_frm_car_info">
            
            <!-- START BLOCK ACTION STEP -->
            <div class="action_step">
                        
                <div class="step_car_info">
                    <input type="button" class="action_step_car_info step_action" value="CAR INFO">
                    <input type="button" class="action_step_car_image step_not_action" onclick="step_upload_image()" value="IMAGES">
                    <input type="submit" class="action_step_preview_save step_not_action" value="SAVE">
                </div>
                <div style="display:none" class="step_car_image">
                    <input type="button" class="action_step_car_info step_not_action" value="CAR INFO">
                    <input type="button" class="action_step_car_image step_action" value="IMAGES">
                    <input type="submit" class="action_step_preview_save step_not_action" value="SAVE">
                </div>

            </div>
            
            <input type="hidden" name="idx" id="idx" value="">
            <div id="car_info">    
                <div class="input_container">
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Location<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <select name="values[country]" class="input_select" id="country" required="required">>
                                    <?php foreach($country_list as $country){ ?>
                                    <option 
                                    <?php 
                                    
                                    if ($member_logged->member_country == $country->cc){
                                        echo 'selected="selected"';
                                    } 
                                        
                                    ?> 
                                    value="<?= $country->cc ?>"><?= $country->country_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="input_field_container">
                            <div class="input_control_container">
                                <input type="text" name="values[car_city]" placeholder="City" id="car_city" class="input_text">
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Condition<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <label><input type="radio" id="icon_new_yn_used" name="values[icon_new_yn]" value="N" checked="checked"> Used</label>&nbsp;&nbsp;&nbsp;
                                <label><input type="radio" id="icon_new_yn_new" name="values[icon_new_yn]" value="Y"> New</label>
                            </div>
                        </div>
                        
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Steering<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <label><input type="radio" id="car_steering_left" name="values[car_steering]" value="LHD" checked="checked"> Left</label>&nbsp;&nbsp;&nbsp;
                                <label><input type="radio" id="car_steering_right" name="values[car_steering]" value="RHD" > Right</label>
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Make<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <select id="make_list" class="input_select"  title="To add more Make Please Select -Add New Make-" required="required">
                                        <option value="">- Select -</option>
                                        <?php foreach($make_list as $make){ 
                                                $background_url =$make->icon_name;
                                        ?>
                                                <option value="<?php echo strtoupper($make->make_name) ?>" style="background-image: url('<?php echo $background_url; ?>');background-size:30px 15px; background-repeat: no-repeat;padding-left: 35px;" ><?php echo strtoupper($make->make_name); ?></option>
                                                
                                        <?php } ?>
                                        <option class="add_new_maker" value="add_new_maker" style="background-image: url('/images/admin/add.png');background-size:30px 15px; background-repeat: no-repeat;padding-left: 35px;">NEW MAKER</option>
                                </select>
                            </div>
                        </div>
                        <div class="input_field_container">
                            <div class="input_control_container">
                                <input type="text" style="display: none;" placeholder="New Make" id="car_make" name="values[car_make]" value="" class="input_text">
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Model<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <select id="model_list" title="To add more Model you can select -Add New Model-" class="input_select" required="required">
                                        <option value="">- Select -</option>

                                        <?php foreach($all_model_list as $model){ ?>
                                                <option class="<?php echo strtoupper($model->make_name); ?>" value="<?php echo strtoupper($model->model_name); ?>"><?php echo strtoupper($model->model_name); ?></option>
                                        <?php } ?>

                                         <option class="add_new_model" value="">-Add New Model-</option>
                                        
                                    </select>
                            </div>
                        </div>
                        <div class="input_field_container">
                            <div class="input_control_container">
                                <input type="text" style="display: none;" placeholder="New Model" id="car_model" name="values[car_model]" class="input_text">
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Model Year<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <select class="input_select" id="car_model_year" name="values[car_model_year]" required="required">
                                    <?= func_create_years(); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container" style="width: 450px;">
                            <div class="input_label">Mileage<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <input type="text" placeholder="2000" class="input_text" id="car_mileage" name="values[car_mileage]" required="required"> Km
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container" style="width: 450px;">
                            <div class="input_label">Engine Size<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <input type="text" placeholder="1000" class="input_text" id="car_cc" name="values[car_cc]" required="required"> CC
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Chassis No<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <input type="text" class="input_text" id="car_chassis_no" name="values[car_chassis_no]" required="required">
                                <label class="chassis_error" id="chassis_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Body Type<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <select class="input_select" id="car_body_type" name="values[car_body_type]" required="required">>
                                    <?php 
                                        foreach($bodytype_list as $bodytype){
                                        ?>
                                            <option value="<?= $bodytype->body_name ?>"><?= $bodytype->body_title ?></option>
                                        <?php
                                        } 
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Transmission<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <select class="input_select" id="car_transmission" name="values[car_transmission]">
                                    <?php foreach (Array('Auto', 'Manual') as $Val) { ?>
                                        <option value="<?= $Val ?>"><?= $Val ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Fule Type<span class="important_star">*</span> </div>
                            <div class="input_control_container">
                                <select class="input_select" id="car_fuel" name="values[car_fuel]">
                                    <?php foreach (Array('Petrol', 'Diesel', 'LPG', 'LPG + Petrol', 'Hybrid', 'Gasoline') as $Val) { ?>
                                        <option value="<?= $Val ?>"><?= $Val ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">FOB TYPE</div>
                            <div class="input_control_container">
                                <label><input type="radio" id="normal" checked="checked" name="normal_discount" value="normal"> Normal</label>&nbsp;&nbsp;&nbsp;
                                <label><input type="radio" id="discount" name="normal_discount" value="discount"> Discount</label>
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container" id="input_new_price">
                            <div class="input_label">Previous FOB </div>
                            <div class="input_control_container">
                                <input type="text" class="input_text" id="old_fob_cost" name="values[old_fob_cost]"> USD
                            </div>
                        </div>
                        <div class="input_field_container" id="input_filed_fob">
                            <div class="input_label">FOB </div>
                            <div class="input_control_container">
                                <input type="text" class="input_text" id="car_fob_cost" name="values[car_fob_cost]"> USD
                            </div>
                        </div>  
                    </div>
                    <div class="input_row">
                        <div class="input_field_container" style="width: 450px;">
                            <div class="input_label">Car Dimension </div>
                            <div class="input_control_container">
                                <input type="text" name="values[car_length]" id="car_length" placeholder="Length" class="input_text" style="width:63px" />
                                <input type="text" name="values[car_width]" id="car_width" placeholder="Width" class="input_text" style="width:63px"/>
                                <input type="text" name="values[car_height]" id="car_height" placeholder="Height" class="input_text" style="width:63px"/> mm
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Grade </div>
                            <div class="input_control_container">
                                <input type="text" class="input_text" id="car_grade" name="values[car_grade]">
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Registration Month </div>
                            <div class="input_control_container">
                                <select class="input_select" id="first_registration_month" name="values[first_registration_month]">
                                    <?= func_create_months(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="input_field_container">
                            <div class="input_control_container">
                                <select class="input_select" id="first_registration_year" name="values[first_registration_year]">
                                    <?= func_create_years(); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Manufactured Year </div>
                            <div class="input_control_container">
                                <input type="text" name="values[manu_year]" id="manu_year" class="input_text">
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Door </div>
                            <div class="input_control_container">
                                <input type="text" class="input_text" id="door" name="values[door]">
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Seats </div>
                            <div class="input_control_container">
                                <input type="text" class="input_text" id="car_seat" name="values[car_seat]">
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Color </div>
                            <div class="input_control_container">
                                <select class="input_select" id="car_color" name="values[car_color]">
                                    <option value="">- Select -</option>
                                        <?php 
                                        foreach($color_list as $color){
                                        ?>
                                            <option><?= $color->color_name; ?></option>
                                        <?php
                                        } 
                                        ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                        <div class="input_field_container">
                            <div class="input_label">Drive Type </div>
                            <div class="input_control_container">
                                <select class="input_select" id="car_drive_type" name="values[car_drive_type]">
                                    <option value="">- Select -</option>
                                        <? foreach(Array('2WD', '4WD') as $Val ) { ?>
                                            <option value="<?= $Val ?>"><?= $Val ?></option>
                                        <? } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="input_row">
                            <div class="input_label">Description </div>
                            <div class="input_control_container">
                                <textarea style="width:424px; height:100px" class="input_text" name="values[contents]"></textarea>
                            </div>
                    </div>
                </div>
            </div>
</form>
            <!-- BLOCK CAR IMAGES -->
            <div id="car_image">
                <div class="container_images">
                    <div class="input_row">
                        <!-- <div class="button_upload_image">
                            <a href="#" id="upload_widget_opener"><span>Upload images</span></a>
                        </div> -->
                    
                        <iframe width="100%" scrolling="no" id="upload_iframe" class="upload_iframe" src=""></iframe>
                        
                    </div>
                </div>
            </div>

            

        </div>
    </div>
</div>
<script type="text/javascript">

function resizeIframe(height){
    $("#upload_iframe").css("height", height);
}
// window.onload = function () {
    
//     setInterval(function() {
//         ResizeIframeFromParent('upload_iframe');
//     }, 1000);
// }



// function ResizeIframeFromParent(id) {
//     if (jQuery('#'+id).length > 0) {
//         var window = document.getElementById(id).contentWindow;
//         var prevheight = jQuery('#'+id).attr('height');
//         var newheight = Math.max( window.document.body.scrollHeight, window.document.body.offsetHeight, window.document.documentElement.clientHeight, window.document.documentElement.scrollHeight, window.document.documentElement.offsetHeight );
//         if (newheight != prevheight && newheight > 0) {
//             jQuery('#'+id).attr('height', newheight);
//             console.log("Adjusting iframe height for "+id+": " +prevheight+"px => "+newheight+"px");
//         }
//     }
// }

</script>