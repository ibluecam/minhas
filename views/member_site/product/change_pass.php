<?php
  echo $navigation_bar;
  echo $sidebar; ?>
<div class="block_right" style="background:white; box-shadow: 1px 1px 1px 1px #eee">
  <div class="page_title">Change Password</div>
  <form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=user_changepassword_exec" onsubmit="return insert_adm_bbs(this);">
    <div class="input_row">
      <div class="input_field_container">
        <div class="input_label">Old Password: </div>
        <div class="input_control_container"><input type="password" name="oldpassword" class="input_text"  value="" required=""/></div>
        
      </div>
    </div>
    <div class="input_row">
      <div class="input_field_container">
        <div class="input_label">New Password</div>
        <div class="input_control_container"><input type="password" name="newpassword"  value="" class="input_text" required=""/></div>
        
      </div>
    </div>
    <div class="input_row">
      <div class="input_field_container">
        <div class="input_label">Confirm Password</div>
        <div class="input_control_container"><input type="password" name="confirmpassword"  value="" class="input_text" required=""/></div>
        
      </div>
    </div>
    <div class="input_row">
      <div class="input_field_container">
        <div class="input_label"></div>
        <div class="input_control_container"><input type="submit" id="btn" class="btn_save" value="Change Password"></div>
        
      </div>
    </div>
      
      
    </div>
  </form>
</div>
