<!doctype html>
<html class="no-js" lang="en">
<head>
<style>
/*rado start quote*/
@charset "utf-8";
/*rado Start header*/

body{
	padding: 0px;
	margin: 0px;
	font-family:open sans;
}
.wrapp_content_quote{
	width:100%;
	background:url(/images/member_site/bg_content.png) repeat;
}
.content_quote{
	width:660px;
	margin: 0 auto;
	min-height:500px;
	background:url(/images/member_site/bg_content.png) repeat;
	color:#3d3d3d;
}
.quote_bgheader{
	background:url(/images/member_site/quote_bgheader.png) repeat;
	min-height:6px;
}
.quote_logo{
	text-align:center;
	padding-top:26px;
}
.quote_title_dear{
	font-size: 14px;
    margin-left: 26px;
    margin-top: 20px;
}
.kenta{
	color:#0772ba;
}
.wrapp_quote_you{
	font-size: 14px;
    margin-left: 26px;
    margin-top: 20px;
    overflow: auto;
}
.quote_text_you{
	width:330px;
	float:left;
}
.quote_text_make{
	color: #0772ba;
    float: left;
    margin-left: -26px;
    text-align: right;
    width: 304px;
}
.wrapp_quote_image{
	margin-left: 26px;
    margin-top: 20px;
}
.quote_image_top{
	float:left;
	width:200px;
}
.quote_image_middle{
	float:left;
	width:200px;
	text-align:center;
	font-size:12px;
	margin-right:4px;
}
.quote_image_down{
	float:left;
	width:200px;
	text-align:center;
	font-size:12px;
	margin-left:4px;
}
.quote_image_color_title{
	text-align:center;
	min-height:22px;
	color: #0772ba;
	font-size:14px;
}
.quote_image_color_brown{
	background: #e6f1f8 none repeat scroll 0 0;
    padding-bottom: 4px;
    padding-top: 4px;
    text-align: center;
}
.quote_image_color_brown_price{
	background: #e6f1f8 none repeat scroll 0 0;
    padding-bottom: 4px;
    padding-top: 4px;
    text-align: center;
	color: #0772ba;
}
.quote_image_color_white{
	background: #fefefe none repeat scroll 0 0;
	padding-bottom: 4px;
    padding-top: 4px;
    text-align: center;
}
.clear{
	clear:both;
}
.quote_car_specification{
	color: #0772ba;
    font-size: 14px;
    margin-bottom: 10px;
    padding-top: 22px;
    text-align: center;
	
}
.wrapp_car_specification{
	width:608px;
	font-size:12px;
	margin-bottom: 30px;
}
.quote_car_stickid{
	width:301px;
	float:left;
	margin-right:5px;
}
.quote_car_steering{
	width:301px;
	float:left;
	
}
.quote_car_stickid_stock{
	float:left;
	width:126px;
	padding-left:24px;
	background: #e6f1f8;
	padding-bottom: 4px;
    padding-top: 4px;
}
.quote_car_stickid_stock_num{
	float:left;
	width:127px;
	padding-right:24px;
	background: #e6f1f8;
	text-align:right;
	padding-bottom: 4px;
    padding-top: 4px;
}

.quote_car_stickid_stock_whit{
	float:left;
	width:126px;
	padding-left:24px;
	background: #fefefe;
	padding-bottom: 4px;
    padding-top: 4px;
}
.quote_car_stickid_stock_num_whit{
	float:left;
	width:127px;
	padding-right:24px;
	background: #fefefe;
	text-align:right;
	padding-bottom: 4px;
    padding-top: 4px;
}
.wrapp_star_please_view{
	overflow:auto;
	font-size:12px;
	margin-bottom:30px;
}
.quote_star_please{
	overflow: auto;
}
.quote_star{
	float:left;
	width:50px;
}
.quote_please{
	float:left;
	overflow:auto;
	width:558px;
	text-align: justify;
}
.wrapp_quote_company{
	overflow:auto;
	font-size:12px;
	margin-bottom:30px;
}
.quote_softbllom{
	margin-bottom:18px;
}
.wrapp_quote_footer{
	overflow:auto;
	font-size:12px;
}
.quote_footer{
	background: rgba(0, 0, 0, 0) url("/images/member_site/quote_bg_footer.png") no-repeat scroll 0 0;
    color: #fff;
    min-height: 31px;
    padding-top: 20px;
    text-align: center;
}



/*rado end quote*/
</style>
</head>
<body>
<div class="wrapp_content_quote">
    <div class="content_quote">
    <div class="quote_bgheader"></div>
        <div class="quote_logo"><img src="https://motorbb.com/images/member_site/motor_bb.png" border="0"></div>
        <div class="quote_title_dear">Dear <a class="kenta">Kenta</a>, (2602345343), ZAMBIA</div>
        <div class="wrapp_quote_you">
            <div class="quote_text_you">You ask for a quote on following vehicle:</div>
            <div class="quote_text_make">2015 Toyota Pruis Five Edition</div>
        </div>
        <div class="wrapp_quote_image">
        	<div class="quote_image_top"><img src="https://motorbb.com/images/member_site/quote_image.png" width="195" height="147" border="0"></div>
            <div class="quote_image_middle">
            	<div class="quote_image_color_title">Price Break Down</div>
                <div class="quote_image_color_brown">Fob Price</div>
                <div class="quote_image_color_white">Freight Cost</div>
                <div class="quote_image_color_brown">Inspection Fee</div>
                <div class="quote_image_color_white">MotorBB's Guarantee</div>
                <div class="quote_image_color_brown">Total Cost</div>
            </div>
            <div class="quote_image_down">
            	<div class="quote_image_color_title">Original Price</div>
                <div class="quote_image_color_brown">$ 18,600.00</div>
                <div class="quote_image_color_white">$ 900.00</div>
                <div class="quote_image_color_brown">$ 400</div>
                <div class="quote_image_color_white">Free</div>
                <div class="quote_image_color_brown_price">$ 19,900.00</div>
            </div>
            <div class="clear"></div>
            <div class="quote_car_specification">Car's Specification</div>
            <div class="wrapp_car_specification">
                <div class="quote_car_stickid">
                	<div class="quote_car_stickid_stock">Stock ID</div>
                    <div class="quote_car_stickid_stock_num">00ZBQF</div>
                </div>
                <div class="quote_car_steering">
                	<div class="quote_car_stickid_stock">Steering</div>
                    <div class="quote_car_stickid_stock_num">Right-hand drive</div>
                </div>
                
                <div class="quote_car_stickid">
                	<div class="quote_car_stickid_stock_whit">Condition</div>
                    <div class="quote_car_stickid_stock_num_whit">Used</div>
                </div>
                <div class="quote_car_steering">
                	<div class="quote_car_stickid_stock_whit">Transmission</div>
                    <div class="quote_car_stickid_stock_num_whit">AT</div>
                </div>
                
                <div class="quote_car_stickid">
                	<div class="quote_car_stickid_stock">Make</div>
                    <div class="quote_car_stickid_stock_num">TOYOTA</div>
                </div>
                <div class="quote_car_steering">
                	<div class="quote_car_stickid_stock">Fuel Type</div>
                    <div class="quote_car_stickid_stock_num">Petrol</div>
                </div>
                
                <div class="quote_car_stickid">
                	<div class="quote_car_stickid_stock_whit">Model</div>
                    <div class="quote_car_stickid_stock_num_whit">ALLEX</div>
                </div>
                <div class="quote_car_steering">
                	<div class="quote_car_stickid_stock_whit">Location</div>
                    <div class="quote_car_stickid_stock_num_whit">Japan</div>
                </div>
                
                <div class="quote_car_stickid">
                	<div class="quote_car_stickid_stock">Chassis No</div>
                    <div class="quote_car_stickid_stock_num">NZE121-5029984</div>
                </div>
                <div class="quote_car_steering">
                	<div class="quote_car_stickid_stock">Exterior Color</div>
                    <div class="quote_car_stickid_stock_num">Silver</div>
                </div>
                
                <div class="quote_car_stickid">
                	<div class="quote_car_stickid_stock_whit">Model Year</div>
                    <div class="quote_car_stickid_stock_num_whit">2002</div>
                </div>
                <div class="quote_car_steering">
                	<div class="quote_car_stickid_stock_whit">Manufactured Date</div>
                    <div class="quote_car_stickid_stock_num_whit">2002</div>
                </div>
                
                <div class="quote_car_stickid">
                	<div class="quote_car_stickid_stock">Mileage</div>
                    <div class="quote_car_stickid_stock_num">59</div>
                </div>
                <div class="quote_car_steering">
                	<div class="quote_car_stickid_stock">Registration Date</div>
                    <div class="quote_car_stickid_stock_num">6-2002</div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="wrapp_star_please_view">
                <div class="quote_star_please">
                        <div class="quote_star">*</div>
                        <div class="quote_please">Please view our offer and contact us for immediate confirmation.</div>
                    </div>
                    <div class="clear"></div>
                    <div class="quote_star_please">
                        <div class="quote_star">*</div>
                        <div class="quote_please">If you want confirm or further quotation please click here </div>
                    </div>
                    <div class="quote_star_please">
                        <div class="quote_star">*</div>
                        <div class="quote_please">You may also contact your account manager if you have any questions.</div>
                    </div>
                    <div class="quote_star_please">
                        <div class="quote_star">*</div>
                        <div class="quote_please">
                            We are commited to having the best prices and highest quality vehicles being exported from 
                            Japan and we appreciate your efforts in responding to our prices for negotiation.
                        </div>
                    </div>
                    <div class="clear"></div>
         	</div>
            <div class="wrapp_quote_company">
            	<div class="quote_softbllom">Softbloom CO,.LTD</div>
                <div>+81-45-290-9480/81</div>
                <div>Ark Hills Front Tower17F, 2-23-1 Akasaka, Minato-ku, Tokyo, Japan</div>
            </div>
            <div class="clear"></div>
            <div class="wrapp_quote_footer">
            	<div class="quote_footer">&copy; 2016 MINHAS Trading.</div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
</html>