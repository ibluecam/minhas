       
<?php 
    
    function updateCurrentGetUrl($p=array(), $unset_p=array()){
        $get = $_GET;
        if(count($unset_p)>0){
            foreach($unset_p as $value){
                if(isset($get[$value])) unset($get[$value]);
            }
        }
        if(count($p)>0){
            foreach($p as $key=>$value){
                $get[$key]=$value;
            }
        }
        $updatedUrl_arr = array();
        foreach($get as $key=>$value){
            $updatedUrl_arr[]=$key."=".$value;
        }
        $updatedUrl = implode("&", $updatedUrl_arr);
        return $updatedUrl;
    }
    function getOrderByFieldUrl($fieldname, $default="DESC"){
        if(isset($_GET['order_by'])) $order_by = $_GET['order_by']; else $order_by='';
        if(isset($_GET['order_d'])) $order_d = $_GET['order_d']; else $order_d=$default;
        if($order_by==$fieldname){
            if($order_d!="DESC"){
                $getUrl = updateCurrentGetUrl(array("order_by"=>$fieldname, "order_d"=>"DESC"));
            }else{
                $getUrl = updateCurrentGetUrl(array("order_by"=>$fieldname, "order_d"=>"ASC"));
            }
        }else{
            $getUrl = updateCurrentGetUrl(array("order_by"=>$fieldname, "order_d"=>$default));
        }
        return $getUrl;
    }
    
    $bbs_excel_customer_get = updateCurrentGetUrl(array("c"=>"admin", "m"=>"adm_bbs_excel_customer"));
    $bbs_excel_staff_get = updateCurrentGetUrl(array("c"=>"admin", "m"=>"adm_bbs_excel_staff"));


?>

       <!--carlist content start-->

       <?php

        if(isset($_GET['sch_word'])) $sch_word=  htmlspecialchars($_GET['sch_word']);else $sch_word="";
        if(isset($_GET['year'])) $year=  htmlspecialchars($_GET['year']);else $year="";
       ?>
     <div class="carData">
           <div class="title-carlist">
                    <p>CAR LIST</p>
            </div>
            <div class="carSearch_wrapper">
               <form  action="/?c=admin&m=buyer_car_list" method="post" enctype="multipart/form-data">
                    <input type="submit"  value="Reset" class="btn_reset">
               </form>
              <form id="image_form" name="" action="" method="get" enctype="multipart/form-data">
               <div class="search">
                   
                     <input type="hidden" name="c" value="admin">
                     <input type="hidden" name="m" value="buyer_car_list">
                     <input type="hidden" name="mcd" value="product">
                      <select class="formSelection" name="carmake" id="carmake">
                          <option value="">Make</option>
                             <?php 
                                foreach($select_make_all as $make){                                   
                              
                         ?> 
                        <option
                                 <?php 
                                 echo $carmake=(isset($_GET['carmake']) && $_GET['carmake']=="$make->car_make"? 'selected="selected"':''  );
                                 ?>
                                value="<?php echo $make->car_make; ?>"><?php echo $make->car_make; ?></option>
                        
                        <?php
                          } 
                        ?>
                        </select>
                        <select class="formSelection" name="carmodel" id="carmodel">
                          <option value="">Model</option>

                        
                        <?php foreach ($select_model_all as $model) {  ?>
                        
                           <option 
                              <?php 
                                 echo $carmodel=(isset($_GET['carmodel']) && $_GET['carmodel']=="$model->car_model"? 'selected="selected"':''  );
                              ?>
                            class="<?php echo $model->car_make; ?>" value="<?php echo $model->car_model; ?>"><?php echo $model->car_model; ?></option>
                      
                                      
                        <?php } ?> 

                        </select>
                        <select class="formSelection" name="year" id="year">
                          <option value="">Year</option>
                              <?php 
                                    for($i=date("Y"); $i>=1990; $i--){
                                ?>
                               <option <?php if(isset($_GET['year'])){ if($_GET['year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>

                            <?php 
                             }
                         ?>
                        </select>
                        <select class="formSelection" name="sch_icon_status" id="sch_icon_status">
                          <option value="">Status</option>
                             <?php foreach (Array('sale','reserved','shipok','releaseok','soldout') as $Val) { ?>
                                        <option <?php echo $sch_icon_status=(isset($_GET['sch_icon_status']) && $_GET['sch_icon_status']=="$Val"? 'selected="selected"':''); ?> 
                                                    value="<?php echo $Val; ?>"><?php echo ucfirst($Val); ?></option>
                            <?php } ?>   
                        </select>
                        <input class="keyword" value="<?php echo $sch_word; ?>" name="sch_word" type="text" placeholder="Keyword" />
                        <input type="submit" value="Search" name="btn_search" class="btn_search" />
                   </div>
                </form>
                 
                   
                 
               <form id="image_form" name="car_list" action="" method="post" enctype="multipart/form-data">
              
  
                   <div class="selected_files">
                   		<!-- <div><input type="checkbox" class="check_all"  name="all" id="selectall" /><span style="float:right;">Select All</div>
                        <div><label id="lable_selected"><span id="count_selected"></span></label></div>
                        <div class="delete_all"><a  href="#" title="delete" onclick="delete_all_items_check();">Delete</a></div> -->
                   </div>
             
                   
            </div>

       <div class="carSourceWraper">
        <?php
           if(count($bbs_list_sale)>0){
              foreach ($bbs_list_sale as $rows) {
       
         ?>
      
        
        <div class="listCar">
      
            <!-- <input type="checkbox" id="idx" class="checkcar" name="idxs[]" value="<?php echo $rows->idx; ?>"/> -->
           
            <?php
      
              if($rows->bbs_idx=='0' || $rows->bbs_idx==NULL && $rows->icon_status=="sale"){
                 echo "<a href='/?c=admin&amp;m=view_document_buyer&idx=$rows->idx'><img style='width:167px;height:125px;'  src='https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj'></a>";
              }
               else if($rows->icon_status=="reserved"){
                ?>
                
              
                   <?php
                  echo "<a href='/?c=admin&amp;m=view_document_buyer&idx=$rows->idx'><img  src='" . $rows->base_url . "c_scale,h_125,q_80,w_167/".$rows->public_id."'>";
                  ?>
                  <img class="statust_car" src="/images/member_site/reserved.png"/></a>

              
                <?php

              }
               
               
                else if($rows->icon_status=="soldout"){
                ?>
                
              
                   <?php
                  echo "<a href='/?c=admin&amp;m=view_document_buyer&idx=$rows->idx'><img  src='" . $rows->base_url . "c_scale,h_125,q_80,w_167/".$rows->public_id."'>";
                  ?>
                  <img class="statust_car" src="/images/member_site/soldout.png"/></a>

               
                <?php

              }



              else{
                echo "<a href='/?c=admin&amp;m=view_document_buyer&idx=$rows->idx'><img  src='" . $rows->base_url . "c_scale,h_125,q_80,w_167/".$rows->public_id."'></a>";
              }

             ?>
             <a href="/?c=admin&amp;m=view_document_buyer&idx=<?php echo $rows->idx; ?>">
            <div class="title"><span><?php if($rows->car_model_year==0){
                                echo $rows->car_make.' '.$rows->car_model; 
                           }                  
                           else{
                               echo $rows->car_model_year.' '.$rows->car_make.' '.$rows->car_model;  
                           }
                        ?></span></div>
            <div class="wrap_detail">
                   
                    <div class="line1"><span class="left">Chassis No :</span><span class="right"><?php echo substr($rows->car_chassis_no, -6); ?></span></div>
                 
                <div class="line2"><span class="left">Mileage :</span><span class="right"><?php if($rows->car_mileage=='0'){echo 'N/A';}else {echo $rows->car_mileage ." km";} ?></span></div>
                <div class="line3"><span class="left">Fob :</span><span class="right">
                  
                  <?php  if($rows->car_fob_cost=='' || $rows->car_fob_cost==0 || $rows->car_fob_cost==NULL){
                 echo 'Ask Price';
             }
             else{
            ?>  
                            
                                <? switch ($rows->car_fob_currency) {
                                    case 'USD':
                                        echo 'USD';
                                        break;
                                    case 'KRW':
                                        echo 'KRW';
                                        break;    
                                    case 'JPY':
                                        echo 'JPY';
                                        break;
                                    case 'EUR':
                                        echo 'EUR';
                                        break;   
                                    default:
                                        echo ' ';
                                        break;
                                } ?>

                                <?= number_format($rows->car_fob_cost) ?>
                                <?php } ?>

 
                </span></div></a>
             
            </div>

            <ul class="more_option">

              <div class="list_info">
               
              </div>
          
               
            </ul>
        </div>
       
        <?php
               }
             }
             else{
          ?>
             <?php
               if($_SESSION['ADMIN']['business_type']=='buyer' && (isset($_GET['sch_word'])) ){   


             ?>
               
                <div class="not_data">Search not found.</div>
             <?php
                }
                if($_SESSION['ADMIN']['business_type']=='buyer' && (!isset($_GET['sch_word'])) ){   
             ?>
              <div class="not_data">You don't have any reserved car Yet.</div>
             <?php
              }
             ?>
          
            </form>
           
            
        <?php
          }
        ?>
		</div>
       <table class="total_wrapper">
           <tr> 
            <?php
                 for ($x = 0; $x <$total_page/10; $x++) {
            }
           
           ?>
             <td class="td_total">
               <img border="0" src="/images/member_site/poin_icon.png" style="vertical-align: middle;">
               Total: <?php echo $total_rows ?> Cars,Page: <?php if($cur_page==0)
               {echo "1".'/'.$x;}else{echo ($cur_page+1).'/'.$x;} ?>
                

             </td>
             <td class="td_page">
               <?php
                 echo $pagination;
                ?>
             </td>
           </tr>
       </table>


       <!-- <div class="total">
        <span>
          <div class="footer_img_poin_icon" ><img border="0" src="/images/member_site/poin_icon.png" style="vertical-align: middle;">
           
          </div>Total: <?php echo $total_rows ?> Cars,<?php echo $cuurent; ?> Page:
       </span>
      </div>
      <div class="page_right">
        <div class="paginate" style="margin-right:500px; margin-bottom:25px; box-sizing: border-box; overflow: auto; padding: 20px;">
         <?php
           echo $pagination;
          ?>
      
      </div>  

      </div> -->
      
  
    


    

    </div>

       <!--carlist content end-->