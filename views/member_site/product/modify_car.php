<div class="outer">

    <!-- Navigation Bar -->

    

    <?php echo $navigation_bar; ?>



    <div class="inner add_info">

        <!-- SIDEBAR MENU-->

        

            <?php echo $sidebar; ?>



        <!-- ADD & MODIFY INFORMATION -->

        <div class="block_right" style="padding-top: 0">

            <!-- START BLOCK TITLE & QUICK ACTION -->

            <div class="page_title">

              <?php if($bbs_detail_list > 0){ ?>

              <?= $bbs_detail_list[0]->car_model_year." ".$bbs_detail_list[0]->car_make." ".$bbs_detail_list[0]->car_model; ?>

              <?php } ?>

            </div>



            <?php



            if (count($bbs_detail_list) > 0) {

                foreach ($bbs_detail_list as $rows) {

            ?>

            <form action="?c=admin&m=modify_car_exec&idx=<?=$rows->idx?>" enctype="multipart/form-data" method="post" id="adm_frm_car_info" name="adm_frm_car_info">

            

            <!-- START BLOCK ACTION STEP -->

            <div class="action_step">

                      

                <div class="step_car_info">

                    <input type="button" class="action_step_car_info step_action" value="CAR INFO">

                    <input type="button" class="action_step_car_image step_not_action" value="IMAGES">

                    <input type="submit" class="action_step_preview_save step_not_action" value="SAVE">

                </div>

                <div style="display:none" class="step_car_image">

                    <input type="button" class="action_step_car_info step_not_action" value="CAR INFO">

                    <input type="button" class="action_step_car_image step_action" value="IMAGES">

                    <input type="submit" class="action_step_preview_save step_not_action" value="SAVE">

                </div>



            </div>

            

            <input type="hidden" name="idx" id="idx" value="<?=$rows->idx?>">

            <div id="car_info">    

                <div class="input_container">

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Location<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <select name="values[country]" class="input_select" id="country">

                                    <?php foreach($country_list as $country){ ?>

                                    <option 

                                    <?php 

                                    if ($rows->country !=''){

                                        if($rows->country == $country->cc){

                                                echo 'selected="selected"';

                                        }

                                    }else{

                                        if ($member_logged->member_country == $country->cc){

                                            echo 'selected="selected"';

                                        } 

                                    }

                                        

                                    ?> 

                                    value="<?= $country->cc ?>"><?= $country->country_name ?></option>

                                    <?php } ?>

                                </select>

                            </div>

                        </div>

                        <div class="input_field_container">

                            <div class="input_control_container">

                                <input type="text" name="values[car_city]" placeholder="City" id="car_city" value="<?= $rows->car_city ?>" class="input_text">

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Condition<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <label><input type="radio" id="icon_new_yn_used" name="values[icon_new_yn]" <?php if($rows->icon_new_yn=='N' || $rows->icon_new_yn=='' ) echo "checked"; ?> value="N"> Used</label>&nbsp;&nbsp;&nbsp;

                                <label><input type="radio" id="icon_new_yn_new" name="values[icon_new_yn]" <?php if($rows->icon_new_yn=='Y' ) echo "checked"; ?> value="Y"> New</label>

                            </div>

                        </div>

                        

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Availability<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <select class="input_select" name="values[icon_status]" id="icon_status">

                                        <? foreach(Array('sale', 'reserved', 'soldout') as $Val ) { ?>

                                            <option <?= ($rows->icon_status==$Val)? 'selected="selected"' : '' ?> value="<?= $Val ?>"><?= $Val ?></option>

                                        <? } ?>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Steering<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <label><input type="radio" id="car_steering_left" name="values[car_steering]" <?php if($rows->car_steering=='LHD' || $rows->car_steering=='' ) echo "checked"; ?> value="LHD" > Left</label>&nbsp;&nbsp;&nbsp;

                                <label><input type="radio" id="car_steering_right" name="values[car_steering]" <?php if($rows->car_steering=='RHD') echo "checked"; ?> value="RHD" > Right</label>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Make<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <select id="make_list" class="input_select"  title="To add more Make Please Select -Add New Make-" required="required">>

                                        <option value="">- Select -</option>

                                        <?php foreach($make_list as $make){ 

                                                $background_url =$make->icon_name;

                                        ?>

                                                <option <?php if(isset($rows->car_make)){ if(strtoupper($rows->car_make) == strtoupper($make->make_name)){ ?> SELECTED <?php } } ?> value="<?php echo strtoupper($make->make_name) ?>" style="background-image: url('<?php echo $background_url; ?>');background-size:30px 15px; background-repeat: no-repeat;padding-left: 35px;" ><?php echo strtoupper($make->make_name); ?></option>

                                                

                                        <?php } ?>

                                        <option class="add_new_maker" value="add_new_maker" style="background-image: url('/images/admin/add.png');background-size:30px 15px; background-repeat: no-repeat;padding-left: 35px;">NEW MAKER</option>

                                </select>

                            </div>

                        </div>

                        <div class="input_field_container">

                            <div class="input_control_container">

                                <input type="text" style="display: none;" placeholder="Naw Make" id="car_make" value="<?= $rows->car_make ?>" name="values[car_make]" value="" class="input_text">

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Model<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <select id="model_list" title="To add more Model you can select -Add New Model-" class="input_select" required="required">>

                                        <option value="">- Select -</option>



                                        <?php foreach($all_model_list as $model){ ?>

                                                <option <?php if(isset($rows->car_model)){ if(strtoupper($rows->car_model) == strtoupper($model->model_name)){ ?> SELECTED <?php } } ?> class="<?php echo strtoupper($model->make_name); ?>" value="<?php echo strtoupper($model->model_name); ?>"><?php echo strtoupper($model->model_name); ?></option>

                                        <?php } ?>



                                         <option class="add_new_model" value="">-Add New Model-</option>

                                        

                                    </select>

                            </div>

                        </div>

                        <div class="input_field_container">

                            <div class="input_control_container">

                                <input type="text" style="display: none;" placeholder="New Model" id="car_model" value="<?= $rows->car_model ?>" name="values[car_model]" class="input_text">

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Model Year<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <select class="input_select" id="car_model_year" name="values[car_model_year]" required="required">>

                                    <?= func_create_years($rows->car_model_year); ?>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container" style="width: 450px;">

                            <div class="input_label">Mileage<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <input type="text" class="input_text" placeholder="2000" id="car_mileage" name="values[car_mileage]" value="<?= ($rows->car_mileage!=0) ? $rows->car_mileage : '' ?>" required="required"> Km

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container" style="width: 450px;">

                            <div class="input_label">Engine Size<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <input type="text" class="input_text" id="car_cc" name="values[car_cc]" value="<?= ($rows->car_cc!=0) ? $rows->car_cc : '' ?>" required="required"> CC

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Chassis No<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <input type="text" class="input_text" id="car_chassis_no" name="values[car_chassis_no]" value="<?= $rows->car_chassis_no ?>" required="required">

                                <label class="chassis_error" id="chassis_error"></label>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Body Type<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <select class="input_select" id="car_body_type" name="values[car_body_type]" required>

                                    <?php 

                                        foreach($bodytype_list as $bodytype){

                                        ?>

                                            <option <?= ($rows->car_body_type == $bodytype->body_name) ? 'selected="selected"' : '' ?> value="<?= $bodytype->body_name ?>"><?= $bodytype->body_title ?></option>

                                        <?php

                                        } 

                                    ?>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Transmission<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <select class="input_select" id="car_transmission" name="values[car_transmission]">

                                    <?php foreach (Array('Auto', 'Manual') as $Val) { ?>

                                        <option <?= ($rows->car_transmission==$Val)? 'selected="selected"' : '' ?> value="<?= $Val ?>"><?= $Val ?></option>

                                    <?php } ?>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Fule Type<span class="important_star">*</span> </div>

                            <div class="input_control_container">

                                <select class="input_select" id="car_fuel" name="values[car_fuel]">

                                    <?php foreach (Array('Petrol', 'Diesel', 'LPG', 'LPG + Petrol', 'Hybrid', 'Gasoline') as $Val) { ?>

                                        <option <?= ($rows->car_fuel==$Val)? 'selected="selected"' : '' ?> value="<?= $Val ?>"><?= $Val ?></option>

                                    <?php } ?>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">FOB TYPE</div>

                            <div class="input_control_container">

                                <label><input type="radio" id="normal" name="normal_discount" <?php if($rows->car_fob_cost!='') echo 'checked';?> value="normal"> Normal</label>&nbsp;&nbsp;&nbsp;

                                <label><input type="radio" id="discount" name="normal_discount" <?php if(!empty($rows->old_fob_cost)) echo 'checked';?> value="discount"> Discount</label>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container" id="input_new_price">

                            <div class="input_label">Previous FOB </div>

                            <div class="input_control_container">

                                <input type="text" class="input_text" id="old_fob_cost" name="values[old_fob_cost]" value="<?=($rows->old_fob_cost!=0) ? $rows->old_fob_cost : '' ?>"> USD

                            </div>

                            <!-- <div class="input_control_container" style="margin-top: 5px">

                                <a href="#" data-url="/?c=admin&amp;m=discount_car&idx=<?=$rows->idx?>&fob_cost=<?=$rows->car_fob_cost?>" class="btn_quick_discount input_button">Discount</a>

                            </div> -->

                        </div>  

                        <div class="input_field_container" id="input_filed_fob">

                            <div class="input_label">FOB </div>

                            <div class="input_control_container">

                                <input type="text "class="input_text" id="car_fob_cost" name="values[car_fob_cost]" value="<?=($rows->car_fob_cost!=0) ? $rows->car_fob_cost : '' ?>"> USD

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container" style="width: 450px;">

                            <div class="input_label">Car Dimension</div>

                            <div class="input_control_container">

                                <input type="text" name="values[car_length]" id="car_length" placeholder="Length" value="<?= ($rows->car_length!=0) ? $rows->car_length : '' ?>" class="input_text" style="width:63px" />

                                <input type="text" name="values[car_width]" id="car_width" placeholder="Width" value="<?= ($rows->car_width!=0) ? $rows->car_width : '' ?>" class="input_text" style="width:63px"/>

                                <input type="text" name="values[car_height]" id="car_height" placeholder="Height" value="<?= ($rows->car_height!=0) ? $rows->car_height : '' ?>" class="input_text" style="width:63px"/> mm

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Grade </div>

                            <div class="input_control_container">

                                <input type="text" class="input_text" id="car_grade" name="values[car_grade]" value="<?= $rows->car_grade ?>">

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Registration Month </div>

                            <div class="input_control_container">

                                <select class="input_select" id="first_registration_month" name="values[first_registration_month]">

                                    <?= func_create_months($rows->first_registration_month); ?>

                                </select>

                            </div>

                        </div>

                        <div class="input_field_container">

                            <div class="input_control_container">

                                <select class="input_select" id="first_registration_year" name="values[first_registration_year]">

                                    <?= func_create_years($rows->first_registration_year); ?>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Manufactured Year </div>

                            <div class="input_control_container">

                                <input type="text" name="values[manu_year]" id="manu_year" class="input_text" value="<?= ($rows->manu_year!=0) ? $rows->manu_year : '' ?>">

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Door </div>

                            <div class="input_control_container">

                                <input type="text" class="input_text" id="door" name="values[door]" value="<?= ($rows->door!=0) ? $rows->door : '' ?>">

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Seats </div>

                            <div class="input_control_container">

                                <input type="text" class="input_text" id="car_seat" name="values[car_seat]" value="<?= ($rows->car_seat!=0) ? $rows->car_seat : '' ?>">

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Color </div>

                            <div class="input_control_container">

                                <select class="input_select" id="car_color" name="values[car_color]">

                                    <option value="">- Select -</option>

                                        <?php 

                                        foreach($color_list as $color){

                                        ?>

                                            <option  <?=($rows->car_color == $color->color_name) ? 'selected="selected"' : '' ?> value="<?= $color->color_name; ?>"><?= $color->color_name; ?></option>

                                        <?php

                                        } 

                                        ?>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                        <div class="input_field_container">

                            <div class="input_label">Drive Type </div>

                            <div class="input_control_container">

                                <select class="input_select" id="car_drive_type" name="values[car_drive_type]">

                                    <option value="">- Select -</option>

                                        <? foreach(Array('2WD', '4WD') as $Val ) { ?>

                                            <option <?= ($rows->car_drive_type==$Val)? 'selected="selected"' : '' ?> value="<?= $Val ?>"><?= $Val ?></option>

                                        <? } ?>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="input_row">

                            <div class="input_label">Description </div>

                            <div class="input_control_container">

                                <textarea style="width:424px; height:100px" class="input_text" name="values[contents]"><?= ($rows->contents!="") ? $rows->contents : '' ?></textarea>
                            
                            </div>

                    </div>

                </div>

            </div>



            <!-- BLOCK CAR IMAGES -->

            <div id="car_image">

                <div class="container_images">

                    <div class="input_row">

                       <!--  <div class="button_upload_image">

                            <a href="#" id="upload_widget_opener"><span>Upload images</span></a>

                        </div> -->



                        <iframe id="upload_iframe" width="100%" class="upload_iframe" src="/?c=admin&m=adm_upload_image_car&idx=<?=$_GET['idx']?>&mcd=product" scrolling="no"></iframe>

            

                    </div>

                </div>

            </div>

            </form>

                <?php



                }

            } 

            ?>



        </div>

    </div>

</div>



<script type="text/javascript">

function resizeIframe(height){
    $("#upload_iframe").css("height", height);
}

// window.onload = function () {

    

//     setInterval(function() {

//         ResizeIframeFromParent('upload_iframe');

//     }, 1000);

// }

// function ResizeIframeFromParent(id) {

//     if (jQuery('#'+id).length > 0) {

//         var window = document.getElementById(id).contentWindow;

//         var prevheight = jQuery('#'+id).attr('height');

//         var newheight = Math.max( window.document.body.scrollHeight, window.document.body.offsetHeight, window.document.documentElement.clientHeight, window.document.documentElement.scrollHeight, window.document.documentElement.offsetHeight );

//         if (newheight != prevheight && newheight > 0) {

//             jQuery('#'+id).attr('height', newheight);

//             console.log("Adjusting iframe height for "+id+": " +prevheight+"px => "+newheight+" px");

//         }

//     }

// }

</script>