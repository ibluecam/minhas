
  <div class="outer wrapp_wrpp_header_outer">
      <div class="inner wrapp_contact_us">
      <?php if(!empty($_SESSION['ADMIN']['member_id'])){ ?>
        <div class="head_user_block">
          <ul class="user_dropdown_nav">
            <li>
            <a href="mailto:info@motorbb.com"><img src="images/personal_user.png"></a>
            <a class="user_id" href="#"><?php echo ucfirst($_SESSION['ADMIN']['member_id']); ?> <img src="images/or_drop.png"></a>
              <ul class="dropdown" >
                <li><a href="/?c=admin&m=member_info">My Info</a></li>
                <li><a href="/?c=user&m=logout_exec">Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div>
      <?php } ?>
        <div class="head_mail_us">
          <a href="mailto:info@motorbb.com"><img src="images/mail_us.png"></a>
          <a href="mailto:info@motorbb.com">Mail us</a>
        </div>
        <div class="head_call_us">
          <img src="images/call_us.png">
          Call Us <b>81-80-4095-0379</b>
        </div>
      </div>
      <div class="inner wrapp_hader_inner">
        <div class="clear"></div>
      	<div class="wrapp_header_logo">
        	<div class="header_logo"><a href="/"><img src="/images/public_site/header_logo.png" border="0"></a></div>
        </div>

        <div class="wrapp_header_right">

            <div class="wrapp_header_wellcome">
                    
                   <?php
                    if(empty($_SESSION['ADMIN']['member_id'])){
                   ?>
                     <div class="head_welcom">
                       <a class="head_total_car_in_stock">Total Cars In Stock</a><a class="head_total_fob"><?php echo number_format($total_bbs_count); ?></a>
                    </div>
                    <div class="head_username">
                    	<div class="header_login"><a href="/?c=user&m=public_login"><img src="/images/public_site/login.png"></a></div>
                        <div class="header_img_button_register">
                        	<a href="/?c=user&m=public_register"><img src="/images/public_site/register.png"></a>
                        </div>
                  </div>  
                        <div class="clear"></div>
                  <?php }
                  else{     
                  ?>
                  <div class="head_welcome_logout">
                    <a class="head_total_car_in_stock">Total Cars In Stock</a><a class="head_total_fob"><?php echo number_format($total_bbs_count); ?></a>
                  </div>
                  <div class="head_username_logout">
                      <div class="header_login_user">
                        <!-- <span class="user_hello">Hi</span> -->
                        <?if($_SESSION['ADMIN']['member_id']!= ''){
                            //echo "<span>".$_SESSION['ADMIN']['member_id']."</span>";  
                        }?>
                      </div>
                      <div class="header_login_logout">
                       <!--  <input type="submit" value="Memember Site" name="submit" class="member_bnt"> -->
                      <a href="/?c=admin&m=addcar">
                        <img src="/images/public_site/add_car.png" border="0">
                      </a>
                      <a href="/?c=admin">
                        <img src="/images/public_site/member_site.png" border="0">
                      </a>
                      </div>
                        <!-- <div class="header_img_button_register">
                          <a href="/?c=user&m=logout_exec"><img src="/images/public_site/log out.png" border="0"></a>
                        </div> -->
                  </div>  
                  <?php
                   }
                 ?>  
                    <div class="clear"></div>         
            </div> 
            
            </div>
               
      	</div>
        <div class="outer wrapp_nav_outer">
        	<div class="inner nav_inner">
            	<div class="nav_menu">
                	<ul class="nav">
                    	 <li <?php if($action_menu == "public_home"){ ?>class='active'<?php } ?>><a class="menu" href="/">Home</a></li>
                       <li <?php if($action_menu == "car_list"){ ?>class='active'<?php } ?>><a href="/?c=user&m=car_list">Cars</a></li>
                       <li <?php if($action_menu == "public_seller"){ ?>class='active'<?php } ?>><a href="/?c=user&m=public_seller">Sellers</a></li>
                       <li <?php if($action_menu == "how_to_buy"){ ?>class='active'<?php } ?>><a href="/?c=user&m=how_to_buy">How to buy</a></li>
                       <li <?php if($action_menu == "our_customer"){ ?>class='active'<?php } ?>><a href="/?c=user&m=our_customer">Our customer</a></li>
                       <li <?php if($action_menu == "about_us"){ ?>class='active'<?php } ?>><a href="/?c=user&m=about_us">About us</a></li>
                       <li <?php if($action_menu == "contact_us"){ ?>class='active'<?php } ?>><a href="/?c=user&m=contact_us">Contact us</a></li>
                    </ul>
                </div>
                <div class="nav_follow_us">
                	<div class="nav_follow_us_text">follow us :</div>
                    <div class="nav_follow_us_img">
                    	  <a href="https://www.facebook.com/sb.motorbb" target="_blank"><img src="/images/public_site/follow_fb.png" border="0"></a>&nbsp;&nbsp;
                        <a href="https://twitter.com/motorbbgroup" target="_blank"><img src="/images/public_site/tweeter.png" border="0"></a>&nbsp;&nbsp;
                        <a href="https://www.youtube.com/channel/UCBUHxSHcye0_LluyJ1_dQFA/feed" target="_blank"><img src="/images/public_site/youtub.png" border="0"></a>
                    </div>
                </div>
            </div>
        </div> 
      </div>
  </div>