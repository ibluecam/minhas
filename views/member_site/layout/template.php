<!doctype html>
<html class="no-js" lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<head>
  		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    	<link rel="stylesheet" href="/css/member_site/global.css" />
	    <?php 
		    /*** OUTPUT EXTERNAL CSS LINK ***/
		    foreach($arr_ex_css as $ex_css){ 
		        echo '<link rel="stylesheet" href="'.$ex_css.'" />';
		    } 
		?>
		
    <title>Motorbb Member</title>
  	</head>
  	<body>


		<?php include('views/public_site/layout/header.php'); ?>


		<div class="outer wrapp_content_outer">
	    	<div class="inner content_inner">
	        	<?php echo $page_content; ?>
	    	</div>
		</div>

		<?php 

			include('footer.php');
                 ?>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                        <?php 
            
            /*** OUTPUT EXTERNAL JS LINK ***/
	    	foreach($arr_ex_js as $ex_js){
	        	echo '<script type="text/javascript" src="'.$ex_js.'"></script>';
	    	}

		?>
	</body>
</html>