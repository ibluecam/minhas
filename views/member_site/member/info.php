
<?php 
  echo $navigation_bar;
  echo $sidebar; 
?>

<div class="block_right" style="background:white; box-shadow: 1px 1px 1px 1px #eee;">
	<div class="page_title">
		My info
	</div>
	<div>
    <form onsubmit="return insert_adm_bbs(this);" action="/?c=admin&amp;m=adm_member_info_exec" enctype="multipart/form-data" method="post" id="adm_frm" name="adm_frm">
      <!-- <div class="input_row_info">
        <div class="input_field_container">
          <div class="input_label">Business Type: </div>
          <div class="input_control_container_info"><label><?= $row->business_type;?></label></div>
          
        </div>
      </div> -->
      <div class="input_row_info_info">
        <div class="input_field_container">
          <div class="input_label">Login ID: </div>
          <div class="input_control_container_info"><?= $row->member_id; ?></div>
          
        </div>
        <div class="input_field_container">
          <div class="input_label">Password</div>
          <div class="input_control_container_info">
            <a style="text-decoration: none; color: blue;" href="/?c=admin&m=change_password">Change</a>
          </div>
          
        </div>
      </div>
      <div class="input_row_info">
        
      </div>
      <div class="input_row_info">
        <div class="input_field_container">
          <div class="input_label">Company Name </div>
          <div class="input_control_container_info"><input type="text" name="values[company_name]" value="<?= $row->company_name; ?>" class="input_text"/></div>
          
        </div>
        <div class="input_field_container">
          <div class="input_label">Contact Person </div>
          <div class="input_control_container_info"><input type="text" name="values[contact_person_name]" value="<?= $row->contact_person_name; ?>" class="input_text"/></div>
          
        </div>
      </div>
      <div class="input_row_info">
        
      </div>
      <div class="input_row_info">
        <div class="input_field_container">
          <div class="input_label">Email </div>
          <div class="input_control_container_info"><input type="text" name="values[email]" value="<?= $row->email; ?>" class="input_text"/></div>
          
        </div>
      </div>
      
      <div class="input_row_info">
        <div class="input_field_container">
          <div class="input_label">Phone Number</div>
          <div class="input_control_container_info"><input type="text" name="values[phone_no1]" id="phone" placeholder="Phone" class="input_text buyer-mobile-number" value="<?= "+".$row->phone_no1." ".$row->phone_no2; ?>" /></div>
          
        </div>
        <div class="input_field_container">
          <div class="input_label">Mobile Number</div>
          <div class="input_control_container_info"><input type="text" name="values[mobile_no1]" id="mobile" placeholder="Mobile" class="input_text buyer-mobile-number" value="<?= "+".$row->mobile_no1." ".$row->mobile_no2; ?>" /></div>
            
        </div>
      </div>
      
      <div class="input_row_info">
        <div class="input_field_container">
          <div class="input_label">Country</div>
          <div class="input_control_container_info">
            <select name="values[member_country]" class="input_select">
              <option value="">- Select -</option>
              <?php 


              foreach($country_list as $country){
                $select = '';
                if($row->member_country == $country->cc){
                  $select = "selected";
                }
                echo '<option '.$select.' value="'.$country->cc.'">'.$country->country_name.'</option>';
              } ?>
            </select>
          </div>
          
        </div>
        <div class="input_field_container">
          <div class="input_label">Address </div>
          <div class="input_control_container_info"><input name="values[address]" type="text" value="<?= $row->address; ?>" class="input_text"/></div>
          
        </div>
      </div>
      <div class="input_row_info">
        <div class="input_field_container" style="width:100%;">
          <div class="input_label">Description</div>
          <div class="input_control_container_info"><textarea style="width:100%; max-width:570px;" name="values[user_memo]" class="input_textarea"><?= $row->user_memo; ?></textarea></div>
          
        </div>
      </div>
      <div class="input_row_info">
        <div class="input_field_container" style="width:100%;">
          <div class="input_label">Images</div>
          <div class="input_control_container_info"><iframe height="280px" width="780" class="upload_iframe" src="/?c=admin&m=user_cloud_upload&member_no=<?=$_SESSION['ADMIN']['member_no']?>"></iframe></div>
            
        </div>
      </div>

      <div class="input_row_info">
        <input style="float:right; border-radius: 5px;" type="submit" class="btn-info" id="btn-info-insert" value="Save">
      </div>
      
      
    </form>    
	</div>
</div>
