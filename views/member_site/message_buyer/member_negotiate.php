<?php 
    
    function updateCurrentGetUrl($p=array(), $unset_p=array()){
        $get = $_GET;
        if(count($unset_p)>0){
            foreach($unset_p as $value){
                if(isset($get[$value])) unset($get[$value]);
            }
        }
        if(count($p)>0){
            foreach($p as $key=>$value){
                $get[$key]=$value;
            }
        }
        $updatedUrl_arr = array();
        foreach($get as $key=>$value){
            $updatedUrl_arr[]=$key."=".$value;
        }
        $updatedUrl = implode("&", $updatedUrl_arr);
        return $updatedUrl;
    }
?>
<?php
   
   if(isset($_GET['sch_car_location'])) $sch_car_location=  htmlspecialchars($_GET['sch_car_location']);else $sch_car_location="";
   if(isset($_GET['sch_create_dt_s'])) $sch_create_dt_s=  htmlspecialchars($_GET['sch_create_dt_s']);else $sch_create_dt_s="";
   if(isset($_GET['sch_create_dt_e'])) $sch_create_dt_e=  htmlspecialchars($_GET['sch_create_dt_e']);else $sch_create_dt_e="";
   if(isset($_GET['sch_icon_status'])) $sch_icon_status=  htmlspecialchars($_GET['sch_icon_status']);else $sch_icon_status="";
   if(isset($_GET['sch_word'])) $sch_word=  htmlspecialchars($_GET['sch_word']);else $sch_word="";
   if(isset($_GET['carmake'])) $carmake=  htmlspecialchars($_GET['carmake']);else $carmake="";
   if(isset($_GET['carmodel'])) $carmodel=  htmlspecialchars($_GET['carmodel']);else $carmodel="";
   if(isset($_GET['staff_name'])) $staff_name=  htmlspecialchars($_GET['staff_name']);else $staff_name="";
   echo $navigation_bar;
   echo $sidebar;
?>

<div class="block_right" style="padding-top: 0">
	
    <div class="page_title">Negotiate</div>


	<div class="table_list_box">
		<h1 class="table_title">Search</h1>
	</div>
        
    <!--wrap search_box start-->
    <div class="search_box">
		<div style="width: 100%; padding: 10px;">
            Result :
            <span class="title" style="color:red"><?= $total_rows ?></span> found<!--, Pages :<span class="title"><?= $cur_page ?></span>/<?= $total_page ?>-->
        </div>

		<div class="search_result">
			<div class="search_condition" style="text-align:left">
				<form name="bbs_search_frm" id="adm_frm" method="get" action="" enctype="multipart/form-data">
                    <input type="hidden" name="c" value="admin">
                    <input type="hidden" name="m" value="member_negotiate">
                    
                   
                    
                    <select name="sch_car_location" class="input_2" style="width:110px;" align="absmiddle">
                        <option value="">Country</option>
                        <?php 
                            foreach($country_list as $country){
                            ?>
                            <option
                                 <?php 
                           echo $sch_car_location=(isset($_GET['sch_car_location']) && $_GET['sch_car_location']=="$country->cc"? 'selected="selected"':''  );
                                 ?>
                                value="<?php echo $country->cc; ?>"><?php echo $country->country_name; ?></option>
                            <?php
                            } 
                        ?>
                    </select>
                           
                    <select name="carmake" id="member_carmake" class="input_2" style="width:110px;" align="absmiddle">
                        <option value="">Make</option>
                        <?php 
                                foreach($select_make_all as $make){                                   
                              
                         ?> 
                        <option
                                 <?php 
                                 echo $carmake=(isset($_GET['carmake']) && $_GET['carmake']=="$make->car_make"? 'selected="selected"':''  );
                                 ?>
                                value="<?php echo $make->car_make; ?>"><?php echo $make->car_make; ?></option>
                        
                        <?php
                          } 
                        ?>
                    </select>

                    <select id="member_carmodel" name="carmodel" class="input_2" style="width:110px;" align="absmiddle">
                        <option value="">Model</option>
                        
                        <?php foreach ($select_model_all as $model) {  ?>
                        
                        <option <?php 
                                 echo $carmodel=(isset($_GET['carmodel']) && $_GET['carmodel']=="$model->car_model"? 'selected="selected"':''  );
                                 ?> class="<?php echo $model->car_make; ?>" value="<?php echo $model->car_model; ?>"><?php echo $model->car_model; ?></option>
                                       
                        <?php } ?>
                    </select>
                    
                    <select name="sch_icon_status" id="sch_icon_status" class="input_2" style="width:110px;" align="absmiddle">
                        <option value="">Status</option>
                         <?php foreach (Array('sale','reserved','shipok','releaseok','soldout') as $Val) { ?>
                                        <option <?php echo $sch_icon_status=(isset($_GET['sch_icon_status']) && $_GET['sch_icon_status']=="$Val"? 'selected="selected"':''); ?> 
                                                    value="<?php echo $Val; ?>"><?php echo ucfirst($Val); ?></option>
                        <?php } ?> 
                    </select>
                    
                    <!-- <select name="staff_name" id="staff_name" class="input_2" style="width:125px;" align="absmiddle">
                        <option value="">Staff Name</option>
                        <?php foreach($sale_staff as $cs){ ?>
                          <option <?php 
                                 echo $staff_name=(isset($_GET['staff_name']) && $_GET['staff_name']=="$cs->member_no"? 'selected="selected"':''  );
                                 ?> value="<?= $cs->member_no ?>"><?= $cs->member_name ?></option>
                      <?php } ?>
					</select> -->

                     <input type="text" name="sch_word" id="sch_word" value="<?php echo $sch_word; ?>" class="input_1" style="width:120px;" placeholder="Keyword..." />

                    <input type="image" src="/images/admin/btn_search_01.gif" style="width:60px;" align="absmiddle" />
                    <input type="reset" id="reset" value="Clear" style="background: #504f4b none repeat scroll 0 0;
    border: medium none;border-radius: 0.3em;color: #fff;height: 25px;width: 62px;cursor: pointer;">
                   </form>
                   </div>


					<br/><div class="hearder_button">
                       <a href="/?c=admin&m=member_negotiate&msg=unread"/><span class="button blue btn_send_rec">Unread</span></a>
                       <a href="#"><span class="button rosy btnGetAll">Delete</span></a>  
                       </div>

				</div>

			</div><!--wrap search_box end-->

	<div style="clear:both"></div>
			
	<!--Wrap Message list start-->
    <form id="load_form" name="load_form" method='post' action="" style="width:100%;">
    
    <table class="wrap_content_negotiate" cellpadding="0" cellspacing="0">
        <tr>
        	<th style="width:25px"><input type="checkbox" class="chkSelectAll" name="id"/></th>
            <th style="width:300px">Sender's Information</th>
            <th style="width:400px">product Detail</th>
            <th style="width:230px">Status</th>
        </tr>
        
    <?php 
	if(count($list_car) > 0 ){
		foreach($list_car as $row){ 
	
		$car_image = $row->base_url.'thumb/'.$row->fileimgname;
        if($car_image != ""){
            $car_image_show = $row->base_url.'thumb/'.$row->fileimgname;
		}else{
            $car_image_show = $row->base_url.'thumb/v1435029821/no_image_comzyj';
        }
		$cartitle = $row->car_make ." ". $row->car_model;
		
		// car_mileage
		$car_mileage = "N/A";
		if(($row->car_mileage != 0) and ($row->car_mileage != "")){
          $car_mileage = number_format($row->car_mileage)." km";
        }

        // car_stock_no
		$car_stock_no = "N/A";
		if(($row->car_stock_no != 0) and ($row->car_stock_no != "")){
           $car_stock_no = $row->car_stock_no;
        }

		 // car_chassis_no
		$car_chassis_no = "N/A";
		if($row->car_chassis_no != ""){
          $car_chassis_no =  substr(strrev($row->car_chassis_no) , 0 ,6);
		  $car_chassis_no = strrev($car_chassis_no);
		}
		
		// car_model_year
		$car_model_year = "N/A";
		if(($row->car_model_year != 0) and ($row->car_model_year != "")){
           $car_model_year = $row->car_model_year;
        }
		
		// company_name
		$member_first_name = "N/A";
		if($row->member_first_name != ""){
           $member_first_name = $row->member_first_name;
        }
		
		// date
		$message_update_date = "N/A";
		if($row->last_seen != ""){
		   $message_update_date = date_create($row->last_seen);
           $message_update_date = date_format($message_update_date,"Y/m/d H:i");
        }
		
		// country_name
		$country_name = "N/A";
		if($row->country_name != ""){
           $country_name = $row->country_name;
        }
		
		// country_name
		
			if($row->read == "seen"){
			   $read = "/images/admin/mail_open.jpg";
			}
		else{
			$read = "/images/admin/mail_close.jpg";
		}
		
		if($row->member_country == "jp"){
              $member_country = "<img class='cc' src='/images/admin/icon_japan.png' style='margin-right:5px;'>";
         }elseif($row->member_country == "kr"){ 
              $member_country = "<img class='cc' src='/images/admin/icon_korea.png' style='margin-right:5px;'>";
         }elseif($row->member_country == "kh"){ 
              $member_country = "<img src='/images/admin/icon_flag.png' border='0' style='margin-right:5px;'>";
         }else{ $member_country = "N/A"; }
	?>
    
        <tr>
        	<td style="width:25px; border-right:none; text-align:center" class="check"><input type="checkbox" name="idx[]" id="idx" value="<?php echo $row->car_idx;?>" data-idx="<?php echo $row->car_idx;?>" data-sender="<?php echo $row->sender_no;?>"/></td>
            <td style="width:300px;">
            	<div class="sender_info">
            	<div style="float:left; margin-right:10px;margin-bottom:10px; font-size:12px">
                <img src="<?php echo $read; ?>" style="margin-bottom:10px;"/><br/>(<?php echo $row->total_smg; ?>)</div>
                <div style="float:left; text-align:left"><span style="font-weight:bold;font-size:12px"><?php echo $member_first_name; ?></span><br/>
                <div style="margin-top:15px;font-size:12px"><?php echo $member_country; ?><?php echo $country_name; ?></div>
                </div>
                
                <div style="clear:both"></div>
                <div style="text-align:left;font-size:12px"><?php echo $message_update_date; ?></div>
            	</div>
           </td>
            
            <td style="width:400px;">
            <div class="pro_detail">
            	<div class="car_img"><img src="<?php echo $car_image_show; ?>" /></div>
                <div class="source_car">
                <span style="color:#0772ba;font-size:12px"><?php echo $cartitle; ?></span><br/>
                <div style="margin-bottom:10px;margin-top:8px;font-size:12px"><span style="font-weight:bold;font-size:12px">ChassisNo</span>: <?php echo $car_chassis_no; ?> | <span style="font-weight:bold;font-size:12px">ID</span>: <?php echo $car_stock_no; ?></div>
                <div style="margin-bottom:10px; font-size:12px"><span style="font-weight:bold;font-size:12px">Mileage</span>: <?php echo $car_mileage; ?> | <span style="font-weight:bold;font-size:12px">Year</span>: <?php echo $car_model_year; ?></div>
                <span style="color:red; font-weight:bold;font-size:12px"><?php echo ($row->car_fob_cost !=0 ? $row->car_fob_currency ." ".number_format($row->car_fob_cost) : 'ASK'); ?></span>
                </div>
            </div>
            </td>
            
            <td style="width:230px; border-right:none;">
            <div class="stat">
            
            <div style="font-size:12px"><a id="status" class="negogotiate_read" href="/?c=admin&m=member_message_read&car_idx=<?php echo $row->car_idx; ?>&sender_no=<?php echo $row->sender_no; ?>"><img class="image_on" src="/images/admin/msg_active.png" style="width:54px; height:42px; margin-bottom:5px;" /><img class="image_off" src="/images/admin/msg_hover.png" style="width:54px; height:42px; margin-bottom:5px;" /></a><br/>Negotiation</div>

            </div>
            </td>
            
           <!-- <td style="width:125px; height:auto">
                <div class="perform" style="padding-top: 20px;">
                <p style="margin-bottom:20px;"><a class="msg button blue" href="/?c=admin&m=adm_message_detail">Message</a></p>
                <p><a class="per_invoice button orange" href="/?c=admin&m=adm_proforma_invoice">Performa Invoice</a></p>
                </div>
            </td>-->
            
        </tr>
        
	<?php 
		} 
	}else{
	?>
    <tr><td colspan="4" style="text-align:center; padding:15px; color:#E00408">NO MESSAGE FOUND</td></tr>
	<?php
	}
	?>
    
    </table>

    </form>
	<!--Wrap Message list end-->	
        <div class="page_navi" style="text-align:center; background:#F4F4F4; border:1px solid #D7D7D7; height:20px; margin:0px auto; padding:6px 0px;">
        <?php if(!empty($pagination)){ echo $pagination; }else{ echo "1"; } ?>
        </div>
	</div>
    
    
    
   
