<!-- Content -->
<div class="main_content">
	<div class="title_about_us">
        <img src="/images/public_mobile_site/about_us/details.png" /><p class="bigger">About us</p>
    </div>

    <div class="main_content_about_us">

    	<h4 class="title_content_about_us">Company Information</h4>
    	<p class="text_content_about_us">MotorBB is automobile trading companies whose headquarter is located in Kawasaki, Japan.We are exporting cars from South Korea and Japan to Kenya, Myanmar, Republic of Dominica and Paraguay. Once a customer buys our automobile, it is sent to the customer in the shortest time possible through our smooth and prompt procedures.</p>
		<p>We support customer who wants to buy our cars 24 hours / 365 days. We perform thorough inspection for each automobile, so you never have to worry the quality and condition of the automobile, and will be surely satisfied with your automobile. We never stop making efforts for providing our customers with better services, and keep pursuing higher customer satisfaction!</p>
		
		<div class="main_note_For_certificate">

			<p class="note_For_certificate">See our certificate from Japanese Government:</p> 
			<div class="note_For_download_certificate">
				<img class="icon_download" src="/images/public_mobile_site/about_us/icon_pdf.png" /> 
				<a href="uploads/file/certificate of Motorbb from Japanese government.pdf" target="_blank">Click To Download</a><div class="clear"></div>
			</div><div class="clear"></div>

		</div>

		<div class="company_outline_about_us">
			<h4 class="title_content_about_us">Company Outline</h4>

			<div class="company_outline_about_us_table">
				
				<table class="company_outline_table">
					<tr>
						<td class="company_outline_table_data_left">Company Name: </td>
						<td class="company_outline_table_data_right">Motorbb Co., LTD</td>
					</tr>

					<tr class="row_color">
						<td class="company_outline_table_data_left">Headquarter (Japan): </td>
						<td class="company_outline_table_data_right">Ark Hills Front Tower17F, 2-23-1 Akasaka, Minato-ku, Tokyo, Japan</td>
					</tr>

					<tr>
						<td class="company_outline_table_data_left">Branch (South Korea): </td>
						<td class="company_outline_table_data_right">E-1906B, Songdo Smart Valley, 30 Songdomirae-ro, Yeonsu-gu, Incheon,Korea</td>
					</tr>

					<tr class="row_color">
						<td class="company_outline_table_data_left">Branch (Cambodia): </td>
						<td class="company_outline_table_data_right">#B26, St.1019 Sangkat Toek Thlar, Khann Sensok Phnom Penh, Cambodia</td>
					</tr>

					<tr>
						<td class="company_outline_table_data_left">PhoneNumber (Japan): </td>
						<td class="company_outline_table_data_right"><p>+(81) 80-4095-0379</p></td>
					</tr>

					<tr class="row_color">
						<td class="company_outline_table_data_left">PhoneNumber (South Korea): </td>
						<td class="company_outline_table_data_right"><p>+(82) 32-264-5500</p></td>
					</tr>

					<tr>
						<td class="company_outline_table_data_left">PhoneNumber (Cambodia): </td>
						<td class="company_outline_table_data_right"><p>+(855) 16-4040-32</p></td>
					</tr>

					<tr class="row_color">
						<td class="company_outline_table_data_left">Foundation: </td>
						<td class="company_outline_table_data_right">April/2015</td>
					</tr>

					

					<tr class="row_color">
						<td class="company_outline_table_data_left">Business description: </td>
						<td class="company_outline_table_data_right">Export of automobiles</td>
					</tr>

				</table>

			</div>

		</div>



		<div class="main_content_right_about_us">
			<div class="img_about_us">
				<img src="../images/public_site/aboutus1.jpg">
			</div>

			<div id="img_about_us" class="img_about_us">
				<img src="../images/public_site/aboutus2.jpg">
			</div>

		</div><div class="clear"></div>

    </div>

</div>