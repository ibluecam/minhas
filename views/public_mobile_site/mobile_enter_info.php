<div class="wrapp_mobile_enter_info display_enter_info">
	<div class="mobile_img_enter_info"><img class="mobile_img_enter_info" src="/images/public_mobile_site/enter_info/enter_info_03.jpg"></div>
    <div class="wrapp_mobile_cofirm_email">
    <form action="" autocomplete="off" name="adm_frm" id="adm_frm" method="post">
    	<div class="wrapp_mobile_enter_info_form">
    	<input type="hidden" value="<?php echo $vcode_member;?>"/>
	        <div class="mobile_confirm_left">Email</div>
	        <div class="">
	        	 <?php echo $email_member; ?>   
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left">ID</div>
	        <div class="">
	            <input type="hidden" value="buyer" name="business_type"> 
	            <input type="text" maxlength="20" placeholder="ID" class="input-text member_id input_confirm" value="" name="member_id" id="member_id">           
	            <div class="error_under error_under_register error_member_id"></div>
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left">Password</div>
	        <div class="">
	           	<input type="password" class="input-text password_1 input_confirm" name="member_pwd" value="" placeholder="Password" id="input_password"> 
	            <div class="error_under error_under_register error_password_1"></div>
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left">Confirm Password</div>
	        <div class="">
	           	<input type="password" class="input-text cpassword_1 input_confirm" name="member_password_check" value="" placeholder="Confirm password" id="input_cpassword">
	            <div class="error_under error_under_register error_cpassword_1"></div>
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left">First Name</div>
	        <div class="">
	           	<input type="text" name="member_first_name" value="" id="input_first_name" class="input_confirm member_first_name" placeholder="First Name">
	            <div class="error_under error_under_register error_first_name"></div>
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left">Last Name</div>
	        <div class="">
	           	<input type="text" name="member_last_name" value="" class="input_confirm member_last_name" id="input_last_name" placeholder="Last Name">
	            <div class="error_under error_under_register error_last_name"></div>
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left">Tel</div>
	        <div class="intl_tel_input_next">
	        	<input class="countryid country_confirm" type="hidden" name="values[member_country]" id="seller_member_country" value=""/>
	        	<input type="tel" id="mobile" placeholder="Mobile" class="Field-input-data input-text buyer-mobile-number seller-mobile-number" value="" name="values[mobile_no1]">
	        </div>
	        <div class="error_under error_under_register error_phone"></div>
        </div>

        <div class="wrapp_back_submit">
        <div class="wrapp_submit">
            <button type="submit" name="submit" class="enter_info_submit" style="display:none;" id="submitregister">Next</button>
            <input type="button" value="Next" name="submit" class="enter_info_submit" id="bnt_validate"/>
	        </div>
	    </div>
	</form>
    </div>
</div>



<div class="wrapp_mobile_enter_info display_confirm" style="display:none;">
	<div class="mobile_img_enter_info"><img class="mobile_img_enter_info" src="/images/public_mobile_site/enter_info/confirm.png"></div>
    <div class="wrapp_mobile_cofirm_email">
    <form action="/?c=user&m=mobile_registerupdate" autocomplete="off" name="adm_frm" id="adm_frm" method="post">
    <input type="hidden" name="values[verification]" value="<?php echo $vcode_member;?>"/>
        <input type="hidden" name="values[email]" value="<?php echo $email_member;?>"/>
    	<div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left_one">Email</div>
	        <div class="mobile_confirm_right_style">  
	        <?php  echo $email_member; ?>         
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left_one">ID</div>
	        <div class="mobile_confirm_right_style">
	            <input type="hidden" name="values[business_type]" value="seller"/> 
            	<input type="text" readonly onfocus="this.blur()" id="member_id" name="values[member_id]" value="" class="input-text member_id input_confirm_border_none member_id_confirm" maxlength="20" required> 
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left_one">Password</div>
	        <div class="mobile_confirm_right_style">
	        	
	           	<input readonly onfocus="this.blur()" type="password" id="1" value="" name="values[member_pwd]" class="input-text password_1 input_confirm_border_none password_1_confirm" required>
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left_one">Confirm Password</div>
	        <div class="mobile_confirm_right_style">
	        	  
	           	<input type="password" readonly onfocus="this.blur()" id="1" value="" name="values[member_pwd]" class="input-text cpassword_1 input_confirm_border_none cpassword_1_confirm" required>
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left_one">First Name</div>
	        <div class="mobile_confirm_right_style">
	        	    
	           	<input type="text" readonly onfocus="this.blur()"  class="input_confirm_border_none member_first_name_confirm" id="id" value="" name="values[member_first_name]">
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left_one">Last Name</div>
	        <div class="mobile_confirm_right_style">
	        	
	           	<input type="text" readonly onfocus="this.blur()"  id="id" class="input_confirm_border_none member_last_name_confirm" value="" name="values[member_last_name]">
	        </div>
        </div>

        <div class="wrapp_mobile_enter_info_form">
	        <div class="mobile_confirm_left_one">Tel</div>
	        <div class="mobile_confirm_right_style intl_tel_input_submit">
	        	 <input class="countryid country_confirm" type="hidden" name="values[member_country]" id="seller_member_country" value=""/>
	        	 <div id="icon_flag_enter_info" class="iti-flag kh"></div>
	        	 <input value="" type="tel" onfocus="this.blur()"  class="input_confirm_border_none phone_no1_confirm" name="values[phone_no1]" readonly>
	       		 
       		 </div>

        <div class="wrapp_back_submit">
	        <div class="wrapp_submit input_confirm_border_none">
	        	<input id="submitregister" type="button" class="confirm_back" value="Back"/>  
            	<input type="submit" value="Submit" name="submit" class="enter_info_submit" id="bnt_validate">      	
	        </div>
	    </div>
	</form>
    </div>
</div>