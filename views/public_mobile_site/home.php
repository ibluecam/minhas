<?php 
	//var_dump($premium_cars); 
?>

<div class="main_category_type_brand_country">


	<!-- Start Category By Country -->
	<div class="category_country">
		
		<div class="category_country_top">
			<div class="category_country_top_left"></div>
			<div class="category_country_top_right">
				<p>BY COUNTRY</p> 
			</div>
			<div class="category_icon_top_right">
				<img class="click_slide_up_down_country" src="/images/public_mobile_site/home/select_drop_down.png">
			</div>
			<div class="clear"></div>	
		</div>
		
		<div style="display: none;" class="category_country_content">
			<div class="item_country">
				<a href="/?c=user&amp;m=mobile_car_list&amp;country=jp">
					<div class="item_country_icon">
						<img src="/images/flags/32/JP.png">	
					</div>
					<div class="item_country_title">
						<p>JAPAN <span>(<?php echo $countcarjapan; ?>)</span></p>
					</div><div class="clear"></div>
				</a><div class="clear"></div>
			</div>

			<div class="item_country">
				<a href="/?c=user&amp;m=mobile_car_list&amp;country=kr">
					<div class="item_country_icon">
						<img src="/images/flags/32/KR.png">	
					</div>
					<div class="item_country_title">
						<p>KOREA <span>(<?php echo $countcarkorea; ?>)</span></p>
					</div><div class="clear"></div>
				</a><div class="clear"></div>
			</div>

			<div class="item_country">
				<a href="/?c=user&amp;m=mobile_car_list&amp;country=th">
					<div class="item_country_icon">
						<img src="/images/flags/32/TH.png">	
					</div>
					<div class="item_country_title">
						<p>THAILAND <span>(<?php echo $countcarthailand; ?>)</span></p>
					</div><div class="clear"></div>
				</a><div class="clear"></div>
			</div>

			<div class="item_country">
				<a href="/?c=user&amp;m=mobile_car_list&amp;country=hk">
					<div class="item_country_icon">
						<img src="/images/flags/32/HK.png">	
					</div>
					<div class="item_country_title">
						<p>HONG KONG <span>(<?php echo $countcarhongkong; ?>)</span></p>
					</div><div class="clear"></div>
				</a><div class="clear"></div>
			</div>

		</div>

	</div><!-- End Category By Country -->






	<!-- Start Category By Brand -->
	<div class="type_of_category">

		<div class="type_of_category_top">
			<div class="type_of_category_top_left"></div>
			<div class="type_of_category_top_right">
				<p>BY MAKER</p> 
			</div>
			<div class="category_icon_top_right">
				<img class="click_slide_up_down_brand" src="/images/public_mobile_site/home/select_drop_down.png">
			</div>
			<div class="clear"></div>	
		</div>

		<div style="display: none;" class="content_category content_category_brand">
			<ul class="main_category_list">

			<?php foreach($get_brand as $make){

                  if( ($make->icon_name=='' || $make->icon_name==NULL) &&  $make->c_brand <=0 ){
                    echo "";
                  }else{
            
            ?>
				<li class="category_list ">

					<a href="?c=user&m=mobile_car_list&car_make=<?php echo $make->make_name; ?>&page=1">
						
						<div class="category_icon">
							<img width="40" height="28" src="<?php echo $make->icon_name; ?>"> 
						</div>

						<div class="category_title">
							<?php echo strtoupper($make->make_name);  ?> (<?php echo $make->c_brand; ?>)
						</div><div class="clear"></div>

					</a>

				</li>

			<?php } } ?>


			</ul>
		</div>


	</div><!-- End Category By Brand -->






	<!-- Start Category By Type -->

	<div class="type_of_category">

		<div class="type_of_category_top">
			<div class="type_of_category_top_left"></div>
			<div class="type_of_category_top_right">
				<p>BY TYPE</p> 
			</div>
			<div class="category_icon_top_right">
				<img class="click_slide_up_down_type" src="/images/public_mobile_site/home/select_drop_down.png">
			</div>
			<div class="clear"></div>	
		</div>

		<div style="display: none;" class="content_category content_category_type">
			<ul class="main_category_list">

			<?php foreach($getbody_type as $getbodytype){

                  if( ($getbodytype->icon=='' || $getbodytype->icon==NULL) &&  $getbodytype->countbodytype <=0 ){
                    echo "";
                  }else{
            
            ?>
				<li class="category_list">

					<a href="?c=user&m=mobile_car_list&car_body_type=<?php echo $getbodytype->body_name; ?>&page=1">
						
						<div class="category_icon">
							<img width="50" height="21" border="0" src="/images/user/compo/bt/<?php echo $getbodytype->icon; ?>.png"> 
						</div>

						<div class="category_title">
							<?php echo strtoupper($getbodytype->body_title);  ?> (<?php echo $getbodytype->countbodytype; ?>)
						</div><div class="clear"></div>

					</a>

				</li>

			<?php } } ?>

			

			</ul>
		</div>


	</div><!-- End Category By Type -->




</div>


<!-- Content -->

<div class="main_content">

	<div class="content_popular">

		<div class="popular_title">
			
			<div class="title">
				<h5>MOST POPULAR CARS</h5>
			</div>

			<div class="icon_see_more">
				<a href="/?c=user&m=mobile_car_list&car=popular"><img src="/images/public_mobile_site/home/more_car.png"></a>
			</div><div class="clear"></div>

		</div>

		<div class="block_content">

			<?php 
				foreach($premium_cars as $row){ 
					$img_row = $row->base_url . "thumb/".$row->fileimgname;
					
					$carmodelyear = "";
					$cartitle = "";
					
					if($row->car_model_year != 0){
						$carmodelyear = $row->car_model_year;
					}

					$cartitle = $carmodelyear ." ".$row->car_make ." ". $row->car_model;


					// car_chassis_no
					$car_chassis_no = "N/A";

					if($row->car_chassis_no != ""){
          
                      $car_chassis_no =  substr(strrev($row->car_chassis_no) , 0 ,6);

                      $car_chassis_no = strrev($car_chassis_no);
                    }


                    // Car_Mileage
                    $car_mileage = "N/A";

					if(($row->car_mileage != 0) and ($row->car_mileage != "")){
          
                      $car_mileage = number_format($row->car_mileage)." km";
                    }


                    // car_transmission
					$car_transmission = "N/A";

					if($row->car_transmission != ""){
          
                      $car_transmission = $row->car_transmission;

                    }


                    // car_steering
					$car_steering = "N/A";

                    if($row->car_steering == "LHD"){ 
                    	$steering = "Left hand";
                    }else{ 
                    	$steering = "Right hand";
                    }

			?>

			<div class="list_item">
				
				<div class="image_item">
					<a rel="canonical" href="?c=user&m=mobile_cardetail&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>">
						<img src="<?php echo ($row->fileimgname !='' ? $img_row : "/images/user/no_image.png"); ?> ">	
						<?php if($row->icon_status=="soldout"){
			                   echo' <img class="sold_out" src="/images/user/sold-out.png"/>';
			                }
			               if($row->icon_status=="reserved"){
			                   echo' <img class="sold_out" src="/images/user/reserved.png"/>';
			                }
			               ?>
					</a>
				</div>

				<div class="info_item">
					<h5><a rel="canonical" href="?c=user&m=mobile_cardetail&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>"><?php echo $cartitle; ?></a></h5>
					
					<div class="wrap_item">
						<div><div class="text_left"><span>Chassis No : </span><?php echo $car_chassis_no; ?> |</div> <div class="text_right"><span>Mileage : </span><?php echo $car_mileage; ?></div></div><div class="clear"></div>
						<div><div class="text_left"><span>Transmission : </span><?php echo $car_transmission; ?> |</div> <div class="text_right"><span>Steering : </span><?php echo $car_steering; ?></div></div>
					</div><div class="clear"></div>

					<p class="fob_price">FOB <span class="fob_price_number"><?php echo ($row->car_fob_cost !=0 ? "US $".number_format($row->car_fob_cost) : 'ASK'); ?></span></p>
				</div><div class="clear"></div>

			</div>

			<?php } ?>


		</div>

	</div>

	<div class="content_new_update">
		
		<div class="new_update_title">
			
			<div class="title">
				<h5>NEW & UPDATED</h5>
			</div>

			<div class="icon_see_more">
				<a href="/?c=user&m=mobile_car_list"><img src="/images/public_mobile_site/home/more_car.png"></a>
			</div><div class="clear"></div>

		</div>

		<div class="block_content">


			<?php 
				foreach($get_cars as $row){ 
					$img_row = $row->base_url . "thumb/".$row->fileimgname;
					
					$carmodelyear = "";
					$cartitle = "";
					
					if($row->car_model_year != 0){
						$carmodelyear = $row->car_model_year;
					}

					$cartitle = $carmodelyear ." ".$row->car_make ." ". $row->car_model;


					// car_chassis_no
					$car_chassis_no = "N/A";

					if($row->car_chassis_no != ""){
          
                      $car_chassis_no =  substr(strrev($row->car_chassis_no) , 0 ,6);

                      $car_chassis_no = strrev($car_chassis_no);
                    }


                    // Car_Mileage
                    $car_mileage = "N/A";

					if(($row->car_mileage != 0) and ($row->car_mileage != "")){
          
                      $car_mileage = number_format($row->car_mileage)." km";
                    }


                    // car_transmission
					$car_transmission = "N/A";

					if($row->car_transmission != ""){
          
                      $car_transmission = $row->car_transmission;

                    }


                    // car_steering
					$car_steering = "N/A";

                    if($row->car_steering == "LHD"){ 
                    	$steering = "Left hand";
                    }else{ 
                    	$steering = "Right hand";
                    }

			?>

			<div class="list_item">
				
				<div class="image_item">
					<a rel="canonical" href="?c=user&m=mobile_cardetail&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>">
						<img src="<?php echo ($row->fileimgname !='' ? $img_row : "/images/user/no_image.png"); ?> ">	
						<?php if($row->icon_status=="soldout"){
			                   echo' <img class="sold_out" src="/images/user/sold-out.png"/>';
			                }
			               if($row->icon_status=="reserved"){
			                   echo' <img class="sold_out" src="/images/user/reserved.png"/>';
			                }
			               ?>
					</a>
				</div>

				<div class="info_item">
					<h5><a rel="canonical" href="?c=user&m=mobile_cardetail&idx=<?php echo $row->idx; ?>" id="<?php echo $row->idx; ?>"><?php echo $cartitle; ?></a></h5>
					
					<div class="wrap_item">
						<div><div class="text_left"><span>Chassis No : </span><?php echo $car_chassis_no; ?> |</div> <div class="text_right"><span>Mileage : </span><?php echo $car_mileage; ?></div></div><div class="clear"></div>
						<div><div class="text_left"><span>Transmission : </span><?php echo $car_transmission; ?> |</div> <div class="text_right"><span>Steering : </span><?php echo $car_steering; ?></div></div>
					</div><div class="clear"></div>

					<p class="fob_price">FOB <span class="fob_price_number"><?php echo ($row->car_fob_cost !=0 ? "US $".number_format($row->car_fob_cost) : 'ASK'); ?></span></p>
				</div><div class="clear"></div>

			</div>

			<?php } ?>


			<!-- Keep This is default <div class="list_item">
				
				<div class="image_item">
					<img src="https://res.cloudinary.com/softbloom/image/upload/thumb/tasb4lnt7mwflndy3xzc">	
				</div>

				<div class="info_item">
					<h5>2013 HYUNDAI SONATA</h5>

					<div class="wrap_item">
						<div><div class="text_left"><span>Chassis No : </span>013381 |</div> <div class="text_right"><span>Mileage : </span>106,845 km</div></div><div class="clear"></div>
						<div><div class="text_left"><span>Transmission : </span>Auto |</div> <div class="text_right"><span>Steering : </span>right</div></div>
					</div><div class="clear"></div>

					<p class="fob_price">FOB <span class="fob_price_number">US $100,00</span></p>
				</div><div class="clear"></div>

			</div>-->


			


			

		</div>

	</div>

</div>