
<div class="main_content">
	<div id="wrapper_seller">
		<div class="main_category_type_brand_country">
			<div class="type_of_category">
				<div class="type_of_category_top">
					<div class="type_of_category_top_left"></div>
					<div class="type_of_category_top_right">
                    <?php
						foreach( $sellerdetail as $rows){
					?>
						<p><?php echo ($rows->company_name !="" ? $rows->company_name : "N/A"); ?></p>
                    <?php } ?>
                     
					</div>
					<div class="clear"></div>	
				</div>
			</div>
		</div>
	</div>
	<!-- Start blog image seller -->
	 <div class="wrapper_image_seller">
     <?php
				foreach ($member_profile_detail as $img_rows){
					$img_row = $img_rows->base_url."c_fill,h_768,w_1024/".$img_rows->image;
					if($img_rows->image != ""){
            ?>
	 	<img src="<?php echo $img_row; ?>" width="100%" />
        <?php }else{ ?>
         <img src="https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_big_d2odu9" width="100%" />
			<?php
                  }
				}
        ?>
	 </div>
	<!-- end blog image seller -->
	<div id="wrapper_seller">
		<div class="main_category_type_brand_country">
			<div class="type_of_category">
				<div class="type_of_category_top">
					<div class="type_of_category_top_left"></div>
					<div class="type_of_category_top_right">
						<p>Information</p> 
					</div>
					<div class="clear"></div>	
				</div>
			</div>
		</div>
	</div>
	<div class="wrapper_decribtion_info">
    <?php
             foreach( $sellerdetail as $rows){
    ?>
		<div class="spec">
			<div class="textLeft smaller_bold">Country</div>
			<div class="textRight smaller"><?= $rows->country; ?></div>
		</div>
		<div class="spec">
			<div class="textLeft blue smaller_bold">Contact Person</div>
			<div class="textRight blue smaller"><?php echo ($rows->contact_person_name !="" ? $rows->contact_person_name : "N/A"); ?></div>
		</div>
		<div class="spec">
			<div class="textLeft smaller_bold">Address</div>
			<div class="textRight address"><?= $rows->address; ?></div>
		</div>
		<div class="clear"></div>	
		<div class="introduction_seller">
			<span>Introduction</span>
			<p><?= $rows->user_memo; ?></p>
		</div>
        <?php } ?>
	</div>






	<div class="content_seller_car">
    
		<div class="new_update_title">
			<div class="title">
            <?php
				foreach( $sellerdetail as $rows){
			?>
				<h5>Cars from <?php echo ($rows->company_name !="" ? $rows->company_name : "N/A"); ?></h5>
            <?php
				}
			?>
			</div>
		</div>

		<div class="block_content">
        
        <?php
  					foreach( $carlist as $rows){
					$img_row = $rows->base_url."thumb/".$rows->fileimgname;			
					$img_rowb = $rows->base_url."c_fill,h_500,w_700/".$rows->fileimgname;
		?>
			<div class="list_item">
				<div class="image_item">
                <a href="/?c=user&m=mobile_cardetail&idx=<?php echo $rows->idx; ?>">
                <?php if ($rows->fileimgname != ''){ ?>
                    <img src="<?php echo $img_row; ?>">
                <?php }else{ ?>
                
                <img alt="<?php echo $rows->car_make ." ". $rows->car_model." ". $rows->car_model_year;; ?>" title="<?php echo $rows->car_make ." ". $rows->car_model." ". $rows->car_model_year;; ?>" src="https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj"/>
                
               <?php } ?>
               <?php if($rows->icon_status=="soldout"){
                              echo' <img class="sold_out" src="/images/user/sold-out.png"/>';
                          }
                          if($rows->icon_status=="reserved"){
                              echo' <img class="sold_out" src="/images/user/reserved.png"/>';
                          }
                    ?>
                </a>	  
				</div>

				<div class="info_item">
                <?php
						$carmakestr2 = $rows->car_model_year ." ".$rows->car_make ." ". $rows->car_model;
						if($rows->car_chassis_no != ""){
          
						  $car_chassis_no =  substr(strrev($rows->car_chassis_no) , 0 ,6);
	
						  $car_chassis_no = strrev($car_chassis_no);
						}
                ?>
					<a class="title" href="/?c=user&m=mobile_cardetail&idx=<?php echo $rows->idx; ?>"><?php echo $carmakestr2; ?></a>

					<div class="wrap_item">
						<div><div class="text_left"><span>Chassis No : </span><?php echo ($car_chassis_no !='' || $car_chassis_no > 0 ? $car_chassis_no : 'N/A'); ?> |</div> <div class="text_right"><span>Mileage : </span><?php echo ($rows->car_mileage !='' && $rows->car_mileage > 0 ? number_format($rows->car_mileage) ." km" : 'N/A'); ?></div></div><div class="clear"></div>
						<div><div class="text_left"><span>Transmission : </span><?php echo ($rows->car_transmission !='' || $rows->car_transmission > 0 ? $rows->car_transmission : 'N/A'); ?> |</div> <div class="text_right"><span>Steering : </span><?php
                                       if($rows->car_chassis_no == "LHD"){ $steering = "Left";}else{ $steering = "Right";}
                                        echo ($steering !='' || $steering > 0 ? $steering : 'N/A'); ?></div></div>
					</div><div class="clear"></div>

					<p class="fob_price">FOB <span class="fob_price_number"><?php echo ($rows->car_fob_cost !=0 ? $rows->car_fob_currency ." ".number_format($rows->car_fob_cost) : 'ASK'); ?></span></p>
				</div><div class="clear"></div>
			</div>
            <?php } ?>
            
		</div>
        
        
</div>
		

		
<?php

if($countcarlist > $perpagepagination){
      $totalpage = ceil($countcarlist / $perpagepagination);

    ?>

<div class="brandId">
      <input type="hidden" id="txttype" value="<?php echo $carlist[0]->car_make; ?>">
</div>
<div class="paginater-site">
        <ul class="pagination" role="menubar" aria-label="Pagination">
        <?php if($getnumpage > 1){ ?>
          <li><a class="clickpagination" id="1">&laquo;</a></li>
          <li class="arrow <?php if($getnumpage == 1){ ?> unavailable <?php } ?> " <?php if($getnumpage == 1){ ?> aria-disabled="true" <?php } ?> ><a class="clickpagination" id="<?php if($getnumpage == 1){ $numpage = $getnumpage; }else{ $numpage = $getnumpage - 1; } echo $numpage; ?>">Previous</a></li>
         <?php } ?>
          <?php

              if(($getnumpage-2) <= 0){
                  $currentpagenumber = 1;
              }else{
                  $currentpagenumber = $getnumpage-2;
              }

              if(($getnumpage+2) > $totalpage){
                  $totalcurrent = $totalpage;
              }else{
                  $totalcurrent = $getnumpage+2;
              }

          ?>

          <?php for($i=$currentpagenumber; $i<=$totalcurrent; $i++){  ?>
               <li<?php if($i == $getnumpage){ ?> class="current" <?php } ?>><a class="clickpagination valuepagination_<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i;?></a></li>
          <?php } ?>
          <?php if($getnumpage != $totalpage){ ?>
          <li class="arrow <?php if($getnumpage >= $totalpage){ ?> unavailable <?php } ?> " <?php if($getnumpage >= $totalpage){ ?> aria-disabled="true" <?php } ?> ><a class="clickpagination" id="<?php if($getnumpage >= $totalpage){ $numpage = $getnumpage; }else{ $numpage = $getnumpage + 1; } echo $numpage; ?>">Next</a></li>
          <li><a class="clickpagination" id="<?php echo $totalpage; ?>">&raquo;</a></li>
          <?php } ?>
       </ul>
</div>
<?php } ?>

		







