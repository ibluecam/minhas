<!-- Content -->
<div class="main_content">
	
	<div class="title_content_how_to_buy">
		<p>Importing from foreign countries is not as difficult as you think!</p>
		<p>How do you receive your vehicle?</p>
		<p class="step_of_buy">5 Easy Steps to Buy Japanese and Korean Used Car</p>
	</div>


	<div class="desc_content_how_to_buy">

		<div class="row_content">
			<div class="title_step_content">
				<p><span>STEP 1</span>: ORDER</p>
			</div>

			<div class="step_content">

				<div class="step_content_image">
					<img src="/images/public_mobile_site/howtobuy/step1.jpg">
				</div>

				<div class="step_content_text">
					<p>&rarr; <span>Choose</span> the car which you want to buy. Our stock search engine will help you search through our stocks.</p>
					<p>&rarr; <span>Receive</span> the invoice from our company and check the price.</p>
					<p>&rarr; If it is the <span>first time</span> for you to buy a car by online, you should find a <span>Clearing Agent.</span></p>
					<p class="last_desc">It will help you a lot.</p>
				</div><div class="clear"></div>

			</div><div class="clear"></div>
		
		</div>


		<div class="row_content">
			<div class="title_step_content">
				<p><span>STEP 2</span>: Payment</p>
			</div>

			<div class="step_content">

				<div class="step_content_image">
					<img src="/images/public_mobile_site/howtobuy/step2.jpg">
				</div>

				<div class="step_content_text">
					
					<p>
						Go to your  Bank(*1)  to make <span class="add_line">payment</span>(*2)    by <span class="add_color">Telegraphic transfer</span>. And send a copy of bank 
		receipt  to MotorBB  by E-mail.
					</p>

					<div class="bank_details">
						<p>(*1) Bank DETAILS</p>
						<p>Take the Invoice to your local bank and ask a banker to arrange the payment by "telegraphic transfer". 
	You can basically use any of the banks who accept the foreign currency transfer.</p>
					</div>

					<div class="banking_acc_detail">

						<div class="text_acc_detail">
							<p>(*2) Make payment"Invoice Price" at the Bank. Make payment 
	"Additional Price" at the Port.</p>
						</div>

						<div class="img_acc_detail">
							<img src="/images/public_mobile_site/howtobuy/bank-acc.jpg">
						</div><div class="clear"></div>

					</div>

				</div><div class="clear"></div>

			</div><div class="clear"></div>
		
		</div>




		<div class="main_address_bank_account">
			<p class="title_address_bank_account">Bank Account</p>
			<p class="name_address_bank_account">Company Name  :  MOTORBB CO., LTD</p>

			<div class="address_content_bank_account">

				<div class="main_content_bank_account_text">
					<table class="table_bank_account">
						<tr class="tr_bank_account">
							<td class="td1_bank_account">ACCOUNT NUMBER</td>
							<td class="td2_bank_account">0194171</td>
						</tr>
						<tr class="tr_bank_account">
							<td class="td1_bank_account">BANK NAME</td>
							<td class="td2_bank_account">The Bank of Tokyo-Mitsubishi UFJ,Ltd; 005</td>
						</tr>
						<tr class="tr_bank_account">
							<td class="td1_bank_account">BRANCH</td>
							<td class="td2_bank_account">YOKOHAMA Ekimae Branch; 251</td>
						</tr>
						<tr class="tr_bank_account">
							<td class="td1_bank_account">SWIFT CODE</td>
							<td class="td2_bank_account">BOTKJPJT</td>
						</tr>
						<tr class="tr_bank_account">
							<td class="td1_bank_account">BANK ADDRESS</td>
							<td class="td2_bank_account">220-0004,1-11-20,Kitasaiwai, Nishi-Ku,Yokohama,Japan</td>
						</tr>
					</table>
				</div>

				<div class="right_content_bank_account">
					<img src="/images/public_mobile_site/howtobuy/MUFG.png">
				</div><div class="clear"></div>

			</div>


			<div class="mian_shipping_how_to_buy">
					
				<div class="img_shipping_how_to_buy">
					<img src="../images/public_site/howtobuy/step2-img2.jpg">
				</div>

				<div class="desc_shipping_how_to_buy">
					<p>(*3) Ask the Clearing Agent how much does it cost to pick up the car from the port.</p>
				</div>

			</div>

		</div>


		<div id="step3" class="row_content">
			<div class="title_step_content">
				<p><span>STEP 3</span>: Shipment</p>
			</div>

			<div class="step_content">

				<div class="step_content_image">
					<img src="/images/public_mobile_site/howtobuy/step3.jpg">
				</div>

				<div class="step_content_text">
					<p>Wait for the &ldquo;B/L copy&ldquo; (*3) by E-mail and &ldquo;B/L original&ldquo;(*4) by DHL.</p>
					<p>(*3) B/L  (Bill of Landing) Copy is certificate to show that your car was perfectly shipped out.</p>
					<p>Within 1-2 days after shipping out from Japan, we will send you &ldquo;Notice for Shipment&ldquo; Including </p>
					<p><span class="bill_of_landing">Bill of Landing Copy(PDF)</span> by E-mail.</p>
					
					<div class="img_bill_of_landing">
						<img src="/images/public_mobile_site/howtobuy/pdf.png">
					</div>

					<div class="text_bill_of_landing">
						<p>(*4) It is needed at the port of your country in order to receive your vehicle from the port.</p>
					</div>

					<div class="img_shipping_of_landing">
						<img src="/images/public_mobile_site/howtobuy/shipinglanding.png">
					</div>

				</div><div class="clear"></div>

			</div><div class="clear"></div>
		
		</div>


		<div id="step4" class="row_content">
			<div class="title_step_content">
				<p><span>STEP 4</span>: Clearing</p>
			</div>

			<div class="step_content">

				<div class="step_content_image">
					<img src="/images/public_mobile_site/howtobuy/step4.jpg">
				</div>

				<div class="step_content_text">
					<p>Go to  your local agent and pass the B/L original.</p>

				</div><div class="clear"></div>
				<div class="img_clearing">
						<img src="/images/public_mobile_site/howtobuy/step4-img1.png">
				</div>
			</div><div class="clear"></div>
		
		</div>


		<div id="step5" class="row_content">
			<div class="title_step_content">
				<p><span>STEP 5</span>: Recieving</p>
			</div>

			<div class="step_content">

				<div class="step_content_image">
					<img src="/images/public_mobile_site/howtobuy/step5.jpg">
				</div>

				<div class="step_content_text">
					<p>Pick up your car at your port. Clearing Agent will help you to receive it.</p>
					<p>And if you register  your car in your local authority, you can drive your car from that day.</p>

				</div><div class="clear"></div>
				
			</div><div class="clear"></div>
		
		</div>




	</div>

</div>