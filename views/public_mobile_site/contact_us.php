<!-- Content -->
<div class="main_content">

    <div class="team_japan_korea_export">

	   	<div class="title_about_us">
	        <img src="/images/public_mobile_site/about_us/details.png" /><p class="bigger">The Team for Japanese Export</p>
	    </div>

	    <?php
              if(count($slelect_team_sale_list>0)){
              	foreach ($slelect_team_sale_list as $row_jp) {
            ?>

	    <div class="main_seller_team">
	    	
	    	<div class="item_seller_team">

				<div class="item_images_seller_team">
					<img src="<?php echo $row_jp->secure_url; ?>">
				</div>

				<div class="item_info_seller_team">
					<h4 class="name"><?php echo $row_jp->member_first_name; ?></h4>
					<p class="phone"><?php echo "+".$row_jp->phone_no1.'-'.$row_jp->phone_no2; ?></p>
					<p class="email"><a href="mailto:<?= $row_jp->email ?>"><?php echo $row_jp->email; ?></a></p>
					<p class="media">
						<?php 
							if(!empty($row_jp->skype_id)){
								echo '<a class="tooltip" title="Skype ID: &lt;u&gt;'.$row_jp->skype_id.'&lt;/u&gt;" href="skype:'.$row_jp->skype_id.'?add"><img src="images/user/skype.png"></a>';
							} 
							if(!empty($row_jp->facebook_id)){
								echo '<a class="tooltip" title="Facebook: &lt;u&gt;'.$row_jp->facebook_id.'&lt;/u&gt;" target="_blank" href="'.$row_jp->facebook_id.'"><img src="images/user/facebook.png"></a> ';
							}
							if(!empty($row_jp->viber_id)){
								echo '<a class="tooltip" title="Viber: &lt;u&gt;'.$row_jp->viber_id.'&lt;/u&gt;" href="#"><img src="images/user/1.png"></a>';
							}
							if(!empty($row_jp->whatsapp_id)){
								echo '<a class="tooltip" title="Viber: &lt;u&gt;'.$row_jp->whatsapp_id.'&lt;/u&gt;" href="#"><img src="images/user/4(1).png"></a>';
							}
						?>
					</p>
				</div><div class="clear"></div>

			</div><div class="clear"></div>

	    </div>

	    		<?php 
           		}
        	}
        ?>

    </div>



   	<div id="team_korea_export" class="team_japan_korea_export">

	   	<div class="title_about_us">
	        <img src="/images/public_mobile_site/about_us/details.png" /><p class="bigger">The Team for Korean Export</p>
	    </div>

	    <?php
		if(count($slelect_team_sale_list_kr>0)){
				  	
			foreach ($slelect_team_sale_list_kr as $row_kr) {	 
		?>

	    <div class="main_seller_team">
	    	
	    	<div class="item_seller_team">

				<div class="item_images_seller_team">
					<img src="<?php echo $row_kr->secure_url; ?>">
				</div>

				<div class="item_info_seller_team">
					<h4 class="name"><?php echo $row_kr->member_first_name; ?></h4>
					<p class="phone"><?php echo "(+".$row_kr->phone_no1.')'.$row_kr->phone_no2; ?></p>
					<p class="email"><a href="mailto:<?= $row_kr->email ?>"><?php echo $row_kr->email; ?></a></p>
					<p class="media">
						
						<?php 
							if(!empty($row_kr->skype_id)){
								echo '<a class="tooltip" title="Skype ID: &lt;u&gt;'.$row_kr->skype_id.'&lt;/u&gt;" href="skype:'.$row_kr->skype_id.'?add"><img src="images/user/skype.png"></a>';
							} 
							if(!empty($row_kr->facebook_id)){
								echo '<a class="tooltip" title="Facebook: &lt;u&gt;'.$row_kr->facebook_id.'&lt;/u&gt;" target="_blank" href="'.$row_kr->facebook_id.'"><img src="images/user/facebook.png"></a> ';
							}
							if(!empty($row_kr->viber_id)){
								echo '<a class="tooltip" title="Viber: &lt;u&gt;'.$row_kr->viber_id.'&lt;/u&gt;" href="#"><img src="images/user/1.png"></a>';
							}
							if(!empty($row_kr->whatsapp_id)){
								echo '<a class="tooltip" title="Viber: &lt;u&gt;'.$row_kr->whatsapp_id.'&lt;/u&gt;" href="#"><img src="images/user/4(1).png"></a>';
							}
						?>

				</div><div class="clear"></div>

			</div><div class="clear"></div>

	    </div>

		    	<?php 
				}
			}
		?>

    </div>

</div>