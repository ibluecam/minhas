<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <title>Motorbb Mobile</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="/css/public_mobile_site/global.css">
	   <link rel="stylesheet" href="/css/public_site/lightbox.css?v=1.0.7" />
    <?php 
      if(isset($arr_css)){
          foreach($arr_css as $arr_css){
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $arr_css; ?>">
    <?php } } ?>

 </head>

  <body>

		<?php 
			include('header.php'); 
            include('content.php');
			include('footer.php'); 
		?>
		
  
  
    <script src="/js/public_mobile_site/jquery-1.11.3.min.js?v=1.0.7"></script>
    <script type="text/javascript" src="/js/public_mobile_site/jquery.chained.min.js"></script>
    <script type="text/javascript" src="/js/public_mobile_site/global.js"></script>

    <?php 
      if(isset($arr_js)){
          foreach($arr_js as $arr_js){
    ?>
    <script type="text/javascript" src="<?php echo $arr_js; ?>"></script>
    <?php } } ?>
	
  </body>

</html>