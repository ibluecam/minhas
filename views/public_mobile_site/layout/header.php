
      <header class="mainHeader">
      
       	<!--link to member -->
         <?php
              if($is_login){
         ?>
        <div class="wrap_top_head_member">
        <div class="member_left">Hello!
        <?php if($session_member_id != ''){ ?>
        	<span style="color:#368cc5"><?php echo $session_member_id; ?></span></div>
        <?php } ?>
        <div class="member_right">
          <div class="logged_in"><img src="/images/public_mobile_site/logged_out.png" /><a href="/?c=user&amp;m=mobile_logout_exec">Log out</a></div>
        
          <?php if($_SESSION['ADMIN']['business_type']=="seller") { ?>
            
            <div class="logged_out"><img src="/images/public_mobile_site/logged.png" /><a href="/?c=admin&m=mobile_list_car">Member site</a></div>
          
          <?php } elseif($_SESSION['ADMIN']['business_type']=="buyer") {?>
            
            <div class="logged_out"><img src="/images/public_mobile_site/logged.png" /><a href="/?c=admin&m=mobile_buyer_list_car">Member site</a></div>

          <?php } else {?>
            
            <div class="logged_out"><img src="/images/public_mobile_site/logged.png" /><a href="/?c=admin">Member site</a></div>
          
          <?php } ?>
        
        </div>
        
        </div>
        <?php } ?>
        <!--link to member -->
        
        <div class="top_head_logo_menu">    
          
          <div class="logo">
            <a href="/?c=user&amp;m=mobile_home"><img src="/images/public_mobile_site/m_logo.png"></a>
          </div>
          
          <div class="nav">
            <img class="click_slide_up" src="/images/public_mobile_site/m_nav_icon.png">
          </div><div class="clear"></div>

        </div><div class="clear"></div>

        <div class="bg_bottom">
          <div class="search_left"></div>

            
          <div class="search_center"></div>
            
          <div class="search_right"></div>
        </div>  

       
        <div style="display: none;" class="content_hide_show nav_content">
            <ul>
              <li <?php if($action_menu == "mobile_home"){ ?>class="active"<?php } ?>><a href="/?c=user&amp;m=mobile_home">Home</a></li>
              <li <?php if($action_menu == "mobile_car_list"){ ?>class="active"<?php } ?>><a href="/?c=user&amp;m=mobile_car_list">Cars</a></li>
              <li <?php if($action_menu == "mobile_seller"){ ?>class="active"<?php } ?>><a href="/?c=user&amp;m=mobile_seller">Sellers</a></li>
              <li <?php if($action_menu == "mobile_how_to_buy"){ ?>class="active"<?php } ?>><a href="/?c=user&amp;m=mobile_how_to_buy">How to buy</a></li>
              <li <?php if($action_menu == "mobile_about_us"){ ?>class="active"<?php } ?>><a href="/?c=user&amp;m=mobile_about_us">About us</a></li>
              <li <?php if($action_menu == "mobile_contact_us"){ ?>class="active"<?php } ?>><a href="/?c=user&amp;m=mobile_contact_us">Contact us</a></li>
        <?php
          if(!$is_login){
        ?>
              <li <?php if($action_menu == "mobile_login"){ ?>class="active"<?php } ?>><a href="/?c=user&amp;m=mobile_login">Log In</a></li>
        <?php 
          }    
        ?>
        
            </ul>
        </div>
       

       

       




        <?php if(isset($show_search_box)){ ?>
        <div class="top_head_search">

          <div class="search_left"></div>

            
          <div class="search_center"></div>
            
          <div class="search_right"></div>

          <div class="search_box_tool">
            <form>
              <input class="searchbox keyword" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword']; } ?>" type="text" placeholder="Keyword">
              <input class="button_submit" id="button_search" type="button" value="">
              <img id="icon_slide_down_searchbox" class="icon_slide_down_searchbox" src="images/public_mobile_site/down_search_box.png">
              <div class="clear"></div>
            </form>
          </div>

          <div class="clear"></div>


          <div class="content_search_box" style="display:none;">
              <div class="search_box_left"></div>
            
              <div class="search_box_center"></div>
                
              <div class="search_box_right"></div><div class="clear"></div>


              <div class="search_box_tool_content">

               
                <div class="title_search">
                    <img class="icon_search" src="images/public_mobile_site/icon_search.png"><h5>SEARCH CARS</h5><div class="clear"></div>

                </div> 

                <div class="content_form">

                  <form>
                    
                    <select id="make_car" class="field_select make">
                        <option value="">Make</option>
                        <?php if(count($carmaketodropdownsearch) > 0){ foreach($carmaketodropdownsearch as $carmaketodropdownsearchs){ ?>
                          <option value="<?php echo $carmaketodropdownsearchs->car_make; ?>"<?php if(isset($_GET['car_make'])){ if($_GET['car_make']==$carmaketodropdownsearchs->car_make){ echo "selected";}}?> >
                          <?php echo $carmaketodropdownsearchs->car_make; ?></option>
                        <?php }} ?>
                    </select>

                    <select id="series_car" class="field_select model">
                        <option value="">Model</option>
                        <?php foreach($carmodeltodropdownsearch as $carmodeltodropdownsearchs){ ?>
                          <option class="<?php echo $carmodeltodropdownsearchs->car_make; ?>" value="<?php echo $carmodeltodropdownsearchs->car_model; ?>"<?php if(isset($_GET['car_model'])){ if($_GET['car_model']==$carmodeltodropdownsearchs->car_model){ echo "selected";}}?> >
                          <?php echo $carmodeltodropdownsearchs->car_model; ?></option>
                        <?php } ?>
                    </select>
                  
                    <div class="row_field">
                        <div class="column_field">
                            <select class="field_select_year country">
                                <option value="">Country</option>
                                
                                 <?php foreach($carcountrytodropdownsearch as $carcountrytodropdownsearchs){ ?>
                                  <option <?php if(isset($_GET['country'])){ if($_GET['country']==$carcountrytodropdownsearchs->cc){ ?> Selected <?php } } ?> value="<?php echo $carcountrytodropdownsearchs->cc; ?>"><?php echo $carcountrytodropdownsearchs->com; ?></option>
                                <?php }  ?>

                            </select>
                        </div>
                        <div class="column_field">
                            <select class="field_select_year search_drive_type">

                               <option value="">Drive Type</option>
                                <?php foreach(Array('2WD', '4WD') as $Val ) { ?>
                                  <option <?php if(isset($_GET['car_drive_type'])){ if($_GET['car_drive_type']==$Val){ ?> Selected <?php } } ?> value="<?= $Val ?>"><?= $Val ?></option>
                                <?php } ?>

                            </select>
                        </div><div class="clear"></div>
                    </div>


                    <div class="row_field">
                        <div class="column_field">
                            <select class="field_select_year min_year">
                                <option value="">Min Year</option>
                                
                                <?php for($i=date("Y"); $i>=1990; $i--){ ?>
                                  <option <?php if(isset($_GET['min_model_year'])){ if($_GET['min_model_year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }  ?>

                            </select>
                        </div>
                        <div class="column_field">
                            <select class="field_select_year max_year">
                                <option value="">Max Year</option>

                                <?php for($i=date("Y"); $i>=1990; $i--){ ?>
                                  <option <?php if(isset($_GET['max_model_year'])){ if($_GET['max_model_year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>

                            </select>
                        </div><div class="clear"></div>
                    </div>

                    <div class="row_field">
                        <div class="column_field_price">
                           <input type="text" class="field_select_price min_price" placeholder="Min Price">
                        </div>
                        <div class="column_field_price">
                            <input type="text" class="field_select_price max_price" placeholder="Min Price">
                        </div><div class="clear"></div>
                    </div>
                  
                    
                  
                    <div class="row_field" id="row_field_button">

                        <div class="field_total_product">
                            <p>Total product : 000</p>
                        </div>

                        <div class="field_btn_click">
                            <input class="button_reset" id="button_reset" type="button" value="Reset">
                            <input class="button_submit" id="button_search" type="button" value="Submit">
                        </div><div class="clear"></div>

                    </div>
                   
                  </form>

                </div><div class="clear"></div>

              </div>

          </div>


        </div><!-- End top_head_search -->
        <?php } ?>


      </header><!-- End mainHeader -->

