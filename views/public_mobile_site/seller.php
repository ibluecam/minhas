
<div class="main_content">
	<div id="wrapper_seller">
		<div class="main_category_type_brand_country">
			<div class="type_of_category">
				<div class="type_of_category_top">
					<div class="type_of_category_top_left"></div>
					<div class="type_of_category_top_right">
						<p>SELLER</p> 
					</div>
					<div class="clear"></div>	
				</div>
			</div><!-- End Category By Type -->
		</div>
		<!-- search -->
		<div class="search_blog">
			<form action="?c=user&m=mobile_seller" method="post">
				<select name="country">
					<option value="">Country</option>
		             <?php foreach($country as $rows){ ?>
		              <option name="country"  value="<?php echo $rows->member_country ;?>" <?php if(isset($_GET['country'])){ if($_GET['country'] == $rows->member_country){ ?> selected <?php } }?> ><?php echo $rows->country_name;  ?></option>
		             <?php } ?>
				</select>
				<input type="text" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}?>"  placeholder="Keyword..." name="keyword"/>
				<input type="submit" value="Search" name="btn_search"/>
			</form>
		</div>
		<!-- end search -->

		<!-- start info seller -->
		 <?php 
		 	foreach($sellers as $row) { 
		 	$row_img=$row->base_url.$row->public_id;
		 	?>
		<div class="wrapper_info_seller">
			<div class="info_seller_blog">
				<div class="info_title_seller">
					<span><?php echo ($row->company_name !="" ? $row->company_name : $row->contact_person); ?></span>
				</div>
				<div class="seller_left">
					<?php 
	                  if($row->public_id !=""){ ?>
	                  	<img  src="<?php echo $row_img; ?>"/>
		              <?php  }else{ ?>
		              	<img src="<?php echo "/images/user/no_image_seller.jpg";?>"/>
	              	<?php } ?>
				</div>
				<div class="seller_right">
				    <div class="float_left"><span>Contact Person :</span></div>
		 			<div class="contact_person"><p style="word-break: break-all;"><?php echo ($row->contact_person !="" ? $row->contact_person : "N/A" ); ?></p></div>
		 			<div class="clear"></div>

		 			<div class="float_left"><span>Country :</span></div>
		 			<div class="contact_addr_country"><p><?php echo ($row->country_name!='' ? $row->country_name : 'N/A'); ?></p></div>
		 			<div class="clear"></div>

		 			<div class="float_left"><span>Address :</span></div>
		 			<div class="contact_addr_country"><p><?php echo ($row->customer_address!='' ? substr($row->customer_address,0,75)  : 'N/A' ); ?></p></div>
		 			<div class="clear"></div>
				</div>
				<div class="clear"></div>
				<div class="total_product_seller">
					<div class="seller_bottom_left">
						<span>Total products :</span>
						<p><?php echo number_format($row->count_cars); ?></p>
					</div>
					<div class="seller_bottom_right">
						<a href="/?c=user&m=mobile_seller_detail&id=<?php echo $row->member_no; ?>">
						<input type="button" value="View Profile"/></a>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	 <?php	} ?>
		<!-- end info seller -->

		
	 <?php

if($total_count > $perpagepagination){
      $totalpage = ceil($total_count / $perpagepagination);
    ?>
	<div class="pagination">
		<ul class="paginnav">
			<?php if($getnumpage > 1){ ?>
				<li><a href="?c=user&m=mobile_seller&country=<?php if(isset($_GET['country'])){ echo $_GET['country'];}else{ echo "";} ?>&keyword=<?php  if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}?>&page=1">&laquo;</a></li>
				<li><a href="?c=user&m=mobile_seller&country=<?php if(isset($_GET['country'])){ echo $_GET['country'];}else{ echo "";} ?>&keyword=<?php  if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}  ?>&page=<?php if($getnumpage == 1){ $numpage = $getnumpage; }else{ $numpage = $getnumpage - 1; } echo $numpage; ?>">Previous</a></li>
			<?php } ?>

			<?php

              if(($getnumpage-2) <= 0){
                  $currentpagenumber = 1;
              }else{
                  $currentpagenumber = $getnumpage-2;
              }

              if(($getnumpage+2) > $totalpage){
                  $totalcurrent = $totalpage;
              }else{
                  $totalcurrent = $getnumpage+2;
              }

          ?>
          <?php $n=1;for($i=$currentpagenumber; $i<=$totalcurrent; $i++){  ?>
               <li <?php if($i == $getnumpage){ ?> class="current" <?php } ?>><a class="<?php if($i == $getnumpage){ ?>not_click<?php } ?>" href="?c=user&m=mobile_seller&country=<?php if(isset($_GET['country'])){ echo $_GET['country'];}else{ echo "";} ?>&keyword=<?php  if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}  ?>&page=<?php echo $i; ?>"><?php echo $i;?></a></li>
          <?php } ?>

          <?php if($getnumpage != $totalpage){ ?>
			<li><a href="?c=user&m=mobile_seller&country=<?php if(isset($_GET['country'])){ echo $_GET['country'];}else{ echo "";} ?>&keyword=<?php  if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}  ?>&page=<?php if($getnumpage >= $totalpage){ $numpage = $getnumpage; }else{ $numpage = $getnumpage + 1; } echo $numpage; ?>">Next</a></li>
			<li><a href="?c=user&m=mobile_seller&country=<?php if(isset($_GET['country'])){ echo $_GET['country'];}else{ echo "";} ?>&keyword=<?php  if(isset($_GET['keyword'])){ echo $_GET['keyword'];}else{ echo "";}  ?>&page=<?php echo $totalpage; ?>">&raquo;</a></li>
		  <?php } ?>

		</ul>
	</div><div class="clear"></div>
<?php } ?>
		
	</div>
</div>