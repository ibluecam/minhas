
  <?php //=========================================== Content include ========================================== ?>


<div class="detail_wrapper">
<!---- car detail left start ---->

	<div class="title_detail">
		<?php
           if($cardetail[0]->num_rows>0){
                 foreach( $cardetail as $rows){
					$carmodelyear = "";
                    if($rows->car_model_year != 0){
						$carmodelyear = $rows->car_model_year;
                    }
        ?>
        <img src="/images/public_site/details.png" /><p class="bigger"><?php echo $carmodelyear."&nbsp;".$rows->car_make."&nbsp;".$rows->car_model; ?></p>
        <?php 
			} 
		} ?>
    </div>
    
    <!---- car info start ---->
    <div class="car_info_wrapper">

		<div class="wrap_image">
        <div class="wrap_image_big">
        	<div class="imgload" id="carSelected">
            	
                <?php             
				$i = 0;          
               foreach ($getPrimaryImage as $img_rows){
                    if($getPrimaryImage[0]->num_rows > 0){
                ?>
      <a class="fancybox" data-fancybox-group="gallery"></a>
      <a class="clickimg"><img class="imgchange" src="<?php echo $img_rows->base_url."c_fill,h_768,w_1024/".$img_rows->public_id; ?>">
   <?php if($rows->icon_status=="soldout"){
      	echo' <img class="sold_out" src="https://res.cloudinary.com/softbloom/image/upload/v1435031658/sold-out_xfi7yz"/>';
         }

         if($rows->icon_status=="reserved"){
        echo' <img class="sold_out" src="https://res.cloudinary.com/softbloom/image/upload/v1435031658/reserved_oisi6d"/>';
         }
	?>
             
     </a>
           
               <?php
               }else{
               ?>
              <?php if($cardetail[0]->num_rows>0){ ?>
          <img class="imgchange" src="https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_big_d2odu9"/>
              <?php }else{ ?>
              <p>The car does not exit.</p>

                                    <?php
								   }
										 }
									 break;
								  $i++;
									 }
									 ?>
                    </div>
            </div>

                         <!--block image small start-->
             <div class="wrap_image_small">
             <form action="/?c=user&m=download_image" name="zips" method="post">
            <div class="small_img">
            
             <?php
             
                $i=0;
                foreach($getDetailImage as $img_rows){
                $i++;
                $file = $img_rows->public_id;
                ?>
                <div class="small-img carAcessory2" style="opacity: 1;">
                
                    <a class="fancybox" accesskey="<?php echo $img_rows->base_url.$img_rows->public_id; ?>" data-fancybox-group="gallery" href="#"> </a>
                       <a  class="example-image-link cantrig_<?php echo $i; ?>" href="<?php echo $img_rows->base_url."c_fill,h_768,w_1024/".$img_rows->public_id; ?>" data-lightbox="example-set" data-title="<a  class='a_download bigger' href='<?php echo $img_rows->base_url."c_fill,h_768,w_1024/".$img_rows->public_id; ?>'>Download</a>"><img class="smallimg imgactive_<?php echo $i; ?> <?php if($i == 1){ ?> imgactive <?php } ?>" a="<?php echo $i; ?>" id="new" src="<?php echo $img_rows->base_url.'c_fill,h_66,w_86/'.$img_rows->public_id; ?>" border="0"></a>
                      
                   <input type="hidden" name="files[]" value="<?php echo $file; ?>" />
                   
                </div>
            <?php } ?>
            
                    
            </div>
            <input class="download_img" type="submit" name="download_image" value="Download All" />
            </form>
            </div>
            <!--block image small end-->
            <div style="clear:both;"></div>
        </div>

        <div style="clear:both;"></div>
        <!---- car specification start ---->
        <div class="spec_wrapper">
        	<div class="title_detail">
		
        		<img src="/images/public_site/details.png" /><p class="bigger">Car's Specification</p>
        
    		</div>
            <?php
				foreach( $cardetail as $rows){
			?>
        	
            
            	<div class="spec">
                	<div class="left blue smaller">Stock ID</div>
                    <div class="right blue smaller"><?php echo ($rows->car_stock_no !='' && $rows->car_stock_no > 0 ? $rows->car_stock_no : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Condition</div>
                    <div class="right smaller">
					<?php if( $rows->icon_new_yn == "Y"){
						echo "New";}else{ echo "Used";}
					?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Make</div>
                    <div class="right blue smaller"><?php echo ($rows->car_make !='' || $rows->car_make > 0 ? $rows->car_make : 'N/A');?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Model</div>
                    <div class="right smaller"><?php echo ($rows->car_model !='' || $rows->car_model > 0 ? $rows->car_model : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Chassis No</div>
                    <div class="right blue smaller"> <?php echo ($rows->car_chassis_no !='' || $rows->car_chassis_no > 0 ? $rows->car_chassis_no : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Model Year</div>
                    <div class="right smaller"><?php echo ($rows->car_model_year !='' && $rows->car_model_year > 0 ? $rows->car_model_year : 'N/A');?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Body Type</div>
                    <div class="right blue smaller"><?php echo ($rows->car_body_type !='' || $rows->car_body_type > 0 ? $rows->car_body_type : 'N/A');  ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Mileage</div>
                    <div class="right smaller"><?php echo ($rows->car_mileage !='' && $rows->car_mileage > 0 ? number_format($rows->car_mileage) ." km" : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Steering</div>
                    <div class="right blue smaller"><?php
                                       if($rows->car_steering == "LHD"){ $steering = "Left Hand Drive";}else{ $steering = "Right Hand Drive";}
                                        echo ($steering !='' || $steering > 0 ? $steering : 'N/A'); ?></div>
                </div>
                
                
           
            
            
            
            	<div class="spec">
                	<div class="left smaller">Transmission</div>
                    <div class="right smaller"><?php echo ($rows->car_transmission !='' || $rows->car_transmission > 0 ? $rows->car_transmission : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Engine Size</div>
                    <div class="right blue smaller"><?php echo ($rows->car_cc !='' && $rows->car_cc > 0 ? number_format($rows->car_cc) ." CC" : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Drive Type</div>
                    <div class="right smaller"><?php echo ($rows->car_drive_type !='' || $rows->car_drive_type > 0 ? $rows->car_drive_type : 'N/A');  ?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Fuel Type</div>
                    <div class="right blue smaller"><?php echo ($rows->car_fuel !='' || $rows->car_fuel > 0 ? $rows->car_fuel : 'N/A');  ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Location</div>
                    <div class="right smaller">
					<?php
                          foreach ($list_country as $list ){
                             if($list->cc==$rows->country){
                    ?>
					<?php echo ($rows->country !='' || $rows->country > 0 ? $list->country_name .",": 'N/A'); ?>
                    <?php echo ($rows->car_city !='' || $rows->car_city > 0 ? $rows->car_city: 'N/A'); ?>
                    <?
                               }
                          }
                    ?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">Exterior Color</div>
                    <div class="right blue smaller"><?php echo ($rows->car_color !='' || $rows->car_color > 0 ? $rows->car_color : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">Manufactured Date</div>
                    <div class="right smaller"><?php echo ($rows->manu_year !='' && $rows->manu_year > 0 ? $rows->manu_year : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left blue smaller">registration Date</div>
                    <div class="right blue smaller"><?php echo ($rows->first_registration_year !='' && $rows->first_registration_year > 0 ? $rows->first_registration_year : 'N/A'); ?></div>
                </div>
                
                <div class="spec">
                	<div class="left smaller">FOB</div>
                    <div class="right smaller"><span style="color:#0772ba">
					<?php
                                                $price = number_format($rows->car_fob_cost);
                                                if(empty($price))
                                                {
                                                $price = 'ASK';
                                                }
                                                else
                                                {
                                                $price = $price." ".$rows->car_fob_currency;
                                                }
                                                ?>
                                                <?= $price ?>
                                          </span></div>
                </div>
                
                <div class="spec">
                    <div class="left blue smaller"><p class="bigger">Files</p></div>
                    <div class="right blue smaller"></div>
                </div>
                <div class="spec files_for_buyer">
      
                        <?=func_create_attach_files_name($bbs_attach_files,'carItem','N','Y','','Y','','N')?>
                </div>
                
               
            
             <?php } ?>
            </div>
        <!---- car specification end ---->    
        </div>
		<!---- car info end ---->
</div>

 <?php //=========================================== Content ========================================== ?>

