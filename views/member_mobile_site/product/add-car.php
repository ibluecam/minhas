<?
     if (count($bbs_detail_list) > 0) {
         foreach ($bbs_detail_list as $rows) {
 ?>
 
 <form name="modify_frm" id="modify_frm" action="?c=admin&m=mobile_modify_car_exec&idx=<?=$rows->idx?>" method="post" enctype="multipart/form-data">
 
<div class="wrapper_addcar">
 
 
 <input type="hidden" name="idx" id="idx" value="<?=$rows->idx?>">
 <?php
    if(isset($_GET["create_mode"]) =='new'){
 ?>
<input type="hidden" value="<?php echo $_GET['create_mode']; ?>" name="values[create_mode]"/>
<?php 
    } 
?>
 
 
    <label>Car Condition</label><br/>
    <select name="values[icon_new_yn]">
    <option <?php if($rows->icon_new_yn=='N' || $rows->icon_new_yn=='' ) echo "selected"; ?> value="N">Used</option>
    <option <?php if($rows->icon_new_yn=='Y' ) echo "selected"; ?> value="Y">New</option>
    </select>
    
    <label>Location</label><br/>
    <div class="wrapper_two_column">
    	<select class="left" name="values[country]">
       <?php foreach($country_list as $country){ ?>
            <option 
            <?php 
            if ($rows->country !=''){
				if($rows->country == $country->cc){
					 echo 'selected="selected"';
				 }
            }else{
                if ($member_logged->member_country == $country->cc){
                      echo 'selected="selected"';
                } 
            }
                                        
       ?> 
              value="<?= $country->cc ?>"><?= $country->country_name ?></option>
      <?php } ?>
        </select>
        
        <input class="right" type="text" name="values[car_city]" value="<?= $rows->car_city ?>" placeholder="City" />
    </div><div class="clear"></div>
    
     <label>Status</label><br/>
    <select>
    <option value="sale"<?php echo ($rows->icon_status == 'sale') ? 'selected="selected"' : ''; ?>>Sale</option>
                                    <option value="reserved"
                                            <?php echo ($rows->icon_status == 'reserved') ? 'selected="selected"' : ''; ?>> Reserved</option>
                                    <option value="shipok" <?php echo ($rows->icon_status == 'shipok') ? 'selected="selected"' : ''; ?>>ShipOk</option>
                                    <option value="soldout" <?php echo ($rows->icon_status == 'soldout') ? 'selected="selected"' : ''; ?>>Soldout</option>
                                    <option value="releaseok" <?php echo ($rows->icon_status == 'releaseok') ? 'selected="selected"' : ''; ?>>ReleaseOk</option>
    </select>
    
     <label>make (*)</label><br/>
    <select id="make_list" title="To add more Make Please Select -Add New Make-" required>
    <option value="">- Select -</option>
                                        <?php foreach($make_list as $make){ 
                                                $background_url =$make->icon_name;
                                        ?>
                                                <option <?php if(isset($rows->car_make)){ if($rows->car_make == $make->make_name){ ?> SELECTED <?php } } ?> value="<?php echo strtoupper($make->make_name) ?>" style="background-image: url('<?php echo $background_url; ?>');background-size:30px 15px; background-repeat: no-repeat;padding-left: 35px;" ><?php echo strtoupper($make->make_name); ?></option>
                                                
                                        <?php } ?>
       <option class="add_new_maker" value="add_new_maker" style="background-image: url('/images/admin/add.png');background-size:30px 15px; background-repeat: no-repeat;padding-left: 35px;">NEW MAKER</option>
    </select>
    <input type="text" style="display: none;" id="car_make" value="<?= $rows->car_make ?>" name="values[car_make]" />
    
    
     <label>Model (*)</label><br/>
    <select id="model_list" title="To add more Model you can select -Add New Model-" required>
    <option value="">- Select -</option>

                                        <?php foreach($all_model_list as $model){ ?>
                                                <option <?php if(isset($rows->car_model)){ if($rows->car_model == $model->model_name){ ?> SELECTED <?php } } ?> class="<?php echo strtoupper($model->make_name); ?>" value="<?php echo strtoupper($model->model_name); ?>"><?php echo strtoupper($model->model_name); ?></option>
                                        <?php } ?>

                                         <option class="add_new_model" value="">-Add New Model-</option>
    </select>
    <input type="text" style="display: none;" id="car_model" value="<?= $rows->car_model ?>" name="values[car_model]" />
    
    
    <label>Model year</label><br/>
    <input type="text" name="values[car_model_year]" value="<?= ($rows->car_model_year!=0) ? $rows->car_model_year : '' ?>" />
    
     <label>Registration Date</label><br/>
    <div class="wrapper_two_column">
    	<select class="left" name="values[first_registration_month]">
         <?= func_create_months($rows->first_registration_month); ?>
        </select>
        
        <select class="right" name="values[first_registration_year]">
        <?= func_create_years($rows->first_registration_year); ?>
        </select>
    </div><div class="clear"></div>
    
    <label>body Type</label><br/>
    <select name="values[car_body_type]">
    <?php 
                                            foreach($bodytype_list as $bodytype){
                                            ?>
                                                <option <?= ($rows->car_body_type == $bodytype->body_name) ? 'selected="selected"' : '' ?> value="<?= $bodytype->body_name ?>"><?= $bodytype->body_title ?></option>
                                            <?php
                                            } 
                                        ?>
    </select>
    
    <label>Transmission</label><br/>
    <select name="values[car_transmission]">
    <?php foreach (Array('Auto', 'Manual') as $Val) { ?>
                                            <option <?= ($rows->car_transmission==$Val)? 'selected="selected"' : '' ?> value="<?= $Val ?>"><?= $Val ?></option>
                                        <?php } ?>
    </select>
    
      <label>Car Dimension (mm)</label><br/>
    <div class="wrapper_two_column">
    	<input type="text" name="values[car_length]" class="length" value="<?= ($rows->car_length!=0) ? $rows->car_length : '' ?>" placeholder="Length" />
       	<input type="text" name="values[car_width]" class="width" value="<?= ($rows->car_width!=0) ? $rows->car_width : '' ?>" placeholder="Width" />
       	<input type="text" name="values[car_height]" class="height" value="<?= ($rows->car_height!=0) ? $rows->car_height : '' ?>" placeholder="Height" />
    </div><div class="clear"></div>
    
    <label>Steering</label><br/>
    <select name="values[car_steering]">
    <option <?php if($rows->car_steering=='LHD' || $rows->car_steering=='' ) echo "selected"; ?> value="LHD">- LHD -</option>
    <option <?php if($rows->car_steering=='RHD') echo "checked"; ?> value="RHD">- RHD -</option>
    </select>
    
    <label>Chassis No (*)</label><br/>
    <input type="text" id="car_chassis_no" name="values[car_chassis_no]" value="<?= $rows->car_chassis_no ?>" required />
    <label class="chassis_error" id="chassis_error"></label>
    
    <label>Mileage</label><br/>
    <input type="text" name="values[car_mileage]" value="<?= ($rows->car_mileage!=0) ? $rows->car_mileage : '' ?>" />
    
    <label>Grade</label><br/>
    <input type="text" name="values[car_grade]" value="<?= $rows->car_grade ?>" />
    
    <label>Engine Size</label><br/>
    <input type="text" name="values[car_cc]" value="<?= ($rows->car_cc!=0) ? $rows->car_cc : '' ?>" />
    
    <label>Manufactured</label><br/>
    <input type="text" name="values[manu_year]" value="<?= ($rows->manu_year!=0) ? $rows->manu_year : '' ?>" />
    
    <label>Seats</label><br/>
    <input type="text" name="values[car_seat]" value="<?= ($rows->car_seat!=0) ? $rows->car_seat : '' ?>" />
    
    <label>Doors</label><br/>
    <input type="text" name="values[door]" value="<?= ($rows->door!=0) ? $rows->door : '' ?>" />

    <label>Color</label><br/>
    <select name="values[car_color]">
    <option value="">- Select -</option>
        								<?php 
                                        foreach($color_list as $color){
                                        ?>
                                            <option  <?=($rows->car_color == $color->color_name) ? 'selected="selected"' : '' ?> value="<?= $color->color_name; ?>"><?= $color->color_name; ?></option>
                                        <?php
                                        } 
                                        ?>
    </select>
    
    <label>Fuel</label><br/>
    <select name="values[car_fuel]">
    <?php foreach (Array('Petrol', 'Diesel', 'LPG', 'LPG + Petrol', 'Hybrid', 'Gasoline') as $Val) { ?>
    <option <?= ($rows->car_fuel==$Val)? 'selected="selected"' : '' ?> value="<?= $Val ?>"><?= $Val ?></option>
     <?php } ?>
    </select>
    
    <label>Drive Type</label><br/>
    <select name="values[car_drive_type]">
    <option>- Select -</option>
      <? foreach(Array('2WD', '4WD') as $Val ) { ?>
         <option <?= ($rows->car_drive_type==$Val)? 'selected="selected"' : '' ?> value="<?= $Val ?>"><?= $Val ?></option>
      <? } ?>
    </select>
    
    <label>Fob Cost</label><br/>
    <div class="wrapper_two_column">
    	<input type="text" class="fob_price" name="values[car_fob_cost]" value="<?= ($rows->car_fob_cost!=0) ? $rows->car_fob_cost : '' ?>" />
        
        <select class="currency" id="field-car-select-Cost" name="values[car_fob_currency]">
        <?= func_currency_sym($rows->car_fob_currency); ?>
        </select>
    </div><div class="clear"></div>
    
    <label>Description</label><br/>
    <textarea name="values[contents]" id="description-data" class="textarea"><?= func_decode(func_get_config($bbs_config_result, 'editor_yn'), 'Y', $rows->contents, $rows->contents) ?></textarea>
    
    
    <label>Images</label><br/>
    <div style="margin-bottom:10px;"><a href="#" id="upload_widget_opener" class="upload_img">Upload images</a></div>
    
   
    <div style="height:auto" class="info-field-data">
    	<iframe style="border:none;" id="upload_iframe" width="100%" scrolling="yes" class="upload_iframe" src="/?c=admin&m=member_mobile_bbs_cloud_upload&idx=<?=$rows->idx?>&mcd=product"></iframe>
    </div>
    
</div>

<div style="width:100%; overflow:auto; margin-bottom:30px; margin-top:30px; text-align:center">
    <input type="submit" class="btn-info" id="btn-info-insert" value="Save">
    <input type="button" onclick="cancel_modify()" class="btn-info_cancel" id="btn-info-cancel"  value="Cancel">
</div><div class="clear"></div>

</form>
 <?
            }
       } 
 ?>