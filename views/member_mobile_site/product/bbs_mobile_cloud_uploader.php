<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <title>PhotoAlbum - Upload page</title> 
   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script> 
    <link rel="stylesheet" type="text/css" href="/css/admin/upload_iframe.css" />
  </head>
  
  <body>
     
    <div id="mynewcontainer" class="upload_container">
    <div id="list">

    </div>

     <!-- action="/?c=admin&m=adm_bbs_cloud_upload_exec&idx=<?=$_GET['idx']?>" -->
      <form id="image_form" name="adm_frm" action="" method="post" enctype="multipart/form-data">
      <input type="hidden" id="idx" value="<?=$_GET['idx']?>" name="idx"/>
      <!-- <input type="file" id="attach" class="attach" name="file[]" id="fileToUpload" multiple accept=".jpg, .jpeg, .gif, .png"> -->

      <!--<div class="block_delete">

        <a href="#" onclick="delete_all_items_check();"  title="delete" style="float: right;text-decoration: none;"><span class="img_b">Delete Selected</span></a>

        <label style="float: right;margin: 8px;font-family: Arial,sans-serif;font-weight: bold;font-size: 12px;">Select All <input type="checkbox" name="all" id="selectall" /></label>
        <label style="float: right;margin: 12px;font-family: Arial,sans-serif;font-weight: bold;font-size: 12px;" id="lable_selected"><span id="count_selected"></span></label>

      </div>-->
      <input type="hidden" id="count_image" name="count_image" value="<?=count($bbs_cloud_list)?>">

 <?php
            $ifile = true;
            $modi_att = count($bbs_cloud_list);
            if($modi_att==0) echo '<ul id="sortable">';

            if (count($bbs_cloud_list) > 0) {
                echo "<div class='div-img-title'";
                
                echo "</div>";
                $first_primary=true;
                echo '<ul id="sortable">';
                foreach ($bbs_cloud_list as $att_rows) {

                  echo "<li>";
                  echo "<div class='div-image-view'>";
                  //echo "<label style='bottom: 108px;left: 2px;padding: 10px;position: absolute;'>
//
//                            <span style='padding:1px 1px 0 0;background:#606F77;margin: 3px;'><input type='checkbox' class='checkbox' title='You can delete multiple images by checked this option' name='idxs[]' id='idx' value=" .$att_rows->public_id." ></span>
//
//                        </label>";
                  echo "<img id='courser_style' title='Drag image to change the position of your image on website' src='" . $att_rows->base_url . "t_item_thumb/".$att_rows->public_id."' width='150' >";
                  ?>
                 


                  
                  <label class="label_primary"><input type="radio" value="<?=$att_rows->id ?>" class="primary" name="primary" <?php if($att_rows->sort==0&&$first_primary) echo "checked";  ?>/><span>Primary</span></label>  
                  <a href="/?c=admin&m=adm_bbs_delete_attach_cloud&public_id=<?php echo $att_rows->public_id; ?>&idx=<?php echo $att_rows->bbs_idx; ?>" onClick="return (confirm('Do you want to delete image?'))" title="Delete" class="delete_image"></a>
                  <input type="hidden" class="sort_value" name="sort[<?=$att_rows->id ?>]" value="<?=$att_rows->sort ?>">
                  
                  <?php
                  echo "</div>";
                  if($att_rows->sort==0) $first_primary=false; 
                  echo "</li>";

              } 
            }

            ?>
            </ul>
        
      </form>
        
      
    </div>
          
    <style>
    /*.block_delete{
      border: solid 1px #000;
      height: 20px;
    }*/
    .delete_image{
      display: inline-block;
      float: right;           
      zoom: 1;
      vertical-align: baseline;
      outline: none;
      cursor: pointer;
      text-align: center;
      text-decoration: none;
      font: 14px/100% Arial, Helvetica, sans-serif;
      padding: 24px 8px;
	  background:url(../../../images/public_mobile_site/enter_info/icon_delete.png) no-repeat;
    }
    .img_b{
      color: red;
      display: inline-block;           
      zoom: 1;
      vertical-align: baseline;
      margin: 0 2px;
      outline: none;
      cursor: pointer;
      text-align: center;
      text-decoration: none;
      font: 14px/100% Arial, Helvetica, sans-serif;
      padding: .5em 2em .55em;
      text-shadow: 0 1px 1px rgba(0,0,0,.3);
      -webkit-border-radius: .5em;
      -moz-border-radius: .5em;
      border-radius: .5em;
      -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
      -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
      box-shadow: 0 1px 2px rgba(0,0,0,.2);
      color: #fae7e9;
      border: solid 1px #b73948;
      background: #da5867;
      background: -webkit-gradient(linear, left top, left bottom, from(#f16c7c), to(#bf404f));
      background: -moz-linear-gradient(top, #f16c7c, #bf404f);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f16c7c', endColorstr='#bf404f');
    }
    #courser_style:hover{
      cursor: move;
    }
    </style>
<script>

$("#selectall").click(function(e){
        var count_selected = $(".checkbox").prop('checked', this.checked);
        if(count_selected.length > 0){
          $(".img_b").css('background','');
          $(".img_b").css('border-color','');
          $(".img_b").css('color','');
        }else{
          $(".img_b").css('background', '#F4F4F4');
          $(".img_b").css('border-color', '#ADB2B5');
          $(".img_b").css('color', '#ADB2B5');
        }
});

$('input[type="file"]').change(function () {
  if($(this)[0].files.length >50){
    alert('You can only choose 50!');
    $(this).val('');
  }else{
    $("#image_form").submit();
  }
});
var orderChange = false;
var invalid_exts = [];
$(document).ready(function(e){
    var count_image = $("#count_image").val();
    if(count_image==0){
        $(".block_delete").attr('style','display: none !important');
    }

    $(".img_b").css('background', '#F4F4F4');
    $(".img_b").css('border-color', '#ADB2B5');
    $(".img_b").css('color', '#ADB2B5');
    
    $(document).on("click","input[type*='checkbox']",function() {
    // $('input[type="checkbox"]').click(function(){

        if(($('#idx:checked').length) > 0){
          $(".img_b").css('background','');
          $(".img_b").css('border-color','');
          $(".img_b").css('color','');
          $("#lable_selected").html('<span id="count_selected"></span> Images selected')
          $("#count_selected").html($('#idx:checked').length);
        }
        else{
          $("#lable_selected").html('');
          $(".img_b").css('background', '#F4F4F4');
          $(".img_b").css('border-color', '#ADB2B5');
          $(".img_b").css('color', '#ADB2B5');
        }

    });

  $( "#sortable" ).sortable({
  change: function(event, ui){
    orderChange = true;
  },
  stop: function(event, ui){
    refreshOrder();
  }
});
$( "#sortable" ).disableSelection();
$(document).on("click","input[name*='primary']",function() {
// $(".primary").change(function(e){
  var bbs_idx = $("#idx").val();
  var idx = $(this).val();
  var data = {
    idx: idx,
    bbs_idx: bbs_idx,
  }
  $.ajax({
    type: "POST",
    url: "/?c=admin&m=adm_set_primary_cloud_image",
    data: data,
    success: function(data){
      if(data==1){
        location.reload();
      }else{
        alert("Fail!");
      }
    }
  });
});
   
}); 
  
function resetPrimaryRadio(){
     $("#sortable li:first-child .primary").prop( "checked", true );
}
function refreshOrder(){
  var i=0;
  if(orderChange==true){
    $(".sort_value").each(function(e){  
      $(this).val(i);
      i++;
    });
    orderChange = false;
    sort_all_item_images();
  }
}
function sort_all_item_images(){
  var f = $("#image_form");
  var form_data = f.serialize();
      $.ajax({
      type: "POST",
      url: "/?c=admin&m=adm_set_all_bbs_image_sort",
      data: form_data,
      success: function(data){
        resetPrimaryRadio();
      },
      dataType: "html"
  });
          
}
function append_image(data){
  $.each(JSON.parse(data), function(idx, obj) {
    $('#sortable').append('<li class="ui-sortable-handle"><div class="div-image-view">'
     /* +'<label style="bottom: 108px;left: 2px;padding: 10px;position: absolute;">'
      +'<span style="padding:1px 1px 0 0;background:#606F77;margin: 3px;">'
      +'<input type="checkbox" class="checkbox" title="You can delete multiple images by checked this option" name="idxs[]" id="idx" value="'+obj.public_id+'" >'
      +'</span></label>'*/
      +'<img src="'+obj.base_url+'t_item_thumb/'+obj.public_id +'" id="courser_style" width="150" title="Drag image to change the position of your image on website" />'
      +'<label class="label_primary"><input type="radio" value="'+obj.idxs+'" class="primary" name="primary"/><span>Primary</span></label>'  
      +'<a href="/?c=admin&m=adm_bbs_delete_attach_cloud&public_id='+obj.public_id+'&idx='+obj.bbs_idx+'" title="Delete" class="delete_image">Delete</a>'
      +'<input type="hidden" class="sort_value" name="sort['+obj.bbs_idx+']" value="'+obj.sort+'">'
      +'</div></li>');  
  });
    $('.delete_image').attr("onClick", "return (confirm('Do you want to delete image?'))");
    $(".block_delete").attr('style','display: list-item !important');
    $('input:radio:first').attr('checked',true);
    location.reload();
} 

function delete_all_items_check() {
        var f = document.adm_frm;
        var fg = false;
    
        if (f.idx != null) {
            for (i =0; i < f.idx.length; i++) {
              
                if (f.idx[i].checked) {
                   
                    fg = true;  
        
                }
                
            }

            if (!fg) {
                alert('Please select images that you want to delete.');
                return;
            }

            if (!confirm('Are you sure want to delete these images?'))
            {
                return;

            }else{
                     
                f.action = '/?c=admin&m=adm_bbs_delete_attach_cloud_check&mcd=<?= $mcd ?>';
                f.submit();
               
            }


        }
    }
</script>

  </body> 
</html>