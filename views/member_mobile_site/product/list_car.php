



	<div class="content_new_update">
		
		<div class="new_update_title">
			
			<div class="title">
				<h5>CAR LIST</h5>
			</div>

		</div>

		<div class="block_content">


			<?php
	           if(count($bbs_list_sale)>0){
	              foreach ($bbs_list_sale as $row) {
		              
						$carmodelyear = "";
						$cartitle = "";
						
						if($row->car_model_year != 0){
							$carmodelyear = $row->car_model_year;
						}

						$cartitle = $carmodelyear ." ".$row->car_make ." ". $row->car_model;


						// car_chassis_no
						$car_chassis_no = "N/A";

						if($row->car_chassis_no != ""){
	          
	                      $car_chassis_no =  substr(strrev($row->car_chassis_no) , 0 ,6);

	                      $car_chassis_no = strrev($car_chassis_no);
	                    }


	                    // Car_Mileage
	                    $car_mileage = "N/A";

						if(($row->car_mileage != 0) and ($row->car_mileage != "")){
	          
	                      $car_mileage = number_format($row->car_mileage)." km";
	                    }


	                    // car_transmission
						$car_transmission = "N/A";

						if($row->car_transmission != ""){
	          
	                      $car_transmission = $row->car_transmission;

	                    }


	                    // car_steering
						$car_steering = "N/A";

	                    if($row->car_steering == "LHD"){ 
	                    	$steering = "Left hand";
	                    }else{ 
	                    	$steering = "Right hand";
	                    }
	        ?>

			        <div class="list_item">
						
						<div class="image_item">
							<?php
      
					              if($row->bbs_idx=='0' || $row->bbs_idx==NULL && $row->icon_status=="sale"){
					                 echo "<img style='width:167px;height:125px;'  src='https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_comzyj'>";
					              }
					               else if($row->icon_status=="reserved"){
					                ?>
					                
					              
					                   <?php
					                  echo "<img  src='" . $row->base_url . "c_scale,h_125,q_80,w_167/".$row->public_id."'>";
					                  ?>
					                  <img class="statust_car" src="/images/member_site/reserved.png"/>

					              
					                <?php

					              }
					               
					               
					                else if($row->icon_status=="soldout"){
					                ?>
					                
					              
					                   <?php
					                  echo "<img  src='" . $row->base_url . "c_scale,h_125,q_80,w_167/".$row->public_id."'>";
					                  ?>
					                  <img class="statust_car" src="/images/member_site/soldout.png"/>

					               
					                <?php

					              }



					              else{
					                echo "<img  src='" . $row->base_url . "c_scale,h_125,q_80,w_167/".$row->public_id."'>";
					              }

					        ?>	
							
							<div class="tools_edite_delete">
								 <input type="checkbox" style="display: none;" class="checkcar" data-title="<?php echo $row->idx; ?>"  name="idxs[]" value="<?php echo $row->idx; ?>"/>
								<div class="tools_edite"><a href="/?c=admin&m=mobile_modify_car&mcd=product&idx=<?=$row->idx?>"><img src="/images/mobile_member_site/icon_edit_admin_carlist.png"></a></div> 

								<div class="tools_delete"><a data-toggle="modal" id="delete_one" href="#myModal2" class="info_delete"><img src="/images/mobile_member_site/icon_delete_admin_carlist.png"></a></div>
								<div class="clear"></div>
							</div><div class="clear"></div>

						</div>

						<div class="info_item">
							<h5><?php echo $cartitle; ?></h5>

							<div class="wrap_item">
								<div><div class="text_left"><span>Chassis No : </span><?php echo $car_chassis_no; ?> |</div> <div class="text_right"><span>Mileage : </span><?php echo $car_mileage; ?></div></div><div class="clear"></div>
								<div><div class="text_left"><span>Transmission : </span><?php echo $car_transmission; ?> |</div> <div class="text_right"><span>Steering : </span><?php echo $car_steering; ?></div></div>
							</div><div class="clear"></div>

							<p class="fob_price">FOB <span class="fob_price_number"><?php echo ($row->car_fob_cost !=0 ? "US $".number_format($row->car_fob_cost) : 'ASK'); ?></span></p>
						</div><div class="clear"></div>

					</div>



	        <?php 
	        		}//End foreach
	        	 		} //End IF 
	        	 		else{
	        	 			echo "<center>Car Not Found</center>";
	        	 		}
	        ?>


		

			<!--<div class="list_item">
				
				<div class="image_item">
					<img src="https://res.cloudinary.com/softbloom/image/upload/thumb/tasb4lnt7mwflndy3xzc">	
					
					<div class="tools_edite_delete">

						<div class="tools_edite"><a href="#"><img src="/images/mobile_member_site/icon_edit_admin_carlist.png"></a></div> 

						<div class="tools_delete"><a href="#"><img src="/images/mobile_member_site/icon_delete_admin_carlist.png"></a></div>
						<div class="clear"></div>
					</div><div class="clear"></div>

				</div>

				<div class="info_item">
					<h5>2013 HYUNDAI SONATA</h5>

					<div class="wrap_item">
						<div><div class="text_left"><span>Chassis No : </span>013381 |</div> <div class="text_right"><span>Mileage : </span>106,845 km</div></div><div class="clear"></div>
						<div><div class="text_left"><span>Transmission : </span>Auto |</div> <div class="text_right"><span>Steering : </span>right</div></div>
					</div><div class="clear"></div>

					<p class="fob_price">FOB <span class="fob_price_number">US $100,00</span></p>
				</div><div class="clear"></div>

			</div>-->


			


			

		</div>

		<?php
				
	        echo $pagination;
	    ?>

	</div>

	<div class="modal display_yes" id="myModal2" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">

                         
                    
              <div class="modal-header">
                   
                <h4 class="modal-title" id="myModalLabel_car">Confirmation</h4>

                   
                
              </div><div class="container">
               
             </div>
              <div class="modal-body" id="msgBody_car">
                  
                  Are you sure want to delete this car?
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="btn_yes_one">YES</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id='btnNO_one'>NO</button>
              </div>
            </div>
          </div>
    </div>


