
      <header class="mainHeader">
        
        <div class="top_head_logo_menu">    
          
          <div class="logo">
            <a href="/?c=user&amp;m=mobile_home"><img src="/images/public_mobile_site/m_logo.png"></a>
          </div>
          
          <div class="nav">
            <img class="click_slide_up" src="/images/public_mobile_site/m_nav_icon.png">
          </div><div class="clear"></div>

        </div><div class="clear"></div>

        <div class="bg_bottom">
          <div class="search_left"></div>

            
          <div class="search_center"></div>
            
          <div class="search_right"></div>
        </div>  

        <div style="display: none;" class="content_hide_show nav_content">
            <ul>

              <?php if($_SESSION['ADMIN']['business_type']=="seller") { ?>

              <li <?php if($action_menu == "mobile_list_car"){ ?>class="active"<?php } ?>><a href="/?c=admin&amp;m=mobile_list_car">Car List</a></li>
              <li <?php if($action_menu == "mobile_modify_car"){ ?>class="active"<?php } ?>><a href="/?c=admin&amp;m=mobile_addcar">Add Car</a></li>
              <li <?php if($action_menu == "mobile_info"){ ?>class="active"<?php } ?>><a href="/?c=admin&amp;m=mobile_info">My Info</a></li>

              <?php } else { ?>

              <li <?php if($action_menu == "mobile_list_car"){ ?>class="active"<?php } ?>><a href="/?c=admin&amp;m=mobile_buyer_list_car">Car List</a></li>
              <li <?php if($action_menu == "mobile_info"){ ?>class="active"<?php } ?>><a href="/?c=admin&amp;m=mobile_info_buyer">My Info</a></li>

              <?php } ?>
              <li><a href="/?c=user&amp;m=mobile_logout_exec">Log out</a></li>
            </ul>
        </div>
        <?php if(isset($show_search_box)){ ?>
        <div class="top_head_search">

          <div class="search_left"></div>

            
          <div class="search_center"></div>
            
          <div class="search_right"></div>

          <div class="search_box_tool">
            <form>
              <input class="searchbox keyword" value="<?php if(isset($_GET['sch_word'])){ echo $_GET['sch_word']; } ?>" type="text" placeholder="Keyword">
              <input class="btn_searchbox" id="button_search" type="button" value="">
              <img id="icon_slide_down_searchbox" class="icon_slide_down_searchbox" src="images/public_mobile_site/down_search_box.png">
              <div class="clear"></div>
            </form>
          </div>

          <div class="clear"></div>


          <div class="content_search_box" style="display:none;">
              <div class="search_box_left"></div>
            
              <div class="search_box_center"></div>
                
              <div class="search_box_right"></div><div class="clear"></div>

              <div class="search_box_tool_content">
                <div class="title_search">
                    <img class="icon_search" src="images/public_mobile_site/icon_search.png"><h5>SEARCH CARS</h5><div class="clear"></div>
                </div> 

                <div class="content_form">

                  <form id="image_form" name="" action="" method="get" enctype="multipart/form-data">
                    
                    <select name="carmake" id="make_car" class="field_select make">
                        <option value="">Make</option>
                        <?php if(count($carmaketodropdownsearch) > 0){ foreach($carmaketodropdownsearch as $carmaketodropdownsearchs){ ?>
                          <option value="<?php echo $carmaketodropdownsearchs->car_make; ?>"<?php if(isset($_GET['carmake'])){ if($_GET['carmake']==$carmaketodropdownsearchs->car_make){ echo "selected";}}?> >
                          <?php echo $carmaketodropdownsearchs->car_make; ?></option>
                        <?php }} ?>
                       
                    </select>

                    <select name="carmodel" id="series_car" class="field_select model">
                         <option value="">Model</option>
                        <?php foreach($carmodeltodropdownsearch as $carmodeltodropdownsearchs){ ?>
                          <option class="<?php echo $carmodeltodropdownsearchs->car_make; ?>" value="<?php echo $carmodeltodropdownsearchs->car_model; ?>"<?php if(isset($_GET['carmodel'])){ if($_GET['carmodel']==$carmodeltodropdownsearchs->car_model){ echo "selected";}}?> >
                          <?php echo $carmodeltodropdownsearchs->car_model; ?></option>
                        <?php } ?>
                       
                    </select>
                    
                   <select name="year" class="field_select year">
                      <option value="">Year</option>
                      
                      <?php for($i=date("Y"); $i>=1990; $i--){ ?>
                        <option <?php if(isset($_GET['year'])){ if($_GET['year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                      <?php }  ?>

                  </select>

                 <select name="sch_icon_status" class="field_select status">
                    <option value="">Status</option>
                       <?php foreach (Array('sale','reserved','shipok','releaseok','soldout') as $Val) { ?>
                                  <option <?php echo $sch_icon_status=(isset($_GET['sch_icon_status']) && $_GET['sch_icon_status']=="$Val"? 'selected="selected"':''); ?> 
                                              value="<?php echo $Val; ?>"><?php echo ucfirst($Val); ?></option>
                      <?php } ?>   
                  </select>

                  <input type="text" name="sch_word" class="field_keyword keyword" value="<?php if(isset($_GET['sch_word'])){ echo $_GET['sch_word']; } ?>" placeholder="Keyword">
                  

                  <div class="row_field" id="row_field_button">

                      <div class="field_total_product">
                          <p>Total product : 000</p>
                      </div>

                      <div class="field_btn_click">
                          <input class="btn_resethbox" id="button_reset" type="button" value="Reset">
                          <input class="btn_searchbox" id="button_submit" type="button" value="Submit">
                      </div><div class="clear"></div>

                  </div>
                   
                  </form>

                </div><div class="clear"></div>

              </div>

          </div>
        </div><!-- End top_head_search -->
        <?php } ?>
      </header><!-- End mainHeader -->

