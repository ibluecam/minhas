<!doctype html>
<html class="no-js" lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<head>
  		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    	<link rel="stylesheet" href="/css/member_mobile_site/global.css" />
      <link rel="stylesheet" href="/css/member_mobile_site/lightbox.css" />
	    <?php 
		    /*** OUTPUT EXTERNAL CSS LINK ***/
		    foreach($arr_ex_css as $ex_css){ 
		        echo '<link rel="stylesheet" href="'.$ex_css.'" />';
		    } 
		?>
    
   <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
   
    <title>Motorbb Member</title>
  	</head>
  	<body>
    
		<?php 
			include('header.php'); 
            include('content.php');
			include('footer.php'); 
		?>

		
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
            <script type="text/javascript" src="/js/member_mobile_site/jquery.chained.min.js"></script>
    		<script type="text/javascript" src="/js/member_mobile_site/global.js"></script>           
     <?php 
      if(isset($arr_ex_js)){
          foreach($arr_ex_js as $arr_js){
    ?>
    	<script type="text/javascript" src="<?php echo $arr_js; ?>"></script>
    <?php } } ?>
    
	</body>
</html>