<div class="sidebar_country">
	<div class="sidebar_by_country bg_title">BY COUNTRY</div>
    <div class="wrapp_country">
        <div class="sidebar_japan">
        	<a href="?c=user&m=car_list&country=jp&page=1">
                <div class="sidebar_japan_img"><img src="/images/public_site/icon_japan.png" border="0"></div>
                <div class="sidebar_japan_text">JAPAN (<?php echo $countcarjapan; ?>)</div>
            </a>
        </div>
        <div class="sidebar_japan_img_text">
        	<a href="?c=user&m=car_list&country=kr&page=1">
                <div class="sidebar_japan_img"><img src="/images/public_site/icon_korea.png" border="0"></div>
                <div class="sidebar_japan_text">KOREA (<?php echo $countcarkorea; ?>)</div>
            </a>
        </div>
    </div>
</div>

<div class="sidebar_search_cars">
	<div class="wrapp_sidebar_search_cars">
    	<div class="sidebar_search">SEARCH CARS</div>
        <form action="/?c=user&m=car_list" method="post">
        <div class="select_make">
            <select align="absmiddle" class="sidebar_search_cars_make" id="carmake" name="carmake">
            	<option value="">Make</option>
                <?php if(count($carmaketodropdownsearch) > 0){ foreach($carmaketodropdownsearch as $carmaketodropdownsearchs){ ?>
                <option value="<?php echo $carmaketodropdownsearchs->car_make; ?>"<?php if(isset($_GET['carmake'])){ if($_GET['carmake']==$carmaketodropdownsearchs->car_make){ echo "selected";}}?> >
                <?php echo $carmaketodropdownsearchs->car_make; ?></option>
              <?php }} ?>
            </select>
        </div>
        <div class="select_make">
            <select align="absmiddle" class="sidebar_search_cars_make" id="carmodel" name="carmodel">
            	<option value="">Model</option>
                <?php if(isset($model_list)){ foreach ($model_list as $model) {  ?>
                      <option <?php if(isset($_GET['carmodel'])){ if($_GET['carmodel']==$model->car_model){ ?> Selected <?php } } ?>  value="<?php echo $model->car_model; ?>"><?php echo $model->car_model; ?></option>
                 <?php } }  ?>
            </select>
        </div>
        <div class="select_make">
            <select align="absmiddle" class="sidebar_search_cars_make"  name="searchcountry" id="carcountry">
            	<option value="">Country</option>
                <?php foreach($carcountrytodropdownsearch as $carcountrytodropdownsearchs){ ?>
                  <option <?php if(isset($_GET['carcountry'])){ if($_GET['carcountry']==$carcountrytodropdownsearchs->cc){ ?> Selected <?php } } ?> value="<?php echo $carcountrytodropdownsearchs->cc; ?>"><?php echo $carcountrytodropdownsearchs->com; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="select_make">
            <select align="absmiddle" class="sidebar_search_cars_min_year" id="carminyear" name="searchminyear">
            	<option value="">Min Year</option>
                <?php for($i=date("Y"); $i>=1990; $i--){ ?>
                    <option <?php if(isset($_GET['carminyear'])){ if($_GET['carminyear']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php }  ?>
            </select>
            <select align="absmiddle" class="sidebar_search_cars_max_year" name="searchmaxyear">
            	<option value="">Max Year</option>
                <?php for($i=date("Y"); $i>=1990; $i--){ ?>
                    <option <?php if(isset($_GET['carmaxyear'])){ if($_GET['carmaxyear']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="select_make">
            <input type="text" onkeydown="return isNumberKey(event)" name="searchminprice" value="<?php if(isset($_GET['carminprice'])){ echo $_GET['carminprice']; } ?>" class="sidebar_search_cars_min_year" placeholder="Min Price">
            <input type="text" onkeydown="return isNumberKey(event)" name="searchmaxprice" class="sidebar_search_cars_max_year" value="<?php if(isset($_GET['carmaxrpice'])){ echo $_GET['carmaxrpice']; } ?>" placeholder="Max Price">
        </div>
        <div class="select_make">
            <input type="text" name="keyword" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];} ?>" class="sidebar_search_cars_make" placeholder="Keyword">
        </div>
        <div class="select_make">
            <input type="submit" name="searchboxcar" class="sidebar_btn_search" value="Search">
            <input type="submit" class="sidebar_btn_reset" value="Reset">
        </div>
        
    </form>
    </div>
    <div class="clear"></div>
</div>


<div class="sidebar_brand">
	<div class="sidebar_by_country bg_title">BY BRAND</div>
    <div class="wrapp_country">
    	<?php foreach($get_brand as $make){ ?>
        <div class="sidebar_brand_img_text <?php if(($action == "car_list") and ($param == $make->make_name)){ ?>active<?php } ?>">
        	<a href="?c=user&m=car_list&car_make=<?php echo $make->make_name; ?>&page=1">
            	<div class="sidebar_japan_img" id="<?php echo $make->id; ?>" a="<?php echo $make->make_name; ?>">
                	<img src="<?php echo $make->icon_name; ?>" width="40" height="28" border="0">
                </div>
            	<div class="sidebar_brand_text"><?php echo strtoupper($make->make_name);  ?> (<?php echo $make->c_brand; ?>)</div>
            </a>
        </div>
        <?php }; ?>
        <div class="clear"></div>
    </div>
</div>

<div class="sidebar_brand">
	<div class="sidebar_by_country bg_title">BY TYPE</div>
    <div class="wrapp_country">
    	<?php foreach($getbody_type as $getbodytype){ ?>
        <div class="sidebar_type_img_text <?php if(($action == "car_list") and ($param == strtoupper($getbodytype->body_name))){ ?>active<?php } ?>">
        	<a href="?c=user&m=car_list&car_body_type=<?php echo $getbodytype->body_name; ?>&page=1">
                <div class="sidebar_japan_img" id="<?php echo $getbodytype->id; ?>" a="<?php echo $getbodytype->body_name; ?>">
                	<img src="https://res.cloudinary.com/softbloom/image/upload/v1435028219/logo/bt/<?php echo $getbodytype->icon; ?>" class="type_img" width="50" height="21">
                </div>
                <div class="sidebar_brand_text">
					<?php echo strtoupper($getbodytype->body_title);  ?> (<?php echo $getbodytype->countbodytype; ?>)
                </div>
            </a>
        </div>
        <?php }; ?>
        <div class="clear"></div>
    </div>
</div>

<div class="sidebar_fabook">
	<div id="fb-root">
      <div class="fb-page" data-href="https://www.facebook.com/sb.motorbb" data-height="410" data-width="263" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true">
          <div class="fb-xfbml-parse-ignore">
              <blockquote cite="https://www.facebook.com/sb.motorbb">
                <a href="https://www.facebook.com/sb.motorbb" id="getpagefb"></a>
              </blockquote>
          </div>
      </div>
   </div>
</div>
