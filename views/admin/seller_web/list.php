<script type="text/javascript">

$(document).ready(function() {

    $('.link_url').click(function(event) {   
       var url = $(this).data('url');
       $.colorbox({
            width:"340", 
            height:"250", 
            iframe:true, 
            href:url,
            onClosed:function(){ 
                location.reload(true);
            }
        });
    });
    
});

</script>

<div class="contents">
	<div class="contents_box_middle" style="height:400px;">
        <div class="table_list_box">
            <b class="table_title">
            <?php echo func_get_nav($nav_array); ?>
            </b>
        </div>
		<h1 class="title">Seller's Web:</h1>
		<table class="table_list">
			<tr>
                <th class="col">No.</th>
                <th class="col">Website Name</th>
                <th class="col">Seller</th>
                <th class="col">FOB Commission</th>
                <th class="col"></th>
            </tr>

			<?php

				foreach($seller_list as $seller){
					echo '<tr>';
                    echo '<td>'.$seller->id.'</td>';
					echo '<td>'.$seller->website_name.'</td>';
                    echo '<td>'.$seller->member_id.'</td>';
                    echo '<td>$ '.$seller->fob_commission.'</td>';
                    echo '<td><a href="#" data-url="/?c=admin&amp;m=adm_modify_seler_web&member_id='.$seller->member_no.'&website_name='.$seller->website_name.'" class="link_url">
                    <img src="/images/admin/modify.gif" alt="modify"></a></td>';
					echo '</tr>';
				}
            
			?>
            
		</table>
	</div>
</div>



