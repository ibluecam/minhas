<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/css/admin/admin.css" />
     
<style>
div {
    box-sizing: initial;
}
#change_customer{
  width: 300px;
  padding-left: 15px;
}
.title{ 
  width: 300px;
  padding: 10px 0px 0px 15px;
}
.select_container select{
  -moz-appearance: tabpanels;
  background-color: transparent;
  box-shadow: none;
  cursor: auto;
  display: block;
  font-size: 100%;
  height: 30px;
  line-height: normal;
  margin: 0;
  padding: 0 10px;
  width: 180px;
}
.btn_container{
  border: solid 0px #000; 
  padding-top: 10px; 
  width: 285px;
}
.btn_container input{
  width: 132px;
}
.btn_container .input1{
  padding: 3px 0px;
  float: right;
}
.msg{
  min-height: 20px;
  width: 300px;
  padding-left: 15px;
}
#website_name{
    box-shadow: none;
    cursor: auto;
    display: block;
    font-size: 100%;
    height: 27px;
    line-height: normal;
    margin: 0;
    padding: 0 10px;
    width: 155px;
}
#fob_commission{
    box-shadow: none;
    cursor: auto;
    display: block;
    font-size: 100%;
    height: 27px;
    line-height: normal;
    margin: 0;
    padding: 0 10px;
    width: 155px;
}
</style>
</head>
<body>
    <h3 class="title">Modify Seller's Web</h3>
    <p class="msg" id="msg"><?= $msg ?></p>
    <form id="change_customer" action="" method="post">
      <div class="select_container">
      <input type="hidden" name="member_id" id="member_id" value="<?= $_GET['member_id']; ?>">
      <table style="height:120px;">
        <tr>
          <td><b>Website Name</b></td>
          <td>
            <input type="text" id="website_name" name="website_name" value="<?= $_GET['website_name']; ?>" disabled>
          </td>
        </tr>
        <tr>
          <td><b>Seller</b></td>
          <td>
            <select id="selected_seller" name="selected_seller">
                <?php foreach($select_seller as $seller){ ?>
                      <option value="<?= $seller->member_no ?>"
                      <?= ($seller->member_no==$selected_seller_web[0]->member_no ? 'selected="selected"': '' )?>>
                        <?= $seller->member_id ?>
                      </option>
                <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td><b>FOB Commission</b></td>
          <td>
            <input type="text" id="fob_commission" name="fob_commission" value="<?=$selected_seller_web[0]->fob_commission?>">
          </td>
        </tr>
      </table>
      </div>
      <div class="btn_container">
        <input id="add_button" class="input1 blue" type="submit" value="Save" name="Save">

      </div>
    </form>
</body>
</html>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>

$(document).ready(function() {
  $("#add_button").attr("disabled", "disabled");
  //called when key is pressed in textbox
  $("#fob_commission").keypress(function (e) {
      $("#add_button").removeAttr('disabled');
      //if the letter is not digit then display error and don't type anything
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message //
          return false;
      }
  });

  $("#selected_seller").change(function(e){
    var member_no = $(this).val();

    var data = {
      member_no: member_no,
    }
    $.ajax({
      type: "POST",
      url: "/?c=admin&m=adm_validate_modify_seller_web",
      data: data,
      success: function(data) {
          $("#msg").html("");
          if (data>0) {
              $(".msg").html("The seller's web set is already!");
              $("#add_button").attr("disabled", "disabled");
          } 
          else{
            $("#add_button").removeAttr('disabled');
          }
      }
    });
  });
});
</script>