<div class="contents">
	<div class="contents_box_middle">
		<div class="table_list_box">
			<b class="table_title">
			<?php echo func_get_nav($nav_array); ?>
			</b>
		</div>
		<h1 class="title">Shipping Port Detail:</h1>
		<div class="shipping_view_container">
				<table class="shipping_view_table">
					<tr>
						<th>Destination Country</th>
						<td><?=$rows->country_name?></td>
					</tr><tr>
						<th>Destination Port</th>
						<td><?=$rows->port_name?></td>
					</tr>
					<tr>
						<th></th>
						<td>
							<a href="/?c=admin&m=adm_shipping_port_edit&amp;id=<?= $id ?>"/><span class="button blue">Edit</span></a>
						</td>
					</tr>
				</table>
		    </div>
		    <div class="space_shipping_port"></div>
	</div>
</div>