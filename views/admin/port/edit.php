<div class="contents">
	<div class="contents_box_middle">
		<div class="table_list_box">
			<b class="table_title">
			<?php echo func_get_nav($nav_array); ?>
			</b>
		</div>
		<h1 class="title">Shipping Port Edit:</h1>
			<div class="shipping_port_edit_container">
				<form enctype="multipart/form-data" action="/?c=admin&m=adm_shipping_port_edit_exec<?php if(!empty($id)) echo '&id='.$id;?>" method="post">
					<table class="shipping_port_edit_table">
						<tr>
							<th>Destination Country *</th>
							<td>
								<select name="shipping[country_iso]" required="required">
									<option value="">- Destination Country -</option>
									<?php 
										foreach($country_list as $country){
											if($country->cc == $shipping_port->country_iso) $select = "selected"; else $select='';
											echo '<option '.$select.' value="'.$country->cc.'">'.$country->country_name.'</option>';
										}
									?>
								</select>
							</td>
						</tr><tr>
							<th>Destination Port *</th>
							<td>
							<input name="shipping[port_name]" required="required" type="text" value="<?php echo $shipping_port->port_name; ?>"/>
							</td>
						</tr>
						<tr>
							<th></th>
							<td style="text-align:right;">
								<input type="submit" title="save" value="Save" class="button blue" name="submit"/>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<div class="space_shipping_port"></div>
	</div>
</div>