	<div class="contents_box_middle">
		<h1 class="title">사이트 관리정보 설정</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 사이트의 관리 정보를 생성 & 셋팅을 합니다.</li>
				<li> - 관리자 계정, 회원등급 등을 설정합니다. (회원관리가 존재하지 않을 경우)</li>
			</ul>
		</div>
		<div class="table_write_box">
			<h1 class="table_title">사이트 관리자 계정 생성</h1>
			<table class="table_write" summary="사이트 관리자 계정 생성" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="22%" /><col width="78%" />
				<thead>
					<tr>
						<th scope="col">설정명</th>
						<th scope="col">설정내용</th>
					</tr>
				</thead>
				<tbody>
					<?
					$member_id = '';
					$member_pwd = '';

					if (count($site_admin_chk) > 0) {
						foreach ($site_admin_chk as $rows) {
							$member_id = $rows->member_id;
							$member_pwd = $rows->member_pwd;
						}
					}
					?>
					<tr>
						<td class="key">관리자 계정 등록 & 수정</td>
						<td class="value">
							<form name="create_admin_frm" method="post">
								<label for="admin_id">아이디 :</label>
								<input type="text" name="values[member_id]" id="member_id" value="<?= $member_id ?>" class="input_1" style="width:120px;" title="관리자아이디" onfocus="f_text(this);" onblur="b_text(this);" maxlength="20" />
								<label for="admin_pwd">비밀번호 :</label>
								<input type="text" name="values[member_pwd]" id="member_pwd" value="<?= func_base64_decode($member_pwd) ?>" class="input_1" style="width:120px;" title="관리자비밀번호" onfocus="f_text(this);" onblur="b_text(this);" maxlength="20" />
								<a href="#" onclick="create_admin(); return false;" title="생성"><img src="/images/admin/btn_s_make.gif" alt="생성" align="middle" /></a>
								<span>(?! 사이트 관리자 계정을 생성합니다.)</span>
							</form>
						</td>
					</tr>
					<?
					$iw_member_id = '';
					$iw_member_pwd = '';
					if (count($iw_site_admin_chk) > 0) {
						foreach ($iw_site_admin_chk as $rows) {
							$iw_member_id = $rows->member_id;
							$iw_member_pwd = $rows->member_pwd;
						}
					}
					?>
					<!--
					<tr>
					<td class="key">ROOT 계정 등록 & 수정</td>
					<td class="value">
					<form name="create_admin_frm" method="post">
					<label for="admin_id">아이디 :</label>
					<input type="text" name="values[iw_member_id]" id="iw_member_id" value="<?= $iw_member_id ?>" class="input_1" style="width:120px;" title="관리자아이디" onfocus="f_text(this);" onblur="b_text(this);" maxlength="20" />
					<label for="admin_pwd">비밀번호 :</label>
					<input type="text" name="values[iw_member_pwd]" id="iw_member_pwd" value="<?= func_base64_decode($iw_member_pwd) ?>" class="input_1" style="width:120px;" title="관리자비밀번호" onfocus="f_text(this);" onblur="b_text(this);" maxlength="20" />
					<a href="#" onclick="iw_create_admin(); return false;" title="생성"><img src="/images/admin/btn_s_make.gif" alt="생성" align="middle" /></a>
					<span>(?! ROOT 관리자 계정을 생성합니다.)</span>
					</form>
					</td>
					</tr>
					<tr>
					<td class="key">사이트 정보 초기화</td>
					<td class="value">
					<a href="#" onclick="data_reset(); return false;" title="초기화"><img src="/images/admin/btn_reformat.gif" alt="초기화" align="absmiddle" /></a>
					<span>(?! 사이트 모든 데이터베이스 정보를 초기화 합니다.)</span>
					</td>
					</tr>
					//-->
				</tbody>
			</table>
			<br/>
			<form name="iwc_frm" id="iwc_frm" method="post" action="/?c=iwc&amp;m=exec_site_admin" onsubmit="return reg_siteadmin(this);">
				<h1 class="table_title">사이트 기본 정보 설정</h1>
				<table class="table_write" summary="사이트 기본 정보 설정" cellspacing="0">
					<caption class="hidden"></caption>
					<col width="22%" /><col width="78%" />
					<thead>
						<tr>
							<th scope="col">설정명</th>
							<th scope="col">설정내용</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="key">- 고객(상호)명</td>
							<td class="value">
								<input type="text" name="values[customer_name]" id="customer_name" value="<?= func_get_config($site_admin, 'customer_name', '') ?>" class="input_1" style="width:300px;" title="고객명" onfocus="f_text(this);" onblur="b_text(this);" />
								<span> ?! 고객(상호)명</span>
							</td>
						</tr>
						<tr>
							<td class="key">- 고객 대표 이메일</td>
							<td class="value">
								<input type="text" name="values[customer_mail]" id="customer_mail" value="<?= func_get_config($site_admin, 'customer_mail', '') ?>" class="input_1" style="width:300px;" title="고객메일" onfocus="f_text(this);" onblur="b_text(this);" />
								<span> ?! 대표이메일</span>
							</td>
						</tr>
						<tr>
							<td class="key">- 고객 대표 전화번호</td>
							<td class="value">
								<input type="text" name="values[customer_phone_no1]" id="customer_phone_no1" value="<?= func_get_config($site_admin, 'customer_phone_no1', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, iwc_frm.elements["values[customer_phone_no2]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
								<input type="text" name="values[customer_phone_no2]" id="customer_phone_no2" value="<?= func_get_config($site_admin, 'customer_phone_no2', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, iwc_frm.elements["values[customer_phone_no3]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
								<input type="text" name="values[customer_phone_no3]" id="customer_phone_no3" value="<?= func_get_config($site_admin, 'customer_phone_no3', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" />
								<span> ?! 고객 대표 전화번호</span>
							</td>
						</tr>
						<tr>
							<td class="key">- 고객 대표 휴대폰번호</td>
							<td class="value">
								<input type="text" name="values[customer_mobile_no1]" id="customer_mobile_no1" value="<?= func_get_config($site_admin, 'customer_mobile_no1', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, iwc_frm.elements["values[customer_mobile_no2]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
								<input type="text" name="values[customer_mobile_no2]" id="customer_mobile_no2" value="<?= func_get_config($site_admin, 'customer_mobile_no2', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, iwc_frm.elements["values[customer_mobile_no3]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
								<input type="text" name="values[customer_mobile_no3]" id="customer_mobile_no3" value="<?= func_get_config($site_admin, 'customer_mobile_no3', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" />
								<span> ?! 고객 대표 휴대폰번호</span>
							</td>
						</tr>
						<tr>
							<td class="key">- 사이트 명</td>
							<td class="value">
								<input type="text" name="values[site_name]" id="site_name" value="<?= func_get_config($site_admin, 'site_name', '') ?>" class="input_1" style="width:300px;" title="사이트명" onfocus="f_text(this);" onblur="b_text(this);" />
								<span> ?! 사이트 명을 작성합니다. (사이트 타이틀)</span>
							</td>
						</tr>
						<tr>
							<td class="key">- 사이트 URL</td>
							<td class="value">
								<input type="text" name="values[site_url]" id="site_url" value="<?= func_get_config($site_admin, 'site_url', 'http://') ?>" class="input_1" style="width:300px;" title="사이트명" onfocus="f_text(this);" onblur="b_text(this);" />
								<span> ?! 사이트 URL을 작성합니다.</span>
							</td>
						</tr>
					</tbody>
				</table>
				<br/>
				<h1 class="table_title">사이트 관리 메뉴 설정</h1>
				<table class="table_write" summary="사이트 관리 메뉴 설정" cellspacing="0">
					<caption class="hidden"></caption>
					<col width="22%" /><col width="78%" />
					<thead>
						<tr>
							<th scope="col">설정명</th>
							<th scope="col">설정내용</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="key">- 회원 관리</td>
							<td class="value">
								<input type="radio" name="values[member]" id="member" value="N" class="radio" <?= func_decode(func_get_config($site_admin, 'member'), 'N', 'checked', 'checked') ?> /> 미사용
								<input type="radio" name="values[member]" id="member" value="Y" class="radio" <?= func_decode(func_get_config($site_admin, 'member'), 'Y', 'checked', '') ?> /> 사용
								<span> (?! 사이트 관리자 시스템에서 회원관리 메뉴 사용 여부) </span>
							</td>
						</tr>
						<tr>
							<td class="key">- 커뮤니티 관리</td>
							<td class="value">
								<input type="radio" name="values[bbs]" id="bbs" value="N" class="radio" <?= func_decode(func_get_config($site_admin, 'bbs'), 'N', 'checked', 'checked') ?> /> 미사용
								<input type="radio" name="values[bbs]" id="bbs" value="Y" class="radio" <?= func_decode(func_get_config($site_admin, 'bbs'), 'Y', 'checked', '') ?> /> 사용
								<span> (?! 사이트 관리자 시스템에서 게시판 관리 메뉴 사용 여부) </span>
							</td>
						</tr>
						<tr>
							<td class="key">- 팝업 관리</td>
							<td class="value">
								<input type="radio" name="values[popup]" id="popup" value="N" class="radio" <?= func_decode(func_get_config($site_admin, 'popup'), 'N', 'checked', 'checked') ?> /> 미사용
								<input type="radio" name="values[popup]" id="popup" value="Y" class="radio" <?= func_decode(func_get_config($site_admin, 'popup'), 'Y', 'checked', '') ?> /> 사용
								<span> (?! 사이트 관리자 시스템에서 팝업 관리 메뉴 사용 여부) </span>
							</td>
						</tr>
						<tr>
							<td class="key">- 접속 통계 관리</td>
							<td class="value">
								<input type="radio" name="values[statistics]" id="statistics" value="N" class="radio" <?= func_decode(func_get_config($site_admin, 'statistics'), 'N', 'checked', 'checked') ?> /> 미사용
								<input type="radio" name="values[statistics]" id="statistics" value="Y" class="radio" <?= func_decode(func_get_config($site_admin, 'statistics'), 'Y', 'checked', '') ?> /> 사용
								<span> (?! 사이트 관리자 시스템에서 접속 통계 관리 메뉴 사용 여부) </span>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="btn_area_center">
					<input type="image" src="/images/admin/btn_ok.gif" title="확인" />
					<a href="<?= $base_url ?>" title="취소"><img src="/images/admin/btn_cancel.gif" alt="취소" /></a>
				</div>
		</div>
	</div>
</form>