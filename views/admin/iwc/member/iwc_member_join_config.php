<script type="text/javascript">
	//회원가입 설정 정보 등록 체크
	function reg_member_join_config(frm) {
		if(!validate(frm.elements['values[member_join_skin]'], '회원가입 스킨을 선택 하세요.')) return false;
		return true;

	}
</script>
<form name="iwc_frm" id="iwc_frm" method="post" action="/?c=iwc&amp;m=exec_member_join_config" onsubmit="return reg_member_join_config(this)">
	<input type="hidden" name="member_join_mcd" id="mcd" value="<?= $member_join_mcd ?>" />
	<div class="contents_box_middle">
		<h1 class="title">회원 관리</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 사이트맵에 등록된 회원가입 메뉴의 기본정보를 설정한다.</li>
			</ul>
		</div>
		<div class="table_write_box">
			<h1 class="table_title">회원가입 설정</h1>
			<table class="table_write" summary="회원가입 설정" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="20%" /><col width="80%" />
				<tr>
					<th class="key_01">※ 회원가입 메뉴코드</th>
					<td class="value_01" colspan="3">
						<a href="/?c=user&mcd=<?= $member_join_mcd ?>" title="바로가기" target="_blank"><span class="title">회원가입 [<?= $member_join_mcd ?>]</span></a>
					</td>
				</tr>
				<tr>
					<th class="key_01">※ 회원가입 스킨</th>
					<td class="value_01" colspan="3">
						<select name="values[member_join_skin]" id="member_join_skin" class="input_2" style="width:150px;">
							<option value="">:: 선택하세요. ::</option>
							<?
							$dir_default = array();
							$dir_default_path = 'views/user/member_join/skin';

							$dir_default = scandir($dir_default_path);

//회원가입
							for ($i = 0; $i < count($dir_default); $i++) {
//디렉토리만..
								if (!strstr($dir_default[$i], '.')) {
									?>
									<option value="<?= $dir_default[$i] ?>" <?= func_decode(func_get_config($member_config_result, 'member_join_skin'), $dir_default[$i], 'selected', '') ?>><?= $dir_default[$i] ?></option>
									<?
								}
							}
							?>
						</select>
						<?
//스킨생성이 되었다면
						if (func_get_config($member_config_result, 'member_join_skin') != '') {
							?>
							<input type="checkbox" name="skin_cover" value="Y" /> 덮어쓰기
<? } ?>
						<span> ?!) 해당 스킨 폴더가 생성 되어 있으면 덮어쓰지 않음.</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">관리자 인증</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[certi_yn]" id="certi_yn" value="N" class="radio" <?= func_decode(func_get_config($member_config_result, 'certi_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[certi_yn]" id="certi_yn" value="Y" class="radio" <?= func_decode(func_get_config($member_config_result, 'certi_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 회원가입 후 회원을 관리자가 인증 여부(관리자 미인증시 로그인 불가)</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">가입여부 확인 페이지</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[join_check_yn]" id="join_check_yn" value="N" class="radio" <?= func_decode(func_get_config($member_config_result, 'join_check_yn'), 'N', 'checked', 'checked') ?> /> 미표시
						<input type="radio" name="values[join_check_yn]" id="join_check_yn" value="Y" class="radio" <?= func_decode(func_get_config($member_config_result, 'join_check_yn'), 'Y', 'checked', '') ?> /> 표시
						<span> ?!) 회원가입 시 가입여부 체크 페이지 표시 여부</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">메일발송</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[join_email_yn]" id="join_email_yn" value="N" class="radio" <?= func_decode(func_get_config($member_config_result, 'join_email_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[join_email_yn]" id="join_email_yn" value="Y" class="radio" <?= func_decode(func_get_config($member_config_result, 'join_email_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 회원관리(관리자)에서 메일발송 여부</span>
					</td>
				</tr>
				<!--
				<tr>
				<th class="key_01">탈퇴처리(DB삭제)</th>
				<td class="value_01" colspan="3">
				<input type="radio" name="values[drop_yn]" id="drop_yn" value="N" class="radio" <?= func_decode(func_get_config($member_config_result, 'drop_yn'), 'N', 'checked', '') ?> /> 미삭제
				<input type="radio" name="values[drop_yn]" id="drop_yn" value="Y" class="radio" <?= func_decode(func_get_config($member_config_result, 'drop_yn'), 'Y', 'checked', 'checked') ?> /> 삭제
				<span> ?!) 회원탈퇴시 DB 삭제 여부</span>
				</td>
				</tr>
				//-->
				<tr>
					<th class="key_01">상단 HTML</th>
					<td class="value_01" colspan="3" style="padding:10px;">
						<textarea name="values[member_join_html_header]" id="member_join_html_header" class="textarea_03" style="width:98%;height:100px;padding:3px;" onkeydown="useTab(this);"><?= func_get_config($member_config_result, 'member_join_html_header') ?></textarea><br/>
						<span> ?!) 회원가입 상단 HTML </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">하단 HTML</th>
					<td class="value_01" colspan="3" style="padding:10px;">
						<textarea name="values[member_join_html_footer]" id="member_join_html_footer" class="textarea_03" style="width:98%;height:50px;padding:3px;" onkeydown="useTab(this);"><?= func_get_config($member_config_result, 'member_join_html_footer') ?></textarea><br/>
						<span> ?!) 회원가입 하단 HTML </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">이용약관</th>
					<td class="value_02" colspan="3" style="padding:10px;">
						<textarea name="values[agree]" id="agree" class="textarea_03" style="width:98%;height:150px;padding:3px;" onkeydown="useTab(this);"><?= func_decode(func_get_config($member_config_result, 'agree'), '', '이용약관', func_get_config($member_config_result, 'agree')) ?></textarea>
						<!--
						<script type="text/javascript">
						var xed;
						xed = new xq.Editor("agree");
						xed.isSingleFileUpload = true;
						xed.addPlugin('FileUpload');
						xed.setFileUploadTarget('helpers/iw_SWFUpload_single.php?mcd=<?= $member_join_mcd ?>', null);
						xed.setEditMode('wysiwyg');
						xed.setWidth("100%");
						xed.setHeight("50%");
						</script>
						//-->
					</td>
				</tr>
				<tr>
					<th class="key_01">개인정보취급방침</th>
					<td class="value_02" colspan="3" style="padding:10px;">
						<textarea name="values[privacy]" id="privacy" class="textarea_03" style="width:98%;height:150px;padding:3px;" onkeydown="useTab(this);"><?= func_decode(func_get_config($member_config_result, 'agree'), '', '개인정보취급방침', func_get_config($member_config_result, 'privacy')) ?></textarea>
						<!--
						<script type="text/javascript">
						var xed2;
						xed2 = new xq.Editor("privacy");
						xed2.isSingleFileUpload = true;
						xed2.addPlugin('FileUpload');
						xed2.setFileUploadTarget('helpers/iw_SWFUpload_single.php?mcd=<?= $member_join_mcd ?>', null);
						xed2.setEditMode('wysiwyg');
						xed2.setWidth("100%");
						xed2.setHeight("50%");
						</script>
						//-->
					</td>
				</tr>
			</table>
			<div class="btn_area_center">
				<input type="image" src="/images/admin/btn_ok.gif" title="확인" />
				<a href="<?= $base_url ?>" title="취소"><img src="/images/admin/btn_cancel.gif" alt="취소" /></a>
			</div>

		</div>
	</div>
</form>