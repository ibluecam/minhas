<form name="iwc_frm" id="iwc_frm" method="post" action="/?c=iwc&amp;m=exec_member_grade" onsubmit="return reg_member_grade(this);">
	<input type="hidden" name="exec_type" id="exec_type" value="<?= $exec_type ?>" />
	<input type="hidden" name="seq_no" id="seq_no" value="<?= $seq_no ?>" />
	<div class="contents_box_middle">
		<h1 class="title">회원 등급 설정</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 사이트에 회원등급을 설정 합니다.</li>
				<li> - 회원등급은 15단계까지 설정 할 수 있습니다.</li>
				<li> - 관리자는 1등급입니다. 등급이 높은 순으로 등록합니다.</li>
				<li> - 등급 설정시 기본등급에 체크를 하시면 회원가입시 해당 등급으로 설정 됩니다.</li>
			</ul>
		</div>
		<div class="table_list_box">
			<h1 class="table_title">회원등급</h1>
			<table class="table_list" summary="회원등급" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="10%" /><col width="2px" />
				<col width="" /><col width="" />
				<col width="" /><col width="" />
				<col width="12%" /><col width="2px" />
				<col width="20%" />
				<thead>
					<tr>
						<th scope="col">회원등급</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">회원등급 명</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">회원등급 설명</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">기본등급 여부</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">생성일</th>
					</tr>
				</thead>
				<tbody>
					<?
					if (count($member_grade_result) > 0) {
						foreach ($member_grade_result as $rows) {
							?>
							<tr>
								<td><a href="#" onclick="select_member_grade(document.iwc_frm, '<?= $rows->seq_no ?>', '/?c=iwc'); return false;" title="<?= $rows->grade_name ?>"><span style="color:#e85716;padding:0px 0px 0px 5px;"><?= $rows->grade_no ?>등급</span></a></td>
								<td class="line">&nbsp;</td>
								<td><a href="#" onclick="select_member_grade(document.iwc_frm, '<?= $rows->seq_no ?>', '/?c=iwc'); return false;" title="<?= $rows->grade_name ?>"><?= $rows->grade_name ?></a></td>
								<td class="line">&nbsp;</td>
								<td>&nbsp;<?= $rows->grade_desc ?></td>
								<td class="line">&nbsp;</td>
								<td><?= $rows->default_yn ?></td>
								<td class="line">&nbsp;</td>
								<td><?= $rows->created_dt ?></td>
							</tr>
							<?
						}
					} else {
						?>
						<tr>
							<td colspan="20" class="no_data">Data does not exist.</td>
						</tr>
<? } ?>
				</tbody>
			</table>
		</div>

		<div class="table_write_box">
			<h1 class="table_title">회원등급 설정</h1>
			<table class="table_write" summary="회원등급 설정" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
				<tbody>
<?
$rows = '';
$grade_no = '';
$grade_name = '';
$grade_desc = '';
$default_yn = '';
if ($exec_type == 'UPDATE') {
	if ($member_grade_detail_result->num_rows() > 0) {
		$rows = $member_grade_detail_result->row();
		$grade_no = $rows->grade_no;
		$grade_name = $rows->grade_name;
		$grade_desc = $rows->grade_desc;
		$default_yn = $rows->default_yn;
	}
}
?>
					<tr>
						<th class="key">회원등급</th>
						<td class="value">
							<select name="values[grade_no]" id="grade_no" class="input_2" style="width:150px;">
								<option value="">:: 선택하세요 ::</option>
								<option value="1" <?= func_decode($grade_no, '1', 'selected', '') ?>>①등급</option>
								<option value="2" <?= func_decode($grade_no, '2', 'selected', '') ?>>②등급</option>
								<option value="3" <?= func_decode($grade_no, '3', 'selected', '') ?>>③등급</option>
								<option value="4" <?= func_decode($grade_no, '4', 'selected', '') ?>>④등급</option>
								<option value="5" <?= func_decode($grade_no, '5', 'selected', '') ?>>⑤등급</option>
								<option value="6" <?= func_decode($grade_no, '6', 'selected', '') ?>>⑥등급</option>
								<option value="7" <?= func_decode($grade_no, '7', 'selected', '') ?>>⑦등급</option>
								<option value="8" <?= func_decode($grade_no, '8', 'selected', '') ?>>⑧등급</option>
								<option value="9" <?= func_decode($grade_no, '9', 'selected', '') ?>>⑨등급</option>
								<option value="10" <?= func_decode($grade_no, '10', 'selected', '') ?>>⑩등급</option>
								<option value="11" <?= func_decode($grade_no, '11', 'selected', '') ?>>⑪등급</option>
								<option value="12" <?= func_decode($grade_no, '12', 'selected', '') ?>>⑫등급</option>
								<option value="13" <?= func_decode($grade_no, '13', 'selected', '') ?>>⑬등급</option>
								<option value="14" <?= func_decode($grade_no, '14', 'selected', '') ?>>⑭등급</option>
								<option value="15" <?= func_decode($grade_no, '15', 'selected', '') ?>>⑮등급</option>
							</select>
						</td>
						<th class="key">회원등급 명</th>
						<td class="value">
							<input type="text" name="values[grade_name]" id="grade_name" value="<?= $grade_name ?>" class="input_1" style="width:200px;" title="회원등급 명" onfocus="f_text(this);" onblur="b_text(this);" maxlength="10" />
						</td>
					</tr>
					<tr>
						<th class="key">회원등급 설명</th>
						<td class="value" colspan="3">
							<input type="text" name="values[grade_desc]" id="grade_desc" value="<?= $grade_desc ?>" class="input_1" style="width:400px;" title="회원등급 설명" onfocus="f_text(this);" onblur="b_text(this);" maxlength="50" />
						</td>
					</tr>
					<tr>
						<th class="key">회원기본 등급 설정</th>
						<td class="value" colspan="3">
							<input type="radio" name="values[default_yn]" id="default_yn" value="N" class="radio" <?= func_decode($default_yn, 'N', 'checked', 'checked') ?> /> 미설정
							<input type="radio" name="values[default_yn]" id="default_yn" value="Y" class="radio" <?= func_decode($default_yn, 'Y', 'checked', '') ?> /> 설정
							<span>ex) 체크시 사이트 회원가입시 해당 회원의 등급으로 설정 됩니다.</span>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="btn_area_center">
<? if ($exec_type != 'UPDATE') { ?>
					<input type="image" src="/images/admin/btn_regist.gif" title="등록" />
				<? } else { ?>
					<input type="image" src="/images/admin/btn_modify.gif" title="수정" />
					<a href="#" onclick="delete_member_grade(document.iwc_frm, '/?c=iwc'); return false;" title="삭제"><img src="/images/admin/btn_delete.gif" alt="삭제" /></a>
				<? } ?>

				<a href="/?c=iwc&amp;m=view_member_grade" title="취소"><img src="/images/admin/btn_cancel.gif" alt="취소" /></a>
			</div>
		</div>
	</div>
</form>