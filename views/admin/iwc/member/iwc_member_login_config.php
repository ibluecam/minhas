<script type="text/javascript">
	//회원가입 설정 정보 등록 체크
	function reg_member_login_config(frm) {

		if(!validate(frm.elements['values[member_login_skin]'], '회원 로그인 스킨을 선택 하세요.')) return false;

		//비밀번호 메일 발송
		if(frm.elements['values[passwd_mail_yn]'][1].checked) {
			if(!validate(frm.elements['values[mail_name]'], '보내는 사람을 입력하세요.')) return false;
			if(!validate(frm.elements['values[mail_addr]'], '보내는 이메일 주소를 입력하세요.')) return false;
		}


		return true;

	}
</script>
<form name="iwc_frm" id="iwc_frm" method="post" action="/?c=iwc&amp;m=exec_member_login_config" onsubmit="return reg_member_login_config(this)">
	<input type="hidden" name="member_login_mcd" id="mcd" value="<?= $member_login_mcd ?>" />
	<div class="contents_box_middle">
		<h1 class="title">회원 관리</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 사이트맵에 등록된 회원 로그인 메뉴의 기본정보를 설정한다.</li>
			</ul>
		</div>
		<div class="table_write_box">
			<h1 class="table_title">회원 로그인 설정</h1>
			<table class="table_write" summary="회원 로그인 설정" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="20%" /><col width="80%" />
				<tr>
					<th class="key_01">※ 로그인 메뉴코드</th>
					<td class="value_01" colspan="3">
						<a href="/?c=user&mcd=<?= $member_login_mcd ?>" title="바로가기" target="_blank"><span class="title">로그인 [<?= $member_login_mcd ?>]</span></a>
					</td>
				</tr>
				<tr>
					<th class="key_01">※ 로그인 스킨</th>
					<td class="value_01" colspan="3">
						<select name="values[member_login_skin]" id="member_login_skin" class="input_2" style="width:150px;">
							<option value="">:: 선택하세요. ::</option>
							<?
							$dir_default = array();
							$dir_default_path = 'views/user/member_login/skin';

							$dir_default = scandir($dir_default_path);

//회원 로그인
							for ($i = 0; $i < count($dir_default); $i++) {
//디렉토리만..
								if (!strstr($dir_default[$i], '.')) {
									?>
									<option value="<?= $dir_default[$i] ?>" <?= func_decode(func_get_config($member_config_result, 'member_login_skin'), $dir_default[$i], 'selected', '') ?>><?= $dir_default[$i] ?></option>
									<?
								}
							}
							?>
						</select>
						<?
//스킨생성이 되었다면
						if (func_get_config($member_config_result, 'member_login_skin') != '') {
							?>
							<input type="checkbox" name="skin_cover" value="Y" /> 덮어쓰기
<? } ?>
						<span> ?!) 해당 스킨 폴더가 생성 되어 있으면 덮어쓰지 않음.</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">비밀번호 메일 발송</th>
					<td class="value_01" colspan="3">
						<div style="padding-top:3px;">
							<input type="radio" name="values[passwd_mail_yn]" id="passwd_mail_yn" value="N" class="radio" <?= func_decode(func_get_config($member_config_result, 'passwd_mail_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[passwd_mail_yn]" id="passwd_mail_yn" value="Y" class="radio" <?= func_decode(func_get_config($member_config_result, 'passwd_mail_yn'), 'Y', 'checked', '') ?> /> 사용
							<span> ?!) 비밀번호 찾기 시 메일 발송여부</span>
						</div>
						<div style="padding-top:3px;">
							※ 보내는 사람:<input type="text" name="values[mail_name]" id="mail_name" value="<?= func_get_config($member_config_result, 'mail_name') ?>" class="input_1" style="width:120px;" title="관리자명" onfocus="f_text(this);" onblur="b_text(this);"  />
							※ 보내는 이메일:<input type="text" name="values[mail_addr]" id="mail_name" value="<?= func_get_config($member_config_result, 'mail_addr') ?>" class="input_1" style="width:120px;" title="관리자이메일" onfocus="f_text(this);" onblur="b_text(this);"  />
						</div>
					</td>
				</tr>
				<tr>
					<th class="key_01">상단 HTML</th>
					<td class="value_01" colspan="3" style="padding:10px;">
						<textarea name="values[member_login_html_header]" id="member_login_html_header" class="textarea_03" style="width:98%;height:100px;padding:3px;" onkeydown="useTab(this);"><?= func_get_config($member_config_result, 'member_login_html_header') ?></textarea>
						<span> ?!) 로그인 상단 HTML </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">하단 HTML</th>
					<td class="value_01" colspan="3" style="padding:10px;">
						<textarea name="values[member_login_html_footer]" id="member_login_html_footer" class="textarea_03" style="width:98%;height:50px;padding:3px;" onkeydown="useTab(this);"><?= func_get_config($member_config_result, 'member_login_html_footer') ?></textarea>
						<span> ?!) 로그인 하단 HTML </span>
					</td>
				</tr>
			</table>
			<div class="btn_area_center">
				<input type="image" src="/images/admin/btn_ok.gif" title="확인" />
				<a href="<?= $base_url ?>" title="취소"><img src="/images/admin/btn_cancel.gif" alt="취소" /></a>
			</div>

		</div>
	</div>
</form>