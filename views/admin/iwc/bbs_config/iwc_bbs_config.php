<script type="text/javascript">

	//게시판 설정 정보 복사
	function copy_bbs_config() {
		var f = document.iwc_frm;

		if(!confirm('설정을 복사 하시겠습니까?')) {
			f.bbs_config_copy.selectedIndex = 0;
			return;
		}
		else
		{
			f.action = '/?c=iwc&m=copy_bbs_config';
			f.submit();
		}
	}

	//게시판 설정 정보 등록 체크
	function reg_bbs_config(frm) {
		if(!validate(frm.elements['values[bbs_type]'], '게시판 유형을 선택하세요.')) return false;
		if(!validate(frm.elements['values[bbs_type_skin]'], '게시판 스킨을 선택하세요.')) return false;

		//썸네일 생성 여부
		if(frm.elements['values[attach_thumbnail_yn]'][1].checked) {
			//if(!validate(frm.elements['values[attach_thumbnail_size1]'], '기타(최소) 썸네일 이미지 가로 사이즈를 입력하세요.')) return false;
			if(!validate(frm.elements['values[attach_thumbnail_size2]'], '요약 썸네일 이미지 가로 사이즈를 입력하세요.')) return false;
			if(!validate(frm.elements['values[attach_thumbnail_size3]'], '목록 썸네일 이미지 가로 사이즈를 입력하세요.')) return false;
			if(!validate(frm.elements['values[attach_thumbnail_size4]'], '내용 썸네일 이미지 가로 사이즈를 입력하세요.')) return false;
			if(!validate(frm.elements['values[attach_thumbnail_size5]'], '기타(최대) 썸네일 이미지 가로 사이즈를 입력하세요.')) return false;
		}

		return true;

	}

	//카테고리 추가(사용안함) -20101011
	function category_add() {
		var f = document.iwc_frm;

		if(!validate(f.elements['values[category_cnt]'], '카테고리 수를 입력하세요.')) return;
		if(f.elements['values[category_cnt]'].value == '0') {
			alert('카테고리 수를 입력하세요.');
			f.elements['values[category_cnt]'].focus();
			return;
		}

		var pre_category_cnt = '<?= func_get_config($bbs_config_result, "category_cnt") ?>';
		if(pre_category_cnt == '') pre_category_cnt = 0;

		var category_cnt = f.elements['values[category_cnt]'].value;
		var category_area = document.getElementById('category');
		var t_html = '';

		//카테고리 수가 이미 저장 되어 있다면 -> UPDATE
		if(pre_category_cnt != 0) {
			for(i = parseInt(pre_category_cnt) + 1; i<=parseInt(parseInt(pre_category_cnt) + parseInt(category_cnt)); i++) {
				t_html +=   "※ 카테고리 코드(숫자)_" + i + " : ";
				t_html +=   "<input type=\"text\" name=\"values[category_code_" + i + "]\" id=\"category_code_" + i + "\" value=\"\" class=\"input_1\" style=\"width:35px;\" title=\"카테고리코드\" onfocus=\"f_text(this);\" onblur=\"b_text(this);\" onkeypress=\"onlyNum4();\" maxlength=\"4\" />";
				t_html +=   "※ 카테고리 명_" + i + " : ";
				t_html +=   "<input type=\"text\" name=\"values[category_name_" + i + "]\" id=\"category_name_" + i + "\" value=\"\" class=\"input_1\" style=\"width:70px;\" title=\"카테고리명\" onfocus=\"f_text(this);\" onblur=\"b_text(this);\" /><br/>";
			}
			category_area.innerHTML += t_html;
		}
		else
		{
			for(i=1; i<=parseInt(category_cnt); i++) {
				t_html +=   "※ 카테고리 코드(숫자)_" + i + " : ";
				t_html +=   "<input type=\"text\" name=\"values[category_code_" + i + "]\" id=\"category_code_" + i + "\" value=\"\" class=\"input_1\" style=\"width:35px;\" title=\"카테고리코드\" onfocus=\"f_text(this);\" onblur=\"b_text(this);\" onkeypress=\"onlyNum4();\" maxlength=\"4\" />";
				t_html +=   "※ 카테고리 명_" + i + " : ";
				t_html +=   "<input type=\"text\" name=\"values[category_name_" + i + "]\" id=\"category_name_" + i + "\" value=\"\" class=\"input_1\" style=\"width:70px;\" title=\"카테고리명\" onfocus=\"f_text(this);\" onblur=\"b_text(this);\" /><br/>";
			}
			category_area.innerHTML = t_html;
		}

	}

	//타입별 스킨 선택
	function bbs_type_change(val) {
		var f = document.iwc_frm;

		if(val == 'default') {
			f.elements['values[bbs_type_skin_1]'].style.display = 'inline';
			f.elements['values[bbs_type_skin_2]'].style.display = 'none';
		}
		if(val == 'album') {
			f.elements['values[bbs_type_skin_1]'].style.display = 'none';
			f.elements['values[bbs_type_skin_2]'].style.display = 'inline';
		}
	}

	//스킨 선택시
	function bbs_skin_select(val) {
		var f = document.iwc_frm;
		f.elements['values[bbs_type_skin]'].value = val;
	}

</script>
<form name="iwc_frm" id="iwc_frm" method="post" action="/?c=iwc&amp;m=exec_bbs_config" onsubmit="return reg_bbs_config(this);">
	<input type="hidden" name="mcd" id="mcd" value="<?= $mcd ?>" />
	<input type="hidden" name="mcd_name" id="mcd_name" value="<?= $mcd_name ?>" />
	<input type="hidden" name="values[bbs_code_name]" id="bbs_code_name" value="<?= $mcd_name ?>" />
	<input type="hidden" name="values[bbs_type_skin]" id="bbs_type_skin" value="<?= func_get_config($bbs_config_result, 'bbs_type_skin') ?>" />

	<div class="contents_box_middle">
		<h1 class="title">게시판 관리</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 사이트맵에 등록된 게시판 메뉴의 기본정보를 설정한다.</li>
			</ul>
		</div>
		<div class="table_write_box">
			<h1 class="table_title">게시판 설정</h1>
			<table class="table_write" summary="게시판 설정" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="20%" /><col width="30%" /><col width="20%" /><col width="30%" />
				<tr>
					<th class="key_01">※ 게시판 명 [코드]</th>
					<td class="value_01">
						<a href="/?c=user&mcd=<?= $mcd ?>" title="바로가기" target="_blank"><span class="title"><?= func_get_config($bbs_config_result, 'bbs_code_name', $mcd_name) ?> [<?= $mcd ?>]</span></a>
					</td>
					<th class="key_01">게시판 설정 복사</th>
					<td class="value_01">
						<select name="bbs_config_copy" id="bbs_copy" onchange="copy_bbs_config();" class="input_2" style="width:150px;">
							<option value="">:: 선택하세요. ::</option>
							<?
//사이트맵 정보
							if (count($site_map_list) > 0) {
								$menu_code = '';
								$menu_code_name = '';
								foreach ($site_map_list as $rows) {
									if ($rows->menu_type == 'bbs') {
										$menu_code = $rows->menu_code;
										$menu_code_name = $rows->menu_code_name;
										?>
										<option value="<?= $menu_code ?>"><?= $menu_code_name ?></option>
										<?
									}
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<th class="key_01">※ 게시판 유형</th>
					<td class="value_01" colspan="3">
						<select name="values[bbs_type]" id="bbs_type" class="input_2" style="width:100px;" onchange="bbs_type_change(this.value);">
							<option value="">:: 선택하세요. ::</option>
							<option value="default" <?= func_decode(func_get_config($bbs_config_result, 'bbs_type'), 'default', 'selected', '') ?>>일반형 타입</option>
							<option value="album" <?= func_decode(func_get_config($bbs_config_result, 'bbs_type'), 'album', 'selected', '') ?>>앨범형 타입</option>
						</select>
						<?
						$dir_default = array();
						$dir_album = array();
						$dir_default_path = 'views/user/bbs/default';
						$dir_album_path = 'views/user/bbs/album';

						$dir_default = scandir($dir_default_path);
						$dir_album = scandir($dir_album_path);
						?>
						<select name="values[bbs_type_skin_1]" id="bbs_type_skin_1" class="input_2" style="width:200px;display:none;" onchange="bbs_skin_select(this.value);">
							<option value="">:: 선택하세요. ::</option>
							<?
//일반형
							for ($i = 0; $i < count($dir_default); $i++) {
//디렉토리만..
								if (!strstr($dir_default[$i], '.')) {
									?>
									<option value="default:<?= $dir_default[$i] ?>" <?= func_decode(func_get_config($bbs_config_result, 'bbs_type_skin'), 'default:' . $dir_default[$i], 'selected', '') ?>><?= $dir_default[$i] ?></option>
									<?
								}
							}
							?>
						</select>
						<select name="values[bbs_type_skin_2]" id="bbs_type_skin_2" class="input_2" style="width:200px;display:none;" onchange="bbs_skin_select(this.value);">
							<option value="">:: 선택하세요. ::</option>
<?
//앨범형
for ($i = 0; $i < count($dir_album); $i++) {
//디렉토리만..
	if (!strstr($dir_album[$i], '.')) {
		?>
									<option value="album:<?= $dir_album[$i] ?>" <?= func_decode(func_get_config($bbs_config_result, 'bbs_type_skin'), 'album:' . $dir_album[$i], 'selected', '') ?>><?= $dir_album[$i] ?></option>
									<?
								}
							}
							?>
						</select>
						<?
//스킨생성이 되었다면
						if (func_get_config($bbs_config_result, 'bbs_type_skin') != '') {
							?>
							<input type="checkbox" name="skin_cover" value="Y" /> 덮어쓰기
						<? } ?>
						<span> ?!) 해당 스킨 폴더가 생성 되어 있으면 덮어쓰지 않음.</span>
						<script type="text/javascript">
<? if (func_get_config($bbs_config_result, 'bbs_type') == 'default') { ?>
								document.getElementById('bbs_type_skin_1').style.display = 'inline';
	<?
}
if (func_get_config($bbs_config_result, 'bbs_type') == 'album') {
	?>
								document.getElementById('bbs_type_skin_2').style.display = 'inline';
<? } ?>
						</script>
					</td>
				</tr>
				<tr>
					<th class="key_01">답변 기능</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[reply_yn]" id="reply_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'reply_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[reply_yn]" id="reply_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'reply_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 답변기능 사용시 자유게시판 형태, 미사용시 공지사항 형태 </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">답변 여부 표시</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[reply_exp_yn]" id="reply_exp_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'reply_exp_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[reply_exp_yn]" id="reply_exp_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'reply_exp_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 답변 등록시 게시판 리스트 페이지 답변 여부 표시 </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">댓글 기능(코멘트)</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[comment_yn]" id="comment_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'comment_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[comment_yn]" id="comment_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'comment_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 게시판 내용 화면 댓글 등록 여부</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">댓글시 답변 처리 여부</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[comment_reply_yn]" id="comment_reply_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'comment_reply_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[comment_reply_yn]" id="comment_reply_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'comment_reply_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 댓글 등록시 답변 처리 간주 여부(답변 등록과 같은 기능)</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">비밀글 기능</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[secret_yn]" id="secret_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'secret_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[secret_yn]" id="secret_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'secret_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 공개/비공개 기능 추가 여부</span>
					</td>
				</tr>
				<!--
				<tr>
				<th class="key_01">공개/비공개 생성</th>
				<td class="value_01" colspan="3">
				<input type="radio" name="values[open_yn]" id="open_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'open_yn'), 'N', 'checked', 'checked') ?> /> 미생성
				<input type="radio" name="values[open_yn]" id="open_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'open_yn'), 'Y', 'checked', '') ?> /> 생성
				<span> ?!) 게시판 글 등록시 공개/비공개 필드 생성 여부</span>
				</td>
				</tr>
				//-->
				<tr>
					<th class="key_01">페이지 줄 수</th>
					<td class="value_01">
						<input type="text" name="values[page_rows]" id="page_rows" value="<?= func_get_config($bbs_config_result, 'page_rows', '10') ?>" class="input_1" style="width:35px;" title="페이지 줄 수" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
						<span> ?!) 페이지당 표시 줄수</span>
					</td>
					<th class="key_01">페이지 표지 수</th>
					<td class="value_01">
						<input type="text" name="values[page_views]" id="page_views" value="<?= func_get_config($bbs_config_result, 'page_views', '10') ?>" class="input_1" style="width:35px;" title="페이지(하단) 표지 수" onfocus="f_text(this);" onblur="b_text(this);"  onkeypress="onlyNum4();" maxlength="4" />
						<span> ?!) 페이지 하단 네비게이션 수</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">New Icon 표시</th>
					<td class="value_01">
						<input type="text" name="values[icon_new]" id="icon_new" value="<?= func_get_config($bbs_config_result, 'icon_new', '2') ?>" class="input_1" style="width:35px;" title="New Icon 표시" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
						<span> ?!) New 아이콘 표시 기준 일자</span>
					</td>
					<th class="key_01">Hot Icon 표시</th>
					<td class="value_01">
						<input type="text" name="values[icon_hot]" id="icon_hot" value="<?= func_get_config($bbs_config_result, 'icon_hot', '9999') ?>" class="input_1" style="width:35px;" title="Hot Icon 표시" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
						<span> ?!) Hot 아이콘 표시 기준 조회수</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">날짜 포맷</th>
					<td class="value_01" colspan="3">
						<input type="text" name="values[date_format]" id="date_format" value="<?= func_get_config($bbs_config_result, 'date_format', 'Y-m-d') ?>" class="input_1" style="width:120px;" title="날짜 포맷" onfocus="f_text(this);" onblur="b_text(this);"  />
						<span> ?!) 게시판에 표시될 날짜의 포맷 지정 (2010-02-08 18:06:16 => Y-m-d H:i:s) </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">게시판 Index</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[bbs_index]" id="bbs_index" value="L" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'bbs_index'), 'L', 'checked', 'checked') ?> /> 목록 화면
						<!--<input type="radio" name="values[bbs_index]" id="bbs_index" value="V" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'bbs_index'), 'V', 'checked', '') ?> /> 내용 화면-->
						<input type="radio" name="values[bbs_index]" id="bbs_index" value="R" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'bbs_index'), 'R', 'checked', '') ?> /> 등록 화면
						<span> ?!) 게시판 로딩시 목록/등록 화면 INDEX 여부 </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">캘린더(리스트)</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[calendar_yn]" id="calendar_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'calendar_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[calendar_yn]" id="calendar_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'calendar_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 게시판 리스트 캘린더 사용 여부(캘린더 스킨을 선택하세요.)</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">설문조사</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[survey_yn]" id="survey_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'survey_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[survey_yn]" id="survey_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'survey_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 게시판 설문조사 사용 여부(설문조사 스킨을 선택하세요.)</span>
						<div style="padding:3px; border:1px solid red;">
							※중복응모 방지(IP) <input type="radio" name="values[survey_dup_ip_yn]" id="survey_dup_ip_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'survey_dup_ip_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[survey_dup_ip_yn]" id="survey_dup_ip_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'survey_dup_ip_yn'), 'Y', 'checked', '') ?> /> 사용
							<br/>
							※중복응모 방지(쿠키) <input type="radio" name="values[survey_dup_ck_yn]" id="survey_dup_ck_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'survey_dup_ck_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[survey_dup_ck_yn]" id="survey_dup_ck_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'survey_dup_ck_yn'), 'Y', 'checked', '') ?> /> 사용
							<br/>
							※중복응모 방지(아이디) <input type="radio" name="values[survey_dup_id_yn]" id="survey_dup_id_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'survey_dup_id_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[survey_dup_id_yn]" id="survey_dup_id_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'survey_dup_id_yn'), 'Y', 'checked', '') ?> /> 사용
							<br/>
							※응모권한 : <select name="values[member_grade_survey_entry]" id="member_grade_survey_entry" class="input_2">
								<option value="">:: 회원전체 ::</option>
								<? //회원권한정보
								if (count($member_grade_result) > 0) {
									foreach ($member_grade_result as $rows) {
										$conf = explode('|', func_get_config($bbs_config_result, 'member_grade_survey_entry'));
										?>
										<option value="<?= $rows->grade_no ?>|<?= $rows->grade_name ?>" <?= func_decode($conf[0], $rows->grade_no, 'selected', '') ?>><?= $rows->grade_name ?></option>
										<?
									}
								}
								?>
							</select>
							<span> ?!) 하단의 "회원권한 고정(fix)" 설정의 영향을 받음</span>
							<br/>
							※결과화면 <input type="radio" name="values[survey_result_yn]" id="survey_result_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'survey_result_yn'), 'N', 'checked', 'checked') ?> /> 응모 종료후 보임
							<input type="radio" name="values[survey_result_yn]" id="survey_result_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'survey_result_yn'), 'Y', 'checked', '') ?> /> 항상 보임
							<br/>
							※결과권한 : <select name="values[member_grade_survey_result]" id="member_grade_survey_result" class="input_2">
								<option value="">:: 회원전체 ::</option>
								<? //회원권한정보
								if (count($member_grade_result) > 0) {
									foreach ($member_grade_result as $rows) {
										$conf = explode('|', func_get_config($bbs_config_result, 'member_grade_survey_result'));
										?>
										<option value="<?= $rows->grade_no ?>|<?= $rows->grade_name ?>" <?= func_decode($conf[0], $rows->grade_no, 'selected', '') ?>><?= $rows->grade_name ?></option>
										<?
									}
								}
								?>
							</select>
							<span> ?!) 하단의 "회원권한 고정(fix)" 설정의 영향을 받음</span>
						</div>
					</td>
				</tr>
				<tr>
					<th class="key_01">제목 길이 제한</th>
					<td class="value_01" colspan="3">
						<input type="text" name="values[subject_limit]" id="subject_limit" value="<?= func_get_config($bbs_config_result, 'subject_limit', '0') ?>" class="input_1" style="width:35px;" title="제목 길이 제한" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="2" />
						<span> ?!) 게시판 리스트 제목 길이 0 이면 무제한</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">HTML Editor</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[editor_yn]" id="editor_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'editor_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[editor_yn]" id="editor_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'editor_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) HTML Editor 사용 여부 </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">이전글 / 다음글</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[pre_next_view_yn]" id="pre_next_view_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'pre_next_view_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[pre_next_view_yn]" id="pre_next_view_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'pre_next_view_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 게시판 내용 화면에 이전글 다음글 표시 여부 </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">첨부파일</th>
					<td class="value_01" colspan="3">
						<div style="padding-top:3px;">
							<input type="radio" name="values[attach_file_yn]" id="attach_file_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'attach_file_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[attach_file_yn]" id="attach_file_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'attach_file_yn'), 'Y', 'checked', '') ?> /> 사용
							<span> ?!) 글 등록시 첨부 파일 사용 여부 </span>
						</div>
						<div style="padding-top:3px;">
							※ 첨부파일 개수 :
							<input type="text" name="values[attach_file_cnt]" id="attach_file_cnt" value="<?= func_get_config($bbs_config_result, 'attach_file_cnt', '1') ?>" class="input_1" style="width:20px;" title="첨부파일갯수" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="2" />
							<input type="text" name="values[attach_file_allow]" id="attach_file_allow" value="<?= func_get_config($bbs_config_result, 'attach_file_allow', 'doc|xls|ppt|hwp|docx|xlsx|pptx|pdf|zip|txt|jpg|gif|png') ?>" class="input_1" style="width:350px;" title="첨부파일허용확장자" onfocus="f_text(this);" onblur="b_text(this);" maxlength="100" />
							<span> ?!) 첨부파일 허용 확장자</span>
						</div>
						<div style="padding:3px; border:1px solid red;">
							※ 썸네일 생성여부 :
							<input type="radio" name="values[attach_thumbnail_yn]" id="attach_thumbnail_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'attach_thumbnail_yn'), 'N', 'checked', 'checked') ?> /> 미생성
							<input type="radio" name="values[attach_thumbnail_yn]" id="attach_thumbnail_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'attach_thumbnail_yn'), 'Y', 'checked', '') ?> /> 생성
							<span> ?!) 첨부파일이 이미지(GD2) -> (GD, GD2, ImageMagick, NetPBM 라이브러리 세팅) </span><br/>

							※ 썸네일 크기 :
							①.기타(최소) : <input type="text" name="values[attach_thumbnail_size1]" id="attach_thumbnail_size1" value="<?= func_get_config($bbs_config_result, 'attach_thumbnail_size1', '') ?>" class="input_1" style="width:35px;" title="썸네일크기" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
							②.요약 :       <input type="text" name="values[attach_thumbnail_size2]" id="attach_thumbnail_size2" value="<?= func_get_config($bbs_config_result, 'attach_thumbnail_size2', '') ?>" class="input_1" style="width:35px;" title="썸네일크기" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
							③.목록 :       <input type="text" name="values[attach_thumbnail_size3]" id="attach_thumbnail_size3" value="<?= func_get_config($bbs_config_result, 'attach_thumbnail_size3', '') ?>" class="input_1" style="width:35px;" title="썸네일크기" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
							④.내용 :       <input type="text" name="values[attach_thumbnail_size4]" id="attach_thumbnail_size4" value="<?= func_get_config($bbs_config_result, 'attach_thumbnail_size4', '') ?>" class="input_1" style="width:35px;" title="썸네일크기" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
							⑤.기타(최대) : <input type="text" name="values[attach_thumbnail_size5]" id="attach_thumbnail_size5" value="<?= func_get_config($bbs_config_result, 'attach_thumbnail_size5', '') ?>" class="input_1" style="width:35px;" title="썸네일크기" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" /><br/>

							※ 워터마크 생성여부 :
							<input type="radio" name="values[attach_watermaking_yn]" id="attach_watermaking_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'attach_watermaking_yn'), 'N', 'checked', 'checked') ?> /> 미생성
							<input type="radio" name="values[attach_watermaking_yn]" id="attach_watermaking_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'attach_watermaking_yn'), 'Y', 'checked', '') ?> /> 생성
							<span> ?!) 텍스트-> 삽입 문구, 폰트색상-> 예)ffffff, 폰트크기-> 예)1~5</span><br/>

							※ 워터마크 속성 :
							①.텍스트 :   <input type="text" name="values[attach_watermaking_text]" id="attach_watermaking_text" value="<?= func_get_config($bbs_config_result, 'attach_watermaking_text', '') ?>" class="input_1" style="width:120px;" title="워터마크 텍스트" onfocus="f_text(this);" onblur="b_text(this);" maxlength="50" />
							②.폰트색상 : <input type="text" name="values[attach_watermaking_font_color]" id="attach_watermaking_font_color" value="<?= func_get_config($bbs_config_result, 'attach_watermaking_font_color', '') ?>" class="input_1" style="width:40px;" title="폰트색상" onfocus="f_text(this);" onblur="b_text(this);" maxlength="6" />
							②.폰트크기 : <input type="text" name="values[attach_watermaking_font_size]" id="attach_watermaking_font_size" value="<?= func_get_config($bbs_config_result, 'attach_watermaking_font_size', '') ?>" class="input_1" style="width:40px;" title="폰트크기" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="2" />
						</div>
					</td>
				</tr>
				<!--
				<tr>
				<th class="key_01">Summay 메인 노출</th>
				<td class="value_01" colspan="3">
				<input type="radio" name="values[main_summary_yn]" id="main_summary_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'main_summary_yn'), 'N', 'checked', 'checked') ?> /> 미사용
				<input type="radio" name="values[main_summary_yn]" id="main_summary_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'main_summary_yn'), 'Y', 'checked', '') ?> /> 사용
				<span> ?!) 관리자 게시판 등록시 해당글 메인 노출 사용 여부</span>
				</td>
				</tr>
				//-->
				<tr>
					<th class="key_01">Summay 사용</th>
					<td class="value_01" colspan="3">
						<div style="padding-top:3px;">
							<input type="radio" name="values[summary_yn]" id="summary_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'summary_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[summary_yn]" id="summary_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'summary_yn'), 'Y', 'checked', '') ?> /> 사용
							<span> ?!) 해당 게시판 메인 써머리 사용 여부</span>
						</div>
						<div style="padding-top:3px;">
							※ 표시줄수:
							<input type="text" name="values[summary_page_rows]" id="summary_page_rows" value="<?= func_get_config($bbs_config_result, 'summary_page_rows', '5') ?>" class="input_1" style="width:35px;" title="페이지 줄 수" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
							※ 제목길이:
							<input type="text" name="values[summary_subject_limit]" id="summary_subject_limit" value="<?= func_get_config($bbs_config_result, 'summary_subject_limit', '0') ?>" class="input_1" style="width:35px;" title="제목 길이 제한" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="2" />
							※ 날짜포맷:
							<input type="text" name="values[summary_date_format]" id="summary_date_format" value="<?= func_get_config($bbs_config_result, 'summary_date_format', 'Y-m-d') ?>" class="input_1" style="width:80px;" title="날짜 포맷" onfocus="f_text(this);" onblur="b_text(this);"  />
							※ New Icon:
							<input type="text" name="values[summary_icon_new]" id="summary_icon_new" value="<?= func_get_config($bbs_config_result, 'summary_icon_new', '2') ?>" class="input_1" style="width:35px;" title="New Icon 표시" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
							※ Hot Icon:
							<input type="text" name="values[summary_icon_hot]" id="summary_icon_hot" value="<?= func_get_config($bbs_config_result, 'summary_icon_hot', '9999') ?>" class="input_1" style="width:35px;" title="Hot Icon 표시" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
						</div>
					</td>
				</tr>
				<tr>
					<th class="key_01">카테고리 설정</th>
					<td class="value_01" colspan="3">
						<div style="padding-top:3px;">
							<input type="radio" name="values[category_yn]" id="category_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'category_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[category_yn]" id="category_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'category_yn'), 'Y', 'checked', '') ?> /> 사용
							<!--
							※ 카테고리 수 :
							<input type="text" name="values[category_cnt]" id="category_cnt" value="<?= func_get_config($bbs_config_result, 'category_cnt') ?>" class="input_1" style="width:35px;" title="카테고리 수" onfocus="f_text(this);" onblur="b_text(this);" onkeypress="onlyNum4();" maxlength="4" />
							<a href="#" onclick="category_add(); return false;" title="추가"><span style="font-weight:bold;font-size:12px;color:red;">[추가]</span></a>
							// -->
							<span style="font-weight:bold;font-size:12px;color:red;">※ 카테고리 추가시 '|'(파이프) 분리</span>
						</div>
						<div id="category" style="padding-top:3px;">
							※ 카테고리 제목 :
							<input type="text" name="values[category_title]" id="category_title" value="<?= func_get_config($bbs_config_result, 'category_title') ?>" class="input_1" style="width:80px;" title="카테고리 타이틀" onfocus="f_text(this);" onblur="b_text(this);"  /><br/>
							※ 카테고리 코드 :
							<input type="text" name="values[category_code]" id="category_code" value="<?= func_get_config($bbs_config_result, 'category_code') ?>" class="input_1" style="width:500px;" title="카테고리 코드(숫자)" onfocus="f_text(this);" onblur="b_text(this);" maxlength="50" /><br/>
							※ 카테고리 명 :&nbsp;&nbsp;&nbsp;
							<input type="text" name="values[category_name]" id="category_name" value="<?= func_get_config($bbs_config_result, 'category_name') ?>" class="input_1" style="width:500px;" title="카테고리 명" onfocus="f_text(this);" onblur="b_text(this);" />

							<?
							/* 사용안함
							  //카테고리 수
							  $tmp_cnt = func_get_config($bbs_config_result, 'category_cnt');

							  if((integer)$tmp_cnt > 0) {
							  for($i=1; $i<=(integer)$tmp_cnt; $i++) {
							  $category_code = func_get_config($bbs_config_result, "category_code_".$i);
							  $category_name = func_get_config($bbs_config_result, "category_name_".$i);
							  echo "※ 카테고리 코드(숫자)_".$i." : ";
							  echo "<input type=\"text\" name=\"values[category_code_".$i."]\" id=\"category_code_".$i."\" value=\"".$category_code."\" class=\"input_1\" style=\"width:35px;\" title=\"카테고리코드\" onfocus=\"f_text(this);\" onblur=\"b_text(this);\" onkeypress=\"onlyNum4();\" maxlength=\"4\" />";
							  echo "※ 카테고리 명_".$i." : ";
							  echo "<input type=\"text\" name=\"values[category_name_".$i."]\" id=\"category_name_".$i."\" value=\"".$category_name."\" class=\"input_1\" style=\"width:70px;\" title=\"카테고리명\" onfocus=\"f_text(this);\" onblur=\"b_text(this);\" /><br/>";
							  }
							  }
							 */
							?>
						</div>
						<span>예) 카테고리 코드(숫자) : 0001|0002, 카테고리 명 : 답변대기|답변완료</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">답변 아이디</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[reply_id_yn]" id="reply_id_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'reply_id_yn'), 'N', 'checked', 'checked') ?> /> 자신
						<input type="radio" name="values[reply_id_yn]" id="reply_id_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'reply_id_yn'), 'Y', 'checked', '') ?> /> 부모
						<span> ?!) 답변 등록시 member_id -> 부모 회원 아이디 등록 또는 자신 아이디 등록 여부 </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">자신 글(리스트)</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[member_list_yn]" id="member_list_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'member_list_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[member_list_yn]" id="member_list_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'member_list_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 본인 등록 글만 게시판 리스트에 표시 여부</span>
					</td>
				</tr>
				<tr>
					<th style="background-color:#ededed" colspan="4">부가 설정</th>
				</tr>
				<tr>
					<th class="key_01">사용자 글 등록(E-MAIL)</th>
					<td class="value_01" colspan="3">
						<div style="padding-top:3px;">
							<input type="radio" name="values[user_email_yn]" id="user_email_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'user_email_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[user_email_yn]" id="user_email_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'user_email_yn'), 'Y', 'checked', '') ?> /> 사용
							<span> ?!) 사용자 글 등록시 관리자에게 E-MAIL 발송 여부 </span>
						</div>
						<div style="padding-top:3px;">
							<input type="text" name="values[user_email_addr]" id="user_email_addr" value="<?= func_get_config($bbs_config_result, 'user_email_addr', '') ?>" class="input_1" style="width:400px;" title="관리자이메일" onfocus="f_text(this);" onblur="b_text(this);"  />
							<span> ?!) 관리자 이메일 주소 | 구분 </span>
						</div>
					</td>
				</tr>
				<tr>
					<th class="key_01">사용자 글 등록(SMS)</th>
					<td class="value_01" colspan="3">
						<div style="padding-top:3px;">
							<input type="radio" name="values[user_sms_yn]" id="user_sms_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'user_sms_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[user_sms_yn]" id="user_sms_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'user_sms_yn'), 'Y', 'checked', '') ?> /> 사용
							<span> ?!) 사용자 글 등록시 관리자에게 SMS 전송 여부 </span>
						</div>
						<div style="padding-top:3px;">
							①.<input type="text" name="values[user_mobile_no1_1]" id="user_mobile_no1_1" value="<?= func_get_config($bbs_config_result, 'user_mobile_no1_1', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
							<input type="text" name="values[user_mobile_no2_1]" id="user_mobile_no2_1" value="<?= func_get_config($bbs_config_result, 'user_mobile_no2_1', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
							<input type="text" name="values[user_mobile_no3_1]" id="user_mobile_no3_1" value="<?= func_get_config($bbs_config_result, 'user_mobile_no3_1', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" />
							②.<input type="text" name="values[user_mobile_no1_2]" id="user_mobile_no1_2" value="<?= func_get_config($bbs_config_result, 'user_mobile_no1_2', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
							<input type="text" name="values[user_mobile_no2_2]" id="user_mobile_no2_2" value="<?= func_get_config($bbs_config_result, 'user_mobile_no2_2', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
							<input type="text" name="values[user_mobile_no3_2]" id="user_mobile_no3_2" value="<?= func_get_config($bbs_config_result, 'user_mobile_no3_2', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" />
							<span> ?!) 관리자 핸드폰 번호</span>
						</div>
					</td>
				</tr>
				<tr>
					<th class="key_01">관리자 글 답변(E-MAIL)</th>
					<td class="value_01" colspan="3">
						<div style="padding-top:3px;">
							<input type="radio" name="values[admin_email_yn]" id="admin_email_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'admin_email_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[admin_email_yn]" id="admin_email_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'admin_email_yn'), 'Y', 'checked', '') ?> /> 사용
							<span> ?!) 관리자 글 답변시 사용자에게 E-MAIL 발송 여부 </span>
						</div>
						<div style="padding-top:3px;">
							※ 보내는 사람:<input type="text" name="values[admin_email_name]" id="admin_email_addr" value="<?= func_get_config($bbs_config_result, 'admin_email_name', '') ?>" class="input_1" style="width:120px;" title="관리자명" onfocus="f_text(this);" onblur="b_text(this);"  />
							※ 보내는 이메일:<input type="text" name="values[admin_email_addr]" id="admin_email_addr" value="<?= func_get_config($bbs_config_result, 'admin_email_addr', '') ?>" class="input_1" style="width:120px;" title="관리자이메일" onfocus="f_text(this);" onblur="b_text(this);"  />
							<span> ?!) 관리자 대표 보내는 사람 및 이메일 </span>
						</div>
					</td>
				</tr>
				<tr>
					<th class="key_01">관리자 글 답변(SMS)</th>
					<td class="value_01" colspan="3">
						<div style="padding-top:3px;">
							<input type="radio" name="values[admin_sms_yn]" id="admin_sms_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'admin_sms_yn'), 'N', 'checked', 'checked') ?> /> 미사용
							<input type="radio" name="values[admin_sms_yn]" id="admin_sms_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'admin_sms_yn'), 'Y', 'checked', '') ?> /> 사용
							<span> ?!) 관리자 글 답변시 사용자에게 SMS 발송 여부 </span>
						</div>
						<div style="padding-top:3px;">
							<input type="text" name="values[admin_mobile_no1]" id="admin_mobile_no1" value="<?= func_get_config($bbs_config_result, 'admin_mobile_no1', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
							<input type="text" name="values[admin_mobile_no2]" id="admin_mobile_no2" value="<?= func_get_config($bbs_config_result, 'admin_mobile_no2', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
							<input type="text" name="values[admin_mobile_no3]" id="admin_mobile_no3" value="<?= func_get_config($bbs_config_result, 'admin_mobile_no3', '') ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" />
							<span> ?!) 관리자 대표 회신 번호 </span>
						</div>
					</td>
				</tr>
				<tr>
					<th class="key_01">상단 HTML</th>
					<td class="value_01" colspan="3" style="padding:10px;">
						<textarea name="values[bbs_html_header]" id="bbs_html_header" class="textarea_03" style="width:98%;height:100px;padding:5px;" onkeydown="useTab(this);"><?= func_get_config($bbs_config_result, 'bbs_html_header') ?></textarea><br/>
						<span> ?!) 게시판 상단 HTML </span>
					</td>
				</tr>
				<tr>
					<th class="key_01">하단 HTML</th>
					<td class="value_01" colspan="3" style="padding:10px;">
						<textarea name="values[bbs_html_footer]" id="bbs_html_footer" class="textarea_03" style="width:98%;height:50px;padding:3px;" onkeydown="useTab(this);"><?= func_get_config($bbs_config_result, 'bbs_html_footer') ?></textarea><br/>
						<span> ?!) 게시판 하단 HTML </span>
					</td>
				</tr>
				<tr>
					<th style="background-color:#ededed" colspan="4">회원권한 설정</th>
				</tr>
				<tr>
					<th class="key_01">회원권한 설정</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[member_grade_yn]" id="member_grade_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'member_grade_yn'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[member_grade_yn]" id="member_grade_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'member_grade_yn'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 회원등급 연동 여부 (관리자 등급은 1등급)</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">회원권한 고정(fix)</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[member_grade_fix]" id="member_grade_fix" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'member_grade_fix'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[member_grade_fix]" id="member_grade_fix" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'member_grade_fix'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 회원권한 설정 후 해당 권한만 설정할 경우</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">로그인 페이지</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[member_login_return_url]" id="member_login_return_url" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'member_login_return_url'), 'N', 'checked', 'checked') ?> /> 미사용
						<input type="radio" name="values[member_login_return_url]" id="member_login_return_url" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'member_login_return_url'), 'Y', 'checked', '') ?> /> 사용
						<span> ?!) 회원권한 체크 후 로그인 페이지 Rediect 여부</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">권한별 버튼 노출</th>
					<td class="value_01" colspan="3">
						<input type="radio" name="values[member_grade_button_yn]" id="member_grade_button_yn" value="N" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'member_grade_button_yn'), 'N', 'checked', 'checked') ?> /> 무조건
						<input type="radio" name="values[member_grade_button_yn]" id="member_grade_button_yn" value="Y" class="radio" <?= func_decode(func_get_config($bbs_config_result, 'member_grade_button_yn'), 'Y', 'checked', '') ?> /> 권한별
						<span> ?!) 회원권한 체크후 버튼 노출</span>
					</td>
				</tr>
				<tr>
					<th class="key_01">회원권한 적용</th>
					<td class="value_01" colspan="3">
						<div style="padding:3px 0px;">
							※목록 : <select name="values[member_grade_list]" id="member_grade_list" class="input_2">
								<option value="">:: 회원전체 ::</option>
								<? //회원권한정보
								if (count($member_grade_result) > 0) {
									foreach ($member_grade_result as $rows) {
										$conf = explode('|', func_get_config($bbs_config_result, 'member_grade_list'));
										?>
										<option value="<?= $rows->grade_no ?>|<?= $rows->grade_name ?>" <?= func_decode($conf[0], $rows->grade_no, 'selected', '') ?>><?= $rows->grade_name ?></option>
										<?
									}
								}
								?>
							</select>
							※내용 : <select name="values[member_grade_view]" id="member_grade_view" class="input_2">
								<option value="">:: 회원전체 ::</option>
								<? //회원권한정보
								if (count($member_grade_result) > 0) {
									foreach ($member_grade_result as $rows) {
										$conf = explode('|', func_get_config($bbs_config_result, 'member_grade_view'));
										?>
										<option value="<?= $rows->grade_no ?>|<?= $rows->grade_name ?>" <?= func_decode($conf[0], $rows->grade_no, 'selected', '') ?>><?= $rows->grade_name ?></option>
										<?
									}
								}
								?>
							</select>
							※등록 : <select name="values[member_grade_write]" id="member_grade_write" class="input_2">
								<option value="">:: 회원전체 ::</option>
								<? //회원권한정보
								if (count($member_grade_result) > 0) {
									foreach ($member_grade_result as $rows) {
										$conf = explode('|', func_get_config($bbs_config_result, 'member_grade_write'));
										?>
										<option value="<?= $rows->grade_no ?>|<?= $rows->grade_name ?>" <?= func_decode($conf[0], $rows->grade_no, 'selected', '') ?>><?= $rows->grade_name ?></option>
										<?
									}
								}
								?>
							</select>
							※수정 : <select name="values[member_grade_update]" id="member_grade_update" class="input_2">
								<option value="">:: 회원전체 ::</option>
								<? //회원권한정보
								if (count($member_grade_result) > 0) {
									foreach ($member_grade_result as $rows) {
										$conf = explode('|', func_get_config($bbs_config_result, 'member_grade_update'));
										?>
										<option value="<?= $rows->grade_no ?>|<?= $rows->grade_name ?>" <?= func_decode($conf[0], $rows->grade_no, 'selected', '') ?>><?= $rows->grade_name ?></option>
										<?
									}
								}
								?>
							</select>

						</div>
						<div style="padding:3px 0px;">
							※삭제 : <select name="values[member_grade_delete]" id="member_grade_delete" class="input_2">
								<option value="">:: 회원전체 ::</option>
								<? //회원권한정보
								if (count($member_grade_result) > 0) {
									foreach ($member_grade_result as $rows) {
										$conf = explode('|', func_get_config($bbs_config_result, 'member_grade_delete'));
										?>
										<option value="<?= $rows->grade_no ?>|<?= $rows->grade_name ?>" <?= func_decode($conf[0], $rows->grade_no, 'selected', '') ?>><?= $rows->grade_name ?></option>
										<?
									}
								}
								?>
							</select>
							※답변 : <select name="values[member_grade_reply]" id="member_grade_reply" class="input_2">
								<option value="">:: 회원전체 ::</option>
								<? //회원권한정보
								if (count($member_grade_result) > 0) {
									foreach ($member_grade_result as $rows) {
										$conf = explode('|', func_get_config($bbs_config_result, 'member_grade_reply'));
										?>
										<option value="<?= $rows->grade_no ?>|<?= $rows->grade_name ?>" <?= func_decode($conf[0], $rows->grade_no, 'selected', '') ?>><?= $rows->grade_name ?></option>
										<?
									}
								}
								?>
							</select>
							※댓글 : <select name="values[member_grade_comment]" id="member_grade_comment" class="input_2">
								<option value="">:: 회원전체 ::</option>
								<? //회원권한정보
								if (count($member_grade_result) > 0) {
									foreach ($member_grade_result as $rows) {
										$conf = explode('|', func_get_config($bbs_config_result, 'member_grade_comment'));
										?>
										<option value="<?= $rows->grade_no ?>|<?= $rows->grade_name ?>" <?= func_decode($conf[0], $rows->grade_no, 'selected', '') ?>><?= $rows->grade_name ?></option>
										<?
									}
								}
								?>
							</select>
						</div>
						<span> ?!) 선택한 등급이상 권한부여, 단) 회원권한 고정 - 선택 등급만 권한부여)</span>
					</td>
				</tr>
			</table>
			<div class="btn_area_center">
				<input type="image" src="/images/admin/btn_ok.gif" title="확인" />
				<a href="<?= $base_url ?>" title="취소"><img src="/images/admin/btn_cancel.gif" alt="취소" /></a>
			</div>

		</div>
	</div>
</form>