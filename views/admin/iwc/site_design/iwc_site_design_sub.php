<script type="text/javascript">
	//HTML 미리보기
	function render() {
		var f = document.iwc_frm;
		final_view.document.body.innerHTML = iwc_frm.html_top_source.value + iwc_frm.html_source.value;
	}
	//HTML 소스 가져오기
	function get_html_source() {
		var f = document.iwc_frm;
		if(f.menu_code.value != '') {
			f.action = '/?c=iwc&m=view_site_design_sub_source';
			f.submit();
		}
	}
</script>
<form name="iwc_frm" id="iwc_frm" method="post" action="/?c=iwc&amp;m=exec_site_design_sub" onsubmit="">
	<div class="contents_box_middle">
		<h1 class="title">사이트 디자인 서브 페이지 HTML 설정</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li>- 사이트맵 생성시 [메뉴구분 - 일반컨텐츠]를 선택 등록한 서브 페이지 HTML 소스를 등록합니다.</li>
				<li>- 등록된 일반 컨텐츠 서브 페이지 경로</li>
				<li class="b">ex) 메뉴명:인사말, 메뉴코드:noa0001 - 경로 -> /views/user/contents/noa0001/noa0001.php</li>
				<li>- 사이트맵 등록시 컨텐츠 폴더 및 파일(메뉴코드.php)은 이미 생성 되어 있습니다.</li>
			</ul>
		</div>
		<div class="table_write_box">
			<h1 class="table_title">디자인 서브 페이지 HTML 설정</h1>
			<table class="table_write" summary="디자인 서브 페이지 HTML 설정" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="25%" /><col width="75%" />
				<thead>
					<tr>
						<th scope="col">메뉴명</th>
						<th scope="col">HTML</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td scope="row" align="left" style="vertical-align:top;padding-top:10px;">
							<select name="menu_code" id="menu_code" class="input_2" style="width:185px;height:835px" onchange="get_html_source();" multiple>
								<option value="">:: 선택하세요 ::</option>
								<?
								if (count($site_map_list) > 0) {
									$menu_code = '';
									$menu_code_name = '';
									$menu_url = '';

									foreach ($site_map_list as $rows) {
										if ($rows->menu_type == 'contents') {
											$blank_str = '';
											for ($i = 0; $i <= $rows->menu_code_level - 1; $i++) {
												$blank_str .= '&nbsp;&nbsp;';
											}
											$menu_code = $rows->menu_code;
											$menu_code_name = $rows->menu_code_name;
											$menu_url = $rows->menu_url;
											?>
											<option value="<?= $menu_code ?>" <?= func_decode($contents_menu_code, $menu_code, 'selected', '') ?>><?= $blank_str ?><?= $menu_code_name ?></option>
											<?
										}
									}
								}
								?>
							</select>
						</td>
						<td style="padding:5px;text-align:center;">
							<div style="padding:5px 0px;border:1px solid #FF7927">
								<p style="font-weight:bold;text-align:left;padding:3px;">- Html 상단</p>
								<textarea name="html_top_source" id="html_top_source" style="width:95%;height:100px;background-color:#FCFCC0;padding:5px;line-height:150%;" onkeyup="render()" onkeydown="render();useTab(this);"><?= $html_top_source ?></textarea>
								<p style="font-weight:bold;text-align:left;padding:3px;">- Html</p>
								<textarea name="html_source" id="html_source" style="width:95%;height:300px;background-color:#FCFCC0;padding:5px;line-height:150%;" onkeyup="render()" onkeydown="render();useTab(this);"><?= $html_source ?></textarea>
							</div>
							<div style="padding:5px 0px;border:1px solid #FF7927;margin-top:10px;">
								<div style="padding:5px 13px;text-align:left;">
									<a href="/?c=user&mcd=<?= $contents_menu_code ?>" target="_blank" title="미리보기"><img src="/images/admin/btn_view.gif" alt="미리보기" border="0" align="middle" /></a>
									(HTML 해더 및 스타일이 적용 안된 상태 입니다.)
								</div>
								<iframe name="final_view" id="final_view" width="95%" height="300px" marginwidth="2" marginheight="2" frameborder="0" style="border:1px solid #333333" scrolling="auto" ></iframe>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="btn_area_center">
				<input type="image" src="/images/admin/btn_ok.gif" title="확인" />
				<a href="<?= $base_url ?>"><img src="/images/admin/btn_cancel.gif" alt="취소" /></a>
			</div>

		</div>
	</div>
</form>
<script type="text/javascript">
	window.onload = function() {
		iwc_frm.html_source.onkeyup();
	}
</script>