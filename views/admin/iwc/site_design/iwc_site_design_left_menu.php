<form name="iwc_frm" id="iwc_frm" method="post" action="/?c=iwc&amp;m=exec_site_design_left_menu" onsubmit="">
	<div class="contents_box_middle">
		<h1 class="title">사이트 디자인 LEFT MENU 설정</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li>파일경로 : views/user/left_menu/left_menu.php</li>
				<li>아래 LEFT 메뉴 소스는 사이트 맵 생성 및 수정시 코드가 생성 됩니다.</li>
				<li class="b">참고사항 - 예를들어 메뉴그룹(회사소개) 코드 -> noa 이면  그 하위메뉴들의 메뉴코드에는 -> noa 포함(연혁 -> noa0001) 되어야 합니다.</li>
				<li class="b">위 기준대로 사이트맵을 작성하지 않은 경우 각각의 if 조건문에 OR 조건 -> 예를들어 || strstr($mcd, 'noa0001') 코드 삽입합니다.</li>
			</ul>
		</div>
		<div class="table_write_box">
			<h1 class="table_title">디자인 LEFT MENU 설정</h1>
			<table class="table_write" summary="디자인 LEFT MENU 설정" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="25%" /><col width="75%" />
				<thead>
					<tr>
						<th scope="col">HTML</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="padding:5px;text-align:center;">
							<div style="padding:5px 0px;border:1px solid #FF7927">
								<p style="font-weight:bold;text-align:left;padding:3px;">- LEFT MENU (파일경로 : views/user/left_menu/left_menu.php)</p>
								<textarea name="left_menu_source" id="left_menu_source" style="width:95%;height:800px;background-color:#FCFCC0;padding:5px;line-height:150%;" onkeydown="useTab(this);"><?= $left_menu_source ?></textarea>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="btn_area_center">
				<input type="image" src="/images/admin/btn_ok.gif" title="확인" />
				<a href="<?= $base_url ?>"><img src="/images/admin/btn_cancel.gif" alt="취소" /></a>
			</div>

		</div>
	</div>
</form>