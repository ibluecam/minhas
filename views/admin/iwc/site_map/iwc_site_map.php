<form name="iwc_frm" id="iwc_frm" method="post" action="/?c=iwc&amp;m=exec_site_map" onsubmit="return reg_sitemap(this);">
	<input type="hidden" name="exec_type" id="exec_type" value="<?= $exec_type ?>" />
	<input type="hidden" name="chk_menu_code" id="chk_menu_code" value="" />
	<input type="hidden" name="seq_no" id="seq_no" value="<?= $seq_no ?>" />
	<div class="contents_box_middle">
		<h1 class="title">사이트 맵 설정</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 등록된 각각의 메뉴는 해당 모듈들과 연계가 됩니다.</li>
				<li> - 메뉴 URL를 클릭하면 복사 됩니다.</li>
				<li> - 메뉴 등록시 하위메뉴와 공통적인 code가 되도록 입력하세요. ex) iwa(상위) -> iwa0001(하위), iwa0002(하위)</li>
			</ul>
		</div>
		<div class="table_list_box">
			<h1 class="table_title">사이트 맵</h1>
			<table class="table_list" summary="사이트 맵" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="" /><col width="" />
				<col width="" /><col width="" />
				<col width="" /><col width="" />
				<col width="" /><col width="" />
				<col width="" /><col width="" />
				<col width="" />
				<thead>
					<tr>
						<th scope="col">메뉴그룹</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">메뉴명</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">메뉴코드</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">메뉴구분</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">메뉴URL</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">메뉴레벨</th>
					</tr>
				</thead>
				<tbody>
					<?
					if (count($iw_site_map_result) > 0) {
						$menu_code_grp_name = ''; //상위 메뉴코드명
						$menu_code_name = ''; //메뉴코드명
						$menu_code = ''; //메뉴코드
						$menu_type = ''; //메뉴구분
						$menu_url = ''; //메뉴URL
						$menu_code_level = ''; //메뉴레벨
						foreach ($iw_site_map_result as $rows) {
							$blank_str = '';
							for ($i = 0; $i <= $rows->menu_code_level - 1; $i++) {
								$blank_str .= '&nbsp;&nbsp;';
							}

//최상위 메뉴이면...
							if ($rows->menu_code_level == '0') {
								$menu_code_grp_name = '<strong>' . $rows->menu_code_name . '</strong>';
								$menu_code_name = '<strong>' . $rows->menu_code_name . '</strong>';
								$menu_code = '<strong>' . $rows->menu_code . '</strong>';
								$menu_type = '<strong>' . $rows->menu_type . '</strong>';
								$menu_url = '<strong>' . $rows->menu_url . '</strong>';
								$menu_code_level = '<strong>' . ((int) ($rows->menu_code_level) + 1) . '단계</strong>';
							} else {
								$menu_code_grp_name = '&nbsp;';
								$menu_code_name = $rows->menu_code_name;
								$menu_code = $rows->menu_code;
								$menu_type = $rows->menu_type;
								$menu_url = $rows->menu_url;
								$menu_code_level = ((int) ($rows->menu_code_level) + 1) . '단계';
							}
							?>
							<tr>
								<td style="text-align:left;color:#e85716;padding:0px 0px 0px 5px;"><?= $menu_code_grp_name ?></td>
								<td class="line">&nbsp;</td>
								<td style="text-align:left;"><?= $blank_str ?><a href="#" onclick="select_sitemap(document.iwc_frm, '<?= $rows->seq_no ?>'); return false;" title="<?= $menu_code_name ?>"><?= $menu_code_name ?></a></td>
								<td class="line">&nbsp;</td>
								<td><a href="#" onclick="select_sitemap(document.iwc_frm, '<?= $rows->seq_no ?>'); return false;" title="<?= $menu_code ?>"><?= $menu_code ?></a></td>
								<td class="line">&nbsp;</td>
								<td><?= $menu_type ?></td>
								<td class="line">&nbsp;</td>
								<!-- 복사
								<td><a href="#" onclick="clipboard('<?= $rows->menu_url ?>'); return false;" title="<?= $menu_url ?>"><?= $menu_url ?></td>
								// -->
								<td><a href="<?= $rows->menu_url ?>" target="_blank" title="<?= $menu_url ?>"><?= $menu_url ?></td>
								<td class="line">&nbsp;</td>
								<td><?= $menu_code_level ?></td>
							</tr>
							<?
						}
					} else {
						?>
						<tr>
							<td colspan="20" class="no_data">Data does not exist.</td>
						</tr>
<? } ?>
				</tbody>
			</table>
		</div>
		<?
//사이트 맵 수정 시
		$menu_code = '';
		$menu_code_name = '';
		$menu_type = '';
		$menu_code_order = '';
		$menu_code_level = '';

		if ($exec_type == 'UPDATE') {
			if (count($iw_site_map_detail_result) > 0) {
				foreach ($iw_site_map_detail_result as $rows) {
					$menu_code = $rows->menu_code;
					$menu_code_name = $rows->menu_code_name;
					$menu_type = $rows->menu_type;
					$menu_code_order = $rows->menu_code_order;
					$menu_code_level = $rows->menu_code_level;
				}
			}
		}
		?>
		<div class="table_write_box">
			<h1 class="table_title">사이트 맵 설정</h1>
			<table class="table_write" summary="사이트 맵 설정" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
				<tbody>
					<tr>
						<th class="key">메뉴코드</th>
						<td class="value">
							<input type="text" name="values[menu_code]" id="menu_code" value="<?= func_decode($menu_code, '', '', $menu_code) ?>" class="input_1" style="width:120px;" title="메뉴코드" onfocus="f_text(this);" onblur="b_text(this);" maxlength="20" <?= func_decode($exec_type, 'INSERT', '', 'readonly') ?> />
<? if ($exec_type == 'INSERT') { ?>
								<a href="#" onclick="dup_chk_menu_code(); return false;" title="메뉴코드 중복 확인"><img src="/images/admin/btn_id_check.gif" alt="메뉴코드 중복 확인" align="middle" /></a>
								<span>ex) iwa0001</span>
<? } ?>
						</td>
						<th class="key">메뉴명</th>
						<td class="value">
							<input type="text" name="values[menu_code_name]" id="menu_code_name" value="<?= func_decode($menu_code_name, '', '', $menu_code_name) ?>" class="input_1" style="width:120px;" title="메뉴코드명" onfocus="f_text(this);" onblur="b_text(this);" maxlength="50" />
							<span>ex) 회사소개</span>
						</td>
					</tr>
					<tr>
						<th class="key">메뉴구분</th>
						<td class="value" colspan="3">
							<select name="values[menu_type]" id="menu_type" class="input_2" style="width:150px;">
								<option value="">:: 선택하세요 ::</option>
								<option value="contents" <?= func_decode($menu_type, 'contents', 'selected', '') ?>>컨텐츠</option>
								<option value="member_login" <?= func_decode($menu_type, 'member_login', 'selected', '') ?>>회원관리 - 로그인</option>
								<option value="member_join" <?= func_decode($menu_type, 'member_join', 'selected', '') ?>>회원관리 - 회원가입</option>
								<option value="bbs" <?= func_decode($menu_type, 'bbs', 'selected', '') ?>>게시판관리</option>
								<option value="etc" <?= func_decode($menu_type, 'etc', 'selected', '') ?>>기타(custom)</option>
								<!--<option value="calendar" <?= func_decode($menu_type, 'calendar', 'selected', '') ?>>일정관리</option>-->
								<!--<option value="pay" <?= func_decode($menu_type, 'pay', 'selected', '') ?>>결제관리</option>-->
							</select>
							<span>ex) 각각의 메뉴 구분을 선택하면 해당 모듈과 연계됩니다. "기타" - 모듈 직접 연계 </span>
						</td>
					</tr>
					<tr>
						<th class="key">메뉴위치/레벨</th>
						<td class="value" colspan="3">
							<input type="hidden" name="values[menu_code_order]" id="menu_code_order" value="<?= func_decode($menu_code_order, '', '', $menu_code_order) ?>" />
							<select name="values[menu_code_grp]" id="menu_code_grp" class="input_2" style="width:150px;">
								<option value="">:: 위치를 선택하세요 ::</option>
								<option value="0">메뉴그룹</option>
								<?
								if (count($iw_site_map_result) > 0) {
									foreach ($iw_site_map_result as $rows) {
//if($rows->menu_code_level == '0')
//{
										$blank_str = '';
										for ($i = 0; $i <= $rows->menu_code_level - 1; $i++) {
											$blank_str .= '&nbsp;&nbsp;';
										}
										?>
										<option value="<?= $rows->menu_code ?>"><?= $blank_str ?><?= $rows->menu_code_name ?></option>
										<?
//}
									}
								}
								?>
							</select>
							<select name="values[menu_code_level]" id="menu_code_level" class="input_2" style="width:70px;">
								<option value="">:: 레벨 ::</option>
								<? for ($i = 0; $i < 10; $i++) { ?>
									<option value="<?= $i ?>" <?= func_decode($menu_code_level, $i, 'selected', '') ?>><?= $i + 1 ?>단계</option>
<? } ?>
							</select>
							<span>?!) 선택한 메뉴그룹 바로 밑에 화면상 메뉴가 위치합니다.</span>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="btn_area_center">
				<? if ($exec_type == 'INSERT') { ?>
					<input type="image" src="/images/admin/btn_regist.gif" title="등록" />
				<? } else { ?>
					<input type="image" src="/images/admin/btn_modify.gif" title="수정" />
					<a href="#" onclick="delete_sitemap(document.iwc_frm); return false;" title="삭제"><img src="/images/admin/btn_delete.gif" alt="삭제" /></a>
<? } ?>
				<a href="<?= $base_url . '&amp;m=view_site_map' ?>" title="취소"><img src="/images/admin/btn_cancel.gif" alt="취소" /></a>
			</div>
		</div>
	</div>
</form>