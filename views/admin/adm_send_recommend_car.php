
<div class="contents_box_middle">
        <input type="hidden" id="list" value=""/>
        <h1 class="title">Send Car Recommendation</h1>
        
        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You select cars and buyers' email that you want to send recommendation to.</li>

            </ul>

        </div>
        <div class="progress_wrapper" style="display:none;height: 52px;overflow: hidden;">
            <div class="progress_bar">
                <div class="progress_step"></div>
                <span class="progress_percent"></span>

            </div>
        </div>
        <div class="progress_wrapper msg_wrapper" style="display:none;">
        </div>
        <div class="table_list_box tab_content" style="padding-top:10px; overflow:auto; " id="custom_table_container">

            <form name="adm_form_car" id="adm_form_car" action="" method="post" enctype="multipart/form-data">
                <input type="hidden" value="" name="support_member_no" id="support_member_no"/>
                <div class="top_button_container">
                    <h1 class="table_title label_title">Choose Email Template</h1>
                    <select class="input_select_template" name="template_send_car">
                        <option value="one_car_in_single_email">One Car / Email</option>
                        <option <?php if($send_mode=='multiple') echo 'selected'; ?> value="multiple_cars_in_single_email">Mulitple Cars / Email</option>
                    </select>
                    <label id="car_check_count_tip"></label>                        
                </div>
                <div class="left_container" style="overflow:auto; float: left;width: 50%; padding-right:5px; min-height:450px;" >
                	
                    <div class="top_button_container">

                    <h1 class="table_title" style="display:inline; float:left;margin-top: 10px;">Car List</h1>
                    <input type="button" id="btn_remove_car" onclick="remove_all()" value="Remove" name="btn_remove_car" class="button red"/>
                    
                    </div>

                	 <table class="table_list" cellspacing="0" style="width:100%;">
                	 	<thead>
                      
                            <tr>
                               
                                <th scope="col"><input class="idx_chk_all" type="checkbox" value=""></th>
                                <th scope="col"></th>
                                <th scope="col">Car</th>
                                <th scope="col">Chassis No.</th>
                                <th scope="col">Owner</th>
                                
                            </tr>

                        </thead>
                        <tbody>
                        	<?php 
                            $no_image_primary = "https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_big_d2odu9";
                        		foreach($car_list as $car){
                                    
                                    $image_car = ($car->public_id!="") ? $car->base_url.$car->public_id : $no_image_primary;

                        			echo '<tr class="car_rows car_data_'.$car->idx.'">';
                                    echo '<td><input type="checkbox" name="idx_chk[]" class="idx_check" value="'.$car->idx.'" id="idx"/></td>';
                                    echo '<td>
                                            <input type="hidden" id="car_image_'.$car->idx.'" name="car_image['.$car->idx.']" value="'.$image_car.'"/>
                                            
                                            <img src="'.$image_car.'" width="50px" height="50px" id="image_'.$car->idx.'" />
                                            <input id="upload_widget_opener" data-car-idx="'.$car->idx.'" class="upload_widget_opener" type="file" accept="image/*" name="image_upload_file" id="image_upload_file" style="cursor: pointer;height: 51px;margin-left: 28px;margin-top: -50px;position: absolute;width: 50px;z-index: 0;opacity: 0;">
                                           
                                          </td>';

                        			echo '<td>'.$car->car_model_year.' '.$car->car_make.' '. $car->car_model.'</td>';
                        			echo '<td><input type="hidden" id="idx" value="'.$car->idx.'" name="idx[]"/>'.$car->car_chassis_no.'</td>';
                        			echo '<td></td>';
                        			echo '</tr>';
                        			
                        		} 

                        	?>
                        </tbody>
                	 </table>
                </div>
                 <div class="right_container" style="overflow:auto; float: left;width: 50%; padding-left:5px;" >
                    
                    <table cellspacing="0" style="width:100%;">
                        
                        <tr>
                            <th class="col_send_car"></th>

                            <td class="send_car_recommend">
                                <input type="button" id="btn_sent_email_by_idx" value="Send" name="submit_send_email" class="button blue"/>
                            </td>
                            <td class="send_car_space"></td>
                        </tr>
                        <tr>
                                
                            <th class="col_send_car">Subject</th>
                                                                
                            <td><input type="text" class="input_send_car" name="subject"></td>
                            
                            <td class="send_car_space"></td>
                        </tr>
                         <tr>
                                
                            <th class="col_send_car">Recipient List</th>
                                                                
                            <td>
                                <select class="select_send_car" name="recipient_name">

                                    <option value="">--Select Recipient--</option>

                                    <?php foreach ($rec_list as $list){?>

                                    <option value="<?=$list->list?>"><?=$list->list?></option>

                                    <?php } ?>

                                </select>
                            </td>
                            <td class="send_car_space"></td>    
                        </tr>
                        <tr  id="block_send_now">
                            <td colspan="3" class="col_send_car">
                                <input type="radio" name="send_schedule" value="send_now" id="send_now" checked="checked">
                                <b>Send Now </b><br/>
                                Your Marketing Email will be sent now</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="col_send_car">
                            <div id="block_title_and_description_schedule">
                                <input type="radio" name="send_schedule" value="sechedule_date_time" id="sechedule_date_time">
                                <b>Schedule </b><br/>
                                Your Marketing Email will be sent on the date and time specified below
                            </div>
                            </td>
                        </tr>
                         <tr>
                                
                            <th class="col_send_car"><span id="title_schedule_control">Schedule Date</span></th>
                                                                
                            <td>
                            <div id="block_schedule_control_date_time">

                                <input style="float:left;" type="text" name="date" class="schedule_send" id="schedule_send">
                                
                                <div style="float:left; padding-left: 10px;">

                                    <select name="hour" class="select_hour">
                                        <option value="00">Hour</option>
                                        <?php for($i=0;$i<=23;$i++) {?>
                                            <option value="<?=(strlen($i<10) ? '0'.$i : $i)?>"><?=(strlen($i<10) ? '0'.$i : $i)?></option>
                                        <?php } ?>
                                    </select>

                                    <select name="minute" class="select_minute">
                                        <option value="00">Minute</option>
                                        <?php for($i=0;$i<=59;$i++) {?>
                                            <option value="<?=(strlen($i<10) ? '0'.$i : $i)?>"><?=(strlen($i<10) ? '0'.$i : $i)?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                            </div>
                            </td>
                            <td class="send_car_space"></td>
                        </tr>

                    </table>

                </div>

            </form>

            <div class="page_navi">
               
            </div>
            
            <div class="btn_area_center">
                
                <div class="btn_area_right">
                                        
                </div>
            </div>            
          
        </div>

    </div>

<script src="//widget.cloudinary.com/global/all.js" type="text/javascript"></script>
   
<script type="text/javascript" language="javascript" src="js/jquery.ajaxQueue.min.js"></script>

<script type="text/javascript"> 

    $('#send_now').on("change", function(event){
        $('#block_schedule_control_date_time').css("display", "none");
        $('#title_schedule_control').css("display", "none");
        $('#btn_sent_email_by_idx').val('Send');
    });

    $('#sechedule_date_time').on("change", function(event){
        $('#block_schedule_control_date_time').css("display", "");
        $('#title_schedule_control').css("display", "");
        $('#btn_sent_email_by_idx').val('Save');

    });

    var now = new Date();
    var day_now = now.getDate();
    var month_now = now.getMonth()+1;
    var year_now = now.getFullYear();
    
    $("#schedule_send").datepicker({
        dateFormat: 'yyyy-mm-dd',
        isDisabled: function(date) {
            var day_i = date.getDate();
            var month_i = date.getMonth()+1;
            var year_i = date.getFullYear();
            var date1 = new Date(month_now, day_now, year_now);
            var date2 = new Date(month_i, day_i, year_i);
            if(date2<date1){
                return true;
            }else{
                return false;
            }
            
        }
    });

    function compare_date_time(){  
        if($('#sechedule_date_time').is(':checked')) { 
            // 2015-10-01T07:58:49.116Z 
            var datetime_iso = new Date();
            var get_date_time = datetime_iso.toISOString();

            var set_date   = $('#schedule_send').val();
            var set_time   = $('.select_hour').val();
            var set_minute = $('.select_minute').val();

            var set_date_time = set_date+'T'+set_time+':'+set_minute+'.116Z';
            
            if(set_date_time > get_date_time){
                return true;
            }else{
                alert('you set date is in the past')
                return false;
            }
        }
        else{
            return true;
        }
    }

    function validate_subject(){
        if($('.input_send_car').val()==''){
            $('.input_send_car').focus();
            $('.input_send_car').addClass('error');
            return false;
        }else{
            $('.input_send_car').removeClass('error');
            return true;   
        }
    }
    function validate_recipient(){
        if(!$('.select_send_car').val()){
            $('.select_send_car').focus();
            $('.select_send_car').addClass('error');
            return false;
        }else{
            $('.select_send_car').removeClass('error');
            return true;
        }
    }
    function validate_car_list(){
        if($('input:checkbox').length > 1){
            return true;
        }else{
            alert("You don't have any car to send. Please go to stock list and select some cars to send.");
            return false;
        }
    }
    function validate_fields(){
        //validate 3 fields
        if(validate_subject()&&validate_recipient()&&validate_car_list()&&compare_date_time()){
            return true;
        }
    }

    $(document).ready(function(e){

        $('#block_schedule_control_date_time').css("display", "none");
        $('#title_schedule_control').css("display", "none");

        var i = 0;
        var count_check_idx = 0;

        $("#btn_sent_email_by_idx").click(function(e){
            if(validate_fields()){
                var select_template = $(".input_select_template").val();
                var post_data = $("#adm_form_car").serialize();
                var schedule  = $("#schedule_send").val();
                //send email
                $(".progress_wrapper").css("display", "");
                $(".msg_wrapper").css("display", "none");
                $(this).css("display", "none");
                $(".label_title").css("display", "none");
                $(".input_select_template").css("display", "none");
                $(".left_container").css("display", "none");
                $(".right_container").css("display", "none");

                //************START SEND/SAVE ONE CAR IN SINGLE EMAIL**********************//

                if(select_template=="one_car_in_single_email"){

                    $("#car_check_count_tip").html(i+' / '+count_check_idx);

                    if (schedule!='') {
                        $(".progress_percent").html('Saving Email');
                    } else {
                        $(".progress_percent").html('Sending Email');
                    }
                           
                    $.ajax({
                        type: "POST",
                        url: "/?c=admin&m=send_car_offer",
                        data: post_data,
                        async: true
                    }).done(function(){

                        i++;
                        $("#car_check_count_tip").html(i+' / '+i);
                        if (schedule!='') {
                            $(".progress_percent").html('Saving Email ('+(Math.floor(i*100/i))+'%)');
                        } else{
                            $(".progress_percent").html('Sending Email ('+(Math.floor(i*100/i))+'%)');
                        }
                        $(".progress_step" ).css( "width",i*100/i+'%');
                        if(Math.floor(i*100/i)==100){
                            if (schedule!='') {
                                $(".progress_percent").html('<img src=\"/images/admin/send_complete.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Saving Email Completed!</spand>');
                            } else{
                                $(".progress_percent").html('<img src=\"/images/admin/send_complete.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Sending Email Completed!</spand>');
                            }
                                
                            $(".progress_bar").css( "background",'none');
                            $("#car_check_count_tip").css( "display",'none');
                            $(".progress_step" ).css( "width",'0px');
                        }

                    }).fail(function() {

                        i++;
                        $("#car_check_count_tip").html(i+' / '+i);
                        if (schedule!='') {
                            $(".progress_percent").html('Saving Email ('+(Math.floor(i*100/i))+'%)');
                        } else{
                            $(".progress_percent").html('Sending Email ('+(Math.floor(i*100/i))+'%)');
                        }
                        $(".progress_step" ).css( "width",i*100/i+'%');
                        if(Math.floor(i*100/i)==100){
                            if (schedule!='') {
                                $(".progress_percent").html('<img src=\"/images/admin/send_error.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Saving Email Failed!</spand>');
                            } else{
                                $(".progress_percent").html('<img src=\"/images/admin/send_error.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Sending Email Failed!</spand>');
                            }
                            $(".progress_bar").css( "background",'none');
                            $("#car_check_count_tip").css( "display",'none');
                            $(".progress_step" ).css( "width",'0px');
                        }
                               
                    }); 
                }

                //************END SEND/SAVE ONE CAR IN SINGLE EMAIL**********************//

                if(select_template=="multiple_cars_in_single_email") {

                    //***********START SEND/SAVE MULTIPLE CAR IN SINGLE EMAIL***************//   

                    $("#car_check_count_tip").html(i+' / '+count_check_idx);

                    if (schedule!='') {
                        $(".progress_percent").html('Saving Email');
                    } else {
                        $(".progress_percent").html('Sending Email');
                    }
                           
                    $.ajax({
                        type: "POST",
                        url: "/?c=admin&m=multiple_send_car_offer",
                        data: post_data,
                        async: true
                    }).done(function(){

                        i++;
                        $("#car_check_count_tip").html(i+' / '+i);
                        if (schedule!='') {
                            $(".progress_percent").html('Saving Email ('+(Math.floor(i*100/i))+'%)');
                        } else{
                            $(".progress_percent").html('Sending Email ('+(Math.floor(i*100/i))+'%)');
                        }
                        $(".progress_step" ).css( "width",i*100/i+'%');
                        if(Math.floor(i*100/i)==100){
                            if (schedule!='') {
                                $(".progress_percent").html('<img src=\"/images/admin/send_complete.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Saving Email Completed!</spand>');
                            } else{
                                $(".progress_percent").html('<img src=\"/images/admin/send_complete.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Sending Email Completed!</spand>');
                            }
                                
                            $(".progress_bar").css( "background",'none');
                            $("#car_check_count_tip").css( "display",'none');
                            $(".progress_step" ).css( "width",'0px');
                        }

                    }).fail(function() {

                        i++;
                        $("#car_check_count_tip").html(i+' / '+i);
                        if (schedule!='') {
                            $(".progress_percent").html('Saving Email ('+(Math.floor(i*100/i))+'%)');
                        } else{
                            $(".progress_percent").html('Sending Email ('+(Math.floor(i*100/i))+'%)');
                        }
                        $(".progress_step" ).css( "width",i*100/i+'%');
                        if(Math.floor(i*100/i)==100){
                            if (schedule!='') {
                                $(".progress_percent").html('<img src=\"/images/admin/send_error.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Saving Email Failed!</spand>');
                            } else{
                                $(".progress_percent").html('<img src=\"/images/admin/send_error.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Sending Email Failed!</spand>');
                            }
                            $(".progress_bar").css( "background",'none');
                            $("#car_check_count_tip").css( "display",'none');
                            $(".progress_step" ).css( "width",'0px');
                        }
                               
                    }); 
                }

                //***********END SEND/SAVE MULTIPLE CAR IN SINGLE EMAIL***************//   
            }

        });

        /*** When check checkbox car pressed ***/
        $(".idx_chk_all").change(function(e){
            $('.idx_check').prop('checked', $(this).prop('checked'));
        });
        
        //***********************UPDATE/CHANGE CARS IMAGE**********************//

        // $(".upload_widget_opener").click( function(e) {
        //     var car_idx = $(this).attr('data-car-idx');
        //     cloudinary.openUploadWidget({ cloud_name: 'softbloom', upload_preset: 'juefxlp7', show_powered_by: false, max_files: 1, max_image_width: 1024, max_image_height: 768}, 
        //       function(error, result) {
        //         var url = result[0].url;
    
        //         $("#car_image_"+car_idx).val(url.slice(0,-4));
        //         $("#image_"+car_idx).attr("src",url.slice(0,-4));

        //     });
        // });
    });

    $(document).on('change', '#upload_widget_opener', function () {
        var car_idx = $(this).attr('data-car-idx');
        var formData = new FormData();
        formData.append('file', $(this)[0].files[0]);

        $.ajax({
               url : '/?c=admin&m=adm_upload_image_for_send_email',
               type : 'POST',
               data : formData,
               processData: false,  // tell jQuery not to process the data
               contentType: false,  // tell jQuery not to set contentType
               success : function(data) {
                   $("#car_image_"+car_idx).val($.trim(data));
                   $("#image_"+car_idx).attr("src",$.trim(data));
               }
        });
    });
    //*******************************REMOVE CARS BY CHECKED**********************************//

    function remove_all() {
        var f = document.adm_form_car;
        var fg = false;
        var idxs = new Array();
        
        if (f.idx != null) {
            for (i = 0; i < f.idx.length; i++) {
                if (f.idx[i].checked) {
                    fg = true;
                    idxs[i] =f.idx[i].value;
                }
            }

            if (!fg) {
                alert('Please select cars that you want to remove.');
                return;
            }

            if (!confirm('Do you want to remove this car?'))
            {
                return;

            }else{
                var form_data = $("#adm_form_car").serializeArray();   
                var request = $.ajax({
                  url: "/?c=admin_json&m=delete_idx_from_newsletter",
                  type: "POST",
                  data: form_data,
                  dataType: "json",
                });
                request.done(function(msg){
                    //
                });
                for(var i = 0; i<idxs.length; i++) {
                    $('.car_data_'+idxs[i]).remove();

                }                
            }

        }
    }
</script>