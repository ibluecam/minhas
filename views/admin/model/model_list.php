
<style type="text/css">
    
  .icon_model_list{
       display: inline-block;
        float: left;
         width: 70px;
  }
  .title_model_list{
            display: inline-block;
    float: left;
    padding-top: 10px;
    text-align: left;
    width: 70px;
  }
  .block_icon_title_model{
    display: inline-block;
  }
  .clear{
    clear: both;
  }
</style>

<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />
<?php 
function updateCurrentGetUrl($p=array(), $unset_p=array()){
    $get = $_GET;
    if(count($unset_p)>0){
        foreach($unset_p as $value){
            if(isset($get[$value])) unset($get[$value]);
        }
    }
    if(count($p)>0){
        foreach($p as $key=>$value){
            $get[$key]=$value;
        }
    }
    $updatedUrl_arr = array();
    foreach($get as $key=>$value){
        $updatedUrl_arr[]=$key."=".$value;
    }
    $updatedUrl = implode("&", $updatedUrl_arr);
    return $updatedUrl;
}


?>
<?php
  if(isset($_GET['select_make'])) $select_make=  htmlspecialchars ($_GET['select_make']);else $select_make="";
 if(isset($_GET['sch_word'])) $sch_word=  htmlspecialchars($_GET['sch_word']);else $sch_word="";
?>
<div class="contents_box_middle">

        <h1 class="title">Model</h1>
        
        

        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You can add new model or modify model here.</li>

            </ul>

        </div>
        <div class="table_list_box" style="padding-top:10px;" id="updates">

            <h1 class="table_title">Model List</h1>

            <div class="search_box" style="background:none; border:none">

                <div style="color: #666666;line-height: 18px;padding: 2px 0 2px 0px;">

                    <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Total : Page:<span id="current_num_rows" class="title"><?= $cur_page ?></span> /<span id="total_num_rows" class="title"><?= $total_page ?></span>

                </div>
                <div class="search_condition">
                
                 <div class="btn_area_right" style="text-align:left; margin-left:0px;">
                    <a  class="fancybox_add iframe" href="/?c=admin&amp;m=add_model"><span class="button blue">Add New</span></a>
                    <a href="#" onclick="delete_all();" title="delete"><span class="button red">Delete Model</span></a>
                    
                </div>
                
                    <form name="bbs_search_frm" id="adm_frm" method="get" action="/?c=admin&amp;m=model_list" >
                         <input type="hidden" name="c" value="admin">
                         <input type="hidden" name="m" value="model_list">
                         <label>Search:</label>
                          <select name="select_make"  class="input_2" style="width:150px;" align="absmiddle">
                                 <option value="">--Select Make--</option>
                                   
                                <?php 
                                  if(count($select_make_for_model)>0){
                                     foreach($select_make_for_model as $make){
                                     ?>
                                     <option
                                          <?php 
                               echo $select_make=(isset($_GET['select_make']) && $_GET['select_make']=="$make->make_name"? 'selected="selected"':''  );
                                 ?>
                                        
                                         value="<?php echo $make->make_name; ?>"><?php echo $make->make_name; ?></option>
                                     <?php
                                     } 
                                  }
                                 ?>
                        </select>
                         <input type="text" title="검색어" style="width:140px;" class="input_1" value="<?php echo $sch_word; ?>" id="sch_word" name="sch_word" placeholder="Search Model">
                         <input type="image" align="absmiddle" title="검색" class="search_admin"  src="/images/admin/btn_search_01.gif">
                    </form>
                </div>
                
            </div>
           
             <div style="overflow:auto; float:left; width:100%;" >
                <form name="adm_frm_2" id="adm_frm_2" method="post" enctype="multipart/form-data" action="">
                   
                <input type="hidden" name="address"> <!-- sending url address -->

                <table class="table_list" cellspacing="0" style="width:100%; min-width: 800px;">

                    <caption class="hidden"></caption>

                    <thead>

                        <tr>

                            <th scope="col"><input type="checkbox" name="idx" id="idx" class="id_chk_all" 
                                                onclick="checkbox_all_check(this, document.adm_frm_2.idx);"
                                                   value=""/>
                            </th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col">Make</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>                        
                            <th scope="col">Model</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            <th scope="col"></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col"></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        </tr>

                    </thead>

                    <tbody>
                        <?php
                          if(count($adm_model_select)){
                              foreach ($adm_model_select as $field ){
                           
                        ?>
                        
                        <tr>
                   
                            <td><input type="checkbox" name="idx[]" class="idx" id="idx" value="<?php echo $field->id_model ?>" data-idx="<?=$field->id_model ?>"/>
                            <td class="line">&nbsp;</td>


                            <td>
                                <div class="block_icon_title_model">
                                  <a class="icon_model_list" href="" target="_blank">
                                     <img src="<?php echo $field->icon_name; ?>">
                                  </a>
                                 <a class="title_model_list" href="" target="_blank">
                                 
                                    <?php
                                          echo $field->make_name;
                                   ?>
                                 </a>
                                 <div class="clear"></div>
                               </div>
                            </td>

                            <td class="line">&nbsp;</td>



                            <td>
                                <a href="" target="_blank">
                                   <?php
                                      echo $field->model_name;
                                   ?>
                                </a>
                            </td>
                            <td class="line">&nbsp;</td>
                            <td>
                              <a id="fancybox_change" class="fancybox_change iframe" href="/?c=admin&amp;m=adm_change_model&id=<?= $field->id_model; ?>">
                                <img src="/images/admin/modify.gif" alt="modify">
                              </a>
                            </td>

                        </tr>
                       
                        <?php
                               
                              }
                          }
                          else{
                              
                          
                        ?>
                        
                        <tr>
                        <td class="no_data" colspan="20">Data does not exist.</td>
                       </tr>
                        <?php
                        }
                        ?>
                    </tbody>

                </table>

            </div>
            
             <div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?'.updateCurrentGetUrl(array(), array('cur_page')), $mcd) ?></div>
            
            <div class="btn_area_center">
               
            </div>

            
          </form>
        </div>

    </div>


<script type="text/javascript">
   
  
   
    $(document).ready(function(e){
        
        $("a.fancybox_add").fancybox({
            height  : 250,
            width   : 450,
            onClosed: function() { 
                window.location = window.location;
            }
        }); 
        
        
        
        $("#close").click(function(e){
            $.fancybox.close();
            //extractImageFromPdf();
        });
        $(document).on('click','#btn_load_more',function(){
            var count_row = get_current_row_fetch();
            $("#loading_more").css("display", "inline");
            load_more(count_row);
            
            
        });
    

        $( "tbody" ).sortable();
        $( "tbody" ).disableSelection();

        
    }); 
  
   

    function checkbox_all_check(obj, target) {
    if ( typeof obj == 'undefined' || typeof target == 'undefined') {

    }
    else if ( typeof target.length == 'undefined' ) {
      target.checked = obj.checked;
    }
    else
    {
      for (i = 0 ; i < target.length ; i++) {
        target[i].checked = obj.checked;
      }
    }
  }

  
        
        
        
        function delete_all() {
    var f  = document.adm_frm_2;
    var fg = false;

    if(f.idx != null) {
      for(i=0; i<f.idx.length; i++) {
        if(f.idx[i].checked) {
          fg = true;
        }
      }

      if(!fg) {
        alert('Please select model car that you want to delete.');
        return;
      }

      if (!confirm('Are you sure want to remove this car from model list?'))

          {
              return;
              
          }else{

            f.action = '?c=admin&m=add_delete_model';
                       
                        
        f.submit();
          }
      
    }
  }   
  
   $("a.fancybox_change").fancybox({
            height  : 300,
            width   : 450,
            onClosed: function() {
                window.location = window.location;
            }
        });
            
        $(".id_chk_all").change(function(e){
        $(".idx").prop('checked', this.checked);
    });
   
</script>



       

