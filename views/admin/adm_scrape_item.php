<div class="contents_box_middle">

        <h1 class="title">Pulling Items from Web</h1>
        
				

        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You can pull all items from some websites by selection searching first and then Update All or Insert Images to copy images from those websites to motorbb's site. </li>

            </ul>

        </div>

                <!--<div style="padding-top:5px;text-align:right;"><img src="/images/admin/btn_reset.gif" alt="초기화" /></div>-->

        <div class="table_list_box" style="padding-top:10px;" id="updates">

            <h1 class="table_title">Car List</h1>

            <div class="search_box">

                <div class="search_result">

                    <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" /><img id="fetch_loading" class="loading" style="float:left;visibility:hidden;" src="images/ico-loading2.gif"/> Total : <span id="current_num_rows" class="title">0</span> /<span id="total_num_rows" class="title"> 0</span> cars

                </div>
                
                <div class="search_condition">
                   
                  <form name="bbs_search_frm" id="adm_frm" method="get" action="" >
                    	<select class="input_2" id="website_name">
                    		<option value="primegt" data-detail-link="http://www.primegt.com/stock/detail.aspx?st=">Primegt.com</option>
                    		<option value="broomjapan" data-detail-link="http://www.broomjapan.com/stock/detail.aspx?st=">Broomjapan.com</option>
                    		<option value="sbtjapan" data-detail-link="http://www.sbtjapan.com/catalog/searchstock/stockid=">Sbtjapan.com</option>
                    	</select>
                    	<select class="input_2" id="status">
                    		<option value="sale">Sale</option>
                    		<option value="sold">Sold</option>
                    	</select>
                    	<input type="button" class="adm_button" value="search" id="btn_search"/>
                   </form>
                   <br>

                    
                </div>


            </div>
           	<div class="btn_area_center">
                <!-- <div class="btn_area_left">
                    <a class="fancybox" href="#excel_export_popup"><span class="button green">Excel Export</span></a>
                   
                </div> -->

                <div class="btn_area_right">
                    <input type="button" class="button adm_button blue" id="btn_update_all" disabled="disabled" name="submit_update_all" value="Update All"><img style="visibility:hidden;" id="update_all_loading" class="loading" src="images/ico-loading2.gif"/>
                    
                </div>
            </div>
             <div style="overflow:auto; float:left; width:100%;" >
                <form name="adm_frm_2" id="adm_frm_2" method="post" enctype="multipart/form-data" action="">
                   
                <input type="hidden" name="address"> <!-- sending url address -->

                <table class="table_list" cellspacing="0" style="width:100%; min-width: 800px;">

                    <caption class="hidden"></caption>

                    <thead>

                        <tr>

                            

                            <th scope="col">ID</th>
                            
                            <th scope="col">Chassis No.</th>
                            
                            <th scope="col">Status (Pulling)</th>
                            
                            <th scope="col">Status (Motorbb)</th>
                           
                            <th scope="col">Created Date</th>
                            
                            <th scope="col"></th>
                            
                        </tr>

                    </thead>

                    <tbody id="table_body" style="min-height:100px;">
                          
                       
                    </tbody>

                </table>
                <div id="images_container">

                </div>
            </div>
            
            <div class="page_navi">
                <div id="more" class="loading" style="text-align: center;">

                    <img  style="display:none" id="loading_more_bbs" src="images/ico-loading2.gif"/>
                </div>
                
            </div>
            
            

            
          </form>
        </div>

    </div>

   
            
   
   
<script type="text/javascript" language="javascript" src="js/jquery.ajaxQueue.min.js"></script>

<script type="text/javascript">
   
  
   var page_count = 0;
    var record_count = 0;
	var website_name = '';
	var status = '';
	var current_row = 0;
	$(document).ready(function(e){
		
		//scrapAndAppend();
		//alert(jQuery.timeago(new Date()));
	  	$("#btn_search").click(function(e){
	  		$(this).attr('disabled','disabled');
	  		$('#table_body').html('');
	  		
	  		website_name = $("#website_name").val();
	  		status = $("#status").val();
	  		processPull(website_name, status);
	  		//scrapAndAppend(website_name, status);
	  	});
	  	$("#btn_update_all").click(function(e){
	  		$(this).attr('disabled','disabled');
	  	 	updateAllData();
	  	});
	  	
	  	// $("#btn_update_images").click(function(e){
	  	// 	postUploadImages();
	  	// });
		$(document).on("click", ".btn_insert_image", function(e){
			click_chassis = $(this).attr('data-chassis');
			startInsertImageProgress(click_chassis);
			data = $( "#images_container form[data-chassis='"+click_chassis+"']" ).serializeArray();
			$.ajaxQueue({
			  type: "POST",
			  url: "/?c=admin&m=adm_upload_scrape_images",
			  data: data,
			  dataType: "json"
			})
		  	.done(function( json ) {
		  		stopInsertImageProgress(click_chassis);
		    	updateCountImage(click_chassis, json[0])
		  	})
		  	.fail(function(jqXHR, textStatus){
		  		stopInsertImageProgress(click_chassis);
		  		updateCountImage(click_chassis, null);
		  	});
		});
		
	});

	function updateCountImage(chassis, image_count){
		$( ".image_count[data-chassis='"+chassis+"']" ).html("("+image_count+" image(s))");
		if(image_count!=null){
			if(image_count>0){
				$( ".image_count[data-chassis='"+chassis+"']" ).removeClass("image_zero");
				$( ".btn_insert_image[data-chassis='"+chassis+"']").css("display", "none");
			}
		}else{
			$( ".btn_insert_image[data-chassis='"+chassis+"']").css("display", "none");
		}

	}

	function updateAllData(){
		var i=0;
		var count_form = $("#table_body .form_update_data").length;
		$("#table_body .form_update_data").each(function(e){
			var data = $(this).serializeArray();
			//console.log(this);
			startUpdateAllProgress();
			$.ajax({
			  type: "POST",
			  url: '/?c=admin&m=adm_json_scrape_update_all',
			  data: data,
			  dataType: "json"
			})
		  	.done(function( json ) {
		  		i++;
		    	//$(this).append("OK");
		    	//alert(i+"/ "+record_count);
		    	//console.log(i+'/'+count_form);
		    	if(i>=count_form){
		    		current_row = 0;
		    		stopUpdateAllProgress();
		    		alert("Update Completed!");
		    		$('#table_body').html('');
			  		
			    	processPull(website_name, status);
			    	$("#btn_update_all").removeAttr('disabled');
		    	}
		    	
		    	//$(this).attr('disabled','disabled');
		  		
		  	}).fail(function(jqXHR, textStatus){
		  		i++;
		  		//console.log(i+'/'+count_form);
		  		if(i>=count_form){
		    		current_row = 0;
		    		stopUpdateAllProgress();
		    		alert("Update Completed!");
		    		$('#table_body').html('');
			  		
			    	processPull(website_name, status);
			    	$("#btn_update_all").removeAttr('disabled');
		    	}
		  	});
		});
	}

	function processPull(website_name, status){
		var data = {
			website_name: website_name,
		};
		$('#images_container').html('');
		var url="/?c=admin&m=adm_scrape_total_page";
		current_row = 0;
		startFetchProgress();
		$.ajax({
		  type: "POST",
		  url: url,
		  data: data,
		  dataType: "json"
		})
	  	.done(function( json ) {
	    	record_count = 0;
	    	// record_count =json['id'].length;
	    	// $("#total_num_rows").html(record_count);
	    	//$("#total_num_rows").html("Counting Pages");
	    	page_count = json.total_page;
	    	
	    	for(var i=1;i<=json.total_page;i++){
	    		
	    		scrapAndAppend(website_name, status, i);
	    	}
	    	//appendRow(website_name, json);
	  	})
	  	.fail(function(jqXHR, textStatus){
	  		alert("Request failed! Please try again.");

	  		stopFetchProgress();
	  	});
	}

	function scrapAndAppend(website_name, status, page_num){
		var data = {
			website_name: website_name,
			status : status,
			page_num: page_num
		};
		
		var url="/?c=admin&m=adm_scrape_json_item";
		
		$.ajaxQueue({
		  type: "POST",
		  url: url,
		  data: data,
		  dataType: "json"
		})
	  	.done(function( json ) {
	  		if(json['id']!=null){
	  			if(json['id'].length>0){
		    		record_count += json['id'].length;
		    	}
	  		}
		    	
	    	if(page_num == page_count){
	    		$("#total_num_rows").html(record_count);
	    	}

	    	appendRow(website_name, json);
	  	})
	  	.fail(function(jqXHR, textStatus){
	  		alert("Request failed! Please try again.");

	  		stopFetchProgress();
	  	});
	}
	function appendRow(website_name, arr){
		//var i = 0;
		var detail_link = $('#website_name option:selected').attr('data-detail-link');
		$.each(arr['id'], function( index, value ) {
			var data = {
				website_name : website_name
			};
			$.ajaxQueue({
			  type: "POST",
			  url: "/?c=admin&m=adm_scrape_json_item_detail&website_name="+website_name+"&id="+value,
			  data: data,
			  dataType: "json"
			})
		  	.done(function( json ) {
		  		current_row++;
		  		percent = (parseInt(current_row, 10) / parseInt(record_count, 10)) * 100;
		  		var tr = '';
		  		var td = '';
		  		var input_html = '';
		  		tr += '<tr class="success_row">';
		  		
		  		td += '<td>';
		  			td+='<form class="form_update_data">';
		    		td+='<input type="hidden" name="product_id" value="'+value+'"/>';
		    		td+='<input type="hidden" class="chassis_no" name="car_chassis_no" value="'+json.chassis_no+'"/>';
		    		td+='<input type="hidden" name="icon_status" value="'+arr['status'][index]+'"/>';
		    		td+='<input type="hidden" name="old_fob_cost" value="'+arr['old_price'][index]+'"/>';
		    		td+='<input type="hidden" name="website_name" value="'+website_name+'"/>'+value;

		    		td+='<input type="hidden" name="car_body_type" value="'+json.product_type+'"/>';
		    		td+='<input type="hidden" name="car_make" value="'+json.make+'"/>';
		    		td+='<input type="hidden" name="car_color" value="'+json.exterior_color+'"/>';
		    		td+='<input type="hidden" name="car_model" value="'+json.model+'"/>';
		    		td+='<input type="hidden" name="car_steering" value="'+json.steering+'"/>';
		    		td+='<input type="hidden" name="car_transmission" value="'+json.transmission+'"/>';
		    		td+='<input type="hidden" name="car_cc" value="'+json.engine_volume+'"/>';
		    		td+='<input type="hidden" name="car_drive_type" value="'+json.drive+'"/>';
		    		td+='<input type="hidden" name="door" value="'+json.door+'"/>';
		    		td+='<input type="hidden" name="car_fuel" value="'+json.fuel_type+'"/>';
		    		td+='<input type="hidden" name="car_seat" value="'+json.number_passenger+'"/>';
		    		td+='<input type="hidden" name="first_registration_year" value="'+json.registrationyear+'"/>';
		    		td+='<input type="hidden" name="first_registration_month" value="'+json.registrationmonth+'"/>';
		    		td+='<input type="hidden" name="manu_year" value="'+json.manu_year+'"/>';
		    		td+='<input type="hidden" name="manu_month" value="'+json.manu_month+'"/>';
		    		td+='<input type="hidden" name="car_mileage" value="'+json.mileage+'"/>';
		    		td+='<input type="hidden" name="car_length" value="'+json.car_length+'"/>';
					td+='<input type="hidden" name="car_width" value="'+json.car_width+'"/>';
					td+='<input type="hidden" name="car_height" value="'+json.car_height+'"/>';

		    		//td+='<input type="hidden" name="mileage_type" value="'+json.mileage_type+'"/>';
		    		td+='<input type="hidden" name="car_fob_cost" value="'+json.price+'"/>';
		    		td+='<input type="hidden" name="car_fob_currency" value="'+json.currency+'"/>';
		    		td+='<input type="hidden" name="icon_new_yn" value="'+json.icon_new_yn+'"/>';
		    		td+='</form>';
		    	td+='</td>';
		    	td += '<td><a target="_blank" href="/?c=admin&m=adm_bbs_list&mcd=product&sch_condition=car_chassis_no+like&sch_word='+json.chassis_no+'">'+json.chassis_no+'</a></td>';
		    	td += '<td><a target="_blank" href="'+detail_link+value+'">'+arr['status'][index]+'</a></td>';
		    	td += '<td class="motorbb_status"></td>';
		    	td += '<td class="motorbb_created_date"></td>';
		    	td += '<td class="btn_update_container"></td>';
		    	
		    	tr += td+'</tr>';
		    	
		    	$('#table_body').append(tr);
		    	
		    	$("#current_num_rows").html(current_row);
		  //   	$.each(json.images, function( index, value ) {
				//   	appendImages(json.chassis_no, value);
				// });
				//console.log(json.images);
				if(json.images!=null&&json.images!=''){
					appendImages(json.images, json.chassis_no);
				}
		    	if(current_row>=record_count){
		    		getCountChassisNo();
		    		$("#btn_search").removeAttr('disabled');
		    		stopFetchProgress();
		    	}
		  	})
		  	.fail(function(jqXHR, textStatus){
		  		$.ajax(this);
		  		current_row++;
		  		percent = (parseInt(current_row, 10) / parseInt(record_count, 10)) * 100;
		  		
		  		$('#table_body').append('<tr class="fail_row"><td>'+value+'</td><td>Failed</td><td>Failed</td><td></td><td></td></tr>');
		    	$("#current_num_rows").html(current_row);
		    	console.log('current_row: '+current_row+' / Total Row: '+record_count);
		    	if(current_row>=record_count){
		    		getCountChassisNo();
		    		$("#btn_search").removeAttr('disabled');
		    		stopFetchProgress();
		    	}
		  	});
		  	
		});
	}
	function appendImages(images, chassis_no){
		var form_html='';
		form_html += '<form data-chassis="'+chassis_no+'" method="post">';
		form_html += '<input type="hidden" name="chassis_no" value="'+chassis_no+'"/>';
		$.each(images, function( index, value ) {
			form_html += '<input type="hidden" name="images[]" value="'+value+'"/>';
		});
		
		form_html += '</form>';
		$('#images_container').append(form_html);
	}

	// function postUploadImages(){
	// 	var data = $("#form_images").serializeArray();
	// 	$.ajax({
	// 	  method: "POST",
	// 	  url: 'ajax/post_scrap_upload_image.php',
	// 	  data: data,
	// 	  dataType: "html"
	// 	})
	//   	.done(function( json ) {
	//     	alert(json);
	//   	});
	// }
	function getCountChassisNo(){
		var chassis_no = '';
		
		$(".chassis_no").each(function(e){
			if(chassis_no!=''){
				chassis_no += '^';
			}
			chassis_no += $(this).val();
		});
		data = {
			chassis_no : chassis_no
		};
		$.ajax({
		  type: "POST",
		  url: '/?c=admin&m=adm_json_scrape_count_by_chassis',
		  data: data,
		  dataType: "json"
		})
	  	.done(function( json ) {
	    	updateStatusTable(json);
	  	})
	  	.fail(function(jqXHR, textStatus){
		  	alert("Counting products in Motorbb failed! Please try searching again.");
		});
	}

	function updateStatusTable(json){
		var chassis_arr = [];
		var status_arr = [];
		var id_arr = []; 
		var image_arr = [];
		var created_dts_arr = [];
		var timestamp = [];
		var image_count = [];
		var ignore_scan_update = [];
		$.each(json, function( index, value ) {
		  	chassis_arr.push(value.car_chassis_no);
		  	status_arr.push(value.icon_status);
		  	id_arr.push(value.idx);
		  	timestamp.push(value.timestamp);
		  	image_count.push(value.image_count);
		  	ignore_scan_update.push(value.ignore_scan_update);
		});
		// $.each(json.images, function( img_idx, img_val ) {
		// 		  	appendImages(img_val, json.chassis_no);
		// 		});
		$("#table_body tr").each(function(e){
			
			var chassis_value = $(this).find(".chassis_no").val();
			//alert(chassis_value);
			ch_idx = $.inArray(chassis_value, chassis_arr);
			if(ch_idx>=0){
				var image_count_class='';
				if(image_count[ch_idx]<=0){
					image_count_class = 'image_zero';
					$(this).find(".btn_update_container").html('<input type="button" data-chassis="'+chassis_value+'" name="btn_insert_image" class="btn_insert_image" value="Insert Images"/><img class="loading" style="visibility:hidden" src="images/ico-loading2.gif"/>');
					
				}
				if(ignore_scan_update[ch_idx]==1){
					$(this).addClass('ignore_update_row');
					html_skip = ', Skipped Row';
				}else{
					html_skip = '';
				}
				$(this).find(".motorbb_status").html('<a target="_blank" href="/?c=admin&m=adm_bbs_list&mcd=product&sch_condition=car_chassis_no+like&sch_word='+chassis_value+'">'+status_arr[ch_idx]+" <span data-chassis=\""+chassis_value+"\" class=\"image_count "+image_count_class+"\">("+image_count[ch_idx]+" image(s))</span>"+html_skip+"</a>");
				if(status_arr[ch_idx]!='Sold'){
					//$(this).find(".form_update_data").append('<input type="hidden" name="update_mode" value="update"/>');
					$(this).find(".motorbb_created_date").html('<span data-livestamp="'+timestamp[ch_idx]+'"></span>');
				}else{
					$(this).find(".motorbb_created_date").html('<span data-livestamp="'+timestamp[ch_idx]+'"></span>');
					//$(this).find(".form_update_data").append('<input type="hidden" name="update_mode" value="no"/>');
				}
				
				$(this).find(".form_update_data").append('<input type="hidden" value="'+id_arr[ch_idx]+'" name="idx"/>');
					
			}else{
				$(this).find(".motorbb_status").html('Not Exist');
				//$(this).find(".form_update_data").append('<input type="hidden" name="update_mode" value="new"/>');
			}
		});
	}
	function resetFetchProgress(){
		$("#record_step").html('0');
		$("#record_count").html('0');
	}

	function startFetchProgress(){
		$("#current_num_rows").html(current_row);
		$("#total_num_rows").html(' (Fetching)');
		$("#fetch_loading").css("visibility", "visible");
		$("#btn_update_all").attr("disabled", "disabled");
	}

	function stopFetchProgress(){
		$("#fetch_loading").css("visibility", "hidden");
		$("#btn_update_all").removeAttr('disabled');
	}
	function startInsertImageProgress(chassis_no){
		$( ".btn_insert_image[data-chassis='"+chassis_no+"']").next(".loading").css("visibility", "visible");
		$( ".btn_insert_image[data-chassis='"+chassis_no+"']").attr("disabled", "disabled");
	}
	function stopInsertImageProgress(chassis_no){
		$( ".btn_insert_image[data-chassis='"+chassis_no+"']").next(".loading").css("visibility", "hidden");
		$( ".btn_insert_image[data-chassis='"+chassis_no+"']").removeAttr('disabled');
	}
	function startUpdateAllProgress(){
		$( "#update_all_loading").css("visibility", "visible");
	}
	function stopUpdateAllProgress(){
		$( "#update_all_loading").css("visibility", "hidden");
	}

	// function postInsertProductScrap(){
	// 	var data = $("#form_scrap").serializeArray();
	// 	$.ajax({
	// 	  method: "POST",
	// 	  url: "ajax/post_insert_product_scrap.php",
	// 	  data: data,
	// 	  dataType: "json"
	// 	})
	//   	.done(function( json ) {
	    	
	//   	});
	// }   
	$(document).ajaxStart(function(){
        $(".adm_button").attr("disabled", "disabled");
    });
    $(document).ajaxComplete(function(){
        $(".adm_button").removeAttr('disabled');
    });
 //    $( document ).ajaxError(function() {
 //    	alert("Request Failed! Please try again.");
 //    	$(".loading").css("visibility", "hidden");
	//   	$(".adm_button").removeAttr('disabled');
	// }); 
</script>



       

