<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Exchange Rate List</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script type="text/javascript" src="/js/common/common.js"></script>
<link rel="stylesheet" type="text/css" href="/css/admin/button.css" />
<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />

<!-- link calendar resources -->
<link rel="stylesheet" type="text/css" href="/css/tcal.css" />
<script type="text/javascript" src="/js/tcal.js"></script> 

<script type="text/javascript">
    //refresh page


    window.onunload = function(){
     window.opener.location.reload();
    };
	function exit_win(){
		window.close();
	}
	
	function insert_adm_bbs(frm) {
        if (!validate(frm.elements['values[exchange_rate]'], 'INPUT [Exchange Rate]'))
            return false;
        if (!validate(frm.elements['values[exchange_dt]'], 'INPUT [Exchange Date]'))
            return false;

    }
	
	 //게시판 체크박스 전체 선택
    function checkbox_all_check(obj, target) {
        if (typeof obj == 'undefined' || typeof target == 'undefined') {

        }
        else if (typeof target.length == 'undefined') {
            target.checked = obj.checked;
        }
        else
        {
            for (i = 0; i < target.length; i++) {
                target[i].checked = obj.checked;
            }
        }
		
    }
	
	function modify_check() {
        var f = document.adm_frm_rate;
        check_count = 0;
        if (f.id != null) {

            for (i = 0; i < f.id.length; i++) {

                if (f.id[i].checked) {
                    check_count++;
                }
            }
            if (check_count == 0) {
                alert('Please select Exchange rate to Modify');
                return;
            } else if (check_count > 1) {
				
				window.location.reload();
                alert('Please select only one Rate to modify info.');
				
                return false;

            } else {
                f.action = '?c=admin&m=adm_account_rate_modify&mcd=<?php echo $mcd; ?>&id=' + f.id;
                f.submit();
            }
        }
    }
	
	

   
</script>
</head>

<body>

<div class="warp_rate rat_modify">
<?
if(count($account_rate_list)>0){
	foreach ($account_rate_list as $row){
	

?>
	<form name="adm_frm" id="adm_frmb" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_account_rate_modify_exec&amp;mcd=<?= $mcd ?>&id=<?= $row->id ?>" onsubmit="return insert_adm_bbs(this);">
	<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
    <table width="220px" cellpadding="0" cellspacing="0" class="tbl_addrate"  border="0">
        <colgroup>
            <col width="50%"/>
            <col width="50%"/>
        </colgroup>
    
        <tr>
            <th colspan="2" class="box_title" align="left">
            	Current Exchange Rate
            </th>
        </tr>
        <tr>
            <td>From</td>
            <td>To</tdh>
        </tr>
        <tr>
            <td>
            	
					<?=func_currency_sym($row->from_currency,'Y')?>
  
                
            </td>
            <td align="left">
            	
					<?=func_currency_sym($row->to_currency,'Y')?>
  
               
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="text" id="exchange_rate" name="values[exchange_rate]" value="<?= $row->exchange_rate?>"></td>
        </tr>
        <tr>
            <td colspan="2">
            <input type="text" id="exchange_dt" name="values[exchange_dt]" value="<?= $row->exchange_dt?>" class="" readonly >
            
            	<img src="/images/admin/calander.gif" alt="날짜"align="middle" class="calan"/>
			
            </td>
        </tr>
        <tr><td colspan="2"><input type="image" src="/images/admin/submit.gif" title="Check" /></td></tr>

    </table>
    </form>
<?
	}
}
?>
    
</div>

</body>
</html>