
<script type="text/javascript">

	//게시판 체크박스 전체 선택
	function checkbox_all_check(obj, target) {
		if ( typeof obj == 'undefined' || typeof target == 'undefined') {

		}
		else if ( typeof target.length == 'undefined' ) {
			target.checked = obj.checked;
		}
		else
		{
			for (i = 0 ; i < target.length ; i++) {
				target[i].checked = obj.checked;
			}
		}
	}

	//deposit input
	function deposit_select() {
		var f  = document.adm_frm;
		var fg = false;

		check_count = 0;
		//var idxs = new Array();
		var customer_num = '';


		if(f.idx != null) {
			
			for(i=0; i<f.idx.length; i++) {

				if(f.idx[i].checked) {
					fg = true;

					//idxs[i] = " \' " + f.idx[i].value+ " \' ";
					customer_num = f.idx[i].value;


					check_count++;
				} /*else { idxs[i] = " \' \' "; }*/
			}

			//alert(customer_num); return;

			if(check_count == 1) {
				window.open('/?c=admin&m=adm_customer_deposit_popup_update&mcd=<?=$mcd?>&idx='+customer_num,'deposit','width=600,height=550,top=100,left=100'); 
				
				return;
			}

			if(check_count != 1) {
				alert('Please select one customer to deposit.');
				return;
			}

/*			alert(idxs[0]);
			alert(idxs);
			alert(f.idx);
				return;*/


			f.action = '/?c=admin&m=adm_customer_deposit_update&mcd=<?= $mcd ?>&idx='+f.idx;
			f.submit();
		}
	}


	//전체 삭제
	// function delete_all() {
	// 	var f  = document.adm_frm;
	// 	var fg = false;

	// 	if(f.idx != null) {
	// 		for(i=0; i<f.idx.length; i++) {
	// 			if(f.idx[i].checked) {
	// 				fg = true;
	// 			}
	// 		}

	// 		if(!fg) {
	// 			alert('Please select accounts that you want to delete.');
	// 			return;
	// 		}


	// 		if (!confirm('Do you want to delete this account?'))
	//         {
	//             return;

	//         }else{

	//         	f.action = '/?c=admin&m=adm_account_all_delete&mcd=<?= $mcd ?>';
	// 			f.submit();
	//         }
			
	// 	}
	// }

			//function modeify
	
	function modify_check(){
		var f  = document.adm_frm;
		check_count = 0;

		var customer_num = '';

		if(f.idx != null) {
			
			for(i=0; i<f.idx.length; i++) {
		
				if(f.idx[i].checked) {
					customer_num = f.idx[i].value;
					check_count++;
				} 
			}
			if(check_count == 0) {
				alert('Please select car to modify info.');
				return ;
			}else if(check_count >1){
				alert('Please select only one car to modify info.');
				return ;
				
			}else{
				f.action = '?c=admin&m=adm_account_update&id='+ customer_num+'&cur_page=<?= $cur_page ?>';
				f.submit();	
			}
		}
	}
	function exchangeRateClick(){
		window.open('/?c=admin&m=adm_account_rate&mcd=account_rate', 'shipment', 'width=600,height=550,top=100,left=100');

	}

</script>
<?php
   
   if(isset($_POST['sch_account_date_s'])) $sch_account_date_s=  htmlspecialchars($_POST['sch_account_date_s']);else $sch_account_date_s="";
   if(isset($_POST['sch_account_date_e'])) $sch_account_date_e=  htmlspecialchars($_POST['sch_account_date_e']);else $sch_account_date_e="";
   if(isset($_POST['sch_account_type'])) $sch_account_type=  htmlspecialchars($_POST['sch_account_type']);else $sch_account_type="";
   if(isset($_POST['sch_condition'])) $sch_condition=  htmlspecialchars ($_POST['sch_condition']);else $sch_condition="";
   if(isset($_POST['sch_word'])) $sch_word=  htmlspecialchars($_POST['sch_word']);else $sch_word="";
   
?>

<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_account" onsubmit="return search_bbs(this);">

	<div class="contents_box_middle">

		<h1 class="title">Account Management</h1>

		<div class="tip_box">

			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

			<ul>

				<li> - You search, add, modify account info</li>

			</ul>

		</div>

		<!--<div style="padding-top:5px;text-align:right;"><img src="/images/admin/btn_reset.gif" alt="초기화" /></div>-->

		<div class="table_list_box" style="padding-top:10px;">

			<h1 class="table_title">Account</h1>

			<div class="search_box">

				<div class="search_result">

					<img src="/images/admin/search_icon.gif" alt="Icon" align="absmiddle" />Total : <span class="title"><?= $total_rows ?></span> records, Pages : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>

				</div>
				<div class="search_condition">
					<input type="text" name="sch_account_date_s" data-date-format="yyyy-mm-dd"  value="<?php echo $sch_account_date_s; ?>" class="input_1 datepicker-add-options text_admin" data-auto-close="true" style="width:65px;" />

					
					<input type="text" name="sch_account_date_e" data-date-format="yyyy-mm-dd"  value="<?php echo $sch_account_date_e; ?>" class="input_1 datepicker-add-options text_admin" data-auto-close="true" style="width:65px;" />

					
					
					<select name="sch_account_type" id="sch_account_type" class="input_2" style="width:120px;" align="absmiddle">
						<option value="">- Account Type -</option>
                        <?php foreach (Array('Expense', 'Income') as $Val) { ?>
                              <option <?php echo $sch_account_type=(isset($_POST['sch_account_type']) && $_POST['sch_account_type']=="$Val"? 'selected="selected"':''); ?> 
                                 value="<?php echo $Val; ?>"><?php echo $Val; ?></option>
                        <?php } ?>

					</select>
					<select name="sch_condition" id="sch_condition" class="input_2" style="width:150px;" align="absmiddle">
						<option value="account_detail">Account Detail</option>
					<!-- 	<option value="account_name">Account Name</option> -->
						 <?php foreach (Array('account_name') as $Val) { ?>
                              <option <?php echo $sch_condition=(isset($_POST['sch_condition']) && $_POST['sch_condition']=="$Val"? 'selected="selected"':''); ?> 
                                 value="<?php echo $Val; ?>"><?php echo 'Account Name'; ?></option>
                        <?php } ?>
					</select>

					<input type="text" name="sch_word" id="sch_word" value="<?php echo $sch_word; ?>" class="input_1 text_admin" style="width:100px;" title="Searches" />

					

					<input type="image" src="/images/admin/btn_search_01.gif" class="search_admin" title="Search" align="absmiddle" />

				</div>

			</div>

			<div style="overflow:auto; float:left; width:100%;">

			<table class="table_list" cellspacing="0">

				<caption class="hidden"></caption>

				<col width="3%" /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col /><col width="2px" />



				<thead>

					<tr>

						<th scope="col"><input type="checkbox" name="idx" id="idx" /></th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Acc. No.</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">Account Type</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">Account Name</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">Account Detail</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">Currency</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">Unit Price</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">Unit Count</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">Total Income</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">Total Expense</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">Account Date</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						



					</tr>

				</thead>

				<tbody>


					<?

					//var_dump($query); exit();

					//일반글

					if (count($account_list) > 0) {

						//var_dump($bbs_list[2]->car_stock_no); exit();

						$bbs_link = ''; //링크

						$subject = ''; //제목

						$level_str = ''; //자식글

						foreach ($account_list as $rows) {
							//var_dump($rows); exit();
							
							//링크
							$bbs_link = "/?c=admin&m=adm_account_detail&mcd=$mcd&id=$rows->id&cur_page=$cur_page" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);


							//Secret 아이콘
							$secret_icon = '';

							// if (func_get_config($bbs_config_result, 'secret_yn') == 'Y' && $rows->secret_yn == 'Y')

							// 	$secret_icon = func_secret_icon($mcd);

							//New, Hot 아이콘
							//$icon = '&nbsp;&nbsp;' . func_new_icon(func_get_config($bbs_config_result, 'icon_new'), $rows->created_dt, $mcd) . '&nbsp;&nbsp;' . func_hot_icon(func_get_config($bbs_config_result, 'icon_hot'), $rows->visit_cnt, $mcd);

							//Totalsellprice
							//$total_sell_price = $rows->car_fob_cost + $rows->car_freight_fee;

							?>

							<tr>

								<td><input type="checkbox" class="chk" data-title="<?= $rows->id ?>"   name="idx[]" id="idx" value="<?= $rows->id ?>" /></td>
								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $row_cnt-- ?></a></td>
								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $rows->account_type ?></a></td>
								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $rows->account_name ?></a></td>
								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $rows->account_detail ?></a></td>
								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $rows->currency_type ?></a></td>
								<td class="line">&nbsp;</td>

								<td>
                                                                    <?php
                                                                      if($rows->unit_price=='' || $rows->unit_price==0 || $rows->unit_price==NULL){
                                                                          echo '';
                                                                      }
                                                                      else{
                                                                          
                                                                     
                                                                    ?>
									<?php
										if(isset($rows->tax_flg) && $rows->tax_flg == 'Tax')
										{
											echo number_format($rows->unit_price + $rows->tax_price);
										}else{
											echo number_format($rows->unit_price);
										}
                                                                               }
									?>
								</td>
								<td class="line">&nbsp;</td>

								<td><?= $rows->unit_count ?></td>
								<td class="line">&nbsp;</td>

								<td><?= $rows->total_income ?></td>
								<td class="line">&nbsp;</td>

								<td> <?php if($rows->total_expense==NULL || $rows->total_expense=='' || $rows->total_expense==0){
                                                                       echo '';
                                                                    }
                                                                    else{
                                                                        echo number_format($rows->total_expense);
                                                                    }
                                                                     ?></td>
								<td class="line">&nbsp;</td>

								<td><?= date('Y-m-d', strtotime($rows->account_date)); ?></td>
								<td class="line">&nbsp;</td>

								


							</tr>

						<? } ?>

					<? } ?>

					<? //데이터가 존재하지 않을 경우

					if (count($account_list) == 0) {

					?>

						<tr>

							<td colspan="20" class="no_data">Data does not exist.</td>

						</tr>

					<? } ?>



				</tbody>

			</table>

		</div>

			<div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_account&mcd=' . $mcd, $mcd) ?></div>

<!-- 			<? var_dump($_POST); ?>
			<? var_dump($_GET); ?>
			<? var_dump($total_rows); ?><br/>
			<? var_dump($sParam); ?>


			<? foreach ($bbs_list as $rows) { ?>
				<?= $rows->idx ?> 

			<? } ?>

			<? var_dump($bbs_list[0]->idx); ?> -->
			<? $query = $this->db->last_query(); ?>

			<div class="btn_area_center">
            	<div class="btn_area_left">
					<a href="#"  title="Exchange Rate" onclick="exchangeRateClick();"><span class="button blue">Exchange Rate</span></a>

                </div>
                <div class="btn_area_right">
                  	<a href="/?c=admin&amp;m=adm_account_write&amp;mcd=<?= $mcd ?>" title="등록"><span class="button blue">Register</span></a>
                    <a href="#" onclick="modify_check();" title="modify"><span class="button blue">Modify</span></a>
                   <!--  <a href="#" onclick="delete_all();" title="delete"><span class="button red">Delete</span></a> -->
                   <button type="button" id="btnDelete" class="button red disabled"><span></span> <b id='del'>Delete</b></button>
				</div>
			</div>
 
			<div class="search_box" style="padding:15px 10px;background-color:#ffffff">

				<!-- <img src="/images/admin/search_icon.gif" alt="Icon" align="absmiddle" />Selected records

				<a href="#" onclick="delete_all();" title="delete"><img src="/images/admin/btn_delete_eng.gif" alt="delete" align="middle" /></a> -->
				<img src="/images/admin/search_icon.gif" alt="Icon" align="absmiddle" />You search, add, modify account info
			</div>

		</div>

	</div>

</form>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
      </div>
      <div class="modal-body" id="msgBody">
         <!--  Message Here -->
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default" id="btn_yes">YES</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" id='btnNO'>NO</button>
      </div>
    </div>
  </div>
</div>

<!-- ? print_r($this->db->last_query()); ? -->

 <script type="text/javascript">
$(document).ready(function() {



        $("#btnDelete").click(btnDelete_click);

         $('#idx').click(function(event) {
		  if(this.checked) {
		      $(':checkbox').each(function() {
		          this.checked = true;
		          $('#btnDelete').removeClass('disabled');
		      });
		  }
		  else {
		    $(':checkbox').each(function() {
		          this.checked = false;
		          $('#btnDelete').addClass('disabled'); 
		      });
		  }
		});

        $('.chk').click(function(event) {   
		    if(this.checked) {
		        this.checked = true;
		        $('#btnDelete').removeClass('disabled');   
		    }
		    else{
		    	 $('#btnDelete').addClass('disabled'); 
		    }
        });
        
        $("#btn_yes").click(function (){
            var abc ='';
            $(".chk").each(function (){
                if ($(this).is(":checked") ===true)
                	

                    abc += $(this).attr("data-title") + ";";
            });
            var ajax = $.ajax({
                url: "/?c=admin&m=adm_account_all_delete&mcd=<?= $mcd ?>",
                type: 'POST',
                data : {
                    
                    id : abc    
                }
            });
            ajax.done(function (msg){
                $("#myModal").modal("hide");
                window.location.href = "/?c=admin&m=adm_account";
            });
        });


          function btnDelete_click(){
            if($(this).hasClass("disabled") === false){
                if($("#del").html().indexOf("Delete") !== -1){
                    $("#msgBody").html("Do you want to delete this account?");
                    $("#myModalLabel").html("Confirmation");
                    $('#btnNO').html('NO');
                    $("#btn_yes").show();
                    $("#myModal").modal("show");
                }
              
                else{
                     window.location.href = "/?c=admin&m=adm_account";
                }
            }
        }
        

    
    $(".datepicker-add-options").datepicker({
        
    });







});
 </script>
 

