<?

    //회원 상세 조회

    $rows = '';

    if(count($account_detail_list) > 0)

    {

       $rows = $account_detail_list->row();

    }

/*var_dump($rows);*/
?>
<script type="text/javascript">

	//게시판 등록
	//Start Account type
	var expense_json=<?php echo json_encode($account_type_expense_list);?>;
	var income_json=<?php echo json_encode($account_type_income_list);?>;
	var set_account_type='<?= $rows->account_type ?>';
	var set_account_id='<?= $rows->account_id ?>';
	var set_account_detail='<?= $rows->account_detail ?>';
	var set_currency_type='<?= $rows->currency_type ?>';
	var set_tax_flg='<?= $rows->tax_flg ?>';
	$(document).ready(function(e){
		selectAccountType();
		selectCurrencyType();
		selectTaxFlg();
		//calculation();
		$("#account_detail_input, #account_detail_select").change(function(){
			var selected_account_detail = $(this).val();
			$("#account_detail").val(selected_account_detail);
		});
		disableAll();
	});
	function disableAll(){
		$("input, select").css("color", "black");
		$('[name="values[account_memo]"]').replaceWith("<label>"+ $('[name="values[account_memo]"]').val() + "</label>");


		//$('[name="values[account_detail]"]').replaceWith("<label>"+ $('[name="values[account_detail]"]').val() + "</label>");

		$('[name="values[account_id]"]').replaceWith("<label>"+ $('[name="values[account_id]"]').find(":selected").text() + "</label>");

		$('[name="values[account_date]"]').replaceWith("<label>"+ $('[name="values[account_date]"]').val() + "</label>");
		
		$('[name="values[unit_count]"]').replaceWith("<label>"+ $('[name="values[unit_count]"]').val() + "</label>");
		
		$('[name="values[unit_value_price]"]').replaceWith("<label>"+ $('[name="values[unit_value_price]"]').val() + "</label>");
		
		$('[name="values[unit_total_price]"]').replaceWith("<label>"+ $('[name="values[unit_total_price]"]').val() + "</label>");
		
		$('[name="values[unit_price]"]').replaceWith("<label>"+ $('[name="values[unit_price]"]').val() + "</label>");
		
		$('[name="values[tax_flg]"]').replaceWith("<label>"+ $('[name="values[tax_flg]"]').val() + "</label>");
		
		$('[name="values[currency_type]"]').replaceWith("<label>"+ $('[name="values[currency_type]"]').val() + "</label>");
		
		$('[name="values[tax_price]"]').replaceWith("<label>"+ $('[name="values[tax_price]"]').val() + "</label>");
		
		$('#account_detail_select').replaceWith("");
		$('#account_detail_input').replaceWith("<label>"+ set_account_detail + "</label>");
		
		
		$('#account_type_1').attr("disabled", "disabled");
		$('#account_type_2').attr("disabled", "disabled");
		
		$('.qtyplus').css("display", "none");
		$('.qtyminus').css("display", "none");
		$('.date_picker').css("display", "none");
		//$("select").css("border", "none");
	}
	function selectCurrencyType(){
		$("#currency_type").val(set_currency_type);
	}
	function selectTaxFlg(){
		$("#tax_flg").val(set_tax_flg);
		if(set_tax_flg=='Tax'){
			$("#tax_price").removeAttr("readonly");
			$("#tax_price").attr("class","input_1");
			document.getElementById("tax_price_span").style.visibility = "visible";
		}else if(tax_flg=='Including'){
			$("#tax_price").attr("readonly","true");
			$("#tax_price").attr("class","input_3");
			document.getElementById("tax_price_span").style.visibility = "visible";
		}else{
			document.getElementById("tax_price_span").style.visibility = "hidden";
		}
	}
	function selectAccountType(){
		//alert($('[name="account_type"]:checked').val());
		var account_type=$('[name="account_type"]:checked').val();
		//var account_type=$("#account_type").val();
		$("#account_detail").val(set_account_detail);
		if(account_type=='Expense'){
			reloadAccountIdList(expense_json);
			$("#account_detail_input").css("display", "inline");
			$("#account_detail_select").css("display", "none");
			$("#account_detail_input").val(set_account_detail);
		}else{
			reloadAccountIdList(income_json);
			$("#account_detail_input").css("display", "none");
			$("#account_detail_select").css("display", "inline");
			$("#account_detail_select").val(set_account_detail);
		}
	}
	function reloadAccountIdList(account_type_json){
		var html;
		var len = account_type_json.length;
	    for (var i = 0; i< len; i++) {
	        html += '<option value="' + account_type_json[i].id + '">' + account_type_json[i].account_name + '</option>';
	    }
	    $('#account_id').html('');
	    $('#account_id').append(html);
	    if(set_account_id!=''){
	    	$('#account_id').val(set_account_id);
	    	set_account_id='';
	    }
	}
	//End Account type
	function showTaxPriceInput(e){
		var tax_flg = e.value;
		var tax = parseInt($('#tax_price').val());

		$('#unit_count').val(1);

		$('#tax_price').val(0);
		$('#unit_price').val(0);
		$('#unit_value_price').val(0);
		$('#unit_total_price').val(0);

		if(tax_flg=='Tax'){
			tax = 0;
			$("#tax_price").removeAttr("readonly");
			$("#tax_price").attr("class","input_1");
			document.getElementById("tax_price_span").style.visibility = "visible";
		}else if(tax_flg=='Including'){
			tax = 0;
			$("#tax_price").attr("readonly","true");
			$("#tax_price").attr("class","input_3");
			document.getElementById("tax_price_span").style.visibility = "visible";
		}else{
			tax = 0;
			document.getElementById("tax_price_span").style.visibility = "hidden";
		}
	}

	
	jQuery(document).ready(function(){
	    // This button will increment the value
	    $('.qtyplus').click(function(e){
	        // Stop acting like a button
	        e.preventDefault();
	        // Get the field name
	        fieldName = $(this).attr('field');
	        // Get its current value
	        var currentVal = parseInt($('#'+fieldName).val());
	        // If is not undefined
	        if (!isNaN(currentVal)) {
	            // Increment
	            $('#'+fieldName).val(currentVal + 1);
	        } else {
	            // Otherwise put a 0 there
	           $('#'+fieldName).val(0);
	        }
	    });
	    // This button will decrement the value till 0
	    $(".qtyminus").click(function(e) {
	        // Stop acting like a button
	        e.preventDefault();
	        // Get the field name
	        fieldName = $(this).attr('field');
	        // Get its current value
	        var currentVal = parseInt($('#'+fieldName).val());
	        // If it isn't undefined or its greater than 0
	        if (!isNaN(currentVal) && currentVal > 0) {
	            // Decrement one
	            $('#'+fieldName).val(currentVal - 1);
	        } else {
	            // Otherwise put a 0 there
	            $('#'+fieldName).val(0);
	        }
	    });
	});


	function calculation(){

		$('#unit_count').val(1);

		var tax_flg = $('#tax_flg').val();

		var price = parseInt($('#unit_price').val());
		var tax = parseInt($('#tax_price').val());
		var units = parseInt($('#unit_count').val());
		var value = parseInt($('#unit_value_price').val());
		
		
		//var result = ( value + tax ) * units;
		//alert(result);
		if(tax_flg == 'NoTax'){

			if(isNaN(price)||isNaN(tax)||isNaN(units)||isNaN(value)){
				
				$('#unit_price').val(0);
				$('#tax_price').val(0);
				$('#unit_value_price').val(0);
				$('#unit_total_price').val(0);

			}else{

				$('#tax_price').val(0);
				$('#unit_value_price').val(price*units);
				$('#unit_total_price').val(price*units);
			}

		} else if(tax_flg == 'Including'){

			if(isNaN(price)||isNaN(tax)||isNaN(units)||isNaN(value)){
				
				$('#unit_price').val(0);
				$('#tax_price').val(0);
				$('#unit_value_price').val(0);
				$('#unit_total_price').val(0);

			}else{

				$('#unit_value_price').val(price * 0.9*units);
				$('#tax_price').val(price * 0.1*units);
				$('#unit_total_price').val(price*units);

			}

		} else if(tax_flg == 'Tax'){
			
			if(isNaN(price)||isNaN(tax)||isNaN(units)||isNaN(value)){

				$('#unit_price').val(0);
				$('#tax_price').val(0);
				$('#unit_value_price').val(0);
				$('#unit_total_price').val(0);
				
			}else{

				$('#unit_value_price').val(price*units);
				$('#tax_price').val(tax);
				$('#unit_total_price').val((price+tax)*units);

			}


		}
		
	}


	function calculation_button_plus(){

		var tax_flg = $('#tax_flg').val();

		var price = parseInt($('#unit_price').val());
		var tax = parseInt($('#tax_price').val());
		var units = parseInt($('#unit_count').val()) + 1;
		var value = parseInt($('#unit_value_price').val());

		
		if(isNaN(price)) { alert("Input Unit Price First."); }
		
		if(tax_flg == 'Including'){
			var value_result = ( price * 0.9 ) * units;
			var total_result = price * units;
		}else{
			var value_result = price * units;
			var total_result = ( price + tax ) * units;
		}

		//alert(result);
		$('#unit_value_price').val(value_result);
		$('#unit_total_price').val(total_result);
		
	}

	function calculation_button_minus(){

		var tax_flg = $('#tax_flg').val();

		var price = parseInt($('#unit_price').val());
		var tax = parseInt($('#tax_price').val());
		var units = parseInt($('#unit_count').val());
		var value = parseInt($('#unit_value_price').val());

		if(units > 0){ units = units -1; } else { units = 0; }

		if(isNaN(price)) { alert("Input Unit Price First."); }
		
		if(tax_flg == 'Including'){
			var value_result = ( price * 0.9 ) * units;
			var total_result = price * units;
		}else{
			var value_result = price * units;
			var total_result = ( price + tax ) * units;
		}

		//alert(result);
		$('#unit_value_price').val(value_result);
		$('#unit_total_price').val(total_result);
		
	}

	function currency_selector(){
		
		var html;
		var currency_type = $('#currency_type').val();

	    html = currency_type;
	    
	    $(".currency_show").empty();
	   	$(".currency_show").append(html);

	}

</script>

<style type="text/css">
	.qty {
    width: 40px;
    height: 25px;
    text-align: center;
	}
	input.qtyplus { width:25px; height:25px;}
	input.qtyminus { width:25px; height:25px;}
</style>


	<div class="contents_box_middle">

		<h1 class="title">Account Info</h1>

		<div class="tip_box">

			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

			<ul>

				<li> - Update account info.</li>

			</ul>

		</div>

		<div class="table_write_box">

			<h1 class="table_title">Account Info</h1>
			<input type="hidden" value="<?php echo $rows->id; ?>" name="values[id]"/>
			<table class="table_write" summary=" " cellspacing="0">

				<caption class="hidden"></caption>

				<col width="18%" /><col width="32%" /><col width="18%" /><col width="32%" />

					
				<tr>
					<th class="key_02">Account Type</th>

					<td class="value_02">
						<input type="radio" id="account_type_1" value="Expense" name="account_type" onchange="selectAccountType();" <?php if($rows->account_type=="Expense") echo "checked"; ?>/>
						<label for="account_type_1">Expense</label>
						<input type="radio" id="account_type_2" value="Income" name="account_type" onchange="selectAccountType();" <?php if($rows->account_type=="Income") echo "checked"; ?>/>
						<label for="account_type_2">Income</label>
					</td>
				</tr>

				<tr>
					<th class="key_02">Account Name</th>

					<td class="value_02">
						[ <select name="values[account_id]" id="account_id" style="min-width:150px" required class="input_2">
							<!-- options -->
						</select> ]
						<input id="account_detail_input" placeholder="Account Detail"/>
						<select style="display:none" id="account_detail_select">
							<?php 
								foreach($customer_list as $customer){
							?>
								<option value="<?= $customer->customer_name ?>"><?= $customer->customer_name ?></option>
							<?php 
								}
							?>
						</select>
						<input type="hidden" name="values[account_detail]" value="" id="account_detail"/>
					</td>


					<th class="key_02">Account Date</th>

					<td class="value_02" colspan="3">

						<input type="text" name="values[account_date]" id="account_date" readonly value="<?= $rows->account_date ?>" class="input_1" style="width:100px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="13" />
						<a href="#" class="date_picker" onclick="Calendar(document.adm_frm.elements['account_date']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
					</td>

				</tr>

				</tr>


				<tr>

					<th class="key_02">Unit Price</th>

					<td class="value_02">
						<input type="text" name="values[unit_price]" id="unit_price" value="<?= $rows->unit_price; ?>" class="input_1" style="width:150px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="13" onchange="calculation();"/>
						
						<!-- currency selector -->
						<select name="values[currency_type]" id="currency_type" class="input_2" onchange="currency_selector();">
							
							<option value="USD" <?php if($rows->account_type=="Expense") echo "checked"; ?>>USD</option>
							<option value="KRW" >KRW(₩)</option>
							<option value="JPY" >JPY(¥)</option>
							<option value="EUR" >EUR(€)</option>

						</select>
						
					</td>

					<th class="key_02">Unit Count</th>

					<td class="value_02">
						<input type="text" name="values[unit_count]" id="unit_count" value="<?= $rows->unit_count ?>" class="input_1" style="width:50px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="13" onchange="calculation();"/>
						<!-- unit count button -->
						<input type='button' value='+' class='qtyplus' field='unit_count' onclick="calculation_button_plus();"/><input type='button' value='-' class='qtyminus' field='unit_count' onclick="calculation_button_minus();"/>
					</td>

				</tr>


				<tr>

					<th class="key_02">Unit Tax</th>

					<td class="value_02">

						<select name="values[tax_flg]" id="tax_flg" class="input_2" onchange="showTaxPriceInput(this);">
							
							<option value="NoTax" >No Tax</option>
							<option value="Including" >Including</option>
							<option value="Tax" >Tax</option>

						</select>
						<span style="visibility:hidden" id="tax_price_span">
							<input type="text" name="values[tax_price]" id="tax_price" value="<?= $rows->tax_price ?>" class="input_1" style="width:150px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="13" onchange="calculation();"/>
							<span class="currency_show"><?=$rows->currency_type ?></span>
						</span>
					</td>

					<th class="key_02">Unit Value Price</th>

					<td class="value_02" colspan="3">

						<input type="text" name="values[unit_value_price]" id="unit_value_price" required value="<?=$rows->unit_value_price?>" class="input_3" style="width:150px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="13" readonly/>
						<span class="currency_show"><?=$rows->currency_type ?></span>
					</td>

				</tr>

				<tr>

					<th class="key_02"></th>

					<td class="value_02">
						
					</td>

					<th class="key_02" style="background:#FDBFBF;">Unit Total Price</th>

					<td class="value_02" colspan="3">

						<input type="text" name="values[unit_total_price]" id="unit_total_price" required value="<?=$rows->unit_total_price?>" class="input_3" style="width:150px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="13" readonly/>
						<span class="currency_show"><?=$rows->currency_type ?></span>
					</td>

				</tr>
				

				<tr>

					<th class="key_02">Memo</th>

					<td class="value_02" colspan="3" style="padding:10px;">

						<textarea name="values[account_memo]" id="contents" class="textarea"><?= $rows->account_memo ?></textarea>


					</td>

				</tr>


			</table>

			<div class="btn_area_center">

                <div class="btn_area_right">
                    <a href="/?c=admin&amp;m=adm_account_update&amp;id=<?= $rows->id ?>&amp;cur_page=<?=$cur_page?>" title="edit"><span class="button blue">Edit</span></a>
                    <a href="/?c=admin&amp;m=adm_account&amp;cur_page=<?=$cur_page?>" title="back"><span class="button blue">Back</span></a>
                </div>

            </div>


		</div>

	</div>
