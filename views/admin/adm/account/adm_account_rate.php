<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Exchange Rate List</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script type="text/javascript" src="/js/common/common.js"></script>
<link rel="stylesheet" type="text/css" href="/css/admin/button.css" />
<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />

<!-- link calendar resources -->
<link rel="stylesheet" type="text/css" href="/css/tcal.css" />
<script type="text/javascript" src="/js/tcal.js"></script> 

<script type="text/javascript">
    //refresh page


    window.onunload = function(){
      window.opener.location.reload();
    };
	function exit_win(){
		window.close();
	}
	
	function insert_adm_bbs(frm) {
        if (!validate(frm.elements['values[exchange_rate]'], 'INPUT [Exchange Rate]'))
            return false;
        if (!validate(frm.elements['values[exchange_dt]'], 'INPUT [Exchange Date]'))
            return false;

    }
	
	 //게시판 체크박스 전체 선택
    function checkbox_all_check(obj, target) {
        if (typeof obj == 'undefined' || typeof target == 'undefined') {

        }
        else if (typeof target.length == 'undefined') {
            target.checked = obj.checked;
        }
        else
        {
            for (i = 0; i < target.length; i++) {
                target[i].checked = obj.checked;
            }
        }
		
    }
	
	function modify_check() {
        var f = document.adm_frm_rate;
        check_count = 0;
        if (f.id != null) {

            for (i = 0; i < f.id.length; i++) {

                if (f.id[i].checked) {
                    check_count++;
                }
            }
            if (check_count == 0) {
                alert('Please select Exchange rate to Modify');
                return;
            } else if (check_count > 1) {
				
				window.location.reload();
                alert('Please select only one Rate to modify info.');
				
                return false;

            } else {
                f.action = '?c=admin&m=adm_account_rate_modify&mcd=<?php echo $mcd; ?>&id=' + f.id;
                f.submit();
            }
        }
    }

	//------------delete
	
	function delete_rate() {
        var f = document.adm_frm_rate;
        check_count = 0;
        if (f.id != null) {

            for (i = 0; i < f.id.length; i++) {

                if (f.id[i].checked) {
                    check_count++;
                }
            }
            if (check_count == 0) {
                alert('Please select Exchange rate to delete');
                return;
            } else {
                f.action = '?c=admin&m=adm_account_rate_delete_exec&mcd=<?php echo $mcd; ?>&id=' + f.id;
                f.submit();
            }
        }
    }
   
</script>
</head>

<body >
<div class="warp_rate">
<form name="adm_frm_rate" id="adm_frm_rate" method="post" enctype="multipart/form-data" action="" onsubmit="">
<table width="500px" cellpadding="0" cellspacing="0">
	<colgroup>
		<col width="3%"/>
        <col width="12%"/>
        <col width="15%"/>
        <col width="15%"/><col width="32%"/>
        <col width="25%"/>

    </colgroup>

	<tr>
        <th colspan="6" class="box_title" align="left">
            	Exchange Rate List
        </th>
        
    </tr>
    <tr>
    	<th><input type="checkbox" name="id" id="id" value="" onclick="checkbox_all_check(this, document.adm_frm_rate.id);"/></th>
        <th>ID</th>
        <th>From</th>
        <th>To</th>
        <th>Rate</th>
        <th>Date</th>
    </tr>
    

<? 
		$i=1;
	//if(count($account_rate_list)>0){
		foreach($account_rate_list as $row){
	?>
	<tr class="line" valign="middle">
    	<th ><input type="checkbox" name="id[]" id="id" value="<?= $row->id ?>" /></th>
        <th><?=$i?></th>
        <th><?=$row->from_currency?></th>
        <th><?=$row->to_currency?></th>
        <th><?=$row->exchange_rate?></th>
        <th><?=$row->exchange_dt?></th>
    </tr>		
	<?	
		$i++;	
		}
	//}

?>
    <tr>
        <td colspan="6">
            <ul>
                <li><a href="#" onClick="exit_win()"><span class="button blue">Exit</span></a>
                <li><a href="#" onClick="delete_rate()"><span class="button blue">Delete</span></a>
                <li><a href="#" onClick="modify_check()"><span class="button blue">Modify</span></a>
                <li><a href="#add"><span class="button blue">Add</span></a></li>
            </ul>	
        </td>
    </tr>
</table>
</form>
<a href="#x" class="overlay" id="add"></a>
<div class="popup">
	<form name="adm_frm" id="adm_frmb" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_account_rate_exec&amp;mcd=<?= $mcd ?>" onsubmit="return insert_adm_bbs(this);">

    <table width="220px" cellpadding="0" cellspacing="0" class="tbl_addrate">
        <colgroup>
            <col width="50%"/>
            <col width="50%"/>
        </colgroup>
    
        <tr>
            <th colspan="2" class="box_title" align="left">
            	Current Exchange Rate
            </th>
        </tr>
        <tr>
            <td>From</td>
            <td>To</tdh>
        </tr>
        <tr>
            <td>
            	<select name="values[from_currency]" id="" class="input_2" onchange="currency_selector();">
							
                    <option value="USD" >USD</option>
                    <option value="KRW" >KRW</option>
                    <option value="JPY" >JPY</option>
                    <option value="EUR" >EUR</option>
  
                </select>
            </td>
            <td align="left">
            	<select name="values[to_currency]" id="" class="input_2"  onchange="currency_selector();">
							
                    <option value="USD" >USD</option>
                    <option value="KRW" >KRW</option>
                    <option value="JPY" >JPY</option>
                    <option value="EUR" >EUR</option>
  
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="text" id="exchange_rate" name="values[exchange_rate]" placeholder="Rate" autocomplete="off"></td>
        </tr>
        <tr>
            <td colspan="2"><input type="text" id="exchange_dt" name="values[exchange_dt]" placeholder="Exchange Date" class="tcal" value="" readonly >
            
            	<img src="/images/admin/calander.gif" alt="날짜"align="middle" class="calan"/>
			
            </td>
        </tr>
        <tr><td colspan="2"><input type="image" src="/images/admin/submit.gif" title="Check" /></td></tr>

    </table>
    <a class="close" href="#close" ></a>
    </form>
</div>

</div>
</body>
</html>