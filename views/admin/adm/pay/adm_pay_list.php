<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_pay_list" onsubmit="">
	<div class="contents_box_middle">
		<h1 class="title">회원 결제 관리</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 회원결제정보를 조회, 수정, 삭제등의 관리를 할 수 있습니다.</li>
			</ul>
		</div>
		<div class="table_list_box">
			<h1 class="table_title">회원결제정보</h1>
			<div class="search_box">
				<div class="search_result">
					<img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />전체 : <span class="title"><?= $total_rows ?></span> 건, 페이지 : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>
				</div>
				<div class="search_condition">
					<select name="sch_condition" id="sch_condition" class="input_2" style="width:100px;" align="absmiddle">
						<option value="lgd_buyerid like">아이디</option>
						<option value="lgd_buyer like">신청자명</option>
						<option value="lgd_productinfo like">강의명</option>
					</select>
					<input type="text" name="sch_word" id="sch_word" value="" class="input_1" style="width:100px;" title="검색어" />
					<input type="image" src="/images/admin/btn_search_01.gif" title="검색" align="absmiddle" />
				</div>
			</div>
			<table class="table_list" summary="결제정보" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="" /><col width="2px" />
				<col width="" /><col width="2px" />
				<col width="" /><col width="2px" />
				<col width="" /><col width="2px" />
				<col width="" /><col width="2px" />
				<col width="" /><col width="2px" />
				<col width="" /><col width="2px" />
				<col width="" /><col width="2px" />
				<col width="" />
				<thead>
					<tr>
						<th scope="col">번호</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">주문번호</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">아이디</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">구매자명</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">상품명</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">결제금액</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">수강시작일</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">수강종료일</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">수강상태</th>
					</tr>
				</thead>
				<tbody>
					<?
					//일반글
					if (count($payment_list) > 0) {
						$link = ''; //링크

						foreach ($payment_list as $rows) {
//링크
							$link = "/?c=admin&m=adm_pay_detail&seq_no=$rows->seq_no&cur_page=$cur_page" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);
							?>
							<tr>
								<td><?= $row_cnt-- ?></td>
								<td class="line">&nbsp;</td>
								<td><a href="<?= $link ?>" title="결제상세"><?= $rows->lgd_oid ?></a></td>
								<td class="line">&nbsp;</td>
								<td><?= $rows->lgd_buyerid ?></td>
								<td class="line">&nbsp;</td>
								<td><?= $rows->lgd_buyer ?></td>
								<td class="line">&nbsp;</td>
								<td><?= $rows->lgd_productinfo ?></td>
								<td class="line">&nbsp;</td>
								<td>\<?= func_num_comma($rows->lgd_amount) ?></td>
								<td class="line">&nbsp;</td>
								<td><?= func_get_date_format($rows->start_dt, 'Y-m-d') ?></td>
								<td class="line">&nbsp;</td>
								<td><?= func_get_date_format($rows->end_dt, 'Y-m-d') ?></td>
								<td class="line">&nbsp;</td>
								<td>&nbsp;
									<?
									if ($rows->course_status == 'Y')
										echo '정상';
									if ($rows->course_status == 'S')
										echo '중지';
									if ($rows->course_status == 'N')
										echo '만료';
									?>
								</td>
							</tr>
						<? } ?>
					<? } else { ?>

						<tr>
							<td colspan="20" class="no_data">Data does not exist.</td>
						</tr>
<? } ?>

				</tbody>
			</table>
			<div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_pay_list') ?></div>
			<div class="btn_area_right">

			</div>
		</div>
	</div>
</form>