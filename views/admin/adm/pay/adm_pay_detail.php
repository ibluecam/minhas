<?
//결제정보 상세 조회
$rows = '';
if (count($payment_detail_list) > 0) {
	$rows = $payment_detail_list->row();
}
?>
<script type="text/javascript">

	//결제정보 수정
	function update_adm_payment(frm) {
		if(!confirm('수정하시겠습니까?')) {
			return false;
		}

		return true;
	}

</script>
<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_pay_update" onsubmit="return update_adm_payment(this);">
	<input type="hidden" name="values[seq_no]" id="seq_no" value="<?= $seq_no ?>" />
	<input type="hidden" name="cur_page" value="<?= $cur_page ?>" />
	<input type="hidden" name="sParam" value="<?= $sParam ?>" />
	<div class="contents_box_middle">
		<h1 class="title">회원 결제 관리</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 회원결제정보를 조회, 수정, 삭제등의 관리를 할 수 있습니다.</li>
			</ul>
		</div>
		<div class="table_write_box">
			<h1 class="table_title">회원결제상세정보</h1>
			<table class="table_write" summary="회원결제상세정보" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="16%" /><col width="34%" /><col width="16%" /><col width="34%" />
				<tr>
					<th class="key_02">주문번호</th>
					<td class="value_02" colspan="3"><strong>&nbsp;<?= $rows->lgd_oid ?></strong></td>
				</tr>
				<tr>
					<th class="key_02">결제구분</th>
					<td class="value_02">
						<?
						if ($rows->lgd_paytype == 'SC0010')
							echo '신용카드(' . $rows->lgd_financename . ' ' . $rows->lgd_financeauthnum . ')';
						else if ($rows->lgd_paytype == 'SC0030')
							echo '계좌이체(' . $rows->lgd_financename . ')';
						else if ($rows->lgd_paytype == 'SC0040')
							echo '가상계좌(무통장)(' . $rows->lgd_financename . ' ' . $rows->lgd_accountnum . ')';
						else
							echo '(' . $rows->lgd_financename . ' ' . $rows->lgd_financeauthnum . ')';
						?>

					</td>
					<th class="key_02">결제금액</th>
					<td class="value_02"><strong>&nbsp;\<?= func_num_comma($rows->lgd_amount) ?>원</strong></td>
				</tr>
				<tr>
					<th class="key_02">결과코드</th>
					<td class="value_02">&nbsp;<?= $rows->lgd_respcode ?></td>
					<th class="key_02">결과메시지</th>
					<td class="value_02">&nbsp;<?= $rows->lgd_respmsg ?></td>
				</tr>
				<tr>
					<th class="key_02">아이디</th>
					<td class="value_02">&nbsp;<?= $rows->lgd_buyerid ?></td>
					<th class="key_02">구매자명</th>
					<td class="value_02">&nbsp;<?= $rows->lgd_buyer ?></td>
				</tr>
				<tr>
					<th class="key_02">상품코드</th>
					<td class="value_02">&nbsp;<?= $rows->lgd_productcode ?></td>
					<th class="key_02">상품명</th>
					<td class="value_02">&nbsp;<?= $rows->lgd_productinfo ?></td>
				</tr>
				<tr>
					<th class="key_02">휴대폰번호</th>
					<td class="value_02">&nbsp;<?= $rows->lgd_buyerphone ?></td>
					<th class="key_02">이메일</th>
					<td class="value_02">&nbsp;<?= $rows->lgd_buyeremail ?></td>
				</tr>
				<tr>
					<th class="key_02">주소</th>
					<td class="value_02" colspan="3">&nbsp;<?= $rows->lgd_buyeraddress ?></td>
				</tr>
				<tr>
					<th class="key_02">수강상태</th>
					<td class="value_02">
						<input type="radio" name="values[course_status]" id="course_status_01" value="Y" class="radio" <?= func_decode($rows->course_status, 'Y', 'checked', '') ?> />
						<label for="course_status_01">정상</label>
						<input type="radio" name="values[course_status]" id="course_status_02" value="S" class="radio" <?= func_decode($rows->course_status, 'S', 'checked', '') ?> />
						<label for="course_status_02">중지</label>
						<input type="radio" name="values[course_status]" id="course_status_03" value="N" class="radio" <?= func_decode($rows->course_status, 'N', 'checked', '') ?> />
						<label for="course_status_03">만료</label>
					</td>
					<th class="key_02">재수강여부</th>
					<td class="value_02">
						<input type="radio" name="values[re_course_yn]" id="re_course_yn_01" value="N" class="radio" <?= func_decode($rows->re_course_yn, 'N', 'checked', '') ?> />
						<label for="re_course_yn_01">수강</label>
						<input type="radio" name="values[re_course_yn]" id="re_course_yn_02" value="Y" class="radio" <?= func_decode($rows->re_course_yn, 'Y', 'checked', '') ?> />
						<label for="re_course_yn_02">재수강</label>
					</td>
				</tr>
				<tr>
					<th class="key_02">수강기간</th>
					<td class="value_02" colspan="3">
						<input type="text" name="values[start_dt]" id="start_dt" value="<?= func_get_date_format($rows->start_dt, 'Y-m-d') ?>" class="input_1" style="width:65px;" readonly />
						<a href="#" onclick="Calendar(document.adm_frm.elements['values[start_dt]']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
						~
						<input type="text" name="values[end_dt]" id="end_dt" value="<?= func_get_date_format($rows->end_dt, 'Y-m-d') ?>" class="input_1" style="width:65px;" readonly />
						<a href="#" onclick="Calendar(document.adm_frm.elements['values[end_dt]']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
						<span class="text">(?! 일반수강:60일, 재수강:30일) 수강기간이 지나면 자동 만료가 됩니다.</span>
					</td>
				</tr>
				<tr>
					<th class="key_02">수강중지여부</th>
					<td class="value_02" colspan="3">
						<strong>(<?= func_decode($rows->course_stop_cnt, '0', '미중지', '중지') ?>)</strong>,
						횟수 : <input type="text" name="values[course_stop_cnt]" id="course_stop_cnt" value="<?= $rows->course_stop_cnt ?>" class="input_1" style="width:20px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="2" />
						<span class="text">(?! 기본 수강 중지는 1회, 중지시 90일 연장, 숫자 1이상 입력시 강의중지 ex) 숫자 2입력 90일+90일 연장)</span>
					</td>
				</tr>
				<tr>
					<th class="key_02">수강중지기간</th>
					<td class="value_02" colspan="3">
						&nbsp;<?= $rows->course_stop_sdt ?> ~ <?= $rows->course_stop_edt ?>
					</td>
				</tr>
				<tr>
					<th class="key_02">결제일</th>
					<td class="value_02" colspan="3"><strong>&nbsp;<?= $rows->pay_dt ?></strong></td>
				</tr>
				<tr>
					<th class="key_02">최근접속일</th>
					<td class="value_02"><strong>&nbsp;<?= $rows->last_login_dt ?></strong></td>
					<th class="key_02">방문수</th>
					<td class="value_02"><strong>&nbsp;<?= $rows->login_cnt ?></strong></td>
				</tr>
			</table>
			<div class="btn_area_center">
				<input type="image" src="/images/admin/btn_modify.gif" title="수정" />
				<a href="/?c=admin&amp;m=adm_pay_list&amp;cur_page=<?= $cur_page ?><?= func_sPUrlEncode($sParam, "sch_word") ?>" title="목록"><img src="/images/admin/btn_list.gif" alt="목록" /></a>
			</div>
		</div>
	</div>
</form>