
<html lang="en" >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title>Customer Deposit</title>
        <script type='text/javascript' src='../../js/common/jquery-1.7.2.js'></script>
        <script type="text/javascript" src="../../js/jquery.simplePagination.js"></script>

        <link href="css/simplePagination.css" rel="stylesheet" type="text/css">




        <!-- <script type="text/javascript" src="/js/common/common.js"></script> -->
        <link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />


        <!-- link calendar resources -->
        <link rel="stylesheet" type="text/css" href="/css/tcal.css" />
        <!-- <script type="text/javascript" src="/js/tcal.js"></script>  -->

        <!-- <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> -->

        <script type="text/javascript" src="../../js/common/jquery.fancybox-1.3.4.js"></script>

        <link rel="stylesheet" type="text/css" href="../../css/common/jquery.fancybox-1.3.4.css" media="screen" />
        <style type="text/css">
            .btn_container, #search_container{
                padding: 5px 0;
            }
            #btn_container1 {
                background: none repeat scroll 0 0 #eee;
                border-left: 1px solid #aaa;
                border-right: 1px solid #aaa;
                text-align: center;
            }
            #container2{
                min-height: 50px;
                border: 1px solid #aaa;
            }

            #container2 table{
                border-collapse: collapse;
                width: 100%;
            }
            #container2 td{
                padding: 5px;
                background: #eee;
            }

            #container2 tr {
                border-bottom: 1px solid #aaa;
            }
            #customer_distribute{
                padding: 10px;

            }

            .container.balance_container {
                font-weight: bold;
                padding: 5px;
            }
            .container.balance_container .currency {
                color: red;
                font-size: 16px;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <header>
            <h2>Customer Deposit</h2>
            <span>Please Insert Deposit Amount</span>
        </header>
 
        <!-- panel with buttons -->
        <div class="main">
            <div class="panel">
                <a href="#customer_distribute" class="fancybox" id="status_pop">Distribute</a>
                <!-- <a href="#shipment_info" id="shipment_pop">Deposit</a> -->
            </div>
            

            <div class="CSSTableGenerator" >
                <table >
                    <thead>
                        <tr style="height:30px;">
                            <td>
                                No.
                            </td>
                            <td >
                                Deposit
                            </td>
                            <td>
                                Allocated
                            </td>
                            <td>
                                Date
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <? foreach($dist_list as $rows) { ?>

                            <tr style="height:30px;">
                                <td >
                                    <?= $rows->pay_idx ?>
                                </td>
                                <td>
                                    <? if(isset($rows->car_deposit)) { ?>

                                        <? switch ($rows->currency_type) {
                                            case 'USD':
                                                echo '$';
                                                break;
                                            case 'KRW':
                                                echo '₩';
                                                break;    
                                            case 'JPY':
                                                echo 'Ұ';
                                                break;
                                            case 'EUR':
                                                echo '€';
                                                break;   
                                            default:
                                                echo ' ';
                                                break;
                                        } ?>

                                        <?= $rows->car_deposit ?>

                                    <? } ?>
                                </td>
                                <td>
                                    <? if(isset($rows->car_allocate)) { ?>

                                        <? switch ($rows->currency_type) {
                                            case 'USD':
                                                echo '$';
                                                break;
                                            case 'KRW':
                                                echo '₩';
                                                break;    
                                            case 'JPY':
                                                echo 'Ұ';
                                                break;
                                            case 'EUR':
                                                echo '€';
                                                break;   
                                            default:
                                                echo ' ';
                                                break;
                                        } ?>

                                        <?= $rows->car_allocate ?>

                                    <? } ?>
                                </td>
                                <td>
                                	<? if(!isset($rows->allocate_dt)){ ?>
                                    	<?= $rows->deposit_dt ?>
                                    <? } else { ?>
                                    	<?= $rows->allocate_dt ?>
                                    <? } ?>	
                                </td>
                            </tr>
                        <?  } ?>
                    </tbody>
                </table>

            </div>
            
            <div id="light-pagination" class="pagination-page"></div>

        </div>
        <div style="display:none">
            <div id="customer_distribute">
                <div id="search_container" class="container">
                    <div><b>Distribute to Customer</b></div>
                    <select id="sch_condition">
                        <option value="customer_name">Customer Name</option>
                    </select> 
                    <input id="sch_word" type="text" /> <input type="button" id="search_btn" value="Search" />
                </div>
                <div class="container balance_container">
                    <u>BALANCE</u>: <span id="deposit_balance_label" class="currency"><?= $deposit_detail->deposit ?> <?= $deposit_detail->currency_type ?></span>
                    <input type="hidden" name="deposit_balance" id="deposit_balance" value="<?=$deposit_detail->deposit?>" />
                </div>
                <div id="container1" class="container">
                    <select multiple="multiple" id='select_group1' style="width:100%;height:130px;overflow:auto;">
                        <?php
                            foreach($customer_list as $customer){
                                $customer_name = ucfirst($customer->member_first_name).' '.ucfirst($customer->member_last_name);
                                echo '<option class="group1_customer_'.$customer->member_no.'" data-customer-name="'.$customer_name.'" value="'.$customer->member_no.'">'.$customer_name.' ('.$customer->email.')'.'</option>';
                            }
                        ?>
                    </select>
                </div>
                <div id="btn_container1" class="btn_container">
                    <input type="button" value="▽" id="group_add"/>
                    <input type="button" value="△" id="group_remove"/>
                </div>
                <form action="/?c=admin&m=adm_customer_ditribute_insert_exec" method="post">
                    <input type="hidden" name="deposit_id" value="<?=$deposit_id?>" />
                    <input type="hidden" id="is_posted" value="<?= $is_posted ?>"/>
                    <input type="hidden" name="currency_type" value="<?= $deposit_detail->currency_type ?>" />
                    <div id="container2" class="container">
                        <table id="select_group2">
                            
                            
                        </table>
                    </div>
                    <div id="btn_container2" class="btn_container">
                        <input type="submit" id="btn_save" value="Save"/> <input type="button" id="btn_close" value="Close"/>
                    </div>
                </form>
            </div>
        </div>
<script>
   var base_url = "<?=base_url()?>";

$(document).ready(function(){
    //reloadCarList();
    var is_post = $("#is_posted").val();
    if(is_post=='yes'){
        refreshMainPage();
    }
    reloadCustomerList();
 
    
    $("#search_btn").click(function(){
        reloadCustomerList();
        getSelectedCustomer();
        return false;
    });
    $("#group_add").click(function(){
        var selected_group1 = []; 
        $('#select_group1 :selected').each(function(i, selected){ 
            var customer_no = $(selected).val();
            var customer_name = $(selected).attr("data-customer-name");
            add_customer_to_distribute(customer_no, customer_name); 
        });
    });
    $("#group_remove").click(function(){
        var selected_group1 = []; 
        $('#select_group2 :checked').each(function(i, selected){ 
            var customer_no = $(selected).val();
            remove_customer_from_distribute(customer_no); 
        });
    });
    $("#btn_close").click(function(e){
        $.fancybox.close();
    });
    $("#btn_save").click(function(e){
        var allow_save=false;
        var deposit_balance = $("#deposit_balance").val();
        var total_distribute = total_distribute_amount();
        if(total_distribute<=0){
            allow_save = false;
            error_msg = "Please insert distribute amount";
        }else if(total_distribute > deposit_balance){
            allow_save = false;
            error_msg = "Not enough balance to distribute";
        }else{
            allow_save = true;
            
        }
        if(allow_save == false){
            alert(error_msg);
        }
        return allow_save;
    });
    $("a.fancybox").fancybox({
        height: "100%",
        width: "450",
       'scrolling' : 'yes',
       'autoDimensions'    : false
    });

    
});

$(document).on("change", ".txt_dist", function(e){
    var deposit_balance = $("#deposit_balance").val();
    var total_distribute = total_distribute_amount();
    $("#deposit_balance_label").html(parseInt(deposit_balance) - total_distribute);
    $("#deposit_balance_label").append(" USD");
    

});
function refreshMainPage(){
    opener.location.reload(); // or opener.location.href = opener.location.href;
}
    

function total_distribute_amount(){
    var sum = 0;
    $('.txt_dist').each(function(){
        if(this.value!=''&& $.isNumeric(this.value)){
            sum += parseInt(this.value);
        }     
    });
    return sum;
}
function add_customer_to_distribute(customer_no, customer_name){
    var td1 = '<td><input class="customer_dist_chk" type="checkbox" value="'+customer_no+'"/></td>';
    var td2 = '<td>'+customer_name+'</td>';
    var td3 = '<td><input type="text" required name="customer_no['+customer_no+']" class="txt_dist" placeholder="Distribute Amount"/></td>';
    
    var tr = '<tr class="tr_customer_'+customer_no+'">'+td1+td2+td3+"</tr>";
    $("#container2 #select_group2").prepend(tr);
    /*** Hide customer selected customer from list 1 ***/
    $(".group1_customer_"+customer_no).css("display", "none");
}

function remove_customer_from_distribute(customer_no){
    $(".tr_customer_"+customer_no).remove();
    /*** Show customer removed customer back to list 1 ***/
    $(".group1_customer_"+customer_no).css("display", "block");
}

function reloadCustomerList(){
   
    var sch_condition = $("#sch_condition").val();
    var sch_word = $("#sch_word").val();
    var selected_customers = getSelectedCustomer();
    var data = {
        selected_customers : selected_customers
    };
    $.ajax({
        method: "GET",
        url: "/?c=admin&m=adm_customer_option_for_distribute&sch_condition="+sch_condition+"&sch_word="+sch_word,
        data: data
        })
        .done(function( data ) {
            $("#select_group1").html(data);
            return false;
        });
}
function getSelectedCustomer(){
    var selectedCars = [];
    $("#select_group2 > .customer_dist_chk").each(function(){
        selectedCars.push(this.value);
    });
    return selectedCars;
}
</script>
    </body>
</html>