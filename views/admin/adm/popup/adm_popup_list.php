<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_popup_list" onsubmit="">

	<div class="contents_box_middle">

		<h1 class="title">Pop-up management</h1>

		<div class="tip_box">

			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

			<ul>

				<li> - You can manage the registered popup.</li>

			</ul>

		</div>

		<div class="table_list_box">

			<h1 class="table_title">Pop-up management</h1>

			<div class="search_box">

				<div class="search_result">

					<img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />전체 : <span class="title"><?= $total_rows ?></span> 건, 페이지 : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>

				</div>

				<div class="search_condition">

					<select name="sch_condition" id="sch_condition" class="input_2" style="width:80px;" align="absmiddle">

						<option value="subject like">Subject</option>

						<option value="contents like">Contents</option>

						<option value="subject like|contents like">Subject+Contents</option>

					</select>

					<input type="text" name="sch_word" id="sch_word" value="" class="input_1" style="width:100px;" title="검색어" />

					<input type="image" src="/images/admin/btn_search_01.gif" title="검색" align="absmiddle" />

				</div>

			</div>

			<table class="table_list" summary="팝업관리" cellspacing="0">

				<caption class="hidden"></caption>

				<col width="7%" /><col width="2px" />

				<col width="" /><col width="2px" />

				<col width="10%" /><col width="2px" />

				<col width="15%" /><col width="2px" />

				<col width="15%" />

				<thead>

					<tr>

						<th scope="col">No.</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Popup Subject</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Post Status</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Registration Date</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Preview</th>

					</tr>

				</thead>

				<tbody>



					<?

					//일반글

					if (count($popup_list) > 0) {



						$popup_link = ''; //링크

						$subject = ''; //제목

						foreach ($popup_list as $rows) {

//링크

							$popup_link = "/?c=admin&m=adm_popup_detail&idx=$rows->idx&cur_page=$cur_page" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);



							$subject = $rows->subject;

							?>

							<tr>

								<td><?= $row_cnt-- ?></td>

								<td class="line">&nbsp;</td>

								<td style="text-align:left;"><a href="<?= $popup_link ?>" title="내용보기"><?= $subject ?></a></td>

								<td class="line">&nbsp;</td>

								<td><?= func_decode($rows->open_yn, 'Y', '게시', '미게시') ?></td>

								<td class="line">&nbsp;</td>

								<td><?= func_get_date_format($rows->created_dt, 'Y-m-d') ?></td>

								<td class="line">&nbsp;</td>

								<td><a href="#" onclick="win_open('/?c=admin&m=adm_popup_preview&idx=<?= $rows->idx ?>', 'preview', '0', '0', '<?= $rows->win_width ?>', '<?= $rows->win_height ?>', '<?= func_decode($rows->scroll_yn, 'Y', 'yes', 'no') ?>'); return false;" title="미리보기"><img src="/images/admin/btn_view.gif" alt="미리보기" /></a></td>

							</tr>

						<? } ?>

					<? } ?>

<? if (count($popup_list) == 0) { //데이터가 존재하지 않을 경우  ?>

						<tr>

							<td colspan="20" class="no_data">Data does not exist</td>

						</tr>

<? } ?>



				</tbody>

			</table>

			<div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_popup_list') ?></div>

			<div class="btn_area_right">

				<a href="/?c=admin&amp;m=adm_popup_write" title="등록"><img src="/images/admin/btn_regist_01.gif" alt="등록" /></a>

			</div>

		</div>

	</div>

</form>