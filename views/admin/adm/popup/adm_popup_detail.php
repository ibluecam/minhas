<?
//팝업 상세 조회
$rows = '';
if (count($popup_detail_list) > 0) {
	$rows = $popup_detail_list->row();
}
?>
<script type="text/javascript">
	//팝업 수정
	function popup_update(frm) {
		if(!validate(frm.elements['subject'], '팝업제목을 입력하세요.')) return false;
		if(!validate(frm.elements['win_width'], '팝업 가로 사이즈를 입력하세요.')) return false;
		if(!validate(frm.elements['win_height'], '팝업 세로 사이즈를 입력하세요.')) return false;
		if(!validate(frm.elements['scroll_yn'], '팝업 스크롤 여부를 선택하세요.')) return false;
		if(!validate(frm.elements['open_yn'], '팝업 게시 여부를 선택하세요.')) return false;
		if(!validate(frm.elements['close_type'], '팝업 종료 유형을 선택하세요.')) return false;

		if(xed.getWysiwygContent() == '' || xed.getWysiwygContent() == '<p>&nbsp;</p>') {
			alert("내용을 입력하세요.");
			return false;
		}

		return true;
	}

	//팝업 삭제
	function popup_delete(frm) {
		if(!confirm('삭제하시겠습니까?')) {
			return;
		}
		frm.action = '/?c=admin&m=adm_popup_delete';
		frm.submit();
	}
</script>
<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_popup_update" onsubmit="return popup_update(this);">
	<input type="hidden" name="idx" value="<?= $idx ?>" />
	<input type="hidden" name="cur_page" value="<?= $cur_page ?>" />
	<input type="hidden" name="sParam" value="<?= $sParam ?>" />
	<div class="contents_box_middle">
		<h1 class="title">팝업 관리</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 팝업을 관리 할 수 있습니다.</li>
			</ul>
		</div>
		<div class="table_write_box">
			<h1 class="table_title">팝업수정&삭제</h1>
			<table class="table_write" summary="팝업수정" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="18%" /><col width="32%" /><col width="18%" /><col width="32%" />
				<tr>
					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />팝업 제목</th>
					<td class="value_02" colspan="3">
						<input type="text" name="values[subject]" id="subject" value="<?= $rows->subject ?>" class="input_1" style="width:395px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="100" />
					</td>
				</tr>
				<tr>
					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />팝업 사이즈</th>
					<td class="value_02">
						- 가로 : <input type="text" name="values[win_width]" id="win_width" value="<?= $rows->win_width ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> px
						- 세로 : <input type="text" name="values[win_height]" id="win_height" value="<?= $rows->win_height ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> px
					</td>
					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />스크롤 여부</th>
					<td class="value_02">
						<input type="radio" name="values[scroll_yn]" id="scroll_yn" value="Y" class="radio" <?= func_decode($rows->scroll_yn, 'Y', 'checked', '') ?> /> 사용
						<input type="radio" name="values[scroll_yn]" id="scroll_yn" value="N" class="radio" <?= func_decode($rows->scroll_yn, 'N', 'checked', '') ?> /> 미사용
					</td>
				</tr>
				<tr>
					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />게시 여부</th>
					<td class="value_02">
						<input type="radio" name="values[open_yn]" id="open_yn" value="Y" class="radio" <?= func_decode($rows->open_yn, 'Y', 'checked', '') ?> /> 게시
						<input type="radio" name="values[open_yn]" id="open_yn" value="N" class="radio" <?= func_decode($rows->open_yn, 'N', 'checked', '') ?> /> 미게시
					</td>
					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />종료 유형</th>
					<td class="value_02">
						<select name="values[close_type]" id="close_type" class="input_2" style="width:150px;">
							<option value="">::선택하세요.::</option>
							<option value="1" <?= func_decode($rows->close_type, '1', 'selected', '') ?>>1일</option>
							<option value="3" <?= func_decode($rows->close_type, '3', 'selected', '') ?>>3일</option>
							<option value="5" <?= func_decode($rows->close_type, '5', 'selected', '') ?>>5일</option>
							<option value="7" <?= func_decode($rows->close_type, '7', 'selected', '') ?>>7일</option>
							<option value="0" <?= func_decode($rows->close_type, '0', 'selected', '') ?>>사용안함</option>
						</select>
					</td>
				</tr>
				<tr>
					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />내용</th>
					<td class="value_02" colspan="3" style="padding:10px;">
						<textarea name="values[contents]" id="contents" class="textarea"><?= $rows->contents ?></textarea>
						<script type="text/javascript">
							var xed;
							window.onload = function() {
								xed = new xq.Editor("contents");
								xed.isSingleFileUpload = true;
								xed.addPlugin('FileUpload');
								xed.setFileUploadTarget('helpers/iw_SWFUpload_single.php?mcd=popup', null);
								xed.setEditMode('wysiwyg');
								xed.setWidth("100%");
							}
						</script>

					</td>
				</tr>
			</table>
			<div class="btn_area_center">
				<input type="image" src="/images/admin/btn_modify.gif" title="수정" />
				<a href="#" onclick="popup_delete(document.adm_frm); return false;" title="삭제"><img src="/images/admin/btn_delete.gif" alt="삭제" /></a>
				<a href="/?c=admin&amp;m=adm_popup_list&amp;cur_page=<?= $cur_page ?><?= func_sPUrlEncode($sParam, "sch_word") ?>" title="목록"><img src="/images/admin/btn_list.gif" alt="목록" /></a>
			</div>
		</div>
	</div>
</form>