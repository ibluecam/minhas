<!--Rado start comment box-->
<link rel="stylesheet" href="/css/admin/styles.css?version=1.0.0">

<!--Rado end comment box-->

<div class="contents_box_middle">
<div class="wrapp_message_negotiat">
</div>
    <h1 class="title">Message Detail</h1>                                                                                     
<div class="gcm_msg">
</div>              
    <div class="tip_box">
        <p class="title"><img alt="tip" src="/images/admin/tip_img.gif"></p>
        <ul>
            <li> - You can negotiate with buyer</li>
        </ul>
    </div>
        <div class="table_list_box">
            <h1 class="table_title">Sender's Information</h1>
            <div class="search_box w_message_negotiate">
                <div class="wrapp_message_negotiat">
                    <?php if($cardetail_count > 0){ ?>
                        <?php foreach( $cardetail as $rows){ ?>
                            <img align="absmiddle" style="vertical-align:text-top;" alt="아이콘" src="/images/admin/icon_negotiate.png" border="0"> <?php echo $rows->member_first_name." ".$rows->member_last_name; ?><span class="title"> <?php echo $rows->member_no; ?> <img align="absmiddle" style="vertical-align:middle;" alt="아이콘" src="/images/admin/icon_point_down.png" border="0"></span> &nbsp;&nbsp;&nbsp;&nbsp; <img align="absmiddle" style="vertical-align:middle;" alt="아이콘" src="/images/admin/icon_message.png" border="0"> 
                        <?php } ?>
                        <a class="title icon_number"><?php echo $cardetail_count; ?></a>
                    <?php }else{ ?>
                    <?php foreach( $cardetail as $rows){ ?>
                        <img align='absmiddle' style='vertical-align:text-top;' alt='아이콘' src='/images/admin/icon_negotiate.png' border='0'> <?php echo $rows->member_first_name." ".$rows->member_last_name; ?> <span class='title'> <?php echo $rows->member_no; ?> <img align='absmiddle' style='vertical-align:middle;' alt='아이콘' src='/images/admin/icon_point_down.png' border='0'></span> &nbsp;&nbsp;&nbsp;&nbsp; <img align='absmiddle' style='vertical-align:middle;' alt='아이콘' src='/images/admin/blang_message.png' border='0'>
                    <?php } } ?>
                </div>

                <?php

                    foreach( $cardetail as $rows){

                        $car_chassis_no = "N/A";
                        if($rows->car_chassis_no != ""){
                          $car_chassis_no =  substr(strrev($rows->car_chassis_no) , 0 ,6);
                          $car_chassis_no = strrev($car_chassis_no);
                        }

                        $contact_person_name = "N/A";
                        if($rows->contact_person_name != ""){
                          $contact_person_name =  $rows->contact_person_name;
                        }

                        $member_first_name = "N/A";
                        if($rows->member_first_name != ""){
                          $member_first_name =  $rows->member_first_name;
                        }


        
                ?>                                 
                <div class="message_negotiat_sender_information">                    
                    <div class="message_negotiat_tradding">
                        <div class="message_negotiat_ssa_tradding"><?php echo $member_first_name; ?></div>
                        <div><!-- <img align="absmiddle" alt="아이콘" src="/images/admin/icon_flag.png" border="0"> -->
                            <?php 
                                if($rows->member_country == "jp"){
                                     echo "<img class='cc' src='/images/admin/icon_japan.png'>";
                                }elseif($rows->member_country == "kr"){ 
                                    echo "<img class='cc' src='/images/admin/icon_korea.png'>";
                                }elseif($rows->member_country == "kh"){ 
                                    echo "<img align='absmiddle' alt='아이콘' src='/images/admin/icon_flag.png' border='0'>";
                                }else{ $member_country = "N/A"; }
                               
                            ?>
                            <?php echo $rows->country_name; ?></div>
                        <div class="wrapp_message_negotiat_contact_person"><span class="message_negotiat_contact_persons contact_bold">Contact Person</span> : <?php echo $contact_person_name; ?></div>
                        <div class="wrapp_message_negotiat_contact_person email_float"><span class="message_negotiat_contact_person">Tel</span> : <?php echo $rows->phone_no2; ?></div>
                        <div class="wrapp_message_negotiat_contact_person email_floats">|</div>
                        <div class="wrapp_message_negotiat_contact_person email_floats"><span class="message_negotiat_contact_person">Email</span> : <?php echo $rows->email; ?></div>
                    </div>
                    <div style="width:100%">
                        <div class="message_negotiat_tradding_right">
                            <div class="message_negotiat_product_info">Product Info</div>
                            <div class="wrapp_message_negotiat_contact_person email_floatss">
                                <div class="message_negotiat_product_info_image"><img align="absmiddle" alt="아이콘" src="<?php echo $rows->secure_url; ?>" width="71" height="50" border="0"></div> 
                                <div class="message_negotiat_title">
                                    <div class="message_negotiat_title_name"><?php echo $rows->car_make." ".$rows->car_model; ?></div>
                                    <div class="clear"></div>
                                    <div class="wrapp_message_negotiat_contact_person email_float"><span class="message_negotiat_contact_person">ChassisNo</span> : <?php echo $car_chassis_no; ?></div>
                                    <div class="wrapp_message_negotiat_contact_person email_float_product">|</div>
                                    <div class="wrapp_message_negotiat_contact_person email_float_product"><span class="message_negotiat_contact_person">ID</span> : <?php echo $rows->car_stock_no; ?></div>
                                    <div class="wrapp_message_negotiat_contact_person email_float_product">|</div>
                                    <div class="wrapp_message_negotiat_contact_person email_float_product"><span class="message_negotiat_contact_person">Mileage</span> : <?php echo $rows->car_mileage; ?> km</div>
                                    <div class="wrapp_message_negotiat_contact_person email_float_product">|</div>
                                    <div class="wrapp_message_negotiat_contact_person email_float_product"><span class="message_negotiat_contact_person">Year</span> : <?php echo $rows->car_model_year; ?></div>
                                    <div class="message_negotiat_fob"><?php echo ($rows->car_fob_cost !=0 ? "US $".number_format($rows->car_fob_cost) : 'ASK'); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="table_list_box">
            <div class="search_box">
                <div class="wrapp_message_negotiat_proforma_invoice">
                    <div style="width:100%;"><img align="absmiddle" alt="아이콘" src="/images/admin/negotiate_proforma_invoice.png" border="0"></div>
                    <div style="float:left;padding: 7px 0;" class="hearder_buuton">
                        <button class="button orange disabled" id="btninvoice" type="button" onClick="location.href=''"><span></span> <b id="del">Proforma Invoice</b></button> 
                    </div>
                </div>
            </div>

            <div style="float:left; width:100%;margin-top: 20px;">
            <form id="addCommentForm" method="post" action="">
                <div class="wrapp_message_negotiat_user_admin">
                    <div class="message_negotiat_user_admin"><?php echo $member_id; ?> (me) :</div>
                    
                    <input type="hidden" value="<?php if(isset($_GET["car_idx"])){ echo $_GET["car_idx"]; } ?>" name="data[car_idx]">
                    <input type="hidden" value="<?php echo $member_no; ?>" name="data[sender_no]">

                    <input type="hidden" value="seller" class="get_note_role" id="get_note_role">
                    

                    <div class="message_negotiat_message"><textarea placeholder="Message..." class="textarea textarea_pading_top textarea_pading_left" name="data[message]" id="body"></textarea>
                    <div class="message_negotiat_message_send"><input style="width:100px;" type="submit" id="submit" value="Send" class="button blue btn_send_rec"></div>
                    </div>
                </div>
            </form>
            </div>

            <div id="get_message"></div>
            <div class="btn_area_center">
                <div class="btn_area_right"></div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="/js/admin/script.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    
     $( ".clickpagination" ).click(function() {
        var PageID = $(this).attr("id");
        var datastring = "page="+PageID;
        var urlstring = "/?c=admin&m=get_message";
        $.ajax({
            url: urlstring,
            type: "GET",
            data: datastring,
            success: function(data){
           
            }
        });
     });
  });
</script>