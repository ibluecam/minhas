<script type="text/javascript">

	//게시판 체크박스 전체 선택
	function checkbox_all_check(obj, target) {
		if ( typeof obj == 'undefined' || typeof target == 'undefined') {

		}
		else if ( typeof target.length == 'undefined' ) {
			target.checked = obj.checked;
		}
		else
		{
			for (i = 0 ; i < target.length ; i++) {
				target[i].checked = obj.checked;
			}
		}
	}
	

	function register_check(){
		var fm  = document.adm_frm;
		check_count = 0;
		var invoice_noval = '';
		if(fm.invoice_no != null) {
			
			for(i=0; i<fm.invoice_no.length; i++) {
		
				if(fm.invoice_no[i].checked) {
					
					invoice_noval = fm.invoice_no[i].value;
					check_count++;
				} 
			}
			if(check_count == 0) {
				alert('Please select invoice_no to register document.');
				return ;
			}else if(check_count >1){
				alert('Please select only one invoice_no to register.');
				return ;
				
			}else{

				window.open('/?c=admin&m=adm_documents_register&mcd=document&invoice_no='+invoice_noval,'Documents upload','width=400,height=150,top=100,left=100');

				return ;

			}
		}
	}


	function modify_check(){
		var fm  = document.adm_frm;
		
		var check_count = 0;
		
		var myc = fm.invoice_no.value;
		
		if(fm.invoice_no != null) {
			
			for(i=0; i< fm.invoice_no.length; i++) {
		
				if(fm.invoice_no[i].checked) {
					check_count++;
				} 
			}
			if(check_count == 0) {
				alert('Please select invoice_no to modify info.'+myc);
				return ;
			}else if(check_count >1){
				alert('Please select only one invoice_no to modify.');
				return ;
				
			}else{
				fm.action = '?c=admin&m=adm_documents_modify&mcd=<?= $mcd ?>&invoice_no='+ fm.invoice_no;
				fm.submit();	
			}
		}
		
		
	}
	$(document).ready(function(e){

		 $("#btnDelete").click(btnDelete_click);

         $('#invoice_no').click(function(event) {
		  if(this.checked) {
		      $(':checkbox').each(function() {
		          this.checked = true;
		          $('#btnDelete').removeClass('disabled');
		      });
		  }
		  else {
		    $(':checkbox').each(function() {
		          this.checked = false;
		          $('#btnDelete').addClass('disabled'); 
		      });
		  }
		});

        $('.invoice_no').click(function(event) {   
		    if(this.checked) {
		        this.checked = true;
		        $('#btnDelete').removeClass('disabled');   
		    }
		    else{
		    	 $('#btnDelete').addClass('disabled'); 
		    }
        });
        
        $("#btn_yes").click(function (){
            var abc ='';
            $(".invoice_no").each(function (){
                if ($(this).is(":checked") ===true)
                	

                    abc += $(this).attr("data-title") + ";";
            });
            var ajax = $.ajax({
                url: "/?c=admin&m=adm_delete_invoice_by_no",
                type: 'POST',
                data : {
                    
                    invoice_no : abc    
                }
            });
            ajax.done(function (msg){
                $("#myModal").modal("hide");
                window.location.href = "/?c=admin&m=adm_invoice_list";
            });
        });


          function btnDelete_click(){
            if($(this).hasClass("disabled") === false){
                if($("#del").html().indexOf("Delete") !== -1){
                    $("#msgBody").html("Are you sure want to delete the selected items?");
                    $("#myModalLabel").html("Confirmation");
                    $('#btnNO').html('NO');
                    $("#btn_yes").show();
                    $("#myModal").modal("show");
                }
              
                else{
                     window.location.href = "/?c=admin&m=adm_invoice_list";
                }
            }
        }
        

    
    // end Delete
		$("#dhl_popup_btn").click(function(event) {
			if($(".invoice_no:checked").length<=0){
				alert("Please select one or more Invoice(s) first!");
			}else{
				$( "a.fancybox" ).trigger( "click" );

			}
			
		});
		$("#save_button").click(function(event) {
			//$('#invoice_no_frm').submit();
			postDhlNo();
		});
		$("a.fancybox").fancybox({
			width: "30%",
			height: "100%"
		});
		$("#close_button").click(function(event) {
			closePopup();
		});
		// $("#delete_btn").click(function(event) {
		// 	if($(".invoice_no:checked").length<=0){
		// 		alert("Please select one or more invoice(s) to delete!");
		// 	}else{
		// 		if(confirm("Are you sure want to delete the selected items?")){
		// 			submitDeleteForm();
		// 		}
		// 	}
			
		// });
	});
	/* FUNCTION FOR INVOICE INPUT POPUP */
	function getAllInvoiceNo(){
		var invoice_nos = [];
		$(".invoice_no:checked").each(function(index, el) {
			var invoice_no = $(this).val();
			invoice_nos.push(invoice_no);
		});
		return invoice_nos;
	}
	function getDhlInput(){
		return $("#dhl_no").val();
	}
	function postDhlNo(){
		var invoice_nos = getAllInvoiceNo();
		var dhl_no = getDhlInput();
		if(dhl_no==undefined||dhl_no==''){
			alert("Please input the value in the textbox");
			return;
		}
		var data = {
			invoice_nos: invoice_nos,
			dhl_no: dhl_no
		};
		$.ajax({
			type: "POST",
			url: '/?c=admin&m=adm_save_dhl_no',
			data: data,
			success: function(data){
				alert(data);
				$.fancybox.close();
			},
			dataType: 'html'
		});
	}
	function closePopup(){
		$("#dhl_no").val('');
		$.fancybox.close();
	}
	// function submitDeleteForm(){
	// 	$('#adm_frm').attr('action', "/?c=admin&m=adm_delete_invoice_by_no").submit();
	// }
	/* END FUNCTION FOR INVOICE INPUT POPUP */
</script>

<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />


<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_invoice_list">

	<div class="contents_box_middle">

		<h1 class="title">motorbb Car Invoice</h1>

		  <div class="tip_box">

			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

			<ul>

				<li> - You can register/search/modify invoices.</li>

			</ul>

		</div>

		<div class="table_list_box" style="padding-top:10px;">

			<h1 class="table_title">Document</h1>

			<div class="search_box">

				<div class="search_result">

					<img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Total : <span class="title"><?= $total_rows ?></span> invoice, Pages : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>

				</div>

				<div class="search_condition">

					<?

					//카테고리 사용 여부
					if(isset($bbs_config_result)){

						if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {

						$category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));

						$category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));

					}


						?>

<? } ?>

					<select name="sch_condition" id="sch_condition" class="input_2" style="width:150px;" align="absmiddle">

						<option value="customer_name like">Customer</option>
						
						<option value="a.invoice_no like">invoice_no</option>
						
					</select>

					<input type="text" name="sch_word" id="sch_word" value="" class="input_1 text_admin" style="width:100px;" title="검색어" />

					<input type="image" src="/images/admin/btn_search_01.gif" class="search_admin" title="검색" align="absmiddle" />

				</div>

			</div>

			
            
            
            <div style="overflow:auto;">

			<table class="table_list" summary=" " cellspacing="0"  style="width:100%;">

				<caption class="hidden"></caption>

				<col width="3%" /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col />



				<thead>

					<tr>

						<th scope="col"><input type="checkbox" name="invoice_no" id="invoice_no" value="" onclick="checkbox_all_check(this, document.adm_frm.invoice_no);"/></th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">No.</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">invoice_no</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        
                        <th scope="col">Total Car</th>
                        
					</tr>

				</thead>

				<tbody>

					

					<?

					//var_dump(count($bbs_list)); exit();

					//일반글

					//print_r(count($doc_list));
					if (count($invoice_list) > 0) {
					
						$counter_id= 0;
						$no_coun =1;

						$bbs_link = ''; //링크


						foreach ($invoice_list as $rows) {
							//var_dump($rows); exit();
							
							//링크
							$bbs_link = "/?c=admin&m=adm_invoice_view&invoice_no=$rows->invoice_no&mcd=".$mcd."&cur_page=$cur_page" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);

							//Secret 아이콘
							$secret_icon = '';


							?>

							<tr>
                            
								<td><input type="checkbox" name="invoice_no[]" data-title="<?= $rows->invoice_no ?>" id="invoice_no" class="invoice_no" value="<?= $rows->invoice_no ?>" /> </td>

								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $no_coun++ ?></a></td>

								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $rows->invoice_no ?></a></td>

								<td class="line">&nbsp;</td>
								 
                                <td>
								<?= $rows->total_car ?>
                                 
                                 </td>

							</tr>

						<? } ?>

					<? } ?>

					<? //데이터가 존재하지 않을 경우

					if (count($invoice_list) == 0) {

						?>

						<tr>

							<td colspan="20" class="no_data">Data does not exist.</td>

						</tr>

					<? } ?>



				</tbody>

			</table>

		</div>
            
            
            
            

			<div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_bbs_list&mcd=' . $mcd, $mcd) ?></div>


			<? $query = $this->db->last_query(); ?>

			<div class="btn_area_center">
                <div class="btn_area_right">
                    <a href="/?c=admin&m=adm_invoice_register" title="register"><span class="button blue">Register</span></a>
                    <!-- <a href="#" onclick="register_check();" title="register"><span class="button blue">Register</span></a> -->
                    <a href="#" onclick="modify_check();" title="modify"><span class="button blue">Modify</span></a>
                   <!-- <input type="button" id="delete_btn" class="button red" value="Delete"/> -->
                    <button type="button" id="btnDelete" class="button red disabled"><span></span> <b id='del'>Delete</b></button>
				</div>
				<div class="btn_area_left">
                    <input type="button" id="dhl_popup_btn" class="button blue" value="DHL"/>
					<a href="#add_dhl_popup" class="fancybox" title="DHL NO"></a>
				</div>
			</div>
 			<div style="display:none;">
 				<div id="add_dhl_popup">
 					<div class="popup_title">
 						DHL No
 					</div>
 					<div class="popup_note">
 						Please input DHL No: 
 					</div>
 					<div class="invoice_input">
 						<input type="text" id="dhl_no"/>
 					</div>
 					<div class="bottom_button_container">
		                <input type="button" name="save" id="save_button" value="Save" class="button blue"/>
		                 <input type="button" name="close" id="close_button" value="Close" class="button blue"/>
		            </div>
 				</div>
 				
 			</div>
			<div class="search_box" style="padding:15px 10px;background-color:#ffffff">

				<!-- <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Selected cars

				<a href="#" onclick="delete_all();" title="delete"><img src="/images/admin/btn_delete_eng.gif" alt="delete" align="middle" /></a> -->
				<img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />You can register/search/modify BL documents
			</div>

		</div>

	</div>

</form>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
      </div>
      <div class="modal-body" id="msgBody">
         <!--  Message Here -->
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default" id="btn_yes">YES</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" id='btnNO'>NO</button>
      </div>
    </div>
  </div>
</div>
</div>
