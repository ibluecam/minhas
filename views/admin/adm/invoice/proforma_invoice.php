<div class="contents_box_middle">
    <h1 class="title">Proforma Invoice</h1>                                                  
	<div style="float:right;" class="excel_ship"></div>                                    
	<div class="gcm_msg"></div>
	<div class="tip_box">
	    <p class="title"><img alt="tip" src="/images/admin/tip_img.gif"></p>
	    <ul>
	        <li> - You can negotiate with buyer</li>
	    </ul>
	</div>

	<div class="table_list_box">

	    <h1 class="table_title">Sender's Information</h1>

	    <div class="search_box w_message_negotiate">

	        <div class="wrapp_message_negotiat">

                    <img align="absmiddle" style="vertical-align:text-top;" alt="아이콘" src="/images/admin/icon_negotiate.png" border="0"> User <span class="title">389 <img align="absmiddle" style="vertical-align:middle;" alt="아이콘" src="/images/admin/icon_point_down.png" border="0"></span> &nbsp;&nbsp;&nbsp;&nbsp; <img align="absmiddle" style="vertical-align:middle;" alt="아이콘" src="/images/admin/icon_message.png" border="0"> <a class="title icon_number">3</a>
                </div>
	                         
	        <div class="message_negotiat_sender_information">
	        	<div class="message_negotiat_tradding">
	        		<div class="message_negotiat_ssa_tradding">SSA Trading</div>
	        		<div><img align="absmiddle" alt="아이콘" src="/images/admin/icon_flag.png" border="0"> Cambodia</div>
	        		<div class="wrapp_message_negotiat_contact_person"><span class="message_negotiat_contact_person">Contact Person</span> : user name</div>
	        		<div class="wrapp_message_negotiat_contact_person email_float"><span class="message_negotiat_contact_person">Tel</span> : 0000000</div>
	        		<div class="wrapp_message_negotiat_contact_person email_floats">|</div>
	        		<div class="wrapp_message_negotiat_contact_person email_floats"><span class="message_negotiat_contact_person">Email</span> : example@gmail.com</div>
	        	</div>
	        	<div class="message_negotiat_tradding_right">
	        		<div class="message_negotiat_product_info">Product Info</div>
	        		<div class="wrapp_message_negotiat_contact_person email_floatss">
	        			<div class="message_negotiat_product_info_image"><img align="absmiddle" alt="아이콘" src="/images/admin/image71_50.png" width="71" height="50" border="0"></div> 
	        			<div class="message_negotiat_title">
	        				<div class="message_negotiat_title_name">HYUNDAI SONATA</div>
	        				<div class="clear"></div>
	        				<div class="wrapp_message_negotiat_contact_person email_float"><span class="message_negotiat_contact_person">ChassisNo</span> : 01338</div>
	            			<div class="wrapp_message_negotiat_contact_person email_float_product">|</div>
	            			<div class="wrapp_message_negotiat_contact_person email_float_product"><span class="message_negotiat_contact_person">ID</span> : 000001</div>
	            			<div class="wrapp_message_negotiat_contact_person email_float_product">|</div>
	            			<div class="wrapp_message_negotiat_contact_person email_float_product"><span class="message_negotiat_contact_person">Mileage</span> : 106,845 km</div>
	            			<div class="wrapp_message_negotiat_contact_person email_float_product">|</div>
	            			<div class="wrapp_message_negotiat_contact_person email_float_product"><span class="message_negotiat_contact_person">Year</span> : 2012</div>
	            			<div class="message_negotiat_fob">US $100,00</div>
	        			</div>
	        		</div>
	        	</div><div class="clear"></div>
	        </div><div class="clear"></div>
	    </div><div class="clear"></div>
	</div>

	<div class="motorbb_information">
		<h1>Motorbb's information</h1>
		<div class="list_view">
			<ul>
				<li><img alt="Icon List" src="/images/admin/icon_list_invoice_page.png" border="0"><a href="#">ChangeMotorbb's information</a></li>
			</ul>
		</div>	
	</div>

	<div class="customer_information">
		<h1>Customer's information</h1>
		<div class="list_view">
			<ul>
				<li><img alt="Icon List" src="/images/admin/icon_list_invoice_page.png" border="0"><a href="#">Change the buyer’s information</a></li>
			</ul>
		</div>	
	</div>

	<div class="product_information">
		<h1>Detail of Product</h1>
		
		<div class="list_view_product">
			<table class="table_view_product">
				<tr>
					<td class="title">Stock ID</td>
					<td class="value">00000</td>
				</tr>
				<tr>
					<td class="title">Mode</td>
					<td class="value">2008 TOYOTA Vitz</td>
				</tr>
				<tr>
					<td class="title">Car Size</td>
					<td class="value">
						<label class="label">Length <input type="text" name="lenght" class="field_text">cm</label>
						<label class="label">Width <input type="text" name="lenght" class="field_text">cm</label>
						<label class="label">Height <input type="text" name="lenght" class="field_text">cm</label>
						<label class="label">=</label>
						<label class="label"><input type="text" name="lenght" class="field_text"> CBM</label>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="customer_information">
		<div class="list_view">
			<ul>
				<li><img alt="Icon List" src="/images/admin/icon_list_invoice_page.png" border="0"><a href="#">Change the product's information</a></li>
			</ul>
		</div>	
	</div>
	<div class="product_information">
		<h1>Delivering Information</h1>
		
		<div class="list_view_product">
			<table class="table_view_product">
				<tr>
					<td class="title">Departure Port</td>
					<td class="value">
						<select class="country select_long" id="carcountry" name="searchcountry">
							<option value="" selected="">Country</option>
			                <option value="jp">Japan</option>
                            <option value="kh">Cambodia</option>
                            <option value="kr">South Korea</option>
            		    </select>
            		    <select class="country select_long" id="carcountry" name="searchcountry">
							<option value="" selected="">Port</option>
            		    </select>
					</td>
				</tr>
				<tr>
					<td class="title">Arrival Port</td>
					<td class="value">
						<select class="country select_long" id="carcountry" name="searchcountry">
							<option value="" selected="">Country</option>
			                <option value="jp">Japan</option>
                            <option value="kh">Cambodia</option>
                            <option value="kr">South Korea</option>
            		    </select>
            		    <select class="country select_long" id="carcountry" name="searchcountry">
							<option value="" selected="">Port</option>
            		    </select>
					</td>
				</tr>
				<tr>
					<td class="title">Arrival Country</td>
					<td class="value">
						<select class="country select_long" id="carcountry" name="searchcountry">
							<option value="" selected="">Country</option>
			                <option value="jp">Japan</option>
                            <option value="kh">Cambodia</option>
                            <option value="kr">South Korea</option>
            		    </select>
            		    <select class="country select_long" id="carcountry" name="searchcountry">
							<option value="" selected="">Port</option>
            		    </select>
					</td>
				</tr>
				<tr>
					<td class="title">Inspection</td>
					<td class="value">
						<select class="country select_longs" id="carcountry" name="searchcountry">
							<option value="" selected="">JEVIC</option>
            		    </select>
					</td>
				</tr>
				<tr>
					<td class="title">Total Price</td>
					<td class="value">
						<select class="country select_longs" id="carcountry" name="searchcountry">
							<option value="" selected="">US</option>
            		    </select>
            		    <input type="text" class="keyword text_box_long" placeholder="" id="keyword" name="" value="">
					</td>
				</tr>
				<tr>
					<td class="title">Term of Payment</td>
					<td class="value">
						<select class="country select_long" id="carcountry" name="searchcountry">
							<option value="" selected="">3days</option>
			                <option value="jp">7days</option>
                            <option value="kh">15days</option>
                            <option value="kr">30days</option>
            		    </select>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="but_confirm_cancel"><input type="button" id="btn_send_rec" class="button blue btn_confirms" value="Confirm"> <button type="button" id="btnDelete" class="button orange btn_confirm disabled"> <b id="del" class="btn_cancel">Cancel</b></button></div>
</div>