<script type="text/javascript">

	function checkbox_all_check(obj, target) {
		if ( typeof obj == 'undefined' || typeof target == 'undefined') {

		}
		else if ( typeof target.length == 'undefined' ) {
			target.checked = obj.checked;
		}
		else
		{
			for (i = 0 ; i < target.length ; i++) {
				target[i].checked = obj.checked;
			}
		}
	}

	function register_check(){
		var fm  = document.adm_frm;
		check_count = 0;
		var bl_noval = '';
		if(fm.bl_no != null) {
			
			for(i=0; i<fm.bl_no.length; i++) {
		
				if(fm.bl_no[i].checked) {
					
					bl_noval = fm.bl_no[i].value;
					check_count++;
				} 
			}
			if(check_count == 0) {
				alert('Please select Bl_no to register document.');
				return ;
			}else if(check_count >1){
				alert('Please select only one Bl_no to register.');
				return ;
				
			}else{

		       $.colorbox({
		            width:"540", 
		            height:"230", 
		            iframe:true, 
		            href:'/?c=admin&m=adm_documents_register&mcd=document&bl_no='+bl_noval,
		            onClosed:function(){ 
		                location.reload(true);
		            }
		        });

			}
		}
	}


	function modify_check(){
		var fm  = document.adm_frm;
		
		var check_count = 0;
		
		var myc = fm.bl_no.value;
		
		if(fm.bl_no != null) {
			
			for(i=0; i< fm.bl_no.length; i++) {
		
				if(fm.bl_no[i].checked) {
					check_count++;
				} 
			}
			if(check_count == 0) {
				alert('Please select Bl_no to modify info.'+myc);
				return ;
			}else if(check_count >1){
				alert('Please select only one Bl_no to modify.');
				return ;
				
			}else{
				fm.action = '?c=admin&m=adm_documents_modify&mcd=<?= $mcd ?>&bl_no='+ fm.bl_no;
				fm.submit();	
			}
		}
		
		
	}


	$(document).ready(function(e){

		 $("#btnDelete").click(btnDelete_click);

         $('#bl_no').click(function(event) {
		  if(this.checked) {
		      $(':checkbox').each(function() {
		          this.checked = true;
		          $('#btnDelete').removeClass('disabled');
		      });
		  }
		  else {
		    $(':checkbox').each(function() {
		          this.checked = false;
		          $('#btnDelete').addClass('disabled'); 
		      });
		  }
		});

        $('.bl_no').click(function(event) {   
		    if(this.checked) {
		        this.checked = true;
		        $('#btnDelete').removeClass('disabled');   
		    }
		    else{
		    	 $('#btnDelete').addClass('disabled'); 
		    }
        });
        
        $("#btn_yes").click(function (){
            var abc ='';
            $(".bl_no").each(function (){
                if ($(this).is(":checked") ===true)
                	

                    abc += $(this).attr("data-title") + ";";
            });
            var ajax = $.ajax({
                url: "/?c=admin&m=adm_delete_bl_by_no",
                type: 'POST',
                data : {
                    
                    bl_no : abc    
                }
            });
            ajax.done(function (msg){
                $("#myModal").modal("hide");
                window.location.href = "/?c=admin&m=adm_documents_list";
            });
        });


          function btnDelete_click(){
            if($(this).hasClass("disabled") === false){
                if($("#del").html().indexOf("Delete") !== -1){
                    $("#msgBody").html("Are you sure want to delete the selected items?");
                    $("#myModalLabel").html("Confirmation");
                    $('#btnNO').html('NO');
                    $("#btn_yes").show();
                    $("#myModal").modal("show");
                }
              
                else{
                     window.location.href = "/?c=admin&m=adm_documents_list";
                }
            }
        }
        
        // end delet
		$("#invoice_popup_btn").click(function(event) {
			if($(".bl_no:checked").length<=0){
				alert("Please select one or more BL(s) first!");
			}else{
				$( "a.fancybox" ).trigger( "click" );

			}
			
		});
		$("#save_button").click(function(event) {
			postInvoiceNo();
		});
		$("a.fancybox").colorbox({
			inline: true, 
			href: '#add_invoice_popup'
		});
		jQuery(document).on('fancybox-cleanup', function() {
		    location.reload();
		} );
		$("#close_button").click(function(event) {
			closePopup();
		});

	});
	/* FUNCTION FOR INVOICE INPUT POPUP */
	function getAllBlNo(){
		var bl_nos = [];
		$(".bl_no:checked").each(function(index, el) {
			var bl_no = $(this).val();
			bl_nos.push(bl_no);
		});
		return bl_nos;
	}
	function getInvoiceInput(){
		return $("#invoice_no").val();
	}

	function postInvoiceNo(){
		var bl_nos = getAllBlNo();
		var invoice_no = getInvoiceInput();
		if(invoice_no==undefined||invoice_no==''){
			alert("Please input the value in the textbox");
			return;
		}
		var data = {
			bl_nos: bl_nos,
			invoice_no: invoice_no
		};
		$.ajax({
			type: "POST",
			url: '/?c=admin&m=adm_save_invoice_no',
			data: data,
			success: function(data){
				alert(data);
				$.fancybox.close();
			},
			dataType: 'html'
		});
	}
	function closePopup(){
		$("#invoice_no").val('');
		$.colorbox.close();
	}
	
	/* END FUNCTION FOR INVOICE INPUT POPUP */
</script>

<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />


<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_documents_list&mcd=<?= $mcd ?>">

	<div class="contents_box_middle">

		<h1 class="title">motorbb Car Documents</h1>

		  <div class="tip_box">

			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

			<ul>

				<li> - You can register/search/modify BL documents.</li>

			</ul>

		</div>

		<div class="table_list_box" style="padding-top:10px;">

			<h1 class="table_title">Document</h1>

			<div class="search_box">

				<div class="search_result">

					<img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Total : <span class="title"><?= $total_rows ?></span> cars, Pages : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>

				</div>
                
                 <div class="btn_area_right">
                	<input type="button" id="invoice_popup_btn" class="button blue" value="Invoice"/>
					<a href="#add_invoice_popup" class="fancybox" title="Invoice No"></a>
                    <a href="#" onclick="register_check();" title="register"><span class="button blue">Register</span></a>
                    <a href="#" onclick="modify_check();" title="modify"><span class="button blue">Modify</span></a>
                   <button type="button" id="btnDelete" class="button red disabled"><span></span> <b id='del'>Delete</b></button>
				</div>

				<div class="search_condition">

					<?

					//카테고리 사용 여부
					if(isset($bbs_config_result)){

						if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {

						$category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));

						$category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));

					}


						?>

<? } ?>

					<select name="sch_condition" id="sch_condition" class="input_2" style="width:150px;" align="absmiddle">

						<option value="customer_name like">Customer</option>
						
						<option value="a.bl_no like">BL no</option>
						
					</select>

					<input type="text" name="sch_word" id="sch_word" value="" class="input_1 text_admin" style="width:100px;" title="검색어" />

					<input type="image" src="/images/admin/btn_search_01.gif" class="search_admin" title="검색" align="absmiddle" />

				</div>

			</div>

			
            
            
            <div style="overflow:auto; float:left;width:100%;">

			<table class="table_list" summary=" " cellspacing="0"  style="width:100%;">

				<caption class="hidden"></caption>

				<col width="3%" /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col />



				<thead>

					<tr>

						<th scope="col"><input type="checkbox" name="bl_no" id="bl_no" value="" onclick="checkbox_all_check(this, document.adm_frm.bl_no);"/></th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">No.</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">BL no</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">VesselName</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Discharge</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">PortLeavedDate</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        
                        <th scope="col">Customer</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        
                        <th scope="col">File</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        
                        <th scope="col">Total Car</th>
                        
					</tr>

				</thead>

				<tbody>

					

					<?

					//var_dump(count($bbs_list)); exit();

					//일반글

					//print_r(count($doc_list));
					if (count($doc_list) > 0) {
					
						$counter_id= 0;
						$no_coun =$total_rows;

						$bbs_link = ''; //링크


						foreach ($doc_list as $rows) {
							//var_dump($rows); exit();
							
							//링크
							$bbs_link = "/?c=admin&m=adm_documents_view&bl_no=$rows->bl_no&mcd=".$mcd."&cur_page=$cur_page" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);

							//Secret 아이콘
							$secret_icon = '';


							?>

							<tr>
                            	
								<td><input type="checkbox" name="bl_no[]" data-title="<?= $rows->bl_no ?>" id="bl_no" class="bl_no" value="<?= $rows->bl_no ?>" /> </td>

								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $no_coun-- ?></a></td>

								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $rows->bl_no ?></a></td>

								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $rows->vessel_name ?></a></td>

								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= $rows->discharge ?></a></td>

								<td class="line">&nbsp;</td>

								<td><a href="<?= $bbs_link ?>"><?= func_get_config_date('Y-m-d', $rows->port_leaved_date) ?></a></td>

								<td class="line">&nbsp;</td>
                                
                                <td><?= $rows->customer_name ?></td>

								<td class="line">&nbsp;</td>
                                
                                <td><?= $rows->total_file ?></td>

								<td class="line">&nbsp;</td>
								 
                                <td>
								<?= $rows->total_car ?>
                                 
                                 </td>

							</tr>

						<? } ?>

					<? } ?>

					<? //데이터가 존재하지 않을 경우

					if (count($doc_list) == 0) {

						?>

						<tr>

							<td colspan="20" class="no_data">Data does not exist.</td>

						</tr>

					<? } ?>



				</tbody>

			</table>

		</div>
            
            
            
            

			<div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_bbs_list&mcd=' . $mcd, $mcd) ?></div>


			<? $query = $this->db->last_query(); ?>

	
 			<div style="display:none;">
 				<div id="add_invoice_popup">
 					<div class="popup_title">
 						Invoice No
 					</div>
 					<div class="popup_note">
 						Please input Invoice No: 
 					</div>
 					<div class="invoice_input">
 						<input type="text" id="invoice_no"/>
 					</div>
 					<div class="bottom_button_container">
		                <input type="button" name="save" id="save_button" value="Save" class="button blue"/>
		                 <input type="button" name="close" id="close_button" value="Close" class="button blue"/>
		            </div>
 				</div>
 				
 			</div>
			<div class="search_box" style="padding:15px 10px;background-color:#ffffff; margin-top:30px;">

				<!-- <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Selected cars

				<a href="#" onclick="delete_all();" title="delete"><img src="/images/admin/btn_delete_eng.gif" alt="delete" align="middle" /></a> -->
				<img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />You can register/search/modify BL documents
			</div>

		</div>

	</div>

</form>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
      </div>
      <div class="modal-body" id="msgBody">
         <!--  Message Here -->
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default" id="btn_yes">YES</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" id='btnNO'>NO</button>
      </div>
    </div>
  </div>
</div>

<!--		<div class="panel">
            <a href="#car_status" id="status_pop">Doc Upload</a>
        </div>

        <!-- popup form #1
        <a href="#x" class="overlay" id="car_status"></a>
        <div class="popup">
            <h2>Car Status</h2>
            <p>Please select car status</p>
            <form action="accept-file.php" method="post" enctype="multipart/form-data">
				<input type="file" name="photo" size="25" />
				<input type="submit" name="submit" value="Submit" />
			</form>

            <a class="close" href="#close"></a>
        </div>-->
</div>
