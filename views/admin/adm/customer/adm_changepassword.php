<form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_changepassword_exec&amp;" onsubmit="return insert_adm_bbs(this);">
   <div class="contents_box_middle">
      <h1 class="title">Change Password</h1>
      <div class="tip_box">
         <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
         <ul>
            <li> - Change your password here.</li>
         </ul>
      </div>
      <div class="table_write_box">
         <h1 class="table_title">Change Password</h1>
         <table class="table_write" summary=" " cellspacing="0">
            <caption class="hidden"></caption>
            
            <input type="hidden" name="member_id"  value=" <?php
                  echo $seesion_memberid= $_SESSION['ADMIN']['member_no'];
                 ?>" />
            <tr>
                <th style="text-align: center">Old Password</th>
               <td class="value_02">
                   <input type="password" name="oldpassword" class="input_1"  value="" style="width:50%; float: left; height: 25px;" required=""/>
               </td>
            </tr>
            <tr>
               <th  style="text-align: center">New Password</th>
               <td class="value_02">
                   <input type="password" name="newpassword"  value="" class="input_1" style="width:50%; float: left; height: 25px;" required=""/>
               </td>
            </tr>
            <tr>
               <th  style="text-align: center">Confirm Password</th>
               <td class="value_02">
                   <input type="password" name="confirmpassword"  value="" class="input_1" style="width:50%; float: left; height: 25px;" required=""/>
               </td>
            </tr>
            
            
            
         </table>
         
         <div class="btn_change" style="float:right; margin-right:410px;">
               <input type="image" src="/images/admin/submit.gif" id="btn" title="Save"/>
         </div>
         
         
      </div>
   </div>
</form>