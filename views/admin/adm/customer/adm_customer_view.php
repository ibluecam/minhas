
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<?

//회원 상세 조회
 function updateCurrentGetUrl($p=array(), $unset_p=array()){
        $get = $_GET;
        if(count($unset_p)>0){
            foreach($unset_p as $value){
                if(isset($get[$value])) unset($get[$value]);
            }
        }
        if(count($p)>0){
            foreach($p as $key=>$value){
                $get[$key]=$value;
            }
        }
        $updatedUrl_arr = array();
        foreach($get as $key=>$value){
            $updatedUrl_arr[]=$key."=".$value;
        }
        $updatedUrl = implode("&", $updatedUrl_arr);
        return $updatedUrl;
    }
$rows = '';

if(count($member_detail_list) > 0)

{

$rows = $member_detail_list->row();

}

?>

<script type="text/javascript">



    //회원 수정

    function update_adm_member(frm)

    {

        /*if(!validate(frm.elements['values[zipcode1]'], '우편번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[zipcode2]'], '우편번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[address]'], '주소를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[address_de]'], '상세주소를 입력하세요.')) return false;
         
         
         
         if(!validate(frm.elements['values[phone_no1]'], '전화번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[phone_no2]'], '전화번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[phone_no3]'], '전화번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[mobile_no1]'], '휴대폰번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[mobile_no2]'], '휴대폰번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[mobile_no3]'], '휴대폰번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[email]'], '이메일을 입력하세요.')) return false;
         
         if(!emailCheck(frm.elements['values[email]'])) return false;
         
         */

        return true;

    }



    //회원 삭제
$(document).ready(function() {

    $("#btn_yes_one").click(function (){
               
                var ajax = $.ajax({
                    url: "/?c=admin&m=adm_customer_all_delete&member_no=<?= $member_no ?>",
                    type: 'POST',
                    data : {
                        
                        idx :"<?= $member_no ?>" 
                    }
                });
                ajax.done(function (msg){
                    window.location.href = "/?c=admin&m=adm_customer_list";
                });
            });

  });

    // function delete_adm_member()

    // {

    //     if (!confirm('Do you want to delete this customer?'))

    //     {

    //         return;

    //     }

    //     else

    //     {
    //         var f  = document.adm_frm;
    //         //location.href = "/?c=admin&m=adm_customer_all_delete&member_no=<?= $member_no ?>&cur_page=<?= $cur_page ?>&sParam=<?= urlencode($sParam) ?>";
    //         f.action = "/?c=admin&m=adm_customer_delete";
    //         f.submit();
    //     }

    // }



</script>

<form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="" onsubmit="">
    <input type="hidden" name="customer_no" id="customer_no" value="<?=$rows->member_no ?>" />
    <input type="hidden" name="cur_page" value="<?= $cur_page ?>" />

    <input type="hidden" name="sParam" value="<?= $sParam ?>" />

    <div class="contents_box_middle">

        <h1 class="title">Customer No. <?=$rows->member_no ?> Info</h1>

        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You can see all info of member here.</li>

            </ul>

        </div>


        <div class="table_write_box">

            <h1 class="table_title">Customer Info</h1>

            <table class="table_write" summary="회원정보" cellspacing="0">

                <caption class="hidden"></caption>

                <col width="16%" /><col width="34%" /><col width="16%" /><col width="34%" />
                
                
                  <tr>
                    
                    <th class="key_02">ID</th>
                  <td class="value_02">
                      <label><?php echo $rows->member_id; ?></label>
                  </td>
                    
                  <th class="key_02">Commission</th>
                  <td class="value_02">
                     <?=($rows->commission==1 ? 'Yes' : 'No') ?>
                     <div id="error_member_id" class="error_under error_member_id">
                     </div>
                  </td>
                
               </tr>
               
                
                 <tr>
                    
                    <th class="key_02">Business Type</th>
                  <td class="value_02">
                      <label>
                          <?php 
                              echo $rows->business_type;
                          ?>
                      </label>
                  </td>
                    
                  <th class="key_02"></th>
                  <td class="value_02">
                     
                     <div id="error_member_id" class="error_under error_member_id">
                     </div>
                  </td>
                
               </tr>
                <tr>

                    <th class="key_02">Country</th>

                    <td class="value_02"><strong>&nbsp;<?= $rows->country_name ?></strong></td>

                    <th class="key_02">Company Name</th>

                    <td class="value_02"><strong>&nbsp;<?= $rows->company_name ?></strong></td>

                </tr>

                <tr>

                    <th class="key_02">Name</th>

                    <td class="value_02">
                      <?php if($rows->business_type=='seller'||$rows->business_type=='both'){ ?>
                          <strong>&nbsp;<?= $rows->contact_person_name ?> </strong>
                      <?php }else{ ?>
                        <strong>&nbsp;<?= $rows->member_first_name ?> 
                        <?= $rows->member_last_name ?></strong>
                          
                      <?php } ?>
                    </td>

                    <th class="key_02">Email</th>

                    <td class="value_02"><strong>&nbsp;<?= $rows->email ?></strong></td>

                </tr>

                <tr>

                    <th class="key_02">Phone</th>

                    <td class="value_02">&nbsp;
                    <?php if(!empty($rows->phone_no1)){ ?>
                    	 <?= "+".$rows->phone_no1?> - <?= $rows->phone_no2 ?>
                    <?php } ?>
                    </td>

                    <th class="key_02">Mobile</th>

                    <td class="value_02">&nbsp;
                    <?php if(!empty($rows->mobile_no1)){ ?>
                       <?= "+".$rows->mobile_no1 ?> - <?= $rows->mobile_no2 ?>
                    <?php } ?>
                    </td>

                </tr>


                <tr>

                    <th class="key_02">Address</th>

                    <td class="value_02" colspan="3">

                        <?= $rows->address ?>

                    </td>

                </tr>


                <tr id="show_hihe">

                    <th class="key_02">Consignee</th>

                    <td class="value_02" colspan="3" style="padding:10px;">

                        <?
						 	$string1 = $rows->consignee_name;
						 	$string2 = $rows->consignee_address;
						 	$string3 = $rows->consignee_phone;

						 	$consignee_name = explode("|",$string1);
						 	$consignee_address = explode("|",$string2);
						 	$consignee_phone = explode("|",$string3);

						 	$num =  count($consignee_name);
						 ?>



						 <table  id="tbl1" border="1">
                                                      <?php
                                                    if($rows->consignee_name !='' || $rows->consignee_address !='' || $rows->consignee_phone !='' ){
                                                       
                                                        
                                                    ?>
						 <? for($i=0; $i<$num; $i++) { ?>	
						 <tr>
<!--						 	<td>Consignee [<?=$i+1?>] </td>
						 	<td><?=$consignee_name[$i] ?></td>
						 	<td><?=$consignee_address[$i] ?></td>
						 	<td><?=$consignee_phone[$i] ?></td>-->
                                                        <td>Consignee [<?=$i+1?>] :  <label  style="width:100%;"><?=$consignee_name[$i] ?>, <?=$consignee_address[$i] ?>, <?=$consignee_phone[$i] ?></label> </td>
                                                        <td></td>
						 </tr>
						 <?php  } ?>
                                                 
                                                  <?php
                                                      } 
                                                 else{
                                                     echo ' <script>
                                                         $("#show_hihe").css("display","none");
                                                           </script>
                                                          
                                                           ';
                                                 }
                                                 
                                                   
                                                 ?>

						</table>

                    </td>

                </tr>

                <tr>

                    <th class="key_02">Sales (Person In Charge)</th>
                     <? 
                        $slq="SELECT member_no,member_id FROM iw_member WHERE member_no='{$rows->sales_no}'";
                        $sql_query = mysql_query($slq);
                        $row=mysql_fetch_array($sql_query);
                    ?>
                    <td colspan="3" class="value_02"><strong>&nbsp;<?php echo $row['member_id']; ?></strong></td>

                </tr>

                <tr>
                  
                  <th class="key_02">Description</th>

                  <td colspan="3" class="value_o2"><?= $rows->user_memo ?></td>

                </tr>

                <tr>
                  <th class="key_02">Photos</th>
                  <td colspan="3" class="value_02">
                    <?php 
                      if (count($select_cphoto_list) > 0) {
                        foreach ($select_cphoto_list as $att_rows) {
                         echo "<img src='" . $att_rows->base_url . "t_item_thumb/".$att_rows->public_id."' width='150' >";
                        }
                      }
                    ?>
                  </td>
                </tr>
            </table>

            <div class="btn_area_center">

                <div class="btn_area_right">
                    <a href="?c=admin&m=adm_customer_modify&member_no=<?=$_GET['member_no']?>" title="edit"><span class="button blue">Edit</span></a>
                    <?php if($session_grade_no =='1') { ?>
                    <!-- <a href="#" onclick="delete_adm_member();return false;" title="delete"><span class="button red">Delete</span></a> -->

                    <a href="#myModal2" id="delete_one" data-toggle="modal" title="delete"><span class="button red">Delete</span></a>
                    <?php } ?>

                    <a href="/?c=admin&m=adm_customer_list" title="back"><span class="button blue">Back</span></a>
                </div>

            </div>


        </div>

    </div>

</form>

  <div class="modal" id="myModal2" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel_cus">Confirmation</h4>
              </div><div class="container"></div>
              <div class="modal-body" id="msgBody_cus">
                  Do you want to delete this customer?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" id="btn_yes_one">YES</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id='btnNO_one'>NO</button>
              </div>
            </div>
          </div>
    </div>