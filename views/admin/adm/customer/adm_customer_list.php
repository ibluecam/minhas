<?php 
function updateCurrentGetUrl($p=array(), $unset_p=array()){
    $get = $_GET;
    if(count($unset_p)>0){
        foreach($unset_p as $value){
            if(isset($get[$value])) unset($get[$value]);
        }
    }
    if(count($p)>0){
        foreach($p as $key=>$value){
            $get[$key]=$value;
        }
    }
    $updatedUrl_arr = array();
    foreach($get as $key=>$value){
        $updatedUrl_arr[]=$key."=".$value;
    }
    $updatedUrl = implode("&", $updatedUrl_arr);
    return $updatedUrl;
}


?>

<script type="text/javascript">

	//게시판 체크박스 전체 선택
	function checkbox_all_check(obj, target) {
		if ( typeof obj == 'undefined' || typeof target == 'undefined') {

		}
		else if ( typeof target.length == 'undefined' ) {
			target.checked = obj.checked;
		}
		else
		{
			for (i = 0 ; i < target.length ; i++) {
				target[i].checked = obj.checked;
			}
		}
	}

	//deposit input
	function deposit_select() {
		var f  = document.adm_frm;
		var fg = false;

		check_count = 0;
		//var idxs = new Array();
		var customer_num = '';
		var currency_type = '';

		if(f.idx != null) {
			
			for(i=0; i<f.idx.length; i++) {

				if(f.idx[i].checked) {
					fg = true;

					//idxs[i] = " \' " + f.idx[i].value+ " \' ";

					currency_type = f.currency_type[i-1].value;
					customer_num = f.idx[i].value;

					check_count++;
				} /*else { idxs[i] = " \' \' "; }*/
			}

			//alert(customer_num); return;

			if(check_count == 1) {
				window.open('/?c=admin&m=adm_customer_deposit_popup_update&mcd=<?=$mcd?>&idx='+customer_num+'&currency='+currency_type,'deposit','width=600,height=550,top=100,left=100'); 
				
				return;
			}

			if(check_count != 1) {
				alert('Please select one customer to deposit.');
				return;
			}

/*			alert(idxs[0]);
			alert(idxs);
			alert(f.idx);
				return;*/


			f.action = '/?c=admin&m=adm_customer_deposit_update&mcd=<?= $mcd ?>&idx='+f.idx;
			f.submit();
		}
	}


	//전체 삭제
	function delete_all() {
		var f  = document.adm_frm;
		var fg = false;

		if(f.idx != null) {
			for(i=0; i<f.idx.length; i++) {
				if(f.idx[i].checked) {
					fg = true;
				}
			}

			if(!fg) {
				alert('Please select customers that you want to delete.');
				return;
			}

			if (!confirm('Do you want to delete this customer?'))

	        {
	            return;
	            
	        }else{

	        	f.action = '/?c=admin&m=adm_customer_all_delete';
				f.submit();
	        }
			
		}
	}

			//function modeify
	
	function modify_check(){
		var f  = document.adm_frm;
		check_count = 0;

		var customer_num = '';

		if(f.idx != null) {
			
			for(i=0; i<f.idx.length; i++) {
		
				if(f.idx[i].checked) {
					customer_num = f.idx[i].value;
					check_count++;
				} 
			}
			if(check_count == 0) {
				alert('Please select a customer to modify info.');
				return ;
			}else if(check_count >1){
				alert('Please select only one customer to modify info.');
				return ;
				
			}else{
				f.action = '?<?= updateCurrentGetUrl(array("m"=>"adm_customer_modify")) ?>&member_no='+ customer_num;
				f.submit();	
			}
		}
	}

	function DoNav(url)
	{
	   document.location.href = url;
	}


</script>

<?php 
	if(isset($_GET['sch_create_dt_s'])) $sch_create_dt_s=htmlspecialchars($_GET['sch_create_dt_s']);else $sch_create_dt_s="";
   if(isset($_GET['sch_create_dt_e'])) $sch_create_dt_e=  htmlspecialchars($_GET['sch_create_dt_e']);else $sch_create_dt_e="";
    if(isset($_GET['sch_condition_grade'])) $bussines_type=htmlspecialchars($_GET['sch_condition_grade']);else $bussines_type="";
    if(isset($_GET['sch_condition_customer_name'])) $search_name=htmlspecialchars($_GET['sch_condition_customer_name']);else $search_name="";
    if(isset($_GET['sch_condition_country'])) $search_con=  htmlspecialchars($_GET['sch_condition_country']);else $search_con="";
?>


	<div class="contents_box_middle">

		<h1 class="title">Customer Mangement</h1>

		<div class="tip_box">

			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

			<ul>

				<li> - You can search and modify customer Info.</li>

			</ul>

		</div>

		<!--<div style="padding-top:5px;text-align:right;"><img src="/images/admin/btn_reset.gif" alt="초기화" /></div>-->

		<div class="table_list_box" style="padding-top:10px;">

			<h1 class="table_title">Customer</h1>

			<div class="search_box">

				<div class="search_result">

					<img src="/images/admin/search_icon.gif" alt="Icon" align="absmiddle" />Total : <span class="title"><?= $total_rows ?></span> customers, Pages : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>

				</div>

				<div class="search_condition">
                
                 <div class="btn_area_right" style="text-align:left">
                 	
                    <a href="/?<?= updateCurrentGetUrl(array("m"=>"adm_customer_write")) ?>" title="등록"><span class="button blue">Register</span></a>
                    <a href="#" onclick="modify_check();" title="modify"><span class="button blue">Modify</span></a>
                    <a href="#" onclick="delete_all();" title="delete"><span class="button red">Delete</span></a>
                    <a style="font-size:14px;" href="#insert_to_list_popup" id="btn_popup_insert_to_list" class="button white">Add to Recipient</a>
				</div>
                
                
					<form name="adm_frm_search" id="adm_frm_search" method="get" action="/?c=admin&amp;m=adm_customer_list">
						<input type="hidden" name="c" value="<?= $c ?>" />
						<input type="hidden" name="m" value="<?= $m ?>" />
					<!-- <input type="text" name="sch_create_dt_s" id="sch_create_dt_s" value="" class="input_1" style="width:65px;" readonly />

					<a href="#" onclick="Calendar(document.adm_frm.elements['sch_create_dt_s']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>&nbsp;~

					<input type="text" name="sch_create_dt_e" id="sch_create_dt_e" value="" class="input_1" style="width:65px;" readonly />

					<a href="#" onclick="Calendar(document.adm_frm.elements['sch_create_dt_e']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a> -->
					
					<?

					//카테고리 사용 여부
					if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {

						$category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));

						$category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));

						?>

<!-- 
						<select name="category" id="category" class="input_2" align="absmiddle">

							<option value="">::선택하세요::</option>

							<? if (count($category_code) > 0) {

								for ($i = 0; $i < count($category_code); $i++) {

									?>

									<option value="<?= $category_code[$i] ?>"><?= $category_name[$i] ?></option>

							<? } ?>

						<? } ?>

						</select> -->


<? } ?>
					- RegDate
					<input type="text" name="sch_create_dt_s" data-date-format="yyyy-mm-dd"  value="<?php echo $sch_create_dt_s; ?>" class="input_1 datepicker-add-options text_admin" data-auto-close="true" style="width:65px;" />

                    &nbsp;~

                    <input type="text" data-date-format="yyyy-mm-dd" name="sch_create_dt_e"  value="<?php echo $sch_create_dt_e; ?>" class="input_1 datepicker-add-options text_admin" data-auto-close="true" style="width:65px;" />

					<select name="sch_condition_country" id="sch_condition_country" class="input_2" style="width:150px;" align="absmiddle">
						<option value="">Country</option>
						<?php 
                            foreach($country_list as $country){
                            ?>
                            <option
                                 <?php 
                           echo $search_con=(isset($_GET['sch_condition_country']) && $_GET['sch_condition_country']=="$country->cc"? 'selected="selected"':''  );
                                 ?>
                                value="<?php echo $country->cc; ?>"><?php echo $country->country_name; ?></option>
                            <?php
                            } 
                        ?>
						
					</select>

					<select name="sch_condition_grade" id="sch_condition_grade" class="input_2" style="width:150px;" align="absmiddle">
						<option value="">Business Type</option>
                        <?php foreach (Array('seller', 'buyer') as $Val) { ?>
                                        <option <?php echo $bussines_type=(isset($_GET['sch_condition_grade']) && $_GET['sch_condition_grade']=="$Val"? 'selected="selected"':''); ?> 
                                                    value="<?php echo $Val; ?>"><?php echo $Val; ?></option>
                        <?php } ?>
					</select>
<input name="sch_condition_customer_name" class="input_1 text_admin" value="<?php echo $search_name; ?>" type="text" placeholder="Member Name"/>
					<!-- <select name="sch_condition" id="sch_condition" class="input_2" style="width:150px;" align="absmiddle">

						<option value="concat(ifnull(a.member_first_name,''),' ',ifnull(a.member_last_name,'')) like">Name</option>

						<option value="country_name like">Country</option>
						<option value="car_stock_no like|car_chassis_no like">ChassisNO+RefNo</option>

					</select>

					<input type="text" name="sch_word" id="sch_word" value="" class="input_1" style="width:100px;" title="Searches" />

					<br/> -->

					<input type="image" src="/images/admin/btn_search_01.gif" class="search_admin " title="Search" align="absmiddle" />
					</form>
				</div>

			</div>
			<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_customer_list&amp;mcd=product">
			<div style="overflow:auto; float:left; width:100%;">

			<table class="table_list" summary="<?= func_get_config($bbs_config_result, 'bbs_code_name') ?>" cellspacing="0">

				<caption class="hidden"></caption>

				<col width="3%" /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col /><col width="2px" />



				<thead>

					<tr>

						<th scope="col"><input type="checkbox" name="idx" id="idx" value="" onclick="checkbox_all_check(this, document.adm_frm.idx);"/></th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Customer No.</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Grade</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Country</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Name</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>


						<th scope="col">Phone Number</th>
						
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Email</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Person In Charge</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">Reg. Date</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<!-- <th scope="col">Gender</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th> -->


					</tr>

				</thead>

				<tbody>


					<?

					//var_dump($query); exit();

					//일반글

					if (count($customer_list) > 0) {

						//var_dump($bbs_list[2]->car_stock_no); exit();

						$bbs_link = ''; //링크

						$subject = ''; //제목

						$level_str = ''; //자식글

						foreach ($customer_list as $rows) {
							//var_dump($rows); exit();
							
							//링크
							
							
							$bbs_link = "/?".updateCurrentGetUrl(array("m"=>"adm_customer_view", "member_no"=>$rows->member_no));

							//Secret 아이콘
							$secret_icon = '';

							if (func_get_config($bbs_config_result, 'secret_yn') == 'Y' && $rows->secret_yn == 'Y')

								$secret_icon = func_secret_icon($mcd);

							//New, Hot 아이콘
							//$icon = '&nbsp;&nbsp;' . func_new_icon(func_get_config($bbs_config_result, 'icon_new'), $rows->created_dt, $mcd) . '&nbsp;&nbsp;' . func_hot_icon(func_get_config($bbs_config_result, 'icon_hot'), $rows->visit_cnt, $mcd);

							//Totalsellprice
							//$total_sell_price = $rows->car_fob_cost + $rows->car_freight_fee;

							?>

							<tr>

								<td><input type="checkbox" name="idx[]" class="check_member_no" id="idx" value="<?= $rows->member_no ?>" /></td>

								<td class="line">&nbsp;</td>

								<td onClick="DoNav('<?= $bbs_link ?>')" style="cursor:pointer"><?= $rows->member_no ?></td>

								<td class="line">&nbsp;</td>

								<td onClick="DoNav('<?= $bbs_link ?>')" style="cursor:pointer">
								<?php
									if($rows->grade_no==11){
										echo '<a class="grage_no potential_customer"></a>';
									}
									else if ($rows->grade_no==10) {
										echo '<a class="grage_no just_registered"></a>';
									}
									
								 ?>
								<?= $rows->grade_name ?></td>


								<td class="line">&nbsp;</td>

								<td onClick="DoNav('<?= $bbs_link ?>')" style="cursor:pointer">
									<?=
										 $rows->country_name
			                        ?>
								</td>

								<td class="line">&nbsp;</td>

								<td onClick="DoNav('<?= $bbs_link ?>')" style="cursor:pointer">
									<?php 
										if($rows->business_type=='seller') 
											echo $rows->contact_person;
										else
											echo $rows->customer_name;
									?>
								</td>

								<td class="line">&nbsp;</td>

								<td onClick="DoNav('<?= $bbs_link ?>')" style="cursor:pointer"><?= $rows->customer_phone ?></td>

								<td class="line">&nbsp;</td>

								<td onClick="DoNav('<?= $bbs_link ?>')" style="cursor:pointer"><?= $rows->customer_email ?></td>

								<td class="line">&nbsp;</td>

								<td onClick="DoNav('<?= $bbs_link ?>')" style="cursor:pointer"><?=$rows->sales_name ?></td>
								
								<td class="line">&nbsp;</td>
								<td onClick="DoNav('<?= $bbs_link ?>')" style="cursor:pointer"><?=$rows->reg_date ?></td>
								
								<td class="line">&nbsp;</td>
							</tr>

						<? } ?>

					<? } ?>

					<? //데이터가 존재하지 않을 경우

					if (count($customer_list) == 0) {

					?>

						<tr>

							<td colspan="20" class="no_data">Data does not exist.</td>

						</tr>

					<? } ?>



				</tbody>

			</table>

		</div>

			<div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?'.updateCurrentGetUrl(array(), array('cur_page')), $mcd) ?></div>

<!-- 			<? var_dump($_POST); ?>
			<? var_dump($_GET); ?>
			<? var_dump($total_rows); ?><br/>
			<? var_dump($sParam); ?>


			<? foreach ($bbs_list as $rows) { ?>
				<?= $rows->idx ?> 

			<? } ?>

			<? var_dump($bbs_list[0]->idx); ?> -->
			<? $query = $this->db->last_query(); ?>

			<div class="btn_area_center">
               
			</div>
 </form>
			<div class="search_box" style="padding:15px 10px;background-color:#ffffff">

				<!-- <img src="/images/admin/search_icon.gif" alt="Icon" align="absmiddle" />Selected customers

				<a href="#" onclick="delete_all();" title="delete"><img src="/images/admin/btn_delete_eng.gif" alt="delete" align="middle" /></a> -->
				<img src="/images/admin/search_icon.gif" alt="Icon" align="absmiddle" />You can search/register/modify customer Info
			</div>

		</div>

	</div>
	<div style="display:none">
		<div id="insert_to_list_popup" style="padding:20px; min-height: 250px;">
			<div style="color:#ddddff; background: #eeeeff none repeat scroll 0 0; border-bottom: 1px solid #ddddff; border-top: 1px solid #ddddff; padding: 5px;">
				<a href="https://sendgrid.com/newsletter/lists" target="_blank">Manage List</a> | <a target="_blank" href="https://sendgrid.com/newsletter/wizardlists/page/1">Add New</a>
			</div>
			<div style="background: #eee none repeat scroll 0 0; padding: 20px; min-height:170px;">
				<div style="font-size:16px; font-weight:bold; margin-bottom: 10px;">Insert Customers' Email to Recipient List</div>
				<div id="success_msg" style="display:none; background: #c9ffbc none repeat scroll 0 0; border-radius: 5px; color: green; margin-bottom: 10px; padding: 5px;"></div>
				Select Recipient: 
				<select id="select_rec" style="padding:5px">
					<option value="">- Recipient List -</option>
					<?php 

					foreach($rec_list as $rec){
						echo '<option value="'.$rec->list.'">'.$rec->list.'</option>';
					} ?>
						
				</select>
				<input type="button" class="button orange" id="btn_insert_customer_to_list" value="Insert" />
				<img id="insert_loading" style="display:none" src="images/loading_3.gif"/>
			</div>
		</div> 
	</div>

<script>

	$(document).ready(function(e){
		$("#btn_popup_insert_to_list").colorbox({inline:true, width:"600"});
		$("#btn_popup_insert_to_list").click(function(e){
			var count_c = count_check();
			if(count_c>0){

				$("#success_msg").css("display", "none");
			}else{
				alert("Please select some customers first.");
				return false;
			}
			
		});
		$("a.fancybox").fancybox({
			height: "100%"
		});
		$(".datepicker-add-options").datepicker({
        
    	});
		$("#btn_insert_customer_to_list").click(function(e){
			var select_rec = $("#select_rec").val();
			var data = $("#adm_frm").serializeArray();

			if(select_rec!=''){
				$("#insert_loading").css("display", "inline");
				$("#success_msg").css("display", "none");

				$.ajax({
				  	url: '/?c=admin_json&m=insert_email_to_list&list='+select_rec,
				  	method: "POST",
				  	data: data,
				  	dataType: "json"
				}).done(function( json ) {
					$("#insert_loading").css("display", "none");
					$("#select_rec").val('');
					$("#success_msg").css("display", "block");
				  	$("#success_msg").html("Successful! Emails have been inserted to the list: "+select_rec+".");
				});
			}else{
				alert("Please select recipient to insert first.");
			}
			//	f.submit();
		});
	});
function count_check(){
	var count_check = $(".check_member_no:checked").length;
	return count_check;
}
</script>

<!-- ? print_r($this->db->last_query()); ? -->