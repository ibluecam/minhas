<?php 

 function updateCurrentGetUrl($p=array(), $unset_p=array()){
        $get = $_GET;
        if(count($unset_p)>0){
            foreach($unset_p as $value){
                if(isset($get[$value])) unset($get[$value]);
            }
        }
        if(count($p)>0){
            foreach($p as $key=>$value){
                $get[$key]=$value;
            }
        }
        $updatedUrl_arr = array();
        foreach($get as $key=>$value){
            $updatedUrl_arr[]=$key."=".$value;
        }
        $updatedUrl = implode("&", $updatedUrl_arr);
        return $updatedUrl;
    }

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script type="text/javascript">

	//게시판 등록

	function insert_adm_bbs(frm) {
                 
		if(!validate(frm.elements['values[member_country]'], 'Input Country.')) return false;
		
		if(!validate(frm.elements['values[member_first_name]'], 'Input Customer Name.')) return false;
               
               
               
		//if(!validate(frm.elements['values[email]'], 'Input Email.')) return false;

		//if(!emailCheck(frm.elements['values[email]'])) return false; //이메일 체크


	}

	$(document).ready(function(){
		 var cnt = 2;
		 $("#anc_add").click(function(){
			 $('#tbl1 tr').last().after('<tr><td>Consignee ['+cnt+']</td><td><input type="text" name="consignee_name[]" value="" class="input_1" style="width:100%;" maxlength="150" placeholder="Consignee Name"></td><td><input type="text" name="consignee_address[]" value="" class="input_1" style="width:100%;" maxlength="150" placeholder="Consignee Address"></td><td><input type="text" name="consignee_phone[]" value="" class="input_1" style="width:100%;" maxlength="150" placeholder="Consignee Phone"></td></tr>');
			 
			 cnt++;
			 
			 });
			 
			$("#anc_rem").click(function(){
				if($('#tbl1 tr').size()>1){
				 $('#tbl1 tr:last-child').remove();
				 
				 cnt--;
				 
				 }else{
				 alert('One row should be present in table');
				 }
		 });
	 
	});

</script>

<form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_customer_write_exec&amp;" onsubmit="return insert_adm_bbs(this);">

	<div class="contents_box_middle">

		<h1 class="title">Customer Registration</h1>

		<div class="tip_box">

			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

			<ul>

				<li> - Register Customer Info.</li>

			</ul>

		</div>

		<div class="table_write_box">

			<h1 class="table_title">Customer Registration</h1>

			<table class="table_write" summary=" " cellspacing="0">

				<caption class="hidden"></caption>

				<col width="18%" /><col width="32%" /><col width="18%" /><col width="32%" />




                         
                                <tr>

                    <th class="key_02">Business Type</th>

                    <td class="value_02">
                    
                        <select name="values[business_type]" style="width: 129px;" class="input-text" required>
                            <option value="" ></option>
                            <?php foreach (Array('seller', 'buyer', 'both') as $Val) { ?>
                              <option value="<?php echo $Val; ?>"><?php echo $Val; ?></option>
                            <?php } ?>
                           
                        </select>
                                           

                    </td>
                                        
                    <th class="key_02"></th>

                    <td class="value_02">

                                            

                    </td>

                </tr>


                                


                                <tr>

					<th class="key_02">Login ID</th>

					<td class="value_02">
                                            <input type="text" name="values[member_id]" id="member_id">
                                            <div class="error_under error_member_id" id="error_member_id">
                                               
                                            </div>
                                           

					</td>
                                        
					<th class="key_02">Password</th>

					<td class="value_02">

                                            <input type="password" name="values[member_pwd]" id="company_name" value="" class="input_1" style="width:150px;" maxlength="50"/>

					</td>

				</tr>
                                
                                
                                
                                
                                
                                
                                
                                

				<tr>

					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />Country</th>

					<td class="value_02">

						<select name="values[member_country]" id="member_country" class="input-text" required>
                            <option value="" ></option>
                            <?
                            $slq="SELECT cc,country_name FROM iw_country_list";
                            $sql_query = mysql_query($slq);
                            while($row=mysql_fetch_array($sql_query)){


                            ?>
                            <option value="<?= $row['cc'] ?>" ><?= $row['country_name'] ?> </option>

                            <?
                            }mysql_free_result($sql_query);
                            ?>
                        </select>

						<!-- <input type="text" name="values[email]" id="" value="" class="input_1" onfocus="f_text(this);" onblur="b_text(this);" maxlength="100" /> -->

					</td>

					<th class="key_02">Company Name</th>

					<td class="value_02">

						<input type="text" name="values[company_name]" id="company_name" value="" class="input_1" style="width:150px;" maxlength="50" placeholder="Comapany Name"/>

					</td>

				</tr>

				<tr>

					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />Customer Name</th>

					<td class="value_02">

						<input type="text" name="values[member_first_name]" id="member_first_name" value="" class="input_1" style="width:80%;" maxlength="50" placeholder="Customer Name"/>

						<!-- <input type="text" name="values[member_last_name]" id="member_last_name" value="" class="input_1" style="width:150px;" maxlength="50" placeholder="Last Name"/> -->

					</td>


					<th class="key_02">E-mail</th>

					<td class="value_02">

						<input type="text" name="values[email]" id="" value="" class="input_1" style="width:80%;"  onfocus="f_text(this);" onblur="b_text(this);" maxlength="100" placeholder="Email"/>

					</td>
					
				</tr>

				<tr>

					<th class="key_02">Phone</th>

					<td class="value_02">

						<select name="values[phone_no1]" id="phone_code" class="input-text" style="width:20%";>
                            <option value="" ></option>
                            <?
                            $slq="SELECT country_name,phonecode FROM iw_country_list";
                            $sql_query = mysql_query($slq);
                            while($row=mysql_fetch_array($sql_query)){


                            ?>
                            <option value="<?= $row['phonecode'] ?>" ><?= $row['country_name'] ?>(<?=$row['phonecode']?>) </option>

                            <?
                            }mysql_free_result($sql_query);
                            ?>
                        </select>
                        -
						<input type="text" name="values[phone_no2]" value="" class="input_1" style="width:40%;" maxlength="20" />

					</td>

					<th class="key_02">Mobile</th>

					<td class="value_02">

						<select name="values[mobile_no1]" id="mobile_code" class="input-text" style="width:20%";>
                            <option value="" ></option>
                            <?
                            $slq="SELECT country_name,phonecode FROM iw_country_list";
                            $sql_query = mysql_query($slq);
                            while($row=mysql_fetch_array($sql_query)){


                            ?>
                            <option value="<?= $row['phonecode'] ?>" ><?= $row['country_name'] ?>(<?=$row['phonecode']?>) </option>

                            <?
                            }mysql_free_result($sql_query);
                            ?>
                        </select>
                        -

						<input type="text" name="values[mobile_no2]" value="" class="input_1" style="width:40%;" maxlength="20" />

					</td>
					

				</tr>

				<tr>

					<th class="key_02">Address</th>

					<td class="value_02" colspan="3">

						<input type="text" name="values[address]" value="" class="input_1" style="width:80%;" maxlength="150" placeholder="Address"/>
						

					</td>


				</tr>


				<tr>

					<th class="key_02">Consignee</th>

					<td class="value_02" colspan="3" style="padding:10px;">


						 <input type="button" onclick="javascript:void(0);" id='anc_add' value=" Add Row "/>
						 <input type="button" onclick="javascript:void(0);" id='anc_rem' value=" Remove Row "/>
						 
						 <table  id="tbl1" border="1">
						 <tr>
						 	<td>Consignee [1] : </td>
						 	<td><input type="text" name="consignee_name[]" value="" class="input_1" style="width:100%;" maxlength="150" placeholder="Consignee Name"></td>
						 	<td><input type="text" name="consignee_address[]" value="" class="input_1" style="width:100%;" maxlength="150" placeholder="Consignee Address"></td>
						 	<td><input type="text" name="consignee_phone[]" value="" class="input_1" style="width:100%;" maxlength="150" placeholder="Consignee Phone"></td>
						 </tr>
						 
						</table>

					</td>

				</tr>

				<tr>

					<th class="key_02">Sales (Person In Charge)</th>

					<td class="value_02" colspan="3">

						<select name="values[sales_no]" id="sales_no" class="input-text" >
                            <option value="" ></option>
                           <?php if(count($person_sale)>0){
                                foreach ($person_sale as $row){    
                         
                           ?>
                            <option value="<?= $row->member_no ?>" ><?= $row->member_first_name ?> </option>

                            <?
                            }}
                            
                            ?>
                        </select>

						<!-- <input type="text" name="values[email]" id="" value="" class="input_1" onfocus="f_text(this);" onblur="b_text(this);" maxlength="100" /> -->

					</td>					

				</tr>
                <tr>
                    <th scope="row">Photos </th>
                    <td colspan="3">
                        <iframe height="230px" class="upload_iframe" src="<?= base_url() ?>?c=admin&m=adm_cloud_upload&member_no=<?=$_GET['member_no']?>"></iframe>
                        <!-- <iframe style="width:100%;" src="/?c=admin&m=adm_image_uploader"></iframe> -->
                    </td>            

                </tr>
			</table>

			<div class="btn_area_center">

                            <input type="image" src="/images/admin/submit.gif" id="btn" title="Save" />

				<a href="/?<?= updateCurrentGetUrl(array("m"=>"adm_customer_list")) ?>" title="List"><img src="/images/admin/back.gif" alt="List" /></a>

			</div>

		</div>

	</div>

</form>

<script>
    
    
    var allow_save=true;
 $(document).ready(function () {
         $('#btn').click(function(){

         if(allow_save==false){
             alert("This ID already existed. Please choose another");
         }
        return allow_save;

    });
        $('#member_id').change(function () {
            var memeber_id = $(this).val();
           
            if (!memeber_id.match(/^[a-z\d_]{4,20}$/i)) {
                $('.error_member_id').html('');
                return false;
            }
          
            
            if (memeber_id.length >= 4 && memeber_id.length <= 20)
            {
                $.ajax({
                    url: '?c=admin&m=checkmemember',
                    type: "POST",
                    async: true,
                    data: {
                        'id': memeber_id
                    },
                    success: function (result)
                    {
                      
                        if (result == 1 )
                        {  
                            $('.error_member_id').html('This ID already exits');  
                             var ss =document.getElementById('error_member_id').innerHTML;
                            allow_save =false;
                            $('input[type="image"]').addClass('btn');   
                            return false;
                            
                        }
                        
                        if (result == 0)
                        {
                               allow_save =true;
                            $('.error_member_id').html('');
                            $('input[type="image"]').removeClass('btn');
                           
                            
                        }

                    }
                    
                });
                
                return false;
            }
           

        });
        
        
      
      
      
     



      
    });
    
      
     
   
         
    
    
    

    
   


</script>




<style>
    
    
    #showcolor
    {
        color: black;
        font-size: 15px;
        padding-left: 24px;
        text-align: right;
        width: 150px;

    }
    .error_member_id{
        color: red;
    }
    #error
    {
        color: red;
    }
</style>