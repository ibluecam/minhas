<?php 
   // var_dump($member_list);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Title of the document</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="/css/admin/admin.css" />
<link rel="stylesheet" type="text/css" href="/css/admin/button.css" />
<style>
    .container{
        min-width: 800px;
        
    }
     .container table{
       width: 800px;
    }
    .container .table_list{
    	width: 800px;
    }
   .pagination{
    margin: auto;
    width: 800px;
    text-align: center;
    background: #eee;
    padding: 5px;
    margin-bottom: 5px;
   }
   .bottom_button_container{
        width: 800px;
        margin: auto;
        margin-top:10px;
   }
   #add_button{
        padding: 5px 20px;
   }
   
</style>
</head>

<body style="background:white;">
    <div class="container">
        
        <form action="/?c=admin&m=adm_order_list_popup_exec" method="post">
            <table class="table_list" style="margin:auto;">
                <tr>
                    <th></th>
                    <th class="line" scope="col">
                        <img src="/images/admin/board_bar_line.gif">
                    </th>
                    <th>Order ID</th>
                    <th class="line" scope="col">
                        <img src="/images/admin/board_bar_line.gif">
                    </th>
                    <th>Name</th>
                    <th class="line" scope="col">
                        <img src="/images/admin/board_bar_line.gif">
                    </th>
                    <th>Stock No</th>
                    <th class="line" scope="col">
                        <img src="/images/admin/board_bar_line.gif">
                    </th>
                    <th>Order Date</th>
                    <th class="line" scope="col">
                        <img src="/images/admin/board_bar_line.gif">
                    </th>
                    <th>Cancel Date</th>
                    <th class="line" scope="col">
                        <img src="/images/admin/board_bar_line.gif">
                    </th>
                    <th>Order Status</th>
                </tr>
                <?php foreach($order_list as $order){?>
                <tr>
                    <td><input type="checkbox" name="order_idx[]" class="order_check" value="<?=$order->order_idx?>" data-idx="<?=$order->stock_idx?>" data-customer="<?=$order->customer_no?>"/></td>
                    <td></td>
                    <td><?= $order->order_idx ?></td>
                    <td></td>
                    <td><a target="_blank" href="/?c=admin&m=adm_member_view&mcd=&member_no=<?=$order->customer_no?>&cur_page=1&sParam="><?= $order->member_first_name." ".$order->member_last_name ?></a></td>
                    <td></td>
                    <td><?= $order->stock_idx ?></td>
                    <td></td>
                    <td><?= $order->order_dt ?></td>
                    <td></td>
                    <td><?= $order->cancel_dt ?></td>
                    <td></td>
                    <td><?= $order->order_status ?></td>
                </tr>
                <?php } ?>
                
            </table>
            <?php //echo $pagination;?>
            <div class="bottom_button_container">
                <input type="button" name="reserve" id="reserve_button" value="Reserve" class="button orange"/>
            </div>
        </form>
    </div>
    <script>
    	$(document).ready(function(e){
    		$("#reserve_button").click(function(e){
    			//reserve_check(idx);
    			if(countCheck()==1){
    				
    				var stock_idx = getStockIdx();
    				var customer_no = getCustomerNo();
    				
    				reserve_check(stock_idx, customer_no);
    			}
    		});
    	});

    	function countCheck(){
    		var numberOfChecked = $('.order_check:checked').length;
    		return numberOfChecked;
    	}

    	function getStockIdx(){
    		return $(".order_check:checked").attr("data-idx");
    	}

    	function getCustomerNo(){
    		return $(".order_check:checked").attr("data-customer");
    	}

    	function reserve_check(idx, customer_no) {
	       	window.open('/?c=admin&m=adm_reserve_popup_update&mcd=product&idx='+idx+'&customer_no='+customer_no,'deposit','width=720,height=550,top=100,left=100');  
	    }
    </script>
</body>

</html> 