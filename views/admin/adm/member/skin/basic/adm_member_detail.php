<?
    //회원 상세 조회
    $rows = '';
    if(count($member_detail_list) > 0)
    {
       $rows = $member_detail_list->row(); 
    }
?>
<script type="text/javascript">
    
    //회원 수정
    function update_adm_member(frm)
    {      
        if(!validate(frm.elements['values[zipcode1]'], '우편번호를 입력하세요.')) return false;
        if(!validate(frm.elements['values[zipcode2]'], '우편번호를 입력하세요.')) return false;
        if(!validate(frm.elements['values[address]'], '주소를 입력하세요.')) return false;
        if(!validate(frm.elements['values[address_de]'], '상세주소를 입력하세요.')) return false;
        
        if(!validate(frm.elements['values[phone_no1]'], '전화번호를 입력하세요.')) return false;
        if(!validate(frm.elements['values[phone_no2]'], '전화번호를 입력하세요.')) return false;
        if(!validate(frm.elements['values[phone_no3]'], '전화번호를 입력하세요.')) return false;
        if(!validate(frm.elements['values[mobile_no1]'], '휴대폰번호를 입력하세요.')) return false;
        if(!validate(frm.elements['values[mobile_no2]'], '휴대폰번호를 입력하세요.')) return false;
        if(!validate(frm.elements['values[mobile_no3]'], '휴대폰번호를 입력하세요.')) return false;
        if(!validate(frm.elements['values[email]'], '이메일을 입력하세요.')) return false;
        if(!emailCheck(frm.elements['values[email]'])) return false;
        
        return true;
    }    
    
    //회원 삭제
    function delete_adm_member()
    {
        if(!confirm('삭제 하시겠습니까?'))
        {
            return;
        }
        else
        {
            location.href = "/?c=admin&amp;m=adm_member_delete&amp;member_no=<?=$member_no?>&amp;cur_page=<?=$cur_page?>&amp;sParam=<?=urlencode($sParam)?>";
        }
    }    

</script>
<form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_member_update" onsubmit="return update_adm_member(this);">
<input type="hidden" name="values[member_no]" id="member_no" value="<?=$member_no?>" />
<input type="hidden" name="cur_page" value="<?=$cur_page?>" />
<input type="hidden" name="sParam" value="<?=$sParam?>" />
            <div class="contents_box_middle">
                <h1 class="title">회원 관리</h1>
                <div class="tip_box">
                    <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
                    <ul>
                        <li> - 회원정보를 조회, 수정, 삭제, 회원 등급 변경등의 관리를 할 수 있습니다.</li>
                    </ul>
                </div>
                <div class="table_write_box">
                    <h1 class="table_title">회원상세정보</h1>
                    <table class="table_write" summary="회원정보" cellspacing="0">
                        <caption class="hidden"></caption>
                        <col width="16%" /><col width="34%" /><col width="16%" /><col width="34%" />
                        <tr>
                            <th class="key_02">회원명</th>
                            <td class="value_02"><strong>&nbsp;<?=$rows->member_name?></strong></td>                            
                            <th class="key_02">아이디</th>
                            <td class="value_02"><strong>&nbsp;<?=$rows->member_id?></strong></td>
                        </tr>
                        <tr>
                            <th class="key_02">주민등록번호</th>
                            <td class="value_02">&nbsp;
<?
    if($rows->jumin_no != '')
    {
        $jumin_no  = func_base64_decode($rows->jumin_no);
        $jumin_no1 = substr($jumin_no, 0, 6);
    
        echo $jumin_no1.'-'.repeater('*',7);
    }
?>
                            </td>
                            <th class="key_02">비밀번호</th>
                            <td class="value_02">&nbsp;
<? 
    $member_pwd  = func_base64_decode($rows->member_pwd);
    $member_pwd1 = substr($member_pwd, 0, 4);
    
    echo $member_pwd1.repeater('*', strlen($member_pwd) - strlen($member_pwd1));
?>                                
                            </td>                             
                        </tr> 
                        <tr>
                            <th class="key_02">전화번호</th>
                            <td class="value_02">
                                <input type="text" name="values[phone_no1]" id="phone_no1" value="<?=$rows->phone_no1?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, adm_frm.elements["values[phone_no2]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> - 
                                <input type="text" name="values[phone_no2]" id="phone_no2" value="<?=$rows->phone_no2?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, adm_frm.elements["values[phone_no3]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
                                <input type="text" name="values[phone_no3]" id="phone_no3" value="<?=$rows->phone_no3?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" />
                            </td>
                            <th class="key_02">회원등급</th>
                            <td class="value_02">
                                <select name="values[grade_no]" id="grade_no" class="input_2">
<?
    if(count($member_grade_list) > 0)
    {
        foreach($member_grade_list as $grade_rows)
        {
?>
                                    <option value="<?=$grade_rows->grade_no?>" <?=func_decode($grade_rows->grade_no, $rows->grade_no, 'selected', '')?>><?=$grade_rows->grade_name?> <? if($grade_rows->grade_desc != '') echo '('.$grade_rows->grade_desc.')';?></option>
<?            
        }
    }
?>                                
                                </select>
                            </td>                                                 
                        </tr>
                        <tr>
                            <th class="key_02">휴대폰번호</th>
                            <td class="value_02" colspan="3">
                                <input type="text" name="values[mobile_no1]" id="mobile_no1" value="<?=$rows->mobile_no1?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, adm_frm.elements["values[mobile_no2]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> - 
                                <input type="text" name="values[mobile_no2]" id="mobile_no2" value="<?=$rows->mobile_no2?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, adm_frm.elements["values[mobile_no3]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
                                <input type="text" name="values[mobile_no3]" id="mobile_no3" value="<?=$rows->mobile_no3?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" />
                                <input type="checkbox" name="values[sms_recv_yn]" id="sms_recv_yn" value="Y" class="checkbox" <?=func_decode($rows->sms_recv_yn, 'Y', 'checked', '')?> />
                                <span class="text">SMS 수신 여부</span>                            
                            </td>
                        </tr>
                        <tr>                             
                            <th class="key_02">이메일</th>
                            <td class="value_02" colspan="3">
                                <input type="text" name="values[email]" id="email" value="<?=$rows->email?>" class="input_1" style="width:160px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="50" />
                                <input type="checkbox" name="values[email_recv_yn]" id="email_recv_yn" value="Y" class="checkbox" <?=func_decode($rows->email_recv_yn, 'Y', 'checked', '')?> />
                                <span class="text">E-Mail 수신 여부</span>
                            </td>
                        </tr>
                        <tr>
                            <th class="key_02">주소</th>
                            <td class="value_02" colspan="3">
                                <input type="text" name="values[zipcode1]" id="zipcode1" value="<?=substr($rows->zipcode, 0, 3)?>" class="input_1" title="앞번호" style="width:25px;" onfocus="f_text(this);" onblur="b_text(this);" readonly /> -
                                <input type="text" name="values[zipcode2]" id="zipcode2" value="<?=substr($rows->zipcode, 3, 3);?>" class="input_1" title="뒷번호" style="width:25px;" onfocus="f_text(this);" onblur="b_text(this);" readonly />
                                <a href="#" onclick="win_open2('/?c=common&m=zipcode&fn=adm_frm', 'zipcode', '417', '255', 'yes'); return false;" title="우편번호 찾기"><img src="/images/common/member/btn_post.gif" alt="우편번호 찾기" align="middle" /></a>
                                <input type="text" name="values[address]" id="address" value="<?=$rows->address?>" class="input_1" title="주소" style="width:250px;" readonly />
                                <input type="text" name="values[address_de]" id="address_de" value="<?=$rows->address_de?>" class="input_1" style="width:200px;" title="나머지 주소" onfocus="f_text(this);" onblur="b_text(this);" />
                            </td>
                        </tr>                        
                        <tr>                             
                            <th class="key_02">가입일</th>
                            <td class="value_02"><strong>&nbsp;<?=$rows->join_dt?></strong></td>
                            <th class="key_02">수정일</th>
                            <td class="value_02"><strong>&nbsp;<?=$rows->updated_dt?></strong></td>
                        </tr>                        
                        <tr>                             
                            <th class="key_02">최근접속일</th>
                            <td class="value_02"><strong>&nbsp;<?=$rows->last_login_dt?></strong></td>
                            <th class="key_02">방문수</th>
                            <td class="value_02"><strong>&nbsp;<?=$rows->login_cnt?></strong></td>
                        </tr>
                        <tr>                             
                            <th class="key_02">인증여부</th>
                            <td class="value_02">
                            	<input type="radio" name="values[certi_yn]" id="certi_yn_01" value="Y" class="radio" <?=func_decode($rows->certi_yn, 'Y', 'checked', '')?> />
                                <label for="certi_yn_01">인증</label>
                                <input type="radio" name="values[certi_yn]" id="certi_yn_02" value="N" class="radio" <?=func_decode($rows->certi_yn, 'N', 'checked', '')?> />
                            	<label for="certi_yn_02">미인증</label>  
                            </td>
                            <th class="key_02">약관동의여부</th>
                            <td class="value_02"><?=func_decode($rows->contract_agree_yn, 'Y', '동의', '미동의')?></td>
                        </tr>                        
                        <tr>                             
                            <th class="key_02">생년월일</th>
                            <td class="value_02">
                                <input type="text" name="values[birthday]" id="birthday" value="<?=$rows->birthday?>" class="input_1" style="width:65px;" readonly />
                                <a href="#" onclick="Calendar(document.adm_frm.elements['values[birthday]']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
                                <input type="radio" name="values[sun_moon]" id="sun_moon_01" value="S" class="radio" <?=func_decode($rows->sun_moon, 'S', 'checked', '')?> />
                                <label for="sun_moon_01">양력</label>
                                <input type="radio" name="values[sun_moon]" id="sun_moon_02" value="M" class="radio" <?=func_decode($rows->sun_moon, 'M', 'checked', '')?> />
                            	<label for="sun_moon_02">음력</label>
                            </td>
                            <th class="key_02">성별</th>
                            <td class="value_02">
                            	<input type="radio" name="values[sex]" id="sex_01" value="M" class="radio" <?=func_decode($rows->sex, 'M', 'checked', '')?> />
                                <label for="sex_01">남자</label>
                                <input type="radio" name="values[sex]" id="sex_02" value="W" class="radio" <?=func_decode($rows->sex, 'W', 'checked', '')?> />
                            	<label for="sex_02">여자</label> 
                            </td>                            
                        </tr>                                                                                                                                      
                        <tr>
                            <th class="key_02">사용자 메모</th>
                            <td class="value_02" colspan="3" style="padding:10px;">
                                <textarea name="values[user_memo]" id="contents" class="textarea" style="height:80px;"><?=$rows->user_memo?></textarea>
                           </td>
                        </tr>
                        <tr>
                            <th class="key_02">관리자 메모</th>
                            <td class="value_02" colspan="3" style="padding:10px;">
                                <textarea name="values[admin_memo]" id="contents" class="textarea" style="height:80px;"><?=$rows->admin_memo?></textarea>
                           </td>
                        </tr> 
                    </table>
                    <div class="btn_area_center">
                        <input type="image" src="/images/admin/btn_modify.gif" title="수정" />
                        <a href="#" onclick="delete_adm_member(); return false;" title="삭제"><img src="/images/admin/btn_delete.gif" alt="삭제" /></a>
                        <a href="/?c=admin&amp;m=adm_member_list&amp;cur_page=<?=$cur_page?><?=func_sPUrlEncode($sParam, "sch_word")?>" title="목록"><img src="/images/admin/btn_list.gif" alt="목록" /></a>
                    </div>
                </div>                
            </div>
</form>            