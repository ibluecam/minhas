<script type="text/javascript">
    //메일 발송
    function email_insert()
    {
        var frm = document.adm_frm;
        
        if(!validate(frm.elements['send_name'], '보내는 사람을 입력하세요.')) return;
        if(!validate(frm.elements['send_email'], '보내는 메일을 입력하세요.')) return;
        if(!validate(frm.elements['subject'], '메일 제목을 입력하세요.')) return;
        
        if(xed.getWysiwygContent() == '' || xed.getWysiwygContent() == '<p>&nbsp;</p>')
        { 
            alert("메일 내용을 입력하세요."); 
            return; 
        }       
        frm.mail_contents.value = xed.getWysiwygContent();
       
        for(i=0; i<frm.receive_email.length; i++)
        {
            frm.receive_email[i].selected = true;
        } 
        
        frm.action = '/?c=admin&m=adm_member_email_exec';    
        frm.submit();
    }
</script>
<form name="adm_frm" id="adm_frm" method="post" action="" onsubmit="" enctype="multipart/form-data">
<input type="hidden" name="cur_page" value="<?=$cur_page?>" />
<input type="hidden" name="sParam" value="<?=$sParam?>" /> 
<input type="hidden" name="mail_contents" value="" /> 
            <div class="contents_box_middle">
                <h1 class="title">메일발송</h1>
                <div class="tip_box">
                    <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
                    <ul>
                        <li> - 회원들에게 메일을 발송 할 수 있습니다.</li>
                    </ul>
                </div>
                <div class="table_write_box">
                    <h1 class="table_title">메일발송</h1>
                    <table class="table_write" summary="메일발송" cellspacing="0">
                        <caption class="hidden"></caption>
                        <col width="18%" /><col width="32%" /><col width="18%" /><col width="32%" />
                        <tr>
                            <th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />보내는 사람</th>
                            <td class="value_02">
                                <input type="text" name="send_name" id="send_name" value="" class="input_1" style="width:150px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="100" />
                            </td>   
                            <th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />보내는 메일</th>
                            <td class="value_02">
                                <input type="text" name="send_email" id="send_email" value="<?=$email?>" class="input_1" style="width:150px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="100" />
                            </td>                                                     
                        </tr>
                        <tr>
                            <th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />수신자</th>
                            <td style="padding:5px 10px">
                                <select name="receive_email[]" id="receive_email" class="input_2" style="width:240px;height:80px;" multiple>
<?
    $first_name = '';
    $cnt = 0;
    if(count($mail_member_no) > 0)
    {
        for($i=0; $i<count($mail_member_no); $i++)
        {
            if($cnt == 0) $first_name = $mail_member_name[$i];
?>
                                    <option value="<?=$mail_member_no[$i]?>"><?=$mail_member_name[$i]?>(<?=$mail_member_id[$i]?>) - <?=$mail_member_email[$i]?></option>
<?       
            $cnt++;     
        }
    }
?>                                    

                                </select>                                
                            </td> 
                            <td class="value_02" colspan="2" valign="top" style="color:#e85716;"> 
                                - 메일 발송 회원 <strong>"<?=$cnt?>"</strong>명이 선택 되었습니다.
                            </td>
                        </tr>                                                                   
                        <tr>
                            <th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />메일 제목</th>
                            <td class="value_02" colspan="3">
                                <input type="text" name="subject" id="subject" value="" class="input_1" style="width:395px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="100" />
                            </td>                            
                        </tr>  
                                       
                        <tr>
                            <th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />메일 내용</th>
                            <td class="value_02" colspan="3" style="padding:10px;">
                                <textarea name="contents" id="contents" class="textarea"></textarea>
                   
<script type="text/javascript">
    var xed;
    window.onload = function() {
    	xed = new xq.Editor("contents");
    	xed.isSingleFileUpload = true;
    	xed.addPlugin('FileUpload');
    	xed.setFileUploadTarget('helpers/iw_SWFUpload_single.php?mcd=mail', null);
    	xed.setEditMode('wysiwyg');
    	xed.setWidth("100%");
    }
</script>

                            </td>
                        </tr>
                        <tr>
                            <th class="key_02">첨부파일</th>
                            <td class="value_02" colspan="3">
                                <div style="padding:1px; 0px;"><input type="file" name="attach1" class="input_2" style="width:400px;" /></div>
                            </td>
                        </tr>                        
                    </table>
                    <div class="btn_area_center">
                        <a href="#" onclick="email_insert(); return false;" title="확인"><img src="/images/admin/btn_ok.gif" alt="확인" /></a>
                        <a href="/?c=admin&amp;m=adm_member_list" title="목록"><img src="/images/admin/btn_list.gif" alt="목록" /></a>
                    </div>
                </div>                
            </div>
</form>  
<!--<iframe name="hFrame" border="0" style="width:0px;height:0px"></iframe>-->         