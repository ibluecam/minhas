<script type="text/javascript">
    //체크박스 전체 선택
    function checkbox_all_check(obj, target)
    {
    	if ( typeof obj == 'undefined' || typeof target == 'undefined')
    	{
    	}
    	else if ( typeof target.length == 'undefined' )
    	{
    		target.checked = obj.checked;
    	}
    	else
    	{
    		for (i = 0 ; i < target.length ; i++)
    		{
    			target[i].checked = obj.checked;
    		}
    	}
    }
    
    //선택 메일 발송
    function email_send()
    {
        var f  = document.adm_frm;
        var fg = false;
        
        if(!f.all_email.checked)
        {
            if(f.member_no != null)
            {
                for(i=0; i<f.member_no.length; i++)
                {
                    if(f.member_no[i].checked)
                    {
                        fg = true;
                    }
                }
                
                if(!fg)
                {
                    alert('메일발송할 회원을 선택하세요.');
                    return;
                }
            }
        }
        
        alert('참고하세요.!\n\n전체(선택)회원이 100명이상 일시 50명씩 분할 발송 하세요.!');  
            
        f.action = "/?c=admin&m=adm_member_email&cur_page=<?=$cur_page.func_sPUrlEncode($sParam, 'sch_word')?>&sParam=<?=urlencode($sParam)?>";
        f.submit();        
    }
 
</script>
<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_member_list" onsubmit="return search_member(this);">
            <div class="contents_box_middle">
                <h1 class="title">회원 관리</h1>
                <div class="tip_box">
                    <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
                    <ul>
                        <li> - 회원정보를 조회, 수정, 삭제, 회원 등급 변경등의 관리를 할 수 있습니다.</li>
                    </ul>
                </div>
                <div class="table_list_box">
                    <h1 class="table_title">회원정보</h1>
                    <div class="search_box">
                        <div class="search_result">
                            <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />전체 : <span class="title"><?= $total_rows?></span> 건, 페이지 : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>
                        </div>
                        <div class="search_condition">
                            - 가입일
                            <input type="text" name="sch_create_dt_s" id="sch_create_dt_s" value="" class="input_1" style="width:65px;" readonly />
                            <a href="#" onclick="Calendar(document.adm_frm.elements['sch_create_dt_s']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>&nbsp;~
                            <input type="text" name="sch_create_dt_e" id="sch_create_dt_e" value="" class="input_1" style="width:65px;" readonly />
                            <a href="#" onclick="Calendar(document.adm_frm.elements['sch_create_dt_e']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
                            <select name="grade_no" id="grade_no" class="input_2" align="absmiddle">
                                <option value="">::회원등급::</option>
<?
    if(count($member_grade_list) > 0)
    {
        foreach($member_grade_list as $grade_rows)
        {
?>
                                <option value="<?=$grade_rows->grade_no?>"><?=$grade_rows->grade_name?></option>
<? 
        }       
    }
?>
                            </select>                 
                            <select name="sch_condition" id="sch_condition" class="input_2" style="width:100px;" align="absmiddle">
                                <option value="member_id like">아이디</option>
                                <option value="member_name like">회원명</option>
                                <option value="email like">이메일</option>
                                <option value="phone_no1 like|phone_no2 like|phone_no3 like">전화번호</option>
                                <option value="mobile_no1 like|mobile_no2 like|mobile_no3 like">휴대폰번호</option>
                                <option value="address like|address_de like">주소</option>                                
                            </select>
                            <input type="text" name="sch_word" id="sch_word" value="" class="input_1" style="width:100px;" title="검색어" /> 
                            <input type="image" src="/images/admin/btn_search_01.gif" title="검색" align="absmiddle" />
                        </div>
                    </div>             
                    <table class="table_list" summary="회원정보" cellspacing="0">
                        <caption class="hidden"></caption>
                        <col width="4%" /><col width="2px" />
                        <col width="6%" /><col width="2px" />
                        <col width="13%" /><col width="2px" />
                        <col width="13%" /><col width="2px" />
                        <col width="" /><col width="2px" />
                        <col width="" /><col width="2px" />
                        <col width="" /><col width="2px" />
                        <col width="" /><col width="2px" />
                        <col width="" />
                        <thead>
                            <tr>
                                <th scope="col"><input type="checkbox" name="member_no" id="member_no" value="" onclick="checkbox_all_check(this, document.adm_frm.member_no);"/></th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                                <th scope="col">번호</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>                                
                                <th scope="col">아이디</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                                <th scope="col">회원명</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                                <th scope="col">회원등급</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>                                
                                <th scope="col">이메일</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                                <th scope="col">가입일</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                                <th scope="col">최근접속일</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                                <th scope="col">인증</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>                                
                                <th scope="col">방문수</th>                               
                            </tr>
                        </thead>
                        <tbody>
<?
    //일반글
    if(count($member_list) > 0)
    {
        $link = ''; //링크
        
        foreach($member_list as $rows)
        {
            //링크
            $link = "/?c=admin&m=adm_member_detail&member_no=$rows->member_no&cur_page=$cur_page".func_sPUrlEncode($sParam, "sch_word")."&sParam=".urlencode($sParam);
?>                            
                            <tr>
                                <td><input type="checkbox" name="member_no[]" id="member_no" value="<?=$rows->member_no?>" /></td>
                                <td class="line">&nbsp;</td>                               
                                <td><?=$row_cnt--?></td>
                                <td class="line">&nbsp;</td>
                                <td><a href="<?=$link?>" title="회원상세"><strong><?=$rows->member_id?></strong></a></td>
                                <td class="line">&nbsp;</td>
                                <td><a href="<?=$link?>" title="회원상세"><?=$rows->member_name?></a></td>
                                <td class="line">&nbsp;</td>
                                <td><?=$rows->grade_name?></td>
                                <td class="line">&nbsp;</td>
                                <td><?=$rows->email?></td>
                                <td class="line">&nbsp;</td>   
                                <td><?=$rows->join_dt?></td>
                                <td class="line">&nbsp;</td>
                                <td><?=$rows->last_login_dt?></td>
                                <td class="line">&nbsp;</td>
                                <td><?=func_decode($rows->certi_yn, 'Y', '<font color="blue">인증</font>', '<font color="red">미인증</font>')?></td>
                                <td class="line">&nbsp;</td>                                
                                <td><?=$rows->login_cnt?></td>
                            </tr>
<?            
        }
    }
    else
    {
?>
                           
                            <tr>
                                <td colspan="20" class="no_data">Data does not exist.</td>
                            </tr>
<?  
    }
?>       
                      
                        </tbody>
                    </table>
                    <div class="page_navi"><?=func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_member_list')?></div> 
                    <div class="btn_area_right">
                        <!--<a href="#" onclick="alert('준비중입니다.'); return false;" title="등록"><img src="/images/admin/btn_regist_01.gif" alt="등록" /></a>-->
                    </div>
                    <div class="search_box" style="padding:15px 10px;background-color:#ffffff">
<?
    //메일발송 여부 
    if(func_get_config($member_config_result, 'join_email_yn') == 'Y')
    {
?>
                        <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />선택한 회원 또는
                        <span style="font-size:12px;color:#e85716;">(전체 회원 발송</span>&nbsp;<input type="checkbox" name="all_email" id="all_email" value="Y" />)
                        <a href="#" onclick="email_send(); return false;" title="메일발송"><img src="/images/admin/btn_mail_send.gif" alt="메일발송" align="absmiddle" /></a>
<?
    }
?>
                    </div>                     
                </div>
            </div>
</form>            