<?php 
   // var_dump($member_list);
?>
 <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Title of the document</title>
<link rel="stylesheet" type="text/css" href="/css/admin/admin.css" />
<link rel="stylesheet" type="text/css" href="/css/admin/button.css" />
<style>
    .container{
        min-width: 500px;
        
    }
     .container table{
       width: 500px;
    }
   .pagination{
    margin: auto;
    width: 500px;
    text-align: center;
    background: #eee;
    padding: 5px;
    margin-bottom: 5px;
   }
   .bottom_button_container{
        width: 500px;
        margin: auto;
   }
   #add_button{
        padding: 5px 20px;
   }
   
</style>
</head>

<body style="background:white;">
    <div class="container">
        
        <form action="/?c=admin&m=adm_member_list_popup_exec" method="post">
            <table class="table_list" style="margin:auto;">
                <tr>
                    <th></th>
                    <th class="line" scope="col">
                        <img src="/images/admin/board_bar_line.gif">
                    </th>
                    <th>Name</th>
                    <th class="line" scope="col">
                        <img src="/images/admin/board_bar_line.gif">
                    </th>
                    <th>ID</th>
                    <th class="line" scope="col">
                        <img src="/images/admin/board_bar_line.gif">
                    </th>
                    <th>Joined Date</th>
                </tr>
                <?php foreach($member_list as $member){?>
                <tr>
                    <td><input type="checkbox" name="member_no[]" value="<?=$member->member_no?>" /></td>
                    <td></td>
                    <td><?=$member->member_first_name." ".$member->member_last_name?></td>
                    <td></td>
                    <td><?=$member->member_id?></td>
                    <td></td>
                    <td><?=$member->created_dt?></td>
                </tr>
                <?php } ?>
                
            </table>
            <?php echo $pagination;?>
            <div class="bottom_button_container">
                <input type="submit" name="add" id="add_button" value="Add" class="button blue"/>
            </div>
        </form>
    </div>
</body>

</html> 