<?

//회원 상세 조회
$rows = '';
if(count($member_detail_list) > 0)
{
    $rows = $member_detail_list->row();
}
?>

<script type="text/javascript">

    //회원 수정
    function update_adm_member(frm)
    {
        /*if(!validate(frm.elements['values[zipcode1]'], '우편번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[zipcode2]'], '우편번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[address]'], '주소를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[address_de]'], '상세주소를 입력하세요.')) return false;
         
         
         
         if(!validate(frm.elements['values[phone_no1]'], '전화번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[phone_no2]'], '전화번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[phone_no3]'], '전화번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[mobile_no1]'], '휴대폰번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[mobile_no2]'], '휴대폰번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[mobile_no3]'], '휴대폰번호를 입력하세요.')) return false;
         
         if(!validate(frm.elements['values[email]'], '이메일을 입력하세요.')) return false;
         
         if(!emailCheck(frm.elements['values[email]'])) return false;
         
         */
        return true;
    }

    //회원 삭제
    function delete_adm_member()
    {
        if (!confirm('Do you want to delete this member?'))
        {
            return;
        }
        else
        {
            location.href = "/?c=admin&m=adm_member_delete&member_no=<?= $member_no ?>&cur_page=<?= $cur_page ?>&sParam=<?= urlencode($sParam) ?>";
        }

    }

    function modify_check()
    {
        document.forms["adm_frm"].submit();
    }



</script>

<form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_member_update" onsubmit="return update_adm_member(this);">

    <input type="hidden" name="values[member_no]" id="member_no" value="<?= $member_no ?>" />
    <input type="hidden" name="cur_page" value="<?= $cur_page ?>" />

    <input type="hidden" name="sParam" value="<?= $sParam ?>" />

    <div class="contents_box_middle">

        <h1 class="title">Staff Management</h1>

        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You can manage your membership information, such as query, modify, delete, change membership level.</li>

            </ul>

        </div>



        <div class="table_write_box">

            <h1 class="table_title">Member Details</h1>

            <table class="table_write" summary="회원정보" cellspacing="0">

                <caption class="hidden"></caption>

                <col width="16%" /><col width="34%" /><col width="16%" /><col width="34%" />
                <tr>

                    <td class="key_02"></td>

                    <td class="value_02"></td>

                    <th class="key_02">
                        <strong>Live Chat Supporter</strong>
                    </th>

                    <td class="value_02">
                        <label class="chat_support_yes"><input <?php if($rows->chat_support=='Y') echo "checked"; ?> type="radio" value="Y" name="values[chat_support]"/>&nbsp;Yes</label>
                        <label class="chat_support_no"><input <?php if($rows->chat_support!='Y') echo "checked"; ?> type="radio" value="N" name="values[chat_support]"/>&nbsp;No</label>
                    </td>

                </tr>
                <tr>

                    <th class="key_02">Name</th>

                    <td class="value_02"><input type="text" name="values[member_first_name]" id="member_first_name" value="<?= $rows->member_first_name ?>" class="input_1" style="width:160px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="50" />
                    </td>

                    <th class="key_02">ID</th>

                    <td class="value_02"><input type="text" name="values[member_id]" id="member_id" value="<?= $rows->member_id ?>" class="input_1" style="width:160px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="50" /></td>

                </tr>

                <tr>

                    <th class="key_02">Regident Registration Number</th>

                    <td class="value_02">&nbsp;

                        <?

                        if($rows->jumin_no != '')

                        {

                        $jumin_no  = func_base64_decode($rows->jumin_no);

                        $jumin_no1 = substr($jumin_no, 0, 6);



                        echo $jumin_no1.'-'.repeater('*',7);

                        }

                        ?>

                    </td>

                    <th class="key_02">Password</th>

                    <td class="value_02">

                        <?

                        // $member_pwd  = func_base64_decode($rows->member_pwd);

                        ?>

                        <input type="password" name="member_pwd" id="member_pwd" value="" class="input_1"  />



                    </td>

                </tr>

                <tr>

                    <th class="key_02">Tel.</th>

                    <td class="value_02">

                        <select name="values[phone_no1]" id="phone_no1" class="input_2" style="width:60px;ime-mode:disabled;">
                            <?= ($rows->phone_no1 !="") ? '<option value='.$rows->phone_no1.'>'.$rows->phone_no1.'</option>': '<option value="" ></option>'; ?>
                            <?
                            $slq="SELECT country_name,phonecode FROM iw_country_list";
                            $sql_query = mysql_query($slq);
                            while($row=mysql_fetch_array($sql_query)){


                            ?>
                            <option value="<?= $row['phonecode'] ?>" ><?= $row['country_name'] ?>(<?=$row['phonecode']?>) </option>

                            <?
                            }mysql_free_result($sql_query);
                            ?>
                        </select>

                        <input type="text" name="values[phone_no2]" id="phone_no2" value="<?= $rows->phone_no2 ?>" class="input_1" style="width:96px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, adm_frm.elements["values[phone_no2]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="11" /> 


                    </td>

                    <th class="key_02">Member Grade</th>

                    <td class="value_02">

                        <select required name="values[grade_no]" id="grade_no" class="input_2">
                                <option value="" <?= func_decode('', $rows->grade_no, 'selected', '') ?>>
                                    ::Member Rating::
                                </option>
                                <option value="1" <?= func_decode(1, $rows->grade_no, 'selected', '') ?> >Admin</option>
                                <option value="2" <?= func_decode(2, $rows->grade_no, 'selected', '') ?> >Manager</option>
                                <option value="3" <?= func_decode(3, $rows->grade_no, 'selected', '') ?> >Sales</option>

                        </select> 

                    </td>

                </tr>

                <tr>

                    <th class="key_02">Mobile</th>

                    <td class="value_02">
                        <select name="values[mobile_no1]" id="mobile_no1" class="input_2" style="width:60px;ime-mode:disabled;">
                            <?= ($rows->mobile_no1 !="") ? '<option value='.$rows->mobile_no1.'>'.$rows->mobile_no1.'</option>': '<option value="" ></option>'; ?>
                            <?
                            $slq="SELECT country_name,phonecode FROM iw_country_list";
                            $sql_query = mysql_query($slq);
                            while($row=mysql_fetch_array($sql_query)){
                            ?>

                            <option value="<?= $row['phonecode'] ?>" ><?= $row['country_name'] ?>(<?=$row['phonecode']?>) </option>

                            <?
                            }mysql_free_result($sql_query);
                            ?>
                        </select>

                        <input type="text" name="values[mobile_no2]" id="mobile_no2" value="<?= $rows->mobile_no2 ?>" class="input_1" style="width:96px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, adm_frm.elements["values[mobile_no2]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="11" /> 


                    </td>
                    <th class="key_02">Country Stock</th>

                    <td class="value_02">
                        <select name="values[member_country]" id="member_country" class="input_2" style="width:120px;">
                             <option value="" <?= func_decode('', $rows->member_country, 'selected', '') ?>>
                                    ::Stock Country::
                                </option>
                                <option value="jp" <?= func_decode('jp', $rows->member_country, 'selected', '') ?> >Japan</option>
                                <option value="kr" <?= func_decode('kr', $rows->member_country, 'selected', '') ?> >South Korea</option>

                        </select>
                    </td>

                </tr>

                <tr>

                    <th class="key_02">E-Mail</th>

                    <td class="value_02">

                        <input type="text" name="values[email]" id="email" value="<?= $rows->email ?>" class="input_1" style="width:160px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="255" />

                    </td>

                    <th class="key_02">Facebook_ID</th>

                    <td class="value_02">

                        <input type="text" name="values[facebook_id]" id="facebook_id" value="<?= $rows->facebook_id ?>" class="input_1" style="width:160px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="255" />

                    </td>

                </tr>

                <tr>

                    <th class="key_02">Viber_ID</th>

                    <td class="value_02">

                        <input type="text" name="values[viber_id]" id="viber_id" value="<?= $rows->viber_id ?>" class="input_1" style="width:160px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="255" />

                    </td>

                    <th class="key_02">Whatsapp_ID</th>

                    <td class="value_02">

                        <input type="text" name="values[whatsapp_id]" id="whatsapp_id" value="<?= $rows->whatsapp_id ?>" class="input_1" style="width:160px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="255" />

                    </td>

                </tr>

                <tr>

                    <th class="key_02">Skype_ID</th>

                    <td class="value_02" colspan="3">

                        <input type="text" name="values[skype_id]" id="skype_id" value="<?= $rows->skype_id ?>" class="input_1" style="width:160px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="255" />

                    </td>

                </tr>

                <tr>

                    <th class="key_02">Address</th>

                    <td class="value_02" colspan="3">
                        <input type="text" name="values[address]" id="address" value="<?= $rows->address ?>" class="input_1"  style="width:571px;" />

                    </td>

                </tr>

                <tr>

                    <th class="key_02">Join Date</th>

                    <td class="value_02"><strong>&nbsp;<?= $rows->join_dt ?></strong></td>

                    <th class="key_02">Update Date</th>

                    <td class="value_02"><strong>&nbsp;<?= $rows->updated_dt ?></strong></td>

                </tr>

                <tr>

                    <th class="key_02">Last Login Date</th>

                    <td class="value_02"><strong>&nbsp;<?= $rows->last_login_dt ?></strong></td>

                    <th class="key_02">Login Count</th>

                    <td class="value_02"><strong>&nbsp;<?= $rows->login_cnt ?></strong></td>

                </tr>

                <tr>

                    <th class="key_02">Certification</th>

                    <td class="value_02">

                        

                        <label for="certi_yn_01"><input type="radio" name="values[certi_yn]"  id="certi_yn_01" value="Y" <?= func_decode($rows->certi_yn, 'Y', 'checked', '') ?> /> Yes</label>

                        

                        <label for="certi_yn_02"><input type="radio" name="values[certi_yn]" id="certi_yn_02" value="N" <?= func_decode($rows->certi_yn, 'N', 'checked', '') ?> /> No</label>

                    </td>

                    <th class="key_02">Contract Agreement</th>

                    <td class="value_02"><?= func_decode($rows->contract_agree_yn, 'Y', 'Yes', 'No') ?></td>

                </tr>

                <tr>

                    <th class="key_02">Birthday</th>

                    <td class="value_02">

                        <input type="text" data-date-format="yyyy-mm-dd" name="values[birthday]" id="birthday" value="<?= $rows->birthday ?>" class="input_1 datepicker-add-options" style="width:65px;" data-auto-close="true"/>

                        

                      
                    </td>

                    <th class="key_02">Gender</th>

                    <td class="value_02">

                       

                        <label for="sex_01"> <input type="radio" name="values[sex]" id="sex_01" value="M" <?= func_decode($rows->sex, 'M', 'checked', '') ?> /> Male</label>

                        

                        <label for="sex_02"><input type="radio" name="values[sex]" id="sex_02" value="W" <?= func_decode($rows->sex, 'W', 'checked', '') ?> /> Female</label>

                    </td>

                </tr>

                <tr>

                    <th class="key_02">User Memo</th>

                    <td class="value_02" colspan="3" style="padding:10px;">

                        <textarea name="values[user_memo]" id="contents" class="textarea" style="height:80px;"><?= $rows->user_memo ?></textarea>

                    </td>

                </tr>

                <tr>

                    <th class="key_02">Admin Memo</th>

                    <td class="value_02" colspan="3" style="padding:10px;">

                        <textarea name="values[admin_memo]" id="contents" class="textarea" style="height:80px;"><?= $rows->admin_memo ?></textarea>

                    </td>

                </tr>

                <tr>

                    <th scope="row">Photos </th>

                    <td class="text" colspan="3">
                        <!-- <div>
                            <a href="#" id="upload_widget_opener" ><span class="button blue">Upload images</span></a>
                        </div> -->
                            
                        <?php //$url = (("$_SERVER[HTTP_HOST]"=='www.motorbb.com') ? "https" : "http") . "://$_SERVER[HTTP_HOST]"; ?>

                        <iframe height="230px" id="upload_iframe" class="upload_iframe" src="/?c=admin&m=adm_cloud_upload&member_no=<?php echo $member_no =($_GET['member_no']!= '' ? $_GET['member_no'] : '');?>"></iframe>
                       
                    </td>                   

                </tr>
                
            </table>

            <div class="btn_area_center">

                <div class="btn_area_right">
                    <a href="#" onclick="modify_check();" title="save"><span class="button blue">Save</span></a>
                   <!--  <a href="#" onclick="delete_adm_member();return false;" title="delete"><span class="button red">Delete</span></a> -->
                    <a href="/?c=admin&amp;m=adm_member_list&amp;cur_page=<?= $cur_page ?><?= func_sPUrlEncode($sParam, "sch_word") ?>" title="back"><span class="button blue">Back</span></a>
                </div>

            </div>

        </div>

    </div>

</form>
<script src="//widget.cloudinary.com/global/all.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    
    $(".datepicker-add-options").datepicker({
        
    });

});

  // document.getElementById("upload_widget_opener").addEventListener("click", function() {
  //   /** CHECK UPLOAD SERVER ***/
  //   <?php  if ($_SERVER['SERVER_NAME'] == 'motorbb.dev' || $_SERVER['SERVER_NAME'] == 'new.motorbb.com') { ?>
  //       var cloud_name   = 'dy79r0ulf';
  //       var upload_preset= 'xvmizqan';
  //   <?php }else{ ?>
  //       var cloud_name   = 'softbloom';
  //       var upload_preset= 'juefxlp7';
  //   <?php } ?>
  //   /** END CHECK UPLOAD SERVER ***/
  //   var cloud_domain = 'https://res.cloudinary.com/';
  //   var cloud_folder = 'image/upload/'
  //   cloudinary.openUploadWidget({ cloud_name: cloud_name, upload_preset: upload_preset, show_powered_by: false, max_image_width: 115, max_image_height: 115}, 
  //     function(error, result) { 
  //       for (i = 0; i < result.length; i++) {
  //           var public_id           = result[i].public_id; 
  //           var resource_type       = result[i].resource_type;
  //           var secure_url          = result[i].secure_url;
  //           var original_filename   = result[i].original_filename;
  //           var base_url            =  cloud_domain+cloud_name+'/'+cloud_folder;
  //           var file_owner          = $("#member_no").val();

  //           var data = {
  //               base_url          :  base_url,
  //               file_owner        : file_owner,
  //               public_id         : public_id,
  //               resource_type     : resource_type,
  //               secure_url        : secure_url,
  //               original_filename : original_filename,
  //           }
  //          // console.log(result);
  //           $.ajax({
  //             type: "POST",
  //             url: "/?c=admin&m=adm_member_photo_insert",
  //             data: data,
  //             async: true,
  //             success: function(data){
  //                   var iframe = document.getElementById('upload_iframe');
  //                   iframe.src = iframe.src;  
  //              }
  //           });

  //       }
        
  //     });
  // }, false);
</script>