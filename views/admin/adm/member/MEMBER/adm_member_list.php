<script type="text/javascript">

    //체크박스 전체 선택

    function checkbox_all_check(obj, target)

    {
        if (typeof obj == 'undefined' || typeof target == 'undefined')
        {
        }
        else if (typeof target.length == 'undefined')
        {
            target.checked = obj.checked;
        }
        else
        {
            for (i = 0; i < target.length; i++)
            {
                target[i].checked = obj.checked;
            }
        }
    }


    


    function modify_check() {
        var f = document.adm_frm;
        check_count = 0;

        var member_num = '';

        if (f.member_no != null) {

            for (i = 0; i < f.member_no.length; i++) {

                if (f.member_no[i].checked) {
                    member_num = f.member_no[i].value;
                    check_count++;
                }
            }
            if (check_count == 0) {
                alert('Please select a staff to modify info.');
                return;
            } else if (check_count > 1) {
                alert('Please select only one staff to modify info.');
                return;

            } else {
                f.action = "?c=admin&m=adm_member_detail&member_no=" + member_num + "&cur_page=<?= $cur_page . func_sPUrlEncode($sParam, 'sch_word') ?>&sParam=<?= urlencode($sParam) ?>";
                f.submit();
            }
        }
    }

    function delete_all() {
        var f  = document.adm_frm;
        var fg = false;

        if(f.member_no != null) {
            for(i=0; i<f.member_no.length; i++) {
                if(f.member_no[i].checked) {
                    fg = true;
                }
            }

            if(!fg) {
                alert('Please select staff that you want to delete.');
                return;
            }

            if (!confirm('Do you want to delete this staff?'))

            {
                return;
                
            }else{

                f.action = '/?c=admin&m=adm_member_all_delete&mcd=<?= $mcd ?>';
                f.submit();
            }
            
        }
    }


</script>

<?php
   if(isset($_GET['sch_create_dt_s'])) $sch_create_dt_s=htmlspecialchars($_GET['sch_create_dt_s']);else $sch_create_dt_s="";
   if(isset($_GET['sch_create_dt_e'])) $sch_create_dt_e=  htmlspecialchars($_GET['sch_create_dt_e']);else $sch_create_dt_e="";
   if(isset($_GET['grade_no'])) $grade_no=  htmlspecialchars($_GET['grade_no']); else $grade_no="";
   if(isset($_GET['sch_condition'])) $sch_condition=  htmlspecialchars ($_GET['sch_condition']);else $sch_condition="";
   if(isset($_GET['sch_word'])) $sch_word=  htmlspecialchars($_GET['sch_word']);else $sch_word="";
   
?>



    <div class="contents_box_middle">
        <h1 class="title">Staff Management</h1>

        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You can manage your membership information, such as query, modify, delete, change membership level.</li>

            </ul>

        </div>

        <div class="table_list_box">

            <h1 class="table_title">Member Info</h1>

            <div class="search_box">

                <div class="search_result">

                    <img src="/images/admin/search_icon.gif" alt="Icon" align="absmiddle" />Total : <span class="title"><?= $total_rows ?></span> , Page : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>

                </div>
                
                
                
                <div class="btn_area_right">
                    <?php $register_link = "/?c=admin&m=adm_member_write&cur_page=$cur_page&mcd=$mcd" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam); ?>
                    <a href="<?php echo $register_link; ?>" title="등록"><span class="button blue">Register</span></a>
                    <a href="#" onclick="modify_check();" title="modify"><span class="button blue">Modify</span></a>
                    <a href="#" onclick="delete_all();" title="delete"><span class="button red">Delete</span></a>
                </div>
                
                
			
                <div class="search_condition">
                    <form   name="adm_frms" id="adm_frms" method="get" action="/?c=admin&amp;m=adm_member_list&amp;mcd=<?=$mcd?>">
                     <input type="hidden" name="c" value="<?= $c ?>" />
             <input type="hidden" name="m" value="<?= $m ?>" />
                    - RegDate

                    <input type="text" name="sch_create_dt_s" data-date-format="yyyy-mm-dd"  value="<?php echo $sch_create_dt_s; ?>" class="input_1 datepicker-add-options text_admin" data-auto-close="true" style="width:65px;" />

                    &nbsp;~

                                    <input type="text" data-date-format="yyyy-mm-dd" name="sch_create_dt_e"  value="<?php echo $sch_create_dt_e; ?>" class="input_1 datepicker-add-options text_admin" data-auto-close="true" style="width:65px;" />

              

                    

                       
                        <?php if ($mcd=="customer"||$session_grade_no=='3') { ?>

                        <?php }else{ ?>
                            <select name="grade_no" id="grade_no" class="input_2" align="absmiddle">
                                <option value="">
                                    ::Member Rating::
                                </option>
                                <option  <?php 
                           echo $grade_no=(isset($_GET['grade_no']) && $_GET['grade_no']=="1"? 'selected="selected"':''  );
                                 ?> value="1">Admin</option>
                                <option <?php 
                           echo $grade_no=(isset($_GET['grade_no']) && $_GET['grade_no']=="2"? 'selected="selected"':''  );
                                 ?> value="2">Manager</option>
                                <option
                                    <?php 
                           echo $grade_no=(isset($_GET['grade_no']) && $_GET['grade_no']=="3"? 'selected="selected"':''  );
                                 ?>
                                    value="3">Sales</option>
                               <!--  <?

                                    if(count($member_grade_list) > 0){
                                        foreach($member_grade_list as $grade_rows){
                                        ?>

                                            <option value="<?= $grade_rows->grade_no ?>"><?= $grade_rows->grade_name ?></option>

                                        <? 
                                        }       
                                    }
                                ?> -->

                            </select>  
                        <?php } ?>
                        

                                       

                    <select name="sch_condition" id="sch_condition" class="input_2" style="width:100px;" align="absmiddle">

                        <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="member_id like"? 'selected="selected"':''  );
                                 ?>
                            value="member_id like">Username</option>

                        <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="member_first_name like | member_last_name like "? 'selected="selected"':''  );
                                 ?>
                            value="member_first_name like | member_last_name like">Member Name</option>

                        <option <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="email like"? 'selected="selected"':''  );
                                 ?>
                            value="email like">Email</option>

                        <option <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="phone_no1 like|phone_no2 like|phone_no3 like"? 'selected="selected"':''  );
                                 ?>
                            value="phone_no1 like|phone_no2 like|phone_no3 like">Phone Number</option>

                        <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="mobile_no1 like|mobile_no2 like|mobile_no3 like"? 'selected="selected"':''  );
                                 ?>
                            value="mobile_no1 like|mobile_no2 like|mobile_no3 like">Phone number</option>

                        <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="address like|address_de like"? 'selected="selected"':''  );
                                 ?>
                            value="address like|address_de like">Address</option>                                

                    </select>

                                    <input type="text" name="sch_word" id="sch_word" value="<?php echo $sch_word; ?>" class="input_1 text_admin" style="width:100px;" title="Searches" /> 

                    <input type="image" src="/images/admin/btn_search_01.gif" class="search_admin" title="Search" align="absmiddle" />
                     </form>
                </div>
                
                

            </div>
            
                    
                    
                    
<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_member_list&amp;mcd=<?=$mcd?>" onsubmit="return search_member(this);">

            <table class="table_list" summary="Profile" cellspacing="0">

                <caption class="hidden"></caption>

                <col width="4%" /><col width="2px" />

                <col width="6%" /><col width="2px" />

                <col width="13%" /><col width="2px" />

                <col width="13%" /><col width="2px" />

                <col width="" /><col width="2px" />

                <col width="" /><col width="2px" />

                <col width="" /><col width="2px" />

                <col width="" /><col width="2px" />

                <col width="" />

                <thead>

                    <tr>

                        <th scope="col"><input type="checkbox" name="member_no" id="member_no" value="" onclick="checkbox_all_check(this, document.adm_frm.member_no);"/></th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                        <th scope="col">No.</th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>                                

                        <th scope="col">ID</th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                        <th scope="col">Name</th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                        <th scope="col">Grade</th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>                                

                        <th scope="col">E-mail</th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                        <th scope="col">RegDate</th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                        <th scope="col">Last Login</th>

                        <!-- <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                        <th scope="col">Certi</th> -->

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>                                

                        <th scope="col">Visit_cnt</th>                               

                    </tr>

                </thead>

                <tbody>

                    <?

                    //일반글

                    if(count($member_list) > 0)

                    {

                    $link = ''; //링크



                    foreach($member_list as $rows)

                    {

                    //링크

                    $link = "/?c=admin&m=adm_member_view&mcd=$mcd&member_no=$rows->member_no&cur_page=$cur_page".func_sPUrlEncode($sParam, "sch_word")."&sParam=".urlencode($sParam);

                    ?>                            

                    <tr>

                        <td><input type="checkbox" name="member_no[]" id="member_no" value="<?= $rows->member_no ?>" /></td>

                        <td class="line">&nbsp;</td>                               

                        <td><a href="<?= $link ?>" title="회원상세" style="display: block;"><?= $row_cnt++ ?></a></td>

                        <td class="line">&nbsp;</td>

                        <td><a href="<?= $link ?>" title="회원상세"><strong style="display: block;"><?= $rows->member_id ?></strong></a></td>

                        <td class="line">&nbsp;</td>

                        <td><a href="<?= $link ?>" title="회원상세" style="display: block;"><?= $rows->member_first_name ?></a></td>

                        <td class="line">&nbsp;</td>

                        <td><a href="<?= $link ?>" title="회원상세" style="display: block;"><?= $rows->grade_name ?></a></td>

                        <td class="line">&nbsp;</td>

                        <td><a href="<?= $link ?>" title="회원상세" style="display: block;"><?= $rows->email ?></a></td>

                        <td class="line">&nbsp;</td>   

                        <td><a href="<?= $link ?>" title="회원상세" style="display: block;"><?= $rows->join_dt ?></a></td>

                        <td class="line">&nbsp;</td>

                        <td><a href="<?= $link ?>" title="회원상세" style="display: block;"><?= $rows->last_login_dt ?></a></td>

                        <!-- <td class="line">&nbsp;</td>

                        <td><?= func_decode($rows->certi_yn, 'Y', '<font color="blue">Yes</font>', '<font color="red">No</font>') ?></td> -->

                        <td class="line">&nbsp;</td>                                

                        <td><a href="<?= $link ?>" title="회원상세" style="display: block;"><?= $rows->login_cnt ?></a></td>

                    </tr>

                    <?            

                    }

                    }

                    else

                    {

                    ?>



                    <tr>

                        <td colspan="20" class="no_data">Data does not exist.</td>

                    </tr>

                    <?  

                    }

                    ?>       



                </tbody>

            </table>

            <div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_member_list') ?></div> 



        </div>

                

    </div>

</div>

</form>   
            
            
<script type="text/javascript">
$(document).ready(function() {
    
    $(".datepicker-add-options").datepicker({
        
    });

});
 </script>