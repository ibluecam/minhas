<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Exchange Rate List</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script type="text/javascript" src="/js/common/common.js"></script>
<link rel="stylesheet" type="text/css" href="/css/admin/button.css" />
<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />

<!-- link calendar resources -->
<link rel="stylesheet" type="text/css" href="/css/tcal.css" />
<script type="text/javascript" src="/js/tcal.js"></script> 

<script type="text/javascript">
    //refresh page


    window.onunload = function(){
      window.opener.location.reload();
    };
	function exit_win(){
		window.close();
	}
	
	function insert_adm_bbs(frm) {
        if (!validate(frm.elements['values[country_to]'], 'INPUT [Country]'))
            return false;
        if (!validate(frm.elements['values[port_name]'], 'INPUT [PortName]'))
            return false;
        if (!validate(frm.elements['values[shipping_cost]'], 'INPUT [ShippingCost]'))
            return false;
        if (!validate(frm.elements['values[insurance]'], 'INPUT [Insurance]'))
            return false;
        if (!validate(frm.elements['values[inspection]'], 'INPUT [Inspection]'))
            return false;
        if (!validate(frm.elements['values[country_from]'], 'INPUT [FROM]'))
            return false;

    }
	
	 //게시판 체크박스 전체 선택
    function checkbox_all_check(obj, target) {
        if (typeof obj == 'undefined' || typeof target == 'undefined') {

        }
        else if (typeof target.length == 'undefined') {
            target.checked = obj.checked;
        }
        else
        {
            for (i = 0; i < target.length; i++) {
                target[i].checked = obj.checked;
            }
        }
		
    }
	
	function modify_check() {
        var f = document.adm_frm_rate;
        check_count = 0;
        if (f.id != null) {

            for (i = 0; i < f.id.length; i++) {

                if (f.id[i].checked) {
                    check_count++;
                }
            }
            if (check_count == 0) {
                alert('Please select Shipping Charge to Modify');
                return;
            } else if (check_count > 1) {
				
				window.location.reload();
                alert('Please select only one Charge to modify.');
				
                return false;

            } else {
                f.action = '?c=admin&m=adm_shipping_charge_modify&mcd=<?php echo $mcd; ?>&id=' + f.id;
                f.submit();
            }
        }
    }

	//------------delete
	
	function delete_shipping() {
        var f = document.adm_frm_rate;
        check_count = 0;
        if (f.id != null) {

            for (i = 0; i < f.id.length; i++) {

                if (f.id[i].checked) {
                    check_count++;
                }
            }
            if (check_count == 0) {
                alert('Please select Shipping Charge to delete');
                return;
            } else {
                f.action = '?c=admin&m=adm_shipping_charge_delete_exec&mcd=<?php echo $mcd; ?>&id=' + f.id;
                f.submit();
            }
        }
    }
   
</script>
</head>

<body >
<div class="warp_rate" style="width:750px;">
<form name="adm_frm_rate" id="adm_frm_rate" method="post" enctype="multipart/form-data" action="" onsubmit="">
<table width="720px" cellpadding="0" cellspacing="0">
	<colgroup>
		<col width="3%"/>
        <col width="12%"/>
        <col width="15%"/>
        <col width="15%"/><col width="32%"/>
        <col width="25%"/>

    </colgroup>

	<tr>
        <th colspan="9" class="box_title" align="left">
            	Shipping Charge List
        </th>
        
    </tr>
    <tr>
    	<th><input type="checkbox" name="id" id="id" value="" onclick="checkbox_all_check(this, document.adm_frm_rate.id);"/></th>
        <th>ID</th>
        <th>Country</th>
        <th>Port Name</th>
        <th>Shipping Cost</th>
        <th>Insurance</th>
        <th>Inspection</th>
        <th>Currency</th>
        <th>From</th>
    </tr>
    

<?
		$i=count($shipping_charge_list);
	//if(count($account_rate_list)>0){
		foreach($shipping_charge_list as $row){
	?>
	<tr class="line" valign="middle">
    	<th ><input type="checkbox" name="id[]" id="id" value="<?= $row->id ?>" /></th>
        <th><?=$i?></th>
        <th><? echo getCountryName($row->country_to); ?></th>
        <th><?=$row->port_name?></th>
        <th><?=$row->shipping_cost?></th>
        <th><?=$row->insurance?></th>
        <th><?=$row->inspection?></th>
        <th><?=$row->currency_type?></th>
        <th><? echo getCountryName($row->country_from); ?></th>
    </tr>		
	<?	
		$i--;	
		}
	//}

?>
    <tr>
        <td colspan="6">
            <ul>
                <li><a href="#" onClick="exit_win()"><span class="button blue">Exit</span></a>
                <li><a href="#" onClick="delete_shipping()"><span class="button blue">Delete</span></a>
                <li><a href="#" onClick="modify_check()"><span class="button blue">Modify</span></a>
                <li><a href="#add"><span class="button blue">Add</span></a></li>
            </ul>	
        </td>
    </tr>
</table>
</form>
<a href="#x" class="overlay" id="add"></a>
<div class="popup">
	<form name="adm_frm" id="adm_frmb" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_shipping_charge_exec&amp;mcd=<?= $mcd ?>" onsubmit="return insert_adm_bbs(this);">

    <table width="220px" cellpadding="0" cellspacing="0" class="tbl_addrate">
        <colgroup>
            <col width="50%"/>
            <col width="50%"/>
        </colgroup>
    
        <tr>
            <th colspan="2" class="box_title" align="left">
            	Add New Port
            </th>
        </tr>
        <tr>
            <td>Country</td>
            <td>
                <select name="values[country_to]" class="country" style="width:150px;margin-top:5px;margin-right:30px;">
                    <option value='NoValue' selected="selected">---Select Country---</option>
                    <?php
                    $sql=mysql_query("select id,cc,country_name from iw_country_list; ");
                    while($row=mysql_fetch_array($sql))
                    {
                    $country_code=$row['cc'];
                    $country_name=$row['country_name'];
                    echo '<option value="'.$country_code.'">'.$country_name.'</option>';
                    } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>PortName</td>
            <td>
                <input type="text" id="port_name" name="values[port_name]" placeholder="Port Name" autocomplete="off">
            </td>
        </tr>
        <tr>
            <td>ShippingCost</td>
            <td>
                <input type="text" id="shipping_cost" name="values[shipping_cost]" placeholder="ex) 300" autocomplete="off">
            </td>
        </tr>
        <tr>
            <td>Insurance</td>
            <td>
                <input type="text" id="port_name" name="values[insurance]" placeholder="ex) 50" autocomplete="off">
            </td>
        </tr>
        <tr>
            <td>Inspection</td>
            <td>
                <input type="text" id="port_name" name="values[inspection]" placeholder="ex) 150" autocomplete="off">
            </td>
        </tr>
        <tr>
            <td>Currency</td>
            <td>
            	<select name="values[currency_type]" id="" class="input_2" onchange="currency_selector();">
							
                    <option value="USD" >USD</option>
                    <option value="KRW" >KRW</option>
                    <option value="JPY" >JPY</option>
                    <option value="EUR" >EUR</option>
  
                </select>
            </td>
        </tr>
        <tr>
            <td>FROM</td>
            <td>
                <select name="values[country_from]" class="country" style="width:150px;margin-top:5px;margin-right:30px;">
                    <option value='NoValue' selected="selected">---Select Country---</option>
                    <?php
                    //mozambique, tanzania, zambia
                    $sql=mysql_query("select id,cc,country_name from iw_country_list; ");
                    while($row=mysql_fetch_array($sql))
                    {
                    $country_code=$row['cc'];
                    $country_name=$row['country_name'];
                    echo '<option value="'.$country_code.'">'.$country_name.'</option>';
                    } ?>
                </select>
            </td>
        </tr>
        
        <tr><td colspan="2"><input type="image" src="/images/admin/submit.gif" title="Check" /></td></tr>

    </table>
    <a class="close" href="#close" ></a>
    </form>
</div>

</div>
</body>
</html>