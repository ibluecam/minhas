<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Exchange Rate List</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script type="text/javascript" src="/js/common/common.js"></script>
<link rel="stylesheet" type="text/css" href="/css/admin/button.css" />
<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />

<!-- link calendar resources -->
<link rel="stylesheet" type="text/css" href="/css/tcal.css" />
<script type="text/javascript" src="/js/tcal.js"></script> 

<script type="text/javascript">
    //refresh page


    window.onunload = function(){
     window.opener.location.reload();
    };
	function exit_win(){
		window.close();
	}
	
	function insert_adm_bbs(frm) {
        if (!validate(frm.elements['values[country_to]'], 'INPUT [Country]'))
            return false;
        if (!validate(frm.elements['values[port_name]'], 'INPUT [PortName]'))
            return false;
        if (!validate(frm.elements['values[shipping_cost]'], 'INPUT [ShippingCost]'))
            return false;
        if (!validate(frm.elements['values[insurance]'], 'INPUT [Insurance]'))
            return false;
        if (!validate(frm.elements['values[inspection]'], 'INPUT [Inspection]'))
            return false;
        if (!validate(frm.elements['values[country_from]'], 'INPUT [FROM]'))
            return false;

    }
	
	 //게시판 체크박스 전체 선택
    function checkbox_all_check(obj, target) {
        if (typeof obj == 'undefined' || typeof target == 'undefined') {

        }
        else if (typeof target.length == 'undefined') {
            target.checked = obj.checked;
        }
        else
        {
            for (i = 0; i < target.length; i++) {
                target[i].checked = obj.checked;
            }
        }
		
    }
	
	function modify_check() {
        var f = document.adm_frm_rate;
        check_count = 0;
        if (f.id != null) {

            for (i = 0; i < f.id.length; i++) {

                if (f.id[i].checked) {
                    check_count++;
                }
            }
            if (check_count == 0) {
                alert('Please select Exchange rate to Modify');
                return;
            } else if (check_count > 1) {
				
				window.location.reload();
                alert('Please select only one Rate to modify info.');
				
                return false;

            } else {
                f.action = '?c=admin&m=adm_account_rate_modify&mcd=<?php echo $mcd; ?>&id=' + f.id;
                f.submit();
            }
        }
    }
	
	

   
</script>
</head>

<body>

<div class="warp_rate rat_modify">
<?
if(count($shipping_charge_list)>0){
	foreach ($shipping_charge_list as $row){

?>
	<form name="adm_frm" id="adm_frmb" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_shipping_charge_modify_exec&amp;mcd=<?= $mcd ?>&id=<?= $row->id ?>" onsubmit="return insert_adm_bbs(this);">
	<input type="hidden" name="id" id="id" value="<?= $row->id ?>"/>
    <table width="220px" cellpadding="0" cellspacing="0" class="tbl_addrate"  border="0">
        <colgroup>
            <col width="50%"/>
            <col width="50%"/>
        </colgroup>
    
        <tr>
            <th colspan="2" class="box_title" align="left">
            	Current Shipping Charge
            </th>
        </tr>
        <tr>
            <td>Country</td>
            <td>
                <select name="values[country_to]" class="country" style="width:150px;margin-top:5px;margin-right:30px;">
                    <option value='NoValue'>---Select Country---</option>
                    <?php
                    
                    $sql=mysql_query("select id,cc,country_name from iw_country_list; ");
                    while($rows=mysql_fetch_array($sql))
                    {
                    $country_code=$rows['cc'];
                    $country_name=$rows['country_name'];
                    echo '<option value="'.$country_code.'"';
                    if($row->country_to==$country_code){
                        echo 'selected="selected" ';
                    }
                    echo '>'.$country_name.'</option>';
                    } ?>
                </select>
            </td>
        </tr>
         <tr>
            <td>PortName</td>
            <td>
                <input type="text" id="port_name" name="values[port_name]" value="<?=$row->port_name?>" autocomplete="off">
            </td>
        </tr>
        <tr>
            <td>ShippingCost</td>
            <td>
                <input type="text" id="shipping_cost" name="values[shipping_cost]" value="<?=$row->shipping_cost?>" autocomplete="off">
            </td>
        </tr>
        <tr>
            <td>Insurance</td>
            <td>
                <input type="text" id="port_name" name="values[insurance]" value="<?=$row->insurance?>" autocomplete="off">
            </td>
        </tr>
        <tr>
            <td>Inspection</td>
            <td>
                <input type="text" id="port_name" name="values[inspection]" value="<?=$row->inspection?>" autocomplete="off">
            </td>
        </tr>
        <tr>
            <td>Currency</td>
            <td>
                <select name="values[currency_type]" id="" class="input_2" onchange="currency_selector();">
                            
                    <option value="USD" <?php if ( $row->currency_type == 'USD' ) echo 'selected="selected"'; ?> >USD</option>
                    <option value="KRW" <?php if ( $row->currency_type == 'KRW' ) echo 'selected="selected"'; ?> >KRW</option>
                    <option value="JPY" <?php if ( $row->currency_type == 'JPY' ) echo 'selected="selected"'; ?> >JPY</option>
                    <option value="EUR" <?php if ( $row->currency_type == 'EUR' ) echo 'selected="selected"'; ?> >EUR</option>
  
                </select>
            </td>
        </tr>
        <tr>
            <td>FROM</td>
            <td>
                <select name="values[country_from]" class="country" style="width:150px;margin-top:5px;margin-right:30px;">
                    <option value='NoValue'>---Select FROM---</option>
                    <?php
                    
                    $sql=mysql_query("select id,cc,country_name from iw_country_list; ");
                    while($rows=mysql_fetch_array($sql))
                    {
                    $country_code=$rows['cc'];
                    $country_name=$rows['country_name'];
                    echo '<option value="'.$country_code.'"';
                    if($row->country_from==$country_code){
                        echo 'selected="selected" ';
                    }
                    echo '>'.$country_name.'</option>';
                    } ?>
                </select>
            </td>
        </tr>
        
        <tr><td colspan="2"><input type="image" src="/images/admin/submit.gif" title="Check" /></td></tr>

    </table>
    </form>
<?
	}
}
?>
    
</div>

</body>
</html>