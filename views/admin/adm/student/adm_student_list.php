<script type="text/javascript">

	//게시판 체크박스 전체 선택
	function checkbox_all_check(obj, target) {
		if ( typeof obj == 'undefined' || typeof target == 'undefined') {

		}
		else if ( typeof target.length == 'undefined' ) {
			target.checked = obj.checked;
		}
		else
		{
			for (i = 0 ; i < target.length ; i++) {
				target[i].checked = obj.checked;
			}
		}
	}

	//deposit input
	function deposit_select() {
		var f  = document.adm_frm;
		var fg = false;

		check_count = 0;
		//var idxs = new Array();
		var customer_num = '';


		if(f.idx != null) {
			
			for(i=0; i<f.idx.length; i++) {

				if(f.idx[i].checked) {
					fg = true;

					//idxs[i] = " \' " + f.idx[i].value+ " \' ";
					customer_num = f.idx[i].value;


					check_count++;
				} /*else { idxs[i] = " \' \' "; }*/
			}

			//alert(customer_num); return;

			if(check_count == 1) {
				window.open('/?c=admin&m=adm_customer_deposit_popup_update&mcd=<?=$mcd?>&idx='+customer_num,'deposit','width=600,height=550,top=100,left=100'); 
				
				return;
			}

			if(check_count != 1) {
				alert('Please select one customer to deposit.');
				return;
			}

/*			alert(idxs[0]);
			alert(idxs);
			alert(f.idx);
				return;*/


			f.action = '/?c=admin&m=adm_customer_deposit_update&mcd=<?= $mcd ?>&idx='+f.idx;
			f.submit();
		}
	}


	//전체 삭제
	function delete_all() {
		var f  = document.adm_frm;
		var fg = false;

		if(f.idx != null) {
			for(i=0; i<f.idx.length; i++) {
				if(f.idx[i].checked) {
					fg = true;
				}
			}

			if(!fg) {
				alert('Please select customers that you want to delete.');
				return;
			}

			f.action = '/?c=admin&m=adm_student_all_delete&mcd=<?= $mcd ?>';
			f.submit();
		}
	}

			//function modeify
	
	function modify_check(){
		var f  = document.adm_frm;
		check_count = 0;

		var customer_num = '';

		if(f.idx != null) {
			
			for(i=0; i<f.idx.length; i++) {
		
				if(f.idx[i].checked) {
					customer_num = f.idx[i].value;
					check_count++;
				} 
			}
			if(check_count == 0) {
				alert('Please select car to modify info.');
				return ;
			}else if(check_count >1){
				alert('Please select only one car to modify info.');
				return ;
				
			}else{
				f.action = '?c=admin&m=adm_student_modify&idx='+ customer_num;
				f.submit();	
			}
		}
	}

</script>
<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_student_list" onsubmit="">
   <div class="contents_box_middle">
  		<div class="table_list_box">

			<h1 class="table_title">팝업관리</h1>

			<div class="search_box">

				<div class="search_result">

					<img src="../../../images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Student : <span class="title"><?= $total_rows ?></span> Page : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>

				</div>

				<div class="search_condition">

					<select name="sch_condition" id="sch_condition" class="input_2" style="width:120px;"
                            align="absmiddle">

						<option value="studentname like">Student Name</option>
                        <option value="gender like">Gender</option>
                        <option value="dateofbirth like">DOB</option>
                        <option value="placeofbirth like">POB</option>
                        <option value="address like">Address</option>
                        <option value="address like | placeofbirth like | dateofbirth like |
                                       gender like | studentname like | id like | phone like">All</option>

					</select>

					<input type="text" name="sch_word" id="sch_word" value="" class="input_1" style="width:100px;" title="검색어" />

					<input type="image" src="../../../images/admin/btn_search_01.gif" title="검색" align="absmiddle" />

				</div>

			</div>

			<table class="table_list" summary="팝업관리" cellspacing="0">

				<caption class="hidden"></caption>
                
                <col width="5%" /><col width="2px" />
                 
                <col width="5%" /><col width="2px" />

				<col width="1៥%" /><col width="2px" />

				<col width="10%" /><col width="2px" />
                
                <col width="10%" /><col width="2px" />

				<col width="20%" /><col width="2px" />

				<col width="20%" /><col width="2px" />
                                
				<thead>

					<tr>
                    <th scope="col"><input type="checkbox" name="idx" id="idx" value=""
                                     onclick="checkbox_all_check(this, document.adm_frm.idx);"/></th>

                       <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th> 
                        
                        <th scope="col">ID</th>
                        
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th> 
                        
                        <th scope="col">StudentName</th>

						<th scope="col" class="line"><img src="../../../images/admin/board_bar_line.gif"></th>
                        
                        <th scope="col">Gender</th>

						<th scope="col" class="line"><img src="../../../images/admin/board_bar_line.gif"></th>

						<th scope="col">DateOfBirth</th>

						<th scope="col" class="line"><img src="../../../images/admin/board_bar_line.gif"></th>

						<th scope="col">PlaceOfBirth</th>
                        
                      	<th scope="col" class="line"><img src="../../../images/admin/board_bar_line.gif"></th>

						<th scope="col">Address</th>
                        
                        <th scope="col" class="line"><img src="../../../images/admin/board_bar_line.gif"></th>

						<th scope="col">Phone</th>
					</tr>

				</thead>

				<tbody>



					<?

					//일반글

					if (count($student_list) > 0) {

						foreach ($student_list as $rows) {

							$studentname       = $rows->studentname;
							$gender            = $rows->gender;
							$dob               = $rows->dateofbirth;
							$pob               = $rows->placeofbirth;
							$address           = $rows->address;
							$phone             = $rows->phone;
							
							if ($gender == 0)
							{
								$gender = "Male";
							}
							else
							{
								$gender = "Female";
							}

							?>

							<tr>
                                
                                <td><input type="checkbox" name="idx[]" id="idx" value="<?= $rows->id ?>" /></td>
                                 
                               <td class="line">&nbsp;</td>
                               
                                <td><?= $rows->id ?></td>
                                
                                <td class="line">&nbsp;</td>
                               
								<td style="text-align:center;"><?= $studentname ?></td>
                                
                                <td class="line">&nbsp;</td>

								<td style="text-align:center;"><?= $gender ?></td>
                                
                                <td class="line">&nbsp;</td>

								<td style="text-align:center;"><?= $dob    ?></td>

								
                                <td class="line">&nbsp;</td>
                                <td style="text-align:center"><?= $pob     ?></td>
                                
                                 <td class="line">&nbsp;</td>
                                <td style="text-align:center;"><?= $address    ?></td>
                                
                                 <td class="line">&nbsp;</td>
                                <td style="text-align:center;"><?= $phone     ?></td>
                                
                           	</tr>

						<? } ?>

					<? } ?>

<? if (count($student_list) == 0) {   ?>

						<tr>

							<td colspan="20" class="no_data">no data.</td>

						</tr>

<? } ?>



				</tbody>

			</table>

			<div class="page_navi">
			<?=  func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_student_list') ?>
            </div>
            
                <div class="btn_area_center">
            	<!-- <div class="btn_area_left">
					<a href="/?c=admin&amp;m=adm_bbs_excel_exec&amp;mcd=<?= $mcd ?>&amp;last_query=<?= $query ?>"  title="export" class="alg-lef"><img src="/images/admin/export.gif" alt="export" /></a>

                </div> -->
                <div class="btn_area_right">
                  	<a href="#" onclick="deposit_select();" title="deposit" class="alg-rig"><img src="/images/admin/deposit_1.gif" alt="deposit" /></a>
                  	<a href="/?c=admin&amp;m=adm_add_student&amp;mcd=<?= $mcd ?>" title="등록" class="alg-rig"><img src="/images/admin/register_1.gif" alt="등록" /></a>
                    <a href="#" onclick="modify_check();" title="modify" class="alg-rig"><img src="/images/admin/modify_1.gif" alt="modify" /></a>
				</div>
			</div>
 
			<div class="search_box" style="padding:15px 10px;background-color:#ffffff">

				<img src="/images/admin/search_icon.gif" alt="Icon" align="absmiddle" />Selected customers

				<a href="#" onclick="delete_all();" title="delete"><img src="/images/admin/btn_delete_eng.gif" alt="delete" align="middle" /></a>

			</div>
            
            
		  </div>
       </div>
	  </div>
    </div> 
</form>