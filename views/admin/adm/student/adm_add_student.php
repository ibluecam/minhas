<script type="text/javascript">

	//게시판 등록

	function insert_adm_bbs(frm) {

		if(!validate(frm.elements['subject'], '제목을 입력하세요.')) return false;

		if(!emailCheck(frm.elements['email'])) return false; //이메일 체크

<? //답변기능, 비밀글기능 여부

if (func_get_config($bbs_config_result, 'reply_yn') == 'Y' || func_get_config($bbs_config_result, 'secret_yn') == 'Y') {

	?>

			//일반글이면.. 비밀번호 입력

			if(frm.elements['notice_yn'][0].checked || frm.elements['secret_yn'][1].checked) {

				frm.elements['password'].disabled = false;

				if(!validate(frm.elements['password'], '비밀번호를 입력하세요.')) return false;

			}

			else

			{

				frm.elements['password'].disabled = true;

			}

<? } ?>

<? if (func_get_config($bbs_config_result, 'editor_yn') == 'Y') { //게시판 HTML 에디터를 사용할 경우..  ?>

	if(tinyMCE.get('contents').getContent() == '' || tinyMCE.get('contents').getContent() == '<p>&nbsp;</p>') {

		alert("내용을 입력하세요.");

		return false;

	}

<? } else { ?>

			if(!validate(frm.elements['contents'], '내용을 입력하세요.')) return false;

<? } ?>

		return true;

	}



</script>


<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_student_write_exec&amp;" onsubmit="return insert_adm_bbs(this);">

	<div class="contents_box_middle">

		<h1 class="title">Student Registration</h1>

		<div class="tip_box">

			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

			<ul>

				<li> - Register Student Info.</li>

			</ul>

		</div>

		<div class="table_write_box">

			<h1 class="table_title">Student Registration</h1>

			<table class="table_write" summary=" " cellspacing="0">

				<caption class="hidden"></caption>

				<col width="18%" /><col width="32%" /><col width="18%" /><col width="32%" />


				<tr>

					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />StudentName</th>

					<td class="value_02">

						<input type="text" name="values[studentname]" id="writer" value="" class="input_1" style="width:200px;" maxlength="50" />

					</td>

					<th class="key_02">Gender</th>

					<td class="value_02">
                    
                        <select name="values[gender]" style="width:205px;">
                        	<option value="0">Male</option>
                            <option value="1">Female</option>
                        </select>
					</td>
                    
               </tr>
               <tr>
                   <th class="key_02">DOB</th>

					<td class="value_02">

						<input type="text" name="values[dateofbirth]" id="writer" value="" class="input_1" style="width:200px;" maxlength="50" />

					</td>

					<th class="key_02">POB</th>

					<td class="value_02">
                    <input type="text" name="values[placeofbirth]" id="writer" value="" class="input_1" style="width:200px;" maxlength="50" />
					</td>
               </tr>
               
               <tr>
                   <th class="key_02">Address</th>

					<td class="value_02">

						<input type="text" name="values[address]" id="writer" value="" class="input_1" style="width:200px;" maxlength="50" />

					</td>

					<th class="key_02">Phone</th>

					<td class="value_02">
                    <input type="text" name="values[phone]" id="writer" value="" class="input_1" style="width:200px;" maxlength="50" />
					</td>
               </tr>

			</table>

			<div class="btn_area_center">

				<input type="image" src="/images/admin/ok.gif" title="save" />

				<a href="/?c=admin&amp;m=adm_student_list&amp;" title="back"><img src="/images/admin/back.gif" alt="목록" /></a>

			</div>

		</div>

	</div>

</form>