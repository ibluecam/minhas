<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=view_statistics_day" onsubmit="">
	<div class="contents_box_middle">
		<h1 class="title">접속통계관리</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 선택한 날짜의 시간대별 총 접속수를 그래프로 표시 합니다.</li>
				<li> - 날짜를 선택 하신후 검색 버튼을 클릭 하세요.</li>
			</ul>
		</div>
		<div class="table_list_box">
			<h1 class="table_title">일별 통계</h1>
			<div class="search_box">
				<div class="search_result">
					<span class="title">- <?= $statistics_title ?></span>
				</div>
				<div class="search_condition">
					<input type="text" name="sel_sdate" id="sel_sdate" value="<?= $sel_sdate ?>" class="input_1" style="width:65px;" readonly />
					<a href="#" onclick="Calendar(document.adm_frm.elements['sel_sdate']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>&nbsp;~
					<input type="text" name="sel_edate" id="sel_edate" value="<?= $sel_edate ?>" class="input_1" style="width:65px;" readonly />
					<a href="#" onclick="Calendar(document.adm_frm.elements['sel_edate']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
					<input type="image" src="/images/admin/btn_search_01.gif" title="검색" align="absmiddle" />
				</div>
			</div>
			<div class="contents_box">
				<? if (count($select_day_result) > 0) { ?>
					<div class="contents_img"><img src="<?= $graph ?>" /></div>
					<div class="contents_img"><img src="<?= $graph_pie ?>" /></div>
				<? } else { ?>
					<div class="contents_text">Data does not exist.</div>
				<? } ?>
			</div>
		</div>
	</div>
</form>