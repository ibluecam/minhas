<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=view_statistics_year" onsubmit="">
	<div class="contents_box_middle">
		<h1 class="title">접속통계관리</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 선택한 년의 월별 총 접속수를 그래프로 표시 합니다.</li>
				<li> - 년도를 선택 하신후 검색 버튼을 클릭 하세요.</li>
			</ul>
		</div>
		<div class="table_list_box">
			<h1 class="table_title">년간 통계</h1>
			<div class="search_box">
				<div class="search_result">
					<span class="title">- <?= $statistics_title ?></span>
				</div>
				<div class="search_condition">
					<select name="sel_year" id="sel_year" class="input_2" style="width:100px;">
						<option value="">::선택하세요.::</option>
						<?
						if (count($year_group) > 0) {
							foreach ($year_group as $rows) {
								$tmp_str = '';
								if ($rows->visit_year == $sel_year)
									$tmp_str = "selected";
								?>
								<option value="<?= $rows->visit_year ?>" <?= $tmp_str ?>><?= $rows->visit_year ?>년</option>
	<? } ?>
<? } ?>
					</select>
					<input type="image" src="/images/admin/btn_search_01.gif" title="검색" align="absmiddle" />
				</div>
			</div>
			<div class="contents_box">
				<? if (count($select_year_result) > 0) { ?>
					<div class="contents_img"><img src="<?= $graph ?>" /></div>
					<div class="contents_img"><img src="<?= $graph_pie ?>" /></div>
				<? } else { ?>
					<div class="contents_text">Data does not exist.</div>
<? } ?>
			</div>
		</div>
	</div>
</form>