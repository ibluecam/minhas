<script type="text/javascript">

	function checkbox_all_check(obj, target) {
		if ( typeof obj == 'undefined' || typeof target == 'undefined') {

		}
		else if ( typeof target.length == 'undefined' ) {
			target.checked = obj.checked;
		}
		else
		{
			for (i = 0 ; i < target.length ; i++) {
				target[i].checked = obj.checked;
			}
		}
	}
	

	$(document).ready(function(e){

		 $("#btnDelete").click(btnDelete_click);

         $('#invoice_no').click(function(event) {
		  if(this.checked) {
		      $(':checkbox').each(function() {
		          this.checked = true;
		          $('#btnDelete').removeClass('disabled');
		      });
		  }
		  else {
		    $(':checkbox').each(function() {
		          this.checked = false;
		          $('#btnDelete').addClass('disabled'); 
		      });
		  }
		});

        $('.invoice_no').click(function(event) {   
		    if(this.checked) {
		        this.checked = true;
		        $('#btnDelete').removeClass('disabled');   
		    }
		    else{
		    	 $('#btnDelete').addClass('disabled'); 
		    }
        });
        
        $("#btn_yes").click(function (){
            var abc ='';
            $(".invoice_no").each(function (){
                if ($(this).is(":checked") ===true)
                	

                    abc += $(this).attr("data-title") + ";";
            });
            var ajax = $.ajax({
                url: "/?c=admin&m=adm_delete_proforma_invoice",
                type: 'POST',
                data : {
                    
                    invoice_no : abc    
                }
            });
            ajax.done(function (msg){
                $("#myModal").modal("hide");
                window.location.href = "/?c=admin&m=adm_proforma_invoice_list";
            });
        });


          function btnDelete_click(){
            if($(this).hasClass("disabled") === false){
                if($("#del").html().indexOf("Delete") !== -1){
                    $("#msgBody").html("Are you sure want to delete the selected items?");
                    $("#myModalLabel").html("Confirmation");
                    $('#btnNO').html('NO');
                    $("#btn_yes").show();
                    $("#myModal").modal("show");
                }
              
                else{
                     window.location.href = "/?c=admin&m=adm_proforma_invoice_list";
                }
            }
        }
        
	});
</script>
<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />


<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_proforma_invoice_list">

	<div class="contents_box_middle">

		<h1 class="title">motorbb Car Proforma Invoice</h1>

		  <div class="tip_box">

			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

			<ul>

				<li> - You can register/search/modify proforma invoices.</li>

			</ul>

		</div>

		<div class="table_list_box" style="padding-top:10px;">

			<h1 class="table_title">Document</h1>

			<div class="search_box">

				<div class="search_result">

					<img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Total : <span class="title"><?= $total_rows ?></span> invoice, Pages : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>

				</div>

				<div class="search_condition">

					<?

					//카테고리 사용 여부
					if(isset($bbs_config_result)){

						if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {

						$category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));

						$category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));

					}


						?>

<? } ?>

					<select name="sch_condition" id="sch_condition" class="input_2" style="width:150px;" align="absmiddle">

						<option value="m.member_first_name like">Customer</option>
						
						<option value="p.id like">invoice_no</option>
						
					</select>

					<input type="text" name="sch_word" id="sch_word" value="" class="input_1 text_admin" style="width:100px;" title="검색어" />

					<input type="image" src="/images/admin/btn_search_01.gif" class="search_admin" title="검색" align="absmiddle" />

				</div>

			</div>
</form>
			
            
            
            <!-- <div style="overflow:auto;"> -->
            <div>

			<table class="table_list" summary=" " cellspacing="0"  style="width:100%;">

				<caption class="hidden"></caption>

				<col width="3%" /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col  /><col width="2px" />

				<col />



				<thead>

					<tr>

						<th scope="col"><input type="checkbox" name="invoice_no" id="invoice_no" value="" onclick="checkbox_all_check(this, document.adm_frm.invoice_no);"/></th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">No.</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Car Name</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Invoice No</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

						<th scope="col">Car ID</th>

						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        
                        <th scope="col">Customer</th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        
                        <th scope="col">Discount Price</th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        
                        <th scope="col">Created Date</th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        
                        <th scope="col">Invoice (CIF + Inspection)</th>

                        <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        
                        <th scope="col">Invoice (CIF)</th>
                        
					</tr>

				</thead>

				<tbody>

					<?php
					if (count($invoice_list) > 0) {
					
						$no_coun =1;

						foreach ($invoice_list as $rows) {

							?>

							<tr>
                            
								<td><input type="checkbox" name="invoice_no[]" data-title="<?= $rows->id ?>" id="invoice_no" class="invoice_no" value="<?= $rows->id ?>" /> </td>

								<td class="line">&nbsp;</td>

								<td><?= $no_coun++ ?></td>

								<td class="line">&nbsp;</td>

								<td width='250'><?= $rows->car_make.' '.$rows->car_model.' '.$rows->car_model_year ?></td>

								<td class="line">&nbsp;</td>

								<td><?= $rows->id ?></td>

								<td class="line">&nbsp;</td>

								<td><?= $rows->bbs_idx ?></td>

								<td class="line">&nbsp;</td>
								 
                                <td><?= $rows->member_first_name ?></td>

                                <td class="line">&nbsp;</td>
								 
                                <td width='150'><?= $rows->discount_price ?></td>

                                <td class="line">&nbsp;</td>
								 
                                <td><?= $rows->created_dt ?></td>

                                <td class="line">&nbsp;</td>
								 
                                <td>
	                                <div style='display:inline-block'>
	                                	<form action="/?c=admin&m=download_file_proforma_invoice" name="excel_invoice_<?=$no_coun?>" method="post">
	                                		<input type="hidden" name="invoice_no" value="<?=substr('00000' . $rows->id, -5)?>">
	                                		<input type="hidden" name="full_path_invoice" value="<?=$rows->full_path?>">
	                                		<input class="download_invoice" type="submit" name="download_invoice" value="" title="Invoice with CIF" />
	                                	</form>
	                                </div>
                                </td>
                                <td class="line">&nbsp;</td>
                                <td>
	                                <div style='display:inline-block'>
	                                	<form action="/?c=admin&m=download_file_proforma_invoice" name="excel_invoice_<?=$no_coun?>" method="post">
	                                		<input type="hidden" name="invoice_no" value="<?=substr('00000' . $rows->id, -5)?>">
	                                		<input type="hidden" name="full_path_invoice" value="<?=str_replace('proforma_invoice(','cc_proforma_invoice(',$rows->full_path)?>">
	                                		<input class="download_invoice" type="submit" name="download_invoice" value="" title="Invoice without CIF" />
	                                	</form>
	                                </div>
                                </td>
							</tr>

						<?php } ?>

					<?php } ?>

					<? 

					if (count($invoice_list) == 0) {

						?>

						<tr>

							<td colspan="20" class="no_data">Data does not exist.</td>

						</tr>

					<? } ?>



				</tbody>

			</table>

		</div>
            
			<div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_proforma_invoice_list&mcd=' . $mcd, $mcd) ?></div>


			<? $query = $this->db->last_query(); ?>

			<div class="btn_area_center">
                <div class="btn_area_right">
                    <a href="/?c=admin&/?c=admin&m=adm_bbs_list&mcd=product" title="register"><span class="button blue">Register</span></a>
                    <button type="button" id="btnDelete" class="button red disabled"><span></span> <b id='del'>Delete</b></button>
				</div>
				<div class="btn_area_left">
                   
				</div>
			</div>
 			
			<div class="search_box" style="padding:15px 10px;background-color:#ffffff">
				<img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />You can register/search/modify proforma invoices
			</div>

		</div>

	</div>

<!-- </form> -->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
      </div>
      <div class="modal-body" id="msgBody">
         <!--  Message Here -->
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default" id="btn_yes">YES</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" id='btnNO'>NO</button>
      </div>
    </div>
  </div>
</div>
</div>
