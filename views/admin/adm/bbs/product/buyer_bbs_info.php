
<style type="text/css">
.min_menu li{
    display: inline-block;
}
.button-small{
    color: #fff !important;
    word-spacing: 0.25em;
   font-family: 'Open Sans', sans-serif;
   text-transform: uppercase;
   border: none;
    /*line-height: 22px;
    padding: 12px 13px 11px;*/
    padding: 10px 13px 7px 13px;
    text-align: center;
    display: inline-block;
    margin-top: 5px;
    margin-bottom: 18px;
    text-decoration: none;
}
.button-small:visited{
      color: #fff;
}
#btnactive {
    text-decoration: none;
    background-color: #78BC97;
    -webkit-box-shadow: inset 0 -3px 0px #A3CFB6, inset 0 20px 0px #78BC97;
    -moz-box-shadow: inset 0 -3px 0px #A3CFB6, inset 0 20px 0px #78BC97;
    box-shadow: inset 0 -3px 0px #A3CFB6, inset 0 20px 0px #78BC97; 
    border-bottom: solid 1px #78BC97;
}
.rounded3 {
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
.grey_dark {
    background: #5C6165;
    width: 120px;
    font-weight: bolder;   
    -webkit-box-shadow: inset 0 -3px 0px #848181, inset 0 20px 0px #5C6165;
    -moz-box-shadow: inset 0 -3px 0px #848181, inset 0 20px 0px #5C6165;
    box-shadow: inset 0 -3px 0px #848181, inset 0 20px 0px #5C6165; 
    border-bottom: solid 1px #5C6165;
}
#notspace{
    padding: 0px;
    margin: 0px;
}

</style>
<?php
  if(count($bbs_info_buyer_detail)>0){
      foreach ($bbs_info_buyer_detail->result() as $rows){
 
?>
 
<form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="">
<input type="hidden" name="values[category]" value="">	
    <div class="contents_box_middle">
    	
        <h1 class="title">Car Info</h1>
       <?php
                if($_SESSION['ADMIN']['session_grade_no'] !='11' ){
              ?>
		<div class="tip_box">

			<p class="title"><img src="images/admin/tip_img.gif" alt="tip" /></p>

			<ul class="min_menu">

				<li></li>
                <li><span class="button-small grey_dark rounded3"  >Sale</span></li>
                <li id="notspace"><img style="margin-bottom: -6px" src="images/admin/symbol-next.png"></li>
                <li><span class="button-small grey_dark rounded3"  >Order</span></li>
                <li id="notspace"><img style="margin-bottom: -6px" src="images/admin/symbol-next.png"></li>
                <li><span class="button-small grey_dark rounded3" id="btnactive" href="#">Reserved</span></li>
                <li id="notspace"><img style="margin-bottom: -6px" src="images/admin/symbol-next.png"></li>
                <li><span class="button-small grey_dark rounded3"  href="#">Ship Ok</span></li>
                <li id="notspace"><img style="margin-bottom: -6px" src="images/admin/symbol-next.png"></li>
                <li><span class="button-small grey_dark rounded3"  href="#">Release Ok</span></li>
                <li id="notspace"><img style="margin-bottom: -6px" src="images/admin/symbol-next.png"></li>
                <li><span class="button-small grey_dark rounded3"  href="#">Sold Out</span></li>
			</ul>

		</div>
        <?php
                }
        ?>
        
                    <div class="customer_container">
<!--                <h1 class="table_title">Customer: </h1>-->
<!--                <div class="customer_name"><a href="/?c=admin&m=adm_customer_view&mcd=&member_no=259&cur_page=1&sParam=">buyer1 buyer1 (reserved)</a> </div>-->
            </div>
        		<div class="table_write_box">
        <h1 class="table_title">product</h1>
			<table class="table_write" summary="product" border="0" cellspacing="0" width="100%">
                <caption></caption>
                <colgroup>
                    <col width="15%" />
                    <col width="35%" />
                    <col width="15%" />
                    <col width="35%" />
                </colgroup>
                
                <tr><td colspan="4" class="text_info">STOCK NO. <?php echo $rows->car_stock_no;  ?></td></tr>
                    
                     <tr>
                         
                          <th scope="row">Owner</th>
                        <td class="text"><p>

                             <?php 
                              if(count($bbs_detail_list_get_owner)>0){
                                  foreach ($bbs_detail_list_get_owner as $row){
                                      
                                if($row->member_no == $rows->car_owner){
                                    
                               echo $row->company_name;
                                       
                                          }
                                  }
                              }
                            ?>
                            </p></td>
                        <th scope="row"></th>
                        <td class="text"><p></p></td>

                    </tr>
                        
                        
                    <tr>
                         
                        <th scope="row">Car Condition</th>
                        <td class="text">
                          <p>
                           <?php
                              if($rows->icon_new_yn=='Y'){
                                echo 'New';
                              }elseif($rows->icon_new_yn=='N'){
                                echo 'Used';
                              }
                            ?>
                          </p>
                        </td>
                        <th scope="row">Car Dimension (L×W×H) mm</th>
                        <td class="text"><p>
                                <label>
                                    <?php
                                       if($rows->car_length=='' || $rows->car_length==0 || $rows->car_length==NULL){
                                           echo '';
                                       }
                                       else{
                                           echo number_format($rows->car_length) .' x';
                                       }
                                    ?>
                                </label>
                                <label>
                                    <?php
                                       if($rows->car_width=='' || $rows->car_width==0 || $rows->car_width==NULL){
                                           echo '';
                                       }
                                       else{
                                           echo number_format($rows->car_width) .' x';
                                       }
                                    ?>
                                </label>
                                <label>
                                    <?php
                                       if($rows->car_height=='' || $rows->car_height==0 || $rows->car_height==NULL){
                                           echo '';
                                       }
                                       else{
                                           echo number_format($rows->car_height);
                                       }
                                    ?>
                                </label>
                            </p></td>

                    </tr>    
                        
                        
                         
                    <tr>
                        <th scope="row">Location</th>
                        <td class="text"><p><?= $rows->country_name.($rows->car_city!=''? '&nbsp;/ City : &nbsp;'.$rows->car_city:''); ?></p></td>

                        <th scope="row">Steering</th>
                        <td class="text">
                            <?php 
                              if($rows->car_steering=='' || $rows->car_steering=='0'){
                                  echo '';
                              }
                            else {
                            ?>
                            <p>
                                <?php
                                  echo ($rows->car_steering =='LHD') ? 'Left-hand driving' : 'Right-hand driving' ;
                                ?>
                            </p>
                            <?php 
                              }
                            ?>
                        </td>
    
                    </tr>
						
                    
                     <tr>
                        <th scope="row">Car Status</th>
                        <td class="text">
                           
                            <p><?=$rows->icon_status?></p>
                        </td>
                        <th scope="row">CHASSIS NO.</th>
                        <td class="text">
                            <p><?php echo $rows->car_chassis_no; ?></p>
                        </td>
                    </tr>
                    
                    
                   <tr>
                        <th scope="row">MAKE</th>
                        <td class="text">
                        	<p><?php echo $rows->car_make; ?></p>
                        </td>
                        <th scope="row">MODEL</th>
                        <td class="text">
                            <?php 
                               if($rows->car_model=='' ||  $rows->car_model=='0'){
                                   echo '';
                               }
                            else {
                            ?>
                            <p><?php echo $rows->car_model; ?><?php } ?></p>
                        </td>
                    </tr>
    
                    
    
                   <tr>
                        <th scope="row">GRADE</th>
                        <td class="text">
                            <p><?php echo $rows->car_grade; ?></p>
                        </td>
    
                        <th scope="row">ENGINE SIZE</th>
                        <td class="text">
                            <?php
                              if($rows->car_cc=='' || $rows->car_cc=='0'){
                                  echo '';
                              }
                            else {
                            ?>
                            <p><?php echo $rows->car_cc; ?> CC (only number)</p><?php } ?> </td>
                    </tr>
    
                    
    
                    <tr>
                        <th scope="row">Registration Date</th>
                        <td class="text">
                        <p>
                           <?php
                              if($rows->first_registration_month =='0'  && $rows->first_registration_year =='0'){
                               echo '';
                              }
                              else if($rows->first_registration_month =='0' && $rows->first_registration_year !='0') {
                                echo $rows->first_registration_year.' Year';
                              }
                               else if($rows->first_registration_month !='0' && $rows->first_registration_year =='0') {
                                echo date('F', mktime(0, 0, 0, $rows->first_registration_month, 1));
                              }
                              else{
                                echo date('F', mktime(0, 0, 0, $rows->first_registration_month, 1)).' / '.$rows->first_registration_year.' Year';
                              }
                             
                           ?>
                        </p>
                       
                        </td>
                        
                        <th scope="row">MANUFACTURED</th>
                        <td class="text">
                            <?php  
                            
                              if($rows->manu_year=='' || $rows->manu_year=='0'){
                                  echo '';
                              }
                             else {
                            ?>
                            <p><?php echo $rows->manu_year; ?> Year (only number)</p><?php } ?>
                        </td>
                       
                    </tr>
                     <tr>
                        <th scope="row">MODEL YEAR</th>
                        <td class="text">
                            <?php 
                               if($rows->car_model_year=='' || $rows->car_model_year=='0'){
                                   echo '';
                               }
                              else {
                            ?>
                            <p><?php echo $rows->car_model_year; ?> Year (only number)<?php } ?></p>
                        </td>
    					<th scope="row">SEATS</th>
                        <td class="text">
                            <?php
                              if($rows->car_seat=='' || $rows->car_seat=='0'){
                                  echo '';
                              }
                             else {
                            ?>
                            <p><?= $rows->car_seat?> SEATS (only number)</p><?php } ?></td>
                    </tr>
                     <tr>
                        <th scope="row">MILEAGE</th>
               
                        <td class="text">
                             <?php 
                           if($rows->car_mileage=='' || $rows->car_mileage=='0'){
                               echo '';
                           }
                         else {
                        ?>
                            <p><?php echo $rows->car_mileage; ?> Km (only number)<?php } ?></p></td>
                        <th scope="row">COLOR</th>
                        <td class="text" colspan="3">
                            <?php 
                              if($rows->car_color=='' || $rows->car_color=='0'){
                                  echo '';
                              }
                             else {
                            ?>
                         <p><?= $rows->car_color ?></p><?php } ?></td>
                    </tr>
                    <tr>
                        <th scope="row">BODY TYPE</th>
                        <td class="text">
                           <?php 
                              if( $rows->car_body_type=='' ||  $rows->car_body_type=='0' ){
                                  echo '';
                              }
                              else {
                           ?>
                            <p><?= $rows->car_body_type ?></p><?php } ?></td>
                        <th scope="row">FUEL</th>
                        <td class="text">
                         <?php
                            if($rows->car_fuel=='' || $rows->car_fuel=='0'){
                                echo '';
                            }
                         else {
                         ?>
                        <p><?= $rows->car_fuel ?></p><?php } ?></td>
                    </tr>
                    <tr>
                    	<th scope="row">TRANSMISSION</th>
                        <td class="text">
                            <?php 
                            if($rows->car_transmission=='' || $rows->car_transmission=='0'){
                                echo '';
                            }
                        else {
                                    
                             ?>
                            <p><?= $rows->car_transmission ?></p><?php } ?></td>
                        
                    	<th scope="row">DRIVE TYPE</th>
                        <td class="text">
                            <?php 
                              if($rows->car_drive_type=='' || $rows->car_drive_type=='0'){
                                  echo '';
                              }
                               else {
                            ?>
                            <p><?=$rows->car_drive_type?><?php } ?></p>
                        </td>
                    </tr>
                    
                   
                   <tr>
                        <th scope="row">OPTION</th>
                        <td class="text" colspan="3">
                            <div class="check"> 

                            <? 
                                    $str_option = array( );
                                    $str_option = explode("^",$rows->car_options); 

                                    for($i=0 ; $i<count($str_option) ; $i++)
                                    {
                                        echo $str_option[$i];
                                        echo '&nbsp;&nbsp;/&nbsp;&nbsp;';
                                    }

                            ?>
                            </div>
                        </td>
                    </tr>
    
                     <tr>
                        <th scope="row">FOB COST</th>
                        <td class="text"><p><?php if($rows->fob=='' ||$rows->fob=='0'  ){
                                        echo 'Ask Price';}else {
        echo $rows->fob;?> <?=func_currency_sym($rows->car_fob_currency,'Y') ?><?php }?></p> </td>
                        
                        <? $actual_selling_price = $rows->car_actual_price + $rows->car_freight_fee; ?>
                        <?php
                          if($actual_selling_price=='' || $actual_selling_price=='0'){
                              echo '';
                          }
                         else {
                        ?>
                        <th scope="row" style="background:#FDBFBF;">Actual Selling Price </th>
                        <td class="text" colspan="3"><p><?=$actual_selling_price ?> <?=func_currency_sym($rows->car_actual_price_currency,'Y')?><?php } ?></p> </td>                
                    </tr>

                    <tr>
                       
                        <th scope="row" style="background:#FDBFBF;" title="Car Deposit">Car Deposit</th>
                        <td class="text">
                            <?php 
                             if($rows->allocate_sum=='' || $rows->allocate_sum=='0'){
                                 echo '';
                             }
                           else {
                            ?>
                            <p><?=$rows->allocate_sum?> <?=$rows->currency_type?></p><?php } ?> </td>

                    </tr>

                    <tr>
                    	<th scope="row">Files</th>
                        <td class="text" colspan="3">
                            
                            <p>
                            <?=func_create_attach_files_name($bbs_attach_files,'carItem','N','Y','','Y','','N')?>
                            </p>
                        </td>
                    </tr>
                    
                    <tr>
                            <th scope="row">Images</th>
                            <td class="text" colspan="3">
                            <?php 
                                if (count($select_bbs_cloud_list_buyer) > 0) {
                                  foreach ($select_bbs_cloud_list_buyer as $att_rows) {
                                    echo "<img src='" . $att_rows->base_url . "t_item_thumb/".$att_rows->public_id."' width='150' style='padding:4px' >";
                                  }
                                }
                              ?>
                            </td>
                        </tr>
                        
                    
                    </tbody>
                </table>
                
            <div class="btn_area_center">
				
				<a href="?c=admin&m=buyer_product" title="List"><img src="images/admin/list.gif" alt="목록"></a>
			</div>
    <?php
                       }
                    }
                    ?>
		</div>
	</div>
</form>  
