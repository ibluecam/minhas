<script type="text/javascript">

    //게시판 수정

    function update_adm_bbs(frm) {

        //alert("test"); return;

        if (!validate(frm.elements['subject'], '제목을 입력하세요.'))
            return false;

        if (!emailCheck(frm.elements['email'])) return false; //이메일 체크

                < ? //답변기능, 비밀글기능 여부

                if (func_get_config($bbs_config_result, 'reply_yn') == 'Y' || func_get_config($bbs_config_result, 'secret_yn') == 'Y') {

        ? >
                //일반글이면.. 비밀번호 입력

                if (frm.elements['notice_yn'][0].checked || frm.elements['secret_yn'][1].checked) {

        frm.elements['password'].disabled = false;
                if (!validate(frm.elements['password'], '비밀번호를 입력하세요.')) return false;
        }

        else

        {

        frm.elements['password'].disabled = true;
        }

        < ? } ? >
                < ? if (func_get_config($bbs_config_result, 'editor_yn') == 'Y') { //게시판 HTML 에디터를 사용할 경우..  ?>

        if (tinyMCE.get('contents').getContent() == '' || tinyMCE.get('contents').getContent() == '<p>&nbsp;</p>') {

        alert("내용을 입력하세요.");
                return false;
        }

        < ? } else { ? >
                if (!validate(frm.elements['contents'], '내용을 입력하세요.')) return false;
                < ? } ? >
                return true;

    }



    //게시판 첨부파일 삭제

    function delete_bbs_attach(att_idx) {

        location.href = "/?c=admin&amp;m=adm_bbs_update&amp;mcd=<?= $mcd ?>&amp;idx=<?= $idx ?>&att_idx=" + att_idx + "&amp;only_attach=Y&amp;cur_page=<?= $cur_page ?>&amp;sParam=<?= urlencode($sParam) ?>";

    }

</script>

<!-- script>
$(document).ready(function(){

  $("#shipok_yn").click(function(){
    $("#test").toggle();
  });
});
</script -->





<? 
//echo $idx[0];
$sql = 'SELECT * FROM iw_bbs WHERE idx = '. $idx[0];
$ship_detail = mysql_query($sql);
//var_dump($sql); exit();
$rows = mysql_fetch_object($ship_detail);

//var_dump($rows); exit();
?>


<form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_bbs_ship_update_exec&amp;mcd=<?= $mcd ?>&amp;idx=<?= $rows->idx ?>" onsubmit="return update_adm_bbs(this);">

    <input type="hidden" name="idx" value="<? $rows->idx ?>" />
    <!-- 
            <input type="hidden" name="cur_page" value="<?= $cur_page ?>" />
    
            <input type="hidden" name="sParam" value="<?= $sParam ?>" /> -->

    <div class="contents_box_middle">

        <h1 class="title">Shipment</h1>

        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - Shipment</li>

            </ul>

        </div>

        <div class="table_write_box">

            <h1 class="table_title">Car Information</h1>

            <table class="table_write" summary="<?= func_get_config($bbs_config_result, 'bbs_code_name') ?>" cellspacing="0">

                <caption class="hidden"></caption>

                <col width="18%" /><col width="32%" /><col width="18%" /><col width="32%" />

                <tr>
                    <th class="key_02">RefNo</th>
                    <td><?= $rows->car_stock_no ?></td>

                    <th class="key_02">ChassisNo</th>
                    <td><?= $rows->car_chassis_no ?></td>
                </tr>
                <tr>
                    <th class="key_02">Car</th>
                    <td><?= $rows->car_model ?></td>

                    <th class="key_02">Year</th>
                    <td><?= $rows->car_model_year ?></td>
                </tr>

                <tr>
                    <th class="key_02">Status</th>
                    <td>

                        <select name="values[icon_status]" style="width:200px;">
                            <option value="sale" <?php echo ($rows->icon_status == 'sale') ? 'selected="selected"' : ''; ?>> Sale</option>
                            <option value="order" <?php echo ($rows->icon_status == 'order') ? 'selected="selected"' : ''; ?>> Order</option>
                            <option value="reserved" <?php echo ($rows->icon_status == 'reserved') ? 'selected="selected"' : ''; ?>> Reserved</option>
                            <option value="shipok" <?php echo ($rows->icon_status == 'shipok') ? 'selected="selected"' : ''; ?>>ShipOk</option>
                            <option value="releaseok" <?php echo ($rows->icon_status == 'releaseok') ? 'selected="selected"' : ''; ?>>ReleaseOK</option>
                            <option value="soldout" <?php echo ($rows->icon_status == 'soldout') ? 'selected="selected"' : ''; ?>>Soldout</option>
                        </select>



                    </td>

                    <th class="key_02">Sales</th>
                    <td>
                        <?  
                                    $sql=mysql_query("SELECT CONCAT(ifnull(member_first_name,''),'  ',ifnull(member_last_name,'')) as sales_full_name FROM iw_member WHERE member_no='$rows->sales_no' AND grade_no='3' ");
                                    
                                    while($row=mysql_fetch_array($sql))
                                    {
                                        $sales_full_name = $row['sales_full_name'];

                                        if(isset($rows->sales_no)) { echo "$sales_full_name";}else{ echo " ";}
                                    } 

                        ?>
                    </td>
                </tr>
            </table>
        </div>	


        <div class="table_write_box">

            <h1 class="table_title">Shipment Information</h1>

            <table class="table_write" summary="<?= func_get_config($bbs_config_result, 'bbs_code_name') ?>" cellspacing="0">

                <caption class="hidden"></caption>

                <col width="18%" /><col width="32%" /><col width="18%" /><col width="32%" />	


                <tr>

                    <th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />Customer</th>

                    <td class="value_02" colspan="3">

                        <select style="width:200px;" name="values[customer_no]">
                            <option >---Customer---<?=$rows->customer?></option>
                            <? 
                            $sql=mysql_query("SELECT member_no,CONCAT(ifnull(member_first_name,''),'  ',ifnull(member_last_name,'')) as customer_full_name FROM iw_member WHERE grade_no='11' ");
                            while($row=mysql_fetch_array($sql))
                            {
                            $id=$row['member_no'];
                            $data=$row['customer_full_name'];
                            ?>	
                            <option value="<?= $id ?>" <?if($id==$rows->customer_no){echo "selected='selected'";} ?> > <?= $data ?> </option>;
                            <?
                            } 
                            ?>
                        </select>

                    </td>
                </tr>

                <tr>
                    <th class="key_02">Vessel Name</th>

                    <td class="value_02">

                        <input type="text" name="values[vessel_name]" id="discharge" value="<?= $rows->vessel_name ?>" class="input_1" style="width:200px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="30" />

                    </td>


                    <th class="key_02">Discharge</th>

                    <td class="value_02">

                        <input type="text" name="values[discharge]" id="discharge" value="<?= $rows->discharge ?>" class="input_1" style="width:200px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="30" />

                    </td>
                </tr>

                <tr>

                    <th class="key_02">PortLeavedDate</th>

                    <td class="value_02">

                        <? if(isset($rows->port_leaved_date)) { ?>
                        <input type="text" name="values[port_leaved_date]" id="port_leaved_date" value="<?= func_get_config_date(func_get_config($bbs_config_result, 'date_format'), $rows->port_leaved_date) ?>" class="input_1" style="width:70px;background-color:#ededed;" readonly />
                        <a href="#" onclick="Calendar(document.adm_frm.elements['values[port_leaved_date]']);
                                                        return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>

                        <? } else { ?>
                        <input type="text" name="values[port_leaved_date]" id="port_leaved_date" value="" class="input_1" style="width:70px;background-color:#ededed;" readonly />
                        <a href="#" onclick="Calendar(document.adm_frm.elements['values[port_leaved_date]']);
                                                        return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>

                        <? } ?>
                    </td>

                    <th class="key_02">DHL no.</th>

                    <td class="value_02" colspan="3" >

                        <input type="text" name="values[dhl_no]" id="dhl_no" value="<?= $rows->dhl_no ?>" class="input_1" style="width:200px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="30" />

                    </td>

                </tr>
                <tr>

                    <th class="key_02">Consignee Info</th>

                    <td class="value_02">

                        <input type="text" name="values[consignee_info]" id="consignee_info" value="<?= $rows->consignee_info ?>" class="input_1" style="width:200px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="30" />

                    </td>
                    <th class="key_02">BL No.</th>

                    <td class="value_02" colspan="3">

                        <input type="text" name="values[bl_no]" id="bl_no" value="<?= $rows->bl_no ?>" class="input_1" style="width:200px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="20" />

                    </td>

                </tr>


<!--                 <tr>

                    <th class="key_02">Freight Fee</th>

                    <td class="value_02">

                        $ <input type="text" name="values[car_freight_fee]" id="car_freight_fee" value="<?= $rows->car_freight_fee ?>" class="input_1" style="width:150px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="20" />

                    </td>
                    <th class="key_02">Deposit</th>

                    <td class="value_02" colspan="3">

                        $ <input type="text" name="values[car_deposit]" id="car_deposit" value="<?= $rows->car_deposit ?>" class="input_1" style="width:150px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="20" />

                    </td>

                </tr> -->


            </table>

            <div class="btn_area_center">

                <input type="image" src="/images/admin/submit.gif" title="확인" />

                <a href="/?c=admin&amp;m=adm_bbs_list&amp;mcd=<?= $mcd ?>" title="목록"><img src="/images/admin/back.gif" alt="목록" /></a>

            </div>

        </div>

    </div>

</form>
