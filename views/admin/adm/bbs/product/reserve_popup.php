<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Reserve</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../../../../css/admin/datepicker.css">
<script src="../../../../../js/tcal.js"></script>
<script src="../../../../../js/admin/datepicker.js"></script>
<script src="../../../../../js/common/common.js"></script>

<style>
	.reservation_container{
		padding-bottom: 20px;
		overflow: 	auto;
		width: 100%;
		border: 1px solid #ccc;
	}
	.box_title{
		background: linear-gradient(to bottom, rgba(255, 255, 255, 1) 0%, rgba(239, 239, 239, 1) 57%, rgba(246, 246, 246, 1) 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
	    color: #0068b7;
	    font-family: "open Sans";
	    padding: 10px 15px;
	    border-bottom: 1px solid #ccc;
	    margin-bottom: 10px;
	}
	.box{
		padding: 10px 20px;
	}
	
	#savenews{
			background: #2147AF;
			color: white;
			border: none;
			padding: 5px 20px;
			cursor: pointer;
	}
        .txt_error{
            width: 127px; padding: 0px;
        }
</style>
</head>

 <script>
 
 window.onunload = function(){
      window.opener.location.reload();
 };

 	function selectPriceType(){

		var price_type=$("input[name='values[car_price_type]']:checked").val();

		if(price_type=='FOB'){

			$("#fob_cost").css("display", "inline");
			$("#freight_fee").css("display", "none");

		}else{

			$("#fob_cost").css("display", "inline");
			$("#freight_fee").css("display", "inline");

		}
	}

	function currency_selector(){
		var currency_type=$('#car_actual_price_currency option:selected').val();
		
		$("#car_freight_fee_currency option[value="+currency_type+"]").prop("selected", "ture");
	}
	
 </script>

<body>
<? $sales_no = (int)$_SESSION['ADMIN']['member_no']; ?>

<div class="reservation_container">
 <div class="box_title"><b>Car Reservation</b></div>
 <div class="box">Please enter an Actual Selling Price.</div>

	<form name="adm_frm" id="adm_frmb" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_reserve_popup_update_exec&amp;mcd=<?= $mcd ?>&amp;idx=<?= $idx ?>">
		<input type="hidden" name="values[icon_status]" value="reserved" />
		<input type="hidden" name="values[sales_no]" value="<?=$sales_no ?>" />
		<table>
			<tr>
				<td class="box">Customer</td>
				<td class="box">
					<select  id="customer_no" class="txt_error" name="values[customer_no]">
					  <option value='0'>-Customer-</option>
					  <? // Admin:1 , Manager:10, Customer:13, User:14, Register:15
						 $sql=mysql_query("SELECT member_no, CONCAT(member_first_name,member_last_name)AS member_full_name, email FROM iw_member 
                                                 where grade_no IN (11) and business_type='buyer' and draft='N' order by member_full_name ");
							while($row=mysql_fetch_array($sql))
							{
	                            $id=$row['member_no'];
	                            $data=$row['member_full_name'];
	                            if($customer_no == $id) $select="selected"; else $select="";
                            	echo '<option '.$select.' value="'.$id.'">'.$data.' ('.$row['email'].')'.'</option>';
							} 
					   ?>
				    </select>
				</td>
			</tr>
                        
                        
                        <tr>
				<td class="box">Reservation Date</td>
				<td class="box">
                                    <input name="values[reserved_dt]" data-auto-close="true" data-date-format="yyyy-mm-dd" id="reserved_dt"  type="text" class="txt_error datepicker-add-options">
                                    
                                
				</td>
			</tr>	
                        
                        
            <tr>
            	<td class="box">FOB COST</td>
            	<td class="box">
            		<label><?=$car_fob_cost?></label>
            		<label><?=$car_fob_currency?></label>
            	</td>
            </tr>         
                        
                        
                        
			<tr>
				<td class="box">Price Type</td>
				<td class="box">
					<input type="radio" id="price_type_1" value="FOB" id="FOB" name="values[car_price_type]" onchange="selectPriceType();" checked>
					<label for="price_type_1">FOB</label>
					<input type="radio" id="price_type_2" value="CIF" id="CIF" name="values[car_price_type]" onchange="selectPriceType();">
					<label for="price_type_2">CIF</label>
				</td>
			</tr>
			<tr>
				<td class="box">Actual Selling Price</td>
				<td class="box">
					<div id="fob_cost">
						Car Price
						<input name="values[car_actual_price]" id="car_actual_price" value="" style="width:50px;"/>
						<select name="values[car_actual_price_currency]" id="car_actual_price_currency" class="input_2" onchange="currency_selector();">
							
							<option value="USD" >USD</option>
							<option value="KRW" >KRW</option>
							<option value="JPY" >JPY</option>
							<option value="EUR" >EUR</option>

						</select>
					</div>
					<div id="freight_fee" style="display:none">
						+ Freight Fee
						<input name="values[car_freight_fee]" id="car_freight_fee" value="0" style="width:50px;"/>
						<select name="values[car_freight_fee_currency]" id="car_freight_fee_currency" class="input_2" onchange="">
							
							<option value="USD" disabled>USD</option>
							<option value="KRW" disabled>KRW</option>
							<option value="JPY" disabled>JPY</option>
							<option value="EUR" disabled>EUR</option>

						</select>
					</div>
						<input type="hidden" name="car_fob_cost" id="car_fob_cost" value="<? echo $car_fob_cost; ?>">					
				</td>
			</tr>
		</table>
		<div class="box">
			<input type="submit" id="savenews" value="Save" name="submit" onclick="return validateprice();" />
		</div>
	</form>
</div>
<script type="text/javascript">


	//validate text box price input only number
	$(document).ready(function (e) {
	  //called when key is pressed in textbox
	  $("#car_actual_price").keypress(function (e) {
	     //if the letter is not digit then display error and don't type anything
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        //display error message //
	         return false;
	    }
	   });
	   $("#car_freight_fee").keypress(function (e) {
	     //if the letter is not digit then display error and don't type anything
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        //display error message //
	         return false;
	    }
	   });
	});
	function validateprice(){
		// var car_actual_price = document.getElementById('car_actual_price').value;
		// var car_freight_fee  = document.getElementById('car_freight_fee').value;

	  	if (isEmptyCustomer()||isEmptyPrice()) {
	  		//alert("Please input Car Price");
	  		return false;
		}else{
			return true;
		}
	}
	function isEmptyCustomer(){
		if (adm_frm.customer_no.value == "" || adm_frm.customer_no.value == "0") {
	  		alert("Please input Customer. If you cannot find the customer, please go to customers and register a customer first.");
	  		return true;
	  	}else{
	  		return false;
	  	}
	}
	function isEmptyPrice(){
		if (adm_frm.car_actual_price.value == "") {
	  		alert("Please input Car Price");
	  		return true;
	  	}else{
	  		return false;
	  	}
	}
	// function isValidPrice(){
	// 	var car_actual_price = document.getElementById('car_actual_price').value;
	// 	var car_freight_fee  = document.getElementById('car_freight_fee').value;
	// 	var car_fob_cost  = document.getElementById('car_fob_cost').value;

	// 	if(parseInt(car_actual_price) + parseInt(car_freight_fee) >= parseInt(car_fob_cost)){
	// 	//if(adm_frm.car_actual_price.value + adm_frm.car_freight_fee.value <= adm_frm.car_fob_cost.value){	
	// 	  	return true;
	// 	}else{
			
	// 		alert("Actual Selling Price must be higher than boss Price");
	// 		return false;
	// 	}
	// }
</script>
<? 	
	if(isset($_GET['page_acc']) && $_GET['page_acc']=='completed'){
?>

<script type="text/javascript">
	window.close();

</script>

<? } ?>
</body>
</html>
 
<script type="text/javascript">
$(document).ready(function() {
    
    $(".datepicker-add-options").datepicker({
        
    });

});
 </script>