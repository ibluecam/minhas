<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
 -->
 
<script type="text/javascript" src="/js/common/common.js"></script>


<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />

<!-- link calendar resources -->
<link rel="stylesheet" type="text/css" href="/css/tcal.css" />
<script type="text/javascript" src="/js/tcal.js"></script> 
<script>
    //게시판 등록
    function insert_adm_bbs(frm) {
        if (!validate(frm.elements['values[car_make]'], 'INPUT [MAKE]'))
            return false;
        if (!validate(frm.elements['values[car_model]'], 'INPUT [MODEL]'))
            return false;
        if (!validate(frm.elements['values[car_chassis_no]'], 'INPUT [Chassis No.]'))
            return false;

        if(document.getElementById("sbtok").checked){
              document.getElementById('sbtokHidden').disabled = true;
            }
    }

    function delete_bbs_attach(att_idx, idx) {
        var f = document.adm_frm;
        var x;
        if (confirm('Do you want to delete image ID (' + idx + ' )') == true) {
            f.action = '?c=admin&m=adm_bbs_modify&only_attach=Y&idx=' + idx + '&att_idx=' + att_idx;
            f.submit();
        } else {
            x = "You pressed Cancel!";
        }

    }
//    function delete_bbs_attach_files(idx, bbs_idx) {
//        var f = document.adm_frm;
//        var x;
//        if (confirm('Do you want to delete image ID (' + bbs_idx + ' )') == true) {
//            f.action = '?c=admin&m=adm_bbs_modify&mcd=<?php echo $mcd; ?>&only_attach=Y&idx=<?= $idx ?>&id=' + idx + '&bbs_idx=' + bbs_idx;
//            f.submit();
//        } else {
//            x = "You pressed Cancel!";
//        }
//
//    }

    function currency_selector(){
        var currency_type=$('#unit_price_currency option:selected').val();
        
        $("#tax_price_currency option[value="+currency_type+"]").prop("selected", "ture");
    }

    function currency_selector_2(){
        var currency_type=$('#car_actual_price_currency option:selected').val();
        
        $("#car_freight_fee_currency option[value="+currency_type+"]").prop("selected", "ture");
    }
    
    
</script>

<?php
//$sql = 'SELECT * FROM iw_bbs WHERE idx = '. $idx[0];
//$ship_detail = mysql_query($sql);
//$rows = mysql_fetch_object($ship_detail);
//var_dump($rows); exit();
if (count($bbs_detail_list) > 0) {
    foreach ($bbs_detail_list->result() as $rows) {
        $inImageID = array();

        ?>
        <input type="hidden" id="saved_car_make" value="<?=$rows->car_make;?>"/>
        <form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="?c=admin&m=adm_bbs_modify_exec&only_attach=Y&mcd=<?php echo $mcd; ?>&idx=<?php echo $rows->idx; ?><?php ?>" onsubmit="return insert_adm_bbs(this);">
            <input type="hidden" id="idx" value="<?=$rows->idx ?>" >
            
            <input type="hidden" name="address" value="<?php if(isset($_POST['address'])){ echo $_POST['address']; } ?>"> <!-- sending url address -->

            <input type="hidden" name="values[car_stock_no]" value="<?=$rows->car_stock_no ?>" >
            
            <input type="hidden" name="values[category]" value="<?php
            if (isset($_REQUEST['category'])) {
                echo $_REQUEST['category'];
            } else {
                echo '';
            }
            ?>">	
            <div class="contents_box_middle">

                <h1 class="title">
                    <?php if($create_mode=='new'){ ?>
                        New
                        <input type="hidden" name="create_mode" value="new">
                    <?php }else{ ?>
                        Modify
                        <input type="hidden" name="create_mode" value="modify">
                    <?php } ?>
                </h1>

                <div class="tip_box">

                    <p class="title"><img src="images/admin/tip_img.gif" alt="tip" /></p>

                    <ul>
                        <?php if($create_mode=='new'){ ?>
                            <li> - New </li>
                        <?php }else{ ?>
                            <li> - Modify </li>
                        <?php } ?>
                    </ul>

                </div>
                <div class="table_write_box">
                    <table class="table_write" summary="<?php echo func_get_config($bbs_config_result, 'bbs_code_name'); ?>" border="0" cellspacing="0" width="100%">
                        <caption></caption>
                        <colgroup>
                            <col width="15%" />
                            <col width="35%" />
                            <col width="15%" />
                            <col width="35%" />
                        </colgroup>

                        <?
                        //카테고리 사용 여부
                        if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {
                        $category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));
                        $category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));
                        }
                        ?>

                        <tr><td colspan="4" class="text_info">* Car Information</td>
                              <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>
                        </tr>
                        
                        
                        
                       
                        
                        
                        
                        <tr>
                            <th scope="row">Car Condition</th> 

                            <td class="text">
                            <label>
                                <input type="radio" required="" <?php if($rows->icon_new_yn=='Y' ) echo "checked"; ?> value="Y" name="values[icon_new_yn]">
                                New
                            </label>
                                <label style="padding: 10px;">
                                <input type="radio" required="" <?php if($rows->icon_new_yn=='N' || $rows->icon_new_yn=='') echo "checked"; ?> value="N" name="values[icon_new_yn]">
                                 Used
                            </label>
                            </td>

                          
                            <th scope="row">Car Dimension (L×W×H) mm</th>
                            <td class="text">
                                <input style="width:80px;height:20px;" type="text" name="values[car_length]" value="<?php echo $rows->car_length; ?>" placeholder="length">
                                <input style="width:80px;height:20px;" type="text" name="values[car_width]" value="<?php echo $rows->car_width; ?>" placeholder="width">
                                <input style="width:80px;height:20px;" type="text" name="values[car_height]" value="<?php echo $rows->car_height; ?>" placeholder="height">
                            </td>
                       
                             
                             
                             <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>

                            
                        </tr>
                        
                        
                        
                        
                        

                        <tr>
                            <th scope="row">Location</th>
                            <td class="text">
                                <select name="values[country]" id="country" required  class="input_5">
                                <option value="">-Select-</option>
                                    <?php 
                                        foreach($country_list as $country){
                                    ?>
                                            <option 
                                            <?php

                                            if ($rows->country !=''){
                                                if($rows->country == $country->cc){
                                                    echo 'selected="selected"';
                                                }
                                            }else{
                                                if ($member_logged->member_country == $country->cc){
                                                    echo 'selected="selected"';
                                                }                                                
                                            }

                                            ?> 
                                            value="<?php echo $country->cc; ?>"><?php echo $country->country_name; ?></option>
                                    <?php
                                        } 
                                    ?>
                                </select>
                                <?php
                                if (count($category_code) > 0) {
                                    for ($i = 0; $i < count($category_code); $i++) {
                                        //if($category_code[$i] !="Brandnew"){ //brandnew 제외
                                        ?>
                                        <input type="hidden" name="values[category]" value="<?php echo $category_code[$i]; ?>" id="category" />
                                        <?php
                                        //   }
                                    }
                                }
                                ?>
                                City &nbsp;<input type="text" name="values[car_city]" id="car_city" class="b_text input_4" value="<?= $rows->car_city ?>" >
                            </td>

                          

                            <th scope="row">Steering</th> 

                            <td class="text">
                              
                                <label>
                                    <input type="radio" name="values[car_steering]" <?php if($rows->car_steering=='LHD' || $rows->car_steering=='' ) echo "checked"; ?> value="LHD"  >
                                    &nbsp;Left-hand driving
                                </label> 
                               
                                <label style="padding: 10px;">
                                    <input type="radio" name="values[car_steering]" <?php if($rows->car_steering=='RHD') echo "checked"; ?>  value="RHD" >
                                     &nbsp;Right-hand driving
                                </label> 
                               
                            </td>

                           
                             <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>
                          
                        </tr>
                       
                            <th scope="row">AVAILABILITY</th>                        
                            <td class="text">
                                <select  name="values[icon_status]" class="input_5">
                                    <option value="sale"<?php echo ($rows->icon_status == 'sale') ? 'selected="selected"' : ''; ?>>Sale</option>
                                    <option value="reserved"
                                            <?php echo ($rows->icon_status == 'reserved') ? 'selected="selected"' : ''; ?>> Reserved</option>
                                    <option value="shipok" <?php echo ($rows->icon_status == 'shipok') ? 'selected="selected"' : ''; ?>>ShipOk</option>
                                    <option value="soldout" <?php echo ($rows->icon_status == 'soldout') ? 'selected="selected"' : ''; ?>>Soldout</option>
                                    <option value="releaseok" <?php echo ($rows->icon_status == 'releaseok') ? 'selected="selected"' : ''; ?>>ReleaseOk</option>

                                </select>
                            </td>
                            <th scope="row">CHASSIS NO.</th>
                            <td class="text">
                                <input type="text" name="values[car_chassis_no]" class="b_text input_4" value="<?php echo $rows->car_chassis_no; ?>" title="ChassisNo">
                            </td>
                              <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">MAKE</th>
                            <td class="text">
                                <select id="make_list" class="input_5">
                                    <option value="">- Other -</option>
                                    <?php 
                                        foreach($make_list_admin as $make){
                                            $background_url =$make->icon_name;
                                            echo '<option style="background-image: url('.$background_url.');background-size:30px 15px; background-repeat: no-repeat;padding-left: 35px;" value="'.strtoupper($make->make_name).'">'.strtoupper($make->make_name).'</option>';
                                        } 
                                    ?>
                                </select>
                                <input type="text" id="car_make" name="values[car_make]" value="<?php echo $rows->car_make; ?>" class="b_text input_4" title="Maker">
                           

                            </td>

                            <th scope="row">MODEL</th>
                            <td class="text">
                                <select id="model_list" class="input_5">
                                    <option value="">- Other -</option>
                                    <?php 
                                        foreach($model_list_admin as $model){
                                            echo '<option value="'.strtoupper($model->model_name).'">'.strtoupper($model->model_name).'</option>';
                                        } 
                                    ?>
                                </select>
                                <?php if($rows->car_model!='') $display="none"; else $display="inline"; ?>
                                <input type="text" id="car_model" name="values[car_model]" style="display:<?= $display; ?>" value="<?php echo $rows->car_model; ?>" class="b_text input_4" title="Model">
                            </td>
                              <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">GRADE</th>
                            <td class="text">
                                <input type="text" name="values[car_grade]" AutocompleteYN="Y" ViewExYN="Y" value="<?php echo $rows->car_grade; ?>" ViewEx="ex)TLX" class="b_text input_4">
                            </td>

                            <th scope="row">ENGINE SIZE</th>
                            <td class="text"><input type="text" name="values[car_cc]" id="cc" class="b_text input_4" value="<?php echo $rows->car_cc; ?>" ViewExYN="Y" ViewEx="ex)3000"  maxlength="10" /> CC (only number)</td>
                              <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">MODEL YEAR</th>
                            <td class="text">
                                <input type="text" name="values[car_model_year]" AutocompleteYN="Y" ViewExYN="Y" ViewEx="ex)2012" value="<?php echo $rows->car_model_year; ?>" class="b_text input_4" title="Year"> Year (only number)
                            </td>

                            <th scope="row">MANUFACTURED</th>
                            <td class="text">
                                <input type="text" name="values[manu_year]" AutocompleteYN="Y" ViewExYN="Y" ViewEx="ex)2012" value="<?php echo $rows->manu_year; ?>"  class="b_text input_4" title="Year"> Year (only number)
                            </td>
                            <!-- es -->
                              <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">REGISTRATION DATE </th>
                            <td class="text">
                                <select name="values[first_registration_month]" id="first_registration_month" class="input_5">
                                    <?= func_create_months($rows->first_registration_month); ?>
                                </select> Month

                                <select name="values[first_registration_year]" id="first_registration_year" class="input_5">
                                    <?= func_create_years($rows->first_registration_year); ?>
                                </select> Year
                            </td>
                            <th scope="row">SEATS</th>
                            <td class="text"><input name="values[car_seat]" id="car_seat" type="text" class="b_text input_4" value="<?php echo $rows->car_seat; ?>" ViewExYN="Y" ViewEx="ex)4" maxlength="10" /> SEATS (only number)</td>
                              <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">MILEAGE</th>
                            <td class="text"><input type="text" name="values[car_mileage]" id="mileage" class="b_text input_4" value="<?php echo $rows->car_mileage; ?>" ViewExYN="Y" ViewEx="ex)56120"  maxlength="10" /> Km (only number)</td>
                            <th scope="row">COLOR</th>
                            <td class="text" colspan="3">
                                <select name="values[car_color]" class="input_5" id="car_color">
                                    <?php 
                                        foreach($color_list as $color){
                                        ?>
                                            <option <?php echo ($rows->car_color == $color->color_name) ? 'selected="selected"' : '' ?> value="<?php echo $color->color_name; ?>"><?php echo $color->color_name; ?></option>
                                        <?php
                                        } 
                                    ?>
                                </select>
                                
                                <a class="fancybox_color iframe" href="/?c=admin&amp;m=modify_color_popup&amp;mcd=<?= $mcd ?>">Add Color</a>
                            </td>

                        </tr>
                        <tr>
                            <th scope="row">BODY TYPE</th>
                            <td class="text">
                                <select name="values[car_body_type]" class="input_5">
                                    <?php 
                                        foreach($bodytype_list as $bodytype){
                                        ?>
                                            <option <?php echo ($rows->car_body_type == $bodytype->body_name) ? 'selected="selected"' : '' ?> value="<?php echo $bodytype->body_name; ?>"><?php echo $bodytype->body_title; ?></option>
                                        <?php
                                        } 
                                    ?>
                                </select>
                            </td>
                            <th scope="row">FUEL</th>
                            <td class="text">
                                <select name="values[car_fuel]" class="input_5">
                                    <?php foreach (Array('Petrol', 'Diesel', 'LPG', 'LPG + Petrol', 'Hybrid', 'Gasoline') as $Val) { ?>
                                        <option <?php echo ($rows->car_fuel==$Val)? 'selected="selected"' : '' ?> value="<?php echo $Val; ?>"><?php echo $Val; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                              <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">TRANSMISSION</th>
                            <td class="text">
                                <select name="values[car_transmission]" class="input_5">
                                    <?php foreach (Array('Auto', 'Manual') as $Val) { ?>
                                        <option <?php echo ($rows->car_transmission==$Val)? 'selected="selected"' : '' ?> value="<?php echo $Val; ?>"><?php echo $Val; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <th scope="row">DRIVE TYPE</th>
                            <td class="text">
                                <select name="values[car_drive_type]" class="input_5">
                                    <option value="">-Drive Type-</option>
                                    <? foreach(Array('2WD', '4WD') as $Val ) { ?>
                                    <option <?php echo ($rows->car_drive_type==$Val)? 'selected="selected"' : '' ?> value="<?= $Val ?>"><?= $Val ?></option>
                                    <? } ?>
                                </select>
                            </td>
                              <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>
                        </tr>
                       
                        <tr>
                            <th scope="row">FOB COST</th>
                            <td class="text">
                                <input name="values[car_fob_cost]" id="car_fob" type="text" class="b_text input_4" value="<?php  echo $rows->car_fob_cost; ?>" ViewExYN="Y" ViewEx="ex)3000" maxlength="10" />
                                <select name="values[car_fob_currency]" id="currency_type" class="input_2" onchange="">
                                    <?

                                    echo func_currency_sym($rows->car_fob_currency);

                                    ?>
                                </select>
                            </td>

                               <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>
                              <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>

                        </tr>
                           <?php
                            if( $_SESSION['ADMIN']['business_type'] !='seller' ){
                          ?>
                        <tr>
                            <th scope="row">Files</th>
                            <th>
                                <a href="#file_popup" class="fancybox"><span style="color:blue;">Click</span></a> <input type="button" id="extract_file_0" value="Extract Images?" />
                                <div id="extract_pdf_loading" style="margin-top: 5px; display:none;">
                                    <div style="float: left;"><img src="images/admin/loading-bar.gif"/></div>
                                    <div style="float: left; padding: 0px 10px;"><label style="color:#2D7DC0;">Extracting images from PDF</label></div>
                                </div>
                            </th>
                        </tr>
 
                       <!--  <tr>
                            <th scope="row">Images</th>
                            <td class="text" colspan="3">
                                <?php
                                $ifile = 1;
                                $modi_att = count($bbs_attach_list);
                                if (count($bbs_attach_list) > 0) {

                                    echo "<div class='div-img-title'";
                                    echo "<h2>Edite Image </h2>";
                                    echo "</div>";
                                    foreach ($bbs_attach_list->result() as $att_rows) {
                                        echo "<div class='div-image-view'>";
                                        echo "<img src='uploads/" . $mcd . "/" . $att_rows->raw_name . "_" . func_get_config($bbs_config_result, 'attach_thumbnail_size3') . $att_rows->file_ext . "' width='150' >";
                                        ?>
                                        <a href="#" class="close" onclick="delete_bbs_attach(<?php echo $att_rows->att_idx; ?>,<?php echo $att_rows->idx; ?>)" title="Delete"></a>
                                        <?php
                                        echo "</div>";
                                    }
                                } else {
                                    
                                }

                                echo "<div class='div-img-title'";
                                echo "<h2>Add Image</h2>";
                                echo "</div>";
                                ?>
                                <div style="padding:1px; 0px; width:100%; float:left;">
                                    <input type="file"  name="attach1[]" onchange="getFiles();"  id="attach" class="b_text" multiple="multiple" />
                                    <div style="color:red;">only more than ie10 or chrome, safari, firefox browser.</div>
                                </div>


                            </td>
                        </tr> -->
                        <tr>
                        <?php
                            }
                        ?>
                            <th cscope="row">Description</th>

                            <td class="text" colspan="3" style="padding:10px;">

                                <textarea name="values[contents]" id="contents" class="textarea" style="height:80px;"><?php if($rows->contents!='') echo $rows->contents; ?></textarea>

                            </td>
                             <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>

                        </tr>
                         <tr>
                            <th scope="row">Images</th>
                            <td class="text" colspan="3" id="upload_iframes" style="">

                            

                                <iframe id="upload_iframe" scrolling="no" class="upload_iframe" src="/?c=admin&m=adm_bbs_cloud_upload&idx=<?=$idx?>&mcd=product"></iframe>
                                
                            </td> 


                          <td class="text">
                              
                            </td>
                             <td class="text">
                              
                            </td>

                            

                        </tr>
                        </tbody>
                    </table>
                    <div class="btn_area_center">
                        <input type="image" src="images/admin/submit.gif" title="확인" />
                        <a href="?c=admin&amp;m=adm_bbs_list&amp;mcd=<?php echo $mcd; ?>" title="목록"><img src="images/admin/back.gif" alt="목록" /></a>
                        
                    </div>
                </div>
            </div>
            <div style="display:none;">
                <div id="file_popup" style="width: 300px; float:left; padding-bottom:10px;">
                    <center><h2>Car File Attachments</h2>
                    <h4>Please upload your files here</h4></center><br/>
                    <div id="popup0">
                        <label for="pass"><b>Inspection Sheet</b></label>
                        <input type="file" id="file_type_sheet0" name="file_type_sheet0[]" value="inspect" style="margin-top: 4px;"/>
                        <input type="button" value="X" id="clear0"/>
                        <? if(count($bbs_attach_files)>0){ ?>

                        <div id="inspect"  style="width:100%; float:left; padding-top:5px;">

                            <?= func_create_attach_files_name($bbs_attach_files, 'carItem', 'N', 'Y', '', 'N', 'inspect') ?>
                            <?php
                            foreach ($bbs_attach_files as $inspect) {

                                $inspects = $inspect->file_type;
                                if ($inspects == 'inspect') {
                                    echo "&nbsp;&nbsp;<a  class='del' data-file_type='$inspect->file_type' data-id='$inspect->bbs_idx' style='color:blue;cursor: pointer;' href'#'>Delete</a>";
                                }
                            }
                            ?>
                        </div>
                        <? } ?>
                    </div>
                    <div id="popup1">

                        <label for="pass" style="width:auto;"><b>Export Certification</b></label>

                        <input type="file" id="file_type_sheet1" name="file_type_sheet1[]" value="inspect" style="margin-top: 4px;"/>
                        <input type="button" value="X" id="clear0"/>
                        <? if(count($bbs_attach_files)>0){ ?>
                        <div id="export" style="width:100%; float:left; padding-top:5px;">

                            <div id="exports">
                                <?= func_create_attach_files_name($bbs_attach_files, 'carItem', 'N', 'Y', '', 'N', 'export') ?>
                                <?php
                                foreach ($bbs_attach_files as $export) {

                                    $exports = $export->file_type;
                                    if ($exports == 'export') {
                                        echo "&nbsp;&nbsp;<a  class='del' data-file_type='$export->file_type' data-id='$export->bbs_idx' style='color:blue;cursor: pointer;' href'#'>Delete</a>";
                                    }
                                }
                                ?>

                            </div>
                        </div>
                        <? }?>
                    </div>
                    <div id="popup2">
                        <label style="width: 186px;"><b>Cancellation Certification</b></label>
                        <input type="file" id="file_type_sheet2" name="file_type_sheet2[]" value="cancel" style="margin-top: 4px;" />
                        <input type="button" value="X" id="clear2"/>
                        <? if(count($bbs_attach_files)>0){ ?>
                        <div id="cancel" style="width:100%; float:left; padding-top:5px;">

                            <?= func_create_attach_files_name($bbs_attach_files, 'carItem', 'N', 'Y', '', 'N', 'cancel') ?>
                            <?php
                            foreach ($bbs_attach_files as $cancel) {

                                $cancels = $cancel->file_type;
                                if ($cancels == 'cancel') {
                                    echo "&nbsp;&nbsp;<a  class='del' data-file_type='$cancel->file_type' data-id='$cancel->bbs_idx' style='color:blue;cursor: pointer;' href'#'>Delete</a>";
                                }
                            }
                            ?>    </div>
                        <? } ?>
                    </div>
                    <div id="popup3">
                        <label for="pass"><b>Car Registeration</b></label>
                        <input type="file" id="file_type_sheet3"  name="file_type_sheet3[]" value="register" style="margin-top: 4px;"/>
                        <input type="button" value="X" id="clear3"/>
                        <? if(count($bbs_attach_files)>0){ ?>
                        <div id="register" style="width:100%; float:left; padding-top:5px;">
                            <?= func_create_attach_files_name($bbs_attach_files, 'carItem', 'N', 'Y', '', 'N', 'register') ?>
                            <?php
                            foreach ($bbs_attach_files as $register) {

                                $registers = $register->file_type;
                                if ($registers == 'register') {
                                    echo "&nbsp;&nbsp;<a  class='del' data-file_type='$register->file_type' data-id='$register->bbs_idx' style='color:blue;cursor: pointer;' href'#'>Delete</a>";
                                }
                            }
                            ?>      </div>
                        <? } ?>
                    </div>
                    <div id="popup4">
                        <label for="pass"><b>Original Register</b></label>
                        <input type="file"  id="file_type_sheet4" name="file_type_sheet4[]" value="original" style="margin-top: 4px;" />
                        <input type="button" value="X" id="clear4"/>
                        <? if(count($bbs_attach_files)>0){ ?>
                        <div id="original" style="width:100%; float:left; padding-top:5px;">

                            <?= func_create_attach_files_name($bbs_attach_files, 'carItem', 'N', 'Y', '', 'N', 'original') ?>
                            <?php
                            foreach ($bbs_attach_files as $original) {

                                $originals = $original->file_type;
                                if ($originals == 'original') {

                                    echo "&nbsp;&nbsp;<a  class='del' data-file_type='$original->file_type' data-id='$original->bbs_idx' style='color:blue;cursor: pointer;' href'#'>Delete</a>";
                                }
                            }
                            ?>
                        </div>
                        <? } ?>
                    </div>
                    <div id="popup5">
                        <label for="pass"><b>Invoice</b></label><br/>
                        <input type="file"  id="file_type_sheet5" name="file_type_sheet5[]" value="invoice" style="margin-top: 4px;" />
                        <input type="button" value="X" id="clear5"/>
                        <? if(count($bbs_attach_files)>0){ ?>
                        <div id="invoice" style="width:100%; float:left; padding-top:5px;">

                            <?= func_create_attach_files_name($bbs_attach_files, 'carItem', 'N', 'Y', '', 'N', 'invoice') ?>
                            <?php
                            foreach ($bbs_attach_files as $invoice) {

                                $invoices = $invoice->file_type;
                                if ($invoices == 'invoice') {

                                    echo "&nbsp;&nbsp;<a  class='del' data-file_type='$invoice->file_type' data-id='$invoice->bbs_idx' style='color:blue;cursor: pointer;' href'#'>Delete</a>";
                                }
                            }
                            ?>
                        </div>
                        <? } ?>
                    </div><br/>
                    <a href="#close" id="close"><input type="button" value="Ok" style="width: 60px;"></a>
                </div>
            </div>
        </form>
        <?php
    }
}
?>
<script type="text/javascript">
    $(function () {
        $('.del').click(function () {
            var bbs_idx = $(this).data('id');
            var file_type = $(this).data('file_type');
           
            var y;
            if (confirm('Do you want to delete image ? ') == true) {
                $.ajax({
                    url: '?c=admin&m=delete_file_image',
                    type: "POST",
                    async: false,
                    data: {
                        'bbs_idx': bbs_idx,
                        'file_type': file_type
                    },
                    success: function (result) {
                        
                        alert(result);
                        if (file_type == 'inspect') {

                            $('#inspect').html('No file');
                        }
                        if (file_type == 'export') {
                            $('#export').html('No file');
                        }
                        if (file_type == 'cancel') {
                            $('#cancel').html('No file');
                        }
                        if (file_type == 'register') {
                            $('#register').html('No file');
                        }
                        if (file_type == 'original') {
                            $('#original').html('No file');
                        }
                        if(file_type == 'invoice') {
                            $('#invoice').html('No file');
                        }

                    }

                });
            } else {
                return false;
                //y = "Your pressed Cancel!";
            }

        });

        $("#clear0").click(function () {
            $('#popup0').find('input:file').val('');
        });
        $("#clear1").click(function () {
            $('#popup1').find('input:file').val('');
        });
        $("#clear2").click(function () {
            $('#popup2').find('input:file').val('');
        });
        $("#clear3").click(function () {
            $('#popup3').find('input:file').val('');
        });
        $("#clear4").click(function () {
            $('#popup4').find('input:file').val('');
        });
        $("#clear5").click(function () {
            $('#popup5').find('input:file').val('');
        });
    });
    function resizeIframe(height){
        $("iframe").css("height", height);
    }

    function reloadModelList(car_make){
        $.ajax({
            method: "GET",
            url: "/?c=admin&m=adm_get_model_list&car_make="+car_make
            })
            .done(function( data ) {
                $("#model_list").html(data);

                // var selected_model = $("#model_list").val();
                // $("#car_model").val(selected_model);

                $("#car_model").val('');
                $("#car_model").css("display", "inline");
                // if ( $("#model_list").val()==""){
                //     $("#car_model").css("display", "inline");
                //  }else{
                //      $("#car_model").css("display", "none");
                // }
                
        });
        
    }
    
    $(document).ready(function(e){
        $("a.fancybox").fancybox({
            height  : "100%",
            width   : 300
        });  
        $("a.fancybox_color").fancybox({
            height  : 314,
            width   : 300,
            onClosed: function() { 
                reloadColorSelect();
            }
        }); 
        var saved_car_make = $("#saved_car_make").val();
        var saved_car_model = $("#car_model").val();
        if(saved_car_model!=''){
            $("#model_list").val(saved_car_model);
            if($("#model_list").val()==''){
                $("#car_model").css("display", "inline");
                $("#car_model").val(saved_car_model);
            }
        }
        $("#car_make").change(function(e){
            var car_make = $(this).val();
            $(this).val(car_make.toUpperCase());

        });
        $("#car_model").change(function(e){
            var car_model = $(this).val();
            $(this).val(car_model.toUpperCase());
        });
        $("#make_list").val(saved_car_make.toUpperCase());
        if(saved_car_make!=''){
            $("#car_make").css("display", "none");
        }


        $("#make_list").change(function(e){
            $("#car_make").val($(this).val());
            if($(this).val()==''){
                $("#car_make").css("display", "inline");

            }else{
                $("#car_make").css("display", "none");
               
            }
            reloadModelList($(this).val());
        });


        $("#model_list").change(function(e){
            $("#car_model").val($(this).val());
            if($(this).val()==''){
                $("#car_model").css("display", "inline");
            }else{
                $("#car_model").css("display", "none");
            }
        });
        $("#close").click(function(e){
            $.fancybox.close();
            //extractImageFromPdf();
        });
        //change Location
        $("#country").change(function(e){
            $("#country").val($(this).val());
            if($(this).val()=='KOREA'){
                $(".tax_price").css("display", "inline");
                $(".other_fee").css("display", "none");
                $(".refund_fee").css("display", "none");
            }else if($(this).val()=='JAPAN'){
                $(".tax_price").css("display", "none");
                $(".other_fee").css("display", "inline");
                $(".refund_fee").css("display", "inline");
               
            }else{
               $(".tax_price").css("display", "inline");
                $(".other_fee").css("display", "none");
                $(".refund_fee").css("display", "none");
            }
            //reloadModelList($(this).val());
        });

        $("#extract_file_0").click(function () {
            
            var input = document.getElementById('file_type_sheet0');
            file = input.files[0];
            if (file !=undefined){
                 formData = new FormData();
                 formData.append("pdffile",file);
                 formData.append("id", $("#idx").val());
                 $("#extract_pdf_loading").css("display", "block");
                 $.ajax({
                         url:"/?c=json&m=myjsonpost&mcd=product",
                         type:"POST",
                         data: formData,
                         processData:false,
                         contentType:false,
                         success: function(data,status){
                             //var getuploadvideo = data;
                             //var getdata = JSON.parse(data);
                             $('.upload_iframe').attr("src", $('.upload_iframe').attr("src"));
                             $("#extract_pdf_loading").css("display", "none");
                         }
                    
                 });
            }
            
            $.fancybox.close();
        }); 

        $(".datepicker-add-options").datepicker({
   
        });
        
    });    

    function reloadColorSelect(){
        $.ajax({
            method: "GET",
            url: "/?c=admin&m=adm_get_color_options"
            })
            .done(function( data ) {
                $("#car_color").html(data);
        });
        
    }

window.onload = function () {
    
    setInterval(function() {
        ResizeIframeFromParent('upload_iframe');
    }, 1000);
}
function ResizeIframeFromParent(id) {
    if (jQuery('#'+id).length > 0) {
        var window = document.getElementById(id).contentWindow;
        var prevheight = jQuery('#'+id).attr('height');
        var newheight = Math.max( window.document.body.scrollHeight, window.document.body.offsetHeight, window.document.documentElement.clientHeight, window.document.documentElement.scrollHeight, window.document.documentElement.offsetHeight );
        if (newheight != prevheight && newheight > 0) {
            jQuery('#'+id).attr('height', newheight);
            console.log("Adjusting iframe height for "+id+": " +prevheight+"px => "+newheight+"px");
        }
    }
}



  



</script>

      
