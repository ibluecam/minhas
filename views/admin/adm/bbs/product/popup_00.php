

<style type="text/css">
    .main {
    background: #aaa url(../images/bg.png) no-repeat;
    width: 500px;
    height: 250px;
    margin: 0px auto;
    padding: 0px;
    }
    .panel {
        background-color: #444;
        height: 34px;
        padding: 10px;
    }
    .panel a#status_pop, .panel a#shipment_pop {
        border: 2px solid #aaa;
        color: #fff;
        display: block;
        float: right;
        margin-right: 10px;
        padding: 5px 10px;
        text-decoration: none;
        text-shadow: 1px 1px #000;
     
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        -ms-border-radius: 10px;
        -o-border-radius: 10px;
        border-radius: 10px;
    }
    a#status_pop:hover, a#shipment_pop:hover {
        border-color: #eee;
    }
    .overlay {
        background-color: rgba(0, 0, 0, 0.6);
        bottom: 0;
        cursor: default;
        left: 0;
        opacity: 0;
        position: fixed;
        right: 0;
        top: 0;
        visibility: hidden;
        z-index: 1;
     
        -webkit-transition: opacity .5s;
        -moz-transition: opacity .5s;
        -ms-transition: opacity .5s;
        -o-transition: opacity .5s;
        transition: opacity .5s;
    }
    .overlay:target {
        visibility: visible;
        opacity: 1;
    }
    .popup {
        background-color: #fff;
        border: 3px solid #fff;
        display: inline-block;
        left: 50%;
        opacity: 0;
        padding: 15px;
        position: fixed;
        text-align: justify;
        top: 40%;
        visibility: hidden;
        z-index: 10;
     
        -webkit-transform: translate(-50%, -50%);
        -moz-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        -o-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
     
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        -ms-border-radius: 10px;
        -o-border-radius: 10px;
        border-radius: 10px;
     
        -webkit-box-shadow: 0 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
        -moz-box-shadow: 0 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
        -ms-box-shadow: 0 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
        -o-box-shadow: 0 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
        box-shadow: 0 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
     
        -webkit-transition: opacity .5s, top .5s;
        -moz-transition: opacity .5s, top .5s;
        -ms-transition: opacity .5s, top .5s;
        -o-transition: opacity .5s, top .5s;
        transition: opacity .5s, top .5s;
    }
    .overlay:target+.popup {
        top: 50%;
        opacity: 1;
        visibility: visible;
    }
    .close {
        background-color: rgba(0, 0, 0, 0.8);
        height: 30px;
        line-height: 30px;
        position: absolute;
        right: 0;
        text-align: center;
        text-decoration: none;
        top: -15px;
        width: 30px;
     
        -webkit-border-radius: 15px;
        -moz-border-radius: 15px;
        -ms-border-radius: 15px;
        -o-border-radius: 15px;
        border-radius: 15px;
    }
    .close:before {
        color: rgba(255, 255, 255, 0.9);
        content: "X";
        font-size: 24px;
        text-shadow: 0 -1px rgba(0, 0, 0, 0.9);
    }
    .close:hover {
        background-color: rgba(64, 128, 128, 0.8);
    }
    .popup p, .popup div {
        margin-bottom: 10px;
    }
    .popup label {
        display: inline-block;
        text-align: left;
        width: 120px;
    }
    .popup input[type="text"], .popup input[type="password"] {
        border: 1px solid;
        border-color: #999 #ccc #ccc;
        margin: 0;
        padding: 2px;
     
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        -ms-border-radius: 2px;
        -o-border-radius: 2px;
        border-radius: 2px;
    }
    .popup input[type="text"]:hover, .popup input[type="password"]:hover {
        border-color: #555 #888 #888;
    }

    .CSSTableGenerator {
    margin:0px;padding:0px;
    width:99%;
    box-shadow: 10px 10px 5px #888888;
    border:1px solid #000000;
    
    -moz-border-radius-bottomleft:0px;
    -webkit-border-bottom-left-radius:0px;
    border-bottom-left-radius:0px;
    
    -moz-border-radius-bottomright:0px;
    -webkit-border-bottom-right-radius:0px;
    border-bottom-right-radius:0px;
    
    -moz-border-radius-topright:0px;
    -webkit-border-top-right-radius:0px;
    border-top-right-radius:0px;
    
    -moz-border-radius-topleft:0px;
    -webkit-border-top-left-radius:0px;
    border-top-left-radius:0px;
}.CSSTableGenerator table{
    border-collapse: collapse;
        border-spacing: 0;
    width:100%;
    height:100%;
    margin:0px;padding:0px;
}.CSSTableGenerator tr:last-child td:last-child {
    -moz-border-radius-bottomright:0px;
    -webkit-border-bottom-right-radius:0px;
    border-bottom-right-radius:0px;
}
.CSSTableGenerator table tr:first-child td:first-child {
    -moz-border-radius-topleft:0px;
    -webkit-border-top-left-radius:0px;
    border-top-left-radius:0px;
}
.CSSTableGenerator table tr:first-child td:last-child {
    -moz-border-radius-topright:0px;
    -webkit-border-top-right-radius:0px;
    border-top-right-radius:0px;
}.CSSTableGenerator tr:last-child td:first-child{
    -moz-border-radius-bottomleft:0px;
    -webkit-border-bottom-left-radius:0px;
    border-bottom-left-radius:0px;
}.CSSTableGenerator tr:hover td{
    
}
.CSSTableGenerator tr:nth-child(odd){ background-color:#e5e5e5; }
.CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }.CSSTableGenerator td{
    vertical-align:middle;
    
    
    border:1px solid #000000;
    border-width:0px 1px 1px 0px;
    text-align:left;
    padding:7px;
    font-size:10px;
    font-family:Arial;
    font-weight:normal;
    color:#000000;
}.CSSTableGenerator tr:last-child td{
    border-width:0px 1px 0px 0px;
}.CSSTableGenerator tr td:last-child{
    border-width:0px 0px 1px 0px;
}.CSSTableGenerator tr:last-child td:last-child{
    border-width:0px 0px 0px 0px;
}
.CSSTableGenerator tr:first-child td{
        background:-o-linear-gradient(bottom, #cccccc 5%, #b2b2b2 100%);    background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #cccccc), color-stop(1, #b2b2b2) );
    background:-moz-linear-gradient( center top, #cccccc 5%, #b2b2b2 100% );
    filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#cccccc", endColorstr="#b2b2b2");  background: -o-linear-gradient(top,#cccccc,b2b2b2);

    background-color:#cccccc;
    border:0px solid #000000;
    text-align:center;
    border-width:0px 0px 1px 1px;
    font-size:14px;
    font-family:Arial;
    font-weight:bold;
    color:#000000;
}
.CSSTableGenerator tr:first-child:hover td{
    background:-o-linear-gradient(bottom, #cccccc 5%, #b2b2b2 100%);    background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #cccccc), color-stop(1, #b2b2b2) );
    background:-moz-linear-gradient( center top, #cccccc 5%, #b2b2b2 100% );
    filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#cccccc", endColorstr="#b2b2b2");  background: -o-linear-gradient(top,#cccccc,b2b2b2);

    background-color:#cccccc;
}
.CSSTableGenerator tr:first-child td:first-child{
    border-width:0px 0px 1px 0px;
}
.CSSTableGenerator tr:first-child td:last-child{
    border-width:0px 0px 1px 1px;
}
</style>


<?
        $idxs = '';

        $idxs = $_GET['idxs']; 

        $mcd = $_GET['mcd'];
/*
        $num = explode("," , $idxs);

        $a = implode(",", $num);*/

        //var_dump($mcd); exit();


        $sql = "SELECT * FROM iw_bbs WHERE menu_code = '$mcd' AND idx IN ($idxs)";
        $result = mysql_query($sql);
        //var_dump($sql); exit();

?>      

<html lang="en" >
    <head>
        <meta charset="utf-8" />
        <title>Shipment Multi-Insert</title>

        <script type="text/javascript" src="/js/common/common.js"></script>

    </head>
    <body>
        <header>
            <h2>Shipmement</h2>
            <span>Please Check the list of cars you selected</span>
        </header>
 
        <!-- panel with buttons -->
        <div class="main">
            <div class="panel">
                <a href="#car_status" id="status_pop">Status</a>
                <a href="#shipment_info" id="shipment_pop">Shipment</a>
            </div>
            

            <div class="CSSTableGenerator" >
                <table >
                    <tr>
                        <td>
                            RefNo
                        </td>
                        <td >
                            Car
                        </td>
                        <td>
                            ChassisNo
                        </td>
                    </tr>

                    <? while($rows = mysql_fetch_object($result)) { ?>
                        <tr>
                            <td >
                                <?= $rows->car_stock_no ?>
                            </td>
                            <td>
                                <?= $rows->car_model ?>
                            </td>
                            <td>
                                <?= $rows->car_chassis_no ?>
                            </td>
                        </tr>
                    <?  } ?>
                </table>
            </div>
            
        </div>
 
        <!-- popup form #1 -->
        <a href="#x" class="overlay" id="car_status"></a>
        <div class="popup">
            <h2>Car Status</h2>
            <p>Please select car status</p>
            <form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_bbs_ship_popup_update_exec&amp;mcd=<?= $mcd ?>&amp;idxs=<?= $idxs ?>" onsubmit=" ">
                <input type="hidden" name="popup_type" value="car_status" />
                <input type="hidden" name="idxs" value="<?= $idxs ?>" />
                    <div>
                            <input type="checkbox" name="values[icon_reserved_yn]" id="icon_reserved_yn" value="Y">Reserved <br/>
                            <input type="checkbox" name="values[icon_shipok_yn]" id="icon_shipok_yn" value="Y">ShipOK <br/>
                            <input type="checkbox" name="values[icon_releaseok_yn]" id="icon_releaseok_yn" value="Y">ReleaseOK <br/>
                            <input type="checkbox" name="values[icon_release_yn]" id="icon_release_yn" value="Y">Release <br/>
                    </div>


                    <input type="submit" value="save" name="submit" />
            </form>

            <a class="close" href="#close"></a>
        </div>
 
        <!-- popup form #2 -->
        <a href="#x" class="overlay" id="shipment_info"></a>
        <div class="popup">
            <h2>Shipment Info</h2>
            <p>Please enter your details here</p>

            <form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_bbs_ship_popup_update_exec&amp;mcd=<?= $mcd ?>&amp;idxs=<?= $idxs ?>" onsubmit=" ">
                <input type="hidden" name="popup_type" value="car_shipment" />
                <input type="hidden" name="idxs" value="<?= $idxs ?>" />
                <div>
                    <label for="customer">Customer</label>
                    <input type="text" id="customer" name="values[customer]" value="" />
                </div>
                <div>
                    <label for="pass">discharge</label>
                    <input type="text" id="discharge" name="values[discharge]" value="" />
                </div>
                <div>
                    <label for="firstname">PortLeavedDate</label>
                    <input type="text" id="port_leaved_date" name="values[port_leaved_date]" value="" class="" style="" readonly />
                    <a href="#" onclick="Calendar(document.adm_frm.elements[]); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
                </div>
                <div>
                    <label for="consignee_info">Consignee Info</label>
                    <input type="text" id="consignee_info" name="values[consignee_info]" value="" />
                </div>
                <input type="submit" value="save" name="submit" />&nbsp;&nbsp;&nbsp;or&nbsp;&nbsp;&nbsp;<a href="#car_status" id="status_pop">Status</a>
                
            </form>

            <a class="close" href="#close"></a>
        </div>
    </body>
</html>