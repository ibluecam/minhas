
<?php

    function getOrderByFieldUrl($fieldname, $default="DESC"){
        if(isset($_GET['order_by'])) $order_by = $_GET['order_by']; else $order_by='';
        if(isset($_GET['order_d'])) $order_d = $_GET['order_d']; else $order_d=$default;
        if($order_by==$fieldname){
            if($order_d!="DESC"){
                $getUrl = updateCurrentGetUrl(array("order_by"=>$fieldname, "order_d"=>"DESC"));
            }else{
                $getUrl = updateCurrentGetUrl(array("order_by"=>$fieldname, "order_d"=>"ASC"));
            }
        }else{
            $getUrl = updateCurrentGetUrl(array("order_by"=>$fieldname, "order_d"=>$default));
        }
        return $getUrl;
    }
    
     function updateCurrentGetUrl($p=array(), $unset_p=array()){
        $get = $_GET;
        if(count($unset_p)>0){
            foreach($unset_p as $value){
                if(isset($get[$value])) unset($get[$value]);
            }
        }
        if(count($p)>0){
            foreach($p as $key=>$value){
                $get[$key]=$value;
            }
        }
        $updatedUrl_arr = array();
        foreach($get as $key=>$value){
            $updatedUrl_arr[]=$key."=".$value;
        }
        $updatedUrl = implode("&", $updatedUrl_arr);
        return $updatedUrl;
    }

?>

<?php
   if(isset($_GET['sch_create_dt_s'])) $sch_create_dt_s=  htmlspecialchars($_GET['sch_create_dt_s']);else $sch_create_dt_s="";
   if(isset($_GET['sch_create_dt_e'])) $sch_create_dt_e=  htmlspecialchars($_GET['sch_create_dt_e']);else $sch_create_dt_e="";
   if(isset($_GET['sch_icon_status'])) $sch_icon_status=  htmlspecialchars($_GET['sch_icon_status']);else $sch_icon_status="";
   if(isset($_GET['sch_condition'])) $sch_condition=  htmlspecialchars ($_GET['sch_condition']);else $sch_condition="";
   if(isset($_GET['sch_word'])) $sch_word=  htmlspecialchars($_GET['sch_word']);else $sch_word="";
?>

<div class="contents_box_middle">
   <h1 class="title">My Cars</h1>
  
   <div class="tip_box">
      <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
      <ul>
         <li> - You can see the cars that you have reserved or bought here.</li>
      </ul>
   </div>
   <div class="table_list_box" style="padding-top:10px;">
      <h1 class="table_title">My Car List</h1>
      <div class="search_box">
         <div class="search_result">
            <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Total : <span class="title"><?= $total_rows ?></span> cars, Pages : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>
         </div>
         <div class="search_condition">
            <form name="bbs_search" id="adm_frm" method="get" action="" >
               <input type="hidden" name="c" value="<?= $c ?>">
               <input type="hidden" name="m" value="buyer_product">
              

               <input type="text" name="sch_create_dt_s" data-auto-close="true" class="datepicker-add-options"  value="<?php echo $sch_create_dt_s; ?>" data-date-format="yyyy-mm-dd" class="input_1" style="width:65px;height:21px;" />
               &nbsp;~
               <input type="text" data-auto-close="true" name="sch_create_dt_e" class="datepicker-add-options" data-date-format="yyyy-mm-dd" value="<?php echo $sch_create_dt_e; ?>" class="input_1" style="width:65px;height:21px;"  />
               <select name="sch_icon_status" id="sch_icon_status" class="input_2" style="width:150px;" align="absmiddle">
                  <option value="">CarStatus</option>
                   <?php foreach (Array('order', 'reserved','shipok','releaseok','soldout') as $Val) { ?>
                                        <option <?php echo $sch_icon_status=(isset($_GET['sch_icon_status']) && $_GET['sch_icon_status']=="$Val"? 'selected="selected"':''); ?> 
                                                    value="<?php echo $Val; ?>"><?php echo $Val; ?></option>
                        <?php } ?>

               </select>
               <select name="sch_condition" id="sch_condition" class="input_2" style="width:150px;" align="absmiddle">
                  <option value="">Default Car</option>
                  
                                  <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="car_chassis_no like"? 'selected="selected"':''  );
                                 ?>
                            value="car_chassis_no like">ChassisNO</option>
                                  
                                   <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="member_id like"? 'selected="selected"':''  );
                                 ?>
                            value="member_id like">Sales</option>
                                   
                                        <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="dhl_no like"? 'selected="selected"':''  );
                                 ?>
                            value="dhl_no like">DHL</option>
                                        
                                           <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="bl_no like"? 'selected="selected"':''  );
                                 ?>
                            value="bl_no like">BL</option>
                                           
                                           
                                           
                                           <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="car_model like"? 'selected="selected"':''  );
                                 ?>
                            value="car_model like">Car</option>
                                           
                                             <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="discharge like"? 'selected="selected"':''  );
                                 ?>
                            value="discharge like">Discharge</option>
                                             
                                                   <option  <?php 
                           echo $sch_condition=(isset($_GET['sch_condition']) && $_GET['sch_condition']=="car_stock_no like"? 'selected="selected"':''  );
                                 ?>
                            value="car_stock_no like">RefNo</option>
                                
                                
               </select>
               <input type="text" name="sch_word" id="sch_word" value="<?php echo $sch_word ?>" class="input_1" style="width:140px;height:19px;" title="검색어" />
               <input type="image" src="/images/admin/btn_search_01.gif" style="width:60px;" title="검색" align="absmiddle" />
               <input type="reset" value="Clear" style="background: #504f4b none repeat scroll 0 0;
                  border: medium none;border-radius: 0.3em;color: #fff;height: 25px;width: 62px;cursor: pointer;">
            </form>
         </div>
      </div>
      <div style="overflow:auto; float:left;width:100%;">
         <form name="adm_frm_2" id="adm_frm" method="post" action="">
           
            <table class="table_list" summary="" cellspacing="0">
               <caption class="hidden"></caption>
               <thead>
                  <tr>
                     <th scope="col"><input type="checkbox" name="idx" id="idx" class="idx_chk_all" value=""/></th>
                     <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                     <th scope="col">Reserved Date</th>
                     <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                     <th scope="col">Location</th>
                     <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                     <th scope="col">RefNo</th>
                     <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                     <th scope="col">Status</th>
                     <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                     <th scope="col">Car</th>
                     <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                     <th scope="col">ChassisNo</th>
                     <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                     <th scope="col">ModelYear</th>
                     <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                     <th scope="col">FOB PRICE</th>
                     <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                  </tr>
               </thead>
               <tbody>
                   
                   <?php
                     if(count($buyer_bbs_list)>0){
                         foreach ($buyer_bbs_list as $field){
                             
                        $bbs_link = "/?c=admin&m=buyer_bbs_info&idx=$field->idx&cur_page=$cur_page" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);
                   ?>
                  <tr>
                      <td><input type="checkbox" name="idx[]" id="idx" value="<?php echo $field->idx; ?>" class="idx"/>
                     </td>
                     
                     <td class="line">&nbsp;</td>
                     <td><a href="<?php echo $bbs_link ?>" style="display: block;"><?php echo date('Y-m-d', strtotime($field->reserved_dt));?></a></td>
                      <td class="line">&nbsp;</td>
                     <td><a href="<?php echo $bbs_link ?>" style="display: block;"><?php echo $field->country_name;?></a></td>
                     <td class="line">&nbsp;</td>
                     <td><a href="<?php echo $bbs_link ?>" style="display: block;"><?php echo $field->car_stock_no;?></a></td>
                     <td class="line">&nbsp;</td>
                     <td><a href="<?php echo $bbs_link ?>" style="display: block;"><?php echo ucfirst($field->icon_status);?></a></td>
                     <td class="line">&nbsp;</td>
                     <td><a href="<?php echo $bbs_link ?>" style="display: block;"><b><?php if($field->car_model_year==0){
                                echo $field->car_make.' '.$field->car_model; 
                           }                  
                           else{
                               echo $field->car_make .' '.$field->car_model.' '.$field->car_model_year;  
                           }
                        ?></b></a>
                        
                     </td>
                     <td class="line">&nbsp;</td>
                     <td><a href="<?php echo $bbs_link ?>" style="display: block;"><?php echo $field->car_chassis_no;?></a>
                     </td>
                     <td class="line">&nbsp;</td>
                     <td><a href="<?php echo $bbs_link ?>" style="display: block;">
                             <?php
                      if($field->car_model_year=='' || $field->car_model_year==0 || $field->car_model_year==NULL){
                          echo '';
                      }
                      else{
                         echo $field->car_model_year;   
                      }
                     ?>
                         </a>
                    
                     </td>
                     <td class="line">&nbsp;</td>
                     <td><a href="<?php echo $bbs_link ?>" style="display: block;">
                             <?php
                         if($field->car_fob_cost=='' || $field->car_fob_cost==0 || $field->car_fob_cost==NULL ){
                             echo 'Ask Price';
                         }
                         else{
                             echo $field->car_fob_currency .' '.number_format($field->car_fob_cost);
                         }
                         
                         ?>
                         </a>
                     </td>
                     <td class="line">&nbsp;</td>
                     
                     
                  </tr>
                  
                  <?php
                      }
                     }
                     else{
                         
                    
                  ?>
                  <?php
                    if($_SESSION['ADMIN']['business_type']=='buyer'){                                         
                  ?>
                
                  <tr>
		  <td colspan="20" class="no_data">You don't have any reserved car Yet.</td>
		 </tr>
                 <?php
                    }                                                       
                 ?>

                  <?php
                   }
                  ?>
               </tbody>
            </table>
      </div>
      <div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?'.updateCurrentGetUrl(array(), array('cur_page')), $mcd) ?></div>
      <div style="display:none;">
      <div id="excel_export_popup">
      <table style="width: 500px; border-collapse: collapse;">
      <tr>
      <th style="padding:10px" colspan="2">Please click the excel that you want to download</th>
      </tr><tr>
      <td style="padding:10px;border-right:1px solid gray; text-align:center"><a href="/?<?=$bbs_excel_customer_get ?>"><span class="button green">Stock List</span></a></td>
      <td style="padding:10px;text-align:center"><a href="/?<?=$bbs_excel_staff_get ?>"><span class="button red">Car List</span></a></td>
      </tr><tr>
      <td style="padding:10px;border-right:1px solid gray; text-align:center">This excel is for customer </td>
      <td style="padding:10px;text-align:center">This excel is for sales</td>
      </tr>
      </table>
      </div>
      </div>
      <div class="btn_area_center">
      <div class="btn_area_right">
      </div>
      </div>
      </form>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function() {
       
       $(".datepicker-add-options").datepicker({
           
       });
       
       $(".idx_chk_all").change(function(e){
        $(".idx").prop('checked', this.checked);
    });
   
   });
    
</script>