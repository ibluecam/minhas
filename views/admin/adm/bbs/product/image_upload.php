 <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Upload File</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	
	<link rel="stylesheet" type="text/css" href="/css/admin/upload_iframe.css" />
</head>

<body>
	<div id="mynewcontainer" class="upload_container">
		<div id="list">
			
		</div>
		<form id="image_form" name="adm_frm" action="/?c=admin&m=adm_image_upload_exec&idx=<?=$_GET['idx']?>&mcd=product" method="post" enctype="multipart/form-data">
			<input type="hidden" id="idx" value="<?=$_GET['idx']?>" name="idx"/>

			<input type="file" id="attach" name="attach1[]" multiple class="attach" accept=".jpg, .jpeg, .gif, .png"/>
			
			 <?php
		        $ifile = true;
		        $modi_att = count($bbs_attach_list);
		        if (count($bbs_attach_list) > 0) {
		        	
		            echo "<div class='div-img-title'";
		            
		            echo "</div>";
		            $first_primary=true;
		           	echo '<ul id="sortable">';
		            foreach ($bbs_attach_list as $att_rows) {
		            	echo "<li>";
		                echo "<div class='div-image-view'>";
		                echo "<img src='uploads/" . $mcd . "/" . $att_rows->raw_name . "_" . func_get_config($bbs_config_result, 'attach_thumbnail_size3') . $att_rows->file_ext . "' width='150' >";
		                ?>
		                	<a href="#" class="close" onClick="delete_bbs_attach(<?php echo $att_rows->att_idx; ?>,<?php echo $att_rows->idx; ?>, '<?php if($att_rows->sort==0&&$first_primary) echo "deny"; else echo "allow";  ?>')" title="Delete"></a>
		                	<label class="label_primary"><input type="radio" value="<?=$att_rows->att_idx ?>" class="primary" name="primary" <?php if($att_rows->sort==0&&$first_primary) echo "checked";  ?>/><span>Primary</span></label>
		                	<!-- <div class="nav-container" style="float:right"><input type="button" class="nav-left" data-nav="left" data-sort="<?=$att_rows->sort ?>" data-att-idx="<?=$att_rows->att_idx ?>"/><input type="button" data-nav="right" class="nav-right" data-sort="<?=$att_rows->sort ?>" data-att-idx="<?=$att_rows->att_idx ?>"/></div> -->
		                	<input type="hidden" class="sort_value" name="sort[<?=$att_rows->att_idx ?>]" value="<?=$att_rows->sort ?>">
		                <?php
		                echo "</div>";
		                if($att_rows->sort==0) $first_primary=false; 
		                echo "</li>";
		            }
		           	
		        } else {
		            
		        }?>
			
			 <?php 
			 // if (count($bbs_attach_list) > 0) {
			 // 	var_dump($bbs_attach_list);
			 // }
			 ?>
			 </ul>
		</form>
	</div>

	<script>
		var orderChange = false;
		var invalid_exts = [];
		$(document).ready(function(e){
			
			
			$(".attach").on("change", function(e){
				handleFileSelect();

				if(invalid_exts.length>0){
					ext_string = invalid_exts.join(', *.');
					alert("Files with extension: *."+ext_string+" are not allowed. \nPlease try again with *.jpg, *.jpeg, *.gif, or *.png.");
					$(".attach").val('');
				}else{
					$("form").submit();
				}
		   		
			});
			//alert($( document ).height());
			$( "#sortable" ).sortable({
				change: function(event, ui){
					orderChange = true;
				},
				stop: function(event, ui){
					refreshOrder();
				}
			});
			$( "#sortable" ).disableSelection();
			window.parent.resizeIframe($( document ).height());
			$(".primary").change(function(e){
				var idx = $("#idx").val();
				var att_idx = $(this).val();
				var data = {
					idx: idx,
					att_idx: att_idx,
					sort:0
				}
				$.ajax({
					type: "POST",
					url: "/?c=admin&m=adm_set_primary_image",
					data: data,
					success: function(data){
						document.adm_frm.submit();
					},
					dataType: "html"
				});
			});
			$(".nav-left, .nav-right").click(function(e){
				var nav = $(this).attr("data-nav");
				var idx = $("#idx").val();
				var att_idx = $(this).attr("data-att-idx");
				var sort = $(this).attr("data-sort");

				var data = {
					idx: idx,
					att_idx: att_idx,
					nav:nav,
					sort: sort
				}
				$.ajax({
					type: "POST",
					url: "/?c=admin&m=adm_set_image_sort",
					data: data,
					success: function(data){
						document.adm_frm.submit();
					},
					dataType: "html"
				});
			});
		});
		
 		function refreshOrder(){
 			var i=0;
 			if(orderChange==true){
	 			$(".sort_value").each(function(e){	
	 				$(this).val(i);
	 				i++;
	 			});
	 			orderChange = false;
	 			sort_all_item_images();
	 		}
 		}
 		function sort_all_item_images(){
 			var f = $("#image_form");
 			var form_data = f.serialize();
	       	$.ajax({
				type: "POST",
				url: "/?c=admin&m=adm_set_all_image_sort",
				data: form_data,
				success: function(data){
					resetPrimaryRadio();
				},
				dataType: "html"
			});
	        
 		}
	    function delete_bbs_attach(att_idx, idx, permission) {
	        var f = document.adm_frm;
	        var x;
	       	if(permission=='allow'){
		        if (confirm('Do you want to delete image ID (' + idx + ' )') == true) {
		            f.action = '?c=admin&m=adm_delete_attach&mcd=<?php echo $mcd; ?>&only_attach=Y&idx=' + idx + '&att_idx=' + att_idx;
		            f.submit();
		           
		        } else {
		            x = "You pressed Cancel!";
		        }
		    }else{
		    	alert("Cannot delete primary photo. To delete this, please set another photo to be primary first.");
		    }
	    }
		function delete_bbs_attach_files(idx,bbs_idx) {
	        var f = document.adm_frm;
	        var x;
	        if (confirm('Do you want to delete image ID (' + bbs_idx + ' )') == true) {
	            f.action = '?c=admin&m=adm_bbs_modify&mcd=<?php echo $mcd; ?>&only_attach=Y&idx=<?=$idx?>&id=' + idx + '&bbs_idx=' + bbs_idx;
	            f.submit();
	        } else {
	            x = "You pressed Cancel!";
	        }

	    }
	    function resetPrimaryRadio(){
	    	$("#sortable li:first-child .primary").prop( "checked", true );
	    }

	   	function handleFileSelect() {
	   		//var files = evt.target.files; // FileList object
		    var files = $("#attach")[0].files; // FileList object
		    var allowed_ext = ['jpg', 'jpeg', 'png', 'gif'];
		    invalid_exts =[];
		    // files is a FileList of File objects. List some properties.
		    for (var i = 0, f; f = files[i]; i++) {

		    	var filename = f.name;
		    	var ext = filename.substr( (filename.lastIndexOf('.') +1) );
		    	ext = ext.toLowerCase();
		      	//$("#list").append(ext);
		      	if($.inArray(ext, allowed_ext)===-1){
		      		if($.inArray(ext, invalid_exts)===-1){
		      			invalid_exts.push(ext);
		      		}
		      	}
		    }
		    
		  }



		  
	</script>
</body>

</html> 
