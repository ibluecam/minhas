<script>
	function delete_bbs_attach(att_idx,idx) {
		var f  = document.adm_frm;
		var x;
		if (confirm('Do you want to delete image ID ('+ idx +' )') == true) {
			f.action = '?c=admin&m=adm_bbs_modify&mcd=<?php echo $mcd;?>&only_attach=Y&idx='+idx+'&att_idx='+ att_idx;
			f.submit();	
		} else {
			x = "You pressed Cancel!";
		}
		
	}

</script>
<style type="text/css">
.min_menu li{
    display: inline-block;
}
.button-small{
    color: #fff !important;
    word-spacing: 0.25em;
   font-family: 'Open Sans', sans-serif;
   text-transform: uppercase;
   border: none;
    /*line-height: 22px;
    padding: 12px 13px 11px;*/
    padding: 10px 13px 7px 13px;
    text-align: center;
    display: inline-block;
    margin-top: 5px;
    margin-bottom: 18px;
    text-decoration: none;
}
.button-small:visited{
      color: #fff;
}
#btnactive {
    text-decoration: none;
    background-color: #78BC97;
    -webkit-box-shadow: inset 0 -3px 0px #A3CFB6, inset 0 20px 0px #78BC97;
    -moz-box-shadow: inset 0 -3px 0px #A3CFB6, inset 0 20px 0px #78BC97;
    box-shadow: inset 0 -3px 0px #A3CFB6, inset 0 20px 0px #78BC97; 
    border-bottom: solid 1px #78BC97;
}
.rounded3 {
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
.grey_dark {
    background: #5C6165;
    width: 100px;
    font-weight: bolder;   
    -webkit-box-shadow: inset 0 -3px 0px #848181, inset 0 20px 0px #5C6165;
    -moz-box-shadow: inset 0 -3px 0px #848181, inset 0 20px 0px #5C6165;
    box-shadow: inset 0 -3px 0px #848181, inset 0 20px 0px #5C6165; 
    border-bottom: solid 1px #5C6165;
}
#notspace{
    padding: 0px;
    margin: 0px;
}

</style>

 
<?php 
if (count($bbs_detail_list) > 0) {
foreach($bbs_detail_list->result() as $rows){
	$inImageID= array();
	
?>
<form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="?c=admin&m=adm_bbs_modify&mcd=<?php echo $mcd; ?>&category=offer&idx=<?php echo $rows->idx; ?><?php ?>" onsubmit="return insert_adm_bbs(this);">
<input type="hidden" name="values[category]" value="<?php if( isset($_REQUEST['category'])){ echo $_REQUEST['category'];}else {echo '' ;} ?>">	
    <div class="contents_box_middle">
    	<?php
                if($_SESSION['ADMIN']['session_grade_no'] !='11' ){
              ?>
        <h1 class="title">Modify</h1>
              
		<div class="tip_box">

			<p class="title"><img src="images/admin/tip_img.gif" alt="tip" /></p>

			<ul class="min_menu">

				<li> - Modify </li>
                <li><span class="button-small grey_dark rounded3" <?php if($rows->icon_status=='sale'){echo 'id="btnactive"';} ?> >Sale</span></li>
                <li id="notspace"><img style="margin-bottom: -6px" src="images/admin/symbol-next.png"></li>
                
                <li><span class="button-small grey_dark rounded3" <?php if($rows->icon_status=='reserved'){echo 'id="btnactive"';} ?> href="#">Reserved</span></li>
                <li id="notspace"><img style="margin-bottom: -6px" src="images/admin/symbol-next.png"></li>
                <li><span class="button-small grey_dark rounded3" <?php if($rows->icon_status=='shipok'){echo 'id="btnactive"';} ?> href="#">Ship Ok</span></li>
                <li id="notspace"><img style="margin-bottom: -6px" src="images/admin/symbol-next.png"></li>
                <li><span class="button-small grey_dark rounded3" <?php if($rows->icon_status=='releaseok'){echo 'id="btnactive"';} ?> href="#">Release Ok</span></li>
                <li id="notspace"><img style="margin-bottom: -6px" src="images/admin/symbol-next.png"></li>
                <li><span class="button-small grey_dark rounded3" <?php if($rows->icon_status=='soldout'){echo 'id="btnactive"';} ?> href="#">Sold Out</span></li>
			</ul>

		</div>
        <?php
                }
        ?>
        <?php if($rows->icon_status=='order'){ ?>
            <div class="order_container">
                Ordered by <a class="fancybox iframe" href="/?c=admin&amp;m=adm_order_list_popup&amp;mcd=<?= $mcd ?>&amp;idx=<?= $rows->idx ?>" title="등록"><?=$count_order?> users</a>
            </div>
        <?php } ?>

        <?php if(!empty($rows->customer_no)){ ?>
            <div class="customer_container">
                <h1 class="table_title">Customer: </h1>
                <?php 
                 if($_SESSION['ADMIN']['session_grade_no'] !='11' ){
                ?>
                <div class="customer_name"><a href="/?c=admin&m=adm_customer_view&mcd=&member_no=<?php echo $rows->customer_no; ?>&cur_page=1&sParam="><?php echo $rows->c_first_name." ".$rows->c_last_name; ?> (<?= $rows->icon_status ?>)</a> </div>
                <?php
                  }
                  else{
                ?>
                <label class="customer_name"><?php echo $rows->c_first_name." ".$rows->c_last_name; ?> (<?= $rows->icon_status ?>)</label>
                <?php
                 }
                ?>
            </div>
        <?php } ?>
		<div class="table_write_box">
        <h1 class="table_title"><?= func_get_config($bbs_config_result, 'bbs_code_name') ?></h1>
			<table class="table_write" summary="<?php echo func_get_config($bbs_config_result, 'bbs_code_name'); ?>" border="0" cellspacing="0" width="100%">
                <caption></caption>
                <colgroup>
                    <col width="15%" />
                    <col width="35%" />
                    <col width="15%" />
                    <col width="35%" />
                </colgroup>
                
                <?
				//카테고리 사용 여부
				if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {
					$category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));
					$category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));
				}
				?>
                	<tr><td colspan="4" class="text_info">STOCK NO. <?=$rows->car_stock_no?></td></tr>
                    
                     <tr>
                         
                          <th scope="row">Owner</th>
                        <td class="text"><p>

                            <?php 
                              if(count($bbs_detail_list_get_owner)>0){
                                  foreach ($bbs_detail_list_get_owner as $row){
                                      
                                if($row->member_no == $rows->car_owner){
                                    
                               echo $row->company_name;
                                       
                                          }
                                  }
                              }
                            ?>
                            </p></td>
                        <th scope="row">Car Dimension (L×W×H) mm</th>
                        <td class="text"><p>
                            <label>
                                    <?php
                                       if($rows->car_length=='' || $rows->car_length==0 || $rows->car_length==NULL){
                                           echo '';
                                       }
                                       else{
                                           echo number_format($rows->car_length) .' x';
                                       }
                                    ?>
                                </label>
                                <label>
                                    <?php
                                       if($rows->car_width=='' || $rows->car_width==0 || $rows->car_width==NULL){
                                           echo '';
                                       }
                                       else{
                                           echo number_format($rows->car_width) .' x';
                                       }
                                    ?>
                                </label>
                                <label>
                                    <?php
                                       if($rows->car_height=='' || $rows->car_height==0 || $rows->car_height==NULL){
                                           echo '';
                                       }
                                       else{
                                           echo number_format($rows->car_height);
                                       }
                                    ?>
                                </label>
                            </p></td>

                    </tr>
                        
                        
                    <tr>
                         
                        <th scope="row">Car Condition</th>
                        <td class="text">
                          <p>
                            <?php
                              if($rows->icon_new_yn=='Y'){
                                echo 'New';
                              }elseif($rows->icon_new_yn=='N'){
                                echo 'Used';
                              }
                            ?>
                          </p>
                        </td>
                        <th scope="row">Steering</th>
                        <td class="text">
                            <?php 
                              if($rows->car_steering=='' || $rows->car_steering=='0'){
                                  echo '';
                              }
                            else {
                            ?>
                            <p>
                                <?php
                                  echo ($rows->car_steering =='LHD') ? 'Left-hand driving' : 'Right-hand driving' ;
                                ?>
                            </p>
                            <?php 
                              }
                            ?>
                        </td>
                    </tr>    
                        
                        
                    <tr>
                        <th scope="row">Location</th>
                        <td class="text"><p><?= $rows->country_name.($rows->car_city!=''? '&nbsp;/ City : &nbsp;'.$rows->car_city:''); ?></p></td>

                        <th scope="row">CHASSIS NO.</th>
                        <td class="text">
                            <p><?php echo $rows->car_chassis_no; ?></p>
                        </td>
    
                    </tr>
                     <?php
                            if( $_SESSION['ADMIN']['business_type'] !='seller' ){
                          ?>
					
                    <?php
                            }
                    ?>
                    <tr>
                        <th scope="row">Car Status</th>
                        <td class="text">
                            <!-- input type="checkbox" name="values[icon_new_yn]" value="Y" NotValue="N"> <img src="/images/user/new_icon.gif" -->
                            <p><?=$rows->icon_status?></p>
                        </td>
                        <th scope="row">DOORS</th>
                        <td class="text">
                            <p><?php echo $rows->door.' DOORS (only number)'; ?></p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">MAKE</th>
                        <td class="text">
                        	<p><?php echo $rows->car_make; ?></p>
                        </td>
                        <th scope="row">MODEL</th>
                        <td class="text">
                            <?php 
                               if($rows->car_model=='' ||  $rows->car_model=='0'){
                                   echo '';
                               }
                            else {
                            ?>
                            <p><?php echo $rows->car_model; ?><?php } ?></p>
                        </td>
                    </tr>
    
                    <tr>
                        <th scope="row">GRADE</th>
                        <td class="text">
                            <p><?php echo $rows->car_grade; ?></p>
                        </td>
    
                        <th scope="row">ENGINE SIZE</th>
                        <td class="text">
                            <?php
                              if($rows->car_cc=='' || $rows->car_cc=='0'){
                                  echo '';
                              }
                            else {
                            ?>
                            <p><?php echo $rows->car_cc; ?> CC (only number)</p><?php } ?> </td>
                    </tr>
    
                    <tr>
                        <th scope="row">Registration Date ddd</th>
                        <td class="text">
                        <p>
                            <?php
                              if($rows->first_registration_month =='0'  && $rows->first_registration_year =='0'){
                               echo '';
                              }
                              else if($rows->first_registration_month =='0' && $rows->first_registration_year !='0') {
                                echo $rows->first_registration_year.' Year';
                              }
                               else if($rows->first_registration_month !='0' && $rows->first_registration_year =='0') {
                                echo date('F', mktime(0, 0, 0, $rows->first_registration_month, 1));
                              }
                              else{
                                echo date('F', mktime(0, 0, 0, $rows->first_registration_month, 1)).' / '.$rows->first_registration_year.' Year';
                              }
                             
                           ?>
                        </p>
                       
                        </td>
                        
                        <th scope="row">MANUFACTURED</th>
                        <td class="text">
                            <?php  
                            
                              if($rows->manu_year=='' || $rows->manu_year=='0'){
                                  echo '';
                              }
                             else {
                            ?>
                            <p><?php echo $rows->manu_year; ?> Year (only number)</p><?php } ?>
                        </td>
                       
                    </tr>
                    <tr>
                        <th scope="row">MODEL YEAR</th>
                        <td class="text">
                            <?php 
                               if($rows->car_model_year=='' || $rows->car_model_year=='0'){
                                   echo '';
                               }
                              else {
                            ?>
                            <p><?php echo $rows->car_model_year; ?> Year (only number)<?php } ?></p>
                        </td>
    					<th scope="row">SEATS</th>
                        <td class="text">
                            <?php
                              if($rows->car_seat=='' || $rows->car_seat=='0'){
                                  echo '';
                              }
                             else {
                            ?>
                            <p><?= $rows->car_seat?> SEATS (only number)</p><?php } ?></td>
                    </tr>
                    <tr>
                        <th scope="row">MILEAGE</th>
               
                        <td class="text">
                             <?php 
                           if($rows->car_mileage=='' || $rows->car_mileage=='0'){
                               echo '';
                           }
                         else {
                        ?>
                            <p><?php echo $rows->car_mileage; ?> Km (only number)<?php } ?></p></td>
                        <th scope="row">COLOR</th>
                        <td class="text" colspan="3">
                            <?php 
                              if($rows->car_color=='' || $rows->car_color=='0'){
                                  echo '';
                              }
                             else {
                            ?>
                         <p><?= $rows->car_color ?></p><?php } ?></td>
                    </tr>
                    <tr>
                        <th scope="row">BODY TYPE</th>
                        <td class="text">
                           <?php 
                              if( $rows->car_body_type=='' ||  $rows->car_body_type=='0' ){
                                  echo '';
                              }
                              else {
                           ?>
                            <p><?= $rows->car_body_type ?></p><?php } ?></td>
                        <th scope="row">FUEL</th>
                        <td class="text">
                         <?php
                            if($rows->car_fuel=='' || $rows->car_fuel=='0'){
                                echo '';
                            }
                         else {
                         ?>
                        <p><?= $rows->car_fuel ?></p><?php } ?></td>
                    </tr>
                    <tr>
                    	<th scope="row">TRANSMISSION</th>
                        <td class="text">
                            <?php 
                            if($rows->car_transmission=='' || $rows->car_transmission=='0'){
                                echo '';
                            }
                        else {
                                    
                             ?>
                            <p><?= $rows->car_transmission ?></p><?php } ?></td>
                        
                    	<th scope="row">DRIVE TYPE</th>
                        <td class="text">
                            <?php 
                              if($rows->car_drive_type=='' || $rows->car_drive_type=='0'){
                                  echo '';
                              }
                               else {
                            ?>
                            <p><?=$rows->car_drive_type?><?php } ?></p>
                        </td>
                    </tr>
                    
                    <!-- <tr>
                        <th scope="row">OPTION</th>
                        <td class="text" colspan="3">
                            <div class="check">  -->

                            <? 
                                    // $str_option = array( );
                                    // $str_option = explode("^",$rows->car_options); 

                                    // for($i=0 ; $i<count($str_option) ; $i++)
                                    // {
                                    //     echo $str_option[$i];
                                    //     echo '&nbsp;&nbsp;/&nbsp;&nbsp;';
                                    // }

                            ?>

							<!-- <table>
                            	<tr>
                                	<td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="ABS" <?php echo func_checkbox_validate($rows->car_options,"ABS" );?>/>ABS</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Navigation" <?php echo func_checkbox_validate($rows->car_options,"Navigation" );?>/>Navigation</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Roof rack" <?php echo func_checkbox_validate($rows->car_options,"Roof rack" );?>/> Roof rack</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Airbag(driver side)" <?php echo func_checkbox_validate($rows->car_options,"Airbag(driver side)" );?>/>Airbag(driver side)</td>
                                </tr>
                                <tr>
                                	<td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Power Door lock" <?php echo func_checkbox_validate($rows->car_options,"Power Door lock" );?>/>Power Door lock</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Smart key" <?php echo func_checkbox_validate($rows->car_options,"Smart key" );?>/>Smart key</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Airbag(passenger side)" <?php echo func_checkbox_validate($rows->car_options,"Airbag(passenger side)" );?>/>Airbag(passenger side)</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Power seat(Driver side)" <?php echo func_checkbox_validate($rows->car_options,"Power seat(Driver side)" );?>/>Power seat(Driver side)</td>
                                </tr>
                                <tr>
                                	<td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Sunroof" <?php echo func_checkbox_validate($rows->car_options,"Sunroof" );?>/>Sunroof</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled"  value="Rear camera" <?php echo func_checkbox_validate($rows->car_options,"Rear camera" );?>/>Rear camera</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="USB jack" <?php echo func_checkbox_validate($rows->car_options,"USB jack" );?>/>USB jack</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Rear spoiler" <?php echo func_checkbox_validate($rows->car_options,"Rear spoiler" );?>/>Rear spoiler</td>
                                </tr>
                                <tr>
                                	<td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="VDC" <?php echo func_checkbox_validate($rows->car_options,"VDC" );?>/>VDC</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Alloy wheels" <?php echo func_checkbox_validate($rows->car_options,"Alloy wheels" );?>/>Alloy wheels</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Back warning system" <?php echo func_checkbox_validate($rows->car_options,"Back warning system" );?>/>Back warning system</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="CD player" <?php echo func_checkbox_validate($rows->car_options,"CD player" );?>/>CD player</td>
                                </tr>
                                <tr>
                                	<td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Curtain airbag" <?php echo func_checkbox_validate($rows->car_options,"Curtain airbag" );?>/>Curtain airbag</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Dual Sunroof" <?php echo func_checkbox_validate($rows->car_options,"Dual Sunroof" );?>/>Dual Sunroof</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Full auto airconditioner" <?php echo func_checkbox_validate($rows->car_options,"Full auto airconditioner" );?>/>Full auto airconditioner</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Russia" <?php echo func_checkbox_validate($rows->car_options,"Russia" );?>/>Heating</td>
                                </tr>
                                <tr>
                                	<td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Leather seats" <?php echo func_checkbox_validate($rows->car_options,"Leather seats" );?>/>Leather seats</td>
                                    <td><input type="checkbox" name="op_checkbox[]" disabled="disabled" value="Monitor" <?php echo func_checkbox_validate($rows->car_options,"Monitor" );?>/>Monitor</td>
                                </tr>
                            </table> -->
                           <!--  </div>
                        </td>
                    </tr> -->
    
                    <tr>
                        <th scope="row">FOB COST</th>
                        <td class="text">
                            <?php if($rows->fob=='' ||$rows->fob=='0'  ){
                                echo 'Ask Price';
                            }else {
                                echo $rows->fob .' '.$rows->car_fob_currency;}?> 
                        </td>
                        <th scope="row">OLD FOB COST</th>
                        <td class="text">
                            <?php if(!empty($rows->old_fob_cost)){
                                echo '<strike style="color:red;">'.$rows->old_fob_cost.' '.$rows->car_fob_currency."</strike>"; 
                              }
                            ?> 
                        </td>
                        <? //$actual_selling_price = $rows->car_actual_price + $rows->car_freight_fee; ?>
                                
                    </tr>
<?php
                            if( $_SESSION['ADMIN']['business_type'] !='seller' ){
                          ?>
                    
                    
                    <?php
                            }
                    ?>

                    <tr>
                    	<th scope="row">Files</th>
                        <td class="text" colspan="3"><p><?=func_create_attach_files_name($bbs_attach_files,'carItem','N','Y','','Y','','N')?></p></td>
                    </tr>
                    <tr>
                         
                        <th scope="row">Description</th>
                        <td class="text" colspan="3" ><p><?=$rows->contents;?></p></td>

                    </tr> 
                    <tr>
                            <th scope="row">Images</th>
                            <td class="text" colspan="3">
                            <?php 
                                if (count($select_bbs_cloud_list) > 0) {
                                  foreach ($select_bbs_cloud_list as $att_rows) {
                                    echo "<img src='" . $att_rows->base_url . "thumb/".$att_rows->public_id."' width='150' style='padding:4px' >";
                                  }
                                }
                              ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            <div class="btn_area_center">
       
        <a title="edit" href="?c=admin&m=adm_bbs_modify&mcd=<?php echo $mcd; ?>&category=offer&idx=<?php echo $_GET['idx']; ?>"><span class="button blue">Edit</span></a>
			<!-- 	<a href="?c=admin&amp;m=adm_bbs_list&amp;mcd=<?php echo  $mcd; ?>" title="List"><img src="images/admin/list.gif" alt="목록" /></a> -->
        <a title="list" href="?c=admin&amp;m=adm_bbs_list&amp;mcd=<?php echo  $mcd; ?>">
<span class="button blue">Back</span>
</a>
			</div>

		</div>
	</div>
</form>
<?php
	}
}
?>
<script>
    $(document).ready(function(e){
        $("a.fancybox").fancybox({
            width: 800,
            height: "100%"
        });
        
    });
</script>