<?php $admin_group = array('1', '2', '3'); 

?>

<script>
       
       /*$(document).on("click","select[name*='carmake']",function() {
	
               $("#carmodel").empty();
               $("#carmodel").append("<option value>Model</option>");


                  var car_make=$(this).val();
				  var base_url = "<?php echo base_url(); ?>";
                  var current_url=$('#base_url').val();
                  $.getJSON('/?c=car_model&m=get_car_model&car_make='+car_make,function(json){
                      $("select[name*='carmodel']").html("");
                      $( "select[name*='carmodel']").append('<option value="">Model</option>');
                    $.each(json,function(i,data_json){

                      respone_data="<option title='" + data_json.car_model + "' value='" + data_json.car_model + "'>" + data_json.car_model + "</option>";
                      $( "select[name*='carmodel']").append(respone_data);

                    });

                  });


                });*/

</script>
<?php 
    
    // $get_export = $_GET;
    // $get_export['c'] = "admin";
    // $get_export['m'] = "adm_bbs_export_excel";
    // $requests_export = array();
    // foreach($get_export as $key=>$value){
    //     $requests_export[]=$key."=".$value;
    // }
    // $export_excel_url = implode("&", $requests_export);
    
    function updateCurrentGetUrl($p=array(), $unset_p=array()){
        $get = $_GET;
        if(count($unset_p)>0){
            foreach($unset_p as $value){
                if(isset($get[$value])) unset($get[$value]);
            }
        }
        if(count($p)>0){
            foreach($p as $key=>$value){
                $get[$key]=$value;
            }
        }
        $updatedUrl_arr = array();
        foreach($get as $key=>$value){
            $updatedUrl_arr[]=$key."=".$value;
        }
        $updatedUrl = implode("&", $updatedUrl_arr);
        return $updatedUrl;
    }
    function getOrderByFieldUrl($fieldname, $default="DESC"){
        if(isset($_GET['order_by'])) $order_by = $_GET['order_by']; else $order_by='';
        if(isset($_GET['order_d'])) $order_d = $_GET['order_d']; else $order_d=$default;
        if($order_by==$fieldname){
            if($order_d!="DESC"){
                $getUrl = updateCurrentGetUrl(array("order_by"=>$fieldname, "order_d"=>"DESC"));
            }else{
                $getUrl = updateCurrentGetUrl(array("order_by"=>$fieldname, "order_d"=>"ASC"));
            }
        }else{
            $getUrl = updateCurrentGetUrl(array("order_by"=>$fieldname, "order_d"=>$default));
        }
        return $getUrl;
    }
    
    $bbs_excel_customer_get = updateCurrentGetUrl(array("c"=>"admin", "m"=>"adm_bbs_excel_customer"));
    $bbs_excel_staff_get = updateCurrentGetUrl(array("c"=>"admin", "m"=>"adm_bbs_excel_staff"));


?>

<script type="text/javascript">
$(document).ready(function($) {
    $("a.fancybox").colorbox({
        inline: true, 
        href: '#excel_export_popup'
    });

    // $("a.fancybox").fancybox({
    //         height: "35%",
    //         width: "25%",
    //         onClosed: function() {   
    //             parent.location.reload(true); 
    //         }
    //     });
    $(".idx_chk_all").change(function(e){
        $(".idx").prop('checked', this.checked);
    });
    $("#reserve_btn").click(function(e){
        var checked_count = $(".idx:checked").length;
        var idx = $(".idx:checked").val();
        var car_status = $(".idx:checked").attr("data-status");
        if(checked_count==1){

            if(car_status == 'sale') {

                reserveCar(idx);
            }else{
                alert("This car has been reserved already. Please changed status to sale before reserved.");
            }

        }else if(checked_count>1){
            alert("Please select only 1 item to reserve.");
        }else{
            alert("Please select an item to reserve.")
        }
    });
});
function reserveCar(idx){
    window.open('/?c=admin&m=adm_reserve_popup_update&mcd=<?=$mcd?>&idx='+idx, '_blank'); 
    window.location.replace('?c=admin&m=adm_bbs_detail&mcd=<?php echo $mcd; ?>&idx=' + idx);
    
}
    //게시판 체크박스 전체 선택
    // function checkbox_all_check(obj, target) {
    //     if (typeof obj == 'undefined' || typeof target == 'undefined') {

    //     }
    //     else if (typeof target.length == 'undefined') {
    //         target.checked = obj.checked;
    //     }
    //     else
    //     {
    //         for (i = 0; i < target.length; i++) {
    //             target[i].checked = obj.checked;
    //         }
    //     }
    // }

    //선적등록
    function shipment_all() {
        var f = document.adm_frm_2;
        var fg = false;
        check_count = 0;
        var idxs = new Array();

        if (f.idx != null) {

            for (i = 0; i < f.idx.length; i++) {

                if (f.idx[i].checked) {
                    
                    fg = true;

                    idxs[i] =" \' " + f.idx[i].value + " \' ";
                    
                    var car_status = $(".idx:checked").attr("data-status");

                    if(car_status != 'reserved'){

                        alert('Please select reserved cars. If this car is not reserved yet, make this car reserved first.');
                        //window.location.reload();
                        return;
                    }

                    check_count++;

                } else {
                    idxs[i] = " \' \' ";
                }
            }

            // alert(idxs); 

            if (check_count > 0) {
                var idxss = new Array();
                var index = 0;
                for(var i = 0; i<idxs.length; i++) {

                    if(idxs[i] !=" \' \' ") {
                        idxss[index] = idxs[i];
                        index++;
                    }
                }
                
                window.open('/?c=admin&m=adm_bbs_ship_popup_update&mcd=<?= $mcd ?>&idxs=' + idxss, '_blank');

                return;
            }

            if (check_count == 0) {
                alert('Please select car to insert shipment info.');
                return;
            }


/*            f.action = '/?c=admin&m=adm_bbs_ship_update&mcd=<?= $mcd ?>&idx=' + f.idx;
            f.submit();*/
        }
    }


    //전체 삭제
    // function delete_all() {
    //     var f = document.adm_frm_2;
    //     var fg = false;

    //     if (f.idx != null) {
    //         for (i = 0; i < f.idx.length; i++) {
    //             if (f.idx[i].checked) {
    //                 fg = true;
    //             }
    //         }

    //         if (!fg) {
    //             alert('Please select cars that you want to delete.');
    //             return;
    //         }

    //         if (!confirm('Do you want to delete this car?'))
    //         {
    //             return;

    //         }else{

    //             f.action = '/?c=admin&m=adm_bbs_all_delete&mcd=<?= $mcd ?>';
    //             f.submit();
    //         }


    //     }
    // }

    //function modeify

    function modify_check() {
        var f = document.adm_frm_2;
        check_count = 0;
        var checked_idx=-1;
        if (f.idx != null) {

            for (i = 0; i < f.idx.length; i++) {

                if (f.idx[i].checked) {
                    checked_idx = i;
                    check_count++;
                }
            }
            //alert(checked_idx);
            if (check_count == 0) {
                alert('Please select car to modify info.');
                return;
            } else if (check_count > 1) {
                alert('Please select only one car to modify info.');
                return;

            } else {
                f.address.value=document.URL;
                f.method = "post";
                f.action = '?c=admin&m=adm_bbs_modify&mcd=<?php echo $mcd; ?>&category=offer&idx=' + f.idx[checked_idx].value;
                f.submit();
            }
        }
    }








     function modify_sale_check() {
        var f = document.adm_frm_2;
        check_count = 0;
        var checked_idx=-1;
        if (f.idx != null) {

            for (i = 0; i < f.idx.length; i++) {

                if (f.idx[i].checked) {
                    checked_idx = i;
                    check_count++;
                }
            }
            //alert(checked_idx);
            if (check_count == 0) {
                alert('Please select car to modify info.');
                return;
            } else if (check_count > 1) {
                alert('Please select only one car to modify info.');
                return;

            } else {
                f.address.value=document.URL;
                f.method = "post";
                f.action = '?c=admin&m=adm_bbs_sale_modify&mcd=<?php echo $mcd; ?>&category=offer&idx=' + f.idx[checked_idx].value;
                f.submit();
            }
        }
    }


  //   function reserve_check() {
		
  //       var f  = document.adm_frm;
  //       var fg = false;
		// var car_status = '';
		
  //       check_count = 0;
		
  //       //var idxs = new Array();
  //       var item_num = '';

  //       if(f.idx != null) {

  //           for(i=0; i<f.idx.length; i++) {

  //               if(f.idx[i].checked) {
  //                   fg = true;

  //                   item_num = document.getElementsByName("idx[]")[i-1].getAttribute("data-idx");

		// 			var car_status = document.getElementsByName("idx[]")[i-1].getAttribute("data-status");

  //                   var car_refno = document.getElementsByName("idx[]")[i-1].getAttribute("data-refno");
					
  //                   check_count++;
  //               }
  //           }
  //           if(check_count == 1) {
  //               if(car_status == 'sale' ||car_status == 'order'){    
                
		// 		window.location.replace('?c=admin&m=adm_bbs_detail&mcd=<?php echo $mcd; ?>&idx=' + item_num);	//go to detail page

  //               window.open('/?c=admin&m=adm_reserve_popup_update&mcd=<?=$mcd?>&idx='+item_num,'deposit','width=720,height=550,top=100,left=100'); 
                
  //               return;
  //           }else{
		// 			alert('Sorry Car\'s number '+car_refno+' has been Reserved, please try again.');
		// 			window.location.reload();
		// 			return;
		// 		}
  //           }
			
  //           if(check_count != 1) {
				 
  //               alert('Please select one car to reserve.');
		// 		window.location.reload();
  //               return;
  //           }/*else{
				
  //           f.action = '/?c=admin&m=adm_reserve_update&mcd=<?= $mcd ?>&idx='+f.idx;
  //           f.submit();	
		// 	}*/

		
		// }
  //   }

    function reserve_cancel() {
        var f = document.adm_frm_2;
        var fg = false;
        var car_status = '';

        check_count = 0;
        var idxs = new Array();

        if (f.idx != null) {

            for (i = 0; i < f.idx.length; i++) {

                if (f.idx[i].checked) {
                    fg = true;

                    idxs[i] = " \' " + f.idx[i].value + " \' ";

                    var car_status = $(".idx:checked").attr("data-status");

                    if(car_status != 'reserved') {
                        alert("You can cancel only reserved cars. Please selected reserved cars.");
                        return;
                    }

                    check_count++;
                } else {
                    idxs[i] = " \' \' ";
                }
            }

            //alert(idxs); return;

            if (check_count > 0) {

                var idxss = new Array();
                var index = 0;
                for(var i = 0; i<idxs.length; i++) {

                    if(idxs[i] !=" \' \' ") {
                        idxss[index] = idxs[i];
                        index++;
                    }
                }

                if (!confirm('Are you sure to cancel the cars you selected?'))
                {
                    return;

                }else{

                    f.action = '/?c=admin&m=adm_reserve_cancel_exec&mcd=<?= $mcd ?>&idxs=' + idxss;
                    f.submit();
                }

                /*window.open('/?c=admin&m=adm_bbs_ship_popup_update&mcd=<?= $mcd ?>&idxs=' + idxs, 'shipment', 'width=600,height=550,top=100,left=100');

                return;*/
            } else { //check_count == 0
                alert('Please select cars to cancel a reservation.');
                return;
            }

/*            f.action = '/?c=admin&m=adm_bbs_ship_update&mcd=<?= $mcd ?>&idx=' + f.idx;
            f.submit();*/
        }


    }

    function ShippingChargeClick(){
        window.open('/?c=admin&m=adm_shipping_charge&mcd=shipping_charge', 'shipment', 'width=750,height=550,top=100,left=100');

    }

    // function excel_export() {

    //     var f = document.adm_frm;

    //     f.action = '?c=admin&m=adm_bbs_list&mcd=<?= $mcd ?>&export_file=Y';
    //     f.submit();

    //     //f.action = '?c=admin&m=adm_bbs_list&mcd=<?= $mcd ?>';
    // }

</script>

 
    <div class="contents_box_middle">

        <h1 class="title">Motorbb Car Stock </h1>
        <?php 
           if(in_array($_SESSION['ADMIN']['session_grade_no'],$admin_group)){
                                       
       ?>          
                                        
                                        
<!--<div class="excel_ship" style="float:right;">
     <a href="#"  title="Shipping Charge Setting" onclick="ShippingChargeClick();"><span class="button rosy">Shipping Charge Setting</span></a> 
    <a href="/?c=admin&amp;m=adm_shipping_charge"  title="Shipping Charge Setting"><span class="button rosy">Shipping Charge Setting</span></a>
    <a class="fancybox" href="#excel_export_popup"><span class="button green">Excel Export</span></a>
    <a href="/?c=admin&amp;m=adm_bbs_list&amp;mcd=gcm"><span class="button message">Message</span></a>
</div> -->                                   
<div class="gcm_msg">
   
   
</div>
  <?php } ?> 
				
<?php
   if(isset($_GET['sch_car_image'])) $sch_car_image=  htmlspecialchars($_GET['sch_car_image']);else $sch_car_image="";
   if(isset($_GET['sch_car_owner'])) $sch_car_owner=  htmlspecialchars($_GET['sch_car_owner']);else $sch_car_owner="";
   if(isset($_GET['sch_car_location'])) $sch_car_location=  htmlspecialchars($_GET['sch_car_location']);else $sch_car_location="";
   if(isset($_GET['sch_create_dt_s'])) $sch_create_dt_s=  htmlspecialchars($_GET['sch_create_dt_s']);else $sch_create_dt_s="";
   if(isset($_GET['sch_create_dt_e'])) $sch_create_dt_e=  htmlspecialchars($_GET['sch_create_dt_e']);else $sch_create_dt_e="";
   if(isset($_GET['sch_icon_status'])) $sch_icon_status=  htmlspecialchars($_GET['sch_icon_status']);else $sch_icon_status="";
   if(isset($_GET['sch_condition'])) $sch_condition=  htmlspecialchars ($_GET['sch_condition']);else $sch_condition="";
   if(isset($_GET['sch_word'])) $sch_word=  htmlspecialchars($_GET['sch_word']);else $sch_word="";
   if(isset($_GET['carmake'])) $carmake=  htmlspecialchars($_GET['carmake']);else $carmake="";
   if(isset($_GET['carmodel'])) $carmodel=  htmlspecialchars($_GET['carmodel']);else $carmodel="";
   if(isset($_GET['year'])) $year=  htmlspecialchars($_GET['year']);else $year="";
   
   
?>
        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You can search motorbb stock cars.</li>

            </ul>

        </div>

                <!--<div style="padding-top:5px;text-align:right;"><img src="/images/admin/btn_reset.gif" alt="초기화" /></div>-->

        <div class="table_list_box" style="padding-top:10px;">

            <h1 class="table_title"><?= func_get_config($bbs_config_result, 'bbs_code_name') ?></h1>

            <div class="search_box">

                <div class="" style="width: 990px;">

                    <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Total : <span class="title"><?= $total_rows ?></span> cars, Pages : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>

                </div>
                 <?php
                      if( $_SESSION['ADMIN']['business_type'] =='seller' ){
                  ?>
                <div>
                    <div style="float: left;width: 400px;">
                         <div class="btn_area_center" style="width:350px;float: left; margin-top: 10px;margin-left: 20px;">
               
                    <div style="">
                        <a href="/?c=admin&amp;m=adm_bbs_write&amp;mcd=<?= $mcd ?>" title="등록"><span class="button orange">Register</span></a>
                        <a href="#" onclick="modify_sale_check();" title="modify"><span class="button orange">Modify</span></a>
                        <a href="#" onclick="delete_all();" title="delete"><span class="button orange">Delete</span></a>                    
                    </div>
               </div>
                    </div>
                    <div style="float: left;width:900px;height:50px;margin-top: 20px;">
                        <form name="bbs_search_frm" id="adm_frm" method="get" action="" >
                    <input type="hidden" name="c" value="admin">
                    <input type="hidden" name="m" value="adm_bbs_list">
                    <input type="hidden" name="mcd" value="<?= $mcd ?>">
                <div class="search_condition" style="float: right;">
                
                   
                     <select name="carmake" id="admin_carmake" class="input_2"  align="absmiddle">
                        <option value="">Make</option>
                        <?php 
                                foreach($select_make_all as $make){                                   
                              
                         ?> 
                        <option
                                 <?php 
                                 echo $carmake=(isset($_GET['carmake']) && $_GET['carmake']=="$make->car_make"? 'selected="selected"':''  );
                                 ?>
                                value="<?php echo $make->car_make; ?>"><?php echo $make->car_make; ?></option>
                        
                        <?php
                          } 
                        ?>

                    </select>

                     <select id="admin_carmodel" name="carmodel" class="input_2" style="width:150px;" align="absmiddle">
                        <option value="">Model</option>
                        
                        <?php foreach ($select_model_all as $model) {  ?>
                        
                        <option <?php 
                                 echo $carmodel=(isset($_GET['carmodel']) && $_GET['carmodel']=="$model->car_model"? 'selected="selected"':''  );
                                 ?> class="<?php echo $model->car_make; ?>" value="<?php echo $model->car_model; ?>"><?php echo $model->car_model; ?></option>
                                       
                        <?php } ?>
                    </select>

                    
                     <select name="year" id="year" class="input_2" style="width:150px;" align="absmiddle">
                        <option value="">Year</option>
                                                 <?php 
                                      						for($i=date("Y"); $i>=1990; $i--){
                                              ?>

                                                <option <?php if(isset($_GET['year'])){ if($_GET['year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>

                                              <?php 
                                      						}
                                    					?>
    

                    </select>
                     <select name="sch_icon_status" id="sch_icon_status" class="input_2" style="width:150px;" align="absmiddle">
                        <option value="">Status</option>
                         <?php foreach (Array('sale','reserved','shipok','releaseok','soldout') as $Val) { ?>
                                        <option <?php echo $sch_icon_status=(isset($_GET['sch_icon_status']) && $_GET['sch_icon_status']=="$Val"? 'selected="selected"':''); ?> 
                                                    value="<?php echo $Val; ?>"><?php echo ucfirst($Val); ?></option>
                        <?php } ?>   

                    </select>
                    
                    <input type="text" name="sch_word" id="sch_word" value="<?php echo $sch_word; ?>" class="input_1" style="width:140px;" placeholder="Chassis, Stock Number" />

                    <input type="image" src="/images/admin/btn_search_01.gif" style="width:60px;" title="검색" align="absmiddle" />
                    <input type="reset" value="Clear" style="background: #504f4b none repeat scroll 0 0;
    border: medium none;border-radius: 0.3em;color: #fff;height: 25px;width: 62px;cursor: pointer;">
                </div> 
             </form>
                    </div>
                </div>
                
               
               
                <div style="clear: both;">
                    
                </div>
                
                <?php
                    }
                    else{
                ?>
                
                <div class="search_condition" style="padding: 3px 0; height:35px; margin-top:5px; margin-bottom:25px">

                   
                  <form name="bbs_search_frm" id="adm_frm" method="get" action="" >
                    <input type="hidden" name="c" value="admin">
                    <input type="hidden" name="m" value="adm_bbs_list">
                    <input type="hidden" name="mcd" value="<?= $mcd ?>">
                    <label class="search_product">Search Product</label>

                      <select class="input_2" style="width:98px;" name="sch_car_image" align="absmiddle">
                          <option value="">Image</option>
                          <option <?php if($sch_car_image){echo 'selected="selected"';} ?> value="not_image">Not Image</option>
                      </select>

                      <select class="input_2" style="width:98px;" name="sch_car_owner" align="absmiddle">
                          <option value="">Owner</option>
                            <?php 
                                foreach($list_car_owner as $car_owner){
                                ?>
                                <option
                                     <?php 
                               echo $sch_car_owner=(isset($_GET['sch_car_owner']) && $_GET['sch_car_owner']=="$car_owner->member_no"? 'selected="selected"':''  );
                                     ?>
                                    value="<?php echo $car_owner->member_no; ?>"><?php echo ucfirst($car_owner->member_id); ?></option>
                                <?php
                                } 
                            ?>
                      </select>

                      <select name="sch_car_location" class="input_2" style="width:150px;" align="absmiddle">

                        <option value="">Location</option>
                        
                       <?php 
                            foreach($country_list as $country){
                            ?>
                            <option
                                 <?php 
                           echo $sch_car_location=(isset($_GET['sch_car_location']) && $_GET['sch_car_location']=="$country->cc"? 'selected="selected"':''  );
                                 ?>
                                value="<?php echo $country->cc; ?>"><?php echo $country->country_name; ?></option>
                            <?php
                            } 
                        ?>
                    </select>
                           
                  <select name="carmake" id="admin_carmake" class="input_2"  align="absmiddle">
                        <option value="">Make</option>
                        <?php 
                                foreach($select_make_all as $make){                                   
                              
                         ?> 
                        <option
                                 <?php 
                                 echo $carmake=(isset($_GET['carmake']) && $_GET['carmake']=="$make->car_make"? 'selected="selected"':''  );
                                 ?>
                                value="<?php echo $make->car_make; ?>"><?php echo $make->car_make; ?></option>
                        
                        <?php
                          } 
                        ?>

                    </select>

                     <select id="admin_carmodel" name="carmodel" class="input_2" style="width:150px;" align="absmiddle">
                        <option value="">Model</option>
                        
                        <?php foreach ($select_model_all as $model) {  ?>
                        
                        <option <?php 
                                 echo $carmodel=(isset($_GET['carmodel']) && $_GET['carmodel']=="$model->car_model"? 'selected="selected"':''  );
                                 ?> class="<?php echo $model->car_make; ?>" value="<?php echo $model->car_model; ?>"><?php echo $model->car_model; ?></option>
                                       
                        <?php } ?>
                    </select>
                    
                     <select name="year" id="year" class="input_2" style="width:100px;" align="absmiddle">
                        <option value="">Year</option>
                                                 <?php 
                                                            for($i=date("Y"); $i>=1990; $i--){
                                              ?>

                                                <option <?php if(isset($_GET['year'])){ if($_GET['year']==$i){ ?> Selected <?php } } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>

                                              <?php 
                                                            }
                                                        ?>
    

                    </select>
                     <select name="sch_icon_status" id="sch_icon_status" class="input_2" style="width:100px;" align="absmiddle">
                        <option value="">Status</option>
                         <?php foreach (Array('sale','reserved','shipok','releaseok','soldout') as $Val) { ?>
                                        <option <?php echo $sch_icon_status=(isset($_GET['sch_icon_status']) && $_GET['sch_icon_status']=="$Val"? 'selected="selected"':''); ?> 
                                                    value="<?php echo $Val; ?>"><?php echo ucfirst($Val); ?></option>
                        <?php } ?>   

                    </select>

                   
                    <?

                    //카테고리 사용 여부

                    if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {

                    $category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));

                    $category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));

                    ?>

                    <!-- 
                                                                    <select name="category" id="category" class="input_2" align="absmiddle">
                    
                                                                            <option value="">::선택하세요::</option>
                    
                                                                            <? if (count($category_code) > 0) {
                    
                                                                                    for ($i = 0; $i < count($category_code); $i++) {
                    
                                                                                            ?>
                    
                                                                                            <option value="<?= $category_code[$i] ?>"><?= $category_name[$i] ?></option>
                    
                                                                            <? } ?>
                    
                                                                    <? } ?>
                    
                                                                    </select> -->

                    <? } ?>
                    


                     <input type="text" name="sch_word" id="sch_word" value="<?php echo $sch_word; ?>" class="input_1" style="width:230px;" placeholder="Chassis, Stock Number" />

                    <input type="image" src="/images/admin/btn_search_01.gif" style="width:60px;" title="검색" align="absmiddle" />
                    <input type="reset" value="Clear" style="background: #504f4b none repeat scroll 0 0;
    border: medium none;border-radius: 0.3em;color: #fff;height: 25px;width: 62px;cursor: pointer;">
                   </form>
                   <br>
                      
                </div>
                
                <?php
                      }
                ?>


            </div>


            <?php 
                if(in_array($_SESSION['ADMIN']['session_grade_no'],$admin_group)){                            
            ?>  
                        
                <div class="header_button">

                    <div class="wrapper_header_btn">
                        <div>
                            <a href="/?c=admin&amp;m=adm_bbs_write&amp;mcd=<?= $mcd ?>">
                                <img src="/images/admin/button/add.png" /><br/>
                                Register
                            </a>
                        </div>
                        <div>
                            <a href="#" onclick="modify_check();" title="modify">
                                <img src="/images/admin/button/modify.png" /><br/>
                                Modify
                            </a>
                        </div>
                        <div>
                            <a href="#" id="btnDelete" class="disabled">
                                <span id='del'><img src="/images/admin/button/delete.png" /></span><br/>
                                <span id='del'>Delete</span>
                            </a>
                        </div>
                    </div>

                    <div class="wrapper_header_btn">
                        <div>
                            <a href="#" id="reserve_btn" title="reserve">
                                <img src="/images/admin/button/reserve.png" /><br/>
                                Reserve
                            </a>
                        </div>
                        <div>
                            <a href="#" onclick="shipment_all();" title="shipment">
                                <img src="/images/admin/button/shipment.png" /><br/>
                                Shipment
                            </a>
                        </div>
                        <div>
                            <a href="#" onclick="reserve_cancel();" title="reserve cancel">
                                <img src="/images/admin/button/r_cancel.png" /><br/>
                                R Cancel
                            </a>
                        </div>
                    </div>
                    
                    <div class="wrapper_header_btn">
                        <!-- <input type="button" id="btn_insert_to_newsletter" class="btn_send_rec" value="Insert to Newsletter" /> -->
                        <div>
                            <a href="#" id="btn_insert_to_newsletter" class="btn_send_rec">
                                <img src="/images/admin/button/insert_newsletter.png" /><br/>
                                Insert to Newsletter
                            </a>
                        </div>
                        <div>
                            <a href="/?c=admin&m=send_recommend_car">
                                <img src="/images/admin/button/send_newsletter.png" /><br/>
                                Send Newsletter (<span id="newsletter_idx_count">0</span> Car(s))
                            </a>
                            
                        </div>
                        <div>
                            <a href="#" id="btn_clear_newsletter" >
                                <img src="/images/admin/button/clear_newsletter.png" /><br/>
                                Clear Newsletter
                            </a>
                            
                        </div>
                    </div>

                    <div class="wrapper_header_btn">
                         <div>
                            <a class="fancybox" href="#excel_export_popup">
                                <img src="/images/admin/button/export_excel.png" /><br/>
                                Excel Export
                            </a>
                        </div>
                        <div>
                            <a href="#" id="btn_invoice_rec" class="btn_invoice_rec" >
                                <img src="/images/admin/button/create_invoice.png" /><br/>
                                Create Invoices
                            </a>
                        </div>
                    </div>
                         
                </div>
                           
            <?php
               }
            ?>


            <div style="float:left; width:100%">
                <form name="adm_frm_2" id="adm_frm_car" method="post" action="">
                
                <input type="hidden" name="address"> <!-- sending url address -->

                <table class="table_list" summary="<?= func_get_config($bbs_config_result, 'bbs_code_name') ?>" cellspacing="0" style="width:100%;">

                    <caption class="hidden"></caption>

                    

                    <thead>

                        <tr>

                            <th scope="col"><input type="checkbox" name="idx" id="idx" class="idx_chk_all" value=""/></th>

                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <!-- <th scope="col">No.</th>

                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th> -->

                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('country')?>">Location</a></th>

                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                           <?php
                            if( $_SESSION['ADMIN']['business_type'] !='seller' ){
                          ?>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('car_stock_no')?>">RefNo</a></th>
                             <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('icon_status')?>">Status</a></th>

                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('car_model')?>">Car</a></th>

                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('car_chassis_no')?>">ChassisNo</a></th>

                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('manu_year')?>">ModelYear</a></th>

                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('car_fob_cost')?>">FOB Price</a></th>

                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('icon_status')?>">Total Sell Price</a></th>
                            <?php
                            }
                            else{                          
                            ?>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('car_stock_no')?>">Stock Number</a></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('icon_status')?>">Status</a></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('icon_status')?>">Car Make</a></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('icon_status')?>">Car Model</a></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('icon_status')?>">Model Year</a></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('icon_status')?>">ChassisNo</a></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('icon_status')?>">Engine Size</a></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('icon_status')?>">Extorior Color</a></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            <th scope="col"><a href="/?<?=getOrderByFieldUrl('icon_status')?>">FOB Price</a></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            
                            <?php
                             }
                            ?>

                            
                        </tr>
                        
                    </thead>

                    <tbody>

                        <?php

                        //공지글

                        

                        

                        //var_dump(count($bbs_list)); exit();

                        //일반글

                        //print_r(count($bbs_list));
                        if (count($bbs_list) > 0) {

                        //var_dump($bbs_list[2]->car_stock_no); exit();

                        $bbs_link = ''; //링크

                      

                        $level_str = ''; //자식글


                        foreach ($bbs_list as $rows) {
                        //var_dump($rows); exit();

                        //링크
                        $bbs_link = "/?c=admin&m=adm_bbs_detail&mcd=$mcd&idx=$rows->idx&cur_page=$cur_page" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);

                        //자식글
                        $level_str = funct_level_str($rows->depth, $mcd);

                        //제목 길이 제한
                        

                        //Secret 아이콘
                        $secret_icon = '';

                        if (func_get_config($bbs_config_result, 'secret_yn') == 'Y' && $rows->secret_yn == 'Y')

                        $secret_icon = func_secret_icon($mcd);

                        //New, Hot 아이콘
                        $icon = '&nbsp;&nbsp;' . func_new_icon(func_get_config($bbs_config_result, 'icon_new'), $rows->created_dt, $mcd) . '&nbsp;&nbsp;' . func_hot_icon(func_get_config($bbs_config_result, 'icon_hot'), $rows->visit_cnt, $mcd);

                        //Totalsellprice
                        $total_sell_price = $rows->car_actual_price + $rows->car_freight_fee;

                        ?>
                                        <?php
                                                     // if(count($bbs_cloud_list_items) >0){
                                                           // foreach ($bbs_cloud_list_items as $list){


                                        ?>
<!--                                         <input type="text" name="public_id" value="<?php  echo $list->public_id; ?>" >-->
                                         <?php
                                                           // }
                                                   //   }
                                        ?>
                        <tr>
                    <?php
                            if( $_SESSION['ADMIN']['business_type'] !='seller' ){
                          ?>
                           
                            <td><input type="checkbox" name="idx[]" id="idx" value="<?= $rows->idx ?>" class="idx" data-status="<?= $rows->icon_status?>" data-refno="<?=$rows->car_stock_no ?>" data-idx="<?=$rows->idx ?>"/>
                            	
                            </td>

                            <td class="line">&nbsp;</td>

                            <td><a href="<?= $bbs_link ?>"><?= $rows->country_name ?></a></td>

                            <td class="line">&nbsp;</td>
                                                    


                            <td><a href="<?= $bbs_link ?>" style="display: block;"><?= $rows->car_stock_no ?></a></td>

                            <td class="line">&nbsp;</td>

                            <td><a href="<?= $bbs_link ?>" style="display: block;">
                                    <b><?= strtoupper($rows->icon_status); ?></b></a></td>

                            <td class="line">&nbsp;</td>

                            
                              <?php
                            }
                      ?>
                            <?php
                               if($_SESSION['ADMIN']['business_type'] !='seller'){   
                             ?>
                            <td><a href="<?= $bbs_link ?>"><?php if($rows->car_model_year==0){
                                echo $rows->car_make.' '.$rows->car_model; 
                           }                  
                           else{
                               echo $rows->car_make.' '.$rows->car_model.' '.$rows->car_model_year;  
                           }
                        ?></a></td>

                            <td class="line">&nbsp;</td>

                            <td><a href="<?= $bbs_link ?>"><?= $rows->car_chassis_no ?></a></td>

                            <td class="line">&nbsp;</td>

                            <td><?= ($rows->car_model_year==0||$rows->car_model_year == '' ? '' : $rows->car_model_year) ?></td>

                            <td class="line">&nbsp;</td>

                            <td>
							 <?php  if($rows->car_fob_cost=='' || $rows->car_fob_cost==0 || $rows->car_fob_cost==NULL){
								  echo 'Ask Price';
							  }
							  else{
								?>  
							
                                <? switch ($rows->car_fob_currency) {
                                    case 'USD':
                                        echo 'USD';
                                        break;
                                    case 'KRW':
                                        echo 'KRW';
                                        break;    
                                    case 'JPY':
                                        echo 'JPY';
                                        break;
                                    case 'EUR':
                                        echo 'EUR';
                                        break;   
                                    default:
                                        echo ' ';
                                        break;
                                } ?>

                                <?= number_format($rows->car_fob_cost) ?>
                                <?php } ?>
                            </td>

                            <td class="line">&nbsp;</td>
                            <td>
                                    <? if(isset($total_sell_price) && isset($rows->allocate_sum) && !($total_sell_price == 0) ){ ?>
                                    <? 
                                    $percent = ($rows->allocate_sum / $total_sell_price) * 100 ;

                                    echo round($percent,2) . " %";
                                    ?>
                                    <? } ?>
                                </td>
                            <?php
                               }
                                                                                                                                                                      
                            ?>
                          
                              
                        </tr>
                       

                        <?php } ?>

                        <?php }else{ ?>
                         <?php
                          if($_SESSION['ADMIN']['business_type']!='seller'){   
                         ?>
                         <tr>
		         <td colspan="40" class="no_data">Data does not exist.</td>
		        </tr>
                        <?php
                          }
                        ?>
                         <?php
                         }
                        
                           ?>
                        
                        
                        
                        
                        
                        
                        
                          <?php
                            if($_SESSION['ADMIN']['business_type'] =='seller'){   
                              if(count($bbs_list_sale)>0){
                                       foreach ($bbs_list_sale as $sale){
                                         $bbs_links = "/?c=admin&m=adm_bbs_detail&mcd=$mcd&idx=$sale->idx&cur_page=$cur_page" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);
                            ?>
                           <tr>
                            <td><input type="checkbox" name="idx[]" id="idx" value="<?= $sale->idx ?>" class="idx" data-status="<?= $sale->icon_status?>" data-refno="<?=$sale->car_stock_no ?>" data-idx="<?=$sale->idx ?>"/>                           	
                            </td>
                            <td class="line">&nbsp;</td>
                            <td><a href="<?= $bbs_links ?>"><?= $sale->country_name ?></a></td>
                            <td class="line">&nbsp;</td>
                            <td><a href="<?= $bbs_links ?>" style="display: block;"><?= $sale->car_stock_no ?></a></td>
                            <td class="line">&nbsp;</td>
                            <td><a href="<?= $bbs_links ?>" style="display: block;">
                            <b><?= strtoupper($sale->icon_status); ?></b></a></td>
                            <td class="line">&nbsp;</td>
                            <td><a href="<?= $bbs_links ?>"style="display: block;"><?php echo $sale->car_make?></a></td>
                            <td class="line">&nbsp;</td>
                            <td><a href="<?= $bbs_links ?>"style="display: block;"><?php echo $sale->car_model ?></a></td>
                            <td class="line">&nbsp;</td>
                            <td><a href="<?= $bbs_links ?>"style="display: block;"><?php if($sale->car_model_year==0 || $sale->car_model_year==''){echo '';}else{echo $sale->car_model_year;} ?></a></td>
                            <td class="line">&nbsp;</td>
                            <td><a href="<?= $bbs_links ?>"style="display: block;"><?php echo $sale->car_chassis_no ?></a></td>
                            <td class="line">&nbsp;</td>
                            <td><a href="<?= $bbs_links ?>"style="display: block;"><?php if($sale->car_cc==0 || $sale->car_cc==''){echo '';}else{echo $sale->car_cc;} ?></a></td>
                            <td class="line">&nbsp;</td>
                            <td><a href="<?= $bbs_links ?>"style="display: block;"><?php echo $sale->car_color;?></a></td>
                            <td class="line">&nbsp;</td>
                            <td><a href="<?= $bbs_links ?>" style="display: block;">
			<?php  if($sale->car_fob_cost=='' || $sale->car_fob_cost==0 || $sale->car_fob_cost==NULL){
				 echo 'Ask Price';
			 }
			 else{
			?>  
							
                                <? switch ($sale->car_fob_currency) {
                                    case 'USD':
                                        echo 'USD';
                                        break;
                                    case 'KRW':
                                        echo 'KRW';
                                        break;    
                                    case 'JPY':
                                        echo 'JPY';
                                        break;
                                    case 'EUR':
                                        echo 'EUR';
                                        break;   
                                    default:
                                        echo ' ';
                                        break;
                                } ?>

                                <?= number_format($sale->car_fob_cost) ?>
                                <?php } ?>
                                </a>
                                </td><td class="line">&nbsp;</td>
                                <td>
                                    <? if(isset($total_sell_price) && isset($rows->allocate_sum) && !($total_sell_price == 0) ){ ?>
                                    <? 
                                    $percent = ($rows->allocate_sum / $total_sell_price) * 100 ;

                                    echo round($percent,2) . " %";
                                    ?>
                                    <? } ?>
                                </td>
                            </tr>
                            
                            <?php
                              }
                             } else{                                                                                                       
                            ?>
                            <?php
                             if($_SESSION['ADMIN']['business_type']=='seller' && (isset($_GET['sch_word'])) ){   
                            ?>
                             <tr>
		                        <td colspan="40" class="no_data">Search not found.</td>
		                   </tr>
                            <?php
                             }
                            ?>


                            <?php
                             if($_SESSION['ADMIN']['business_type']=='seller' && (!isset($_GET['sch_word'])) ){   
                            ?>
                             <tr>
                                <td colspan="40" class="no_data">You did not register any car yet.</td>
                           </tr>
                            <?php
                             }
                            ?>



                           <?php
                             }
                           ?>
                            

                         <?php
                            }
                         ?>

                    </tbody>

                </table>

            </div>
       
            <div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?'.updateCurrentGetUrl(array(), array('cur_page')), $mcd) ?></div>

<!-- 			<? var_dump($_POST); ?>
                        <? var_dump($_GET); ?>
                        <? var_dump($total_rows); ?><br/>
                        <? var_dump($sParam); ?>


                        <? foreach ($bbs_list as $rows) { ?>
            <?= $rows->idx ?> 

                        <? } ?>

                        <? var_dump($bbs_list[0]->idx); ?> -->
            <? //$query = $this->db->last_query(); ?>
            <div style="display:none;">
                <div id="excel_export_popup">
                    <table style="width: 500px; border-collapse: collapse;">
                        <tr>
                            <th style="padding:10px" colspan="2">Please click the excel that you want to download</th>
                        </tr>
                        <tr>
                            <td style="padding:10px;border-right:1px solid gray;"><b style="padding-left: 38px">Select Port</b></td><td></td></tr>
                        <tr><td style="padding-bottom:10px;border-right:1px solid gray;text-align: center;">Country
                                    <select name="des_country" id="des_country" class="search_condition" style="width:140px;">
                                    <option value="">Select Country</option>
                                    <? 
                                    foreach ($list_destination as $destination) {
                                    ?>
                                    <option value="<?= $destination->country_iso ?>"><?= $destination->country_name ?></option>
                                    <?
                                    }
                                    ?>
                                    </select>
                        </td><td></td>
                        </tr><tr><td style="border-right:1px solid gray;text-align: center;">Port &nbsp; &nbsp; &nbsp;&nbsp;
                                    <select name="portname" id="portname" class="search_condition" style="width:140px;">
                                      <option>Select Port</option>
                                    </select>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                    
                            <td style="padding:10px;border-right:1px solid gray; text-align:center">
                                <input type="hidden" id="link_export_query" value="/?<?=$bbs_excel_customer_get ?>"/>
                                <a href="#" id="link_export"><span class="button green">Stock List</span></a>
                            </td>
                            <td style="padding:10px;text-align:center"><a href="/?<?=$bbs_excel_staff_get ?>"><span class="button red">Car List</span></a></td>
                        </tr><tr>
                            <td style="padding:10px;border-right:1px solid gray; text-align:center">This excel is for customer </td>
                            <td style="padding:10px;text-align:center">This excel is for sales</td>
                        </tr>
                    </table>
                </div>
            </div>
              <?php
                  if( $_SESSION['ADMIN']['business_type'] !='seller' ){
               ?>
            <div class="btn_area_center">
               
                <div class="btn_area_right">
                  <!--   <a href="#" id="reserve_btn" title="reserve"><span class="button blue">Reserve</span></a>
                    <a href="#" onclick="reserve_cancel();" title="reserve cancel"><span class="button blue">R Cancel</span></a>
                    <a href="#" onclick="shipment_all();" title="shipment"><span class="button blue">Shipment</span></a>
                    <a href="/?c=admin&amp;m=adm_bbs_write&amp;mcd=<?= $mcd ?>" title="등록"><span class="button blue">Register</span></a>
                    <a href="#" onclick="modify_check();" title="modify"><span class="button blue">Modify</span></a>
                    <button type="button" id="btnDelete" class="button red disabled"><span></span> <b id='del'>Delete</b></button> -->
                </div>
            </div>
            
            <?php
               }
            ?>

            <div class="search_box" style="padding:15px 10px;background-color:#ffffff">

                <!-- <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Selected cars

                <a href="#" onclick="delete_all();" title="delete"><img src="/images/admin/btn_delete_eng.gif" alt="delete" align="middle" /></a> -->
                <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />You can search/register/modify/delete motorbb stock cars
            </div>
            </form>
        </div>

                    <!-- Modal -->
            

    </div>



<!-- ? print_r($this->db->last_query()); ? -->


   <script type="text/javascript">

   $("#admin_carmodel").chained("#admin_carmake");

$(document).ready(function() {
    update_newsletter_idx();
    

         $('#idx').click(function(event) {
          if(this.checked) {
              $(':checkbox').each(function() {
                  this.checked = true;
                  $('#btnDelete').removeClass('disabled');
              });
          }
          else {
            $(':checkbox').each(function() {
                  this.checked = false;
                  $('#btnDelete').addClass('disabled'); 
              });
          }
        });

        $('.idx').click(function(event) {   
            if(this.checked) {
                this.checked = true;
                $('#btnDelete').removeClass('disabled');   
            }
            else{
                 $('#btnDelete').addClass('disabled'); 
            }
        });
        
        $("#btnDelete").click(function(e){
            var checked_count = $(".idx:checked").length;
            if(checked_count < 1 ){
                 alert("Please select items to delete");
            }else{
                delete_select();
            }
         });

        function delete_select(){
            if(confirm("Are you sure want to delete selected cars?")){
                $('#adm_frm_car').attr('action', "/?c=admin&m=adm_bbs_all_delete&mcd=product").submit();
            }
        }


        

    // end delete
    $("#link_export").attr("onclick", "return false;");
    $(".datepicker-add-options").datepicker({
        
    });
    $("#btn_send_rec").click(function(e){
        
        $("[name='adm_frm_2']").attr('action', "/?c=admin&m=send_recommend_car").submit();
        
        
    });

    $("#btn_invoice_rec").click(function(e){
        
        if(($('#idx:checked').length) > 0){
            $("[name='adm_frm_2']").attr('action', "/?c=admin&m=adm_invoice_register").submit();
        }else{
            alert('Please select car to create invoices.');
        }
        
    });
    $("#btn_insert_to_newsletter").click(function(e){
        update_newsletter_idx();
    });
    $("#btn_clear_newsletter").click(function(e){
        clear_newsletter();
    });

});

function clear_newsletter(){
     
    var request = $.ajax({
        url: "/?c=admin_json&m=clear_newsletter",
        type: "GET",
        dataType: "json"
    });
    request.done(function(msg){
        $("#newsletter_idx_count").html(msg.length);
    });
}

function update_newsletter_idx(){
    var form_data = $("#adm_frm_car").serializeArray();   
    var request = $.ajax({
      url: "/?c=admin_json&m=insert_idx_for_newsletter",
      type: "POST",
      data: form_data,
      dataType: "json",
    });
    request.done(function(msg){
        $("#newsletter_idx_count").html(msg.length);
    });
}
// select destination
$(document).on("change","select[name*='des_country']",function() {
$("#link_export").attr("onclick", "return false;");    

        $("#portname").empty();//To reset cities
        $("#portname").append("<option value=''>Select Port</option>");

            var country_to=$(this).val();
            $.getJSON('/?c=car_model&m=get_port_name&country_to='+country_to,function(json){
                $("select[name*='portname']").html("");
                $( "select[name*='portname']").append('<option value="">Select Port</option>');
             $.each(json,function(i,data_json){
                        
                    

                respone_data="<option title='" + data_json.port_name_title + "' value='" + data_json.port_name + "'>" + data_json.port_name_title + "</option>";
                $( "select[name*='portname']").append(respone_data);

            });

            });


        });
        $("#portname").change(function(){
            var country_to = $("#des_country").val(); 
            var link_export = $("#link_export_query").val();
            var portname = $(this).val();
            $("#link_export").removeAttr('onclick');
            $("#link_export").attr('href',link_export+'&country_to='+country_to+'&port_name='+portname);
        });
 </script>
 




