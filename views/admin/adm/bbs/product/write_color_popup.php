<?php 
   // var_dump($member_list);
?>
 <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Title of the document</title>
<link rel="stylesheet" type="text/css" href="/css/admin/admin.css" />
<link rel="stylesheet" type="text/css" href="/css/admin/button.css" />
<style>
    .container{
        min-width: 250px;
        
    }
     .container table{
       width: 500px;
    }
   .bottom_button_container{
        width: 250px;
        margin-left: 35px; 
   }
   #add_button,#delete_button,#close_button{
        padding: 5px 12px;
   }
   .input_color,.get_color{
        width: 200px;
        font-size: 12px;
        padding: 2px 10px;
   }
   .get_color{
     width: 220px;
   }
   .control{
        border: solid 0px #000;
        text-align: center;
        width: 250px; 
        padding: 10px 0px 5px 23px;
   }
   .control_option{
        border: solid 0px #000;
        text-align: center;
        width: 250px; 
        padding: 10px 0px 15px 23px;
   }
   #alert_style{
        line-height: 10px;
        height: 12px;
        /*border: solid 1px #000;*/
   }
</style>
</head>

<body style="background:white;">

<?php 
$msg ="";
if(isset($_POST['save'])){
    if(isset($_POST['input_color'])){
        $name=$_POST['input_color'];
        $sqls=mysql_query("SELECT * FROM `iw_bbs_color` WHERE `color_name`='$name'");
        $duplicate=mysql_fetch_row($sqls);
        if($duplicate==0){
            if($_POST['input_color'] !=""){
                $sql_query=mysql_query("INSERT  INTO iw_bbs_color(color_name,created_dt)VALUES('$name',NOW())");
                $msg ="You have added the color successfully!";
            }else{
                $msg ="Please enter color before you save!";
            }
        }else{
            $msg ="The color already exists!";
        }
    }
}
if(isset($_POST['delete'])){
    if(isset($_POST['show_color']) !=""){
        $query = "DELETE FROM iw_bbs_color WHERE color_no={$_POST['show_color']}"; 
        $result = mysql_query($query);
        $msg ="You has delete the color(s) successful!";
    }else{$msg ="Please select color before you delete!";}
}
?>
    <div class="container">
        
        <form action="" method="post">
            
        <center> <br><h3>Please add the color you want</h3> </center>
            <div class="control">
            <p id="alert_style"><?php echo $msg; ?></p>
                <input type="text" class="input_color" onkeypress="return isNumberKey(event)" name="input_color">
            </div>
            <div class="control_option">
                <select class="get_color" multiple="multiple" size="10" name="show_color" id="show_color">
                    <?
                    $slq="SELECT * FROM iw_bbs_color ORDER BY color_name";
                    $sql_query = mysql_query($slq);
                    while($row=mysql_fetch_array($sql_query)){
                    ?>
                    <option value="<?= $row['color_no'] ?>" ><?= $row['color_name'] ?> </option>
                    <?
                    }mysql_free_result($sql_query);
                    ?>
                </select>
            </div>
            <div class="bottom_button_container">
                <input type="submit" name="save" id="add_button" value="Save" class="button blue"/>
                <input type="submit" name="delete" id="delete_button" value="Delete" class="button blue"/>
                <!-- <input type="submit" name="close" id="close_button" value="Close" class="button blue"/> -->
            </div>
        </form>
    </div>
    <script type="text/javascript"> 
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode < 48 || charCode > 57)
            return true;

        return false;
    }
   </script>    

</body>

</html> 