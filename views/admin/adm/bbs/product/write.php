



<script type="text/javascript" src="/js/common/common.js"></script>


<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />

<!-- link calendar resources -->
<link rel="stylesheet" type="text/css" href="/css/tcal.css" />


<script type="text/javascript" src="/js/tcal.js"></script> 

<script type="text/javascript">
    //게시판 등록
    function insert_adm_bbs(frm) {
        if (!validate(frm.elements['values[car_make]'], 'INPUT [MAKE]'))
            return false;
        if (!validate(frm.elements['values[car_model]'], 'INPUT [MODEL]'))
            return false;

        //if(!validate(frm.elements['car_info'], 'Please Enter a Title.')) return false;

        //<? if (func_get_config($bbs_config_result, 'editor_yn') == 'Y') { //게시판 HTML 에디터를 사용할 경우..  ?>
        //	if(tinyMCE.get('contents').getContent() == '' || tinyMCE.get('contents').getContent() == '<p>&nbsp;</p>') {
        //		alert("Please Enter Your Details.");
        //		return false;
        //	}
        //<? } else { ?>
        //			if(!validate(frm.elements['contents'], 'Please Enter Your Details. ')) return false;
        //<? } ?>
        //		return true;
    }

    function currency_selector(){
        var currency_type=$('#unit_price_currency option:selected').val();
        
        $("#tax_price_currency option[value="+currency_type+"]").prop("selected", "ture");
    }

</script>
<form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_bbs_write_exec&amp;mcd=<?= $mcd ?>&amp;idx=<?=$idx?>" onsubmit="return insert_adm_bbs(this);">
    <input type="hidden" name="values[category]" value="<?= isset($_REQUEST['category']) ? $_REQUEST['category'] : 'offer'; ?>">	
    <div class="contents_box_middle">
        <div class="table_write_box">
            <table class="table_write" summary="<?= func_get_config($bbs_config_result, 'bbs_code_name') ?>" border="0" cellspacing="0" width="100%">
                <caption></caption>
                <colgroup>
                    <col width="15%" />
                    <col width="35%" />
                    <col width="15%" />
                    <col width="35%" />
                </colgroup>
                <?
                //카테고리 사용 여부
                if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {
                $category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));
                $category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));
                }
                ?>


                <tr><td colspan="4" class="text_info">* Car Information</td></tr>

                <tr>
                    <th scope="row">Location</th>
                    <td class="text" colspan="3">
                        <select name="values[country]" class="select">
                            <? foreach(Array(' ','KOREA', 'JAPAN') as $Location ) { ?>
                            <option value="<?= $Location ?>"><?= $Location ?></option>
                            <? } ?>
                        </select>
                        City &nbsp;<input type="text" name="values[car_city]" id="car_city" value="" placeholder="Car city">
                    </td>

                </tr>
				<tr>
                	<th scope="row">BUYING Date</th>
                    <td class="text">
                        <input name="values[car_buying_dt]" id="car_buying_dt" type="text" class="b_text" value="" readonly />

                        <a href="#" onclick="Calendar(document.adm_frm.elements['car_buying_dt']);
                                return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>

                    </td>
                    <th scope="row" style="background:#FDBFBF;">BUYING PRICE</th>
                    <td class="text">
<!--                         <input name="values[car_buying_price]" id="car_buying_price" type="text" class="b_text" value="" ViewExYN="Y" ViewEx="ex)30000000" maxlength="10" />

                        <select name="values[car_buying_currency]" id="currency_type" class="input_2" onchange="">
                            <option value="USD" >USD($)</option>
                            <option value="KRW" >KRW(₩)</option>
                            <option value="JPY" >JPY(¥)</option>
                            <option value="EUR" >EUR(€)</option>
                        </select> -->

                        <input name="values[unit_price]" id="" type="number" class="" value="" placeholder="Buying Car Price" maxlength="10" />

                        <select name="values[unit_price_currency]" id="unit_price_currency" class="input_2" onchange="currency_selector();">
                            <option value="USD" >USD</option>
                            <option value="KRW" >KRW</option>
                            <option value="JPY" >JPY</option>
                            <option value="EUR" >EUR</option>
                        </select>
                        +
                        <input name="values[tax_price]" id="" type="number" class="" value="" placeholder="Tax" maxlength="10" />

                        <select name="values[tax_price_currency]" id="tax_price_currency" class="input_2" onchange="">
                            <option value="USD" disabled>USD</option>
                            <option value="KRW" disabled>KRW</option>
                            <option value="JPY" disabled>JPY</option>
                            <option value="EUR" disabled>EUR</option>
                        </select>

                    </td> 
                </tr>

                <!-- offer hidden value -->
                <input type="hidden" name="values[category]" id="category" value="offer">

                <tr>
                    <th scope="row">AVAILABILITY</th>
                    <td class="text">
                        <select class="select" name="values[icon_status]" style="width:120px;">
                            <option value="sale">Sale</option>
                            <option value="reserved">Reserved</option>  <!-- temporary use -->
                        </select>
                    </td>
                    <th scope="row">CHASSIS NO.</th>
                    <td class="text">
                        <input type="text" name="values[car_chassis_no]" class="b_text" title="ChassisNo">
                    </td>
                </tr>
                <tr>
                    <th scope="row">MAKE</th>
                    <td class="text">
                        <input type="text" id="car_make" name="values[car_make]" value="" class="b_text" title="Maker">
                    </td>
                    <th scope="row">MODEL</th>
                    <td class="text">
                        <input type="text" id="car_model" name="values[car_model]" value="" class="b_text" title="Model">
                    </td>
                </tr>

                <tr>
                    <th scope="row">GRADE</th>
                    <td class="text">
                        <input type="text" name="values[car_grade]" AutocompleteYN="Y" ViewExYN="Y" ViewEx="ex)TLX" class="b_text">
                    </td>

                    <th scope="row">ENGINE SIZE</th>
                    <td class="text"><input type="number" name="values[car_cc]" id="cc" class="b_text" value="" ViewExYN="Y" ViewEx="ex)3000"  maxlength="10" />CC (only number)</td>
                </tr>

                <tr>
                    <th scope="row">MODEL YEAR</th>
                    <td class="text">
                        <input type="number" name="values[car_model_year]" AutocompleteYN="Y" ViewExYN="Y" ViewEx="ex)2012" class="b_text" title="Year">Year (only number)
                    </td>

                    <th scope="row">MANUFACTURED</th>
                    <td class="text">
                        <input type="number" name="values[manu_year]" AutocompleteYN="Y" ViewExYN="Y" ViewEx="ex)2012" class="b_text" title="Year">Year (only number)
                    </td>

                </tr>
                <tr>
					<th scope="row">Registration Date</th>
                    <td class="text">Month
                        <select name="values[first_registration_month]" id="first_registration_month" class="input_2" style="width:120px; height:25px;">
                            <?= func_create_months(); ?>
                        </select>
                        Year
                        <select name="values[first_registration_year]" id="first_registration_year" class="input_2" style="width:120px; height:25px;">
                            <?= func_create_years(); ?>
                        </select>
                    </td>
                    <th scope="row">SEATS</th>
                    <td class="text"><input name="values[car_seat]" id="car_seat" type="number" class="b_text" value="" ViewExYN="Y" ViewEx="ex)4" maxlength="10" />SEATS (only number)</td>

                </tr>
                <tr>
                    <th scope="row">MILEAGE</th>
                    <td class="text"><input type="number" name="values[car_mileage]" id="mileage" class="b_text" value="" ViewExYN="Y" ViewEx="ex)56120"  maxlength="10" />Km (only number)</td>

                    <th scope="row">COLOR</th>
                    <td class="text" colspan="3">
                        <select name="values[car_color]" class="select" id="car_color">
                            <option value="" ></option>
                            <?
                            $slq="SELECT * FROM iw_bbs_color ORDER BY color_name ASC";
                            $sql_query = mysql_query($slq);
                            while($row=mysql_fetch_array($sql_query)){
                            ?>
                            <option value="<?= $row['color_name'] ?>" ><?= $row['color_name'] ?> </option>
                            <?
                            }mysql_free_result($sql_query);
                            ?>
                        </select>
                        <a class="fancybox_color iframe" href="/?c=admin&amp;m=write_color_popup&amp;mcd=<?= $mcd ?>">Add Color</a>
                    </td>
                </tr>
                <tr>
                    <th scope="row">BODY TYPE</th>
                    <td class="text">
                        <select name="values[car_body_type]" class="select">
                            <? foreach(Array(' ','Sedan', 'Hatchback', 'SUV', 'MiniBus', 'Bus', 'Truck', 'HeavyMachine', 'Van', 'BrandNew') as $BodyType ) { ?>
                            <option value="<?= $BodyType ?>"><?= $BodyType ?></option>
                            <? } ?>
                        </select>
                    </td>
                    <th scope="row">FUEL</th>
                    <td class="text">
                        <select name="values[car_fuel]" class="select">
                            <? foreach(Array(' ','Petrol', 'Diesel', 'LPG','LPG + Petrol', 'Hybrid') as $Val ) { ?>
                            <option value="<?= $Val ?>"><?= $Val ?></option>
                            <? } ?>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <th scope="row">TRANSMISSION</th>
                    <td class="text">
                        <select name="values[car_transmission]" class="select">
                            <? foreach(Array(' ','Auto', 'Manual') as $Val ) { ?>
                            <option value="<?= $Val ?>"><?= $Val ?></option>
                            <? } ?>
                        </select>
                    </td>
                    
                    <th scope="row">DRIVE TYPE</th>
                    <td class="text">
                        <select name="values[car_drive_type]" class="select">
                            <? foreach(Array(' ','2WD', '4WD') as $Val ) { ?>
                            <option value="<?= $Val ?>"><?= $Val ?></option>
                            <? } ?>
                        </select>
                    </td>
                </tr>


                <tr>
                    <th scope="row">OPTION</th>
                    <td class="text" colspan="3">
                        <div class="check">
                            <table>
                                <tr>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="ABS" />ABS</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Navigation"/>Navigation</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Roof rack"/> Roof rack</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Airbag(driver side)"/>Airbag(driver side)</label></td>
                                </tr>
                                <tr>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Power Door lock" />Power Door lock</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Smart key" />Smart key</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Airbag(passenger side)" />Airbag(passenger side)</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Power seat(Driver side)" />Power seat(Driver side)</label></td>
                                </tr>
                                <tr>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Sunroof" />Sunroof</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Rear camera" />Rear camera</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="USB jack" />USB jack</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Rear spoiler" />Rear spoiler</label></td>
                                </tr>
                                <tr>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="VDC" />VDC</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Alloy wheels" />Alloy wheels</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Back warning system" />Back warning system</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="CD player" />CD player</td>
                                </tr>
                                <tr>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Curtain airbag" />Curtain airbag</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Dual Sunroof" />Dual Sunroof</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Full auto airconditioner" />Full auto airconditioner</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Russia" />Heating</label></td>
                                </tr>
                                <tr>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Leather seats" />Leather seats</label></td>
                                    <td><label><input type="checkbox" name="op_checkbox[]" value="Monitor" />Monitor</label></td>
                                </tr>
                            </table>                          

                        </div>
                    </td>
                </tr>

                <tr>
                    <th scope="row">FOB COST</th>
                    <td class="text"><input name="values[car_fob_cost]" id="car_fob_cost" type="number" class="b_text" value="" ViewExYN="Y" ViewEx="ex)3000" maxlength="10" />
                        <select name="values[car_fob_currency]" id="currency_type" class="input_2" onchange="">
                            <option value="USD" >USD</option>
                            <option value="KRW" >KRW</option>
                            <option value="JPY" >JPY</option>
                            <option value="EUR" >EUR</option>
                        </select>
                    </td>

                </tr>
<!--                     <tr>
                <th scope="row">DISCOUNT PRICE</th>
                <td class="text"><input name="values[car_fob_discount]" id="car_fob_discount" type="text" class="b_text" value="" ViewExYN="Y" ViewEx="ex)2000" maxlength="10" />$ (only number) </td>

                <th scope="row"></th>
                <td class="text"></td>
            </tr> -->
                <tr>
                    <th scope="row">Files</th>
                    <td class="text" colspan="3">
                        <div><a href="#file_popup" class="fancybox"><span style="color:blue;">Click</span></a></div>
                        <div id="extract_pdf_loading" style="margin-top: 5px; display:none;">
                            <div style="float: left;"><img src="images/admin/loading-bar.gif"/></div>
                            <div style="float: left; padding: 0px 10px;"><label style="color:#2D7DC0;">Extracting images from PDF</label></div>
                        </div>
                    </td>
                </tr>
             <!-- <tr>                    <td class="text" colspan="3">
                        <a href="#car_stat
                 <td colspan="4" class="text_info">* Memo</td>
             </tr>
             <tr>
                 <td colspan="4">
                     <textarea name="values[contents]" id="contents" style="width:100%;height:200px;"></textarea>
                         <script type="text/javascript">
                             tiny_mce_load('contents');
                         </script>
                 </td>
             </tr> -->
                <!-- <tr>
                    <th scope="row">Image File</th>
                    <td class="text" colspan="3">
                        <? for($ifile=1; $ifile <=1; $ifile ++){ ?>
                        <div style="padding:1px; 0px;">
                            <input type="file" name="attach<?= $ifile ?>[]" class="b_text" multiple="multiple" />
                            <? if($ifile == 10){ ?>
                            <div style="color:red;">only more than ie10 or chrome, safari, firefox browser.</div>
                            <? }?>
                        </div>
                        <? } ?>   

                    </td>

                </tr> -->
                <tr>
                    <th scope="row">Images</th>
                    <td class="text" id="mynewtdtesting1" colspan="3">
                        <iframe class="upload_iframe" src="<?= base_url() ?>?c=admin&m=adm_image_upload&idx=<?=$idx?>&mcd=product"></iframe>
                    </td> 

                </tr>
                </tbody>
            </table>
            <div class="btn_area_center">
                <input type="image" src="/images/admin/submit.gif" title="Check" />
                <a href="/?c=admin&amp;m=adm_bbs_list&amp;mcd=<?= $mcd ?>" title="List">
                    <img src="/images/admin/back.gif" alt="목록" /></a>
            </div>
        </div>
    </div>
    <div style="display:none">
        <div id="file_popup" style="height: 340px;width: 270px;">
            <center><h2>Car File Attachments</h2>
                <h4>Please upload your files here</h4></center><br/>
                
            <div id="popup0"​ style="margin-bottom: 10px;">
                <label for="pass"><b>Inspection Sheet</b></label>
                <input type="file" id="myuploadvideoyoutube"  name="file_type_sheet0" value="" style="margin-top: 2px;"/>
                <input type="button" value="X" id="clear0">

            </div>
            <div id="popup1" style="margin-bottom: 10px;">
                <label for="pass" style="width: 186px;"><b>Export Certification</b></label>
                <input type="file"  name="file_type_sheet1[]" value="" style="margin-top: 2px;" />
                <input type="button" value="X" id="clear1">
            </div>
            <div id="popup2" style="margin-bottom: 10px;">
                <label style="width: 186px;"><b>Cancellation Certification</b></label>
                <input type="file" name="file_type_sheet2[]" value="" style="margin-top: 2px;" />
                <input type="button" value="X" id="clear2">
            </div>
            <div id="popup3" style="margin-bottom: 10px;">
                <label for="pass"><b>Car Registeration</b></label>
                <input type="file"  name="file_type_sheet3[]" value="" style="margin-top: 2px;"/>
                <input type="button" value="X" id="clear3">
            </div>
            <div id="popup4" style="margin-bottom: 10px;">
                <label for="pass"><b>Original Register</b></label>
                <input type="file"  name="file_type_sheet4[]" value="" style="margin-top: 2px;" />
                <input type="button" value="X" id="clear4">
            </div><br/>
            <input type="button" id="close" value="Ok" style="width: 60px;">
        
        </div>
    
    </div>
    <div id="myerror" style="background-color:#FFF;" >
   
        </div>
</form>

    <!-- panel with buttons -->
    <!-- popup form #1 -->
    
    



<script type="text/javascript">

    $(function () {

        $("#save").click(function () {
            $('.popup').hide();
        });


        $("#clear0").click(function () {
            $('#popup0').find('input:file').val('');
        });
        $("#clear1").click(function () {
            $('#popup1').find('input:file').val('');
        });
        $("#clear2").click(function () {
            $('#popup2').find('input:file').val('');
        });
        $("#clear3").click(function () {
            $('#popup3').find('input:file').val('');
        });
        $("#clear4").click(function () {
            $('#popup4').find('input:file').val('');
        });
        // $('iframe').load(function(){
        //     alert($('iframe').contents().height());
        //     //alert($("iframe").css("height", $('iframe').contents().height()));
        // });

    });
    function resizeIframe(height){
        $("iframe").css("height", height);
    }
    $(document).ready(function(e){
        $("a.fancybox").fancybox({
            height  : "61%",
            width   : "25%"
        });  
        $("a.fancybox_color").fancybox({
            height  : "61%",
            width   : "25%",
            onClosed: function() { 
                reloadColorSelect();
            }
        });
		
		var id = <?php echo $idx;?>;
		
		$("#close").click(function () {
			
			// var input = document.getElementById('myuploadvideoyoutube');
			// file = input.files[0];
			// if (file !=undefined){
			// 	formData = new FormData();
			// 	formData.append("pdffile",file);
			// 	formData.append("id",id);
			// 	$("#extract_pdf_loading").css("display", "block");
			// 	$.ajax({
			// 			url:"/?c=json&m=myjsonpost&mcd=product",
			// 			type:"POST",
			// 			data: formData,
			// 			processData:false,
			// 			contentType:false,
			// 			success: function(data,status){
			// 				//var getuploadvideo = data;
			// 				//var getdata = JSON.parse(data);
			// 				$('.upload_iframe').attr("src", $('.upload_iframe').attr("src"));
			// 				$("#extract_pdf_loading").css("display", "none");
			// 			}
				
			// 	});
			
			// }
			
            $.fancybox.close();
        });  
    });

    function reloadColorSelect(){
        $.ajax({
            method: "GET",
            url: "/?c=admin&m=adm_get_color_options"
            })
            .done(function( data ) {
                $("#car_color").html(data);
        });
        
    }

</script>