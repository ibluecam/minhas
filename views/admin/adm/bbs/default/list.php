<script type="text/javascript">
//게시판 체크박스 전체 선택
function checkbox_all_check(obj, target) {
	if ( typeof obj == 'undefined' || typeof target == 'undefined') {
	}
	else if ( typeof target.length == 'undefined' ) {
		target.checked = obj.checked;
	}
	else
	{
		for (i = 0 ; i < target.length ; i++) {
			target[i].checked = obj.checked;
		}
	}
}

//전체 삭제
function delete_all() {
	var f  = document.adm_frm;
	var fg = false;

	if(f.idx != null) {
		for(i=0; i<f.idx.length; i++) {
			if(f.idx[i].checked) {
				fg = true;
			}
		}

		if(!fg) {
			alert('게시물을 선택하세요.');
			return;
		}

		f.action = '/?c=admin&m=adm_bbs_all_delete&mcd=<?= $mcd ?>';
		f.submit();
	}
}

</script>
<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_bbs_list&amp;mcd=<?= $mcd ?>" onsubmit="return search_bbs(this);">
	<div class="contents_box_middle">
		<h1 class="title">게시판 관리</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 등록된 게시물을 관리 할 수 있습니다.</li>
			</ul>
		</div>
		<!--<div style="padding-top:5px;text-align:right;"><img src="/images/admin/btn_reset.gif" alt="초기화" /></div>-->
		<div class="table_list_box" style="padding-top:10px;">
			<h1 class="table_title"><?= func_get_config($bbs_config_result, 'bbs_code_name') ?></h1>
			<div class="search_box">
				<div class="search_result">
					<img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />전체 : <span class="title"><?= $total_rows ?></span> 건, 페이지 : <span class="title"><?= $cur_page ?></span>/<?= $total_page ?>
				</div>
				<div class="search_condition">
					<input type="text" name="sch_create_dt_s" id="sch_create_dt_s" value="" class="input_1" style="width:65px;" readonly />
					<a href="#" onclick="Calendar(document.adm_frm.elements['sch_create_dt_s']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>&nbsp;~
					<input type="text" name="sch_create_dt_e" id="sch_create_dt_e" value="" class="input_1" style="width:65px;" readonly />
					<a href="#" onclick="Calendar(document.adm_frm.elements['sch_create_dt_e']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
					<? //카테고리 사용 여부
					if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {
						$category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));
						$category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));
					?>
					<select name="category" id="category" class="input_2" align="absmiddle">
						<option value="">::선택하세요::</option>
						<? if (count($category_code) > 0) {
						for ($i = 0; $i < count($category_code); $i++) {
						?>
						<option value="<?= $category_code[$i] ?>"><?= $category_name[$i] ?></option>
						<? } ?>
						<? } ?>
					</select>
					<? } ?>
					<select name="sch_condition" id="sch_condition" class="input_2" align="absmiddle">
						<option value="">::선택하세요::</option>
						<option value="subject like">제목</option>
						<option value="contents like">내용</option>
						<option value="subject like|contents like">제목+내용</option>
					</select>
					<input type="text" name="sch_word" id="sch_word" value="" class="input_1" style="width:100px;" title="검색어" />
					<input type="image" src="/images/admin/btn_search_01.gif" title="검색" align="absmiddle" />
				</div>
			</div>
			<table class="table_list" summary="<?= func_get_config($bbs_config_result, 'bbs_code_name') ?>" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="4%" /><col width="2px" />
				<col width="6%" /><col width="2px" />
				<col width="" /><col width="2px" />
				<?= func_decode(func_get_config($bbs_config_result, 'category_yn'), "Y", "<col width=\"11%\" /><col width=\"2px\" />", "") ?>
				<?= func_decode(func_get_config($bbs_config_result, 'reply_exp_yn'), "Y", "<col width=\"7%\" /><col width=\"2px\" />", "") ?>
				<col width="9%" /><col width="2px" />
				<col width="9%" /><col width="2px" />
				<col width="6%" />
				<thead>
					<tr>
						<th scope="col"><input type="checkbox" name="idx" id="idx" value="" onclick="checkbox_all_check(this, document.adm_frm.idx);"/></th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">번호</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">제목</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<?= func_decode(func_get_config($bbs_config_result, 'category_yn'), "Y", "<th scope=\"col\">" . func_get_config($bbs_config_result, 'category_title', '카테고리') . "</th>\n<th scope=\"col\" class=\"line\"><img src=\"/images/admin/board_bar_line.gif\"></th>", "") ?>
						<?= func_decode(func_get_config($bbs_config_result, 'reply_exp_yn'), "Y", "<th scope=\"col\">답변여부</th>\n<th scope=\"col\" class=\"line\"><img src=\"/images/admin/board_bar_line.gif\"></th>", "") ?>
						<th scope="col">작성자</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">작성일</th>
						<th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
						<th scope="col">조회수</th>
					</tr>
				</thead>
				<tbody>
					<? //공지글
					if (count($bbs_notice_list) > 0) {
						$bbs_link = ''; //링크
						$subject = ''; //제목
						foreach ($bbs_notice_list as $rows) {
							//링크
							$bbs_link = "/?c=admin&m=adm_bbs_detail&mcd=$mcd&idx=$rows->idx&cur_page=$cur_page" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);

							//제목 길이 제한
							if (func_get_config($bbs_config_result, 'subject_limit') != '0') {
								$subject = func_cutstr($rows->subject, func_get_config($bbs_config_result, 'subject_limit'));
							} else {
								$subject = $rows->subject;
							}
							?>
							<tr>
								<td><input type="checkbox" name="idx[]" id="idx" value="<?= $rows->idx ?>" /></td>
								<td class="line">&nbsp;</td>
								<td><img src="<?= $skin_images_url ?>/con_notice_icon.gif" border="0" alt="공지" /></td>
								<td class="line">&nbsp;</td>
								<td style="text-align:left;font-weight:bold;"><a href="<?= $bbs_link ?>" title="내용보기"><?= $subject ?></a></td>
								<td class="line">&nbsp;</td>
								<?= func_decode(func_get_config($bbs_config_result, 'category_yn'), "Y", "<td>&nbsp;</td>\n<td class=\"line\">&nbsp;</td>", "") ?>
								<?= func_decode(func_get_config($bbs_config_result, 'reply_exp_yn'), "Y", "<td>&nbsp;</td>\n<td class=\"line\">&nbsp;</td>", "") ?>
								<td style="font-weight:bold;"><?= $rows->writer ?></td>
								<td class="line">&nbsp;</td>
								<td style="font-weight:bold;"><?= func_get_config_date(func_get_config($bbs_config_result, 'date_format'), $rows->created_dt) ?></td>
								<td class="line">&nbsp;</td>
								<td style="font-weight:bold;"><?= $rows->visit_cnt ?></td>
							</tr>
						<? } ?>
					<? } ?>
					<?
					//일반글
					if (count($bbs_list) > 0) {

						$bbs_link = ''; //링크
						$subject = ''; //제목
						$level_str = ''; //자식글
						foreach ($bbs_list as $rows) {
							//링크
							$bbs_link = "/?c=admin&m=adm_bbs_detail&mcd=$mcd&idx=$rows->idx&cur_page=$cur_page" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);

							//자식글
							$level_str = funct_level_str($rows->depth, $mcd);

							//제목 길이 제한
							if (func_get_config($bbs_config_result, 'subject_limit') != '0') {
								$subject = func_cutstr($rows->subject, func_get_config($bbs_config_result, 'subject_limit'));
							} else {
								$subject = $rows->subject;
							}

							//Secret 아이콘
							$secret_icon = '';
							if (func_get_config($bbs_config_result, 'secret_yn') == 'Y' && $rows->secret_yn == 'Y')
								$secret_icon = func_secret_icon($mcd);

							//New, Hot 아이콘
							$icon = '&nbsp;&nbsp;' . func_new_icon(func_get_config($bbs_config_result, 'icon_new'), $rows->created_dt, $mcd) . '&nbsp;&nbsp;' . func_hot_icon(func_get_config($bbs_config_result, 'icon_hot'), $rows->visit_cnt, $mcd);
							?>
							<tr>
								<td><input type="checkbox" name="idx[]" id="idx" value="<?= $rows->idx ?>" /></td>
								<td class="line">&nbsp;</td>
								<td><?= $row_cnt-- ?></td>
								<td class="line">&nbsp;</td>
								<td style="text-align:left;">
									<?= $secret_icon ?>
									<?= $level_str ?>
									<a href="<?= $bbs_link ?>" title="내용보기"><?= $subject ?></a>
									<?= func_decode($rows->comment_cnt, '0', '', '(' . $rows->comment_cnt . ')') ?>
									<?= $icon ?>
								</td>
								<td class="line">&nbsp;</td>
								<? if (func_get_config($bbs_config_result, 'category_yn') == 'Y') { //카테고리 ?>
									<td><?= func_get_gategory(func_get_config($bbs_config_result, 'category_code'), func_get_config($bbs_config_result, 'category_name'), $rows->category) ?></td>
									<td class="line">&nbsp;</td>
								<? } ?>
								<? if (func_get_config($bbs_config_result, 'reply_exp_yn') == 'Y') { //답변여부 ?>
									<td><?= func_decode($rows->reply_yn, 'Y', '<strong>답변완료</strong>', '답변대기') ?></td>
									<td class="line">&nbsp;</td>
								<? } ?>
								<td><?= $rows->writer ?></td>
								<td class="line">&nbsp;</td>
								<td><?= func_get_config_date(func_get_config($bbs_config_result, 'date_format'), $rows->created_dt) ?></td>
								<td class="line">&nbsp;</td>
								<td><?= $rows->visit_cnt ?></td>
							</tr>
						<? } ?>
					<? } ?>
					<? if (count($bbs_notice_list) == 0 && count($bbs_list) == 0) { //데이터가 존재하지 않을 경우 ?>
					<tr>
						<td colspan="20" class="no_data">Data does not exist.</td>
					</tr>
					<? } ?>
				</tbody>
			</table>
			<div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_bbs_list&mcd=' . $mcd, $mcd) ?></div>
			<div class="btn_area_right">
				<a href="/?c=admin&amp;m=adm_bbs_write&amp;mcd=<?= $mcd ?>" title="등록"><img src="/images/admin/btn_regist_01.gif" alt="등록" /></a>
			</div>
			<div class="search_box" style="padding:15px 10px;background-color:#ffffff">
				<img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />선택한 게시물을
				<a href="#" onclick="delete_all();" title="삭제"><img src="/images/admin/btn_del.gif" alt="삭제" align="absmiddle" /></a>
			</div>
		</div>
	</div>
</form>