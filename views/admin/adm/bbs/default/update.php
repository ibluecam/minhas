<?
//게시판 상세 조회
$rows = '';
if (count($bbs_detail_list) > 0) {
	$rows = $bbs_detail_list->row();
}
?>
<script type="text/javascript">
//게시판 수정
function update_adm_bbs(frm) {
	if(!validate(frm.elements['subject'], '제목을 입력하세요.')) return false;
	if(!emailCheck(frm.elements['email'])) return false; //이메일 체크

	<? if (func_get_config($bbs_config_result, 'reply_yn') == 'Y' || func_get_config($bbs_config_result, 'secret_yn') == 'Y') { //답변기능, 비밀글기능 여부 ?>
	//일반글이면.. 비밀번호 입력
	if(frm.elements['notice_yn'][0].checked) {
		frm.elements['password'].disabled = false;
		if(!validate(frm.elements['password'], '비밀번호를 입력하세요.')) return false;
	} else {
		frm.elements['password'].disabled = true;
	}
	<? } ?>

	<? if (func_get_config($bbs_config_result, 'editor_yn') == 'Y') { //게시판 HTML 에디터를 사용할 경우..  ?>
	if(tinyMCE.get('contents').getContent() == '' || tinyMCE.get('contents').getContent() == '<p>&nbsp;</p>') {
		alert("내용을 입력하세요.");
		return false;
	}
	<? } else { ?>
	if(!validate(frm.elements['contents'], '내용을 입력하세요.')) return false;
	<? } ?>

	<? if (func_get_config($bbs_config_result, 'survey_yn') == 'Y') { // 설문조사 사용할 경우  ?>
	if(!validate(frm.elements['values[publish_start]'], '설문조사 시작일을 입력하세요')) return false;
	if(!validate(frm.elements['values[publish_end]'], '설문조사 종료일을 입력하세요')) return false;
	<? } ?>

	return true;
}

//게시판 첨부파일 삭제
function delete_bbs_attach(att_idx) {
	if(confirm('삭제하시겠습니까?')) {
		location.href = "/?c=admin&amp;m=adm_bbs_update&amp;mcd=<?= $mcd ?>&amp;idx=<?= $idx ?>&att_idx=" + att_idx + "&amp;only_attach=Y&amp;cur_page=<?= $cur_page ?>&amp;sParam=<?= urlencode($sParam) ?>";
	}
}

//설문조사 항목 삭제
function survey_delete(survey_no) {
	if(confirm('삭제하시겠습니까?\n저장되지 않은 정보는 사라집니다!')) {
		location.href = "/?c=admin&m=adm_bbs_update&mcd=<?=$mcd?>&idx=<?=$idx?>&survey_no="+survey_no+"&only_survey=Y&cur_page=<?=$cur_page?>&sParam=<?=urlencode($sParam)?>";
	}
}
//설문조사 항목 추가
function survey_create(survey_cnt) {
	if(confirm('생성하시겠습니까?\n저장되지 않은 정보는 사라집니다!')) {
		location.href = "/?c=admin&m=adm_bbs_update&mcd=<?=$mcd?>&idx=<?=$idx?>&survey_cnt="+survey_cnt+"&cur_page=<?=$cur_page?>&sParam=<?=urlencode($sParam)?>";
	}
}

</script>
<form name="adm_frm" id="adm_frm" method="post" enctype="multipart/form-data" action="/?c=admin&amp;m=adm_bbs_update_exec&amp;mcd=<?= $mcd ?>" onsubmit="return update_adm_bbs(this);">
	<input type="hidden" name="idx" value="<?= $idx ?>" />
	<input type="hidden" name="cur_page" value="<?= $cur_page ?>" />
	<input type="hidden" name="sParam" value="<?= $sParam ?>" />
	<div class="contents_box_middle">
		<h1 class="title">게시판 관리</h1>
		<div class="tip_box">
			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
			<ul>
				<li> - 등록된 게시물을 관리 할 수 있습니다.</li>
			</ul>
		</div>
		<div class="table_write_box">
			<h1 class="table_title"><?= func_get_config($bbs_config_result, 'bbs_code_name') ?> 수정</h1>
			<table class="table_write" summary="<?= func_get_config($bbs_config_result, 'bbs_code_name') ?>" cellspacing="0">
				<caption class="hidden"></caption>
				<col width="18%" /><col width="32%" /><col width="18%" /><col width="32%" />
				<tr>
					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />제목</th>
					<td class="value_02" colspan="3">
						<input type="text" name="values[subject]" id="subject" value="<?= $rows->subject ?>" class="input_1" style="width:395px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="200" />
					</td>
				</tr>
				<tr>
					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />작성자</th>
					<td class="value_02">
						<input type="text" name="values[writer]" id="writer" value="<?= $rows->writer ?>" class="input_1" style="width:200px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="10" />
					</td>
					<th class="key_02">작성일</th>
					<td class="value_02">
						<input type="text" name="values[created_dt]" id="created_dt" value="<?= func_get_config_date(func_get_config($bbs_config_result, 'date_format'), $rows->created_dt) ?>" class="input_1" style="width:70px;background-color:#ededed;" readonly />
						<a href="#" onclick="Calendar(document.adm_frm.elements['values[created_dt]']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
					</td>
				</tr>
				<tr>
					<th class="key_02">이메일</th>
					<td class="value_02">
						<input type="text" name="values[email]" id="email" value="<?= $rows->email ?>" class="input_1" style="width:200px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="50" />
					</td>
					<th class="key_02">전화번호</th>
					<td class="value_02">
						<input type="text" name="values[phone_no1]" id="phone_no1" value="<?= $rows->phone_no1 ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, adm_frm.elements["values[phone_no2]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
						<input type="text" name="values[phone_no2]" id="phone_no2" value="<?= $rows->phone_no2 ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, adm_frm.elements["values[phone_no3]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
						<input type="text" name="values[phone_no3]" id="phone_no3" value="<?= $rows->phone_no3 ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" />
					</td>
				</tr>
				<tr>
					<th class="key_02">공지여부</th>
					<td class="value_02">
						<input type="radio" name="values[notice_yn]" id="notice_yn" value="N" class="radio" <?= func_decode($rows->notice_yn, 'N', 'checked', '') ?> /> 일반
						<input type="radio" name="values[notice_yn]" id="notice_yn" value="Y" class="radio" <?= func_decode($rows->notice_yn, 'Y', 'checked', '') ?> /> 공지
					</td>
					<th class="key_02">휴대폰번호</th>
					<td class="value_02" colspan="3">
						<input type="text" name="values[mobile_no1]" id="mobile_no1" value="<?= $rows->mobile_no1 ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, adm_frm.elements["values[mobile_no2]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
						<input type="text" name="values[mobile_no2]" id="mobile_no2" value="<?= $rows->mobile_no2 ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onkeyup='focusMove(this, adm_frm.elements["values[mobile_no3]"]);' onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" /> -
						<input type="text" name="values[mobile_no3]" id="mobile_no3" value="<?= $rows->mobile_no3 ?>" class="input_1" style="width:40px;ime-mode:disabled;" onkeypress="onlyNum();" onfocus="f_text(this);" onblur="b_text(this);" maxlength="4" />
					</td>
				</tr>
				<? if (func_get_config($bbs_config_result, 'secret_yn') == 'Y') { //비밀글기능 여부 ?>
				<tr>
					<th class="key_02">공개여부</th>
					<td class="value_02" colspan="3">
						<input type="radio" name="values[secret_yn]" id="secret_yn" value="N" class="radio" <?= func_decode($rows->secret_yn, 'N', 'checked', '') ?> /> 공개
						<input type="radio" name="values[secret_yn]" id="secret_yn" value="Y" class="radio" <?= func_decode($rows->secret_yn, 'Y', 'checked', '') ?> /> 비공개
					</td>
				</tr>
				<? } ?>

				<? if (func_get_config($bbs_config_result, 'reply_yn') == 'Y' || func_get_config($bbs_config_result, 'secret_yn') == 'Y') { //답변기능, 비밀글기능 여부 ?>
				<tr>
					<th class="key_02">비밀번호</th>
					<td class="value_02" colspan="3">
						<input type="password" name="values[password]" id="password" value="<?= $rows->password ?>" class="input_1" style="width:200px;" title="비밀번호" maxlength="12"  />
					</td>
				</tr>
				<? } ?>

				<? if(func_get_config($bbs_config_result, 'survey_yn') == 'Y') { // 설문조사 사용할 경우  ?>
				<tr>
					<th class="key_02">시행일</th>
					<td class="value_02" colspan="3">
						시작:<input type="text" class="input" name="values[publish_start]" id="publish_start" value="<?=substr($rows->publish_start, 0, 10) ?>" maxlength="10" size="10" readonly="readonly"/>
						<a href="#" onclick="Calendar(document.adm_frm.elements['values[publish_start]']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
						종료:<input type="text" class="input" name="values[publish_end]" id="publish_end" value="<?=substr($rows->publish_end, 0, 10) ?>" maxlength="10" size="10" readonly="readonly"/>
						<a href="#" onclick="Calendar(document.adm_frm.elements['values[publish_end]']); return false;" title="날짜조회"><img src="/images/admin/calendar_icon.gif" alt="날짜" align="middle"/></a>
					</td>
				</tr>
				<? } ?>

				<? //카테고리 사용 여부
				if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {
				$category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));
				$category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));
				?>
				<tr>
					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" /><?= func_get_config($bbs_config_result, 'category_title', '카테고리') ?></th>
					<td class="value_02" colspan="3">
						<select name="values[category]" id="category" class="input_2" style="width:150px;">
						<? if (count($category_code) > 0) {
						for ($i = 0; $i < count($category_code); $i++) {
						?>
						<option value="<?= $category_code[$i] ?>" <?= func_decode($rows->category, $category_code[$i], 'selected', '') ?>><?= $category_name[$i] ?></option>
						<? } ?>
						<? } ?>
						</select>
					</td>
				</tr>
				<? } ?>

				<? if (func_get_config($bbs_config_result, 'main_summary_yn') == 'Y') { //메인 Summary 표시 여부 ?>
				<tr>
					<th class="key_02">메인노출여부</th>
					<td class="value_02" colspan="3">
						<input type="radio" name="values[main_summary_yn]" id="main_summary_yn" value="N" class="radio" checked /> 미노출
						<input type="radio" name="values[main_summary_yn]" id="main_summary_yn" value="Y" class="radio" /> 노출
					</td>
				</tr>
				<? } ?>
				<tr>
					<th class="key_02"><img src="/images/admin/needs_icon.gif" alt="아이콘" />내용</th>
					<td class="value_02" colspan="3" style="padding:10px;">
						<textarea name="values[contents]" id="contents" class="textarea"><?= func_decode(func_get_config($bbs_config_result, 'editor_yn'), 'Y', $rows->contents, $rows->contents) ?></textarea>
						<? if (func_get_config($bbs_config_result, 'editor_yn') == 'Y') { //게시판 HTML 에디터를 사용할 경우..  ?>
								<script type="text/javascript">
								//<!--
								tiny_mce_load('contents', '300px');
								//-->
								</script>
						<? } ?>
					</td>
				</tr>

				<? if (func_get_config($bbs_config_result, 'attach_file_yn') == 'Y') { //첨부파일을 사용할 경우 ?>
				<tr>
					<th class="key_02">첨부파일</th>
					<td class="value_02" colspan="3">
						<?= func_create_attach_files($bbs_attach_list, $mcd, 'Y') ?>
						<? //첨부파일 개수
						$attach_file_cnt = func_get_config($bbs_config_result, 'attach_file_cnt');
						for ($i = 1; $i <= (integer) $attach_file_cnt - (integer) count($bbs_attach_list); $i++) {
						?>
						<div style="padding:1px; 0px;"><input type="file" name="attach<?= $i ?>" class="input_2" style="width:400px;" /></div>
						<? } ?>
					</td>
				</tr>
				<? } ?>

				<? if (func_get_config($bbs_config_result, 'survey_yn') == 'Y') { // 설문조사 사용할 경우 ?>
				<tr>
					<th class="key_02">기본문항</th>
					<td class="value_02" colspan="3">
						1:<input type="text" class="input" name="survey[custom][1][title]" value="<?=$survey['custom'][1]->title?>"><br/>
						2:<input type="text" class="input" name="survey[custom][2][title]" value="<?=$survey['custom'][2]->title?>"><br/>
						3:<input type="text" class="input" name="survey[custom][3][title]" value="<?=$survey['custom'][3]->title?>"><br/>
						4:<input type="text" class="input" name="survey[custom][4][title]" value="<?=$survey['custom'][4]->title?>"><br/>
						5:<input type="text" class="input" name="survey[custom][5][title]" value="<?=$survey['custom'][5]->title?>"><br/>
					</td>
				</tr>
				<?
				$quest = '';
				foreach($survey['field'] as $Skey => $Sval) {
					//$Sval->type->{$Sval->type} = 'selected="selected"';
					$quest .= '<div class="item">';
					$quest .= '	<div class="number"><strong>'.$Skey.'번</strong><br><a href="#" onclick="survey_delete('.$Skey.')">삭제</a></div>';
					$quest .= '	<div class="title">제목 <input type="text" class="input" name="survey[field]['.$Skey.'][title]" value="'.$Sval->title.'" size="70"></div>';
					$quest .= '	<div class="cont">설명 <input type="text" class="input" name="survey[field]['.$Skey.'][cont]" value="'.$Sval->cont.'" size="70"></div>';
					$quest .= '	<div class="type">형태 <select class="select" name="survey[field]['.$Skey.'][type]">';

					if($Sval->type == 'input') {
						$quest .= '		<option value="input" selected="selected">주관식</option>';
					} else {
						$quest .= '		<option value="input">주관식</option>';
					}
					if($Sval->type == 'radio') {
						$quest .= '		<option value="radio" selected="selected">객관식-단일선택</option>';
					} else {
						$quest .= '		<option value="radio">객관식-단일선택</option>';
					}
					if($Sval->type == 'radio|input') {
						$quest .= '		<option value="radio|input" selected="selected">객관식-단일선택+기타</option>';
					} else {
						$quest .= '		<option value="radio|input">객관식-단일선택+기타</option>';
					}
					if($Sval->type == 'checkbox') {
						$quest .= '		<option value="checkbox" selected="selected">객관식-다중선택</option>';
					} else {
						$quest .= '		<option value="checkbox">객관식-다중선택</option>';
					}
					if($Sval->type == 'checkbox|input') {
						$quest .= '		<option value="checkbox|input" selected="selected">객관식-다중선택+기타</option>';
					} else {
						$quest .= '		<option value="checkbox|input">객관식-다중선택+기타</option>';
					}

					$quest .= '	</select></div>';
					$quest .= '	<div class="quest">문제 <input type="text" class="input" name="survey[field]['.$Skey.'][quest]" value="'.$Sval->quest.'" size="70"></div>';
					$quest .= '</div>';
				}
				?>
				<tr>
					<th class="key_02">문항생성</th>
					<td class="value_02" colspan="3">
						총<input type="text" class="input" name="value[survey_cnt]" value="<?=count($survey['field'])?>"> 문항 <input type="button" value="생성" onclick="survey_create(adm_frm.elements['value[survey_cnt]'].value)" /><br/>문항의 문제는 콤마(,) 또는 파이프(|) 문자로 구분
					</td>
				</tr>
				<tr>
					<th class="key_02">문항</th>
					<td class="value_02" colspan="3">
						<style>
						#survey .item { border:solid 1px silver; margin:1px 0 1px 0; background-color:white;}
						#survey .number { position:absolute; width:30px; text-align:center; margin:5px 0 0 5px; }
						#survey div { margin:1px 0 1px 40px;}
						</style>
						<div id="survey"><?=$quest?></div>
					</td>
				</tr>
				<? } ?>
			</table>
			<div class="btn_area_center">
				<input type="image" src="/images/admin/btn_ok.gif" title="확인" />
				<a href="/?c=admin&amp;m=adm_bbs_list&amp;mcd=<?= $mcd ?>&amp;cur_page=<?= $cur_page ?><?= func_sPUrlEncode($sParam, "sch_word") ?>" title="목록"><img src="/images/admin/btn_list.gif" alt="목록" /></a>
			</div>
		</div>
	</div>
</form>