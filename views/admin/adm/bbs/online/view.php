<?
//게시판 상세 조회
$rows = '';
if ($bbs_detail_list->num_rows() > 0) {
	$rows = $bbs_detail_list->row();
}
?>
<script type="text/javascript">

//게시판 수정
function update_adm_bbs() {
	location.href = "/?c=admin&m=adm_bbs_update&mcd=<?= $mcd ?>&idx=<?= $idx ?>&cur_page=<?= $cur_page ?>&sParam=<?= urlencode($sParam) ?>";
}

//게시판 답변
function reply_adm_bbs() {
	location.href = "/?c=admin&m=adm_bbs_reply&mcd=<?= $mcd ?>&idx=<?= $idx ?>&cur_page=<?= $cur_page ?>&sParam=<?= urlencode($sParam) ?>";
}

//게시판 삭제
function delete_adm_bbs() {
	if(!confirm('삭제 하시겠습니까?')) {
		return;
	}
	else
	{
		location.href = "/?c=admin&m=adm_bbs_delete&mcd=<?= $mcd ?>&idx=<?= $idx ?>&cur_page=<?= $cur_page ?>&sParam=<?= urlencode($sParam) ?>";
	}
}

</script>
<div class="contents_box_middle">
	<h1 class="title">게시판 관리</h1>
	<div class="tip_box">
		<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>
		<ul>
			<li> - 등록된 게시물을 관리 할 수 있습니다.</li>
		</ul>
	</div>
	<div class="table_view_box">
		<h1 class="table_title"><?= func_get_config($bbs_config_result, 'bbs_code_name') ?> 상세</h1>
		<table class="table_view_box" summary="<?= func_get_config($bbs_config_result, 'bbs_code_name') ?>" cellspacing="0">
			<caption class="hidden"></caption>
			<col width="18%" /><col width="32%" /><col width="18%" /><col width="32%" />
			<tr>
				<th class="key_01">제목</th>
				<td class="value_01" colspan="3"><strong><?= $rows->subject ?></strong></td>
			</tr>
			<tr>
				<th class="key_01">작성자</th>
				<td class="value_01"><?= $rows->writer ?></td>
				<th class="key_01">작성일</th>
				<td class="value_01"><?= func_get_config_date(func_get_config($bbs_config_result, 'date_format'), $rows->created_dt) ?></td>
			</tr>
			<tr>
				<th class="key_01">이메일</th>
				<td class="value_01"><?= func_decode($rows->email, '', '--', $rows->email) ?></td>
				<th class="key_01">전화번호</th>
				<td class="value_01"><?= func_decode($rows->phone_no, '', '--', $rows->phone_no) ?></td>
			</tr>
			<tr>
				<th class="key_01">공지여부</th>
				<td class="value_01"><?= func_decode($rows->notice_yn, 'Y', '공지글', '일반글') ?></td>
				<th class="key_01">휴대폰번호</th>
				<td class="value_01"><?= func_decode($rows->mobile_no, '', '--', $rows->mobile_no) ?></td>
			</tr>
			<? if(func_get_config($bbs_config_result, 'survey_yn') == 'Y') { // 설문조사 사용할 경우  ?>
			<tr>
				<th class="key_01">시작</th>
				<td class="value_01"><?=$rows->publish_start?></td>
				<th class="key_01">종료</th>
				<td class="value_01"><?=$rows->publish_end?></td>
			</tr>
			<? } ?>
			<? if (func_get_config($bbs_config_result, 'category_yn') == 'Y') { //카테고리 ?>
				<tr>
					<th class="key_01"><?= func_get_config($bbs_config_result, 'category_title', '카테고리') ?></th>
					<td class="value_01" colspan="3"><?= func_get_gategory(func_get_config($bbs_config_result, 'category_code'), func_get_config($bbs_config_result, 'category_name'), $rows->category) ?></td>
				</tr>
			<? } ?>
			<? if (func_get_config($bbs_config_result, 'attach_file_yn') == 'Y') { //첨부파일을 사용할 경우  ?>
			<tr>
				<th class="key_01">첨부파일</th>
				<td class="value_01" colspan="3">
					<?= func_create_attach_files($bbs_attach_list, $mcd) ?>
				</td>
			</tr>
			<? } ?>
			<tr>
				<th class="key_01">내용</th>
				<td class="value_01" colspan="3" style="height:100px;padding:10px;vertical-align:top;">
					<?= func_decode(func_get_config($bbs_config_result, 'editor_yn'), 'Y', $rows->contents, func_html($rows->contents)) ?>
					<? //첨부파일
					if (count($bbs_attach_list) > 0) {
						$movie_ext = array('mpeg', 'mpg', 'mpe', 'qt', 'mov', 'asf', 'asx', 'wma', 'wax', 'wmv', 'wvx', 'wm', 'wmx', 'avi');

						//동영상
						foreach ($bbs_attach_list as $att_rows) {
							if (strstr(func_get_config($bbs_config_result, 'attach_file_allow'), str_replace('.', '', $att_rows->file_ext))) {
								for ($i = 0; $i < count($movie_ext); $i++) {
									if ($movie_ext[$i] == str_replace('.', '', $att_rows->file_ext)) {
										?>
										<iframe src="/views/common/video.php?<?= func_base64_encode('url=/uploads/' . $mcd . '/' . $att_rows->file_name) ?>" name="movie_iframe" id="movie_iframe" width="432" height="380" frameborder="0" scrolling="no"></iframe>
										<?
									}
								}
							}
						}
					}
					?>
				</td>
			</tr>

			<? if(func_get_config($bbs_config_result, 'survey_yn') == 'Y') { // 설문조사 사용할 경우  ?>
			<tr>
				<th class="key_01">응모 다운로드</th>
				<td class="value_01" colspan="3"><a href="./?c=admin&m=adm_survey_entry_download&mcd=<?=$mcd?>&idx=<?=$idx?>">csv 파일 다운로드</a>(마이크로 소프트 엑셀에서 읽기 및 편집 가능)</td>
			</tr>
			<tr>
				<th class="key_01">결과</th>
				<td class="value_01" colspan="3">
					<? if(isset($survey_result['field'])) { ?>
						<table class="table_view_box" cellspacing="0">
							<? foreach($survey_result['field'] as $Rkey => $Rval) { ?>
							<tr>
								<th rowspan="2" class="title01" width="12%"><div align="center"><?=$Rval->no?>번</div></th>
								<td width="*" style="padding:0 0 0 10px"><strong><?=$Rval->title?></strong></td>
							</tr>
							<tr>
								<td width="*" style="padding:0 0 0 10px">
									<?
									foreach($Rval->result as $Dkey => $Dval) { // detail
										//var_dump($Dval);
										$percent = $Dval->cnt / $Rval->result_cnt * 100;
										$percent = round($percent, 2);
										if($Dval->title == '') {
											$Dval->title = '미응답';
										}
									?>
									<div style="width:<?=$percent?>%; height:20px; background:url(<?= $skin_images_url ?>/graph_bar_silver.gif); background-repeat:repeat-x; padding-top:1px; padding-bottom:1px;">
										<div style="position:absolute;"><?=$Dval->title?> <?=$percent?>% (<?=$Dval->cnt?>명)</div>
									</div>
								<? } ?>
							</tr>
							<? } ?>
						</table>
					<? } else {?>
						데이타가 없습니다.
					<? } ?>
				</td>
			</tr>
			<? } ?>

			<? if (func_get_config($bbs_config_result, 'pre_next_view_yn') == 'Y' && $rows->notice_yn == 'N') { //이전글, 다음글 사용여부 ?>
			<tr>
				<th class="key_01"><img src="/images/admin/prev_icon.gif" alt="이전" align="absmiddle" /> 이전글</th>
				<td class="value_01" colspan="3" style="color:#3873c9;">
					<?= func_decode(!isset($prev_subject) ? '' : $prev_subject, '', '이전글이 존재하지 않습니다.', $prev_subject) ?>
				</td>
			</tr>
			<tr>
				<th class="key_01"><img src="/images/admin/next_icon.gif" alt="다음" align="absmiddle" /> 다음글</th>
				<td class="value_01" colspan="3" style="color:#3873c9;">
					<?= func_decode($next_subject, '', '다음글이 존재하지 않습니다.', $next_subject) ?>
				</td>
			</tr>
			<? } ?>
		</table>
		<div class="btn_area_center">
			<? if ((func_get_config($bbs_config_result, 'reply_yn') == 'Y' && $rows->notice_yn != 'Y') || $rows->secret_yn == 'Y') { //답변기능을 사용할 경우, AND 공지글이 아닌경우, 비밀글인 경우  ?>
				<a href="#" onclick="reply_adm_bbs(); return false;" title="답변"><img src="/images/admin/btn_reply.gif" alt="답변" /></a>
			<? } ?>
			<a href="#" onclick="update_adm_bbs(); return false;" title="수정"><img src="/images/admin/btn_modify.gif" alt="수정" /></a>
			<a href="#" onclick="delete_adm_bbs(); return false;" title="삭제"><img src="/images/admin/btn_delete.gif" alt="삭제" /></a>
			<a href="/?c=admin&amp;m=adm_bbs_list&amp;mcd=<?= $mcd ?>&amp;cur_page=<?= $cur_page ?><?= func_sPUrlEncode($sParam, "sch_word") ?>" title="목록"><img src="/images/admin/btn_list.gif" alt="목록" /></a>
		</div>
	</div>
	<? if (func_get_config($bbs_config_result, 'comment_yn') == 'Y') { //댓글(코멘트) 사용하는 경우  ?>
	<script type="text/javascript">
		//게시판 코멘트 등록
		function insert_adm_comment(frm) {
			if(!validate(frm.elements['writer'], '이름을 입력하세요.')) return;
			if(!validate(frm.elements['password'], '비밀번호를 입력하세요.')) return;
			if(!validate(frm.elements['contents'], '코멘트 내용을 입력하세요.')) return;

			frm.submit();
		}
		//게시판 코멘트 삭제 체크
		function delete_adm_comment(cm_idx, pwd) {
			var frm = document.adm_frm;
			frm.cm_idx.value = cm_idx;

			document.getElementById('coment_pw').style.display = 'block';
			frm.elements['password_chk'].value = pwd;
			//frm.elements['password_chk'].focus();
		}
		//게시판 코멘트 삭제 처리
		function delete_adm_comment_exec() {
			var frm = document.adm_frm;

			if(!validate(frm.elements['password_chk'], '비밀번호를 입력하세요.')) return;

			if(confirm('코멘트를 삭제 하시겠습니까?')) {
				frm.action = '/?c=admin&m=adm_bbs_comment_delete';
				frm.submit();
			}
		}

	</script>
	<form name="adm_frm" id="adm_frm" method="post" action="/?c=admin&amp;m=adm_bbs_comment_write_exec" onsubmit="">
		<input type="hidden" name="cm_idx" value="" />
		<input type="hidden" name="idx" value="<?= $idx ?>" />
		<input type="hidden" name="mcd" value="<?= $mcd ?>" />
		<input type="hidden" name="cur_page" value="<?= $cur_page ?>" />
		<input type="hidden" name="sParam" value="<?= urlencode($sParam) ?>" />

		<!-- 댓글 -->
		<div class="comment">
			<div class="text ft_bm2">
				<div style="float:left;">
					- 댓글(코멘트) <?= count($bbs_comment_list) ?>건
				</div>
				<!-- 비밀번호 -->
				<div id="coment_pw" style="float:right;display:none;">
					<input type="password" name="password_chk" id="password_chk" value="" class="input_1" style="width:80px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="20" />
					<a href="#" onclick="delete_adm_comment_exec(); return false;" title="확인"><img src="/images/admin/sbtn_ok.gif" alt="확인" align="middle" /></a>
				</div>
			</div>
			<div class="box" style="clear:both;">
			<? if (count($bbs_comment_list) > 0) {
				foreach ($bbs_comment_list as $rows) {
			?>
				<div class="body1 ft_bm1">
					<?= $rows->writer ?> / <?= func_get_config_date(func_get_config($bbs_config_result, 'date_format'), $rows->created_dt) ?> / <?= $rows->writer_ip ?> /
					<a href="#" onclick="delete_adm_comment('<?= $rows->cm_idx ?>', '<?= $rows->password ?>'); return false;" class="link_bm1"><strong>삭제</strong></a>
				</div>
				<div class="body2 ft_bm6" style="padding:3px;"><?= func_html($rows->contents) ?></div>
				<? } ?>
			<? } ?>
				<div class="write1">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td style="padding-left:3px;">
								<input type="text" name="values[writer]" id="writer" value="<?= $member_name ?>" class="input_1" style="width:80px;" onfocus="f_text(this);" onblur="b_text(this);" maxlength="20" />
							</td>
							<td rowspan="2" align="right">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td>
											<textarea name="values[contents]" id="contents" style="width:600px;height:50px;padding:5px;"></textarea>
										</td>
										<td style="padding-left:5px;">
											<a href="#" onclick="insert_adm_comment(document.adm_frm);" title="등록"><img src="/images/admin/btn_comment.gif" alt="등록" /></a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding-left:3px;">
								<input type="password" name="values[password]" id="password" value="" class="input_1" style="width:80px;" onfocus="f_text(this);this.value = '';" onblur="b_text(this);" maxlength="20" />
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<!-- //댓글 -->
	</form>
<? } ?>
</div>