<form name="iwc_frm" id="iwc_frm" method="post" action="/?c=admin&amp;m=exec_site_meta" onsubmit="return reg_sitemeta(this);">

	<div class="contents_box_middle">

		<h1 class="title">Site meta tag set</h1> <!-- 사이트 메타테그 설정 -->

		<div class="tip_box">

			<p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

			<ul>

				<li>{Site Title} Create a site name that appears on the site title.</li> <!-- {사이트 제목} 사이트 타이틀에 나타날 사이트 명칭을 작성 합니다. -->

				<li>{Site Subject} Register your site topic that can represent the site characteristics.</li> <!-- {사이트 주제} 사이트 특성을 나타낼수 있는 사이트 주제를 등록 합니다. -->

				<li>{Site Description} Register summary article on the site explains.</li> <!-- {사이트 설명} 사이트 설명에 대한 요약 글을 등록 합니다. -->

				<li class="b">{Site Keywords} Register keywords to retrieve information that can represent the site characteristics.(Search engines use a keyword robot)</li>
				<!-- {사이트 키워드} 사이트 특성을 나타낼수 있는 주요 키워드를 등록 합니다.(검색엔진들이 로봇을 이용 검색할때 키워드 정보를 이용합니다.) -->

				<li>{Site Copyright} Create a site for copyrighted content.</li> <!-- {사이트 저작권} 사이트 컨텐츠 저작권에 대해서 작성 합니다. -->

			</ul>

		</div>

		<div class="table_write_box">

			<h1 class="table_title">Meta data set</h1>

			<table class="table_write" summary="Site meta tag set" cellspacing="0">

				<caption class="hidden"></caption>

				<col width="17%" /><col width="83%" />

				<thead>

					<tr>

						<th scope="col">META Tag Name</th>

						<th scope="col">META Tag Content</th>

					</tr>

				</thead>

				<tbody>

					<tr>

						<th scope="row">Site Title</th>

						<td>

							<input type="text" name="values[Title]" id="Title" value="<?= func_get_config($iw_config_result, 'Title') ?>" class="input" style="width:600px;" title="Site Title" onfocus="f_text(this);" onblur="b_text(this);" />

						</td>

					</tr>

					<tr>

						<th scope="row">Site Subject</th>

						<td>

							<input type="text" name="values[Subject]" id="Subject" value="<?= func_get_config($iw_config_result, 'Subject') ?>" class="input" style="width:600px;" title="Site Subject" onfocus="f_text(this);" onblur="b_text(this);" />

						</td>

					</tr>

					<tr>

						<th scope="row">Site Description</th>

						<td>

							<input type="text" name="values[Description]" id="Description" value="<?= func_get_config($iw_config_result, 'Description') ?>" class="input" style="width:600px;" title="Site Description" onfocus="f_text(this);" onblur="b_text(this);" />

						</td>

					</tr>

					<tr>

						<th scope="row">Site Keywords</th>

						<td>

							<input type="text" name="values[Keywords]" id="Keywords" value="<?= func_get_config($iw_config_result, 'Keywords') ?>" class="input" style="width:600px;" title="Site Keywords" onfocus="f_text(this);" onblur="b_text(this);" />

						</td>

					</tr>

					<tr>

						<th scope="row">Site Copyright</th>

						<td>

							<input type="text" name="values[Copyright]" id="Copyright" value="<?= func_get_config($iw_config_result, 'Copyright') ?>" class="input" style="width:600px;" title="Site Copyright" onfocus="f_text(this);" onblur="b_text(this);" />

						</td>

					</tr>

				</tbody>

			</table>

			<div class="btn_area_center">

				<input type="image" src="/images/admin/btn_ok.gif" title="Check" />

				<a href="<?= $base_url ?>"><img src="/images/admin/btn_cancel.gif" alt="Cancel" /></a>

			</div>



		</div>

	</div>

</form>