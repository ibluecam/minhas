<?php 
function updateCurrentGetUrl($p=array(), $unset_p=array()){
    $get = $_GET;
    if(count($unset_p)>0){
        foreach($unset_p as $value){
            if(isset($get[$value])) unset($get[$value]);
        }
    }
    if(count($p)>0){
        foreach($p as $key=>$value){
            $get[$key]=$value;
        }
    }
    $updatedUrl_arr = array();
    foreach($get as $key=>$value){
        $updatedUrl_arr[]=$key."=".$value;
    }
    $updatedUrl = implode("&", $updatedUrl_arr);
    return $updatedUrl;
}


?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/css/admin/admin.css" />
<link rel="stylesheet" type="text/css" href="/css/admin/button.css" />     
<style>
  table, th, td {
      border: 1px solid #ddd;
      border-collapse: collapse;
      height: 20px;
  }
 
  .no_data{
      text-align: center;
  }
  .check_box{
    text-align: center;
  }
  .pagenition{
    text-align: center;
    border:none;
  }
  .sch_country{
    padding:  4px 3px;
    width: 100px;
  }
</style>
</head>
<body>
    
    <div style="padding:10px;">
      <h2>Search for Emails</h2>
          <form action="" method="get" name="adm_form_search" id="adm_form_search">
             <input type="hidden" name="c" value="admin" />
             <input type="hidden" name="m" value="adm_add_email_bbs" />
             <select class="sch_country" name="sch_country" style="padding: 5px 10px; width: 100px;">
               <option value="">- Country -</option>
               <?php 
                foreach($country_list as $country){
                ?>
                <option <?=(isset($_GET['sch_country']) && $_GET['sch_country']=="$country->cc"? 'selected="selected"':'')?> value="<?=$country->cc?>" ><?=$country->country_name?></option>
                <?php
                } 
                ?>
             </select>
             <input style=" padding: 5px 10px; width: 200px;" type="text" name="sch_condition_customer_name_email" value="<?=(isset($_GET['sch_condition_customer_name_email']) && $_GET['sch_condition_customer_name_email']!="" ? $_GET['sch_condition_customer_name_email']:'')?>" placeholder="search by email, name or company" style="width:200px;padding: 4px 3px;">
             <input type="submit" value="Search" name="btn_search" style=" border: 1px solid #888; padding: 5px 20px; cursor:pointer">
          </form>  
            
            <p style="color: green; margin-top:10px;">
            <input type="button" value="Add" name="btn_add" id="btn_add" class="button orange"> 
          
            <span id="msg_success">Select some emails to add</span></p>
      <div id="load_wrapper"> 
         <form action="" method="get" name="adm_form_email" id="adm_form_email">
          <table class="table_lists" cellspacing="0" style="width:100%; height: 1px; margin-top:10px;border: none;">
              <caption class="hidden"></caption>

              <caption class="hidden"></caption>
              <thead>
                <tr>
                    <th scope="col"><input type="checkbox" class="id_chk_all" value=""/></th>
                    <th scope="col">Email</th>
                    <th scope="col">Name</th>
                    <th scope="col">Country</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($customer_list as $customer){ ?>
                  <tr>

                      <td class="check_box"><input class="email_check" name="email[]" type="checkbox" value="<?=$customer->customer_email?>" data-id-customer="<?=$customer->member_no?>" data-name-customer="<?=$customer->customer_name?>"></td>
                      <td><?=$customer->customer_email?></td>
                      <td><?=$customer->customer_name?></td>
                      <td><?=$customer->country_name?></td>
                      
                  </tr>
                <?php }?>
              </tbody>
              <tfoot>
                 <tr>
                  <td colspan="3" class="pagenition"></td></tr>
                <tr>
                  <td colspan="3" class="pagenition">
                    <div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?'.updateCurrentGetUrl(array(), array('cur_page')), $mcd) ?></div>
                  </td>
                </tr>
              </tfoot>
          </table>
          </form>
      </div>
      
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>
        $(document).ready(function(e) {

            /*** When button check all pressed ***/
            $(".id_chk_all").change(function(e){
                $('.email_check').prop('checked', $(this).prop('checked'));
            });

            $('#btn_add').click( function(e) {

                 
                  var customer_email = new Array();
                  var customer_name = new Array();
                  var customer_id = new Array();
                  email = $(".email_check");
                  
                  for(i=0; i<email.length; i++) {

                      if(email[i].checked) {

                          customer_email[i] = email[i].value;
                          customer_name[i] = $(email[i]).attr('data-name-customer');
                          customer_id[i] = $(email[i]).attr('data-id-customer');

                          var data = ('<tr class="email_data_'+customer_id[i]+'">'
                          +'<td><input type="checkbox" class="email_check" id="email" name="email[]" data-id-customer="'+customer_id[i]+'" value="'+customer_email[i]+'"/></td>'
                          +'<td>'+customer_name[i]+'</td>'
                          +'<td>'+customer_email[i]+'</td>'
                          +'<td></td>'
                          +'</tr>');
                          var append_value = false;
                          window.parent.$("input:checkbox:not(:checked)").each(function() {
                              if(customer_email!=''){
                                  if(this.value==customer_email[i]){
                                       append_value = false;
                                  }else{
                                       append_value = true;
                                  }
                                  return append_value;
                              }
                          });
                          if(append_value==true){
                             window.parent.append_data_email(data);
                             $('#msg_success').html('');
                             $('#msg_success').append('You have successfully added selected email(s).');
                          }
                          else{
                            $('#msg_success').html('');
                            $('#msg_success').append('Some of emails have been added already!.');
                          } 
                      }
                  }                 
            });
        });
           
            
    </script>           

</body>
</html>

                
                  
 
