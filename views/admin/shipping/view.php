<div class="contents">
	<div class="contents_box_middle">
		<div class="table_list_box">
			<b class="table_title">
			<?php echo func_get_nav($nav_array); ?>
			</b>
		</div>
		<h1 class="title">Shipping Detail:</h1>
		<div class="shipping_view_container">
				<table class="shipping_view_table">
					<tr>
						<th>Car Country</th>
						<td><?php echo $shipping_charge->from_country_name; ?></td>
					</tr><tr>
						<th>Destination Country</th>
						<td><?php echo $shipping_charge->to_country_name; ?></td>
					</tr><tr>
						<th>Destination Port</th>
						<td><?php echo $shipping_charge->port_name; ?></td>
					</tr><tr>
						<th>-</th>
						<td>-</td>

					</tr><tr>
						<th>Freight Cost By</th>
						<td><?php echo ucfirst($shipping_charge->freight_cost_by); ?></td>
					</tr><tr>
						<th>Insurance Cost</th>
						<td><?php echo $shipping_charge->insurance_cost; ?></td>
					</tr><tr>
						<th>Inspection Cost</th>
						<td><?php echo $shipping_charge->inspection_cost; ?></td>
					</tr>
					<?php if($shipping_charge->freight_cost_by == 'dimension' || $shipping_charge->freight_cost_by == 'body_type'){ ?>
					<tr>
						<th>Shipping Cost</th>
						<td><?php echo $shipping_charge->shipping_cost; ?></td>
					</tr>
					<?php } ?>
					<?php if($shipping_charge->freight_cost_by == 'model'){ ?>
						<tr>
							<th>CBM</th>
							<td><?php echo $shipping_charge->cbm_value; ?></td>
						</tr>
						<tr>
							<th>Price / CBM</th>
							<td><?php echo $shipping_charge->price_per_cbm; ?></td>
						</tr>
						<tr>
							<th>Selected Model</th>
							<td>
								<?php 
									foreach($models as $index=>$model){ 

										echo "- ".$model->model_name."<br/>";
									}
								?>
							</td>
						</tr>
					<?php }elseif($shipping_charge->freight_cost_by == 'body_type'){ ?>
						<tr>
							<th>Selected Body Types</th>
							<td>
								<?php 
									foreach($body_types as $body_type){ 
										echo "- ".$body_type->body_type_name."<br/>";
									}
								?>
							</td>
						</tr>

					<?php } ?>
					<tr>
						<th></th>
						<td>
							<a href="/?c=admin&m=adm_shipping_edit&amp;id=<?= $id ?>"/><span class="button blue">Edit</span></a>
						</td>
					</tr>
				</table>
		    </div>
		<?php
			//var_dump($shipping_charge);
		?>
	</div>
</div>