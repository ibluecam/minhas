<script type="text/javascript">
$(document).ready(function() {

    $("#btnDelete").click(delete_select);

    $('#id').click(function(event) {
        if(this.checked) {
            $(':checkbox').each(function() {
                this.checked = true;
                $('#btnDelete').removeClass('disabled');
            });
        }
        else {
            $(':checkbox').each(function() {
                this.checked = false;
                $('#btnDelete').addClass('disabled'); 
            });
        }
    });

    $('.id').click(function(event) {   
        if(this.checked) {
            this.checked = true;
            $('#btnDelete').removeClass('disabled');   
        }
        else{
            $('#btnDelete').addClass('disabled'); 
        }
    });
        
    function delete_select(){
	    var checked_count = $(".id:checked").length;
	    if(checked_count>0){
	      if(confirm("Are you sure want to delete this items?")){
		        $('#adm_frm_lisst_shipping').attr('action', "/?c=admin&m=adm_shipping_charge_delete_exec&mcd=product").submit();
		   }
	    }else{
	      alert("Please select an item to delete.");
	    }  
    }
});

</script>
<div class="contents">
	<div class="contents_box_middle">
        <div class="table_list_box">
            <b class="table_title">
            <?php echo func_get_nav($nav_array); ?>
            </b>
        </div>
		<h1 class="title">Shipping Charges List:</h1>

		<div class="search_box" >
			<div class="hearder_buuton" style="float:left;"> 
                 <a href="/?c=admin&amp;m=adm_shipping_edit" title="add new"><span class="button blue">Add New</span></a>      
                 <button title="delete" type="button" id="btnDelete" class="button red disabled"><span></span>Delete</button> 
            </div>
        </div>

        <form  name="bbs_search_frm" id="adm_frm_lisst_shipping" method="post" action="">

		<table class="table_list">
			<tr><th class="col"><input type="checkbox" name="id" id="id" class="idx_chk_all" value=""/></th><th class="col">id</th><th class="col">From Country</th><th class="col">To Country</th><th class="col">Destination Port</th><th class="col">Freight By</th></tr>
			<?php 
				foreach($shipping_charges as $charge){
					echo '<tr>';
					echo '<td><input type="checkbox" name="id[]" id="id" value="'.$charge->id.'" class="id"/></td>';
					echo '<td><a href="/?c=admin&m=adm_shipping_view&id='.$charge->id.'">'.$charge->id.'</a></td>';
					echo '<td><a href="/?c=admin&m=adm_shipping_view&id='.$charge->id.'">'.$charge->from_country_name.'</a></td>';
					echo '<td><a href="/?c=admin&m=adm_shipping_view&id='.$charge->id.'">'.$charge->to_country_name.'</a></td>';
					echo '<td><a href="/?c=admin&m=adm_shipping_view&id='.$charge->id.'">'.$charge->port_name.'</a></td>';
					echo '<td><a href="/?c=admin&m=adm_shipping_view&id='.$charge->id.'">'.$charge->freight_cost_by.'</a></td>';
					echo '</tr>';
				} 
			?>
		</table>

	</form>

	</div>
</div>