<div class="contents">
	<div class="contents_box_middle">
		<div class="table_list_box">
			<b class="table_title">
			<?php echo func_get_nav($nav_array); ?>
			</b>
		</div>
		<h1 class="title">Shipping Edit:</h1>
			<div class="shipping_edit_container">
				<form enctype="multipart/form-data" action="/?c=admin&m=adm_shipping_edit_post<?php if(!empty($id)) echo '&id='.$id;?>" method="post">
					<table class="shipping_edit_table">
						<tr>
							<th>Car Country *</th>
							<td><?php //echo $shipping_charge->country_from; ?>

								<select name="shipping[country_from]">
									<option value="">- Select Car Country -</option>
									<?php 
										foreach($country_list as $country){
											if($country->cc == $shipping_charge->country_from) $select = "selected"; else $select='';
											echo '<option '.$select.' value="'.$country->cc.'">'.$country->country_name.'</option>';
										}
									?>
								</select>
							</td>
						</tr><tr>
							<th>Destination Country *</th>
							<td><?php //echo $shipping_charge->to_country_name; ?>
								<select id="des_country_select">
									<option value="">- Destination Country -</option>
									<?php 
										foreach($country_list as $country){
											if($country->cc == $shipping_charge->port_country) $select = "selected"; else $select='';
											echo '<option '.$select.' value="'.$country->cc.'">'.$country->country_name.'</option>';
										}
									?>
								</select>
							</td>
						</tr><tr>
							<th>Destination Port *</th>
							<td><?php //echo $shipping_charge->port_name; ?>
								<select name="shipping[port_to]" id="des_port_select">
									<option value="">- Destination Port -</option>
									<?php foreach($port_list as $port){
										if($shipping_charge->port_to == $port->id) $select = "selected"; else $select='';
										echo '<option '.$select.' value="'.$port->id.'">'.$port->port_name.'</option>';
									} ?>
								</select>
								<a href="/?c=admin&m=adm_shipping_port_edit" style="color:white;" class="button orange" target="_blank">Add New Port</a>
							</td>
						</tr><tr>
							<th>-</th>
							<td>-</td>

						</tr><tr>
							<th>Freight Cost By</th>
							<td><?php //echo ucfirst($shipping_charge->freight_cost_by); ?>
								<label><input name="shipping[freight_cost_by]" class="rdo_fc_by" name="freight_cost_by" <?php if($shipping_charge->freight_cost_by=="dimension") echo "checked";  ?> type="radio" value="dimension" selected/> Dimension</label>
								<label><input name="shipping[freight_cost_by]" class="rdo_fc_by" name="freight_cost_by" <?php if($shipping_charge->freight_cost_by=="model") echo "checked";  ?> type="radio" value="model" selected/> Model</label>
								<label><input name="shipping[freight_cost_by]" class="rdo_fc_by" name="freight_cost_by" <?php if($shipping_charge->freight_cost_by=="body_type") echo "checked";  ?> type="radio" value="body_type" selected/> Body Type</label>
								<input type="hidden" id="fc_by" value="<?= $shipping_charge->freight_cost_by; ?>" />
							</td>
						</tr><tr>
							<th>Insurance Cost</th>
							<td>
								<input name="shipping[insurance_cost]" type="text" value="<?php echo $shipping_charge->insurance_cost; ?>"/> USD
							</td>
						</tr><tr>
							<th>Inspection Cost</th>
							<td>
								<input name="shipping[inspection_cost]" type="text" value="<?php echo $shipping_charge->inspection_cost; ?>"/> USD
							</td>
						</tr><tr id="tr_shipping_cost">
							<th>Shipping Cost</th>
							<td>
								<input name="shipping[shipping_cost]" type="text" value="<?php echo $shipping_charge->shipping_cost; ?>"/> USD
							</td>
						</tr><tr class="tr_for_cbm">
							<th>CBM</th>
							<td>
								<input name="shipping[cbm_value]" type="text" value="<?php echo $shipping_charge->cbm_value; ?>"/> USD
							</td>
						</tr><tr class="tr_for_cbm">
							<th>Price / CBM</th>
							<td>
								<input name="shipping[price_per_cbm]" type="text" value="<?php echo $shipping_charge->price_per_cbm; ?>"/> USD
							</td>
						</tr>
						
						<tr id="tr_model">
							<th>Select Models</th>
							<td>
								<a style="color:white;" id="btn_add_model" href="#model_popup" class="button orange">Add Model</a><input type="button" style="font-size:12px;" id="btn_remove_model" class="button red" value="Remove Model" /><br/>
								<div class="model_group_wrapper">
									<div style="padding:10px;"><label><input style="margin-bottom:0" type="checkbox" id="select_all_model_check"/> Select / Desellect All</label></div>
									<div id="model_group" class="model_group">
										
										<?php 
											$model_name_arr = array();
											foreach($models as $index=>$model){ 
												$model_name_arr[] = $model->model_name;
											}
										?>
									</div>
								</div>
								<input type="hidden" id="selected_model" name="selected_model" value="<?= implode('^', $model_name_arr); ?>"/>
							</td>
						</tr>
					
						<tr id="tr_body_type">

							<th>Select Body Types</th>
							<td>
								<a style="color:white;" id="btn_add_body_type" href="#body_type_popup" class="button orange">Add Body Type</a><input type="button" style="font-size:12px;" id="btn_remove_body_type" class="button red" value="Remove Body Type" /><br/>
								<div class="model_group_wrapper">
									<div style="padding:10px;"><label><input style="margin-bottom:0" type="checkbox" id="select_all_body_type_check"/> Select / Desellect All</label></div>
									<div id="body_type_group" class="body_type_group">
										
										<?php 
											$body_type_name_arr = array();
											foreach($body_types as $index=>$body_type){ 
												$body_type_name_arr[] = $body_type->body_type_name;
											}
										?>
									</div>
								</div>
								<input type="hidden" id="selected_body_type" name="selected_body_type" value="<?= implode('^', $body_type_name_arr); ?>"/>
							</td>
						</tr><tr>
							<th></th>
							<td style="text-align:right;">
								<input type="submit" title="save" value="Save" class="button blue" name="submit"/>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<!-- POPUP FOR MODEL SELECT -->
			<div style="overflow:auto; display:none;">
				<div id="model_popup" style="overflow:auto; padding: 20px; min-height:200px; max-height:100%;">
					<div class="popup_search" style="background: #eee; padding:10px;">
						<select id="make_select" style="padding:5px;">
							<option value="">- Select Make -</option>
							<?php foreach($make_list as $make){ 
								echo '<option value="'.$make->id.'">'.$make->make_name.'</option>';
							}
							?>
						</select>
						<input type="button" id="btn_search_make" value="Search" class="button orange" />
					</div>
					<div class="popup_result" id="model_list_popup">
						<table id="model_list_table">
							<tr><td style="height: 100px; padding: 5px; text-align: center;">Select Make and Search to show models</td></tr>
						</table>
					</div>
				</div>
			</div>
			<!-- END POPUP FOR MODEL SELECT -->
			<!-- POPUP FOR BODY TYPE SELECT -->
			<div style="overflow:auto; display:none;">
				<div id="body_type_popup" style="overflow:auto; padding: 20px; min-height:200px; max-height:100%;">
					
					<div class="popup_result" id="model_list_popup">
						<table id="body_type_list_table">
							<tr><td style="width:20px;"><input id="body_type_all_check" type="checkbox"/></td><td><input type="button" id="btn_add_select_body_type" class="button green" value="Add"/></td></tr>
							<?php foreach($body_type_list as $body_type){ 
								echo '<tr><td><label><input type="checkbox" value="'.$body_type->body_name.'" class="popup_body_type_check"></td><td>'.$body_type->body_title.'</label></td></tr>';
							} ?>
						</table>
					</div>
				</div>
			</div>	
			<!-- END POPUP FOR BODY TYPE SELECT -->
		<?php
			//var_dump($shipping_charge);
		?>
	</div>
</div>
<script>

	var selected_model=[]; /*** STORED MODEL IN MEMORY ***/
	var selected_body_type = [];
	$(document).ready(function(e){
		var fc_by = $("#fc_by").val();
		toggle_row_display(fc_by);
		/*** CHECK IF EXIST SELECTED MODELS ***/
		var selected_model_val = $('#selected_model').val();
		if(selected_model_val!=''){
			selected_model = selected_model_val.split('^');
			reload_selected_model();
		}else{
			$("#model_group").html('No selected models yet.');
		}
		/*** CHECK IF EXIST SELECTED BODY TYPES ***/
		var selected_body_type_val = $('#selected_body_type').val();
		if(selected_body_type_val!=''){
			selected_body_type = selected_body_type_val.split('^');
			reload_selected_body_type();
		}else{
			$("#body_type_group").html('No selected body type yet.');
		}
		$("#des_country_select").change(function(e){
			var country = $(this).val();
			$.ajax({
				type: "GET",
				url: "/?c=admin_json&m=port_list&country="+country,
				dataType: "json"
			})
			.done(function( json ) {
			    load_port_list(json);
			});
		});
		$(".rdo_fc_by").change(function(e){
			var fc_by = $(this).val();
			toggle_row_display(fc_by);
		});

		$("#btn_add_model").colorbox({inline:true, width:"600", height:"90%", scrolling:false, reposition: true, fixed:true});
		$("#btn_add_body_type").colorbox({inline:true, width:"600", scrolling:false, reposition: true, fixed:true});

	});
	/*** WHEN USER FILTER FOR MODELS BY MAKE IN MODEL POPUP ***/
	$(document).on('click', '#btn_search_make', function(){
		var make_id = $("#make_select").val();
		$.ajax({
			type: "GET",
			url: "/?c=user_json&m=json_model_list&make_id="+make_id,
			dataType: "json"
		})
		.done(function( json ) {
		    load_model_list(json);
		});
			
	});
	/*** CHECKBOX ALL AT THE TOP OF MODEL POPUP ***/
	$(document).on('change', '#model_all_check', function(){
		var check = $(this).prop('checked');
		$(".popup_model_check").prop('checked', check);
			
	});
	/*** CHECKBOX ALL AT THE TOP OF BODY TYPE POPUP ***/
	$(document).on('change', '#body_type_all_check', function(){
		var check = $(this).prop('checked');
		$(".popup_body_type_check").prop('checked', check);
			
	});
	/*** CHECKBOX ALL AT SELECTED MODEL BLOCK ***/
	$(document).on('change', '#select_all_model_check', function(){
		var check = $(this).prop('checked');
		$(".selected_model_check").prop('checked', check);
			
	});
	/*** CHECKBOX ALL AT SELECTED MODEL BLOCK ***/
	$(document).on('change', '#select_all_body_type_check', function(){
		var check = $(this).prop('checked');
		$(".selected_body_type_check").prop('checked', check);
			
	});
	/*** WHEN USER CLICK ADD MODEL BUTTON FROM POPUP ***/
	$(document).on('click', '#btn_add_select_model', function(){
		$(".popup_model_check:checked").each(function(e){
			var model_c = $(this).val();
			if(inArray(model_c, selected_model)==false){
				selected_model.push(model_c);
			}
		});
		//console.log(selected_model);
		reload_selected_model();
	});
	/*** WHEN USER CLICK ADD BODY TYPE BUTTON FROM POPUP ***/
	$(document).on('click', '#btn_add_select_body_type', function(){
		$(".popup_body_type_check:checked").each(function(e){
			var body_type_c = $(this).val();
			if(inArray(body_type_c, selected_body_type)==false){
				selected_body_type.push(body_type_c);
			}
		});
		//console.log(selected_model);
		reload_selected_body_type();
	});
	/*** REMOVE SELECTED MODEL ***/
	$(document).on('click', "#btn_remove_model", function(){
		$(".selected_model_check:checked").each(function(e){
			var model_c = $(this).val();
			remove_model(model_c);
		});
		reload_selected_model();
	});
	/*** REMOVE SELECTED BODY TYPE ***/
	$(document).on('click', "#btn_remove_body_type", function(){
		$(".selected_body_type_check:checked").each(function(e){
			var body_type_c = $(this).val();
			remove_body_type(body_type_c);
		});
		reload_selected_body_type();
	});
	function reload_selected_model(){
		var html = '';
		var post_models = '';
		post_models = selected_model.join('^');
		if(selected_model.length>0){
			$.each( selected_model, function( key, value ) {
				html += '<label><input type="checkbox" class="selected_model_check" value="'+value+'"/> '+value+'</label><br/>'
			});
		}else{
			html = 'No selected models yet.';
		}
		$("#selected_model").val(post_models);
		$("#model_group").html(html);	

	}

	function reload_selected_body_type(){
		var html = '';
		var post_body_types = '';
		post_body_types = selected_body_type.join('^');
		if(selected_body_type.length>0){
			$.each( selected_body_type, function( key, value ) {
				html += '<label><input type="checkbox" class="selected_body_type_check" value="'+value+'"/> '+value+'</label><br/>'
			});
		}else{
			html = 'No selected Body Types yet.';
		}
		$("#selected_body_type").val(post_body_types);
		$("#body_type_group").html(html);	

	}

	/*** FUNCTION LOAD MODEL BY SEARCH FROM POPUP***/
	function load_model_list(json_model){
		var html = '';
		if(json_model.length>0){
			html +='<tr><td style="width:20px;"><input id="model_all_check" type="checkbox"/></td><td><input type="button" id="btn_add_select_model" class="button green" value="Add"/></td></tr>';
			$.each(json_model, function( key, value ) {
				html += '<tr><td><label><input type="checkbox" value="'+value.model_name+'" class="popup_model_check"></td><td>'+value.model_name+'</label></td></tr>';
			});
		}else{
			html = '<tr><td style="height: 100px; padding: 5px; text-align: center;">No model found for your search query.</td></tr>';
		}
		$("#model_list_table").html(html);
		//$.colorbox.resize();
	}
	/*** FUNCTION LOAD PORT LIST WHEN DESTINATION COUNTRY IS SELECTED ***/
	function load_port_list(json_ports){
		$("#des_port_select").html('<option value="">- Destination Port -</option>');
		$.each( json_ports, function( key, value ) {
			var option_html = '<option value="'+value.id+'">'+value.port_name+'</option>';
			$("#des_port_select").append(option_html);ss
		});
	}
	/*** EACH FREIGHT TYPE HAS DIFFERENT INPUT NUMBER ***/
	function toggle_row_display(fc_by){
		if(fc_by=='dimension'){
			$("#tr_model").css("display", "none");
			$("#tr_body_type").css("display", "none");
			$("#tr_shipping_cost").css("display", "table-row");
			$(".tr_for_cbm").css("display", "none");
		}else if(fc_by=='model'){
			$("#tr_model").css("display", "table-row");
			$("#tr_body_type").css("display", "none");
			$("#tr_shipping_cost").css("display", "none");
			$(".tr_for_cbm").css("display", "table-row");
		}else if(fc_by=='body_type'){
			$("#tr_model").css("display", "none");
			$("#tr_body_type").css("display", "table-row");
			$("#tr_shipping_cost").css("display", "table-row");
			$(".tr_for_cbm").css("display", "none");
		}
	}
	/*** CUSTOM FUNCTION FOR in_array equivalent of php ***/
	function inArray(needle, haystack) {
	    var length = haystack.length;
	    for(var i = 0; i < length; i++) {
	        if(haystack[i] == needle) return true;
	    }
	    return false;
	}
	/*** FUNCTION FOR REMOVE ARRAY BY VALUE ***/
	function remove_model(model_name) {
	    var index = selected_model.indexOf(model_name);
		selected_model.splice(index, 1);
	}
	/*** FUNCTION FOR REMOVE ARRAY BY VALUE ***/
	function remove_body_type(body_type) {
	    var index = selected_body_type.indexOf(body_type);
		selected_body_type.splice(index, 1);
	}
</script>