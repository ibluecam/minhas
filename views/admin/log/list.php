<script type="text/javascript">
$(document).ready(function() {

    $(".datepicker-add-options").datepicker({});

    $("#btnDelete").click(delete_select);

    $('#id').click(function(event) {
        if(this.checked) {
            $(':checkbox').each(function() {
                this.checked = true;
                $('#btnDelete').removeClass('disabled');
            });
        }
        else {
            $(':checkbox').each(function() {
                this.checked = false;
                $('#btnDelete').addClass('disabled'); 
            });
        }
    });

    $('.id').click(function(event) {   
        if(this.checked) {
            this.checked = true;
            $('#btnDelete').removeClass('disabled');   
        }
        else{
            $('#btnDelete').addClass('disabled'); 
        }
    });

    function delete_select(){
        var checked_count = $(".id:checked").length;
        if(checked_count>0){
          if(confirm("Are you sure want to delete this items?")){
                $('#adm_frm_list_log').attr('action', "/?c=admin&m=adm_delete_log_exec").submit();
           }
        }else{
          alert("Please select an item to delete.");
        }  
    }

    $(".show_less").css("display","none");
    $(".readless").css("display","none");
    $(".readless").css("color","blue");
    $(".readmore").css("color","blue");

    $('.readmore').click(function(event) {   
        var id = $(this).data('id');  
        $('#readmore_'+id).css("display","none");
        $("#show_less_"+id).css("display",'');
        $('#readless_'+id).css("display",'');
        return false;
    });

    $('.readless').click(function(event) {   
        var id = $(this).data('id');   
        $('#readmore_'+id).css("display",'');
        $("#show_less_"+id).css("display","none");
        $('#readless_'+id).css("display","none");
        return false;
    });

});

</script>
<?php
if(isset($_POST['sch_create_dt_s'])) $sch_create_dt_s = htmlspecialchars($_POST['sch_create_dt_s']);else $sch_create_dt_s="";
if(isset($_POST['sch_create_dt_e'])) $sch_create_dt_e = htmlspecialchars($_POST['sch_create_dt_e']);else $sch_create_dt_e="";
if(isset($_POST['sch_condition'])) $sch_condition = htmlspecialchars($_POST['sch_condition']);else $sch_condition="";
if(isset($_POST['sch_author'])) $sch_author = htmlspecialchars($_POST['sch_author']);else $sch_author="";
if(isset($_POST['sch_log_message'])) $sch_log_message = htmlspecialchars($_POST['sch_log_message']);else $sch_log_message="";
?>
<div class="contents">
	<div class="contents_box_middle">
        <div class="table_list_box">
            <b class="table_title">
            <?php echo func_get_nav($nav_array); ?>
            </b>
        </div>
		<h1 class="title">Logs List:</h1>

        <form name="adm_frm_search_log" id="adm_frm_search_log" method="post" action="/?c=admin&amp;m=adm_list_log">

		<div class="search_box" >
			<div class="hearder_buuton" style="float:left;"> 
                 <button title="delete" type="button" id="btnDelete" class="button red disabled"><span></span>Delete</button> 
            </div>

            <div class="search_condition">

            - Date
                    <input type="text" name="sch_create_dt_s" value="<?=$sch_create_dt_s?>" data-date-format="yyyy-mm-dd" class="input_1 datepicker-add-options text_admin" data-auto-close="true" style="width:65px;" />

                    &nbsp;~

                    <input type="text" data-date-format="yyyy-mm-dd" value="<?=$sch_create_dt_e?>" name="sch_create_dt_e" class="input_1 datepicker-add-options text_admin" data-auto-close="true" style="width:65px;" />


                <?

                if(isset($bbs_config_result)){

                    if (func_get_config($bbs_config_result, 'category_yn') == 'Y') {

                    $category_code = explode('|', func_get_config($bbs_config_result, 'category_code'));

                    $category_name = explode('|', func_get_config($bbs_config_result, 'category_name'));

                }

                    ?>

                <? } ?>

                <select name="sch_condition" id="sch_condition" class="input_2" style="width:150px;" align="absmiddle">

                    <option value="">Log type</option>

                    <?php foreach (Array('car','customer','car_image','marketing_email','proforma_invoice','car_status') as $Val) { ?>
                        <option <?php echo ($sch_condition==$Val)? 'selected="selected"' : '' ?> value="<?php echo $Val; ?>"><?php echo $Val; ?></option>
                    <?php } ?>
                                
                </select>

                <input type="text" name="sch_author" id="sch_author" value="<?=$sch_author?>" class="input_1 text_admin" style="width:100px;" title="search by author" placeholder="author" />

                <input type="text" name="sch_log_message" id="sch_log_message" value="<?=$sch_log_message?>" class="input_1 text_admin" style="width:100px;" title="search by log message" placeholder="message" />

                <input type="image" src="/images/admin/btn_search_01.gif" class="search_admin" title="search" align="absmiddle" />

            </div>
        </div>

        </form>

        <form name="adm_frm_list_log" id="adm_frm_list_log" method="post" action="">
		<table class="table_list">
			<tr>
                <th class="col"><input type="checkbox" name="id" id="id" value=""/></th>
                <th class="col">Author</th>
                <th class="col" width="500">Message</th>
                <th class="col">Log Type</th>
                <th class="col">Created Date</th>
            </tr>
			<?php

            // function limit_words($string, $word_limit){
            //     $words = explode(",",$string);
            //     return implode(" ",array_splice($words,0,$word_limit));
            // }

            // function count_word($string){
            //     $words = explode(",", $string);
            //     return count($words);
            // } 

            if (count($log_list) > 0) {

				foreach($log_list as $logs){
					echo '<tr>';
                    echo '<td><input type="checkbox" name="id[]" class="id" value="'.$logs->id.'"/></td>';
					echo '<td>'.$logs->member_id.'</td>';

                    $lenght=50; // Define how many character you want to display.

                    $message=$logs->message;
                    $start = substr($message, 0, $lenght);

                    //$start = limit_words($logs->message,1);

                    $end = str_replace($start, '', $message);

                    $count = count($message);

                    echo '<td>'.$start;

                    if($count < $lenght){

                    echo '<a href="#" data-id="'.$logs->id.'" id="readmore_'.$logs->id.'" class="readmore">...Show more</a>
                          <span class="show_less" id="show_less_'.$logs->id.'" >'.$end.'</span>
                          <a href="#" data-id="'.$logs->id.'" id="readless_'.$logs->id.'" class="readless">...Show less</a></a>';
                    }

                    echo '</td>';
                    echo '<td>'.$logs->log_type.'</td>';
                    echo '<td>'.$logs->created_dt.'</td>';
					echo '</tr>';
				}
            }
            else{
			?>
            
            <tr>
                <td colspan="5">Data does not exist.</td>
            </tr>
            <?php 
            }
            ?>
		</table>

        </form>

    <div class="table_list_box" style="padding-top:10px;">
        <div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?c=admin&m=adm_list_log') ?></div>
    </div>

	</div>
</div>



