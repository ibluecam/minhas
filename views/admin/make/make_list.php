
<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />
<?php 
function updateCurrentGetUrl($p=array(), $unset_p=array()){
    $get = $_GET;
    if(count($unset_p)>0){
        foreach($unset_p as $value){
            if(isset($get[$value])) unset($get[$value]);
        }
    }
    if(count($p)>0){
        foreach($p as $key=>$value){
            $get[$key]=$value;
        }
    }
    $updatedUrl_arr = array();
    foreach($get as $key=>$value){
        $updatedUrl_arr[]=$key."=".$value;
    }
    $updatedUrl = implode("&", $updatedUrl_arr);
    return $updatedUrl;
}


?>
<div class="contents_box_middle">

        <h1 class="title">Makes</h1>
        
        

        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You can manage makes and upload makes logo here.</li>

            </ul>

        </div>
        <div class="table_list_box" style="padding-top:10px;" id="updates">

            <h1 class="table_title">Make List</h1>

            <div class="search_box" style="background:none; border:none">

                <div style="color: #666666;float: left;line-height: 18px;padding: 2px 0 2px 10px;">

                    <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Total : Page:<span id="current_num_rows" class="title"><?= $cur_page ?></span> /<span id="total_num_rows" class="title"><?= $total_page ?></span>

                </div>
                
            </div>
            
            <div class="btn_area_right" style="margin-bottom:10px;">
                    <a  class="fancybox_add iframe" href="/?c=admin&amp;m=add_make"><span class="button blue">Add New</span></a>
                    <a href="#" onclick="delete_all();" title="delete"><span class="button red">Delete Make</span></a>
                    
                </div>
           
             <div style="overflow:auto; float:left; width:100%;" >
                <form name="adm_frm_2" id="adm_frm_2" method="post" enctype="multipart/form-data" action="">
                   
                <input type="hidden" name="address"> <!-- sending url address -->

                <table class="table_list" cellspacing="0" style="width:100%; min-width: 800px;">

                    <caption class="hidden"></caption>

                    <thead>

                        <tr>

                            <th scope="col"><input type="checkbox" name="idx" id="idx" class="id_chk_all" 
                                                onclick="checkbox_all_check(this, document.adm_frm_2.idx);"
                                                   value=""/>
                            </th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col">Logo</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            
                            <th scope="col">Make</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col">Total Model</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col"></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col"></th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                        </tr>

                    </thead>

                    <tbody>
                        <?php
                          if(count($adm_make_select)){
                              foreach ($adm_make_select as $field ){
                           
                        ?>
                        
                        <tr>
                   
                            <td><input type="checkbox" name="idx[]" class="idx" id="idx" value="<?php echo $field->id_make ?>">
                         <td class="line">&nbsp;</td>
                                 <td>
                                     
                                         <img src="<?php echo $field->icon_name; ?>">
                                     
                                 </td>
                         <td class="line">&nbsp;</td>
                         <td>
                               <?php
                                  echo $field->make_name;
                               ?>
                        </td>
                         <td class="line">&nbsp;</td>
                         <td><a href="/?c=admin&m=model_list&select_make=<?php echo $field->make_name; ?>"><?php echo $field->count_model; ?></a></td>
                         <td class="line">&nbsp;</td>
                         <td>
                            <a id="fancybox_change" class="fancybox_change iframe" href="/?c=admin&amp;m=adm_change_make&id=<?= $field->id_make; ?>">
                            <!-- <span class="button blue">Change Label</span> -->
                            <img src="/images/admin/modify.gif" alt="modify">
                            </a>
                         </td>

                        </tr>
                       
                        <?php
                               
                              }
                          }
                        ?>
                    </tbody>

                </table>

            </div>
            
             <div class="page_navi"><?= func_wirte_page_info($page_info, $sParam, '/?'.updateCurrentGetUrl(array(), array('cur_page')), $mcd) ?></div>
            
            <div class="btn_area_center">
                
            </div>

            
          </form>
        </div>

    </div>


<script type="text/javascript">
   
  
   
    $(document).ready(function(e){
        
        $("a.fancybox_add").fancybox({
            height  : 250,
            width   : 450,
            onClosed: function() { 
                window.location = window.location;
            }
        }); 
        
        
        
        $("#close").click(function(e){
            $.fancybox.close();
            //extractImageFromPdf();
        });
        $(document).on('click','#btn_load_more',function(){
            var count_row = get_current_row_fetch();
            $("#loading_more").css("display", "inline");
            load_more(count_row);
            
            
        });
    

        $( "tbody" ).sortable();
        $( "tbody" ).disableSelection();

        
    }); 
  
   

    function checkbox_all_check(obj, target) {
    if ( typeof obj == 'undefined' || typeof target == 'undefined') {

    }
    else if ( typeof target.length == 'undefined' ) {
      target.checked = obj.checked;
    }
    else
    {
      for (i = 0 ; i < target.length ; i++) {
        target[i].checked = obj.checked;
      }
    }
  }

  
        
        
        
        function delete_all() {
    var f  = document.adm_frm_2;
    var fg = false;

    if(f.idx != null) {
      for(i=0; i<f.idx.length; i++) {
        if(f.idx[i].checked) {
          fg = true;
        }
      }

      if(!fg) {
        alert('Please select make car that you want to delete.');
        return;
      }

      if (!confirm('Are you sure want to remove this car from make list?'))

          {
              return;
              
          }else{

            f.action = '?c=admin&m=adm_make_delete';
                       
                        
        f.submit();
          }
      
    }
  }   
  
   $("a.fancybox_change").fancybox({
            height  : 300,
            width   : 450,
            onClosed: function() {
                window.location = window.location;
            }
        });
            
        $(".id_chk_all").change(function(e){
        $(".idx").prop('checked', this.checked);
    });
   
</script>



       

