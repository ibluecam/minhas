
<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />
 
<?php 
function updateCurrentGetUrl($p=array(), $unset_p=array()){
    $get = $_GET;
    if(count($unset_p)>0){
        foreach($unset_p as $value){
            if(isset($get[$value])) unset($get[$value]);
        }
    }
    if(count($p)>0){
        foreach($p as $key=>$value){
            $get[$key]=$value;
        }
    }
    $updatedUrl_arr = array();
    foreach($get as $key=>$value){
        $updatedUrl_arr[]=$key."=".$value;
    }
    $updatedUrl = implode("&", $updatedUrl_arr);
    return $updatedUrl;
}


?>







<div class="contents_box_middle">

        <h1 class="title">Premium Cars</h1>
        
				

        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You can add or change premium cars showing on homepage here. These cars will show in popular section.</li>

            </ul>

        </div>

                <!--<div style="padding-top:5px;text-align:right;"><img src="/images/admin/btn_reset.gif" alt="초기화" /></div>-->

        <div class="table_list_box" style="padding-top:10px;" id="updates">

            <h1 class="table_title">Car List</h1>

            <div class="search_box">

                
                
                <div class="search_condition" style="overflow:auto;">
                
                <div class="search_result" style="float:left; width:55%; text-align:left">

                    <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Total : <span id="current_num_rows" class="title"><?=$current_num_rows?></span> /<span id="total_num_rows" class="title"> <?=$total_num_rows?></span> cars

                </div>
                 <div class="btn_area_right" style="float:right; width:40%; margin-right:10px">
                    <a  class="fancybox_add iframe" href="/?c=admin&amp;m=adm_add_premium_bbs"><span class="button blue">Add New</span></a>
                   <!--  <a href="#" onclick="delete_all();" title="delete"><span class="button red">Remove From Premium</span></a> -->
                    <button type="button" id="btnDelete" class="button red" disabled=""><span></span> <b id='del'>Remove From Premium</b></button>
                    
                </div>
                   
                  <form name="bbs_search_frm" id="adm_frm" method="get" action="" >
                    
                   </form>
                   <br>

                    
                </div>


            </div>
            <div class="tab_container" style="box-sizing: border-box; float: left; margin-top: 10px; width: 100%;">
                <input type="hidden" id="country" value="<?=$country?>"/>
                <a href="/?c=admin&m=adm_premium_bbs" id="tab_customer" class="tab <?php if(empty($country)) echo "selected_tab"; ?>" data-content-id="custom_table_container">All Cars</a>
                <a href="/?c=admin&m=adm_premium_bbs&country=jp" class="tab <?php if($country=='jp') echo "selected_tab"; ?>" id="tab_country" data-content-id="country_table_container">Japan</a>
                <a href="/?c=admin&m=adm_premium_bbs&country=kr" class="tab <?php if($country=='kr') echo "selected_tab"; ?>" id="tab_country" data-content-id="country_table_container">Korea</a>
                
                 
            </div>
             <div style="overflow:auto; float:left; width:100%;" >
                <form name="adm_frm_2" id="adm_frm_2" method="post" enctype="multipart/form-data" action="">
                   
                <input type="hidden" name="address"> <!-- sending url address -->

                <table class="table_list" cellspacing="0" style="width:100%; min-width: 800px;">

                    <caption class="hidden"></caption>

                    <thead>

                        <tr>

                            <th scope="col"><input type="checkbox" id="idx" class="id_chk_all" 
                                                onclick="checkbox_all_check(this, document.adm_frm_2.idx);"
                                                   value=""/>
                            </th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col">ID</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            
                            <th scope="col">Image</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col">Car</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col">Car Status</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col">Chassis No.</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                            <th scope="col">Owner</th>
                            <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                        </tr>

                    </thead>

                    <tbody>
                          <?php
                          
                             if(count($get_adm_premium_bbs)>0){
                           
                             $link = ''; 
                             $links='';
                             foreach ($get_adm_premium_bbs as $row){
                                      
                             
                       
                         $link = "?c=admin&m=adm_customer_view&member_no=$row->mem_no";
                         $links = "/?c=admin&m=adm_bbs_detail&mcd=product&idx=$row->idx";
                          ?>
                        <tr>
                   
                            <td><input type="checkbox" name="idx[]" class="chk" data-title="<?php echo $row->idx; ?>" value="<?php echo $row->idx; ?>">
                         <td class="line">&nbsp;</td>
                         <td><a href="<?php echo $links; ?>" target="_blank"><?php echo $row->idx; ?></a></td>
                         <td class="line">&nbsp;</td>
                         <td><a href="<?php echo $links; ?>" target="_blank"><img width="80" src="<?php echo $row->base_url.'thumb/'.$row->fileimgname; ?>"</a></td>
                         <td class="line">&nbsp;</td>
                         <td><a href="<?php echo $links; ?>" target="_blank">
                                 <?php
                                    if($row->car_model_year=='' || $row->car_model_year==0){
                                        echo $row->car_make.",".$row->car_model; 
                                    }
                                    else{
                                        
                                   
                                 ?>
                             <?php echo $row->car_model_year.",".$row->car_make.",".$row->car_model; ?><?php   } ?></a></td>
                             <td class="line">&nbsp;</td>
                         <td><a href="<?php echo $links; ?>" target="_blank"><?php echo ucfirst($row->icon_status); ?></a></td>
                         <td class="line">&nbsp;</td>
                         <td><a href="<?php echo $links; ?>" target="_blank"><?php echo $row->car_chassis_no; ?></a></td>
                         <td class="line">&nbsp;</td>
                         <td><a href="<?php echo $link; ?>" target="_blank"><?php echo $row->com_name; ?></a></td>
                         <td class="line">&nbsp;</td>
                        </tr>
                       <?php 
                           }
                         }
                         else{
                        
                       ?>
                       <tr>
                         <td colspan="14" class="no_data">Data does not exist.</td>
                       </tr>
                       <?php
                         }
                       ?>
                       
                    </tbody>

                </table>

            </div>
            
            <div class="page_navi">
                <div id="more" class="loading" style="text-align: center;">

                    <img  style="display:none" id="loading_more_bbs" src="images/ico-loading2.gif"/>
                </div>
                <div id="more" class="morebox" style="text-align: center;">

                    <input type="button" id="btn_load_more" value="Load More"/>
                </div>
            </div>
            
            <div class="btn_area_center">
                <!-- <div class="btn_area_left">
                    <a class="fancybox" href="#excel_export_popup"><span class="button green">Excel Export</span></a>
                   
                </div> -->
               
            </div>

            
          </form>
        </div>

    </div>

   
            
   
   



<script type="text/javascript">
                                
</script> 

<script type="text/javascript">
   
  
   
    $(document).ready(function(e){
        $("#btnDelete").click(btnDelete_click);

        $(document).on('change', '#idx', function(event) {   
            $('.chk').prop('checked', $(this).prop('checked'));
        });

        $(document).on('change', '.chk, #idx', function(event) {   
            var count_check = $('.chk:checked').length;
            if(count_check>0){
                $('#btnDelete').removeAttr('disabled'); 
            }else{
                $('#btnDelete').attr('disabled', "disabled");
               
            }
        });
        
        $("#btn_yes").click(function (){
            var abc ='';
            $(".chk").each(function (){
                if ($(this).is(":checked") ===true)
                  

                    abc += $(this).attr("data-title") + ";";
            });
            var ajax = $.ajax({
                url: "?c=admin&m=adm_premium_delete",
                type: 'POST',
                data : {
                    
                    idx : abc    
                }
            });
            ajax.done(function (msg){
                $("#myModal").modal("hide");
                window.location.href = "/?c=admin&m=adm_premium_bbs";
            });
        });


        function btnDelete_click(){
            delete_all();
        }
        // end delete
        
        $("a.fancybox_add").fancybox({
            height  : 500,
            width   : 519,
            onClosed: function() { 
                window.location = window.location;
            }
        }); 
        $("#close").click(function(e){
            $.fancybox.close();
            //extractImageFromPdf();
        });
        
        $("#btn_load_more").click(function(e){
            var count_row = get_current_row_fetch();
            $("#loading_more_bbs").css("display", "inline");
            load_more_bbs(count_row);
        });
    

        $( "tbody" ).sortable();
        $( "tbody" ).disableSelection();

        
    }); 
    $(document).ajaxStart(function(){
        $("#btn_load_more").attr("disabled", "disabled");
    });
    $(document).ajaxComplete(function(){
        $("#btn_load_more").removeAttr("disabled");
    });
    function get_current_row_fetch(){
        var count_row = 0;
        if($(".table_list tbody tr").length){
            count_row = $(".table_list tbody tr").length;
        }
        return count_row;
    }
    function load_more_bbs(offset){
        offset = offset || 0;
        var country = $("#country").val();
        var data = {
            offset: offset,
            country: country
        };
        $.ajax({
            method: "GET",
            url: "/?c=admin&m=adm_json_premium_bbs",
            data: data,
            dataType: 'json'
        })
        .done(function( result ) {
            
            $.each(result['premium_bbs'], function( index, value ) {
               
                append_bbs_table_html(value.idx, value.base_url+'thumb/'+value.fileimgname, value.car_name, value.icon_status, value.car_chassis_no, value.mem_no, value.com_name);
            }); 
            var current_fetch_row = get_current_row_fetch();   
            $("#loading_more_bbs").css("display", "none");
            $("#current_num_rows").html(current_fetch_row);    
            $("#total_num_rows").html(result['total_num_rows']);   
        });
    }

    function append_bbs_table_html(idx, car_image, car_name, icon_status, car_chassis_no, member_no, company_name){
        var tr = '';
        tr += '\
                  <tr class="ui-sortable-handle">\
                      <td>\
                          <input type="checkbox" class="chk" data-title="'+idx+'" value="'+idx+'" name="idx[]"/>\
                      </td><td class="line">&nbsp;</td>\
                      <td>\
                          <a target="_blank" href="/?c=admin&amp;m=adm_bbs_detail&amp;mcd=product&amp;idx='+idx+'">'+idx+'</a>\
                      </td><td class="line">&nbsp;</td>\
                      <td>\
                          <a target="_blank" href="/?c=admin&amp;m=adm_bbs_detail&amp;mcd=product&amp;idx='+idx+'">'+'<img width="80" src="'+car_image+'"/>'+'</a>\
                          </td><td class="line">&nbsp;</td>\
                      <td>\
                          <a target="_blank" href="/?c=admin&amp;m=adm_bbs_detail&amp;mcd=product&amp;idx='+idx+'">'+car_name+'</a>\
                      </td><td class="line">&nbsp;</td>\
                      <td>\
                          <a target="_blank" href="/?c=admin&amp;m=adm_bbs_detail&amp;mcd=product&amp;idx='+idx+'">'+icon_status+'</a>\
                      </td><td class="line">&nbsp;</td>\
                      <td>\
                          <a target="_blank" href="/?c=admin&amp;m=adm_bbs_detail&amp;mcd=product&amp;idx='+idx+'">'+car_chassis_no+'</a>\
                      </td><td class="line">&nbsp;</td>\
                      <td>\
                          <a target="_blank" href="?c=admin&amp;m=adm_customer_view&amp;member_no='+member_no+'">'+company_name+'</a>\
                      </td>\
                  </tr>';
        $(".table_list tbody").append(tr);
    }

    function checkbox_all_check(obj, target) {
    if ( typeof obj == 'undefined' || typeof target == 'undefined') {

    }
    else if ( typeof target.length == 'undefined' ) {
      target.checked = obj.checked;
    }
    else
    {
      for (i = 0 ; i < target.length ; i++) {
        target[i].checked = obj.checked;
      }
    }
  }

  
        
        
        
  function delete_all() {
      var f  = document.adm_frm_2;
      if (confirm('Are you sure want to remove this car from premium list?')){
          f.action = '?c=admin&m=adm_premium_delete';            
          f.submit();
      }
  }       
</script>



       

