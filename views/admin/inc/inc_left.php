<div class="snb">

	<!-- 접속현황 -->
	<div class="snb_status">

<!-- 		<div class="status_box_top">

			<div class="bg_top_left"><img src="/images/admin/left_box_bg_01_01.gif" alt="left_box_bg_01_01" /></div>

			<div class="bg_top_center"></div>

			<div class="bg_top_right"><img src="/images/admin/left_box_bg_01_02.gif" alt="left_box_bg_01_02" /></div>

		</div> -->

		<!-- <div class="status_box_middle">

			<div class="text">

				<p>UserName : <span class="font_bold_default"><?= $member_name ?></span></p>

				<p>Welcome to BlaudaERP</p>

			</div>

			<div class="line"></div>

			<div class="btn">

				<a href="/" target="_blank"><img src="/images/admin/btn_home.gif" alt="홈페이지 바로가기" /></a>

				<a href="#" onclick="location.replace('?c=admin&amp;m=logout_exec'); return false;"><img src="/images/admin/btn_logout.gif" alt="로그아웃" /></a>

			</div>

		</div> -->

		<div class="status_box_bottom">

			<div class="bg_bottom_left"></div>

			<div class="bg_bottom_center"></div>

			<div class="bg_bottom_right"></div>

		</div>

	</div>

	<!-- //접속현황 -->

	<!-- snb 메뉴 -->

<!-- 	<div class="snb_menu">

		<div class="menu_box_top">

			<div class="bg_top_left"><img src="/images/admin/left_box_bg_02_01.gif" alt="left_box_bg_02_01" /></div>

			<div class="bg_top_center"></div>

			<div class="bg_top_right"><img src="/images/admin/left_box_bg_02_02.gif" alt="left_box_bg_02_02" /></div>

		</div>

		<?

		//사이트 관리

		if ($admin_login_yn == 'Y' && $c == 'admin') {

			if (strstr($m, 'view_site_meta')) {

				?>

				<div class="menu_box_middle">

					<div class="title">Site Management</div>

					<div class="menu_list">

						<ul>

							<li class="menu"><a href="/?c=admin&amp;m=view_site_meta" title="사이트 메타테그 관리">Site Meta Tag Management</a></li>

						</ul>

					</div>

				</div>

			<? } ?>

		<? } ?>

		<?

		//회원관리

		if ($admin_login_yn == 'Y' && $c == 'admin') {

			if (strstr($m, 'member') || strstr($m, 'pay')) {

				?>

				<div class="menu_box_middle">

					<div class="title">Membership</div>

					<div class="menu_list">

						<ul>



							<li class="menu"><a href="/?c=admin&amp;m=adm_member_list" title="회원정보">Member Profile</a></li>

							<li class="menu"><a href="/?c=admin&amp;m=view_member_grade" title="회원등급설정">Member Grade Settings</a></li>

							

							<li class="menu"><a href="/?c=admin&amp;m=adm_pay_list" title="회원결제관리">회원결제관리</a></li>

							<li class="menu"><a href="https://pgweb.dacom.net/pg/wmp/LoginMertAdmin.jsp" target="_blank" title="상점관리자(LG텔레콤)">상점관리자(LG텔레콤)</a></li>

							//

						</ul>

					</div>

				</div>

			<? } ?>

		<? } ?>

		<?

		//커뮤니티 관리

		if ($admin_login_yn == 'Y' && $c == 'admin') {

			if (strstr($m, 'adm_bbs')||$m=' ') {

				?>

				<div class="menu_box_middle">

					<div class="title">Items</div>

					<div class="menu_list">

						<ul>

							<?

							if (count($site_map_list) > 0) {

								$menu_code = '';

								$menu_code_name = '';

								$menu_url = '';

								foreach ($site_map_list as $rows) {

									if ($rows->menu_type == 'bbs') {

										$menu_code = $rows->menu_code;

										$menu_code_name = $rows->menu_code_name;

										$menu_url = $rows->menu_url;

										?>

										<li class="menu"><a href="/?c=admin&amp;m=adm_bbs_list&amp;mcd=<?= $menu_code ?>" title="<?= $menu_code_name ?>"><?= $menu_code_name ?></a></li>

				<? } ?>

					<? } ?>

				<? } ?>

						</ul>

					</div>

				</div>

			<? } ?>

		<? } ?>

<?

//접속통계관리

if ($admin_login_yn == 'Y' && $c == 'admin') {

	if (strstr($m, 'statistics')) {

		?>

				<div class="menu_box_middle">

					<div class="title">Statistics</div>

					<div class="menu_list">

						<ul>
						   
						   <li class="menu"><a href="#">Visitor Today</a></li>

						</ul>

					</div>

				</div>

			<? } ?>

<? } ?>

<?

//팝업관리

if ($admin_login_yn == 'Y' && $c == 'admin') {

	if (strstr($m, 'popup')) {

		?>

				<div class="menu_box_middle">

					<div class="title">Popup</div>

					<div class="menu_list">

						<ul>

							<li class="menu"><a href="/?c=admin&amp;m=adm_popup_list" title="팝업관리">Popup</a></li>

						</ul>

					</div>

				</div>

	<? } ?>

<? } ?>

<? if ($admin_login_yn == 'Y' && $iwc_login_yn == 'Y' && $c == 'iwc') { //ROOT 관리자  ?>

 			<div class="menu_box_middle">

				<div class="title">ROOT</div>

				<div class="menu_list">

					<ul>

						<li class="menu"><a href="/?c=iwc&amp;m=view_site_meta" title="사이트 메타테그 설정">사이트 메타테그 설정</a></li>

						<li class="menu"><a href="/?c=iwc&amp;m=view_site_admin" title="사이트 관리정보 설정">사이트 관리정보 설정</a></li>

						<li class="menu"><a href="/?c=iwc&amp;m=view_site_map" title="사이트 맵 설정">사이트 맵 설정</a></li>

						<li class="menu"><a href="#" title="디자인 관리">디자인 관리</a></li>

						<li class="sub_menu"><a href="/?c=iwc&amp;m=view_site_design_sub" title="서브 페이지 HTML">- 서브 페이지 HTML</a></li>

						<li class="sub_menu"><a href="/?c=iwc&amp;m=view_site_design_left_menu" title="LEFT 메뉴 HTML">- LEFT 메뉴 HTML</a></li>







						<li class="menu"><a href="#" title="커뮤니티 관리">커뮤니티 관리</a></li>

						<?

						if (count($site_map_list) > 0) {

							$menu_code = '';

							$menu_code_name = '';

							$menu_url = '';

							foreach ($site_map_list as $rows) {

								if ($rows->menu_type == 'bbs') {

									$menu_code = $rows->menu_code;

									$menu_code_name = $rows->menu_code_name;

									$menu_url = $rows->menu_url;

									?>

									<li class="sub_menu"><a href="/?c=iwc&amp;m=view_bbs_config&amp;mcd=<?= $menu_code ?>&amp;mcd_name=<?= urlencode($menu_code_name) ?>" title="<?= $menu_code_name ?>">- <?= $menu_code_name ?></a></li>

			<? } ?>

		<? } ?>

	<? } ?>



						<li class="menu"><a href="#" title="회원 관리">회원 관리</a></li>

						<li class="sub_menu"><a href="/?c=iwc&amp;m=view_member_grade" title="회원등급설정">- 회원등급설정</a></li>

						<li class="sub_menu"><a href="/?c=iwc&amp;m=view_member_join_config" title="회원가입설정">- 회원가입설정</a></li>

						<li class="sub_menu"><a href="/?c=iwc&amp;m=view_member_login_config" title="회원로그인설정">- 회원로그인설정</a></li>



					</ul>

				</div>

			</div>

<? } ?>

		<div class="menu_box_bottom">

			<div class="bg_bottom_left"><img src="/images/admin/left_box_bg_02_03.gif" alt="left_box_bg_02_03" /></div>

			<div class="bg_bottom_center"></div>

			<div class="bg_bottom_right"><img src="/images/admin/left_box_bg_02_04.gif" alt="left_box_bg_02_04" /></div>

		</div>

	</div> -->

	<!-- //snb 메뉴 -->

</div>

