<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/css/admin/admin.css" />
     
<style>
#change_customer{
  width: 280px;
  padding-left: 15px;
}
.title{ 
  width: 270px;
  padding: 10px 0px 0px 15px;
}
.select_container{
  padding: 5px 0;
}
.select_container select{
  -moz-appearance: tabpanels;
  background-color: transparent;
  box-shadow: none;
  cursor: auto;
  display: block;
  font-size: 100%;
  height: 30px;
  line-height: normal;
  margin: 0;
  padding: 0 10px;
  width: 270px;
}
.btn_container{
  border: solid 0px #000; 
  padding-top: 10px; 
  width: 270px;
}
.btn_container input{
  width: 132px;
}
.btn_container .input1{
  padding: 3px 0px;
}
.btn_container .input2{
  padding: 3px 0px;
}
.msg{
  min-height: 20px;
  width: 270px;
  padding-left: 15px;
}
</style>
</head>
<body>
    <h3 class="title">Change Supporter</h3>
    <p class="msg" id="msg"><?= $msg ?></p>
    <form id="change_customer" action="" method="post">
      <div class="select_container">
      <input type="hidden" name="id" id="id" value="<?= $_GET['id']; ?>">
      <input type="hidden" id="country" name="country" value="<?= $_GET['country']; ?>">
      <select name="selected_member" id="selected_member">
          <?php foreach($customer_services as $cs){ ?>
              <option value="<?= $cs->member_no ?>" <?= ($cs->member_no==$selected_supporter[0]->support_member_no) ? 'selected="selected"': '' ?>><?= $cs->member_name ?></option>
          <?php } ?>
      </select>
      </div>
      <div class="btn_container">
        <input id="add_button" class="input1 blue" type="submit" value="Change" name="change">
        <input id="cancel_button" onclick="parent.jQuery.fancybox.close()" class="input2 blue" type="button" value="Cancel" name="cancel">
      </div>
    </form>
</body>
</html>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
  $("#add_button").attr("disabled", "disabled");
  $("#selected_member").change(function(e){
    var country = $("#country").val();
    var member_no = $(this).val();
    var data = {
      country: country,
      member_no: member_no,
    }
    $.ajax({
      type: "POST",
      url: "/?c=admin&m=adm_validate_change_supporter",
      data: data,
      success: function(data) {
          $("#msg").html("");
          if (data>0) {
              $(".msg").html("The supporter set is already!");
              $("#add_button").attr("disabled", "disabled");
          } 
          else{
            $("#add_button").removeAttr('disabled');
          }
      }
    });
  });
});
</script>