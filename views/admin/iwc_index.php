<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Expires" content="-1" /> 
<meta http-equiv="Pragma" content="no-cache" /> 
<meta http-equiv="Cache-Control" content="no-cache" />
<title><?= $title ?></title>
<link rel="stylesheet" type="text/css" href="/css/admin/admin.css" />
<script type="text/javascript" src="/js/common/common.js"></script>
<script type="text/javascript" src="/js/common/ajax.js"></script>
<script type="text/javascript" src="/js/admin/admin.js"></script>
<link rel="stylesheet" type="text/css" href="/editor/xquared/stylesheets/xq_ui.css" />
<script type="text/javascript" src="/editor/xquared/javascripts/XQuared.js?load_others=1"></script>
<script type="text/javascript" src="/editor/xquared/javascripts/plugin/swfupload/AC_OETags.js"></script>
<script type="text/javascript" src="/editor/xquared/javascripts/plugin/swfupload/swfupload.js"></script>
<script type="text/javascript" src="/editor/xquared/javascripts/plugin/swfupload/swfupload.queue.js"></script>
<script type="text/javascript" src="/editor/xquared/javascripts/plugin/FileUploadPlugin.js"></script>
<!-- Lightbox v2.04 -->
<link rel="stylesheet" href="/css/common/lightbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/js/common/prototype.js" ></script>
<script type="text/javascript" src="/js/common/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="/js/common/lightbox.js"></script>
</head>
<body>
<div id="wrap">
    <!-- header -->
<?= $inc_top ?>
    <!-- //header -->
    <!-- container -->
    <div id="container">
        <!-- left -->
<?= $inc_left ?>
        <!-- //left -->
        <!-- contents -->
        <div class="contents">
            <div class="contents_box_top">
                <div class="bg_top_left"><img src="/images/admin/con_box_bg_01_01.gif" alt="con_box_bg_01_01" /></div>
                <div class="bg_top_center"></div>
                <div class="bg_top_right"><img src="/images/admin/con_box_bg_01_02.gif" alt="con_box_bg_01_02" /></div>
            </div>
<?= $iwc_contents ?>
            <div class="contents_box_bottom">
                <div class="bg_bottom_left"><img src="/images/admin/con_box_bg_01_03.gif" alt="con_box_bg_01_03" /></div>
                <div class="bg_bottom_center"></div>
                <div class="bg_bottom_right"><img src="/images/admin/con_box_bg_01_04.gif" alt="con_box_bg_01_04" /></div>
            </div>
        </div>
        <!-- //contents -->
    </div>
    <!-- //container -->
    <!-- footer -->
<?= $inc_bottom ?>
    <!-- //footer -->
</div>
</body>
</html>