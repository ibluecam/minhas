
<link rel="stylesheet" type="text/css" href="/css/admin/popup.css" />
 
<?php 
function updateCurrentGetUrl($p=array(), $unset_p=array()){
    $get = $_GET;
    if(count($unset_p)>0){
        foreach($unset_p as $value){
            if(isset($get[$value])) unset($get[$value]);
        }
    }
    if(count($p)>0){
        foreach($p as $key=>$value){
            $get[$key]=$value;
        }
    }
    $updatedUrl_arr = array();
    foreach($get as $key=>$value){
        $updatedUrl_arr[]=$key."=".$value;
    }
    $updatedUrl = implode("&", $updatedUrl_arr);
    return $updatedUrl;
}


?>







<div class="contents_box_middle">
        <input type="hidden" id="list" value="<?= $list ?>"/>
        <h1 class="title">Cars for Sales Team</h1>
        
				

        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You can search add items supporter by different modes.</li>

            </ul>

        </div>
        <div class="tab_container">
            <input type="button" data-content-id="custom_table_container"  class="tab selected_tab" id="tab_customer"  value="Car List">
          
            <input type="button" data-content-id="country_table_container" id="tab_country" class="tab" value="Country List">

            <input type="button" data-content-id="default_table_container" id="tab_default" class="tab" value="Default">

             
        </div>

        <!-- START TABLE FOR CUSTOM -->        
        <div class="table_list_box tab_content" style="padding-top:10px; " id="custom_table_container">

            <h1 class="table_title">Car List</h1>

            <div class="search_box">

                <div class="search_result">

                    <img src="/images/admin/search_icon.gif" alt="아이콘" align="absmiddle" />Total : <span id="current_num_rows" class="title">N/A</span> /<span id="total_num_rows" class="title"> N/A</span> cars

                </div>
                
                <div class="search_condition">
                   
                  <form name="bbs_search_frm" id="adm_frm" method="get" action="" >
                        <input type="hidden" value="admin" name="c">
                        <input type="hidden" value="adm_item_supporter" name="m">
                        
                        <select id="sch_mode" name="search_mode">
                          <option value="">Show All</option>
                          <option <?php if($search_mode=='exist') echo "selected";?> value="exist">Show Only Supported</option>
                        </select>
                        <select id="sch_make" name="search_make">
                          <option value="">Make</option>
                          <?php
                            foreach($make_list as $make){
                                if($search_make==$make->car_make){
                                    $select="selected";
                                }else{
                                    $select="";
                                }
                                echo '<option '.$select.' value="'.$make->car_make.'">'.$make->car_make.'</option>';
                            }
                          ?>
                        </select>
                        <select id="sch_model" name="search_model">
                          <option value="">Model</option>
                            <?php 
                                foreach($get_model_list as $model){ 

                                    if($search_model==$model->car_model){
                                        $select="selected";
                                    }else{
                                        $select="";
                                    }

                            ?>
                                <option <?php echo $select; ?> class="<?php echo $model->car_make; ?>" value="<?php echo $model->car_model; ?>"><?php echo $model->car_model; ?></option>
                            <?php }  ?>
                        </select>
                        <select name="search_year">
                          <option value="">Year</option>
                          <?php 
                            for($y=date("Y"); $y>= 1991; $y--){
                                if($search_year==$y){
                                    $select="selected";
                                }else{
                                    $select="";
                                }
                                echo '<option '.$select.' value="'.$y.'">'.$y.'</option>';
                            }
                          ?>
                        </select>
                        <input id="support_keyword" value="<?= $search_keyword ?>" name="search_keyword" type="text" placeholder="Search by chassis no"/>
                        <input id="search_btn" type="submit" name="search_btn" value="SEARCH"/>

                   </form>
                   <br>

                    
                </div>


            </div>
            <form name="adm_form_car" id="adm_form_car" method="post" enctype="multipart/form-data" action="">
                <input type="hidden" value="" name="support_member_no" id="support_member_no"/>
                <div class="top_button_container">
                    <label id="car_check_how_tip" style="float:left;">- Select one or more cars to set or remove sale from car</label>
                    <label id="car_check_count_tip"></label>
                    <!-- <input type="submit" value="Set Sale" disabled="" name="submit_set_idx" class="button white" /> -->
                    <a id="btn_set_car" style="font-size:14px;display:none; color:white;" class="fancybox_add button white" href="#pop_add_by_idx">Set Sale</a>
                    <a id="btn_set_car_dis" style="font-size:14px;" class="button white" href="#pop_add_by_idx">Set Sale</a>
                    <input style="display:none;" type="submit" id="btn_remove_by_idx" value="Remove Sale" name="submit_remove_idx" class="button white"/>
                    <button type="button" data-toggle="modal" id="btnDelete"   class="button red"><span></span> <b id='del'>Remove Sale</b></button>
                        
                </div>
                 <div style="overflow:auto; float: left;width: 100%;" >
                    
                       
                    <input type="hidden" name="address"> <!-- sending url address -->

                    <table class="table_list" cellspacing="0" style="width:100%; min-width: 800px;">

                        <caption class="hidden"></caption>

                        <thead>
                      
                            <tr>

                                <th scope="col"><input type="checkbox" class="id_chk_all" id="car_list" value=""/>
                                </th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                                <th scope="col">ID</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                                
                                <th scope="col">Car</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                                <th scope="col">Chassis No.</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                                <th scope="col">Country</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                                <th scope="col">Owner</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                                <th scope="col">Sales</th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>

                                <th scope="col"></th>
                                <th scope="col" class="line"><img src="/images/admin/board_bar_line.gif"></th>
                            </tr>



                        </thead>

                        <tbody>
                      <?php foreach ($support_car_list as $car) { ?>  
                            <tr>

                            <td><input type="checkbox" name="idx[]" data-title="<?php echo $car->idx; ?>" class="idx_check" value="<?php echo $car->idx; ?>" /></td>

                            <td class="line">&nbsp;</td>
                            <td><?php echo $car->idx; ?></td>
                            <td class="line">&nbsp;</td>
                            <td><?php echo $car->car_model_year .' '. $car->car_make. ' '.$car->car_model; ?></td> 
                            <td class="line">&nbsp;</td>
                            <td><?php echo $car->car_chassis_no; ?></td>
                            <td class="line">&nbsp;</td> 
                            <td><?php echo $car->country_name; ?></td> 
                            <td class="line">&nbsp;</td>
                            <td><?php echo $car->contact_person_name; ?></td>
                            <td class="line">&nbsp;</td> 
                            <td><?php echo $car->support_first_name ." ".$car->support_last_name; ?></td> 
                         

                         </tr>
                
                            <?php }?>
                        </tbody>

                    </table>

                </div>
            </form>
            <div class="page_navi">
                <?= $pagination;?>
            </div>
            
            <div class="btn_area_center">
                <!-- <div class="btn_area_left">
                    <a class="fancybox" href="#excel_export_popup"><span class="button green">Excel Export</span></a>
                   
                </div> -->
                <div class="btn_area_right">
                    
                    
                </div>
            </div>

            
          
        </div>
        <!-- END TABLE FOR CUSTOM -->
        <!-- START TABLE FOR COUNTRY-->
        <div class="table_list_box tab_content" style="padding-top:10px; display:none;" id="country_table_container">

            <h1 class="table_title">Country List</h1>

            <div class="btn_area_center">
                <!-- <div class="btn_area_left">
                    <a class="fancybox" href="#excel_export_popup"><span class="button green">Excel Export</span></a>
                   
                </div> -->
                <div class="btn_area_right">
                    <a  class="fancybox_add_country iframe" href="/?c=admin&amp;m=adm_add_item_country_supporter"><span class="button blue">Add Sale</span></a>
                    <!-- <a href="#"  onclick="delete_all_supporter();" title="delete"><span class="button red">Remove Sale</span></a> -->
                    <button type="button" data-toggle="modal"  id="btnDelete_con" class="button red"><span></span> <b id='del'>Remove Sale</b></button>
                </div>
            </div>

             <div style="overflow:auto; float: left;width: 100%;" >
                <form name="adm_frm_3" id="adm_frm_3" method="post" enctype="multipart/form-data" action="">
                   <input type="hidden" value="country" name="list">
                <input type="hidden" name="address"> <!-- sending url address -->

                <table class="table_list" cellspacing="0" style="width:100%; min-width: 800px;">

                    <caption class="hidden"></caption>

                    <thead>
                  
                        <tr>

                            <th scope="col"><input type="checkbox" name="idx" class="id_chk_all" id="country_list"
                                                onclick="checkbox_all_check(this, document.adm_frm_3.idx);"
                                                   value=""/>
                            </th>
                            <th scope="col">Country</th>
                            <th scope="col">Supporter</th>
                            <th scope="col" class="th_change"></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($support_country_list as $support) { ?>  
                            <tr>
                                <td><input type="checkbox" name="idx[]" id="idx" data-title="<?= $support->id; ?>" class="contry" value="<?= $support->id; ?>" /></td>
                                <td><?= $support->country_name; ?></td>
                                <td><?= $support->support_name; ?></td> 
                                <td class="td_change"><a id="fancybox_change" class="fancybox_change iframe" href="/?c=admin&amp;m=adm_change_supporter&id=<?= $support->id; ?>&country=<?= $support->cc; ?>"><img alt="modify" src="/images/admin/modify.gif"></a></td>

                            </tr>
            
                        <?php }?>
                    </tbody>

                </table>

            </div>
            
            <!-- <div class="page_navi">
                <div id="more" class="loading" style="text-align: center;">

                    <img  style="display:none" id="loading_more_bbs" src="images/ico-loading2.gif"/>
                </div>
                
            </div> -->
            <div class="page_navi"></div>
            
            

         
          </form>
        </div>
        <!-- END TABLE FOR COUNTRY -->
        <!-- START TABLE FOR DEFAULT -->
        <div class="table_list_box tab_content" style="padding-top:10px; display:none;" id="default_table_container">
            <div style="min-height:300px; text-align:center; ">
                <div style="margin-bottom:10px">Select Support for All Stocks</div>
                <div style="background: #eee none repeat scroll 0 0; border: 2px solid #bbb; margin: auto; padding: 20px; width: 300px;">
                    <form action="" method="post"> 
                        <table>
                            <tr>
                                <td>Sales Support: </td>
                                <td style="padding:10px;">
                                    <select name="default_member_no" style="padding: 5px 10px; width:100%;">
                                        <option value="">- Select Sales -</option>

                                        <?php 
                                            if($support_default!=NULL){
                                                $support_member_no = $support_default->support_member_no;
                                            }else{
                                                $support_member_no = 0;
                                            }
                                            foreach($customer_services as $service){ 
                                        ?>
                                            <option <?php if($support_member_no ==$service->member_no ) echo "selected"; ?> value="<?=$service->member_no?>">
                                                <?=$service->member_name?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr><td></td><td style="text-align:right; padding:10px;"><input type="submit" name="submit_support_default" class="button blue" value="Apply"></td></tr>
                        </table>
                    </form> 

                </div>
            </div>
        </div>
        <!-- END TABLE FOR DEFAULT -->
    </div>


    <div style="display:none">
        <div id="pop_add_by_idx">
            
            <!--- Option 2 -->
            <div id="idx_table">
              <form action="" method="post">
                <div class="top_btn_container">
                  <select style="padding:5px;" id="select_idx_support_no">
                      <option value="">- Select Supporter -</option>
                      <?php foreach($customer_services as $cs){ ?>
                          <option value="<?= $cs->member_no ?>"><?= $cs->member_name ?></option>
                      <?php } ?>
                  </select>
                  <input type="button" id="pupup_btn_set_car" class="button orange" value="SET"/>
                </div>
                <!-- <input type="hidden" name="idx[]" value=""/>
                 -->
                
              </form>
            </div>
            
        </div>
    </div>


    <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
              </div>
              <div class="modal-body" id="msgBody">
                  Are you sure want to remove sale from the selected cars?
              </div>
              <div class="modal-footer">
                <button type="submit" name="submit_remove_idx" class="btn btn-default" id="btn_yes">YES</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id='btnNO'>NO</button>
              </div>
            </div>
          </div>
        </div>

    <div class="modal" id="myModal2" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel_car">Confirmation</h4>
              </div><div class="container"></div>
              <div class="modal-body" id="msgBody_car">
                 Are you sure want to remove the  supporters from selected countries?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" id="btn_yes_con">YES</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id='btnNO'>NO</button>
              </div>
            </div>
          </div>
    </div>



<script type="text/javascript">

    $("#sch_model").chained("#sch_make");

    var count_check_idx = 0;
  
    function loadModel(car_make){
      $.getJSON('/?c=car_model&m=get_car_model&car_make='+car_make,function(json){
        $("#sch_model").html('<option value="">Model</option>');
        $.each(json,function(i,data_json){
          respone_data='<option value="' + data_json.car_model + '">' + data_json.car_model + '</option>';
          $( "#sch_model").append(respone_data);
        });
      });
    }
    function refresh_count_check(){
        if(count_check_idx>0){
            //btn set
            $("#btn_set_car").removeClass("white");
            $("#btn_set_car").addClass("blue");
            $("#btn_set_car").css("display", "");
            $("#btn_set_car_dis").css("display", "none");
            //$("#btn_set_car").removeAttr("disabled");
            //btn remove
            $("#btn_remove_by_idx").removeClass("white");
            $("#btn_remove_by_idx").addClass("red");
            //$("#btn_remove_by_idx").removeAttr("disabled");

            $("#car_check_count_tip").html(count_check_idx+" car(s) selected");
        }else{
            //btn set
            $("#btn_set_car").removeClass("blue");
            $("#btn_set_car").addClass("white");
            $("#btn_set_car").css("display", "none");
            $("#btn_set_car_dis").css("display", "");
            //$("#btn_set_car").attr("disabled", true);
            //btn remove
            $("#btn_remove_by_idx").removeClass("red");
            $("#btn_remove_by_idx").addClass("white");
            //$("#btn_remove_by_idx").attr("disabled", true);

            $("#car_check_count_tip").html('');
        }
    }
    $(document).ready(function(e){

         $('#car_list').click(function(event) {
      if(this.checked) {
          $(':checkbox').each(function() {
              this.checked = true;
              $("#btnDelete").attr("href", "#myModal");
              $("#btnDelete").addClass("red");
          });
      }
      else {
        $(':checkbox').each(function() {
              this.checked = false;
              $('#btnDelete').removeAttr("href");
              $('#btnDelete').removeClass("red");
          });
      }
    });

        $('.idx_check').click(function(event) {   
        if(this.checked) {
            this.checked = true;
            $("#btnDelete").attr("href", "#myModal");
            $("#btnDelete").addClass("red");
           
        }
        else{
          $('#btnDelete').removeAttr("href");
          $('#btnDelete').removeClass("red");
         
        }
        });
        
        $("#btn_yes").click(function (){
            $("#btn_remove_by_idx").click();
        });


        // delete update car


         $('#country_list').click(function(event) {
      if(this.checked) {
          $(':checkbox').each(function() {
              this.checked = true;
              $("#btnDelete_con").attr("href", "#myModal2");
          });
      }
      else {
        $(':checkbox').each(function() {
              this.checked = false;
              $('#btnDelete_con').removeAttr("href");
          });
      }
    });

        $('.contry').click(function(event) {   
        if(this.checked) {
            this.checked = true;
           $("#btnDelete_con").attr("href", "#myModal2");
           
        }
        else{
         $('#btnDelete_con').removeAttr("href");
         
        }
        });
        
        $("#btn_yes_con").click(function (){
            var abc ='';
            $(".contry").each(function (){
                if ($(this).is(":checked") ===true)
                  

                    abc += $(this).attr("data-title") + ";";
            });
            var ajax = $.ajax({
                url: "?c=admin&m=adm_supporter_delete",
                type: 'POST',
                data : {
                    
                    idx : abc    
                }
            });
            ajax.done(function (msg){
                $("#myModal").modal("hide");
                window.location.href = "/?c=admin&m=adm_item_supporter&list=country";
            });
        });


        // delete country
        $("a.fancybox_change").fancybox({
            height  : 150,
            width   : 300,
            onClosed: function() {
                // /window.location = window.location;
            }
        });
        /*** When button check all pressed ***/
        $(".id_chk_all").change(function(e){
            $('.idx_check').prop('checked', $(this).prop('checked'));
            count_check_idx = $('.idx_check:checked').length;
            refresh_count_check();
        });
        /*** When user click any check box ***/
        $('.idx_check').change(function(e){
            count_check_idx = $('.idx_check:checked').length;
            refresh_count_check();
        });
        /*** When user change make in select search ***/
        $('#sch_make').change(function(e){
            var car_make = $(this).val();
            loadModel(car_make);
        });
        
        $("#close").click(function(e){
            $.fancybox.close();
            //extractImageFromPdf();
        });
        $("#btn_set_car_dis").click(function(e){
            $("#car_check_count_tip").html("Please select some cars first!");
            
        });
        // $("#btn_remove_by_idx").click(function(e){
        //     if(count_check_idx<=0){
        //         $("#car_check_count_tip").html("Please select some cars first!");
        //         return false;
        //     }else{
        //         if(confirm("Are you sure want to remove sale from the selected cars?")){
        //             return true;
        //         }else{
        //             return false;
        //         }
        //     }
        // });
        
        /*** Check which tab should be open on page ready ***/
        var list = $("#list").val();

        if(list=='country'){
            show_tab('country_table_container');
        }
        /*** HANDLE TAB PRESS ***/
        $(".tab").click(function(e){
            var container_id = $(this).attr("data-content-id");
            //alert(container_id);
            show_tab(container_id);
        });

        $("#pupup_btn_set_car").click(function(e){
            var support_member_no = $("#select_idx_support_no").val();
            //alert(support_member_no);
            $("#support_member_no").val(support_member_no);
            $("#adm_form_car").submit();
        });
        $("a.fancybox_add").fancybox({
            height  : '100%',
            width   : 800,
            onClosed: function() { 
                $('body').css('overflow', 'auto');
                //window.location = window.location;
            },
            onStart: function() { 
                $('body').css('overflow', 'hidden');
            }
        }); 
        $("a.fancybox_add_country").fancybox({
            
            width   : 400,
            onClosed: function() { 
                $('body').css('overflow', 'auto');
                window.location = "/?c=admin&m=adm_item_supporter&list=country";
            },
            onStart: function() { 
                $('body').css('overflow', 'hidden');
            }
        }); 
        
    });

    function show_tab(container_id){
        $(".tab_content").css("display","none");
        $("#"+container_id).css("display", "");
        $(".tab").removeClass('selected_tab');
        
        $(".tab[data-content-id='"+container_id+"']").addClass('selected_tab');
    }

   


    function checkbox_all_check(obj, target) {
    if ( typeof obj == 'undefined' || typeof target == 'undefined') {

    }
    else if ( typeof target.length == 'undefined' ) {
      target.checked = obj.checked;
    }else{
      for (i = 0 ; i < target.length ; i++) {
        target[i].checked = obj.checked;

      }
    }
  }

  
        
        
        
    

  // function delete_all_supporter() {
  //   var f  = document.adm_frm_3;
  //   var fg = false;

  //   if(f.idx != null) {
  //     for(i=0; i<f.idx.length; i++) {
  //       if(f.idx[i].checked) {
  //         fg = true;
  //       }
  //     }

  //     if(!fg) {
  //       alert('Please select supporter that you want to delete.');
  //       return;
  //     }

  //     if (!confirm('Are you sure want to remove the  supporters from selected countries?'))

  //         {
  //             return;
              
  //         }else{

  //           f.action = '?c=admin&m=adm_supporter_delete';
                       
                        
  //       f.submit();
  //         }
      
  //   }
  // } 


</script>



       

