<!DOCTYPE HTML>
<html>
  <head>
  <style type="text/css">
  #upload-file-container{
    background: url("/images/public_mobile_site/enter_info/btn_my_info.png") no-repeat;
    cursor: pointer;
    height: 30px;
  }

  #upload-file-container input {
     filter: alpha(opacity=0);
     opacity: 0;
     cursor: pointer;
  }
  #upload-file-container span {
     left: 32px;
    position: absolute;
    top: 16px;
  }
  </style>

    <meta charset="utf-8">
    <title>PhotoAlbum - Upload page</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script> 
    <link rel="stylesheet" type="text/css" href="/css/admin/upload_iframe.css" />
  </head>
  
  <body>
   
    <div id="mynewcontainer" class="upload_container">
    <div id="list">
      
    </div>
      
      <form id="image_form" name="adm_frm" action="/?c=admin&m=mobile_adm_cloud_upload_exec&member_no=<?=$_SESSION['ADMIN']['member_no']?>" method="post" enctype="multipart/form-data">
      <input type="hidden" id="member_no" value="<?=$_SESSION['ADMIN']['member_no']?>" name="member_no"/>
      
      <!-- <a class="upload_img newstyle" id="upload_widget_opener" href="#">Upload images</a> -->
      <div id="upload-file-container">
       <input type="file" id="attach" class="newstyle" name="file[]" id="fileToUpload" multiple accept=".jpg, .jpeg, .gif, .png">
    </div>
      <?php
            $ifile = true;
            $modi_att = count($select_cphoto_list);
            if (count($select_cphoto_list) > 0) {
                echo "<div class='div-img-title'";
                
                echo "</div>";
                $first_primary=true;
                echo '<ul id="sortable">';
                foreach ($select_cphoto_list as $att_rows) {
                  echo "<li>";
                  echo "<div class='div-image-view'>";
                  echo "<img src='" . $att_rows->base_url . "t_item_thumb/".$att_rows->public_id."' width='150' >";
                  ?>
                  <!-- <a href="/?c=admin&m=user_delete_attach_cloud&public_id=<?php echo $att_rows->public_id; ?>" class="close" onClick="confirm_delete();" title="Delete"></a> -->
                  <label class="label_profile"><input type="radio" value="<?=$att_rows->id ?>" class="profile" name="profile" <?php if($att_rows->profile_image_id==$att_rows->id&&$first_primary) echo "checked";  ?>/>
                  <span>Profile</span>
                  <span><a style="margin-left: 55px;" href="/?c=admin&m=mobile_user_delete_attach_cloud&public_id=<?php echo $att_rows->public_id; ?>" onClick="confirm_delete();" title="Delete"><img src="images/public_mobile_site/enter_info/icon_delete.png"></a></span>
                  </label>  
                 
                  <?php
                  echo "</div>";

                  echo "</li>";

              } 
              echo "</ul>"; 
            }
            ?>
      </form>
    </div>
<script type="text/javascript">
var invalid_exts = [];
    $(document).ready(function(){
        handleFileSelect()
        $('#attach').change(function(){
          if(invalid_exts.length>0){
            ext_string = invalid_exts.join(', *.');
            alert("Files with extension: *."+ext_string+" are not allowed. \nPlease try again with *.jpg, *.jpeg, *.gif, or *.png.");
            $(".attach").val('');
          }else{
            $("#image_form").submit();
          }
        });
    });
  function confirm_delete() {
    if (confirm('Do you want to delete image?')) {
        return true;      
    } else {
        return false;
    }
  } 
  function handleFileSelect() {
    var files = $("#attach")[0].files; // FileList object
    var allowed_ext = ['jpg', 'jpeg', 'png', 'gif'];
    invalid_exts =[];
    // files is a FileList of File objects. List some properties.
    for (var i = 0, f; f = files[i]; i++) {

      var filename = f.name;
      var ext = filename.substr( (filename.lastIndexOf('.') +1) );
      ext = ext.toLowerCase();
     //$("#list").append(ext);
      if($.inArray(ext, allowed_ext)===-1){
        if($.inArray(ext, invalid_exts)===-1){
            invalid_exts.push(ext);
        }
      }
    }
        
  }
  $(".profile").change(function(e){
    var member_no = $("#member_no").val();
    var profile_image_id = $(this).val();
    var data = {
      member_no: member_no,
      profile_image_id: profile_image_id,
    }
    $.ajax({
      type: "POST",
      url: "/?c=admin&m=adm_set_profile_image",
      data: data
    });
  });
  </script>

  </body> 
</html>