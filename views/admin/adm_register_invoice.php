<div class="contents_box_middle">
        <input type="hidden" id="list" value=""/>
        <h1 class="title">Register Car Invoice</h1>
        
        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You select cars and buyers' email that you want to register.</li>

            </ul>

        </div>

        <div class="table_list_box tab_content" style="padding-top:10px; overflow:auto; " id="custom_table_container">

            <form name="adm_form_car" id="adm_form_car" action="/?c=admin&m=adm_invoice_register_exec" method="post" enctype="multipart/form-data">
                
                <div class="top_button_container">
                    <h1 class="table_title label_title">Choose Destination</h1>

                    <select name="des_country" id="des_country" class="input_destination_country" required="required">
                        <option value="">Select Country</option>
                        <? 
                        foreach ($list_destination as $destination) {
                        ?>
                        <option value="<?= $destination->country_iso ?>"><?= $destination->country_name ?></option>
                        <?
                        }
                    ?>
                    </select>

                    <select name="portname" id="portname" class="input_destination_port" required="required">
                        <option>Select Port</option>
                    </select>
                </div>

                <div class="left_container" style="overflow:auto; float: left;width: 50%; padding-right:5px;" >
                	
                    <div class="top_button_container">

                    <h1 class="table_title" style="display:inline; float:left;margin-top: 10px;">Car List</h1>
                    <input type="button" id="btn_remove_car" onclick="remove_all()" value="Remove" name="btn_remove_car" class="button red"/>
                    
                    </div>

                	 <table class="table_list" cellspacing="0" style="width:100%;">
                	 	<thead>                      
                            <tr>
                              
                                <th scope="col"><input class="idx_chk_all" type="checkbox" value=""></th>
                                <th scope="col"></th>
                                <th scope="col">Car</th>
                                <th scope="col">Chassis No.</th>
                                <th scope="col">Discount</th>
                                
                            </tr>

                        </thead>
                        <tbody>
                        	<?php 
                            $no_image_primary = "https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_big_d2odu9";
                        		foreach($car_list as $car){
                                    
                                    $image_car = ($car->public_id!="") ? $car->base_url.$car->public_id : $no_image_primary;

                        			echo '<tr class="car_rows car_data_'.$car->idx.'">';
                                    echo '<td><input type="checkbox" class="idx_check" value="'.$car->idx.'" id="idx"/></td>';
                                    echo '<td>
                                            <input type="hidden" id="car_image_'.$car->idx.'" name="car_image['.$car->idx.']" value="'.$image_car.'"/>
                                            <img src="'.$image_car.'" width="50px" height="50px" id="image_'.$car->idx.'" /></td>';

                        			echo '<td>'.$car->car_model_year.' '.$car->car_make.' '. $car->car_model.'</td>';
                        			echo '<td><input type="hidden" id="idx" value="'.$car->idx.'" name="idx[]"/>'.$car->car_chassis_no.'</td>';
                        			echo '<td><input type="text" name="discount['.$car->idx.']" class="input_discount" maxlength="3" placeholder="0" /> &nbsp;&nbsp;&nbsp;USD</td>';
                        			echo '</tr>';
                        			
                        		} 


                        	?>
                        </tbody>
                	 </table>
                </div>
                 <div class="right_container" style="overflow:auto; float: left;width: 50%; padding-left:5px;" >
                    
                    <div class="top_button_container">

                    <h1 class="table_title" style="display:inline; float:left;margin-top: 10px;">Customer List</h1> 

                    <a  class="fancybox_adds iframe" id="fancybox_adds" href="/?c=admin&amp;m=adm_add_customer_bbs"><span class="button blue">Select Customer</span></a>
                    <input type="submit" id="btn_sent_email_by_idx" value="Create Invoices Now" name="submit_send_email" class="button blue"/>

                    </div>
                    <table class="table_list" cellspacing="0" style="width:100%;">

                  

                        <thead>
                      
                            <tr>

                                <th scope="col"><!-- <input type="checkbox" class="id_chk_all" value=""/> -->
                                </th>
                                
                                <th scope="col">Name</th>
                                
                                <th scope="col">Email</th>

                                <th scope="col">Country</th>
                                
                                
                                <th scope="col"></th>
                                
                            </tr>



                        </thead>

                        <tbody class="list_email_customer" id="list_email_customer">
                
                        </tbody>

                    </table>

                </div>
            </form>
            <div class="page_navi">
               
            </div>
            
            <div class="btn_area_center">
                <!-- <div class="btn_area_left">
                    <a class="fancybox" href="#excel_export_popup"><span class="button green">Excel Export</span></a>
                   
                </div> -->
                <div class="btn_area_right">
                    
                    
                </div>
            </div>

            
          
        </div>


    </div>
<script src="//widget.cloudinary.com/global/all.js" type="text/javascript"></script>
   
<script type="text/javascript" language="javascript" src="js/jquery.ajaxQueue.min.js"></script>

<script type="text/javascript"> 
    $("a.fancybox_adds").fancybox({
            width   : 600,
            height : '100%'
    }); 
    $("a.fancybox_add").fancybox({
            width   :300
        }); 
    $(document).ready(function(e){
        $(".input_discount").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message //
                 return false;
            }
       });
        /*** When check checkbox car pressed ***/
        $(".idx_chk_all").change(function(e){
            $('.idx_check').prop('checked', $(this).prop('checked'));
        });
        $("#btn_sent_email_by_idx").css("display", "none");

    });
    
    function remove_all() {
        var f = document.adm_form_car;
        var fg = false;
        var idxs = new Array();

        if (f.idx != null) {
            for (i = 0; i < f.idx.length; i++) {
                if (f.idx[i].checked) {
                    fg = true;
                    idxs[i] =f.idx[i].value;
                }
            }

            if (!fg) {
                alert('Please select cars that you want to remove.');
                return;
            }

            if (!confirm('Do you want to remove this car?'))
            {
                return;

            }else{
                for(var i = 0; i<idxs.length; i++) {
                    $('.car_data_'+idxs[i]).remove();

                }                
            }

        }
    }
   
    function append_data_email(data){
       $('.list_email_customer').html(data);
       $("#btn_sent_email_by_idx").css("display", "");
       $("#customer_no").css("display", "none");
    }
    
    // select destination
    $(document).on("change","select[name*='des_country']",function() {
    $("#link_export").attr("onclick", "return false;");    

        $("#portname").empty();//To reset cities
        $("#portname").append("<option value=''>Select Port</option>");

            var country_to=$(this).val();
            $.getJSON('/?c=car_model&m=get_port_name&country_to='+country_to,function(json){
                $("select[name*='portname']").html("");
                $( "select[name*='portname']").append('<option value="">Select Port</option>');
             $.each(json,function(i,data_json){
                        
                    

                respone_data="<option title='" + data_json.port_name + "' value='" + data_json.port_name + "'>" + data_json.port_name_title + "</option>";
                $( "select[name*='portname']").append(respone_data);

            });

        });
    });
</script>



       

