<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<title>MEMBER SITE</title>
<!-- <script type="text/javascript" src="/js/common/jquery-1.7.2.js"></script> -->

<script src="/js/admin/jquery.min.js"></script>
<script src="/js/admin/jquery-migrate-1.0.0.js"></script>
<!-- Colorbox -->
<script src="/js/colorbox/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="/js/colorbox/colorbox.css" />
<!-- End Colorbox -->

  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="/css/admin/admin.css" />
<link rel="stylesheet" type="text/css" href="/css/admin/button.css" />
<link rel="stylesheet" type="text/css" href="/css/admin/datepicker.css">
<script type="text/javascript" src="/js/common/common.js"></script>
<script type="text/javascript" src="/js/admin/admin.js"></script>
<script type="text/javascript" src="/js/admin/datepicker.js"></script>

<script type="text/javascript" src="/js/admin/jquery.chained.min.js"></script>

<!--JQuery-->
<!--Start style foundation-->

<link rel="stylesheet" href="../../build/css/intlTelInput.css" />
    
<!--End style foundation-->
<!--Tiny Mce-->
<script type="text/javascript" src="./editor/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="./editor/tiny_mce/tiny_mce_load.js"></script>
<!--Fancy Box-->
<script type="text/javascript" src="./js/common/jquery.mousewheel-3.0.4.js"></script>
<script type="text/javascript" src="./js/common/jquery.fancybox-1.3.4.js"></script>
<script type="text/javascript" src="./js/common/jquery.fancybox.load.js"></script>
<link rel="stylesheet" type="text/css" href="./css/common/jquery.fancybox-1.3.4.css" media="screen" />
<!-- for delete -->
<link rel="stylesheet" href="/css/admin/bootstrap.min.css">
<script src="/js/admin/bootstrap.min.js"></script>
</head>
<body>
<div id="wrap">
    <!-- header -->
<?= $inc_top ?>
    <!-- //header -->
    <!-- container -->
    <div id="container">
        <!-- left -->
<?= $inc_left ?>
        <!-- //left -->
        <!-- contents -->
        <div class="contents">
            <div class="contents_box_top">
                <div class="bg_top_left"><img src="/images/admin/con_box_bg_01_01.gif" alt="con_box_bg_01_01" /></div>
                <div class="bg_top_center"></div>
                <div class="bg_top_right"><!--<img src="/images/admin/con_box_bg_01_02.gif" alt="con_box_bg_01_02" />--></div>
            </div>
<?= $admin_contents ?>
            <div class="contents_box_bottom">
                <div class="bg_bottom_left"><img src="/images/admin/con_box_bg_01_03.gif" alt="con_box_bg_01_03" /></div>
                <div class="bg_bottom_center"></div>
                <div class="bg_bottom_right"><img src="/images/admin/con_box_bg_01_04.gif" alt="con_box_bg_01_04" /></div>
            </div>
        </div>
        <!-- //contents -->
    </div>
    <!-- //container -->
    <!-- footer -->
<?= $inc_bottom ?>
    <!-- //footer -->
</div>
</body>
</html>