<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/css/admin/admin.css" />
 <link rel="stylesheet" type="text/css" href="/css/admin/button.css" />
    
<style>
.table_list{
    width: 100%;
    margin-bottom: 10px;
}

.search_btn_container {
    background: #eee none repeat scroll 0 0;
    margin-bottom: 10px;
    margin-top: 10px;
    padding: 10px;

}

.empty_td {
    color: gray;
    padding: 20px 0;
}
.search_change_container{
    display: inline;
}

.bottom_btn_container{
    background: #eee none repeat scroll 0 0;
    box-sizing: border-box;
    float: left;
    padding: 5px;
    text-align: center;
    width: 100%;
}
</style>
  


</head>
<body>
    <h1>Set Supporter for Cars</h1>
    
   
    <div class="main_container">
        <!--- Option 1 -->
        <div id="country_table">
            <form action="" method="post">
              <table class="table_list">
                  <tbody>
                    <tr>
                        <th></th>
                   
                        <th>Country</th>
                        
                    </tr>
                  </tbody>
                  <tbody>
                      <?php 
                          if(count($country_list)>0){
                            foreach($country_list as $country){ ?>
                                <tr>
                                    <td><input value="<?= $country->cc ?>" type="checkbox" name="selected_countries[]"/></td>
                                    <td><?php echo $country->country_name; ?></td>
                                </tr>
                      <?php }}else{ ?>

                          <tr>
                              <td class="empty_td" colspan="2">(No countries to insert)</td>
                          </tr>

                      <?php } ?>
                  </tbody>
              </table>
              <div class="bottom_btn_container">
                <select style="padding:5px;" name="selected_member">
                    <option value="">- Select Supporter -</option>
                    <?php foreach($customer_services as $cs){ ?>
                        <option value="<?= $cs->member_no ?>"><?= $cs->member_name ?></option>
                    <?php } ?>
                </select>
                <input type="submit" class="button orange" name="btn_set_country" value="SET"/>
              </div>
            </form>
        </div>
        

    </div>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
             // $("#select_support_type").change(function(e){
             //    var select_type = $(this).val();
             //    if(select_type=='car_country'){
             //        $("#country_table").css("display", "block");
             //        $("#custom_table").css("display", "none");
             //        $("#custom_search_fields").css("display", "none");
             //    }else if(select_type=='custom'){
             //        $("#custom_search_fields").css("display", "inline");
             //        $("#custom_table").css("display", "block");
             //        $("#country_table").css("display", "none");
             //    }
                
             // });

        });
    </script>           

</body>
</html>

                
                  
 
