<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Expires" content="-1" /> 
<meta http-equiv="Pragma" content="no-cache" /> 
<meta http-equiv="Cache-Control" content="no-cache" />
<title><?=$title?></title>
<link href="/css/admin/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/common/common.js"></script>
<script type="text/javascript" src="/js/admin/admin.js"></script>
</head>
<body>
<div id="wrap">
<div id="header"></div>
<div id="container">
    <div id="content">
        <div class="login_box">
<form name="frm" id="frm" method="post" action="/?c=admin&amp;m=login_exec" onsubmit="return login_chk(this);">
            <fieldset>
                <legend></legend>
                <p class="header"><img src="/images/admin/administrator_img.gif" width="175" height="24" alt="administrator" /></p>
                <p class="line"><img src="/images/admin/bg_line.gif" width="417" height="2" alt="라인선" /></p>
                <div class="login_box_left">
                    <img src="/images/admin/icon.gif" width="79" height="74" alt="login_icon" />
                </div>
                <div class="login_box_center">
                <p class="pb3">
                    <img src="/images/admin/id_img.gif" width="26" height="10" alt="id" align="middle" />
                    <input type="text" name="member_id" id="member_id" value="" class="input" style="width:145px" title="id" />
                </p>
                <p class="pt3">
                    <img src="/images/admin/pw_img.gif" width="26" height="10" alt="pwd" align="middle" />
                    <input type="password" name="member_pwd" id="member_pwd" value="" class="input" style="width:145px" title="password" />
                </p>                
                </div>
                <div class="login_box_right">
                    <input type="image" src="/images/admin/btn_login.gif" title="로그인 버튼" />
                </div> 
            </fieldset>
</form>
        </div>
        <!--
        <div class="login_footer">
            <p class="footer_logo"><img src="/images/admin/logo.gif" width="101" height="27" alt="인츠웍스 로고" /></p>
            <p class="footer_copy"><img src="/images/admin/copy.gif" width="272" height="10" alt="인츠웍스 카피라이터" /></p>
        </div>        
        //-->
    </div>
</div>
<div id="footer"></div>
</div>
</body>
</html>