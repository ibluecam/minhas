


<div class="contents_box_middle">
        <input type="hidden" id="list" value=""/>
        <h1 class="title">Send Car Recommendation</h1>
        
				

        <div class="tip_box">

            <p class="title"><img src="/images/admin/tip_img.gif" alt="tip" /></p>

            <ul>

                <li> - You select cars and buyers' email that you want to send recommendation to.</li>

            </ul>

        </div>
        <div class="progress_wrapper" style="display:none;height: 52px;overflow: hidden;">
            <div class="progress_bar">
                <div class="progress_step"></div>
                <span class="progress_percent"></span>

            </div>
        </div>
        <div class="progress_wrapper msg_wrapper" style="display:none;">
        </div>
        <div class="table_list_box tab_content" style="padding-top:10px; overflow:auto; " id="custom_table_container">

            

            
            <form name="adm_form_car" id="adm_form_car" action="/?c=admin&m=send_recommend_car_exec" method="post" enctype="multipart/form-data" action="">
                <input type="hidden" value="" name="support_member_no" id="support_member_no"/>
                <div class="top_button_container">
                    <h1 class="table_title label_title">Choose Email Template</h1>
                    <select class="input_select_template" name="template_send_car">
                        <option value="one_car_in_single_email">One Car / Email</option>
                        <option <?php if($send_mode=='multiple') echo 'selected'; ?> value="multiple_cars_in_single_email">Mulitple Cars / Email</option>
                    </select>
                    <label id="car_check_count_tip"></label>
                    <!-- <input type="submit" value="Set Sale" disabled="" name="submit_set_idx" class="button white" /> -->
                    <!-- <a id="btn_set_car" style="font-size:14px;display:none; color:white;" class="fancybox_add button white" href="#pop_add_by_idx">Set Sale</a>
                    <a id="btn_set_car_dis" style="font-size:14px;" class="button white" href="#pop_add_by_idx">Send Email Now</a> -->
                    <input style="display:none;" type="button" id="btn_cancel_email" value="Cancel Sending" name="submit_send_email" class="button white"/>
                    
                    <input type="button" id="btn_sent_email_by_idx" value="Send Email Now" name="submit_send_email" class="button white"/>

                        
                </div>
                <div class="left_container" style="overflow:auto; float: left;width: 50%; padding-right:5px;" >
                	
                    <div class="top_button_container">

                    <h1 class="table_title" style="display:inline; float:left;margin-top: 10px;">Car List</h1>
                    <input type="button" id="btn_remove_car" onclick="remove_all()" value="Remove" name="btn_remove_car" class="button red"/>
                    
                    </div>

                	 <table class="table_list" cellspacing="0" style="width:100%;">
                	 	<thead>
                      
                            <tr>

                               
                                <th scope="col"><input class="idx_chk_all" type="checkbox" value=""></th>
                                <th scope="col"></th>
                                <th scope="col">Car</th>
                                <th scope="col">Chassis No.</th>
                                <th scope="col">Owner</th>
                                
                            </tr>



                        </thead>
                        <tbody>
                        	<?php 
                            $no_image_primary = "https://res.cloudinary.com/softbloom/image/upload/v1435029821/no_image_big_d2odu9";
                        		foreach($car_list as $car){
                                    
                                    $image_car = ($car->public_id!="") ? $car->base_url.$car->public_id : $no_image_primary;

                        			echo '<tr class="car_rows car_data_'.$car->idx.'">';
                                    echo '<td><input type="checkbox" class="idx_check" value="'.$car->idx.'" id="idx"/></td>';
                                    echo '<td>
                                            <input type="hidden" id="car_image_'.$car->idx.'" name="car_image['.$car->idx.']" value="'.$image_car.'"/>
                                            <a href="#" id="upload_widget_opener" data-car-idx="'.$car->idx.'" class="upload_widget_opener"><img src="'.$image_car.'" width="50px" height="50px" id="image_'.$car->idx.'" /></a></td>';

                        			echo '<td>'.$car->car_model_year.' '.$car->car_make.' '. $car->car_model.'</td>';
                        			echo '<td><input type="hidden" id="idx" value="'.$car->idx.'" name="idx[]"/>'.$car->car_chassis_no.'</td>';
                        			echo '<td></td>';
                        			echo '</tr>';
                        			
                        		} 


                        	?>
                        </tbody>
                	 </table>
                </div>
                 <div class="right_container" style="overflow:auto; float: left;width: 50%; padding-left:5px;" >
                    
                    <div class="top_button_container">

                    <h1 class="table_title" style="display:inline; float:left;margin-top: 10px;">Email List</h1>  
                    <a  class="fancybox_adds iframe" id="fancybox_adds" href="/?c=admin&amp;m=adm_add_email_bbs"><span class="button blue">Add More Email</span></a>
                    <!-- <a class="fancybox_add" href="#select_email_customer"><span class="button blue">Add More Email</span></a> -->
                    <input type="button" id="btn_remove_email" onclick="remove_all_email()" value="Remove Email" name="btn_remove_email" class="button red"/>

                    </div>
                    <table class="table_list" cellspacing="0" style="width:100%;">

                  

                        <thead>
                      
                            <tr>

                                <th scope="col"><input type="checkbox" class="id_chk_all" value=""/>
                                </th>
                                
                                <th scope="col">Name</th>
                                
                                <th scope="col">Email</th>

                                <th scope="col">Country</th>
                                
                                
                                <th scope="col"></th>
                                
                            </tr>



                        </thead>

                        <tbody class="list_email_customer" id="list_email_customer">
                      		<?php 
                                // foreach($customer_list as $customer){
                                //  echo '<tr>';
                                //  echo '<td><input type="checkbox" class="email_check" id="email" name="email[]" value="'.$customer->customer_email.'"/></td>';
                                //  echo '<td>'.$customer->customer_name.'</td>';
                                //  echo '<td>'.$customer->customer_email.'</td>';
                                //  echo '<td></td>';
                                    
                                //  echo '</tr>';
                                    
                                // } 


                            ?>
                        </tbody>

                    </table>

                </div>
            </form>
            <div class="page_navi">
               
            </div>
            
            <div class="btn_area_center">
                <!-- <div class="btn_area_left">
                    <a class="fancybox" href="#excel_export_popup"><span class="button green">Excel Export</span></a>
                   
                </div> -->
                <div class="btn_area_right">
                    
                    
                </div>
            </div>

            
          
        </div>


    </div>


    <div style="display:none;">
        <div id="select_email_customer">
            
            <!--- Option 2 -->
            <div id="idx_table">

              <form action="" method="post">
                <div class="top_btn_container">
                  <select style="padding:5px;" id="customer_email" name="customer_email" class="customer_email">
                      <option value="">- Select Buyer -</option>
                      <?php 
                        if(count($customer_list)>0){
                            foreach($customer_list as $customer){
                            ?>
                            <option value="<?=$customer->customer_email?>" data-id-customer="<?=$customer->member_no?>" data-name-customer="<?=$customer->customer_name?>"><?=$customer->customer_email?></option>
                            <?php
                            }
                        }
                    ?>
                  </select>
                  <input type="button" id="pupup_btn_set_car" class="button orange" value="ADD"/>
                </div>
                <!-- <input type="hidden" name="idx[]" value=""/>
                 -->
                
              </form>
            </div>
            
        </div>
    </div>
<script src="//widget.cloudinary.com/global/all.js" type="text/javascript"></script>
   
<script type="text/javascript" language="javascript" src="js/jquery.ajaxQueue.min.js"></script>

<script type="text/javascript"> 
    $("a.fancybox_adds").fancybox({
            width   : 600,
            height : '100%'
    }); 
    $("a.fancybox_add").fancybox({
            width   :300
        }); 

    var count_check_idx = 0;
    function refresh_count_check(){
        if(count_check_idx>0){
            //btn set
            $("#btn_sent_email_by_idx").removeClass("white");
            $("#btn_sent_email_by_idx").addClass("blue");
            $("#btn_set_car").css("display", "");
            $("#btn_sent_email_by_idx").removeAttr("disabled");
            $("#car_check_count_tip").html(count_check_idx+" email(s) selected");

        }else{
            //btn set
            $("#btn_sent_email_by_idx").removeClass("blue");
            $("#btn_sent_email_by_idx").addClass("white");
            $("#btn_set_car").css("display", "none");
            $("#btn_set_car_dis").css("display", "");
            $("#btn_sent_email_by_idx").attr("disabled", "true");

            $("#car_check_count_tip").html('');
            $(".progress_percent").html('');
        }
    }

    function update_progress(current, total){
        $("#car_check_count_tip").html(current+' / '+total);
        var percent = Math.floor(current*100/total);
        $(".progress_percent").html('Sending Email ('+percent+'%)');
        $(".progress_step" ).css( "width",percent+'%');
        if(percent==100){
            $(".progress_percent").html('<img src=\"/images/admin/send_complete.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Sending Email Completed!</spand>');
            $(".progress_bar").css( "background",'none');
            $("#car_check_count_tip").css( "display",'none');
            $(".progress_step" ).css( "width",'0px');
        }
    }

    function process_send_email(data){
        //var count_data = data.length;
        //alert(count_data);
        var i = 0;
        var select_template = $(".input_select_template").val();

        if(select_template=="one_car_in_single_email"){

            $("#car_check_count_tip").html(i+' / '+count_check_idx);
            $(".progress_percent").html('Sending Email');
            var count_idx = $(".car_rows .idx_check").length;
            $(".car_rows .idx_check").each(function(e){
                
                var idx = $(this).val();
                var data = {
                    idx: idx
                };
                $.ajaxQueue({
                    type: "POST",
                    url: "/?c=admin&m=exec_car_email_compaign",
                    data: data,
                    dataType: 'JSON'
                }).done(function(){
                    i++;
                    update_progress(i, count_idx);

                }).fail(function() {
                    i++;
                    update_progress(i, count_idx);
                   
                });  
            });

            // for (var j = 0; j < count_check_idx; j++) {
            // //$.each( data, function( key, value ) {
            //     //var id = value.id;
            //     var data = {
            //         compaign_id: data
            //     };
            //     $.ajaxQueue({
            //         type: "POST",
            //         url: "/?c=admin&m=exec_car_email_compaign",
            //         data: data,
            //         dataType: 'JSON',
            //         async: true
            //     }).done(function(){
            //         i++;
            //         $("#car_check_count_tip").html(i+' / '+j);
            //         $(".progress_percent").html('Sending Email ('+(Math.floor(i*100/j))+'%)');
            //         $(".progress_step" ).css( "width",i*100/j+'%');
            //         if(Math.floor(i*100/j)==100){
            //         $(".progress_percent").html('<img src=\"/images/admin/send_complete.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Sending Email Completed!</spand>');
            //         $(".progress_bar").css( "background",'none');
            //         $("#car_check_count_tip").css( "display",'none');
            //         $(".progress_step" ).css( "width",'0px');
            //         }

            //     }).fail(function() {
            //         i++;
            //         $("#car_check_count_tip").html(i+' / '+j);
            //         $(".progress_percent").html('Sending Email ('+(Math.floor(i*100/j))+'%)');
            //         $(".progress_step" ).css( "width",i*100/j+'%');
            //         if(Math.floor(i*100/j)==100){
            //         $(".progress_percent").html('<img src=\"/images/admin/send_complete.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Sending Email Completed!</spand>');
            //         $(".progress_bar").css( "background",'none');
            //         $("#car_check_count_tip").css( "display",'none');
            //         $(".progress_step" ).css( "width",'0px');
            //         }
                   
            //     });   
            // //});
            // }
        }
        else{
            //for (var j = 0; j < count_check_idx; j++) {

                $("#car_check_count_tip").html(i+' / '+count_check_idx);
                $(".progress_percent").html('Sending Email');
                
                var data = {
                    compaign_id: data
                };

                $.ajaxQueue({
                    type: "POST",
                    url: "/?c=admin&m=exec_multiple_car_email_compaign",
                    data: data,
                    dataType: 'JSON',
                    async: true
                }).done(function(){
                    i++;
                    $("#car_check_count_tip").html(i+' / '+i);
                    $(".progress_percent").html('Sending Email ('+(Math.floor(i*100/i))+'%)');
                    $(".progress_step" ).css( "width",i*100/i+'%');
                    if(Math.floor(i*100/i)==100){
                    $(".progress_percent").html('<img src=\"/images/admin/send_complete.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Sending Email Completed!</spand>');
                    $(".progress_bar").css( "background",'none');
                    $("#car_check_count_tip").css( "display",'none');
                    $(".progress_step" ).css( "width",'0px');
                    }
                   
                }).fail(function() {
                    i++;
                    $("#car_check_count_tip").html(i+' / '+i);
                    $(".progress_percent").html('Sending Email ('+(Math.floor(i*100/i))+'%)');
                    $(".progress_step" ).css( "width",i*100/i+'%');
                    if(Math.floor(i*100/i)==100){
                    $(".progress_percent").html('<img src=\"/images/admin/send_complete.png\" style=\"margin-top: -11px;height:26px;\"><br><spand style="font-size: 16px;color:#6a6969;">Sending Email Completed!</spand>');
                    $(".progress_bar").css( "background",'none');
                    $("#car_check_count_tip").css( "display",'none');
                    $(".progress_step" ).css( "width",'0px');
                    }
                   
                });   
            //}
        }
    }
    $(document).ready(function(e){

        refresh_count_check();
        
        $('#pupup_btn_set_car').click(function(e){
            var email_checks = new Array();
            var email_check = $("input:checkbox:not(:checked)");
            var customer_email = $("#customer_email").val();
            var customer_name = $(".customer_email option:selected").attr("data-name-customer");
            var customer_id = $(".customer_email option:selected").attr("data-id-customer");

            $("input:checkbox:not(:checked)").each(function() {
                email_checks = this.value;
            });
               if(email_checks != customer_email){
                    $('.list_email_customer').append('<tr class="email_data_'+customer_id+'">'
                    +'<td><input type="checkbox" class="email_check" id="email" name="email[]" data-id-customer="'+customer_id+'" value="'+customer_email+'"/></td>'
                    +'<td>'+customer_name+'</td>'
                    +'<td>'+customer_email+'</td>'
                    +'<td></td>'
                    +'</tr>');
               }else{
                    alert('This buyer has add already!');
               } 
        });

        $("#btn_sent_email_by_idx").click(function(e){
            var post_data = $("#adm_form_car").serialize();
            $(this).css("display", "none");
            $(".label_title").css("display", "none");
            $(".input_select_template").css("display", "none");
            $(".left_container").css("display", "none");
            $(".right_container").css("display", "none");
            $.ajax({
                type: "POST",
                url: "/?c=admin&m=send_recommend_car_exec",
                data: post_data,
                dataType: 'JSON',
                async: true,
                success: function(data){
                    if(data !=""){
                        $(".progress_wrapper").css("display", "");
                        $(".msg_wrapper").css("display", "none");
                        process_send_email(data);
                    }else{
                        $("#car_check_count_tip").html('');
                        $(".msg_wrapper").css("display", "");
                        $(".msg_wrapper").css("text-align", "center");
                        $(".msg_wrapper").html('<spand style="font-size: 16px;color:#6a6969;">Sorry email could not be sent dued to invalid recipient email address(es)</spand>');
                        $(".msg_wrapper").css("background-color", "#FFE0E0");
                    }
                    
                }
            });   
            
        });
        /*** When check checkbox car pressed ***/
        $(".idx_chk_all").change(function(e){
            $('.idx_check').prop('checked', $(this).prop('checked'));
        });
         /*** When button check all pressed ***/
        $(".id_chk_all").change(function(e){
            $('.email_check').prop('checked', $(this).prop('checked'));
            count_check_idx = $('.email_check:checked').length;
            refresh_count_check();
        });

        /*** When user click one bye one check box ***/
        $(document).on("click",".email_check",function() {
            count_check_idx = $('.email_check:checked').length;
            refresh_count_check();
        });

        /*** When user click button send email ***/
        $("#btn_sent_email_by_idx").click(function(e){
            var f  = document.adm_form_car;

            if(f.email != null) {
                for(i=0; i<f.email.length; i++) {
                    if(f.email[i].checked) {
                        return true;
                    }
                }
            }
            if(count_check_idx<=0){
                $("#car_check_count_tip").html("Please select some emails first!");
                return false;
            }else{
                return true;
            }
        });
        $(".upload_widget_opener").click( function(e) {
            var car_idx = $(this).attr('data-car-idx');
            cloudinary.openUploadWidget({ cloud_name: 'softbloom', upload_preset: 'juefxlp7', show_powered_by: false, max_files: 1, max_image_width: 1024, max_image_height: 768}, 
              function(error, result) {
                var url = result[0].url;
    
                $("#car_image_"+car_idx).val(url.slice(0,-4));
                $("#image_"+car_idx).attr("src",url.slice(0,-4));
                // var url        = result[0].url;
                // var public_id         = result[0].public_id;        
                // var data = {
                //       public_id  : public_id,
                //       url        :       url,
                    // }
                    // $.ajax({
                    //   type: "POST",
                    //   url: "/?c=admin&m=adm_cloud_upload_thumbnail",
                    //   data: data,
                    //   async: true,
                    //   success: function(data){
                    //     $.each(JSON.parse(data), function(idx, obj) {
                    //         $('#upload_widget_opener').append('<img src="'+obj+'" width="50px" />'
                    //             +'<input type="hidden" value='+data+' id="car_image" />');
                    //     });   
                    //    }
                    // });

              });
          });
    });

    
    function remove_all() {
        var f = document.adm_form_car;
        var fg = false;
        var idxs = new Array();

        if (f.idx != null) {
            for (i = 0; i < f.idx.length; i++) {
                if (f.idx[i].checked) {
                    fg = true;
                    idxs[i] =f.idx[i].value;
                }
            }

            if (!fg) {
                alert('Please select cars that you want to remove.');
                return;
            }

            if (!confirm('Do you want to remove this car?'))
            {
                return;

            }else{
                for(var i = 0; i<idxs.length; i++) {
                    $('.car_data_'+idxs[i]).remove();

                }                
            }

        }
    }
   
   function remove_all_email() {
        var f = document.adm_form_car;
        var fg = false;
        var emails = new Array();

        if (f.email != null) {
            for (i = 0; i < f.email.length; i++) {
                if (f.email[i].checked) {
                    fg = true;
                    emails[i] = $(f.email[i]).attr('data-id-customer');
                }
            }

            if (!fg) {
                alert('Please select email that you want to remove.');
                return;
            }

            if (!confirm('Do you want to remove this email?'))
            {
                return;

            }else{
                for(var i = 0; i<emails.length; i++) {
                    $('.email_data_'+emails[i]).remove();
                }                
            }

        }
    }
    function append_data_email(data){
       $('.list_email_customer').append(data);
    }

</script>



       

