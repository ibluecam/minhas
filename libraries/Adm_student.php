<?php


if (!defined('BASEPATH'))

	exit('No direct script access allowed');



/* ------------------------------------------------

 * 파일명 : Adm_customer.php

 * 개  요 : 회원관리

  ------------------------------------------------ */



class Adm_student {



	var $ci;

	var $mcd;

	var $bbs_config;




	function __construct() {


		$this->ci = &get_instance();


	}
	

	function adm_student_list()
	{
		$where = array(); //sql where 조건

		$sParam = '';	  //검색 string

//검색 조건

		$sch_condition = $this->ci->input->get_post('sch_condition');   //검색조건

		$sch_word = $this->ci->input->get_post('sch_word');		//검색어

//검색조건 + 검색어

		if ($sch_condition != '' && $sch_word != '') {

			$sch_condition_tmp = explode('|', $sch_condition);



//다중 조건인 경우(제목 + 내용)

			if (count($sch_condition_tmp) > 1) {

				$tmpStr = '';

				for ($i = 0; $i < count($sch_condition_tmp); $i++) {

					if ($tmpStr != '') {

						$tmpStr .= ' or ';

					}



					$tmpStr .= $sch_condition_tmp[$i] . ' ? ';

					$where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';

				}



				$tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

				$where[$tmpStr] = '';



				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			} else {

				$where[$sch_condition] = '%' . $sch_word . '%';

				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			}

		}



//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 10; //페이지 로우수

		$page_views = 10; //페이지 네비게이션 수

//팝업 목록 조회

		$bbs_list = $this->ci->student->select_student(($cur_page - 1) * $page_rows, $page_rows, $where);


		$total_rows = $this->ci->student->total_select_student($where); //조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	//글 번호

//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);





		func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 //글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string

		func_set_data($this->ci, 'student_list', $bbs_list->result());

		func_set_data($this->ci, 'page_info', $page_info);		
		
	    func_set_data($this->ci, 'admin_contents', 
		$this->ci->load->view('admin/adm/student/adm_student_list', $this->ci->data, true));
		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}
	
	
	function adm_add_student(){
		
		func_set_data($this->ci, 'admin_contents', 
		$this->ci->load->view('admin/adm/student/adm_add_student', $this->ci->data, true));
		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}
	
	
	function adm_student_write_exec() {

		$inData = array();

		$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴

		//DB 등록

		$idx = $this->ci->student->student_insert($inData);

		//첨부파일 등록


		echo func_jsAlertReplace('Save is Successfully.', '/?c=admin&m=adm_student_list');

	}

	
	
	function student_delete_adm() {

		$id = $this->ci->input->get_post('id');

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	  //검색 string

//DB 삭제

		$idx = $this->ci->student->student_delete_adm($id);



		echo func_jsAlertReplace('Delete Success.', '/?c=admin&m=adm_student_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}
	
	
	function adm_student_all_delete() {

		$customer_no = '';

		$customer_nos = $this->ci->input->get_post('idx'); //배열

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	//검색 string



//선택 게시물 배열

		foreach ($customer_nos as $key => $value) {

			$customer_no = $value; //회원 번호



			if ($customer_no != '') {

				$this->ci->student->student_delete_adm($customer_no);

			}

		}



		echo func_jsAlertReplace('Delete Success.', '/?c=admin&m=adm_student_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}

   function adm_student_modify() {

		$idx = $this->ci->input->get_post('idx');

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	  		//검색 string

		$only_attach = $this->ci->input->get_post('only_attach'); 		//첨부파일 상세 삭제 여부

		$only_survey = $this->ci->input->get_post('only_survey'); //문항 상세 삭제 여부
		

		$customer_detail_list = $this->ci->student->student_select_detail($idx);	//var_dump($customer_detail_list); exit();

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/student/adm_student_modify', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}
	
	
	function adm_student_modify_exec() {

		$inData = array();


		$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴


		$inData['del_flg'] = 'True';						//del_flg


		if (! isset($inData['join_dt'])||($inData['join_dt'] == '')) {						//작성일

			$inData['join_dt'] = func_get_date_time();

		}


		//DB 등록

		$idx = $this->ci->student->student_update($inData);


		//첨부파일 등록


		echo func_jsAlertReplace('update success.', '/?c=admin&m=adm_student_list');

	}


}

?>