<?php
//error_reporting(0);
class ScrapBroomjapan{
	public static $domain = 'http://www.broomjapan.com';
	public static $url = 'http://www.broomjapan.com/stock/search.aspx';
	public static $car_detail_url = 'http://www.broomjapan.com/stock/detail.aspx?st=';
	public static function getChassisById($id){
		include_once('scrapwebdata/simple_html_dom.php');
		
		$num_page = 0;
		$html = file_get_html(self::$car_detail_url.$id);

		$tbody_html = $html->find("table[id=cph_Main_Content_dtv_Stock_Detail] table tbody");
		$chassis_no = $tbody_html[0]->children[1]->children[1]->nodes[0]->innertext;
		$html->clear();
		return $chassis_no;
	}
	/*** GET NUMBER OF PAGE ***/
	public static function getNumberOfPage(){
		include_once('scrapwebdata/simple_html_dom.php');
		
		$num_page = 0;
		$html = file_get_html(self::$url);

		$pager_arr_a = $html->find("div[class=pager] a");
		if(empty($pager_arr_a)){
			return 1;
		}
		$last_key = max(array_keys($pager_arr_a));
		$num_page = intval($pager_arr_a[$last_key]->innertext);
		$html->clear();
		return $num_page;
	}

	/*** GET HTML OF SEARCH RESULT OF THE PAGE NUMBER AFTER EXECUTE ***/
	public static function getSearchResult($pageNumber){
		/**POST BY PHP***/

		$data = ScrapBroomjapan::getSearchInputs($pageNumber);
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($data),
		    ),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents(self::$url, false, $context);

		/**END POST BY PHP***/
		return $result;
	}

	/*** GET ALL ITEM IDS AS ARRAY FROM A PAGE NUMBER ***/
	public static function getAllItemsFromHtml($pageNumber){
		include_once('scrapwebdata/simple_html_dom.php');
		$items = array();
		$html = ScrapBroomjapan::getSearchResult($pageNumber);
		
		$result_parse = str_get_html($html);
		
		//$arr_img = $result_parse->find('table[class=lsv_result] tbody td[class=start] img[class=image]');
		$arr_tr = $result_parse->find('table[class=lsv_result] tbody tr');
		
		foreach($arr_tr as $tr){

			$img = $tr->find('td[class=start] img[class=image]');
			
			//var_dump($status);
			if(count($img)>0){
				/*** GET THE PREVIOUS PRICE ***/
				$old_price_text = null;
				$price_strikes = $tr->children(9)->find('span strike');
				if($price_strikes!=null){
					$old_price_text = $price_strikes[0]->nodes[0]->innertext;
				}

				if($old_price_text!=null){
					$items['old_price'][]= preg_replace("/[^0-9]/","", $old_price_text);
				}else{
					$items['old_price'][]= 0;
				}
				/*** END GET THE PREVIOUS PRICE ***/
				$explode_src = explode('/', $img[0]->src);
				$id = $explode_src[4];
				$items['id'][] = $id;
				/*** GET STATUS ***/
				$status_td = $tr->find('td', 9);
				if(count($status_td)>0){
					$img_status = $status_td->find('img');
					if(count($img_status)>0){
						$items['status'][] = $img_status[0]->alt;
					}else{
						$items['status'][] = 'Sale';
					}
				}else{
					$items['status'][] = 'Sale';
				}
			}
		}
		//var_dump($items);
		// foreach($arr_img as $img){
		// 	$explode_src = explode('/', $img->src);
		// 	$id = $explode_src[4];
		// 	$item_ids[] = $id;
		// 	// if($a->href!=NULL){
		// 	// 	$item_ids[] = ScrapBroomjapan::getValue($a->href, 'st');
		// 	// }
		// }
		//var_dump($item_ids);
		$result_parse->clear();
		return $items;
	}

	/*** GET ALL INPUT IN THE SEARCH FORM AS ARRAY ***/
	public static function getSearchInputs($pageNumber){
		include_once('scrapwebdata/simple_html_dom.php');

		$inputs = array();
		if($pageNumber>0){
			$true_page = $pageNumber - 1;
		}else{
			$true_page = 0;
		}
		$inputs['__EVENTTARGET'] = "ctl00\$cph_Main_Content\$lsv_Search_Result\$dpg_Result_Top\$ctl00\$ctl0".$true_page;
		$inputs['__EVENTARGUMENT'] = "";
		$html = file_get_html(self::$url);
		$form_ele=$html->find('form[id=frm_Main]'); 
		$inputs_ele = $form_ele[0]->children;
		foreach($inputs_ele as $ele){
		    $inputs[$ele->name] = $ele->value ;
		}
		$html->clear();
		return $inputs;
	}	

	/*** GET PARAMETER GET FROM URL ***/
	public static function getValue($url, $getname){
	  $query = parse_url($url, PHP_URL_QUERY);
	  $vars = array();
	  parse_str($query, $vars);
	  return $vars[$getname];
	}
	public static function getAllItemsFromPage($page_num){
		/*** EXECUTE ***/
		$items['id'] = array();
		$items['status'] = array();
		$items['old_price'] = array();
		//$num_page = ScrapBroomjapan::getNumberOfPage();
		//for($i=1;$i<=$num_page;$i++){
			$arr_items = ScrapBroomjapan::getAllItemsFromHtml($page_num);
			
			foreach($arr_items['status'] as $key=>$value){
				if($value=='Sold Out'||$value=='Under Offer'){
					$arr_items['status'][$key] = "soldout";
				}else{
					$arr_items['status'][$key] = "sale";
				}
			}
			$items['id'] = array_merge($items['id'], $arr_items['id']);
			$items['status'] = array_merge($items['status'], $arr_items['status']);
			$items['old_price']= array_merge($items['old_price'], $arr_items['old_price']);
		//}
		
		return $items;
	}
	public static function getAllAvailableItems($page_num){
		/*** EXECUTE ***/
		$items['id'] = array();
		$items['status'] = array();
		$items['old_price'] = array();
		//$num_page = ScrapBroomjapan::getNumberOfPage();
		//for($i=1;$i<=$num_page;$i++){
			$arr_items = ScrapBroomjapan::getAllItemsFromHtml($page_num);
			
			foreach($arr_items['status'] as $key=>$value){
				if($value=='Sold Out'||$value=='Under Offer'){
					unset($arr_items['status'][$key]);
					unset($arr_items['id'][$key]);
					unset($arr_items['old_price'][$key]);
				}else{
					$arr_items['status'][$key] = "sale";
				}
			}
			$items['id'] = array_merge($items['id'], $arr_items['id']);
			$items['status'] = array_merge($items['status'], $arr_items['status']);
			$items['old_price']= array_merge($items['old_price'], $arr_items['old_price']);
		//}
		
		return $items;
	}
	public static function getSoldoutItems($page_num){
		/*** EXECUTE ***/
		$items['id'] = array();
		$items['status'] = array();
		$items['old_price'] = array();
		//$num_page = ScrapBroomjapan::getNumberOfPage();
		//for($i=1;$i<=$num_page;$i++){
			$arr_items = ScrapBroomjapan::getAllItemsFromHtml($page_num);
			
			foreach($arr_items['status'] as $key=>$value){
				if($value!='Sold Out'&&$value!='Under Offer'){
					unset($arr_items['status'][$key]);
					unset($arr_items['id'][$key]);
					unset($arr_items['old_price'][$key]);
				}else{
					$arr_items['status'][$key] = 'soldout';
				}
			}
			$items['id'] = array_merge($items['id'], $arr_items['id']);
			$items['status'] = array_merge($items['status'], $arr_items['status']);
			$items['old_price']= array_merge($items['old_price'], $arr_items['old_price']);
		//}
		
		return $items;
	}

	public static function getItemDetailById($id){
		include_once('scrapwebdata/simple_html_dom.php');
		$item_detail = array();
		$num_page = 0;
		$html = file_get_html(self::$car_detail_url.$id);

		$tbody_html = $html->find("table[id=cph_Main_Content_dtv_Stock_Detail] table tbody");
		$price_span = $html->find("#cph_Main_Content_lbl_Fob_Price");
		$image_inputs = $html->find("#cph_Main_Content_dlt_Photo_List td input[class=thumbnail]");
		/*** ROW 2 ***/
		///////////////////////////////////////// tbody -> tr        -> td        -> (self) -> html
		$item_detail['chassis_no'] 		= $tbody_html[0]->children[1]->children[1]->nodes[0]->innertext;
		$item_detail['product_type'] 	= $tbody_html[0]->children[1]->children[3]->nodes[0]->innertext;	
		/*** ROW 3 ***/
		$item_detail['make'] 			= $tbody_html[0]->children[2]->children[1]->children[0]->nodes[0]->innertext;
		$item_detail['exterior_color'] 	= $tbody_html[0]->children[2]->children[3]->nodes[0]->innertext;
		/*** ROW 4 ***/
		$item_detail['model'] 			= $tbody_html[0]->children[3]->children[1]->children[0]->nodes[0]->innertext;
		$item_detail['steering'] 		= $tbody_html[0]->children[3]->children[3]->nodes[0]->innertext;	
		/*** ROW 5 ***/
		$item_detail['drive'] 			= $tbody_html[0]->children[4]->children[3]->nodes[0]->innertext;	
		/*** ROW 6 ***/
		$item_detail['transmission'] 	= $tbody_html[0]->children[5]->children[3]->nodes[0]->innertext;
		/*** ROW 7 ***/
		$item_detail['engine_volume']	= $tbody_html[0]->children[6]->children[1]->nodes[0]->innertext;
		$item_detail['door']			= $tbody_html[0]->children[6]->children[3]->nodes[0]->innertext;
		/*** ROW 8 ***/
		$item_detail['fuel_type']		= $tbody_html[0]->children[7]->children[1]->nodes[0]->innertext;
		$item_detail['number_passenger']= $tbody_html[0]->children[7]->children[3]->nodes[0]->innertext;
		/*** ROW 9 ***/
		$item_detail['registrationyear']= $tbody_html[0]->children[8]->children[1]->children[0]->nodes[0]->innertext;
		if(isset($tbody_html[0]->children[8]->children[1]->children[1]->nodes[0]->innertext))
			$item_detail['registrationmonth']=$tbody_html[0]->children[8]->children[1]->children[1]->nodes[0]->innertext;

		$dimension						= $tbody_html[0]->children[8]->children[3]->nodes[0]->innertext; //col2
		/*** ROW 10 ***/
		$manu_detail					= $tbody_html[0]->children[9]->children[1]->children[0]->nodes[0]->innertext;
		/*** ROW 11 ***/
		$item_detail['mileage']			= $tbody_html[0]->children[10]->children[1]->children[0]->nodes[0]->innertext;
		$item_detail['mileage_type']	= $item_detail['mileage'];	

		$item_detail['price']			= $price_span[0]->nodes[0]->innertext;
		$item_detail['currency']		= $item_detail['price'];

		$item_detail['icon_new_yn'] = 'N';
		$item_detail['images'] = array();
		foreach($image_inputs as $input){
			$item_detail['images'][] = self::$domain.'/'.$input->src;
		}
		/*** FILTER DATA ***/
		$item_detail['product_type'] = ScrapBroomjapan::getCorrectProductType($item_detail['product_type']);
		$manu_detail_arr = explode('/', $manu_detail);
		$item_detail['manu_year'] = trim($manu_detail_arr[0]);
		$item_detail['manu_month'] = trim($manu_detail_arr[1]);
		//$item_detail['product_type'] = ucfirst(strtolower($item_detail['product_type']));
		if($item_detail['steering']=='Right'){
			$item_detail['steering'] = 'RHD';
		}elseif($item_detail['steering']=='Left'){
			$item_detail['steering'] = 'LHD';
		}else{
			$item_detail['steering'] = 'Undefined';
		}
		$item_detail['engine_volume'] = preg_replace("/[^0-9]/","", $item_detail['engine_volume']);
		$item_detail['mileage'] = preg_replace("/[^0-9]/","", $item_detail['mileage']);
		$item_detail['mileage_type'] = ucfirst(preg_replace("/[^a-zA-Z]/","", $item_detail['mileage_type']));
		$item_detail['price']			= preg_replace("/[^0-9]/","", $item_detail['price']);
		$item_detail['currency'] = substr($item_detail['currency'], 0, 1);
		if($item_detail['currency']=='$'){
			$item_detail['currency'] = 'USD';	
		}
		foreach($item_detail as $key=>$value){
			if($value==NULL){
				$item_detail[$key]='';
			}
		}
		/***GET DIMENSION***/
		$exp_dimension = explode('×', $dimension);
		if(count($exp_dimension)==3){
			$item_detail['car_length'] = preg_replace("/[^0-9]/","",$exp_dimension[0]);
			$item_detail['car_width'] = preg_replace("/[^0-9]/","",$exp_dimension[1]);
			$item_detail['car_height'] = preg_replace("/[^0-9]/","",$exp_dimension[2]);
		}else{
			$item_detail['car_length'] = '';
			$item_detail['car_width'] = '';
			$item_detail['car_height'] = '';
		}
		$html->clear();
		//var_dump($item_detail);
		return $item_detail;
	}
	public static function getCorrectProductType($product_type){
		$product_arr = array(
					'Sedan'=>'sedans',
					'Hatchback'=>'hatchback',
					'Coupe'=>'coupe',
					'Convertible'=>'convertible',
					'StationWagon'=>'wagon',
					'Minivan'=>'minivans',
					'SUV'=>'suv',
					'PickupTruck'=>'pickup',
					'Truck'=>'trucks'
			);
		$product_type = strtolower($product_type);
		foreach($product_arr as $key=>$value){
			if($product_type == strtolower($key)){
				return $value;
			}
		}
		return $product_type;
		//str_ireplace($product_types_from, $product_types_to, $product_type);
	}
}
