<?php


if (!defined('BASEPATH'))

	exit('No direct script access allowed');



/* ------------------------------------------------

 * 파일명 : Adm_account.php

 * 개  요 : 회원관리

  ------------------------------------------------ */



class Adm_account {

	var $ci;
	var $mcd;
	var $bbs_config;
	var $member_join_config;


//constructor
	function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();

//전체 사이트 맵
		$sitemap = $this->ci->data['site_map_list'];
		if (count($sitemap) > 0) {
			foreach ($sitemap as $rows) {
//회원관련 모듈의 메뉴코드_회원가입
				if (strstr($rows->menu_type, 'member_join')) {
					func_set_data($this->ci, 'member_join_mcd', $rows->menu_code);
				}
//회원관련 모듈의 메뉴코드_로그인
				if (strstr($rows->menu_type, 'member_join')) {
					func_set_data($this->ci, 'member_login_mcd', $rows->menu_code);
				}
			}
		}
	}


//회원조회

	function adm_account_list($prev_next = 'N') {

		$grade_no_session = $this->ci->grade_no;
		$where = array(); 		//sql where 조건
		$mcd = $this->ci->input->get_post("mcd");
		$sParam = '';	  		//검색 string
		
		//날짜 검색
		$sch_account_date_s = $this->ci->input->get_post('sch_account_date_s'); 		//등록일 시작
		$sch_account_type= $this->ci->input->get_post('sch_account_type'); 
		if ($sch_account_date_s != '') {

			$sParam .= '&sch_account_date_s=' . $sch_account_date_s;

			$where['account_date >='] = $sch_account_date_s . " 00:00:00";

		}

		$sch_account_date_e = $this->ci->input->get_post('sch_account_date_e'); 		//등록일 종료

		if ($sch_account_date_e != '') {

			$sParam .= '&sch_account_date_e=' . $sch_account_date_e;

			$where['account_date <='] = $sch_account_date_e . " 23:59:59";

		}

		if($sch_account_type!=""){
			$where['account_type ='] = $sch_account_type;
		}

		$sch_condition = $this->ci->input->get_post('sch_condition');   //검색조건

		$sch_word = $this->ci->input->get_post('sch_word');		//검색어

//검색조건 + 검색어

		if ($sch_condition != '' && $sch_word != '') {

			$sch_condition_tmp = explode('|', $sch_condition);



//다중 조건인 경우(제목 + 내용)

			if (count($sch_condition_tmp) > 1) {

				$tmpStr = '';

				for ($i = 0; $i < count($sch_condition_tmp); $i++) {

					if ($tmpStr != '') {

						$tmpStr .= ' or ';

					}



					$tmpStr .= $sch_condition_tmp[$i] . ' ? ';

					$where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';

				}



				$tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

				$where[$tmpStr] = '';



				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			} else {

				$where[$sch_condition." LIKE"] = '%' . $sch_word . '%';

				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			}

		}



//페이지 조건
		$cur_page = $this->ci->input->get_post("cur_page");
		if ($cur_page == NULL || $cur_page == '')
			$cur_page = 1;
		$page_rows = 20; //페이지 로우수 20
		$page_views = 10; //페이지 네비게이션 수 10
		$account_list = $this->ci->account->account_select(($cur_page - 1) * $page_rows, $page_rows, $where);

		$result_total_rows =$this->ci->account->account_total_row($where);
		$total_rows = $result_total_rows['0']->num_rows; 
		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   //글 번호 
//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);

		func_set_data($this->ci, 'mcd', $mcd);   //조회 전체 목록수
		func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수
		func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지
		func_set_data($this->ci, 'row_cnt', $row_cnt);		 //글번호
		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지
		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string
		func_set_data($this->ci, 'account_list', $account_list);
		func_set_data($this->ci, 'page_info', $page_info);

		//func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/account/' . $this->ci->data['customer_join_mcd'] . '/adm_account_list', $this->ci->data, true));
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/account/adm_account_list', $this->ci->data, true));

		//var_dump($this->ci->data);

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}



//회원 상세 조회

	function adm_account_detail() {

		$id = $this->ci->input->get_post('id');
		/* Start Set account type list */
		$where_expense=array();
		$where_income=array();
		$where_expense['account_type=']="Expense";
		$where_income['account_type=']="Income";

		$account_type_expense_list = $this->ci->account_type->account_type_list($where_expense);
		$account_type_income_list = $this->ci->account_type->account_type_list($where_income);
		$customer_list = $this->ci->customer->customer_select_undeleted();

		func_set_data($this->ci, 'account_type_expense_list', $account_type_expense_list->result());	
		func_set_data($this->ci, 'account_type_income_list', $account_type_income_list->result());
		func_set_data($this->ci, 'customer_list', $customer_list->result());
		/* End Set account type list */	

		$cur_page = $this->ci->input->get_post('cur_page');

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$sParam = $this->ci->input->get_post('sParam');		//검색 string



		$account_detail_list = $this->ci->account->account_select_detail_adm($id); //상세 조회

		// $customer_grade_list = $this->ci->customer->customer_grade_select();				//회원등급 조회



		func_set_data($this->ci, 'id', $id);

		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string

		func_set_data($this->ci, 'account_detail_list', $account_detail_list);

		//func_set_data($this->ci, 'customer_grade_list', $customer_grade_list->result());



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/account/adm_account_detail', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}

	function adm_account_update() {

		$id = $this->ci->input->get_post('id');
		/* Start Set account type list */
		$where_expense=array();
		$where_income=array();
		$where_expense['account_type=']="Expense";
		$where_income['account_type=']="Income";

		$account_type_expense_list = $this->ci->account_type->account_type_list($where_expense);
		$account_type_income_list = $this->ci->account_type->account_type_list($where_income);
		$customer_list = $this->ci->customer->customer_select_undeleted();

		func_set_data($this->ci, 'account_type_expense_list', $account_type_expense_list->result());	
		func_set_data($this->ci, 'account_type_income_list', $account_type_income_list->result());
		func_set_data($this->ci, 'customer_list', $customer_list->result());
		/* End Set account type list */	

		$cur_page = $this->ci->input->get_post('cur_page');

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$sParam = $this->ci->input->get_post('sParam');		//검색 string



		$account_detail_list = $this->ci->account->account_select_detail_adm($id); //상세 조회

		// $customer_grade_list = $this->ci->customer->customer_grade_select();				//회원등급 조회



		func_set_data($this->ci, 'id', $id);

		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string

		func_set_data($this->ci, 'account_detail_list', $account_detail_list);

		//func_set_data($this->ci, 'customer_grade_list', $customer_grade_list->result());



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/account/adm_account_update', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}

	//customer 등록 HTML

	function adm_account_write() {
		/* Start Set account type list */
		$where_expense=array();
		$where_income=array();
		$where_expense['account_type=']="Expense";
		$where_income['account_type=']="Income";
		$account_type_expense_list = $this->ci->account_type->account_type_list($where_expense);
		$account_type_income_list = $this->ci->account_type->account_type_list($where_income);
		$customer_list = $this->ci->customer->customer_select_undeleted();

		func_set_data($this->ci, 'account_type_expense_list', $account_type_expense_list->result());	
		func_set_data($this->ci, 'account_type_income_list', $account_type_income_list->result());
		func_set_data($this->ci, 'customer_list', $customer_list->result());
		/* End Set account type list */	
		$cur_page = $this->ci->input->get_post('cur_page');

		if ($cur_page == NULL || $cur_page == '')
			$cur_page = 1;
		func_set_data($this->ci, 'cur_page', $cur_page);
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/account/adm_account_write', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}
	
	
	//게시판 등록 처리

	function adm_account_rate() {
		
		$where = array();
		
		$mcd = $this->ci->input->get_post('mcd');
		
		//$where['member_no ='] = $this->ci->member_no;
		
		$account_rate_list =  $this->ci->account->select_account_rate($where);
		
		func_set_data($this->ci, 'account_rate_list', $account_rate_list->result());
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		$this->ci->load->view('admin/adm/account/adm_account_rate', $this->ci->data, false);
		
	}
	
	
	//게시판 등록 처리

	function adm_account_rate_exec() {
		
		$mcd = $this->ci->input->get_post('mcd');
		
		$id_key =0;
		
		$inData = array();
		
		$where = array();
		
		$keys = array('from_currency','to_currency','exchange_dt','member_no');

        $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴
		
		$inData['member_no'] = $this->ci->member_no;       //아이디

		for($i=0;$i< sizeof($keys);$i++){
			
			//echo ($inData[$keys[$i]]);
			
			$where[$keys[$i]." ="] = $inData[$keys[$i]];
			
		}
        //var_dump($where);
		
		
		if (!isset($inData['created_dt']) || ($inData['created_dt'] == '')) {         //작성일
            $inData['created_dt'] = func_get_date_time();
        }
		//var_dump($inData);
		
		$total_row =  $this->ci->account->total_select_account_rate($where);
		
		$account_rate_list =  $this->ci->account->select_account_rate($where)->result();
		
		if($total_row > 0){
			foreach($account_rate_list as $row){
				$inData['id']= $row->id;	
			}
			if (!isset($inData['modified_dt']) || ($inData['modified_dt'] == '')) {         //작성일
            	$inData['modified_dt'] = func_get_date_time();
       		}
			$this->ci->account->update_currency($inData);
			//var_dump($inData);
		}else{
			$this->ci->account->insert_currency($inData);	
		}
		
		/*$account_rate_list =  $this->ci->account->select_account_rate($where)->result();
		
		func_set_data($this->ci, 'account_rate_list', $account_rate_list);*/
		
		echo func_jsAlertReplace('Exchange Rate has beed added.', '?c=admin&m=adm_account_rate&mcd=' . $mcd);
	}

	//게시판 등록 처리
	function adm_account_rate_modify(){
		
		$id_rat = $this->ci->input->get_post('id');
		
		$mcd = $this->ci->input->get_post('mcd');
		
		$where = array();
		
		$where['member_no ='] = $this->ci->member_no;
		
		if(is_array($id_rat)){
			
			$id_rat=$id_rat[0];	
		}else{ 
			echo $id_rat;
			
		}
		
		//var_dump($id_rat);
		
		$where['id ='] = $id_rat;
		
		$account_rate_list =  $this->ci->account->select_account_rate($where);
		
		func_set_data($this->ci, 'account_rate_list', $account_rate_list->result());
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		$this->ci->load->view('admin/adm/account/adm_account_rate_modify', $this->ci->data, false);
		
	}
	
	//게시판 등록 처리
	function adm_account_rate_modify_exec(){
		
		$id_rat = $this->ci->input->get_post('id');
		
		$mcd = $this->ci->input->get_post('mcd');
		
		$where = array();
		
		$where['member_no ='] = $this->ci->member_no;
		
		$inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴
		
		$inData['id']=$id_rat;
		
		if (!isset($inData['modified_dt']) || ($inData['modified_dt'] == '')) {         //작성일
            	$inData['modified_dt'] = func_get_date_time();
		}
		
		$this->ci->account->update_currency($inData);
		
		//var_dump($id_rat);
		
		$account_rate_list =  $this->ci->account->select_account_rate($where);
		
		//func_set_data($this->ci, 'account_rate_list', $account_rate_list->result());
		
		//func_set_data($this->ci, 'mcd',$mcd);
		
		//$this->ci->load->view('admin/adm/account/adm_account_rate_modify', $this->ci->data, false);
		echo func_jsAlertReplace('Exchange Rate has beed modified.', '?c=admin&m=adm_account_rate&mcd=' . $mcd);
		
	}
	
	//게시판 등록 처리
	function adm_account_rate_delete(){
		
		
	}
	
	//게시판 등록 처리
	function adm_account_rate_delete_exec(){
		
		$id_rat = $this->ci->input->get_post('id');
		
		$mcd = $this->ci->input->get_post('mcd');
		
		for($i=0;$i <count($id_rat);$i++){
			
			$result =$this->ci->account->account_delete_rate($id_rat[$i]);
			
		}
		if($result == true){
			echo func_jsAlertReplace('Exchange Rate beed deleted.', '?c=admin&m=adm_account_rate&mcd=' . $mcd);
			
		}
		
	}

	//게시판 등록 처리

	function adm_account_write_exec() {

		$inData = array();

		$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴

		//$inData['menu_code'] = $this->mcd;					 		//메뉴코드

		//$inData['member_id'] = $this->ci->member_id;		   		//아이디

		//$inData['writer_ip'] = $this->ci->input->ip_address(); 		//작성자 아이피

		// $inData['visit_cnt'] = 0;							  		//조회수

		// $inData['grp_idx'] = 0;							  		//그룹 idx

		// $inData['order_no'] = 0;							  		//정렬 순서

		// $inData['depth'] = 0;							  		//깊이



		// 설문조사 사용할 경우

		// if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

		// 	// 시행일

		// 	$inData['publish_start'] = $inData['publish_end'].' 00:00:00';

		// 	$inData['publish_end'] = $inData['publish_end'].' 23:59:59';

		// }



		//우편번호

		// if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

		// 	$inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

		// 	unset($inData['zipcode1']);

		// 	unset($inData['zipcode2']);

		// }

		// if (!isset($inData['notice_yn'])) {								//공지여부

		// 	$inData['notice_yn'] = 'N';

		// }

		if (! isset($inData['created_dt'])||($inData['created_dt'] == '')) {						 		//작성일

			$inData['created_dt'] = func_get_date_time();
			$inData['updated_dt'] = func_get_date_time();
		}



		//DB 등록

		$idx = $this->ci->account->insert($inData);



		// 설문조사 사용할 경우

		// if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

		// 	$this->ci->load->model('mo_survey', 'survey'); // 설문조사

		// 	// 설문조사 항목 저장

		// 	$Survey = $this->ci->input->get_post('survey');

		// 	if(isset($Survey)) {

		// 		$this->ci->survey->config_insert($idx, $this->mcd, $Survey);

		// 	}

		// }



		//첨부파일 등록

		// $attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn'); 		//첨부파일 사용여부

		// $upload_str = '';





		// //첨부파일을 사용한다면...

		// if ($attach_file_yn != 'N') {

		// 	for ($i = 1; $i <= count($_FILES); $i++) {

		// 		//선택된 첨부 파일만...

		// 		if ($_FILES['attach' . $i]['tmp_name'][0] != '') {

		// 			//파일 업로드

		// 			$upload_str = $this->ci->iwc_common->upload('attach' . $i, $this->mcd, $this->bbs_config);



		// 			//파일 업로드 실패

		// 			if ($upload_str != TRUE) {

		// 				show_error($this->ci->iwc_common->upload_error_msg());

		// 			}

		// 			//파일 업로드 성공

		// 			else {

		// 				$upload_file_info = array();

		// 				$upload_file_info = $this->ci->iwc_common->upload_success_msg();



		// 				//첨부파일 DB 등록

		// 				$upload_file_info['idx'] = $idx;

		// 				$upload_file_info['menu_code'] = $this->mcd;



		// 				$this->ci->bbs->insert_attach($upload_file_info);

		// 			}

		// 		}

		// 	}

		// }




		echo func_jsAlertReplace('Register Complete.', '/?c=admin&m=adm_account&mcd=' . $this->mcd);

	}

	function adm_account_update_exec() {
		$cur_page = $this->ci->input->get_post('cur_page');
		$sParam = $this->ci->input->get_post('sParam');	  //검색 string
        $bb_idx = $this->ci->input->get_post('idx');

		$inData = array();
		$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴
		
		$inData['updated_dt'] = func_get_date_time();										 
		
		//DB 수정
		$idx = $this->ci->account->account_update_adm($inData,$bb_idx);

		echo func_jsAlertReplace('Modification Complete.', '/?c=admin&m=adm_account&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
	}




//회원 삭제

	function adm_customer_delete() {

		$customer_no = $this->ci->input->get_post('customer_no');

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	  //검색 string

//DB 삭제

		$idx = $this->ci->customer->customer_delete_adm($customer_no);



		echo func_jsAlertReplace('삭제되었습니다.', '/?c=admin&m=adm_income_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}



//회원 선택 메일 발송

	function adm_customer_email() {

		$all_email = $this->ci->input->get_post('all_email');



		$customer_no = '';

		$customer_nos = $this->ci->input->get_post('customer_no'); //배열

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	  //검색 string



		$sel_customer_no = array(); //회원번호

		$customer_id = array(); //회원 아이디

		$customer_name = array(); //회원명

		$email = array(); //이메일

		$cnt = 0;



//전체메일이 아닌 경우

		if ($all_email != 'Y') {

//선택 회원 배열

			if (count($customer_nos) > 0) {

				foreach ($customer_nos as $key => $value) {

					$customer_no = $value; //게시물 번호



					if ($customer_no != '') {

						$customer_detail_list = $this->ci->customer->customer_select_detail_adm($customer_no); //상세 조회

//회원 상세 조회

						$rows = '';

						if (count($customer_detail_list) > 0) {

							$rows = $customer_detail_list->row();

						}

						$sel_customer_no[$cnt] = $rows->customer_no;

						$customer_id[$cnt] = $rows->customer_id;

						$customer_name[$cnt] = $rows->customer_first_name;

						$email[$cnt] = $rows->email;

					}



					$cnt++;

				}

			}

		}

//전체메일

		else {

			$customer_all_list = $this->ci->customer->customer_select_all()->result(); //전체 조회



			if (count($customer_all_list) > 0) {

				foreach ($customer_all_list as $rows) {

					$sel_customer_no[$cnt] = $rows->customer_no;

					$customer_id[$cnt] = $rows->customer_id;

					$customer_name[$cnt] = $rows->customer_name;

					$email[$cnt] = $rows->email;



					$cnt++;

				}

			}

		}



		func_set_data($this->ci, 'mail_customer_no', $sel_customer_no);

		func_set_data($this->ci, 'mail_customer_id', $customer_id);

		func_set_data($this->ci, 'mail_customer_name', $customer_name);

		func_set_data($this->ci, 'mail_customer_email', $email);



		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/' . $this->ci->data['customer_join_mcd'] . '/adm_customer_email', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}



//회원 선택 메일 발송 처리

	function adm_customer_email_exec() {

		$customer_no = '';

		$customer_nos = $this->ci->input->get_post('receive_email'); //배열

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');		//검색 string



		$send_name = $this->ci->input->get_post('send_name');	 //보내는 사람

		$send_email = $this->ci->input->get_post('send_email');	//보내는 메일

		$subject = $this->ci->input->get_post('subject');	   //메일 제목

		$contents = $this->ci->input->get_post('mail_contents'); //메일 내용





		$customer_name = array(); //받는사람

		$email = array(); //받는메일

		$attach = ''; //첨부파일 FULL PATH



		$cnt = 0;

//선택 회원 배열

		if (count($customer_nos) > 0) {

			foreach ($customer_nos as $key => $value) {

				$customer_no = $value;



				if ($customer_no != '') {

					$customer_detail_list = $this->ci->customer->customer_select_detail_adm($customer_no); //상세 조회

//회원 상세 조회

					$rows = '';

					if (count($customer_detail_list) > 0) {

						$rows = $customer_detail_list->row();

					}



					$customer_name[$cnt] = $rows->customer_name;

					$email[$cnt] = $rows->email;

				}

				$cnt++;

			}





//메일 발송(백그라운 발송 방법 강구..)

			$this->ci->load->library('sendmail');





//선택된 첨부 파일만...

			if ($_FILES['attach1']['tmp_name'] != '') {

//파일 업로드

				$upload_str = $this->ci->iwc_common->upload('attach1', 'email', NULL, TRUE);



//파일 업로드 실패

				if ($upload_str != TRUE) {

					show_error($this->ci->iwc_common->upload_error_msg());

				}

//파일 업로드 성공

				else {

					$upload_file_info = array();

					$upload_file_info = $this->ci->iwc_common->upload_success_msg();



//rename($upload_file_info['full_path'], iconv("UTF-8", "EUC-KR", $upload_file_info['full_path']));

					$attach = $upload_file_info['full_path'];

				}

			}



//echo $attach;

//exit;



			if (count($email) > 0) {

				set_time_limit(0);



				for ($i = 0; $i < count($email); $i++) {

//이메일 유효성

					if (func_email_check($email[$i])) {

						$this->ci->sendmail->clear(TRUE);   //모든 이메일 변수들을 초기화  TRUE 시 첨부파일도 초기화

						$this->ci->sendmail->setMailType("html");

						$this->ci->sendmail->setInit();

						$this->ci->sendmail->setFrom($send_email, $send_name);

						$this->ci->sendmail->setTo($email[$i]);

						$this->ci->sendmail->setSubject($subject);

						$this->ci->sendmail->setMessage($contents);

						if ($attach != '') {

							$this->ci->sendmail->setAttach($attach);

						}

						$mail_result = $this->ci->sendmail->send(); //전송여부 TRUE FALSE 반환

//echo $this->ci->sendmail->printDebugger();

					}

				}

			}

		}

//메일 첨부 파일 삭제

		@unlink($attach);

		echo func_jsAlertReplace('메일이 발송 되었습니다.', '/?c=admin&m=adm_income_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}



//회원정보 선택 삭제

	function adm_account_all_delete() {


		$idxs ="";

		// $idxs = explode(";", $this->ci->input->get_post('idx'));

		 // $cnt = count($idxs);
		 // for ($i = 0; $i < $cnt; $i++){
		 
		 // 	exit();
		 // 	 if (is_numeric($a)){
		 // 	 		$this->ci->account->account_delete_adm($a);
		 // 	 }
		 // }
		

		$idxs = explode(";", $this->ci->input->get_post('id')); //배열

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	//검색 string

//선택 게시물 배열

		foreach ($idxs as $key => $value) {


			$idx = $value; //회원 번호
          
         
			if ($idx != '') {

			$this->ci->account->account_delete_adm($idx);
			

			}

		}



		// echo func_jsAlertReplace('삭제되었습니다.', '/?c=admin&m=adm_account&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}



//회원정보 선택 인증

	function adm_customer_all_certi() {

		$customer_no = '';

		$customer_nos = $this->ci->input->get_post('customer_no'); //배열

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	//검색 string

//선택 게시물 배열

		foreach ($customer_nos as $key => $value) {

			$customer_no = $value; //회원 번호



			if ($customer_no != '') {

				$this->ci->customer->customer_certi_update_adm($customer_no); //회원 인증

			}

		}



		echo func_jsAlertReplace('인증되었습니다.', '/?c=admin&m=adm_income_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}



	/* -------------------------------------------------------------------------------------------- */



//회원등급 HTML

	function view_customer_grade() {

//처리구분

		$exec_type = $this->ci->input->get_post('exec_type');

		$select = '';

		$select_detail = '';

		$seq_no = '';



//회원등급 조회

		$select = $this->ci->customer->customer_grade_select();



		if ($exec_type == 'UPDATE') {

			$seq_no = $this->ci->input->get_post('seq_no');

//회원등급 상세 조회

			$select_detail = $this->ci->customer->customer_grade_select_detail($seq_no);



			func_set_data($this->ci, 'customer_grade_detail_result', $select_detail);

		}



		func_set_data($this->ci, 'customer_grade_result', $select->result());

		func_set_data($this->ci, 'exec_type', $exec_type == '' ? 'INSERT' : $exec_type);

		func_set_data($this->ci, 'seq_no', $seq_no);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/adm_customer_grade', $this->ci->data, true)); //컨텐츠

//VIEW 로드

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}



//회원등급 등록&수정 EXEC

	function exec_customer_grade() {

//처리구분

		$exec_type = $this->ci->input->get_post('exec_type');



//POST 배열을 array['key'] = value 형태로 리턴

		$inData = func_get_post_array($this->ci->input->post('values'));



		$inData['created_dt'] = func_get_date_time();



		if ($exec_type != 'UPDATE') {



			$this->ci->customer->insert_customer_grade($inData);

			echo func_jsAlertReplace('등록 되었습니다.', '/?c=admin&amp;m=view_customer_grade');

		} else {

			$seq_no = $this->ci->input->get_post('seq_no');

			$this->ci->customer->update_customer_grade($inData, $seq_no);

			echo func_jsAlertReplace('수정 되었습니다.', '/?c=admin&amp;m=view_customer_grade');

		}

	}



//회원등급 삭제

	function exec_delete_customer_grade() {

		$seq_no = $this->ci->input->get_post('seq_no');



		$this->ci->customer->delete_customer_grade($seq_no);



		echo func_jsAlertReplace('삭제 되었습니다.', '/?c=admin&amp;m=view_customer_grade');

	}

	

}

?>