<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Adm_member.php
 * 개  요 : 회원관리
  ------------------------------------------------ */

class Adm_member {

    var $ci;
    var $member_join_config;

//constructor
    function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
        $this->ci = &get_instance();
         /* - Model Load



          ------------------------------------------------------------ */



        $this->ci->load->model('mo_bbs', 'bbs');
        $this->ci->load->model('mo_member', 'member');
        $this->ci->load->model('mo_file', 'mo_file');

//전체 사이트 맵
        $sitemap = $this->ci->data['site_map_list'];
        if (count($sitemap) > 0) {
            foreach ($sitemap as $rows) {
//회원관련 모듈의 메뉴코드_회원가입
                if (strstr($rows->menu_type, 'member_join')) {
                    func_set_data($this->ci, 'member_join_mcd', $rows->menu_code);
                }
//회원관련 모듈의 메뉴코드_로그인
                if (strstr($rows->menu_type, 'member_login')) {
                    func_set_data($this->ci, 'member_login_mcd', $rows->menu_code);
                }
            }
        }

//회원가입 설정정보 조회
        $this->member_join_config = $this->ci->iw_config->select_member_join_config()->result();

//회원가입을 설정을 하였다면..
        if (count($this->member_join_config) > 0) {
            func_set_data($this->ci, 'member_config_result', $this->member_join_config);

//스킨 이미지 경로(해당 회원가입 이미지 스킨 경로에 존재)
            $member_join_skin_images_url = '/views/user/member_join/' . $this->ci->data['member_join_mcd'] . '/images';
            func_set_data($this->ci, 'member_join_skin_images_url', $member_join_skin_images_url);
        } else {
//echo func_jsAlertReplace('회원가입이 생성되지 않았습니다.\n\n관리자에게 문의하세요.', '/?c=iwc', TRUE);
        }
    }

    function adm_customer_option_for_distribute(){
        $where=array();
        $sch_condition = $this->ci->input->get_post('sch_condition'); //카테고리 코드
        $sch_word = $this->ci->input->get_post('sch_word'); //카테고리 코드
        $where[$sch_condition] = $sch_word;
        $customers = $this->ci->member->select_customer($where);
        foreach($customers as $c){
            $customer_name = $c->member_first_name." ".$c->member_last_name;
            echo '<option class="group1_customer_'.$c->member_no.'" value="'.$c->member_no.'" data-customer-name="'.$customer_name.'">'.$customer_name.' ('.$c->email.')</option>';
        }
    }

    function adm_member_list_popup(){
        $this->ci->load->helper('url');
        //pagination init
        $this->ci->load->library('pagination');
        //pagination end init
        $where['grade_no'] = '11';
       
        //pagination process
        $total_rows = $this->ci->member->adm_count_member($where);
        $current_url = current_url()."?c=admin&m=adm_member_list_popup";
        $per_page = $this->ci->input->get_post("per_page");

        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';
        $config['num_links'] = 5;
        $config['base_url'] = $current_url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = 15;

        $this->ci->pagination->initialize($config); 
        $pagination = $this->ci->pagination->create_links();
        $limit_s = $per_page; 
        $limit_e = $config['per_page'];
        //pagination end process

        $member_list_query = $this->ci->member->adm_member_select($limit_s, $limit_e, $where);
        $member_list = $member_list_query->result();

        func_set_data($this->ci, 'pagination', $pagination);
        func_set_data($this->ci, 'member_list', $member_list);
        

        $this->ci->load->view('admin/adm/member/MEMBER/adm_member_list_popup', $this->ci->data, false);
    }
//회원조회
    function current_url()
    {
        $CI =& get_instance();
        $url = $CI->config->site_url($CI->uri->uri_string());
        return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
    }
    function adm_member_list_popup_exec(){
        $member_no_group = $this->ci->input->get_post('member_no'); //카테고리 코드
        $data['grade_no'] = 11;
        foreach($member_no_group as $data['member_no']){
            $this->ci->member->member_update_adm($data);
        }
        $this->adm_member_list_popup();
    }
    function adm_member_list($prev_next = 'N') {
       
        $grade_no_session = $this->ci->grade_no;
        $mcd = $this->ci->input->get_post("mcd");
        $where = array(); //sql where 조건
        $sParam = '';   //검색 string
//카테고리 검색
        $grade_no = $this->ci->input->get_post('grade_no'); //카테고리 코드
        $allowedGrades = array(1, 2, 3, 10, 11);
        
        if($grade_no_session!='1'){
            $allowedGrades = array(10, 11);
        }

        if ($grade_no != '') {
            if(in_array($grade_no, $allowedGrades)){
                $sParam .= '&grade_no=' . $grade_no;
                $where['grade_no ='] = $grade_no;
            }
        }elseif($grade_no_session == 2 || $grade_no_session == 3 || $mcd=='customer'){
                $sParam .= '&grade_no=' . '11';
                $where['grade_no ='] = '11';
        }

//검색 조건
        $sch_create_dt_s = $this->ci->input->get_post('sch_create_dt_s'); //등록일 시작
        if ($sch_create_dt_s != '') {
            $sParam .= '&sch_create_dt_s=' . $sch_create_dt_s;
            $where['created_dt >='] = $sch_create_dt_s . " 00:00:01";
        }
        $sch_create_dt_e = $this->ci->input->get_post('sch_create_dt_e'); //등록일 종료
        if ($sch_create_dt_e != '') {
            $sParam .= '&sch_create_dt_e=' . $sch_create_dt_e;
            $where['created_dt <='] = $sch_create_dt_e . " 23:59:59";
        }

        $sch_condition = $this->ci->input->get_post('sch_condition');   //검색조건
        $sch_word = $this->ci->input->get_post('sch_word');  //검색어
//검색조건 + 검색어
        if ($sch_condition != '' && $sch_word != '') {
            $sch_condition_tmp = explode('|', $sch_condition);

//다중 조건인 경우(제목 + 내용)
            if (count($sch_condition_tmp) > 1) {
                $tmpStr = '';
                for ($i = 0; $i < count($sch_condition_tmp); $i++) {
                    if ($tmpStr != '') {
                        $tmpStr .= ' or ';
                    }

                    $tmpStr .= $sch_condition_tmp[$i] . ' ? ';
                    $where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';
                }

                $tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';
                $where[$tmpStr] = '';

                $sParam .= '&sch_condition=' . $sch_condition;
                $sParam .= '&sch_word=' . $sch_word;
            } else {
                $where[$sch_condition] = '%' . $sch_word . '%';
                $sParam .= '&sch_condition=' . $sch_condition;
                $sParam .= '&sch_word=' . $sch_word;
            }
        }

//페이지 조건
        $cur_page = $this->ci->input->get_post("cur_page");
        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;
        $page_rows =20; //페이지 로우수
        $page_views = 10; //페이지 네비게이션 수
//회원등급 조회
        $member_grade_list = '';

        if ($mcd=="customer"||$grade_no_session=='2'||$grade_no_session=='3') {
            $member_grade_list = $this->ci->member->member_grade_select_register();
        } else {
            $member_grade_list = $this->ci->member->member_grade_select();    //회원등급 조회
        }
//회원 목록 조회
        $member_list = $this->ci->member->member_select(($cur_page - 1) * $page_rows, $page_rows, $where);

        $country_list = $this->ci->bbs->admGetCountry_list();
        
        $total_rows = $this->ci->member->total_member_select($where); //조회 전체 목록수
        
        $row_cnt =1+($cur_page - 1) * $page_rows;    //글 번호
       


//페이지정보 조회
        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);
        func_set_data($this->ci, 'mcd', $mcd);   //조회 전체 목록수
        func_set_data($this->ci, 'session_grade_no', $grade_no_session);   //조회 전체 목록수
        func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수
        func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지
        func_set_data($this->ci, 'row_cnt', $row_cnt);   //글번호
        func_set_data($this->ci, 'cur_page', $cur_page);    //현재 페이지
        func_set_data($this->ci, 'sParam', $sParam);     //검색 string
        func_set_data($this->ci, 'country_list', $country_list);
        func_set_data($this->ci, 'member_list', $member_list->result());
        func_set_data($this->ci, 'member_grade_list', $member_grade_list->result());
        func_set_data($this->ci, 'page_info', $page_info);
        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/member/' . $this->ci->data['member_join_mcd'] . '/adm_member_list', $this->ci->data, true));

        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }
    function adm_member_view(){
        $grade_no_session = $this->ci->grade_no;
        $member_no = $this->ci->input->get('member_no');
        $cur_page = $this->ci->input->get_post('cur_page');
        $mcd = $this->ci->input->get_post('mcd');
        $member_detail_list = $this->ci->member->member_select_detail_adm($member_no); //상세 조회
        //Check grade //
        $member_detail = $member_detail_list->result();
        $allowedGrades = array(1,2,3,10,11);
        $allowedToShow = false;
        //End Check grade //
        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;
        $sParam = $this->ci->input->get_post('sParam');  //검색 string

       

        $member_grade_list = '';

        if ($mcd=="customer"||$grade_no_session=='2' || $grade_no_session=='3') {
            $member_grade_list = $this->ci->member->select_all_user_grades();
        } else {
            $member_grade_list = $this->ci->member->select_all_admin_grades();    //회원등급 조회
        }
        //Check Grade // 
        if($grade_no_session!='1'){
            $allowedGrades = array(11);
        }
        if(in_array($member_detail['0']->grade_no, $allowedGrades)){
            $allowedToShow = true;
        }
        //End Check grade //
        if($allowedToShow){
            func_set_data($this->ci, 'session_grade_no', $grade_no_session);
            
            $select_cphoto_list = $this->ci->mo_file->select_cphoto($_GET['member_no']);

            func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
            func_set_data($this->ci, 'member_no', $member_no);
            func_set_data($this->ci, 'cur_page', $cur_page);    //현재 페이지
            func_set_data($this->ci, 'sParam', $sParam);     //검색 string
            func_set_data($this->ci, 'member_detail_list', $member_detail_list);
            func_set_data($this->ci, 'member_grade_list', $member_grade_list->result());

            func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/member/' . $this->ci->data['member_join_mcd'] . '/adm_member_view', $this->ci->data, true));
            $this->ci->load->view('admin/adm_index', $this->ci->data, false);
        }
    }
//회원 상세 조회
    function adm_member_detail() {
        $grade_no_session = $this->ci->grade_no;
        $member_no = $this->ci->input->get('member_no');
        $cur_page = $this->ci->input->get_post('cur_page');
        $mcd = $this->ci->input->get_post('mcd');
        $country_list = $this->ci->bbs->admGetCountry_list();
       
        $member_detail_list = $this->ci->member->member_select_detail_adm($member_no); //상세 조회
        //Check grade //
        $member_detail = $member_detail_list->result();
        $allowedGrades = array(1, 2, 3, 10, 11);
        $allowedToShow = false;
        //End Check grade //
        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;
        $sParam = $this->ci->input->get_post('sParam');  //검색 string

       

        $member_grade_list = '';

        if ($mcd=="customer"||$grade_no_session=='2'||$grade_no_session=='3') {
            $member_grade_list = $this->ci->member->member_grade_select_register();
        } else {
            $member_grade_list = $this->ci->member->member_grade_select();    //회원등급 조회
        }
        //Check Grade // 
        if($grade_no_session!='1'){
            $allowedGrades = array(11);
        }
        if(in_array($grade_no_session, $allowedGrades)){
            $allowedToShow = true;
        }
        //End Check grade //
        if($allowedToShow){
            func_set_data($this->ci, 'member_no', $member_no);
            func_set_data($this->ci, 'cur_page', $cur_page);    //현재 페이지
            func_set_data($this->ci, 'sParam', $sParam);     //검색 string 
            func_set_data($this->ci, 'country_list', $country_list);
            func_set_data($this->ci, 'member_detail_list', $member_detail_list);
            func_set_data($this->ci, 'member_grade_list', $member_grade_list->result());

            func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/member/' . $this->ci->data['member_join_mcd'] . '/adm_member_detail', $this->ci->data, true));
            $this->ci->load->view('admin/adm_index', $this->ci->data, false);
        }
    }

    function adm_add_member() {
        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/member/' . $this->ci->data['member_join_mcd'] . '/adm_add_member', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }

    function adm_member_write() {
        $this->ci->load->helper('url');
        $insert_id = $this->ci->user_member->insert_member();
        ob_clean();
        header("Location: /?c=admin&m=adm_member_detail&member_no=".$insert_id);
        //echo "This is member write page";
        // $grade_no_session = $this->ci->grade_no;

        // $country_list = $this->ci->bbs->admGetCountry_list();
        
        // $cur_page = $this->ci->input->get_post('cur_page');
        // if ($cur_page == NULL || $cur_page == '')
        //     $cur_page = 1;
        // $sParam = $this->ci->input->get_post('sParam');  //검색 string
        // $member_grade_list = '';
        // $mcd = $this->ci->input->get_post('mcd');

        // if ($mcd=="customer"||$grade_no_session=='3'||$grade_no_session=='2') {
        //     $member_grade_list = $this->ci->member->select_all_user_grades();
        // } else {
        //     $member_grade_list = $this->ci->member->select_all_admin_grades();    //회원등급 조회
        // }

        // func_set_data($this->ci, 'cur_page', $cur_page);    //현재 페이지
        // func_set_data($this->ci, 'sParam', $sParam);     //검색 string
        
        // func_set_data($this->ci, 'member_grade_list', $member_grade_list->result());

        // func_set_data($this->ci, 'country_list', $country_list);

        // func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/member/' . $this->ci->data['member_join_mcd'] . '/adm_member_write', $this->ci->data, true));
        // $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }

    function adm_member_write_exec() {
        $cur_page = $this->ci->input->get_post('cur_page');
        $sParam = $this->ci->input->get_post('sParam');   //검색 string
         $grade_no_session = $this->ci->grade_no;
        
        $inData = $this->ci->input->get_post('values');
//        $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];        //우편번호
        $inData['updated_dt'] = func_get_date_time();            //회원수정일


        unset($inData['zipcode1']);
        unset($inData['zipcode2']);
        $inData['contract_agree_yn'] = 'Y';        //메뉴코드

        $inData['member_pwd'] = func_base64_encode($inData['member_pwd']);            //비밀번호
        
        if (!isset($inData['created_dt']) || ($inData['created_dt'] == '')) {         //작성일
            $inData['created_dt'] = func_get_date_time();
        }
        if (!empty($inData['jumin_no'])) {
            $inData['jumin_no'] = func_base64_encode($inData['jumin_no']);
        }
        $member_id_check = $this->ci->member->exist_member_id_check($inData['member_id']);
        $allowedGrades = array(1, 2, 3, 10, 11);
        
        if($grade_no_session!='1'){
            $allowedGrades = array(10, 11);
        }
        
        //DB 등록
        if(in_array($inData['grade_no'], $allowedGrades)){
            if ($member_id_check->num_rows <= 0 || $inData['member_id'] == '') {
                $idx = $this->ci->member->member_insert($inData);
                echo func_jsAlertReplace('Register Complete.', '/?c=admin&m=adm_member_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
            } else {
                //var_dump($member_id_check);
                die("Member id exists");
            }
        }
        
    }

//회원 수정
    function adm_member_update() {
        $cur_page = $this->ci->input->get_post('cur_page');
        $sParam = $this->ci->input->get_post('sParam');   //검색 string
        $inData = array();
        $inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴
        $grade_no_session = $this->ci->grade_no;
        $member_no = $this->ci->input->get_post('member_no');
        $password = $this->ci->input->get_post('member_pwd');
        $member_detail_list = $this->ci->member->member_select_detail_adm($inData['member_no'])->result(); //상세 조회
       
        $allowedGrades = array(1, 2, 3, 10, 11);
        $allowedToSave = false;
        if($grade_no_session!='1'){
            $allowedGrades = array(10, 11);
        }
        if(in_array($grade_no_session, $allowedGrades)){
            $allowedToSave = true;
        }
        if ($password !='') {
            $inData['member_pwd'] = func_base64_encode($password);            //비밀번호
        }
        

//        $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];        //우편번호
        $inData['updated_dt'] = func_get_date_time();            //회원수정일

        unset($inData['zipcode1']);
        unset($inData['zipcode2']);
        if($allowedToSave){
    //첨부파일이 존재한다면...
            if (count($_FILES) > 0) {
    //선택된 첨부 파일만...
                if ($_FILES['attach']['tmp_name'] != '') {
    //파일 업로드
                    $upload_str = $this->ci->iwc_common->upload_image('attach', $this->mcd);

    //파일 업로드 실패
                    if ($upload_str != TRUE) {
                        show_error($this->ci->iwc_common->upload_error_msg());
                    }
    //파일 업로드 성공
                    else {
                        $upload_file_info = array();
                        $upload_file_info = $this->ci->iwc_common->upload_success_msg();

                        $inData['photo'] = $upload_file_info['file_name']; //사진 파일명
                    }
                }
            }
            
    //DB 수정
            if(!in_array($inData['grade_no'], $allowedGrades)) unset($inData['grade_no']);
            $idx = $this->ci->member->member_update_adm($inData);

            echo func_jsAlertReplace('Modification Complete.', '/?c=admin&m=adm_member_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
        }
    }

//회원 삭제
    function adm_member_delete() {
        $member_no = $this->ci->input->get_post('member_no');
        $cur_page = $this->ci->input->get_post('cur_page');
        $sParam = $this->ci->input->get_post('sParam');   //검색 string
//DB 삭제
        $idx = $this->ci->member->member_delete_adm($member_no);

        echo func_jsAlertReplace('삭제되었습니다.', '/?c=admin&m=adm_member_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
    }


//회원정보 선택 삭제
    function adm_member_all_delete() {
        
        $member_nos = $this->ci->input->get_post('member_no'); //배열

        $cur_page = $this->ci->input->get_post('cur_page');
        $sParam = $this->ci->input->get_post('sParam'); //검색 string
//선택 게시물 배열
        foreach ($member_nos as $key => $value) {
            $member_no = $value; //회원 번호

            if ($member_no != '') {
                $this->ci->member->member_delete_adm($member_no);

                $file_owner = $this->ci->mo_file->select_file_public_id($member_no);

                if (count($file_owner) > 0){

                    foreach($file_owner as $id){
                        $bbs_idx[] = $id->public_id;
                        $bbs_idx[] = implode(',', $bbs_idx);
                        \Cloudinary\Uploader::destroy($id->public_id);
                    }
                    $this->ci->mo_file->delete_cloud_file($bbs_idx);

                }
            }
        }

        echo func_jsAlertReplace('Deletion Complete.', '/?c=admin&m=adm_member_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
    }

//회원정보 선택 인증
    function adm_member_all_certi() {
        $member_no = '';
        $member_nos = $this->ci->input->get_post('member_no'); //배열
        $cur_page = $this->ci->input->get_post('cur_page');
        $sParam = $this->ci->input->get_post('sParam'); //검색 string
//선택 게시물 배열
        foreach ($member_nos as $key => $value) {
            $member_no = $value; //회원 번호

            if ($member_no != '') {
                $this->ci->member->member_certi_update_adm($member_no); //회원 인증
            }
        }

        echo func_jsAlertReplace('인증되었습니다.', '/?c=admin&m=adm_member_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
    }

    /* -------------------------------------------------------------------------------------------- */

//회원등급 HTML
    function view_member_grade() {
//처리구분
        $exec_type = $this->ci->input->get_post('exec_type');
        $select = '';
        $select_detail = '';
        $seq_no = '';

//회원등급 조회
        $select = $this->ci->member->member_grade_select();

        if ($exec_type == 'UPDATE') {
            $seq_no = $this->ci->input->get_post('seq_no');
//회원등급 상세 조회
            $select_detail = $this->ci->member->member_grade_select_detail($seq_no);

            func_set_data($this->ci, 'member_grade_detail_result', $select_detail);
        }

        func_set_data($this->ci, 'member_grade_result', $select->result());
        func_set_data($this->ci, 'exec_type', $exec_type == '' ? 'INSERT' : $exec_type);
        func_set_data($this->ci, 'seq_no', $seq_no);
        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/member/adm_member_grade', $this->ci->data, true)); //컨텐츠
//VIEW 로드
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }

//회원등급 등록&수정 EXEC
    function exec_member_grade() {
//처리구분
        $exec_type = $this->ci->input->get_post('exec_type');

//POST 배열을 array['key'] = value 형태로 리턴
        $inData = func_get_post_array($this->ci->input->post('values'));

        $inData['created_dt'] = func_get_date_time();

        if ($exec_type != 'UPDATE') {

            $this->ci->member->insert_member_grade($inData);
            echo func_jsAlertReplace('등록 되었습니다.', '/?c=admin&amp;m=view_member_grade');
        } else {
            $seq_no = $this->ci->input->get_post('seq_no');
            $this->ci->member->update_member_grade($inData, $seq_no);
            echo func_jsAlertReplace('수정 되었습니다.', '/?c=admin&amp;m=view_member_grade');
        }
    }

//회원등급 삭제
    function exec_delete_member_grade() {
        $seq_no = $this->ci->input->get_post('seq_no');

        $this->ci->member->delete_member_grade($seq_no);

        echo func_jsAlertReplace('삭제 되었습니다.', '/?c=admin&amp;m=view_member_grade');
    }

    /* -------------------------------------------------------------------------------------------- */
}

?>