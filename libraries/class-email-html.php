<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/libraries/scrapwebdata/simple_html_dom.php';
class EmailHtml{
	var $attachments = array();
	var $html;
	function __construct($html){
        set_time_limit(0);
		$html = str_get_html($html);

        $i=0;
        $imgs = $html->find('img');
        $images = array();
        foreach($imgs as $key=>$img){
            $i++;

            $img_src = $img->src;
            $img_name = md5($img->src);
            $img_des = 'email_template/tmp/'.$img_name;

            $this->download($img->src, $img_des);

            $images[] = array('des'=>$img_des, 'name'=>$img_name, 'cid'=>'file-cid'.$img_name);
            $html->find('img', $key)->src='cid:file-cid'.$img_name;
            
        }
        $this->html = $html;
        $this->attachments = $images;
	}

    function download($src, $des){
        //File to save the contents to
        $fp = fopen ($des, 'w+');
         
        $url = $src;
         
        //Here is the file we are downloading, replace spaces with %20
        $ch = curl_init(str_replace(" ","%20",$url));
         
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
         
        //give curl the file pointer so that it can write to it
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
         
        $data = curl_exec($ch);//get curl response
         
        //done
        curl_close($ch);
    }
}