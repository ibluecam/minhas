<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Adm_pay.php
 * 개  요 : 결제관리
  ------------------------------------------------ */

class Adm_pay {

	var $ci;

//constructor
	function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();

		$this->ci->load->model('mo_pay', 'payment');
		$this->ci->load->model('mo_member', 'member');
		$this->ci->load->model('mo_income', 'income');
	}

//결제회원조회
	function adm_pay_list() {
		$where = array(); //sql where 조건
		$sParam = '';	  //검색 string
//검색 조건
		$sch_create_dt_s = $this->ci->input->get_post('sch_create_dt_s'); //등록일 시작
		if ($sch_create_dt_s != '') {
			$sParam .= '&sch_create_dt_s=' . $sch_create_dt_s;
			$where['created_dt >='] = $sch_create_dt_s . " 00:00:01";
		}
		$sch_create_dt_e = $this->ci->input->get_post('sch_create_dt_e'); //등록일 종료
		if ($sch_create_dt_e != '') {
			$sParam .= '&sch_create_dt_e=' . $sch_create_dt_e;
			$where['created_dt <='] = $sch_create_dt_e . " 23:59:59";
		}

		$sch_condition = $this->ci->input->get_post('sch_condition');   //검색조건
		$sch_word = $this->ci->input->get_post('sch_word');		//검색어
//검색조건 + 검색어
		if ($sch_condition != '' && $sch_word != '') {
			$sch_condition_tmp = explode('|', $sch_condition);

//다중 조건인 경우(제목 + 내용)
			if (count($sch_condition_tmp) > 1) {
				$tmpStr = '';
				for ($i = 0; $i < count($sch_condition_tmp); $i++) {
					if ($tmpStr != '') {
						$tmpStr .= ' or ';
					}

					$tmpStr .= $sch_condition_tmp[$i] . ' ? ';
					$where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';
				}

				$tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';
				$where[$tmpStr] = '';

				$sParam .= '&sch_condition=' . $sch_condition;
				$sParam .= '&sch_word=' . $sch_word;
			} else {
				$where[$sch_condition] = '%' . $sch_word . '%';
				$sParam .= '&sch_condition=' . $sch_condition;
				$sParam .= '&sch_word=' . $sch_word;
			}
		}

//페이지 조건
		$cur_page = $this->ci->input->get_post("cur_page");
		if ($cur_page == NULL || $cur_page == '')
			$cur_page = 1;
		$page_rows = 10; //페이지 로우수
		$page_views = 10; //페이지 네비게이션 수
//결제 회원 목록 조회
		$payment_list = $this->ci->payment->payment_select(($cur_page - 1) * $page_rows, $page_rows, $where);

		$total_rows = $this->ci->payment->total_payment_select($where); //조회 전체 목록수
		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   //글 번호
//페이지정보 조회
		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);

		func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수
		func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지
		func_set_data($this->ci, 'row_cnt', $row_cnt);		 //글번호
		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지
		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string
		func_set_data($this->ci, 'payment_list', $payment_list->result());
		func_set_data($this->ci, 'page_info', $page_info);
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/pay/adm_pay_list', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}

//결제정보 상세 조회
	function adm_pay_detail() {
		$seq_no = $this->ci->input->get_post('seq_no');
		$cur_page = $this->ci->input->get_post('cur_page');
		if ($cur_page == NULL || $cur_page == '')
			$cur_page = 1;
		$sParam = $this->ci->input->get_post('sParam');		//검색 string

		$payment_detail_list = $this->ci->payment->payment_select_detail_adm($seq_no); //상세 조회

		func_set_data($this->ci, 'seq_no', $seq_no);
		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지
		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string
		func_set_data($this->ci, 'payment_detail_list', $payment_detail_list);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/pay/adm_pay_detail', $this->ci->data, true));
		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}

//결제정보 수정
	function adm_pay_update() {
		$cur_page = $this->ci->input->get_post('cur_page');
		$sParam = $this->ci->input->get_post('sParam');	  //검색 string

		$inData = array();
		$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴
//강의 중지일 경우..
		if ((integer) $inData['course_stop_cnt'] > 0) {
			$inData['course_stop_sdt'] = func_get_date_time();
			$inData['course_stop_edt'] = func_add_date_time(func_get_date_time(), (90 * (integer) $inData['course_stop_cnt']));
		}

//DB 수정
		$idx = $this->ci->payment->payment_update_adm($inData);

		echo func_jsAlertReplace('Modification Complete.', '/?c=admin&m=adm_pay_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
	}

	function adm_customer_distribution(){
		/***RETRIVE DATA FROM REQUEST***/
		$deposit_id = $this->ci->input->get_post('deposit_id');
		$is_posted = $this->ci->input->get_post('is_posted');
		$deposit_where['id'] = $deposit_id;
		if(empty($is_posted)){
			$is_posted="no";
		}
		/***GET DATA FROM MODEL***/
		$dist_list = $this->ci->payment->get_customer_distribution($deposit_id);
		$deposits = $this->ci->income->select_deposit($deposit_where);
		$customer_list = $this->ci->member->select_customer();
		$deposit_detail = $deposits['0'];
		/*** Set Variable to view ***/
		func_set_data($this->ci, 'dist_list', $dist_list);
		func_set_data($this->ci, 'is_posted', $is_posted);
		func_set_data($this->ci, 'deposit_detail', $deposit_detail);
		func_set_data($this->ci, 'deposit_id', $deposit_id);
		func_set_data($this->ci, 'customer_list', $customer_list);
		/*** LOAD VIEW ***/
		$this->ci->load->view('admin/adm/pay/adm_customer_distribution', $this->ci->data, false);
	}

	function adm_customer_ditribute_insert_exec(){
		/***RETRIVE DATA FROM REQUEST***/
		$customers_distribute = $this->ci->input->get_post('customer_no');
		$currency_type = $this->ci->input->get_post('currency_type');
		$deposit_id = $this->ci->input->get_post('deposit_id');
		$deposit_where['id'] = $deposit_id;
		/***GET DATA FROM MODEL***/
		$deposits = $this->ci->income->select_deposit($deposit_where);
		$deposit_detail = $deposits['0'];
		/** Validate balance with distribute ***/
		$balance = $deposit_detail->deposit;
		$array_distribute_value = array_values($customers_distribute);
		$sum_input_distribute = array_sum($array_distribute_value);
		/***EXECUTE DATABASE VIA MODEL***/	
		
		if($balance>=$sum_input_distribute){
			/***Enough Balance to distribute ***/
			$distribute_result = $this->ci->payment->adm_distribute_customer($customers_distribute, $currency_type, $deposit_id);
			if($distribute_result){
				$this->ci->income->sub_deposit_balance($deposit_id, $sum_input_distribute);
				echo func_jsAlertReplace('Success! You have distributed to customers successfully.', '/?c=admin&m=adm_customer_distribution&is_posted=yes&deposit_id='.$deposit_id);
			}
		}else{
			/***Insufficient Balance to distribute ***/
			die("Not enough balance");
		}
	}
}
?>