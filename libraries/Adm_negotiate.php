<?php
if (!defined('BASEPATH'))

    exit('No direct script access allowed');



/* ------------------------------------------------

 

 * 개  요 : 회원관리

  ------------------------------------------------ */
class Adm_negotiate {
    
   var $ci;

    var $mcd;

    var $bbs_config;

    var $member_no;
    var $member_id;


    //constructor

    function __construct() {

      

        $this->ci = &get_instance();

        /* ------------------------------------------------------------



          - Model Load
        


          ------------------------------------------------------------ */
            $this->ci->load->library('phpsession');
            $this->ci->load->model('mo_message_detail_negotiate', 'message_details');
        	$this->ci->load->model('mo_bbs', 'bbs');
			$this->ci->load->model('mo_member', 'user_member');
            $this->member_no = $this->ci->phpsession->get('member_no', 'ADMIN');

            $this->member_id = $this->ci->phpsession->get('member_id', 'ADMIN');


    }

	function unsetGetUrl($unset_p=array()){
        $get = $_GET;
        if(count($unset_p)>0){
            foreach($unset_p as $value){
                if(isset($get[$value])) unset($get[$value]);
            }
        }
        $updatedUrl_arr = array();
        $updatedUrl = http_build_query($get);
        return $updatedUrl;
    }
	
	function _load_member_view($view_path, $arr_ex_js=array(), $arr_ex_css=array()){
        func_set_data($this->ci, 'arr_ex_css', $arr_ex_css);
        func_set_data($this->ci, 'arr_ex_js', $arr_ex_js);
        func_set_data($this->ci, 'page_content', $this->ci->load->view($view_path, $this->ci->data, true));
        $this->ci->load->view('member_site/layout/template.php', $this->ci->data, false);
    }
	
	
    function adm_negotiate(){
		$this->ci->load->helper('url');
        $this->ci->load->library('pagination'); 
        $where=array();
        $gets = $_GET;
        $get_values = array();
        // search condition
		$sParam = '';
			$msg = $this->ci->input->get_post('msg');
		if (!empty($msg)) {
            $where['read'] = 'unseen';
        }
			$sch_date = $this->ci->input->get_post('sch_date');
		if (!empty($sch_date)) {
            $where['message_update_date'] = $sch_date; 
        }
			$sch_car_location = $this->ci->input->get_post('sch_car_location');
        if (!empty($sch_car_location)) {
            $where['country'] = $sch_car_location;
        }
           $carmake = $this->ci->input->get_post('carmake');
         if ($carmake !='') {
            $where['car_make'] = $carmake;
         }
        $carmodel = $this->ci->input->get_post('carmodel');
          if ($carmodel  !='') {
            $where['car_model'] = $carmodel;
        }
        $sch_icon_status = $this->ci->input->get_post('sch_icon_status');
        if ($sch_icon_status !='') {
            $where['icon_status'] = $sch_icon_status;
        }
		
		$sch_staff_name = $this->ci->input->get_post('staff_name');
        if ($sch_staff_name !='') {
            $where['support_member_no'] = $sch_staff_name;
        }

       $sch_word = $this->ci->input->get_post('sch_word');
        if ($sch_word !='') {
           $where['car_keyword'] ='%'.$sch_word.'%';  
           
        }
        // end search
       
		$limit_s = $this->ci->input->get_post("offset");
        if(!$limit_s) $limit_s =0;
		$order='message_update_date DESC';
        $get_url = $this->unsetGetUrl(array("offset"));
        $result_total_rows = $this->ci->bbs->adm_negotiate_total_select($where);
        if($result_total_rows == null){
             $total_rows = 1;
        }else{
             $total_rows = $result_total_rows;
        }
       

        $cur_page = $this->ci->input->get_post("offset");
        if ($cur_page == NULL || $cur_page == '')
        $cur_page = 1;


        /*****START PAGINATION*****/
        $config['base_url'] = site_url()."/?".$get_url;
        $config['uri_segment'] = 3;
        $config['num_links'] = 5;
        $config['total_rows'] = $total_rows;
        $config['per_page'] =10;
        $config['prev_link']='PREVIOUS';
        $config['next_link']='NEXT';
        $config['last_tag_open'] = '<div style="display:none;">';
        $config['last_tag_close'] = '</div>';
        $config['query_string_segment'] = "offset";
        $this->ci->pagination->initialize($config);
        $pagination = $this->ci->pagination->create_links();
		
        $cuurent=$config['use_page_numbers'] = TRUE;
        /*****END PAGINATION*****/
        $cur_page =$limit_s/10;
        $total_page=$total_rows;
		
		$list_car = $this->ci->bbs->get_car_negotiate($where,$limit_s,$config['per_page'],$order);
		$select_make_all = $this->ci->bbs->search_make_Negotiate();
        $select_model_all = $this->ci->bbs->search_model_Negotiate();
		$country_list = $this->ci->bbs->list_country_location_item();
		$date_list = $this->ci->bbs->search_date_Negotiate();
        $model_list = array();
        $where_model['car_make']=$carmake;
        $model_list = $this->ci->bbs->getModel($where_model, true);
		$sale_staff = $this->ci->user_member->adm_select_customer_service();
		func_set_data($this->ci, 'list_car', $list_car);
		func_set_data($this->ci, 'sale_staff', $sale_staff);
		func_set_data($this->ci, 'select_make_all',$select_make_all);
        func_set_data($this->ci, 'select_model_all',$select_model_all);
		func_set_data($this->ci, 'model_list',$model_list);
		func_set_data($this->ci, 'country_list', $country_list);
		func_set_data($this->ci, 'date_list', $date_list);
		func_set_data($this->ci, 'pagination', $pagination);
        func_set_data($this->ci, 'total_rows', $total_rows);
        func_set_data($this->ci, 'cur_page', $cur_page);
		func_set_data($this->ci, 'sParam', $sParam);
        func_set_data($this->ci, 'total_page', $total_page);
        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/negotiate/adm_negotiate', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
        
    }


	function member_negotiate(){
		$this->ci->load->helper('url');
        $this->ci->load->library('pagination'); 
        $where=array();
        $gets = $_GET;
        $get_values = array();
        $order='message_update_date DESC';
        // search condition
		$sParam = '';
		 
		 
			$msg = $this->ci->input->get_post('msg');
		if (!empty($msg)) {
            $where['read'] = 'unseen';
        }
			$sch_car_location = $this->ci->input->get_post('sch_car_location');
        if (!empty($sch_car_location)) {
            $where['country'] = $sch_car_location;
        }
           $carmake = $this->ci->input->get_post('carmake');
         if ($carmake !='') {
            $where['car_make'] = $carmake;
         }
        $carmodel = $this->ci->input->get_post('carmodel');
          if ($carmodel  !='') {
            $where['car_model'] = $carmodel;
        }
        $sch_icon_status = $this->ci->input->get_post('sch_icon_status');
        if ($sch_icon_status !='') {
            $where['icon_status'] = $sch_icon_status;
        }
		
		$sch_staff_name = $this->ci->input->get_post('staff_name');
        if ($sch_staff_name !='') {
            $where['support_member_no'] = $sch_staff_name;
        }

       $sch_word = $this->ci->input->get_post('sch_word');
        if ($sch_word !='') {
           $where['car_keyword'] ='%'.$sch_word.'%';  
           
        }
        // end search
        $customer_no= $_SESSION['ADMIN']['member_no'];
		$limit_s = $this->ci->input->get_post("offset");
        if(!$limit_s) $limit_s =0;
        $get_url = $this->unsetGetUrl(array("offset"));
        $result_total_rows = $this->ci->bbs->member_negotiate_total_select($where);
        if($result_total_rows == null){
             $total_rows = 1;
        }else{
             $total_rows = $result_total_rows;
        }

        $cur_page = $this->ci->input->get_post("offset");
        if ($cur_page == NULL || $cur_page == '')
        $cur_page = 1;


        /*****START PAGINATION*****/
        $config['base_url'] = site_url()."/?".$get_url;
        $config['uri_segment'] = 3;
        $config['num_links'] = 5;
        $config['total_rows'] = $total_rows;
        $config['per_page'] =10;
        $config['prev_link']='PREVIOUS';
        $config['next_link']='NEXT';
        $config['last_tag_open'] = '<div style="display:none;">';
        $config['last_tag_close'] = '</div>';
        $config['query_string_segment'] = "offset";
        $this->ci->pagination->initialize($config);
        $pagination = $this->ci->pagination->create_links();
        $cuurent=$config['use_page_numbers'] = TRUE;
        /*****END PAGINATION*****/
        $cur_page =$limit_s/10;
        $total_page=$total_rows;
		
		$list_car = $this->ci->bbs->member_negotiate($where,$limit_s,$config['per_page'],$order);
		$select_make_all = $this->ci->bbs->search_make_Negotiate();
        $select_model_all = $this->ci->bbs->search_model_Negotiate();
		$country_list = $this->ci->bbs->list_country_location_item();
        $model_list = array();
        $where_model['car_make']=$carmake;
        $model_list = $this->ci->bbs->getModel($where_model, true);
		$sale_staff = $this->ci->user_member->adm_select_customer_service();
        $total_bbs = $this->ci->bbs->user_total_select();

        $business_type = $_SESSION['ADMIN']['business_type'];
        $access_type = true;
        if($business_type=='buyer'){
            $access_type = false;
        }
        func_set_data($this->ci, 'access_type', $access_type);

        $nav_array = array(
            "Home"=>"/",
            "Dashboard"=>"/?c=admin",
            "My Info"=>"#",
        );
        $navigation_bar = func_get_nav($nav_array);
        func_set_data($this->ci, 'navigation_bar', $navigation_bar);
        $sidebar = $this->ci->load->view('member_site/product/sidebar.php', $this->ci->data, true);

        func_set_data($this->ci, 'sidebar', $sidebar);
        func_set_data($this->ci, 'total_bbs_count', $total_bbs);
		func_set_data($this->ci, 'list_car', $list_car);
		func_set_data($this->ci, 'sale_staff', $sale_staff);
		func_set_data($this->ci, 'select_make_all',$select_make_all);
        func_set_data($this->ci, 'select_model_all',$select_model_all);
		func_set_data($this->ci, 'model_list',$model_list);
		func_set_data($this->ci, 'country_list', $country_list);
		func_set_data($this->ci, 'pagination', $pagination);
        func_set_data($this->ci, 'total_rows', $total_rows);
        func_set_data($this->ci, 'cur_page', $cur_page);
		func_set_data($this->ci, 'sParam', $sParam);
        func_set_data($this->ci, 'total_page', $total_page);
        $arr_css = array('/css/member_site/negotiate.css');
        $arr_js = array('/js/admin/jquery.chained.min.js','/js/member_site/negotiate.js');
        $this->_load_member_view('member_site/message_buyer/member_negotiate',$arr_js,$arr_css);
        
    }


	function adm_message_read(){
        
		if(isset($_GET['car_idx'])){
			$car_idx = $_GET['car_idx'];
			$car_idx = str_replace("email_id_","",$car_idx);
			$sender_no = $_GET['sender_no'];
			$this->ci->bbs->message_read_exec($car_idx, $sender_no);
			
			redirect('/?c=admin&m=adm_message_detail&car_idx='.$car_idx.'&sender_no='.$sender_no, 'refresh');
		}
        
    }
	
	function adm_delete_message(){
        if(isset($_GET['idx'])) {
			$idx_array = $_GET['idx']; // return array
			$sender_array = $_GET['sender_no']; // return array
	   			$this->ci->bbs->message_delete_exec($idx_array, $sender_array);
	
		redirect('/?c=admin&m=adm_negotiate', 'refresh');
		}

    }
	
	function member_delete_message(){
        if(isset($_GET['idx'])) {
			$idx_array = $_GET['idx']; // return array
			$sender_array = $_GET['sender_no']; // return array
			
	   			$this->ci->bbs->message_delete_exec($idx_array, $sender_array);
	
		redirect('/?c=admin&m=member_negotiate', 'refresh');
		}

    }
	
	function member_message_read(){
       
		if(isset($_GET['car_idx'])){
			$car_idx = $_GET['car_idx'];
			$car_idx = str_replace("email_id_","",$car_idx);
			$sender_no = $_GET['sender_no'];
			$this->ci->bbs->message_read_exec($car_idx, $sender_no);
			
			redirect('/?c=admin&m=adm_message_buyer&car_idx='.$car_idx.'&sender_no='.$sender_no, 'refresh');
		}
        
    }
	
    function adm_message_detail(){
        
        //$get_session_sender_no = $this->ci->phpsession->get("sender_no", "ADMIN");
        

        if(isset($_GET["car_idx"]) && isset($_GET["sender_no"])){
            $set_userdata = array(
                                    'car_idx' => $_GET["car_idx"] 
                                ); 
            $this->ci->session->set_userdata("set_userdata",$set_userdata);
             
        }
        
        

        $comments = $this->ci->message_details->get_comment();


        if(isset($_POST["data"])){
            $data = $_POST["data"];

        
            $set_comments = $this->ci->message_details->set_comment($data);

        }
        //var_dump($customers); exit();
        //$comments = array('Hello','world');
        if(isset($_GET['car_idx'])){
            $car_idx = $_GET['car_idx'];
            $sender_no = $_GET['sender_no'];
            $count_message = 0;

            $cardetail = $this->ci->bbs->getCarSpecification_message_detail($car_idx ,$sender_no);
            $cardetail_count = $this->ci->bbs->getCarSpecification_message_detail_count();
            if(!empty($cardetail_count)){
                $count_message = $cardetail_count[0]->count_read;
            }
           
         
        }else{
            redirect('/?c=admin&m=adm_negotiate', 'refresh');
        }

        func_set_data($this->ci, 'comments',$comments);
        func_set_data($this->ci, 'cardetail',$cardetail);
        func_set_data($this->ci, 'member_no',$this->member_no);
        func_set_data($this->ci, 'cardetail_count',$count_message);
        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/negotiate/adm_message_detail', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
        
    }

    function adm_message_buyer(){
        
        
        if(isset($_GET["car_idx"]) && isset($_GET["sender_no"])){
            $set_userdata = array(
                                    'car_idx' => $_GET["car_idx"], 
                                    'sender_no' => $_GET["sender_no"],
                                ); 
            $this->ci->session->set_userdata("set_userdata",$set_userdata);
            
        }

        $comments = $this->ci->message_details->get_comment();
        
        if(isset($_POST["data"])){
            $data = $_POST["data"];
        
            $set_comments = $this->ci->message_details->set_comment($data);

        }
        //var_dump($customers); exit();
        //$comments = array('Hello','world');
        if(isset($_GET['car_idx'])){
            $car_idx = $_GET['car_idx'];
            $car_owner = $_GET['sender_no'];
            $count_message = 0;

            $cardetail = $this->ci->bbs->getCarSpecification_message_detail($car_idx,$car_owner);
            $detail_team_seller = $this->ci->bbs->get_team_seller($car_idx);
            $cardetail_count = $this->ci->bbs->getCarSpecification_message_detail_count();
            if(!empty($cardetail_count)){
                $count_message = $cardetail_count[0]->count_read;
            }

           
         
        }else{
            redirect('/?c=admin&m=member_negotiate', 'refresh');
        }


        func_set_data($this->ci, 'member_name',$this->member_id);
        func_set_data($this->ci, 'comments',$comments);
        func_set_data($this->ci, 'detail_team_seller',$detail_team_seller);
        func_set_data($this->ci, 'cardetail',$cardetail);
        $arr_css = array('/css/admin/admin_message_buyer.css');
        $arr_js = array();
        $this->_load_member_view('member_site/message_buyer/adm_message_buyer',$arr_js,$arr_css);

    }
    function get_message(){
        
       $where = $this->ci->session->userdata("set_userdata");
        

        //====================== Pagination ==============//
            $per_page = 5;


            if(isset($_GET["page"])){

                if ($_GET["page"] != 0) {
                   // var_dump($_GET["page"]); exit();
                    $page=$_GET["page"];
                }else{
                    $page=1;
                }

            }else{
                $page=1;
            }


            $start_page=($page-1)*$per_page;
        //====================== End Pagination ==============//

        /*--------------------------------*/
        /* get Note for admin / buyer*/
        /*--------------------------------*/

        
        if(isset($_GET["n"])){
            $note_role = $_GET["n"];
        }



        //================== Get Message ==============//     
        $comments = $this->ci->message_details->get_comment($where,$start_page,$per_page);

        //================== Count Message ==============//
        $countcomments = $this->ci->message_details->count_comment($where);


        //$comments = array('Hello','world');
        func_set_data($this->ci, 'comments',$comments);
        func_set_data($this->ci, 'countcomments',$countcomments);
        func_set_data($this->ci, 'perpagepagination',$per_page);
        func_set_data($this->ci, 'getnumpage',$page);

        func_set_data($this->ci, 'note_role',$note_role);

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/negotiate/get_message', $this->ci->data, true));
        $this->ci->load->view('admin/adm_blank', $this->ci->data, false);
        
    }
    function get_message_buyer(){
        

        $comments = $this->ci->message_details->get_comment();

    
        //var_dump($customers); exit();
        //$comments = array('Hello','world');
        func_set_data($this->ci, 'comments',$comments);
        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/negotiate/get_message_buyer', $this->ci->data, true));
        $this->ci->load->view('admin/adm_blank', $this->ci->data, false);
        
    }


   
}

?>