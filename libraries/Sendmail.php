<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Sendmail {

	var $ci;
	var $mail_config = array();

	// constructor
	function __construct() {
		$this->ci = &get_instance();
		$this->ci->load->library("email");
		$this->ci->config->load("mail");

		//메일 설정정보 세팅
		$this->_setEmailConfiguration();
	}

	//메일설정정보 세팅
	function _setEmailConfiguration() {
		$this->mail_config["useragent"] = $this->ci->config->item("useragent");
		$this->mail_config["protocol"] = $this->ci->config->item("protocol");
		$this->mail_config["mailpath"] = $this->ci->config->item("mailpath");
		$this->mail_config["smtp_host"] = $this->ci->config->item("smtp_host");
		$this->mail_config["smtp_user"] = $this->ci->config->item("smtp_user");
		$this->mail_config["smtp_pass"] = $this->ci->config->item("smtp_pass");
		$this->mail_config["smtp_port"] = $this->ci->config->item("smtp_port");
		$this->mail_config["smtp_timeou"] = $this->ci->config->item("smtp_timeou");
		$this->mail_config["wordwrap"] = $this->ci->config->item("wordwrap");
		$this->mail_config["wrapchars"] = $this->ci->config->item("wrapchars");
		$this->mail_config["mailtype"] = $this->ci->config->item("mailtype");
		$this->mail_config["charset"] = $this->ci->config->item("charset");
		$this->mail_config["validate"] = $this->ci->config->item("validate");
		$this->mail_config["priority"] = $this->ci->config->item("priority");
		$this->mail_config["newline"] = $this->ci->config->item("newline");
		$this->mail_config["bcc_batch_mode"] = $this->ci->config->item("bcc_batch_mode");
		$this->mail_config["bcc_batch_size"] = $this->ci->config->item("bcc_batch_size");

		/*

		var_dump($this->mail_config["useragent"]);var_dump("<br/>");
		var_dump($this->mail_config["protocol"]);var_dump("<br/>");
		var_dump($this->mail_config["mailpath"]);var_dump("<br/>");
		var_dump($this->mail_config["smtp_host"]);var_dump("<br/>");
		var_dump($this->mail_config["smtp_user"]);var_dump("<br/>");
		var_dump($this->mail_config["smtp_pass"]);var_dump("<br/>");
		var_dump($this->mail_config["smtp_port"]);var_dump("<br/>");
		var_dump($this->mail_config["smtp_timeou"]);var_dump("<br/>");
		var_dump($this->mail_config["wordwrap"]);var_dump("<br/>");
		var_dump($this->mail_config["wrapchars"]);var_dump("<br/>");
		var_dump($this->mail_config["mailtype"]);var_dump("<br/>");
		var_dump($this->mail_config["charset"]);var_dump("<br/>");
		var_dump($this->mail_config["validate"]);var_dump("<br/>");
		var_dump($this->mail_config["priority"]);var_dump("<br/>");
		var_dump($this->mail_config["newline"]);var_dump("<br/>");
		var_dump($this->mail_config["bcc_batch_mode"]);var_dump("<br/>");
		var_dump($this->mail_config["bcc_batch_size"]);var_dump("<br/>");
		exit;
		*/
		$this->mail_config["charset"] = $this->ci->config->item("charset");
	}

	function setFrom($fromMail, $fromName) {
		$this->ci->email->from($fromMail, $this->mail_encode($fromName, $this->mail_config["charset"]));
	}

	function setTo($toMail) {
		/*
		var_dump($toMail);
		exit;
		*/
		$this->ci->email->to($toMail);
	}

	function setCc($ccMail) {
		$this->ci->email($ccMail);
	}

	function setInit() {
		$this->ci->email->initialize($this->mail_config);
	}

	function send() {

		if (!$this->ci->email->send())
			return FALSE;
		else
			return TRUE;
	}

	function setAttach($path) {
		$this->ci->email->attach($path);
	}

	function clear($bool = NULL) {
		$this->ci->email->clear($bool);
	}

	function setBcc($bccMail) {
		$this->ci->email($bccMail);
	}

	function setSubject($subject) {
		//$this->ci->email->subject($this->mail_encode($subject, $this->mail_config["charset"]));
		$this->ci->email->subject($subject);
	}

	function setMessage($message) {
		$this->ci->email->message($message);
	}

	function setAltMessage($message) {
		$this->ci->email->set_alt_message($message);
	}

	function setUseragent($value) {
		$this->mail_config["useragent"] = $value;
	}

	function setProtocol($value) {
		$this->mail_config["protocol"] = $value;
	}

	function setMailpath($value) {
		$this->mail_config["mailpath"] = $value;
	}

	function setSmtp_host($value) {
		$this->mail_config["smtp_host"] = $value;
	}

	function setSmtp_user($value) {
		$this->mail_config["smtp_user"] = $value;
	}

	function setSmtp_pass($value) {
		$this->mail_config["smtp_pass"] = $value;
	}

	function setSmtp_port($value) {
		$this->mail_config["smtp_port"] = $value;
	}

	function setSmtp_timeou($value) {
		$this->mail_config["smtp_timeou"] = $value;
	}

	function setWordwrap($value) {
		$this->mail_config["wordwrap"] = $value;
	}

	function setWrapchars($value) {
		$this->mail_config["wrapchars"] = $value;
	}

	function setMailtype($value) {
		$this->mail_config["mailtype"] = $value;
	}

	function setCharset($value) {
		$this->mail_config["charset"] = $value;
	}

	function setValidate($value) {
		$this->mail_config["validate"] = $value;
	}

	function setPriority($value) {
		$this->mail_config["priority"] = $value;
	}

	function setNewline($value) {
		$this->mail_config["newline"] = $value;
	}

	function setBccbatchmode($value) {
		$this->mail_config["bcc_batch_mode"] = $value;
	}

	function setBccbatchsize($value) {
		$this->mail_config["bcc_batch_size"] = $value;
	}

	function printDebugger() {
		return $this->ci->email->print_debugger();
	}

	function mail_encode($in_str, $charset) {
		$out_str = $in_str;
		if ($out_str && $charset) {
		// define start delimimter, end delimiter and spacer
			$end = "?=";
			$start = "=?" . $charset . "?B?";
			$spacer = $end . "\r\n " . $start;

			// determine length of encoded text within chunks
			// and ensure length is even
			$length = 1000 - strlen($start) - strlen($end);
			$length = floor($length / 2) * 2;

			// encode the string and split it into chunks
			// with spacers after each chunk
			$out_str = base64_encode($out_str);
			$out_str = chunk_split($out_str, $length, $spacer);

			// remove trailing spacer and
			// add start and end delimiters
			$spacer = preg_quote($spacer);
			$out_str = preg_replace("/" . $spacer . "$/", "", $out_str);
			$out_str = $start . $out_str . $end;
		}
		return $out_str;
	}
}
?>
