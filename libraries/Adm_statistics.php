<?php



if (!defined('BASEPATH'))

	exit('No direct script access allowed');



/* ------------------------------------------------

 * 파일명 : Adm_statistics.php

 * 개  요 : 접속통계관리

  ------------------------------------------------ */



class Adm_statistics {



	var $ci;



//constructor

	function __construct() {

//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당

		$this->ci = &get_instance();



		$this->ci->load->model('mo_statistics', 'statistics');

		$this->ci->load->plugin('jpgraph');



		func_del_chart_file(); //차트 파일 삭제

	}



//년간 접속 통계

	function view_statistics_year() {

//년도 그룹

		$year_group_result = $this->ci->statistics->select_year_group();

		func_set_data($this->ci, 'year_group', $year_group_result);



//검색조건

		$sel_year = $this->ci->input->get_post('sel_year');

		if ($sel_year == '') {

			if (count($year_group_result) > 0) {

				$sel_year = $year_group_result[0]->visit_year;

			}

		}



		func_set_data($this->ci, 'sel_year', $sel_year);



//타이틀 셋팅

		func_set_data($this->ci, 'statistics_title', $sel_year . '년 사이트 접속 통계');



//통계조회

		$select_year_result = $this->ci->statistics->select_year($sel_year);

		func_set_data($this->ci, 'select_year_result', $select_year_result);



		if (count($select_year_result) > 0) {

//값세팅

			$xdata = NULL;

			$ydata = NULL;



			for ($i = 0; $i < count($select_year_result); $i++) {

				$xdata[$i] = $select_year_result[$i]->visit_month;

				$ydata[$i] = $select_year_result[$i]->cnt;

			}



//차트이미지 저장 경로

			$graph_file_location = func_get_chart_filepath();



			$graph = statistics($xdata, $ydata, "MONTH");  // add more parameters to plugin function as required

//차트 생성

			$graph->Stroke('./' . $graph_file_location);  // create the graph and write to file

			func_set_data($this->ci, 'graph', $graph_file_location);





//파이차트이미지 저장경로

			$graph_pie_file_location = func_get_chart_filepath('pie');



//파이차트 생성

			$graph_pie = statisticsPie($ydata, $xdata, "MONTH");  // add more parameters to plugin function as required

//파이차트 생성

			$graph_pie->Stroke('./' . $graph_pie_file_location);  // create the graph and write to file

			func_set_data($this->ci, 'graph_pie', $graph_pie_file_location);

		}



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/statistics/adm_statistics_year', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}
//월간 접속 통계

	function view_statistics_month() {

//월간 그룹

		$month_group_result = $this->ci->statistics->select_month_group();

		func_set_data($this->ci, 'month_group', $month_group_result);



//검색조건

		$sel_month = $this->ci->input->get_post('sel_month');

		if ($sel_month == '') {

			if (count($month_group_result) > 0) {

				$sel_month = array($month_group_result[0]->visit_year, $month_group_result[0]->visit_month);

			}

		} else {

			$sel_month = explode("|", $sel_month);

		}



		func_set_data($this->ci, 'sel_month', $sel_month[0] . '|' . $sel_month[1]);



//타이틀 셋팅

		func_set_data($this->ci, 'statistics_title', $sel_month[0] . '년 ' . $sel_month[1] . '월 사이트 접속 통계');



//통계조회

		$select_month_result = $this->ci->statistics->select_month($sel_month[0], $sel_month[1]);



		func_set_data($this->ci, 'select_month_result', $select_month_result);



		if (count($select_month_result) > 0) {

//값세팅

			$xdata = NULL;

			$ydata = NULL;



			for ($i = 0; $i < count($select_month_result); $i++) {

				$xdata[$i] = $select_month_result[$i]->visit_day;

				$ydata[$i] = $select_month_result[$i]->cnt;

			}



//차트이미지 저장 경로

			$graph_file_location = func_get_chart_filepath();



			$graph = statistics($xdata, $ydata, "DAY");  // add more parameters to plugin function as required

//차트 생성

			$graph->Stroke('./' . $graph_file_location);  // create the graph and write to file

			func_set_data($this->ci, 'graph', $graph_file_location);





//파이차트이미지 저장경로

			$graph_pie_file_location = func_get_chart_filepath('pie');



//파이차트 생성

			$graph_pie = statisticsPie($ydata, $xdata, "DAY");  // add more parameters to plugin function as required

//파이차트 생성

			$graph_pie->Stroke('./' . $graph_pie_file_location);  // create the graph and write to file

			func_set_data($this->ci, 'graph_pie', $graph_pie_file_location);

		}



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/statistics/adm_statistics_month', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}

  
     function adm_vistitors()
	 {
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/statistics/adm_vistitors', $this->ci->data, true));
		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	 }
	 



//일별 접속 통계

	function view_statistics_day() {

//검색조건

		$sel_sdate = $this->ci->input->get_post('sel_sdate');

		$sel_edate = $this->ci->input->get_post('sel_edate');



		if ($sel_sdate == '' || $sel_edate == '') {

//가장 최근값 조회

			$day_group_result = $this->ci->statistics->select_time_group();

			if (count($day_group_result) > 0) {

				$sel_sdate = $day_group_result[0]->visit_year . '-' . $day_group_result[0]->visit_month . '-' . $day_group_result[0]->visit_day;

				$sel_edate = $day_group_result[0]->visit_year . '-' . $day_group_result[0]->visit_month . '-' . $day_group_result[0]->visit_day;

			} else {

				$sel_sdate = date('Y-m-d');

				$sel_edate = date('Y-m-d');

			}

		}



		func_set_data($this->ci, 'sel_sdate', $sel_sdate);

		func_set_data($this->ci, 'sel_edate', $sel_edate);



//타이틀 세팅

		func_set_data($this->ci, 'statistics_title', $sel_sdate . " ~ " . $sel_edate . " 기간의 시간대별 사이트 접속 통계");



//통계조회

		$select_day_result = $this->ci->statistics->select_time($sel_sdate, $sel_edate);



		func_set_data($this->ci, 'select_day_result', $select_day_result);



		if (count($select_day_result) > 0) {

//값세팅

			$xdata = NULL;

			$ydata = NULL;



			for ($i = 0; $i < count($select_day_result); $i++) {

				$xdata[$i] = $select_day_result[$i]->visit_time;

				$ydata[$i] = $select_day_result[$i]->cnt;

			}



//차트이미지 저장 경로

			$graph_file_location = func_get_chart_filepath();



			$graph = statistics($xdata, $ydata, "HOUR");  // add more parameters to plugin function as required

//차트 생성

			$graph->Stroke('./' . $graph_file_location);  // create the graph and write to file

			func_set_data($this->ci, 'graph', $graph_file_location);





//파이차트이미지 저장경로

			$graph_pie_file_location = func_get_chart_filepath('pie');



//파이차트 생성

			$graph_pie = statisticsPie($ydata, $xdata, "HOUR");  // add more parameters to plugin function as required

//파이차트 생성

			$graph_pie->Stroke('./' . $graph_pie_file_location);  // create the graph and write to file

			func_set_data($this->ci, 'graph_pie', $graph_pie_file_location);

		}



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/statistics/adm_statistics_day', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}



//메뉴별 접속 통계

	function view_statistics_menu() {

//검색조건

		$sel_sdate = $this->ci->input->get_post('sel_sdate');

		$sel_edate = $this->ci->input->get_post('sel_edate');



		if ($sel_sdate == '' || $sel_edate == '') {

//가장 최근값 조회

			$day_group_result = $this->ci->statistics->select_time_group();

			if (count($day_group_result) > 0) {

				$sel_sdate = $day_group_result[0]->visit_year . '-' . $day_group_result[0]->visit_month . '-' . $day_group_result[0]->visit_day;

				$sel_edate = $day_group_result[0]->visit_year . '-' . $day_group_result[0]->visit_month . '-' . $day_group_result[0]->visit_day;

			} else {

				$sel_sdate = date('Y-m-d');

				$sel_edate = date('Y-m-d');

			}

		}



		func_set_data($this->ci, 'sel_sdate', $sel_sdate);

		func_set_data($this->ci, 'sel_edate', $sel_edate);



//타이틀 세팅

		func_set_data($this->ci, 'statistics_title', $sel_sdate . " ~ " . $sel_edate . " 기간의 메뉴별 사이트 접속 통계");



//통계조회

		$select_menu_result = $this->ci->statistics->select_menu($sel_sdate, $sel_edate);



		func_set_data($this->ci, 'select_menu_result', $select_menu_result);



		if (count($select_menu_result) > 0) {

//값세팅

			$xdata = NULL;

			$ydata = NULL;



			for ($i = 0; $i < count($select_menu_result); $i++) {

				$xdata[$i] = iconv('UTF-8', 'EUC-KR', $select_menu_result[$i]->menu_code_name);

				$ydata[$i] = $select_menu_result[$i]->cnt;

			}



//차트이미지 저장 경로

			$graph_file_location = func_get_chart_filepath();



			$graph = menuSatistics($xdata, $ydata);  // add more parameters to plugin function as required

//차트 생성

			$graph->Stroke('./' . $graph_file_location);  // create the graph and write to file

			func_set_data($this->ci, 'graph', $graph_file_location);



			/*

			  //파이차트이미지 저장경로

			  $graph_pie_file_location = func_get_chart_filepath('pie');



			  //파이차트 생성

			  $graph_pie = statisticsPie($ydata, $xdata, "HOUR");  // add more parameters to plugin function as required



			  //파이차트 생성

			  $graph_pie->Stroke('./'.$graph_pie_file_location);  // create the graph and write to file

			  func_set_data($this->ci, 'graph_pie', $graph_pie_file_location);

			 */

		}



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/statistics/adm_statistics_menu', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}



//접근경로 통계

	function view_statistics_referer() {

//검색조건

		$sel_sdate = $this->ci->input->get_post('sel_sdate');

		$sel_edate = $this->ci->input->get_post('sel_edate');



		if ($sel_sdate == '' || $sel_edate == '') {

//가장 최근값 조회

			$day_group_result = $this->ci->statistics->select_time_group();

			if (count($day_group_result) > 0) {

				$sel_sdate = $day_group_result[0]->visit_year . '-' . $day_group_result[0]->visit_month . '-' . $day_group_result[0]->visit_day;

				$sel_edate = $day_group_result[0]->visit_year . '-' . $day_group_result[0]->visit_month . '-' . $day_group_result[0]->visit_day;

			} else {

				$sel_sdate = date('Y-m-d');

				$sel_edate = date('Y-m-d');

			}

		}



		func_set_data($this->ci, 'sel_sdate', $sel_sdate);

		func_set_data($this->ci, 'sel_edate', $sel_edate);



//타이틀 세팅

		func_set_data($this->ci, 'statistics_title', $sel_sdate . " ~ " . $sel_edate . " 기간의 접근경로 통계");



//통계조회

		$select_referer_result = $this->ci->statistics->select_referer($sel_sdate, $sel_edate);



		func_set_data($this->ci, 'select_referer_result', $select_referer_result);



		if (count($select_referer_result) > 0) {

//값세팅

			$xdata = NULL;

			$ydata = NULL;



			for ($i = 0; $i < count($select_referer_result); $i++) {

				$xdata[$i] = iconv('UTF-8', 'EUC-KR', $select_referer_result[$i]->referer);

				$ydata[$i] = $select_referer_result[$i]->cnt;

			}



//차트이미지 저장 경로

			$graph_file_location = func_get_chart_filepath();



			$graph = refererSatistics($xdata, $ydata);  // add more parameters to plugin function as required

//차트 생성

			$graph->Stroke('./' . $graph_file_location);  // create the graph and write to file

			func_set_data($this->ci, 'graph', $graph_file_location);



			/*

			  //파이차트이미지 저장경로

			  $graph_pie_file_location = func_get_chart_filepath('pie');



			  //파이차트 생성

			  $graph_pie = statisticsPie($ydata, $xdata, "HOUR");  // add more parameters to plugin function as required



			  //파이차트 생성

			  $graph_pie->Stroke('./'.$graph_pie_file_location);  // create the graph and write to file

			  func_set_data($this->ci, 'graph_pie', $graph_pie_file_location);

			 */

		}



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/statistics/adm_statistics_referer', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}



//검색키워드 통계

	function view_statistics_keyword() {

//검색엔진

		$search_site = $this->ci->input->get_post('search_site');

//검색엔진명

		$search_site_name = '';



		switch ($search_site) {

			case "naver":

				$search_site_name = '네이버';

				break;

			case "daum":

				$search_site_name = '다음';

				break;

			case "nate":

				$search_site_name = '네이트';

				break;

			case "yahoo":

				$search_site_name = '야후';

				break;

			case "google":

				$search_site_name = '구글';

				break;

			case "paran":

				$search_site_name = '파란';

				break;

			default:

				$search_site_name = '전체';

				break;

		}

		func_set_data($this->ci, 'search_site', $search_site);



//검색조건

		$sel_sdate = $this->ci->input->get_post('sel_sdate');

		$sel_edate = $this->ci->input->get_post('sel_edate');



		if ($sel_sdate == '' || $sel_edate == '') {

//가장 최근값 조회

			$day_group_result = $this->ci->statistics->select_time_group();

			if (count($day_group_result) > 0) {

				$sel_sdate = $day_group_result[0]->visit_year . '-' . $day_group_result[0]->visit_month . '-' . $day_group_result[0]->visit_day;

				$sel_edate = $day_group_result[0]->visit_year . '-' . $day_group_result[0]->visit_month . '-' . $day_group_result[0]->visit_day;

			} else {

				$sel_sdate = date('Y-m-d');

				$sel_edate = date('Y-m-d');

			}

		}



		func_set_data($this->ci, 'sel_sdate', $sel_sdate);

		func_set_data($this->ci, 'sel_edate', $sel_edate);



//타이틀 세팅

		func_set_data($this->ci, 'statistics_title', '"' . $search_site_name . '" 검색 키워드 조회 결과입니다.');



//통계조회

		$select_keyword_result = $this->ci->statistics->select_keyword($sel_sdate, $sel_edate, $search_site);



		func_set_data($this->ci, 'select_keyword_result', $select_keyword_result);



		if (count($select_keyword_result) > 0) {

//값세팅

			$xdata = NULL;

			$ydata = NULL;



			for ($i = 0; $i < count($select_keyword_result); $i++) {

				$xdata[$i] = iconv('UTF-8', 'EUC-KR', $select_keyword_result[$i]->keyword);

				$ydata[$i] = $select_keyword_result[$i]->keyword_cnt;

			}



//차트이미지 저장 경로

			$graph_file_location = func_get_chart_filepath();



			$graph = keywordSatistics($xdata, $ydata);  // add more parameters to plugin function as required

//차트 생성

			$graph->Stroke('./' . $graph_file_location);  // create the graph and write to file

			func_set_data($this->ci, 'graph', $graph_file_location);



			/*

			  //파이차트이미지 저장경로

			  $graph_pie_file_location = func_get_chart_filepath('pie');



			  //파이차트 생성

			  $graph_pie = statisticsPie($ydata, $xdata, "HOUR");  // add more parameters to plugin function as required



			  //파이차트 생성

			  $graph_pie->Stroke('./'.$graph_pie_file_location);  // create the graph and write to file

			  func_set_data($this->ci, 'graph_pie', $graph_pie_file_location);

			 */

		}



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/statistics/adm_statistics_keyword', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}



}

?>