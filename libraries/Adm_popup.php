<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Adm_popup.php
 * 개  요 : 관리자 팝업 Class
  ------------------------------------------------ */

class Adm_popup {

	var $ci;
	var $mcd;
	var $bbs_config;

//constructor
	function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();
	}

//팝업목록 조회
	function adm_popup_list() {
		$where = array(); //sql where 조건
		$sParam = '';	  //검색 string
//검색 조건
		$sch_create_dt_s = $this->ci->input->get_post('sch_create_dt_s'); //등록일 시작
		if ($sch_create_dt_s != '') {
			$sParam .= '&sch_create_dt_s=' . $sch_create_dt_s;
			$where['created_dt >='] = $sch_create_dt_s . " 00:00:01";
		}
		$sch_create_dt_e = $this->ci->input->get_post('sch_create_dt_e'); //등록일 종료
		if ($sch_create_dt_e != '') {
			$sParam .= '&sch_create_dt_e=' . $sch_create_dt_e;
			$where['created_dt <='] = $sch_create_dt_e . " 23:59:59";
		}

		$sch_condition = $this->ci->input->get_post('sch_condition');   //검색조건
		$sch_word = $this->ci->input->get_post('sch_word');		//검색어
//검색조건 + 검색어
		if ($sch_condition != '' && $sch_word != '') {
			$sch_condition_tmp = explode('|', $sch_condition);

//다중 조건인 경우(제목 + 내용)
			if (count($sch_condition_tmp) > 1) {
				$tmpStr = '';
				for ($i = 0; $i < count($sch_condition_tmp); $i++) {
					if ($tmpStr != '') {
						$tmpStr .= ' or ';
					}

					$tmpStr .= $sch_condition_tmp[$i] . ' ? ';
					$where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';
				}

				$tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';
				$where[$tmpStr] = '';

				$sParam .= '&sch_condition=' . $sch_condition;
				$sParam .= '&sch_word=' . $sch_word;
			} else {
				$where[$sch_condition] = '%' . $sch_word . '%';
				$sParam .= '&sch_condition=' . $sch_condition;
				$sParam .= '&sch_word=' . $sch_word;
			}
		}

//페이지 조건
		$cur_page = $this->ci->input->get_post("cur_page");
		if ($cur_page == NULL || $cur_page == '')
			$cur_page = 1;
		$page_rows = 10; //페이지 로우수
		$page_views = 10; //페이지 네비게이션 수
//팝업 목록 조회
		$bbs_list = $this->ci->common->select_popup(($cur_page - 1) * $page_rows, $page_rows, $where);

		$total_rows = $this->ci->common->total_select_popup($where); //조회 전체 목록수
		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	//글 번호
//페이지정보 조회
		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);


		func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수
		func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지
		func_set_data($this->ci, 'row_cnt', $row_cnt);		 //글번호
		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지
		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string
		func_set_data($this->ci, 'popup_list', $bbs_list->result());
		func_set_data($this->ci, 'page_info', $page_info);
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/popup/adm_popup_list', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}
	
	
	//vistitor
	  function adm_popup_list_visitor() {

		$where = array(); //sql where 조건

		$sParam = '';	  //검색 string

//검색 조건



		$sch_condition = $this->ci->input->get_post('sch_condition');   //검색조건

		$sch_word = $this->ci->input->get_post('sch_word');		//검색어

//검색조건 + 검색어

		if ($sch_condition != '' && $sch_word != '') {

			$sch_condition_tmp = explode('|', $sch_condition);



//다중 조건인 경우(제목 + 내용)

			if (count($sch_condition_tmp) > 1) {

				$tmpStr = '';

				for ($i = 0; $i < count($sch_condition_tmp); $i++) {

					if ($tmpStr != '') {

						$tmpStr .= ' or ';

					}



					$tmpStr .= $sch_condition_tmp[$i] . ' ? ';

					$where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';

				}



				$tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

				$where[$tmpStr] = '';



				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			} else {

				$where[$sch_condition] = '%' . $sch_word . '%';

				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			}

		}



//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 10; //페이지 로우수

		$page_views = 10; //페이지 네비게이션 수

//팝업 목록 조회

		$bbs_list = $this->ci->common->select_visitors(($cur_page - 1) * $page_rows, $page_rows, $where);



		$total_rows = $this->ci->common->total_select_visitor($where); //조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	//글 번호

//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);





		func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 //글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string

		func_set_data($this->ci, 'popup_list', $bbs_list->result());

		func_set_data($this->ci, 'page_info', $page_info);

 func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/popup/adm_popup_list_visitor', $this->ci->data, true));
		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}
	
	
	//end visitor



//팝업 상세 조회
	function adm_popup_detail() {
		$idx = $this->ci->input->get_post('idx');
		$cur_page = $this->ci->input->get_post('cur_page');
		if ($cur_page == NULL || $cur_page == '')
			$cur_page = 1;
		$sParam = $this->ci->input->get_post('sParam');		//검색 string

		$popup_detail_list = $this->ci->common->select_detail_popup($idx); //상세 조회

		func_set_data($this->ci, 'idx', $idx);
		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지
		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string
		func_set_data($this->ci, 'popup_detail_list', $popup_detail_list);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/popup/adm_popup_detail', $this->ci->data, true));
		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}

//팝업 등록 HTML
	function adm_popup_write() {
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/popup/adm_popup_write', $this->ci->data, true));
		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}

//팝업 등록 처리
	function adm_popup_write_exec() {
		$inData = array();
		$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴
		$inData['created_dt'] = func_get_date_time();

//DB 등록
		$idx = $this->ci->common->insert_popup($inData);

		echo func_jsAlertReplace('등록되었습니다.', '/?c=admin&m=adm_popup_list');
	}

//팝업 수정
	function adm_popup_update() {
		$cur_page = $this->ci->input->get_post('cur_page');
		$sParam = $this->ci->input->get_post('sParam');	  //검색 string

		$inData = array();
		$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴
		$inData['idx'] = $this->ci->input->get_post('idx');		//게시물 일련번호
//DB 수정
		$this->ci->common->update_popup($inData);

		echo func_jsAlertReplace('Modification Complete.', '/?c=admin&m=adm_popup_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
	}

//팝업 삭제
	function adm_popup_delete() {
		$idx = $this->ci->input->get_post('idx');
		$cur_page = $this->ci->input->get_post('cur_page');
		$sParam = $this->ci->input->get_post('sParam');   //검색 string

		$this->ci->common->delete_popup($idx); //삭제

		echo func_jsAlertReplace('삭제되었습니다.', '/?c=admin&m=adm_popup_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
	}

//팝업 미리 보기
	function adm_popup_preview() {
		$idx = $this->ci->input->get_post('idx');
		$popup_detail_list = $this->ci->common->select_detail_popup($idx); //상세 조회

		func_set_data($this->ci, 'idx', $idx);
		func_set_data($this->ci, 'popup_detail_list', $popup_detail_list);

		$this->ci->load->view('common/popup', $this->ci->data, false);
	}

	function write_color_popup() {
		
		$this->ci->load->view('admin/adm/bbs/product/write_color_popup', $this->ci->data, false);
	}
	function modify_color_popup() {
		
		$this->ci->load->view('admin/adm/bbs/product/modify_color_popup', $this->ci->data, false);
	}
        
      //   function adm_add_premium(){
      //       $premium_list = $this->ci->common->select_premium_list(); 	  
	    	// func_set_data($this->ci, 'premium_list',$premium_list);
      //       $this->
      //       //$this->ci->load->view('admin/adm_add_premium', $this->ci->data, false);
      //   }
                
        function sender(){
              $this->ci->load->view('admin/adm/customer/sender', $this->ci->data, false);
        }
                function order_this_car_popup_checklogin() {

        $this->ci->load->view('user/bbs/product/order_this_car_popup_checklogin', $this->ci->data, false);
    }

    function order_this_car_popup() {

        // $bbs_detail_list = $this->ci->user_bbs->select_detail($idx, $this->mcd); //상세 조회
        return $this->ci->load->view('user/bbs/product/order_this_car_popup', $this->ci->data, false);
    }
}
?>