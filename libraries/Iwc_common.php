<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Iwc_common.php
 * 개  요 : IWC 공통 클래스
  ------------------------------------------------ */

class Iwc_common {

	var $ci;
	var $upload_error_msg;
	var $upload_success_msg;

	//constructor
	function __construct() {
		//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();

		$this->ci->load->library('user_agent');
		$this->ci->load->library('upload');		  //파일업로드
		$this->ci->load->library('image_lib', NULL); //이미지
		$this->ci->load->helper('file');			 //파일
		$this->ci->load->helper('download');		 //다운로드
		$this->ci->load->helper('directory');		//디렉토리
		$this->ci->load->helper('url');			  //URL
		$this->ci->load->helper('string');		   //문자
		$this->ci->load->helper('date');			 //날짜
		$this->ci->load->helper('cookie');		   //쿠키
	}

	//파일 업로드 에러 메세지
	function upload_error_msg() {
		return $this->upload_error_msg;
	}

	//파일 업로드 성공시 파일 정보 메세지(파일정보)
	function upload_success_msg() {
		return $this->upload_success_msg;
	}

	//파일 업로드
	function upload($field, $i, $mcd, $conf = NULL, $encrypt_name = TRUE) {
		$return_str = '';
		$config = array();
		//$config['upload_path']   = APPPATH.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$mcd;//메뉴코드별 업로드 디렉토리 생성
		$config['upload_path'] = $this->ci->config->item('upload_path') . DIRECTORY_SEPARATOR . $mcd; //메뉴코드별 업로드 디렉토리 생성
		//첨부파일 허용 확장자
		if ($conf != NULL) {
			$config['allowed_types'] = func_get_config($conf, 'attach_file_allow');
		} else {
			$config['allowed_types'] = 'doc|xls|ppt|hwp|docx|xlsx|pptx|pdf|zip|txt|jpg|gif|png';
		}


		//첨부파일 용량
		$config['max_size'] = $this->ci->config->item('upload_max_size');
		$config['encrypt_name'] = $encrypt_name;

		//디렉토리 생성
		$this->make_dir($config['upload_path']);

		$this->ci->upload->initialize($config);

		//업로드 실패
		if (!$this->ci->upload->do_upload($field, $i)) {
			$this->upload_error_msg = $this->ci->upload->display_errors();

			return false;
		}
		//업로드 성공
		else {
			//업로드 파일 정보
			if($mcd =='documents'){
				$this->upload_success_msg = $this->ci->upload->data_upload();
				
			}elseif($mcd =='carItem'){
                            
                                $this->upload_success_msg = $this->ci->upload->car_upload();
                                
                        }else{
				$this->upload_success_msg = $this->ci->upload->data();
			}

			//파일 사이즈(default KByte)
			if (((integer) $this->upload_success_msg['file_size']) > 1024) {
				$this->upload_success_msg['file_size'] = round(((integer) $this->upload_success_msg['file_size']) / 1024, 1) . "(Mb)";
			} else {
				$this->upload_success_msg['file_size'] = round(((integer) $this->upload_success_msg['file_size']), 1) . "(Kb)";
			}


			//이미지를 업로드 한경우
                        if($mcd !='carItem'){
			if ($this->upload_success_msg['is_image'] != FALSE) {

				//워터마크를 생성하는 경우
				if (func_get_config($conf, 'attach_watermaking_yn') != 'N') {
					$this->make_text_watermarking($this->upload_success_msg['full_path'], func_get_config($conf, 'attach_watermaking_text'), func_get_config($conf, 'attach_watermaking_font_color'), func_get_config($conf, 'attach_watermaking_font_size'));
				}

				//썸네일을 생성하는 경우
				if (func_get_config($conf, 'attach_thumbnail_yn') != 'N') {
					$cnt = 0;
					$thumbnails = array();
					if (func_get_config($conf, 'attach_thumbnail_size1') != '') {
						$thumbnails[$cnt] = func_get_config($conf, 'attach_thumbnail_size1');
						$cnt++;
					}
					if (func_get_config($conf, 'attach_thumbnail_size2') != '') {
						$thumbnails[$cnt] = func_get_config($conf, 'attach_thumbnail_size2');
						$cnt++;
					}
					if (func_get_config($conf, 'attach_thumbnail_size3') != '') {
						$thumbnails[$cnt] = func_get_config($conf, 'attach_thumbnail_size3');
						$cnt++;
					}
					if (func_get_config($conf, 'attach_thumbnail_size4') != '') {
						$thumbnails[$cnt] = func_get_config($conf, 'attach_thumbnail_size4');
						$cnt++;
					}
					if (func_get_config($conf, 'attach_thumbnail_size5') != '') {
						$thumbnails[$cnt] = func_get_config($conf, 'attach_thumbnail_size5');
						$cnt++;
					}
					if ($cnt > 0) {
						for ($j = 0; $j < $cnt; $j++) {
							$this->make_thumbnail($this->upload_success_msg['file_path'], $this->upload_success_msg['raw_name'], $this->upload_success_msg['file_ext'], $thumbnails[$j], $conf);
						}
					}
				}
			}
                           
                        }
			return true;
		}
	}

	//멀티 파일 업로드
	function upload_multiple($field, $mcd, $conf = NULL, $encrypt_name = TRUE) {
		$return_str = '';
		$config = array();
		//$config['upload_path']   = APPPATH.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$mcd;//메뉴코드별 업로드 디렉토리 생성
		$config['upload_path'] = $this->ci->config->item('upload_path') . DIRECTORY_SEPARATOR . $mcd; //메뉴코드별 업로드 디렉토리 생성
		//첨부파일 허용 확장자
		if ($conf != NULL) {
			$config['allowed_types'] = func_get_config($conf, 'attach_file_allow');
		} else {
			$config['allowed_types'] = 'doc|xls|ppt|hwp|docx|xlsx|pptx|pdf|zip|txt|jpg|gif|png';
		}


		//첨부파일 용량
		$config['max_size'] = $this->ci->config->item('upload_max_size');
		$config['encrypt_name'] = $encrypt_name;

		//디렉토리 생성
		$this->make_dir($config['upload_path']);

		$this->ci->upload->initialize($config);

		//업로드 실패
		if (!$this->ci->upload->do_upload_multiple($field)) {
			$this->upload_error_msg = $this->ci->upload->display_errors();

			return false;
		}
		//업로드 성공
		else {
			//업로드 파일 정보
			$this->upload_success_msg = $this->ci->upload->data();

			//파일 사이즈(default KByte)
			if (((integer) $this->upload_success_msg['file_size']) > 1024) {
				$this->upload_success_msg['file_size'] = round(((integer) $this->upload_success_msg['file_size']) / 1024, 1) . "(Mb)";
			} else {
				$this->upload_success_msg['file_size'] = round(((integer) $this->upload_success_msg['file_size']), 1) . "(Kb)";
			}


			//이미지를 업로드 한경우
			if ($this->upload_success_msg['is_image'] != FALSE) {

				//워터마크를 생성하는 경우
				if (func_get_config($conf, 'attach_watermaking_yn') != 'N') {
					$this->make_text_watermarking($this->upload_success_msg['full_path'], func_get_config($conf, 'attach_watermaking_text'), func_get_config($conf, 'attach_watermaking_font_color'), func_get_config($conf, 'attach_watermaking_font_size'));
				}

				//썸네일을 생성하는 경우
				if (func_get_config($conf, 'attach_thumbnail_yn') != 'N') {
					$cnt = 0;
					$thumbnails = array();
					if (func_get_config($conf, 'attach_thumbnail_size1') != '') {
						$thumbnails[$cnt] = func_get_config($conf, 'attach_thumbnail_size1');
						$cnt++;
					}
					if (func_get_config($conf, 'attach_thumbnail_size2') != '') {
						$thumbnails[$cnt] = func_get_config($conf, 'attach_thumbnail_size2');
						$cnt++;
					}
					if (func_get_config($conf, 'attach_thumbnail_size3') != '') {
						$thumbnails[$cnt] = func_get_config($conf, 'attach_thumbnail_size3');
						$cnt++;
					}
					if (func_get_config($conf, 'attach_thumbnail_size4') != '') {
						$thumbnails[$cnt] = func_get_config($conf, 'attach_thumbnail_size4');
						$cnt++;
					}
					if (func_get_config($conf, 'attach_thumbnail_size5') != '') {
						$thumbnails[$cnt] = func_get_config($conf, 'attach_thumbnail_size5');
						$cnt++;
					}
					if ($cnt > 0) {
						for ($i = 0; $i < $cnt; $i++) {
							$this->make_thumbnail($this->upload_success_msg['file_path'], $this->upload_success_msg['raw_name'], $this->upload_success_msg['file_ext'], $thumbnails[$i], $conf);
						}
					}
				}
			}

			return true;
		}
	}

	//파일 업로드 image
	function upload_image($field, $mcd) {
		$return_str = '';
		$config = array();
		$config['upload_path'] = $this->ci->config->item('upload_path') . DIRECTORY_SEPARATOR . $mcd; //메뉴코드별 업로드 디렉토리 생성
		$config['allowed_types'] = 'gif|jpg|png';												   //첨부파일 허용 확장자
		$config['max_size'] = 0;															   //첨부파일 용량
		$config['encrypt_name'] = TRUE;

		//디렉토리 생성
		$this->make_dir($config['upload_path']);

		$this->ci->upload->initialize($config);

		//업로드 실패
		if (!$this->ci->upload->do_upload($field)) {
			$this->upload_error_msg = $this->ci->upload->display_errors();

			return false;
		}
		//업로드 성공
		else {
			//업로드 파일 정보
			$this->upload_success_msg = $this->ci->upload->data();

			return true;
		}
	}

	//로컬에 동일한 디렉토리를 만들어주는 함수
	function make_dir($path) {
		if (!(is_dir($path) )) {
			$path_temp = explode("/", $path);
			$path_str = '';
			for ($i = 0; $i < count($path_temp); $i++) {
				$path_str .= trim($path_temp[$i]) . "/";

				if (!(is_dir($path_str) )) {
					mkdir($path_str, 0777);
					chmod($path_str, 0777);
				}
			}
		}
	}

	//썸네일 생성
	function make_thumbnail($file_path, $raw_name, $file_ext, $width, $conf) {
		$full_path = $file_path . $raw_name . $file_ext;
		$prop = $this->ci->image_lib->get_image_properties($full_path, TRUE);

		//원본이 제작하려고 하는 섬네일보다 큰경우만 실행 한다.
		if (((integer) $prop['width']) > $width) {
			$config['image_library'] = 'GD2';
			$config['source_image'] = $full_path;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = FALSE;
			$config['thumb_marker'] = '_' . $width;
			$config['quality'] = '100%';
			$config['master_dim'] = 'width';
			$config['width'] = $width;
			$config['height'] = $width;

			$this->ci->image_lib->clear();			 // clear settings
			$this->ci->image_lib->initialize($config); // reinitialize
			$this->ci->image_lib->resize();

			echo $this->ci->image_lib->display_errors();
		}
	}

	//워터마크(TEXT) 생성
	function make_text_watermarking($file_path, $wm_text, $wm_font_color = 'ffffff', $wm_font_size = '16', $wm_font_path = '', $wm_shadow_color = '000000', $wm_shadow_distance = '1', $wm_vrt_alignment = 'middle', $wm_hor_alignment = 'center', $wm_padding = '10') {
		$wm_config = array();

		$wm_config['image_library'] = 'GD2';
		$wm_config['source_image'] = $file_path;
		$wm_config['quality'] = '100%';
		$wm_config['wm_text'] = $wm_text;
		$wm_config['wm_type'] = 'text';
		$wm_config['wm_font_path'] = '/usr/share/fonts/korean/TrueType/dotum.ttf';		 //사용할 트루타입폰트의 서버경로를 설정합니다.이 옵션을 설정하지 않으시면 GD에 내장된 폰트가 사용됩니다.
		$wm_config['wm_font_size'] = $wm_font_size;
		$wm_config['wm_font_color'] = $wm_font_color;
		$wm_config['wm_shadow_color'] = $wm_shadow_color;	  //그림자의 색상을 16진수로 설정합니다.
		$wm_config['wm_shadow_distance'] = $wm_shadow_distance;   //그림자가 얼마나 떨어져서 표시될지를 픽셀단위로 지정합니다.
		$wm_config['wm_vrt_alignment'] = $wm_vrt_alignment;
		$wm_config['wm_hor_alignment'] = $wm_hor_alignment;
		$wm_config['wm_padding'] = $wm_padding;

		$this->ci->image_lib->clear();			 // clear settings
		$this->ci->image_lib->initialize($wm_config);
		$this->ci->image_lib->watermark();

//echo "<script>alert('".$file_path."');</script>";
//echo "<script>alert('".$wm_text."');</script>";
//echo "<script>alert('".$wm_font_color."');</script>";
//echo "<script>alert('".$wm_font_size."');</script>";
//echo $this->ci->image_lib->display_errors();
	}

}
?>