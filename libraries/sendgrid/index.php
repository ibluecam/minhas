<?php 
require 'vendor/autoload.php';
require 'lib/SendGrid.php';

$sendgrid = new SendGrid('sengkean', 'Se987654');
$email = new SendGrid\Email();
$email
    ->addTo('test@motorbb.com')
    ->setFrom('noreply@motorbb.com')
    ->setSubject('Car offer From Motorbb')
    ->setText('Hello World!')
    ->setHtml('<strong>Hello World!</strong>')
;

$sendgrid->send($email);

// Or catch the error

try {
    $sendgrid->send($email);
} catch(\SendGrid\Exception $e) {
    echo $e->getCode();
    foreach($e->getErrors() as $er) {
        echo $er;
    }
}