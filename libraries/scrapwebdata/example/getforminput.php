<?php
// example of how to use basic selector to retrieve HTML contents
include('../simple_html_dom.php');
$inputs = array();
$item_ids = array();
$inputs['__EVENTTARGET'] = "ctl00\$cph_Main_Content\$lsv_Search_Result\$dpg_Result_Top\$ctl00\$ctl00";
$inputs['__EVENTARGUMENT'] = "";
// get DOM from URL or file
$html = file_get_html('http://www.primegt.com/stock/search.aspx');

$form_ele=$html->find('form[id=frm_Main]'); 
$inputs_ele = $form_ele[0]->children;
foreach($inputs_ele as $ele){
    $inputs[$ele->name] = $ele->value ;
}

//var_dump($inputs);


$url = 'http://www.primegt.com/stock/search.aspx';
$data = $inputs;

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data),
    ),
);
$pager_arr_a = $html->find("div[class=pager under] a");
$last_key = max(array_keys($pager_arr_a));
$num_page = intval($pager_arr_a[$last_key]->innertext);
echo "$num_page";
//var_dump($pager_arr_a[1]->nodes[0]->_);
/**POST BY PHP***/
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
//echo $result;
$result_parse = str_get_html($result);
/**END POST BY PHP***/
$arr_a = $result_parse->find('table[class=lsv_result] tbody td[class=start] a');
foreach($arr_a as $a){
  $item_ids[] = getValue($a->href, 'st');
}
//var_dump($item_ids);
function getValue($url, $getname){
  $query = parse_url($url, PHP_URL_QUERY);
  $vars = array();
  parse_str($query, $vars);
  return $vars[$getname];
}


?>