<?php
include('../simple_html_dom.php');
$url = 'http://www.primegt.com/stock/search.aspx';
/*** GET NUMBER OF PAGE ***/
function getNumberOfPage(){
	global $url;
	$num_page = 0;
	$html = file_get_html($url);

	$pager_arr_a = $html->find("div[class=pager] a");
	$last_key = max(array_keys($pager_arr_a));
	$num_page = intval($pager_arr_a[$last_key]->innertext);
	return $num_page;
}

/*** GET HTML OF SEARCH RESULT OF THE PAGE NUMBER AFTER EXECUTE ***/
function getSearchResult($pageNumber){
	/**POST BY PHP***/
	global $url;
	$data = getSearchInputs($pageNumber);
	$options = array(
	    'http' => array(
	        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
	        'method'  => 'POST',
	        'content' => http_build_query($data),
	    ),
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);

	/**END POST BY PHP***/
	return $result;
}

/*** GET ALL ITEM IDS AS ARRAY FROM A PAGE NUMBER ***/
function getAllItemIdFromHtml($pageNumber){
	$item_ids = array();
	$html = getSearchResult($pageNumber);
	$result_parse = str_get_html($html);
	$arr_a = $result_parse->find('table[class=lsv_result] tbody td[class=start] a');
	foreach($arr_a as $a){
		if($a->href!=NULL){
			$item_ids[] = getValue($a->href, 'st');
		}
	}
	return $item_ids;
}

/*** GET ALL INPUT IN THE SEARCH FORM AS ARRAY ***/
function getSearchInputs($pageNumber){
	global $url;
	$inputs = array();
	if($pageNumber>0){
		$true_page = $pageNumber - 1;
	}else{
		$true_page = 0;
	}
	$inputs['__EVENTTARGET'] = "ctl00\$cph_Main_Content\$lsv_Search_Result\$dpg_Result_Top\$ctl00\$ctl0".$true_page;
	$inputs['__EVENTARGUMENT'] = "";
	$html = file_get_html('http://www.primegt.com/stock/search.aspx');
	$form_ele=$html->find('form[id=frm_Main]'); 
	$inputs_ele = $form_ele[0]->children;
	foreach($inputs_ele as $ele){
	    $inputs[$ele->name] = $ele->value ;
	}
	return $inputs;
}	

/*** GET PARAMETER GET FROM URL ***/
function getValue($url, $getname){
  $query = parse_url($url, PHP_URL_QUERY);
  $vars = array();
  parse_str($query, $vars);
  return $vars[$getname];
}

/*** EXECUTE ***/
$ids = array();
$num_page = getNumberOfPage();
for($i=1;$i<=$num_page;$i++){
	$arr_id = getAllItemIdFromHtml($i);
	$ids = array_merge($ids, $arr_id);
}

/*** OUTPUT ALL IDS FROM ALL PAGES ***/
var_dump($ids);