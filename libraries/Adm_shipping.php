<?php


if (!defined('BASEPATH'))

	exit('No direct script access allowed');



/* ------------------------------------------------

 * 파일명 : Adm_account.php

 * 개  요 : 회원관리

  ------------------------------------------------ */



class Adm_shipping {

	var $ci;
	var $mcd;
	var $bbs_config;
	var $member_join_config;


//constructor
	function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();

//전체 사이트 맵
		$sitemap = $this->ci->data['site_map_list'];
		if (count($sitemap) > 0) {
			foreach ($sitemap as $rows) {
//회원관련 모듈의 메뉴코드_회원가입
				if (strstr($rows->menu_type, 'member_join')) {
					func_set_data($this->ci, 'member_join_mcd', $rows->menu_code);
				}
//회원관련 모듈의 메뉴코드_로그인
				if (strstr($rows->menu_type, 'member_join')) {
					func_set_data($this->ci, 'member_login_mcd', $rows->menu_code);
				}
			}
		}
	}


	/* -------------------------------------------------------------------------------------------- */
	//게시판 등록 처리

	// function adm_shipping_charge() {
		
	// 	$where = array();
		
	// 	$mcd = $this->ci->input->get_post('mcd');
		
	// 	//$where['member_no ='] = $this->ci->member_no;
		
	// 	$shipping_charge_list =  $this->ci->shipping->select_shipping_charge($where);
		
	// 	func_set_data($this->ci, 'shipping_charge_list', $shipping_charge_list->result());
		
	// 	func_set_data($this->ci, 'mcd',$mcd);
		
	// 	$this->ci->load->view('admin/adm/shipping/adm_shipping_charge', $this->ci->data, false);
		
	// }

	function adm_shipping_charge() {
		$nav_array = array(
			'Home'=>'/?c=admin',
			'Shipping Charges'=>'/?c=admin&m=adm_shipping_charge'
		);

		$shipping_charges =  $this->ci->shipping->select_shipping_charge();
		func_set_data($this->ci, 'nav_array', $nav_array);
		func_set_data($this->ci, 'shipping_charges',$shipping_charges);
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/shipping/list', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
		
	}
	function adm_shipping_view(){
		$where = array();
		$id = $this->ci->input->get('id');
		$where['sc.id'] = $id;
		$where_freight = array('shipping_charge_id'=>$id);
		$shipping_charge = $this->ci->shipping->select_shipping_charge($where);
		$models = $this->ci->shipping->select_freight_model($where_freight);
		$body_types = $this->ci->shipping->select_freight_body_type($where_freight);

		$nav_array = array(
			'Home'=>'/?c=admin',
			'Shipping Charges'=>'/?c=admin&m=adm_shipping_charge',
			'Shipping Detail'=>'/?c=admin&m=adm_shipping_view&id='.$id
		);

		func_set_data($this->ci, 'nav_array', $nav_array);

		func_set_data($this->ci, 'id', $id);
		func_set_data($this->ci, 'models', $models);
		func_set_data($this->ci, 'body_types', $body_types);
		func_set_data($this->ci, 'shipping_charge', $shipping_charge[0]);
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/shipping/view', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}
	function adm_shipping_edit(){
		
		$where = array();
		$id = $this->ci->input->get('id');
		$port_list = array();
		$models = array();
		$make_list = array();
		$body_types = array();
		$shipping_charges = array();
		$shipping_charge_detail = new stdClass();
		$country_list = $this->ci->user_bbs->get_country_list();
		$make_list = $this->ci->user_bbs->select_make_all();
		$body_type_list = $this->ci->user_bbs->get_bodytype_list();
		if(!empty($id)){
			/*** MODIFY MODE ***/
			$where['sc.id'] = $id;
			$where_freight = array('shipping_charge_id'=>$id);
			$shipping_charges = $this->ci->shipping->select_shipping_charge($where);
			if(count($shipping_charges>0)){
				$shipping_charge_detail = $shipping_charges[0];
				$port_list = $this->ci->shipping->select_port($shipping_charge_detail->port_country);
			}

			$models = $this->ci->shipping->select_freight_model($where_freight);
			$body_types = $this->ci->shipping->select_freight_body_type($where_freight);
		}else{
			/*** NEW MODE ***/
			$shipping_charge_detail->port_to = null;
			$shipping_charge_detail->freight_cost_by = 'dimension';
			$shipping_charge_detail->insurance_cost = null;
			$shipping_charge_detail->inspection_cost = null;
			$shipping_charge_detail->shipping_cost = null;
			$shipping_charge_detail->country_from = null;
			$shipping_charge_detail->port_country = null;
			$shipping_charge_detail->price_per_cbm = null;
			$shipping_charge_detail->cbm_value = null;
		}
		
		$nav_array = array(
			'Home'=>'/?c=admin',
			'Shipping Charges'=>'/?c=admin&m=adm_shipping_charge',
			'Shipping Edit'=>'/?c=admin&m=adm_shipping_view&id='.$id
		);

		func_set_data($this->ci, 'nav_array', $nav_array);
		
		func_set_data($this->ci, 'shipping_charge', $shipping_charge_detail);
		func_set_data($this->ci, 'id', $id);
		func_set_data($this->ci, 'country_list', $country_list);
		func_set_data($this->ci, 'make_list', $make_list);
		func_set_data($this->ci, 'body_type_list', $body_type_list);
		func_set_data($this->ci, 'port_list', $port_list);
		func_set_data($this->ci, 'models', $models);
		func_set_data($this->ci, 'body_types', $body_types);
			
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/shipping/edit', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}
	function adm_shipping_edit_post(){
		$insert_id = '';
		$id = $this->ci->input->get('id');
		$shipping_posts = $this->ci->input->post('shipping');
		$selected_model_text = $this->ci->input->post('selected_model');
		$selected_body_type_text = $this->ci->input->post('selected_body_type');
		$selected_models = explode('^', $selected_model_text);
		$selected_body_types = explode('^', $selected_body_type_text);
		$allowed_fields = array('country_from', 'port_to', 'insurance_cost', 'inspection_cost', 'freight_cost_by');

		$freight_by_fields['dimension'] = array_merge($allowed_fields, array('shipping_cost'));
		$freight_by_fields['model'] = array_merge($allowed_fields, array('cbm_value', 'price_per_cbm'));
		$freight_by_fields['body_type'] = array_merge($allowed_fields, array('shipping_cost'));

		/*** SET OTHER FIELDS AS NULL IF NOT RILEVANT ***/
		foreach($shipping_posts as $key=>$value){
			$freight_cost_by = $shipping_posts['freight_cost_by'];
			if(!in_array($key, $freight_by_fields[$freight_cost_by])){
				$shipping_posts[$key]=null;
			}
		}
		if(!empty($shipping_posts['country_from'])&&!empty($shipping_posts['port_to'])&&!empty($shipping_posts['freight_cost_by'])){
			if(empty($id)){
				$insert_id=$this->ci->shipping->insert_shipping_charge($shipping_posts);
				
			}else{
				$insert_id = $id;
				$this->ci->shipping->update_shipping_charge($insert_id, $shipping_posts);
				/*** DELETE FREIGHT MODEL AND BODY TYPE WHERE NOT SET FROM POST ***/
				$this->ci->shipping->delete_freight_model_beside($id, $selected_models);
				$this->ci->shipping->delete_freight_body_type_beside($id, $selected_body_types);

				$count_fm = $this->ci->shipping->select_count_freight_model($insert_id, $selected_models);
				$count_bt = $this->ci->shipping->select_count_freight_body_type($insert_id);
				/*** INSERT MODEL IF THEY ARE NEW ***/
				foreach($count_fm as $cfn){
					$key = array_search($cfn->model_name, $selected_models);   
					if($key>=0){
						unset($selected_models[$key]);
					}
				}
				/*** INSERT MODEL IF THEY ARE NEW ***/
				foreach($count_bt as $cbt){
					$key = array_search($cbt->body_type_name, $selected_body_types);
					/*** DUPLICATED MODEL ***/   
					if($key>=0){
						unset($selected_body_types[$key]);
					}
				}
			}
			if(!empty($insert_id)){
				$this->ci->shipping->insert_freight_model($insert_id, $selected_models);
				$this->ci->shipping->insert_freight_body_type($insert_id, $selected_body_types);
			}
				
		}
		redirect('/?c=admin&m=adm_shipping_view&id='.$insert_id);
			
	}
	//게시판 등록 처리


	function adm_shipping_charge_exec() {
		
		$mcd = $this->ci->input->get_post('mcd');
		
		$id_key =0;
		
		$inData = array();
		
		$where = array();
		
		$keys = array('country_from','country_to','port_name','shipping_cost','insurance','inspection','currency_type');

        $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴
		
		$inData['member_no'] = $this->ci->member_no;       //아이디

		for($i=0;$i< sizeof($keys);$i++){
			
			//echo ($inData[$keys[$i]]);
			
			$where[$keys[$i]." ="] = $inData[$keys[$i]];
			
		}
        //var_dump($where);
		
		
		if (!isset($inData['created_dt']) || ($inData['created_dt'] == '')) {         //작성일
            $inData['created_dt'] = func_get_date_time();
        }
		//var_dump($inData);
		
		$total_row =  $this->ci->shipping->total_select_shipping_charge($where);
		
		$shipping_charge_list =  $this->ci->shipping->select_shipping_charge($where)->result();

		if($total_row > 0){
			foreach($shipping_charge_list as $row){
				$inData['id']= $row->id;	
			}
			if (!isset($inData['modified_dt']) || ($inData['modified_dt'] == '')) {         //작성일
            	$inData['modified_dt'] = func_get_date_time();
       		}
			$this->ci->shipping->update_shipping_charge($inData);
			//var_dump($inData);
		}else{
			$this->ci->shipping->insert_shipping_charge($inData);	
		}
		
		/*$account_rate_list =  $this->ci->account->select_account_rate($where)->result();
		
		func_set_data($this->ci, 'account_rate_list', $account_rate_list);*/
		
		echo func_jsAlertReplace('Shipping Charge has beed added.', '?c=admin&m=adm_shipping_charge&mcd=' . $mcd);
	}

	//게시판 등록 처리
	function adm_shipping_charge_modify(){
		
		$id_shipping = $this->ci->input->get_post('id');
		
		$mcd = $this->ci->input->get_post('mcd');
		
		$where = array();
		
		$where['member_no ='] = $this->ci->member_no;
		
		if(is_array($id_shipping)){
			
			$id_shipping=$id_shipping[0];	
		}else{ 
			echo $id_shipping;
			
		}
		
		//var_dump($id_rat);
		
		$where['id ='] = $id_shipping;
		
		$shipping_charge_list =  $this->ci->shipping->select_shipping_charge($where);
		
		func_set_data($this->ci, 'shipping_charge_list', $shipping_charge_list->result());
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		$this->ci->load->view('admin/adm/shipping/adm_shipping_charge_modify', $this->ci->data, false);
		
	}
	
	//게시판 등록 처리
	function adm_shipping_charge_modify_exec(){
		
		$id_rat = $this->ci->input->get_post('id');
		
		$mcd = $this->ci->input->get_post('mcd');
		
		$where = array();
		
		$where['member_no ='] = $this->ci->member_no;
		
		$inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴
		
		$inData['id']=$id_rat;
		
		if (!isset($inData['modified_dt']) || ($inData['modified_dt'] == '')) {         //작성일
            	$inData['modified_dt'] = func_get_date_time();
		}
		
		$this->ci->shipping->update_shipping($inData);
		
		
		$shipping_charge_list =  $this->ci->shipping->select_shipping_charge($where);
		
		echo func_jsAlertReplace('Shipping Charge has beed modified.', '?c=admin&m=adm_shipping_charge&mcd=' . $mcd);
		
	}
	
	//게시판 등록 처리
	function adm_shipping_charge_delete(){
		
		
	}
	
	//게시판 등록 처리
	function adm_shipping_charge_delete_exec(){
		
		$id_shipping = $this->ci->input->get_post('id');

		$mcd = $this->ci->input->get_post('mcd');
		
		for($i=0;$i <count($id_shipping);$i++){

			$this->ci->shipping->delete_freight_model($id_shipping[$i]);

			$this->ci->shipping->delete_freight_body_type($id_shipping[$i]);

			$result =$this->ci->shipping->delete_shipping_charge($id_shipping[$i]);

		}
		if($result == true){
			echo func_jsAlertReplace('Deletion Successful! Shipping Charge has been deleted.', '?c=admin&m=adm_shipping_charge&mcd=' . $mcd);
			
		}
		
	}

    function adm_shipping_port() {
		$nav_array = array(
			'Home'=>'/?c=admin',
			'Shipping Port'=>'/?c=admin&m=adm_shipping_port'
		);

		$shipping_port =  $this->ci->shipping->select_shipping_port();
		func_set_data($this->ci, 'nav_array', $nav_array);
		func_set_data($this->ci, 'shipping_port',$shipping_port);
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/port/list', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
		
	}

	function adm_shipping_port_delete_exec(){
		
		$id_port = $this->ci->input->get_post('id');

		$mcd = $this->ci->input->get_post('mcd');
		
		for($i=0;$i <count($id_port);$i++){

			$result =$this->ci->shipping->delete_shipping_port($id_port[$i]);

		}
		if($result == true){
			echo func_jsAlertReplace('Deletion Successful! Shipping Port has been deleted.', '?c=admin&m=adm_shipping_port&mcd=' . $mcd);
			
		}
		
	}

	function adm_shipping_port_view(){
		$where = array();
		$id = $this->ci->input->get('id');
		if(!empty($id)){
			$where['iw_port.id'] = $id;
			$shipping_port = $this->ci->shipping->select_shipping_port($where);

			$nav_array = array(
				'Home'=>'/?c=admin',
				'Shipping Port'=>'/?c=admin&m=adm_shipping_port',
				'Shipping Port Detail'=>'/?c=admin&m=adm_shipping_port_view&id='.$id
			);

			func_set_data($this->ci, 'nav_array', $nav_array);
			func_set_data($this->ci, 'rows',$shipping_port[0]);
			func_set_data($this->ci, 'id', $id);
			func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/port/view', $this->ci->data, true));
	        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
	    }else{
	    	redirect('/?c=admin&m=adm_shipping_port');
	    }
	}

	function adm_shipping_port_edit(){
		
		$where = array();
		$id = $this->ci->input->get('id');
		$shipping_port = array();
		$shipping_port_detail = new stdClass();
		$country_list = $this->ci->user_bbs->get_country_list();
		if(!empty($id)){
			/*** MODIFY PORT ***/
			$where['iw_port.id'] = $id;
			
			$shipping_port = $this->ci->shipping->select_shipping_port($where);
			if(count($shipping_port>0)){
				$shipping_port_detail = $shipping_port[0];
				
			}
		}else{
			/*** NEW PORT ***/
			$shipping_port_detail->country_to = null;
			$shipping_port_detail->port_name = null;
		}
		
		$nav_array = array(
			'Home'=>'/?c=admin',
			'Shipping Port'=>'/?c=admin&m=adm_shipping_port',
			'Shipping Port Edit'=>'/?c=admin&m=adm_shipping_port_edit&id='.$id
		);

		func_set_data($this->ci, 'nav_array', $nav_array);
		
		func_set_data($this->ci, 'shipping_port', $shipping_port_detail);
		func_set_data($this->ci, 'id', $id);
		func_set_data($this->ci, 'country_list', $country_list);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/port/edit', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}

	function adm_shipping_port_edit_exec(){
		$insert_id = '';
		$id = $this->ci->input->get('id');
		$shipping_posts = $this->ci->input->post('shipping');
		if(!empty($shipping_posts['country_iso'])&&!empty($shipping_posts['port_name'])){
			if(empty($id)){
				$insert_id=$this->ci->shipping->insert_shipping_port($shipping_posts);
			}else{
				$insert_id = $id;
				$this->ci->shipping->update_shipping_port($insert_id, $shipping_posts);
			}
		}

		redirect('/?c=admin&m=adm_shipping_port_view&id='.$insert_id);
	}

}

?>