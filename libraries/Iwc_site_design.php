<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Iwc_site_design.php
 * 개  요 : 사이트 디자인을 관리한다.
  ------------------------------------------------ */

class Iwc_site_design {

	var $ci;

//constructor
	function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();
	}

//디자인 서브 HTML
	function view_site_design_sub() {
		func_set_data($this->ci, 'html_source', '');
		func_set_data($this->ci, 'html_top_source', '');
		func_set_data($this->ci, 'contents_menu_code', '');

//컨텐츠
		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/site_design/iwc_site_design_sub', $this->ci->data, true));
		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);
	}

//디자인 서브 HTML 소스 가져오기
	function view_site_design_sub_source() {
		$contents_menu_code = $this->ci->input->get_post('menu_code');

//컨텐츠 디렉토리 경로
		$sel_dir = 'views/user/contents/' . $contents_menu_code;
//컨텐츠 상단 디렉토리 경로
		$sel_top_dir = 'views/user/contents_top/' . $contents_menu_code;

//컨텐츠 읽기
		$html_source = read_file($sel_dir . '/' . $contents_menu_code . '.php');
//컨텐츠 상단 읽기
		$html_top_source = read_file($sel_top_dir . '/' . $contents_menu_code . '.php');

		func_set_data($this->ci, 'html_source', $html_source);
		func_set_data($this->ci, 'html_top_source', $html_top_source);
		func_set_data($this->ci, 'contents_menu_code', $contents_menu_code);

//컨텐츠
		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/site_design/iwc_site_design_sub', $this->ci->data, true));
		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);
	}

//디자인 서브 HTML 소스 저장
	function exec_site_design_sub() {
		$contents_menu_code = $this->ci->input->get_post('menu_code');
		$contents_html_source = $this->ci->input->get_post('html_source'); //HTML 소스
		$contents_top_html_source = $this->ci->input->get_post('html_top_source'); //HTML 상단 소스
//디렉토리 경로
		$sel_dir = 'views/user/contents/' . $contents_menu_code;

//컨텐츠 TOP 디렉토리 경로
		$sel_top_dir = 'views/user/contents_top/' . $contents_menu_code;

//파일 저장
		write_file($sel_dir . '/' . $contents_menu_code . '.php', $contents_html_source, 'wb');
//파일 저장
		write_file($sel_top_dir . '/' . $contents_menu_code . '.php', $contents_top_html_source, 'wb');

//파일 읽기
		$html_source = read_file($sel_dir . '/' . $contents_menu_code . '.php');
//파일 읽기
		$html_top_source = read_file($sel_top_dir . '/' . $contents_menu_code . '.php');

		func_set_data($this->ci, 'html_source', $html_source);
		func_set_data($this->ci, 'html_top_source', $html_top_source);
		func_set_data($this->ci, 'contents_menu_code', $contents_menu_code);

//컨텐츠
		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/site_design/iwc_site_design_sub', $this->ci->data, true));
		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);

		echo func_jsAlertReplace('저장 되었습니다.', '/?c=iwc&amp;m=view_site_design_sub');
	}

//디자인 LEFT MUNU 소스 가져오기
	function view_site_design_left_menu() {
//LEFT MENU 디렉토리 경로
		$sel_dir = 'views/user/left_menu/';

//컨텐츠 읽기
		$left_menu_source = read_file($sel_dir . '/left_menu.php');

		func_set_data($this->ci, 'left_menu_source', $left_menu_source);

//컨텐츠
		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/site_design/iwc_site_design_left_menu', $this->ci->data, true));
		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);
	}

//디자인 LEFT MUNU 소스 저장
	function exec_site_design_left_menu() {
		$left_menu_source = $this->ci->input->get_post('left_menu_source');

//디렉토리 경로
		$sel_dir = 'views/user/left_menu/';

//파일명
		$file_name = 'left_menu.php';

//파일 저장
		write_file($sel_dir . '/' . $file_name, $left_menu_source, 'wb');

//컨텐츠
		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/site_design/iwc_site_design_left_menu', $this->ci->data, true));
		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);

		echo func_jsAlertReplace('저장 되었습니다.', '/?c=iwc&amp;m=view_site_design_left_menu');
	}

}
?>