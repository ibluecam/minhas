<?php


if (!defined('BASEPATH'))

	exit('No direct script access allowed');



/* ------------------------------------------------

 * 파일명 : Adm_customer.php

 * 개  요 : 회원관리

  ------------------------------------------------ */



class Adm_customer {



	var $ci;

	var $mcd;

	var $bbs_config;



//constructor

	function __construct() {

//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당

		$this->ci = &get_instance();
		 /* ------------------------------------------------------------



          - Model Load



          ------------------------------------------------------------ */
           $this->ci->load->model('mo_file', 'mo_file');
           $this->ci->load->model('mo_bbs', 'bbs');
           $this->ci->load->model('mo_email', 'mo_email');
//전체 사이트 맵

		$sitemap = $this->ci->data['site_map_list'];

		if (count($sitemap) > 0) {

			foreach ($sitemap as $rows) {

//회원관련 모듈의 메뉴코드_회원가입

				if (strstr($rows->menu_type, 'customer_join')) {

					func_set_data($this->ci, 'customer_join_mcd', $rows->menu_code);

				}

//회원관련 모듈의 메뉴코드_로그인

				if (strstr($rows->menu_type, 'customer_login')) {

					func_set_data($this->ci, 'customer_login_mcd', $rows->menu_code);

				}

			}

		}


	}

	function adm_order_list_popup(){
		$where['stock_idx'] = $this->ci->input->get_post('idx'); 
		$order_list = $this->ci->customer->adm_order_list($where);
		func_set_data($this->ci, 'idx', $where['stock_idx']);
		func_set_data($this->ci, 'order_list', $order_list->result());
        $this->ci->load->view('admin/adm/customer/adm_order_list_popup', $this->ci->data, false);
	}
	function adm_order_list_popup_exec(){
		$where['stock_idx'] = $this->ci->input->get_post('idx'); 
		//Exec only
		$order_list = $this->ci->customer->adm_order_list($where);
		func_set_data($this->ci, 'idx', $where['stock_idx']);
		func_set_data($this->ci, 'order_list', $order_list->result());
        $this->ci->load->view('admin/adm/customer/adm_order_list_popup', $this->ci->data, false);
	}

//회원조회

	function adm_customer_list($prev_next = 'N') {

		$where = array(); //sql where 조건

		$sParam = '';	  //검색 string

		$c = $this->ci->input->get_post('c');
		$m = $this->ci->input->get_post('m');

//카테고리 검색

		$category_yn = func_get_config($this->bbs_config, 'category_yn'); 		//카테고리 검색

		$category = $this->ci->input->get_post('category'); 		//카테고리 코드

		if ($category_yn != 'N' && $category != '') {

			$sParam .= '&category=' . $category;

			$where['category ='] = $category;

		}



//검색 조건

		$sch_create_dt_s = $this->ci->input->get_post('sch_create_dt_s'); //등록일 시작

		if ($sch_create_dt_s != '') {

			$sParam .= '&sch_create_dt_s=' . $sch_create_dt_s;

			$where['member.created_dt >='] = $sch_create_dt_s . " 00:00:01";

		}

		$sch_create_dt_e = $this->ci->input->get_post('sch_create_dt_e'); //등록일 종료

		if ($sch_create_dt_e != '') {

			$sParam .= '&sch_create_dt_e=' . $sch_create_dt_e;

			$where['member.created_dt <='] = $sch_create_dt_e . " 23:59:59";

		}



		$sch_condition = $this->ci->input->get_post('sch_condition');   //검색조건

		$sch_condition_country = $this->ci->input->get_post('sch_condition_country');

		$sch_condition_grade = $this->ci->input->get_post('sch_condition_grade');

		$sch_word = $this->ci->input->get_post('sch_word');		//검색어
		$sch_condition_customer_name = $this->ci->input->get_post('sch_condition_customer_name');	
		//var_dump($sch_condition); var_dump($sch_word)

//검색조건 + 검색어

		if ($sch_condition != '' && $sch_word != '') {

			$sch_condition_tmp = explode('|', $sch_condition);


//다중 조건인 경우(제목 + 내용)

			if (count($sch_condition_tmp) > 1) {

				$tmpStr = '';

				for ($i = 0; $i < count($sch_condition_tmp); $i++) {

					if ($tmpStr != '') {

						$tmpStr .= ' or ';

					}



					$tmpStr .= $sch_condition_tmp[$i] . ' ? ';

					$where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';

				}



				$tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

				$where[$tmpStr] = '';

				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			} else {

				$where[$sch_condition] = '%' . $sch_word . '%';


				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			}

		}



//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 20; //페이지 로우수

		$page_views = 10; //페이지 네비게이션 수

//회원등급 조회

		//$customer_grade_list = $this->ci->customer->customer_grade_select();


//회원 목록 조회
		if ($sch_condition_country !='') {
			$where['member.member_country='] = $sch_condition_country;
		}

		if ($sch_condition_grade !='') {
			$where['member.business_type='] = $sch_condition_grade;
		}
		if ($sch_condition_customer_name !='') {
			$where['keyword_member_name'] = $sch_condition_customer_name;
		}
		$customer_list = $this->ci->customer->customer_select($where, ($cur_page - 1) * $page_rows);

		$total_rows = $customer_list->total_count; 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   //글 번호

//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);

		$country_list = $this->ci->bbs->list_country_customer();
		$rec_list = $this->ci->mo_email->select_recipient_list();

		func_set_data($this->ci, 'country_list', $country_list);
		func_set_data($this->ci, 'rec_list', $rec_list);
		func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 //글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string

		func_set_data($this->ci, 'customer_list', $customer_list->result());

		func_set_data($this->ci, 'page_info', $page_info);
		func_set_data($this->ci, 'c', $c);   //조회 전체 목록수
		func_set_data($this->ci, 'm', $m);   //조회 전체 목록수
		//func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/' . $this->ci->data['customer_join_mcd'] . '/adm_income_list', $this->ci->data, true));
		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/adm_customer_list', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}

 function adm_customer_view(){
        $grade_no_session = $this->ci->grade_no;
        $member_no = $this->ci->input->get_post('member_no');
        $cur_page = $this->ci->input->get_post('cur_page');
        $mcd = $this->ci->input->get_post('mcd');
        $member_detail_list = $this->ci->member->member_select_detail_adm($member_no); //상세 조회
        //Check grade //
        $member_detail = $member_detail_list->result();
        $allowedGrades = array(1, 2, 3, 10, 11);
        $allowedToShow = false;
        //End Check grade //
        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;
        $sParam = $this->ci->input->get_post('sParam');  //검색 string

       

        $member_grade_list = '';

        if ($mcd=="customer"||$grade_no_session=='2'||$grade_no_session=='3') {
            $member_grade_list = $this->ci->member->member_grade_select_register();
        } else {
            $member_grade_list = $this->ci->member->member_grade_select();    //회원등급 조회
        }
        //Check Grade // 
        if($grade_no_session!='1'){
            $allowedGrades = array(11);
        }
        if(in_array($member_detail['0']->grade_no, $allowedGrades)){
            $allowedToShow = true;
        }
        //End Check grade //
        if($allowedToShow){

        	$select_cphoto_list = $this->ci->mo_file->select_cphoto($_GET['member_no']);

        	func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());

            func_set_data($this->ci, 'session_grade_no', $grade_no_session);
            
            func_set_data($this->ci, 'member_no', $member_no);
            func_set_data($this->ci, 'cur_page', $cur_page);    //현재 페이지
            func_set_data($this->ci, 'sParam', $sParam);     //검색 string
            func_set_data($this->ci, 'member_detail_list', $member_detail_list);
            func_set_data($this->ci, 'member_grade_list', $member_grade_list->result());

            func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/adm_customer_view', $this->ci->data, true));
            $this->ci->load->view('admin/adm_index', $this->ci->data, false);
        }
    }	



//회원 상세 조회

	function adm_customer_detail() {

		$customer_no = $this->ci->input->get_post('customer_no');

		$cur_page = $this->ci->input->get_post('cur_page');

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$sParam = $this->ci->input->get_post('sParam');		//검색 string



		$customer_detail_list = $this->ci->customer->customer_select_detail_adm($customer_no); //상세 조회

		$customer_grade_list = $this->ci->customer->customer_grade_select();				//회원등급 조회



		func_set_data($this->ci, 'customer_no', $customer_no);

		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string

		func_set_data($this->ci, 'customer_detail_list', $customer_detail_list);

		func_set_data($this->ci, 'customer_grade_list', $customer_grade_list->result());



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/' . $this->ci->data['customer_join_mcd'] . '/adm_customer_detail', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}


	//customer 등록 HTML

	function adm_customer_write() {
		$this->ci->load->helper('url');
        $insert_id = $this->ci->user_member->insert_customer();
        $seller=array('seller_id'=>$insert_id);
        //$this->insertSellerSQLite($seller);
        ob_clean();
        header("Location: /?c=admin&m=adm_customer_modify&create_mode=new&member_no=".$insert_id);

  //               $person_sale=$this->ci->customer->get_all_persion_sale();
  //               func_set_data($this->ci, 'person_sale', $person_sale);
		// func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/adm_customer_write', $this->ci->data, true));

		// $this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}
        
        function adm_changepassword() {

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/adm_changepassword', $this->ci->data, true));
               
		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}
        
        function adm_changepassword_exec(){
            $member_id=  $this->ci->input->post('member_id');
            $oldpassword= func_base64_encode($this->ci->input->post('oldpassword'));
            $newpassword= func_base64_encode($this->ci->input->post('newpassword'));
            $confirmpassword=func_base64_encode ($this->ci->input->post('confirmpassword'));
            $check=  $this->ci->customer->member_checkid_password($member_id,$oldpassword);
            if($check->num_rows()>0){
               $this->ci->customer->update_pass_true($newpassword,$confirmpassword,$member_id);
            }
            else {
               echo func_jsAlertReplace('Unable to change password. Incorrect Password.', '/?c=admin&m=adm_changepassword');
            }
       
        }

        function user_changepassword_exec(){
        	$param_m = $this->ci->input->post('m');
            $member_id=  $_SESSION['ADMIN']['member_id'];

            $oldpassword= $this->ci->input->post('oldpassword');
            $newpassword= func_base64_encode($this->ci->input->post('newpassword'));
            $confirmpassword=func_base64_encode ($this->ci->input->post('confirmpassword'));
            $valid_password=  $this->ci->customer->validate_password($member_id,$oldpassword);
            
            if($valid_password==true && $newpassword==$confirmpassword){

               $this->ci->customer->update_password($member_id, $newpassword);
              
               if($param_m=="mobile_change_password"){

               		$this->modal_popup("Change Password","Password has been modified.","/?c=admin&m=mobile_change_password");
               
               }else{

               		$this->modal_popup("Change Password","Password has been modified.","/?c=admin&m=change_password");

               }
            
            }
            else {
     
            	if($param_m=="mobile_change_password"){

            		$this->modal_popup("Change Password","Unable to change password. Incorrect Password.","/?c=admin&m=mobile_change_password");
            	
            	}else{

            		$this->modal_popup("Change Password","Unable to change password. Incorrect Password.","/?c=admin&m=change_password");
            	
            	}
            	
            }
       
        }
        
        function adm_my_info(){
        	$grade_no = 10;
        	$country_list = $this->ci->bbs->admGetCountry_list();
        	$grade_no_list = $this->ci->member->select_grade_no($grade_no);
        	$row = $this->ci->member->select_member_login();

        	func_set_data($this->ci, 'row', $row);
        	func_set_data($this->ci, 'country_list', $country_list);
        	func_set_data($this->ci, 'grade_no_list', $grade_no_list->result());

            func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/adm_my_info', $this->ci->data, true)); 
            $this->ci->load->view('admin/adm_index', $this->ci->data, false);
        }
        
       
        
         function view_info(){
         	$select_cphoto_list = $this->ci->mo_file->select_cphoto($_SESSION['ADMIN']['member_no']);
         	$grade_no = 10;
        	$country_list = $this->ci->bbs->admGetCountry_list();
        	$grade_no_list = $this->ci->member->select_grade_no($grade_no);
        	$row = $this->ci->member->select_member_login();

        	func_set_data($this->ci, 'row', $row);
        	func_set_data($this->ci, 'country_list', $country_list);
        	func_set_data($this->ci, 'grade_no_list', $grade_no_list->result());
        	func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());

            func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/view_info', $this->ci->data, true)); 
            $this->ci->load->view('admin/adm_index', $this->ci->data, false);
        }



        //게시판 등록 처리

	function adm_customer_write_exec() {

		$inData = array();

		$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴
                $inData['member_pwd'] =func_base64_encode($inData['member_pwd']);
                 $member_id=$inData['member_id'];
                 $member_pwd=$inData['member_pwd'];
               

		//$inData['del_flg'] = 'True';						//del_flg


		if (! isset($inData['created_dt'])||($inData['created_dt'] == '')) {						//작성일

			$inData['created_dt'] = func_get_date_time();

		}

		$inData['write_ip'] = $this->ci->input->ip_address();													 //작성자 아이피

		$consignee_name    = implode('|',$_POST['consignee_name']);
		$consignee_address = implode('|',$_POST['consignee_address']);
		$consignee_phone   = implode('|',$_POST['consignee_phone']);

		$inData['consignee_name']    = $consignee_name;
		$inData['consignee_address'] = $consignee_address;
		$inData['consignee_phone']   = $consignee_phone;
		$check=  $this->ci->customer->check_member_id($member_id);
               if($check>0 and ($member_id !='' || $member_pwd !='')){
                    echo func_jsAlertReplace(' Your Id has already.', '/?c=admin&m=adm_customer_write');
                    exit();
                }
            else {
                
                //DB 등록
		/*var_dump($_POST);
		var_dump($inData); exit();*/
           
		$idx = $this->ci->customer->customer_insert($inData);
		//첨부파일 등록

		$attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn'); 		//첨부파일 사용여부

		$upload_str = '';


		echo func_jsAlertReplace('Registration Complete.', '/?c=admin&m=adm_customer_list');
                exit();
            }
            

	}

	//customer 수정 HTML

	function adm_customer_modify() {

		$idx = $this->ci->input->get_post('idx');

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	  		//검색 string

		$only_attach = $this->ci->input->get_post('only_attach'); 		//첨부파일 상세 삭제 여부

		$only_survey = $this->ci->input->get_post('only_survey'); //문항 상세 삭제 여부
		

		$customer_detail_list = $this->ci->customer->customer_select_detail($idx);
		//var_dump($customer_detail_list); exit();
                $person_sale=$this->ci->customer->get_all_persion_sale();
                func_set_data($this->ci, 'person_sale', $person_sale);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/adm_customer_modify', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}
        
        function  adm_my_info_exec(){
		
		

            $inData = array();
              $inData['updated_dt'] = func_get_date_time();
              $inData = func_get_post_array($this->ci->input->post('values'));  
		//}

				$phone_no = $inData['phone_no1'];
					
                $phone_no_arr = explode(" ", $phone_no);
                $inData["phone_no1"] = $phone_no_arr[0];
                unset($phone_no_arr[0]);
                $inData["phone_no2"] = implode('', $phone_no_arr);
                $inData["phone_no1"] = preg_replace("/[^0-9]/","",$inData["phone_no1"]);
                $inData["phone_no2"] = preg_replace("/[^0-9]/","",$inData["phone_no2"]);
				
				$phone_no = $inData['mobile_no1'];
					
                $phone_no_arr = explode(" ", $phone_no);
                $inData["mobile_no1"] = $phone_no_arr[0];
                unset($phone_no_arr[0]);
                $inData["mobile_no2"] = implode('', $phone_no_arr);
                $inData["mobile_no1"] = preg_replace("/[^0-9]/","",$inData["mobile_no1"]);
                $inData["mobile_no2"] = preg_replace("/[^0-9]/","",$inData["mobile_no2"]);
				
				
				//var_dump($inData); exit();
				
			
				
				

		$consignee_name    = implode('|',$_POST['consignee_name']);
		$consignee_address = implode('|',$_POST['consignee_address']);
		$consignee_phone   = implode('|',$_POST['consignee_phone']);
		
		
		$inData['consignee_name']    = $consignee_name;
		$inData['consignee_address'] = $consignee_address;
		$inData['consignee_phone']   = $consignee_phone;
		//DB 등록

		$idx = $this->ci->customer->_customer_update($inData);

		//첨부파일 등록

		$attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn'); 		//첨부파일 사용여부

		$upload_str = '';


		echo func_jsAlertReplace('update Completed.', '/?c=admin&m=adm_my_info');
        }
        
        




 function  adm_member_info_exec(){
		
		

            $inData = array();
              $inData['updated_dt'] = func_get_date_time();
              $inData = func_get_post_array($this->ci->input->post('values'));  
		//}

				$phone_no = $inData['phone_no1'];
					
                $phone_no_arr = explode(" ", $phone_no);
                $inData["phone_no1"] = $phone_no_arr[0];
                unset($phone_no_arr[0]);
                $inData["phone_no2"] = implode('', $phone_no_arr);
                $inData["phone_no1"] = preg_replace("/[^0-9]/","",$inData["phone_no1"]);
                $inData["phone_no2"] = preg_replace("/[^0-9]/","",$inData["phone_no2"]);
				
				$phone_no = $inData['mobile_no1'];
					
                $phone_no_arr = explode(" ", $phone_no);
                $inData["mobile_no1"] = $phone_no_arr[0];
                unset($phone_no_arr[0]);
                $inData["mobile_no2"] = implode('', $phone_no_arr);
                $inData["mobile_no1"] = preg_replace("/[^0-9]/","",$inData["mobile_no1"]);
                $inData["mobile_no2"] = preg_replace("/[^0-9]/","",$inData["mobile_no2"]);
				
				
				//var_dump($inData); exit();
				
			
				
				

		// $consignee_name    = implode('|',$_POST['consignee_name']);
		// $consignee_address = implode('|',$_POST['consignee_address']);
		// $consignee_phone   = implode('|',$_POST['consignee_phone']);
		
		
		// $inData['consignee_name']    = $consignee_name;
		// $inData['consignee_address'] = $consignee_address;
		// $inData['consignee_phone']   = $consignee_phone;

		//DB 등록
		//var_dump($inData);

        $inData['member_id'] = $_SESSION['ADMIN']['member_id'];
		$idx = $this->ci->customer->_customer_update($inData);

		//첨부파일 등록

		$attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn'); 		//첨부파일 사용여부

		$upload_str = '';


		// echo func_jsAlertReplace('You info has been updated.', '/?c=admin&m=member_info');

		$this->modal_popup("My info","Your info has been updated","/?c=admin&m=member_info");
        }


        function modal_popup($tittle,$message,$link){
        	

        	echo '
                   <link rel="stylesheet" href="/css/member_site/bootstrap.min.css">
				   <script src="/js/admin/jquery.min.js"></script>
				   <script src="/js/admin/bootstrap.min.js"></script>
                 ';
        	
            echo 
            '
				  <div class="modal-dialog">
				    <div class="modal-content">
				    <div class="modal-header">
				    <div class="bootstrap-dialog-header">
				    <div class="bootstrap-dialog-close-button" style="display: none;">
				    <button class="close">×</button></div><div class="bootstrap-dialog-title" 
				    id="e2985fd5-c4ec-4bbb-aab8-fdcb3d27134b_title">'.$tittle.'</div></div></div>
				    <div class="modal-body"><div class="bootstrap-dialog-body">
				    <div class="bootstrap-dialog-message">
				       '.$message.'
				    </div></div></div>
				    <div class="modal-footer" style="display: block;">
				    <div class="bootstrap-dialog-footer">
				    <div class="bootstrap-dialog-footer-buttons">
				    <a class="btn btn-default" href='.$link.'>Ok</a>
				    </div></div></div>
				    </div></div>
  
            ';
            echo '<script>
                    BootstrapDialog.alert("I want banana!");
                 </script>';
              
        }
        
        
    // rado start mobile info

        function  mobile_adm_member_info_exec(){
		
		

            $inData = array();
              $inData['updated_dt'] = func_get_date_time();
              $inData = func_get_post_array($this->ci->input->post('values'));  
		//}

				$phone_no = $inData['phone_no1'];
					
                $phone_no_arr = explode(" ", $phone_no);
                $inData["phone_no1"] = $phone_no_arr[0];
                unset($phone_no_arr[0]);
                $inData["phone_no2"] = implode('', $phone_no_arr);
                $inData["phone_no1"] = preg_replace("/[^0-9]/","",$inData["phone_no1"]);
                $inData["phone_no2"] = preg_replace("/[^0-9]/","",$inData["phone_no2"]);
				
				$phone_no = $inData['mobile_no1'];
					
                $phone_no_arr = explode(" ", $phone_no);
                $inData["mobile_no1"] = $phone_no_arr[0];
                unset($phone_no_arr[0]);
                $inData["mobile_no2"] = implode('', $phone_no_arr);
                $inData["mobile_no1"] = preg_replace("/[^0-9]/","",$inData["mobile_no1"]);
                $inData["mobile_no2"] = preg_replace("/[^0-9]/","",$inData["mobile_no2"]);
				
				
				//var_dump($inData); exit();
				
			
				
				

		$consignee_name    = implode('|',$_POST['consignee_name']);
		$consignee_address = implode('|',$_POST['consignee_address']);
		$consignee_phone   = implode('|',$_POST['consignee_phone']);
		
		
		$inData['consignee_name']    = $consignee_name;
		$inData['consignee_address'] = $consignee_address;
		$inData['consignee_phone']   = $consignee_phone;
		//DB 등록
		//var_dump($inData);
		$idx = $this->ci->customer->_customer_update($inData);

		//첨부파일 등록

		$attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn'); 		//첨부파일 사용여부

		$upload_str = '';

			if($_SESSION['ADMIN']['business_type']=="buyer"){
				
				echo func_jsAlertReplace('You info has been update.', '/?c=admin&m=mobile_info_buyer');
			
			}else{
				
				echo func_jsAlertReplace('You info has been updated.', '/?c=admin&m=mobile_info');
			
			}
		
        }

    // rado end mobile info

	function adm_customer_modify_exec() {

		$inData = array();

		$inData = func_get_post_array($this->ci->input->post('values'));  
               
        $member_pwd=func_base64_encode($this->ci->input->get_post('pass'));

        $select_email_customer = $this->ci->customer->get_email_by_customer_no($inData['member_no']);

        $email_customer = $select_email_customer[0]->email;

        if(isset($_POST['create_mode'])){
   
        	if($member_pwd !=''){

				$inData['updated_dt'] = func_get_date_time();

				$consignee_name    = implode('|',$_POST['consignee_name']);
				$consignee_address = implode('|',$_POST['consignee_address']);
				$consignee_phone   = implode('|',$_POST['consignee_phone']);

				$inData['consignee_name']    = $consignee_name;
				$inData['consignee_address'] = $consignee_address;
				$inData['consignee_phone']   = $consignee_phone;
	            $inData['member_pwd']   = $member_pwd;

				$idx = $this->ci->customer->customer_update($inData);

				$attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn');

				$upload_str = '';

				//$this->updateSellerSQLite($inData);

				$log_data = array(
		            'author'=>$_SESSION['ADMIN']['member_no'],
		            'message'=>'Customer No. '.$inData['member_no'].' (email: '.$inData['email'].') has been inserted by '.$_SESSION['ADMIN']['member_id'],
		            'log_type'=>'customer',
		            'created_dt'=>func_get_date_time()
		        );
		        $this->ci->mo_activity_log->insert($log_data);

				echo func_jsAlertReplace('Registration Completed.', '/?c=admin&m=adm_customer_list');
			}
                    
       }else{
                
	       if($member_pwd ==''){
	            
	            $inData['updated_dt'] = func_get_date_time();

				$consignee_name    = implode('|',$_POST['consignee_name']);
				$consignee_address = implode('|',$_POST['consignee_address']);
				$consignee_phone   = implode('|',$_POST['consignee_phone']);

				$inData['consignee_name']    = $consignee_name;
				$inData['consignee_address'] = $consignee_address;
				$inData['consignee_phone']   = $consignee_phone;

				$idx = $this->ci->customer->customer_update($inData);

				$attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn'); 

				$upload_str = '';

				//$this->updateSellerSQLite($inData);
				$log_data = array(
		            'author'=>$_SESSION['ADMIN']['member_no'],
		            'message'=>'Customer No. '.$inData['member_no'].' (email: '.$email_customer.') has been updated by '.$_SESSION['ADMIN']['member_id'],
		            'log_type'=>'customer',
		            'created_dt'=>func_get_date_time()
		        );
		        $this->ci->mo_activity_log->insert($log_data);

				echo func_jsAlertReplace('Modified Completed.', '/?c=admin&m=adm_customer_list');
	                     
	        }

        }

	}



//회원 수정

	function adm_customer_update() {

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	  //검색 string





		$inData = array();

		$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴

		$inData['customer_pwd'] = func_base64_encode($inData['customer_pwd']);										  //비밀번호



		$inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];					   //우편번호

		$inData['updated_dt'] = func_get_date_time();										  //회원수정일



		unset($inData['zipcode1']);

		unset($inData['zipcode2']);



//첨부파일이 존재한다면...

		if (count($_FILES) > 0) {

//선택된 첨부 파일만...

			if ($_FILES['attach']['tmp_name'] != '') {

//파일 업로드

				$upload_str = $this->ci->iwc_common->upload_image('attach', $this->mcd);



//파일 업로드 실패

				if ($upload_str != TRUE) {

					show_error($this->ci->iwc_common->upload_error_msg());

				}

//파일 업로드 성공

				else {

					$upload_file_info = array();

					$upload_file_info = $this->ci->iwc_common->upload_success_msg();



					$inData['photo'] = $upload_file_info['file_name']; //사진 파일명

				}

			}

		}



//DB 수정

		$idx = $this->ci->customer->customer_update_adm($inData);



		echo func_jsAlertReplace('Modification Completed.', '/?c=admin&m=adm_customer_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}



//회원 삭제

	function adm_customer_delete() {
		
		$customer_no = $this->ci->input->get_post('customer_no');
		
		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	  //검색 string
		
//DB 삭제

		$idx = $this->ci->customer->customer_delete_adm($customer_no);


		print_r($customer_no);
		//echo func_jsAlertReplace('Deletion Completed.', '/?c=admin&m=adm_customer_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}



//회원 선택 메일 발송

	function adm_customer_email() {

		$all_email = $this->ci->input->get_post('all_email');



		$customer_no = '';

		$customer_nos = $this->ci->input->get_post('customer_no'); //배열

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	  //검색 string



		$sel_customer_no = array(); //회원번호

		$customer_id = array(); //회원 아이디

		$customer_name = array(); //회원명

		$email = array(); //이메일

		$cnt = 0;



//전체메일이 아닌 경우

		if ($all_email != 'Y') {

//선택 회원 배열

			if (count($customer_nos) > 0) {

				foreach ($customer_nos as $key => $value) {

					$customer_no = $value; //게시물 번호



					if ($customer_no != '') {

						$customer_detail_list = $this->ci->customer->customer_select_detail_adm($customer_no); //상세 조회

//회원 상세 조회

						$rows = '';

						if (count($customer_detail_list) > 0) {

							$rows = $customer_detail_list->row();

						}

						$sel_customer_no[$cnt] = $rows->customer_no;

						$customer_id[$cnt] = $rows->customer_id;

						$customer_name[$cnt] = $rows->customer_first_name;

						$email[$cnt] = $rows->email;

					}



					$cnt++;

				}

			}

		}

//전체메일

		else {

			$customer_all_list = $this->ci->customer->customer_select_all()->result(); //전체 조회



			if (count($customer_all_list) > 0) {

				foreach ($customer_all_list as $rows) {

					$sel_customer_no[$cnt] = $rows->customer_no;

					$customer_id[$cnt] = $rows->customer_id;

					$customer_name[$cnt] = $rows->customer_name;

					$email[$cnt] = $rows->email;



					$cnt++;

				}

			}

		}



		func_set_data($this->ci, 'mail_customer_no', $sel_customer_no);

		func_set_data($this->ci, 'mail_customer_id', $customer_id);

		func_set_data($this->ci, 'mail_customer_name', $customer_name);

		func_set_data($this->ci, 'mail_customer_email', $email);



		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string



		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/' . $this->ci->data['customer_join_mcd'] . '/adm_customer_email', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}



//회원 선택 메일 발송 처리

	function adm_customer_email_exec() {

		$customer_no = '';

		$customer_nos = $this->ci->input->get_post('receive_email'); //배열

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');		//검색 string



		$send_name = $this->ci->input->get_post('send_name');	 //보내는 사람

		$send_email = $this->ci->input->get_post('send_email');	//보내는 메일

		$subject = $this->ci->input->get_post('subject');	   //메일 제목

		$contents = $this->ci->input->get_post('mail_contents'); //메일 내용





		$customer_name = array(); //받는사람

		$email = array(); //받는메일

		$attach = ''; //첨부파일 FULL PATH



		$cnt = 0;

//선택 회원 배열

		if (count($customer_nos) > 0) {

			foreach ($customer_nos as $key => $value) {

				$customer_no = $value;



				if ($customer_no != '') {

					$customer_detail_list = $this->ci->customer->customer_select_detail_adm($customer_no); //상세 조회

//회원 상세 조회

					$rows = '';

					if (count($customer_detail_list) > 0) {

						$rows = $customer_detail_list->row();

					}



					$customer_name[$cnt] = $rows->customer_name;

					$email[$cnt] = $rows->email;

				}

				$cnt++;

			}





//메일 발송(백그라운 발송 방법 강구..)

			$this->ci->load->library('sendmail');





//선택된 첨부 파일만...

			if ($_FILES['attach1']['tmp_name'] != '') {

//파일 업로드

				$upload_str = $this->ci->iwc_common->upload('attach1', 'email', NULL, TRUE);



//파일 업로드 실패

				if ($upload_str != TRUE) {

					show_error($this->ci->iwc_common->upload_error_msg());

				}

//파일 업로드 성공

				else {

					$upload_file_info = array();

					$upload_file_info = $this->ci->iwc_common->upload_success_msg();



//rename($upload_file_info['full_path'], iconv("UTF-8", "EUC-KR", $upload_file_info['full_path']));

					$attach = $upload_file_info['full_path'];

				}

			}



//echo $attach;

//exit;



			if (count($email) > 0) {

				set_time_limit(0);



				for ($i = 0; $i < count($email); $i++) {

//이메일 유효성

					if (func_email_check($email[$i])) {

						$this->ci->sendmail->clear(TRUE);   //모든 이메일 변수들을 초기화  TRUE 시 첨부파일도 초기화

						$this->ci->sendmail->setMailType("html");

						$this->ci->sendmail->setInit();

						$this->ci->sendmail->setFrom($send_email, $send_name);

						$this->ci->sendmail->setTo($email[$i]);

						$this->ci->sendmail->setSubject($subject);

						$this->ci->sendmail->setMessage($contents);

						if ($attach != '') {

							$this->ci->sendmail->setAttach($attach);

						}

						$mail_result = $this->ci->sendmail->send(); //전송여부 TRUE FALSE 반환

//echo $this->ci->sendmail->printDebugger();

					}

				}

			}

		}

//메일 첨부 파일 삭제

		@unlink($attach);

		echo func_jsAlertReplace('메일이 발송 되었습니다.', '/?c=admin&m=adm_customer_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}



//회원정보 선택 삭제

	function adm_customer_all_delete() {

		 $customer_nos = $this->ci->input->get_post('idx'); //배열
		 
         
		// $customer_nos = $this->ci->input->get_post('idx'); //배열

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	//검색 string


//선택 게시물 배열

		foreach ($customer_nos as $key => $value) {

			$customer_no = $value; //회원 번호



			if ($customer_no != '') {

				$select_email_customer = $this->ci->customer->get_email_by_customer_no($customer_no);

				$log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'Customer No. '.$customer_no.'( email: '.$select_email_customer[0]->email.') has been deleted by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'customer',
                    'created_dt'=>func_get_date_time()
                );

				$this->ci->customer->customer_delete_adm($customer_no);
				//$this->deleteSellerSQLite($customer_no);

				

                $this->ci->mo_activity_log->insert($log_data);

				$file_owner = $this->ci->mo_file->select_file_public_id($customer_no);

				if (count($file_owner) > 0){

                    foreach($file_owner as $id){
                        $bbs_idx[] = $id->public_id;
                        $bbs_idx[] = implode(',', $bbs_idx);
                        \Cloudinary\Uploader::destroy($id->public_id);
                    }
                    $this->ci->mo_file->delete_cloud_file($bbs_idx);

                }

			}

		}



		echo func_jsAlertReplace('Deletion Completed.', '/?c=admin&m=adm_customer_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}



//회원정보 선택 인증

	function adm_customer_all_certi() {

		$customer_no = '';

		$customer_nos = $this->ci->input->get_post('customer_no'); //배열

		$cur_page = $this->ci->input->get_post('cur_page');

		$sParam = $this->ci->input->get_post('sParam');	//검색 string

//선택 게시물 배열

		foreach ($customer_nos as $key => $value) {

			$customer_no = $value; //회원 번호



			if ($customer_no != '') {

				$this->ci->customer->customer_certi_update_adm($customer_no); //회원 인증

			}

		}



		echo func_jsAlertReplace('인증되었습니다.', '/?c=admin&m=adm_customer_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

	}



	/* -------------------------------------------------------------------------------------------- */



//회원등급 HTML

	function view_customer_grade() {

//처리구분

		$exec_type = $this->ci->input->get_post('exec_type');

		$select = '';

		$select_detail = '';

		$seq_no = '';



//회원등급 조회

		$select = $this->ci->customer->customer_grade_select();



		if ($exec_type == 'UPDATE') {

			$seq_no = $this->ci->input->get_post('seq_no');

//회원등급 상세 조회

			$select_detail = $this->ci->customer->customer_grade_select_detail($seq_no);



			func_set_data($this->ci, 'customer_grade_detail_result', $select_detail);

		}



		func_set_data($this->ci, 'customer_grade_result', $select->result());

		func_set_data($this->ci, 'exec_type', $exec_type == '' ? 'INSERT' : $exec_type);

		func_set_data($this->ci, 'seq_no', $seq_no);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/customer/adm_customer_grade', $this->ci->data, true)); //컨텐츠

//VIEW 로드

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}



//회원등급 등록&수정 EXEC

	function exec_customer_grade() {

//처리구분

		$exec_type = $this->ci->input->get_post('exec_type');



//POST 배열을 array['key'] = value 형태로 리턴

		$inData = func_get_post_array($this->ci->input->post('values'));



		$inData['created_dt'] = func_get_date_time();



		if ($exec_type != 'UPDATE') {



			$this->ci->customer->insert_customer_grade($inData);

			echo func_jsAlertReplace('등록 되었습니다.', '/?c=admin&amp;m=view_customer_grade');

		} else {

			$seq_no = $this->ci->input->get_post('seq_no');

			$this->ci->customer->update_customer_grade($inData, $seq_no);

			echo func_jsAlertReplace('수정 되었습니다.', '/?c=admin&amp;m=view_customer_grade');

		}

	}



//회원등급 삭제

	function exec_delete_customer_grade() {

		$seq_no = $this->ci->input->get_post('seq_no');



		$this->ci->customer->delete_customer_grade($seq_no);



		echo func_jsAlertReplace('삭제 되었습니다.', '/?c=admin&amp;m=view_customer_grade');

	}



	/* -------------------------------------------------------------------------------------------- */

	//cutomer deposit update

	function adm_customer_deposit_popup_update() {

		$idx = '';

		$idx = $_GET['idx']; 


		func_set_data($this->ci, 'idx', $idx);



		$this->ci->load->view('admin/adm/customer/adm_customer_popup', $this->ci->data, false);



		//$this->ci->load->view('admin/adm_index', $this->ci->data, false);

	}

		//선적 수정 처리

	function adm_customer_deposit_popup_update_exec() {


		$idx = '';	//customer_no

		//$idxs = $this->ci->input->get_post('idxs');	  		//배열

		//$cur_page = $this->ci->input->get_post('cur_page');

		//$sParam = $this->ci->input->get_post('sParam');   		//검색 string
		$result = false;


		$inData = array();

		$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴

		//var_dump($inData); exit();

		

		$inData['menu_code'] = $this->mcd;					 		//메뉴코드

		$menu_code = $inData['menu_code'];

		$popup_type =  $this->ci->input->get_post('popup_type');

		//var_dump($popup_type); exit();


		if($popup_type == 'customer_deposit')
		{
			$idx = $inData['customer_no'];

			$car_deposit = $inData['car_deposit'];

			$currency_type = $inData['currency_type'];

			$deposit_dt = func_get_date_time();


			$sql = "INSERT INTO iw_payment (customer_no, car_deposit, currency_type, deposit_dt)
					VALUES ('$idx','$car_deposit', '$currency_type', '$deposit_dt')";

			//var_dump($sql); exit();		
			$result = mysql_query($sql);

		}


		if($popup_type == 'car_allocate')
		{

			$idx = $_GET['idx'];			//게시물 일련번호

			$count = COUNT($_POST['car_idx']);

			$currency_type = $_POST['currency_allocate'];

			$allocate_dt = func_get_date_time();

			for($i=0 ; $i<$count ; $i++){
					
					$car_allocate = '';
					$car_idx = '';

					$car_allocate = $_POST['each_allocate'][$i];
					$car_idx = $_POST['car_idx'][$i];

					$sql = "INSERT INTO iw_payment (customer_no, car_idx, car_allocate, currency_type, allocate_dt)
							VALUES ('$idx', '$car_idx','$car_allocate','$currency_type','$allocate_dt')";

					// var_dump($sql); exit();	
					$result = mysql_query($sql);	
			}


			//var_dump($sql); exit();		
			//$result = mysql_query($sql);

		}


		//var_dump($sql); exit();
		//$this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/popup', $this->ci->data, false);


		//echo func_jsAlertReplace('Modification Complete.', '/?c=admin&m=adm_bbs_ship_popup_update&mcd=' . $this->mcd . '&idxs=' . $idxs );
		if($result){
					echo("<script language=\"JavaScript\" type=\"text/JavaScript\">\n");
					//echo("alert('Input Complete!!!!');\n");
					/*echo("window.location = '/?c=admin&m=adm_bbs_ship_popup_update&mcd=product&idx='");
					echo $idxs;
					echo(" ; ");*/
					echo("</script>\n");
					//$this->ci->load->view('admin/adm/customer/adm_customer_popup', $this->ci->data, false);
					
					//$this->ci->load->view('/?c=admin&m=adm_customer_deposit_popup_update&mcd=&idx=31&currency=JPY', $this->ci->data, false);
					if($popup_type=='car_allocate'){
						echo func_jsAlertReplace('Input Complete!!!!', '/?c=admin&m=adm_customer_deposit_popup_update&mcd=&idx=' . $idx . '&currency=' . $currency_type );	
					}else{
						echo func_jsAlertReplace('Input Complete!!!!', '/?c=admin&m=adm_income_list' );	
					}
					
				}else{
					echo("<script language=\"JavaScript\" type=\"text/JavaScript\">\n");
					//echo("alert('Modification Failed!!!!Please try again!!!');\n");
					
					echo("</script>\n");
					
					//$this->ci->load->view('/?c=admin&m=adm_customer_deposit_popup_update&mcd=&idx=31&currency=JPY', $this->ci->data, false);
					if($popup_type=='car_allocate'){
						echo func_jsAlertReplace('Modification Failed!!!!Please try again!!!', '/?c=admin&m=adm_customer_deposit_popup_update&mcd=' . $idx . '&currency=' . $currency_type );
					}else{
						echo func_jsAlertReplace('Modification Failed!!!!Please try again!!!', '/?c=admin&m=adm_income_list' );	
					}
				}

	}

	function getSellerData($data)
	{
		$result=array();
		$result['seller_id']=$data['member_no'];
		$result['name']=$data['contact_person_name'];
		$result['company']=$data['company_name'];
		$result['country']=$data['member_country'];
		$result['address']=$data['address'];
		return $result;
	}

	function insertSellerSQLite($data)
	{
		$result[]=$data;
		$this->ci->bbs->insertSQLData('sellers',$result);
	}

	function updateSellerSQLite($data)
	{
		$result[]=$this->getSellerData($data);
		$this->ci->bbs->updateSellerSQLData('sellers',$result);
	}

	function deleteSellerSQLite($seller_id)
	{
		$result[]=array('seller_id'=>$seller_id);
		$this->ci->bbs->deleteSQLData('sellers',$result);
	}

}

?>

