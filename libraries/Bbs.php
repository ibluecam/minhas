<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



/* ------------------------------------------------

 * 파일명 : bbs.php

 * 개  요 : 게시판 Class

  ------------------------------------------------ */

class Bbs {

    var $ci;
    var $mcd;
    var $bbs_config;

    //constructor

    function __construct() {



        //CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당

        $this->ci = &get_instance();



        /* ------------------------------------------------------------



          - Model Load



          ------------------------------------------------------------ */



        $this->ci->load->model('mo_bbs', 'user_bbs'); //class명과 동일해서 user_bbs
        $this->ci->load->model('mo_activity', 'activity'); //class명과 동일해서 user_bbs

        $this->ci->load->model('mo_survey', 'survey'); // 설문조사

        $this->ci->load->model('mo_member', 'user_member'); // 맴버
        //게시판 메뉴코드

        $this->mcd = $this->ci->data['mcd'];



        //게시판 설정정보

        $this->bbs_config = $this->ci->iw_config->select_bbs_config($this->mcd)->result();



        //게시판 설정을 하였다면..

        if (count($this->bbs_config) > 0) {

            func_set_data($this->ci, 'bbs_config_result', $this->bbs_config);

            func_set_data($this->ci, 'bbs_code_name', func_get_config($this->bbs_config, 'bbs_code_name')); //게시판 명
            //스킨 이미지 경로(해당 게시판 이미지 스킨 경로에 존재)

            $skin_images_url = '/views/user/bbs/' . $this->mcd . '/images';

            func_set_data($this->ci, 'skin_images_url', $skin_images_url);



            //게시판 상단

            $html_header = func_get_config($this->bbs_config, 'bbs_html_header');

            func_set_data($this->ci, 'html_header', $html_header);



            //게시판 하단

            $html_footer = func_get_config($this->bbs_config, 'bbs_html_footer');

            func_set_data($this->ci, 'html_footer', $html_footer);
        } else {

            //echo func_jsAlertReplace('해당 게시판이 생성되지 않았습니다.\n\n관리자에게 문의하세요.', '/', TRUE);
        }
    }

    //인덱스

    function index() {

        //게시판 Index 화면 결정 L:리스트 화면, V:상세 화면, R:등록화면 (상세 화면 미 개발)

        if (func_get_config($this->bbs_config, 'bbs_index') == 'L') {

            return $this->bbs_list();
        } else if (func_get_config($this->bbs_config, 'bbs_index') == 'V') { //상세보기 마지막 글을 조회한다.
            return $this->bbs_list();

            //return $this->bbs_detail(NULL, NULL, $this->is->bbs_data->getLastMenuCodeView($this->mcd, NULL, $this->code)); //params : menuCode, count(NULL), code(NULL)
        } else if (func_get_config($this->bbs_config, 'bbs_index') == 'R') {

            return $this->bbs_write();
        } else {

            return $this->bbs_list();
        }
		
    }

    function my_bbs_list() {

        //권한체크

        $this->_bbs_certi_check('member_grade_list');

        $where = array(); //sql where 조건

        $where2 = array(); //sql where 조건2

        $sParam = '';   //검색 string
        //카테고리 검색

        $category_yn = func_get_config($this->bbs_config, 'category_yn'); //카테고리 검색

        $category = $this->ci->input->get_post('category'); //카테고리 코드

        if ($category_yn != 'N' && $category != '') {

            $sParam .= '&category=' . $category;

            $where['category ='] = $category;
        }
        $where['member_id ='] = $category;


        /*

          if($this->ci->input->get_post('layout') == 'wishlist') {

          $sParam .= '&category=' . 'false';

          $where['category ='] = 'false';

          }

         */



        if ($this->ci->input->get_post('layout') == 'wishlist') {
            
        }



        //날짜 검색

        $sch_create_dt_s = $this->ci->input->get_post('sch_create_dt_s'); //등록일 시작

        $sch_create_dt_e = $this->ci->input->get_post('sch_create_dt_e'); //등록일 종료



        if ($sch_create_dt_s != '' && $sch_create_dt_s == $sch_create_dt_e) {

            $where['created_dt like'] = $sch_create_dt_s . '%';
        } else {

            if ($sch_create_dt_s != '') {

                $sParam .= '&sch_create_dt_s=' . $sch_create_dt_s;

                $where['created_dt >='] = $sch_create_dt_s . " 00:00:00";
            }

            if ($sch_create_dt_e != '') {

                $sParam .= '&sch_create_dt_e=' . $sch_create_dt_e;

                $where['created_dt <='] = $sch_create_dt_e . " 23:59:59";
            }
        }



        $sch_condition = $this->ci->input->get_post('sch_condition');   //검색조건

        $sch_word = $this->ci->input->get_post('sch_word');  //검색어
        //검색조건 + 검색어

        if ($sch_condition != '' && $sch_word != '') {

            $sch_condition_tmp = explode('|', $sch_condition);

            //다중 조건인 경우(제목 + 내용)

            if (count($sch_condition_tmp) > 1) {

                $tmpStr = '';

                for ($i = 0; $i < count($sch_condition_tmp); $i++) {

                    if ($tmpStr != '') {

                        $tmpStr .= ' or ';
                    }



                    $tmpStr .= $sch_condition_tmp[$i] . ' ? ';

                    $where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';
                }



                $tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

                $where[$tmpStr] = '';

                $sParam .= '&sch_condition=' . $sch_condition;

                $sParam .= '&sch_word=' . $sch_word;
            } else { //조건이 하나일때
                $where[$sch_condition] = '%' . $sch_word . '%';

                $sParam .= '&sch_condition=' . $sch_condition;

                $sParam .= '&sch_word=' . $sch_word;
            }
        }



        // 차량검색

        if (isset($_REQUEST['CarSearch'])) {

            $CarSearch = $_REQUEST['CarSearch'];
        } else {

            $CarSearch = Array();
        }



        foreach ($CarSearch as $CKey => $CVal) {

            if ($CVal != '*' and $CVal != 'Any' and $CVal != '') {



                // var_dump($where);
                // $where[$CKey.' ='] = $CVal;



                $sParam .= '&CarSearch[' . $CKey . ']=' . $CVal;





                if ($CKey == "car_chassis_no") {

                    $where2[$CKey] = "(car_chassis_no = '" . $CVal . "' OR car_stock_no = '" . $CVal . "'  )";
                } else if ($CKey == "car_fob_discount") {
                    if ($CVal == 'SALE') {

                        $where2[$CKey] = 'car_visible="True" and (icon_soldout_yn!="Y" and icon_reserved_yn!="Y")';
                    }
                } else if ($CKey == "country") {

                    $where[$CKey . '='] = $CVal;
                } else if ($CKey == "car_model_year") {

                    $where[$CKey . ' >='] = $CVal;
                } else if ($CKey == "manu_year_end") {

                    $where['car_model_year <='] = $CVal;
                } else {

                    $where[$CKey . ' ='] = $CVal;
                }
            }
        }


        $min_price = $this->ci->input->get_post("car_fob_cost_min");
        $max_price = $this->ci->input->get_post("car_fob_cost_max");
        //

        if ($min_price != '*' && $min_price != 'Any' && $min_price != '') {
            $where['car_fob_cost >='] = $min_price;
        }
        if ($min_price != '*' && $min_price != 'Any' && $min_price != '') {
            $where['car_fob_cost <='] = $max_price;
        }

// var_dump($where);
// var_dump($where2);
        // var_dump($sParam);
        // exit;
        //회원 본인 글만 조회

        $member_id = '';

        if (func_get_config($this->bbs_config, 'member_list_yn') == 'Y') {

            $member_id = $this->ci->data['session_member_id'];
        }



        //캘린더 스킨 사용시..

        if (func_get_config($this->bbs_config, 'calendar_yn') == 'Y') {



            $year = $this->ci->input->get_post('year', TRUE);

            $month = $this->ci->input->get_post('month', TRUE);



            $sParam .= '&year=' . $year . '&month=' . $month;



            if ($year == '' && $year == NULL)
                $year = date('Y');

            if ($month == '' && $month == NULL)
                $month = date('m');



            $nowYear = date('Y');

            $nowMonth = date('m');

            $nowDay = date('d');



            $date_j = date('j'); // Day

            $date_t = date('t', mktime(0, 0, 0, $month, 1, $year)); //지정된 월에 몇 일이 있는지 확인

            $date_w = date('w', mktime(0, 0, 0, $month, 1, $year)); //지정된 월에 1일이 무슨 요일에 시작되는지.. 숫자로



            func_set_data($this->ci, 'year', $year);

            func_set_data($this->ci, 'month', $month);

            func_set_data($this->ci, 'date_j', $date_j);

            func_set_data($this->ci, 'date_t', $date_t);

            func_set_data($this->ci, 'date_w', $date_w);

            func_set_data($this->ci, 'nowYear', $nowYear);

            func_set_data($this->ci, 'nowMonth', $nowMonth);

            func_set_data($this->ci, 'nowDay', $nowDay);
        }



        //페이지 조건

        $cur_page = $this->ci->input->get_post("cur_page");

        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;



        $page_rows = (integer) func_get_config($this->bbs_config, 'page_rows');  //페이지 로우수

        $page_views = (integer) func_get_config($this->bbs_config, 'page_views'); //페이지 네비게이션 수
        //게시판 공지 목록 조회
        // var_dump($where2);
        // var_dump($where);

        $bbs_notice_list = $this->ci->user_bbs->select($this->mcd, 'Y', ($cur_page - 1) * $page_rows, $page_rows, $where, $where2, $member_id);





        /*

          var_dump($cur_page);

          var_dump('<br/>');

          var_dump($page_rows);

          var_dump('<br/>');

          var_dump($member_id);

          var_dump('<br/>');

         */



        //게시판 일반 목록 조회

        $bbs_list = $this->ci->user_bbs->select($this->mcd, 'N', ($cur_page - 1) * $page_rows, $page_rows, $where, $where2, $member_id);



        /*

          var_dump($this->mcd);

          var_dump('<br/>');

          var_dump($bbs_list);

          var_dump('<br/>');

          exit;

         */



        $total_rows = $this->ci->user_bbs->total_select($this->mcd, $where, $where2, $member_id); //조회 전체 목록수

        $row_cnt = $total_rows - ($cur_page - 1) * $page_rows;  //글 번호





        /*

          var_dump($bbs_list);

          var_dump('<br/>');

          var_dump($total_rows);

          var_dump('<br/>');

          var_dump($bbs_list->result());

          exit;

         */





        //페이지정보 조회

        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



        //이전글 다음글 조회

        if ($prev_next == 'Y') {

            $pn_data = array();

            $pn_data['isPrev'] = '';



            $prev = ($cur_page - 1) * $page_rows;



            if ($prev > 0) {

                $pn_data['isPrev'] = "PREV";

                $prev = $prev - 1;
            }



            $pn_data['cur_page'] = $cur_page;

            $pn_data['page_views'] = $page_views;

            $pn_data['page_rows'] = $page_rows;

            $pn_data['total_rows'] = $total_rows;

            $pn_data['row_cnt'] = $row_cnt;



            if ($pn_data['isPrev'] == 'PREV') {

                $page_rows = $page_rows + 2;
            } else {

                $page_rows = $page_rows + 1;
            }



            $pn_data['prevList'] = $this->ci->user_bbs->select($this->mcd, 'N', $prev, $page_rows, $where, $where2, $member_id)->result();



            return $pn_data;
        }



        func_set_data($this->ci, 'grade', $this->_bbs_certi_check());   //권한

        func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수

        func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지

        func_set_data($this->ci, 'row_cnt', $row_cnt);   //글번호

        func_set_data($this->ci, 'cur_page', $cur_page); //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);  //검색 string

        func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list->result());

        func_set_data($this->ci, 'bbs_list', $bbs_list->result());

        func_set_data($this->ci, 'page_info', $page_info);



        if ($this->ci->input->get_post('layout') == 'wishlist') {

            return $this->ci->load->view('user/bbs/' . $this->mcd . '/wishlist', $this->ci->data, true);
        } else {

            return $this->ci->load->view('user/bbs/' . $this->mcd . '/list', $this->ci->data, true);
        }
    }
    /*****Generate a url after unsetting a parameter*****/
    function unsetGetUrl($unset_p=array()){
        $get = $_GET;
        if(count($unset_p)>0){
            foreach($unset_p as $value){
                if(isset($get[$value])) unset($get[$value]);
            }
        }
        $updatedUrl_arr = array();
        $updatedUrl = http_build_query($get);
        return $updatedUrl;
    }

    /*****The Car List for user page*****/
    function bbs_list($prev_next = 'N') {
        
        $this->ci->load->helper('url');
        $this->ci->load->library('pagination');
        /*****START DEFAULT PARAMETERS*****/
        $gets = $_GET;
        $get_values = array();
        $where = $this->ci->input->get_post("CarSearch");
        
        //get search fields
        $searched_fields = $this->ci->input->get_post("CarSearch");
        $search_keys = array('car_make',
            'car_model',
            'car_fob_cost_min',
            'car_fob_cost_max',
            'car_transmission',
            'icon_status',
            'keyword',
            'car_model_year',
            'manu_year_end',
            'country');
        foreach($search_keys as $key){
            if(!isset($searched_fields[$key])){
                $searched_fields[$key] = '';
            }
        }

        $limit_s = $this->ci->input->get_post("offset");
        $whereRaw = '';
        /*****START RANGE CONDITION*****/
        if(isset($where['icon_status'])){
            if(!empty($where['icon_status'])){
           $whereRaw .= "(";
                $whereRaw .= "icon_status='".$where['icon_status']."'";
                $whereRaw .= "OR icon_status='order'";
                
                $whereRaw .=")";
          }
            unset($where['icon_status']);
        }


        if(isset($where['car_model_year'])){
            $where['car_model_year >=']=$where['car_model_year'];
            unset($where['car_model_year']);
        }
        if(isset($where['manu_year_end'])){
            $where['car_model_year <=']=$where['manu_year_end'];
            unset($where['manu_year_end']);
        }
        if(isset($where['car_fob_cost_min'])){
            $where['car_fob_cost >=']=$where['car_fob_cost_min'];
            unset($where['car_fob_cost_min']);
        }
        if(isset($where['car_fob_cost_max'])){
            $where['car_fob_cost <=']=$where['car_fob_cost_max'];
            unset($where['car_fob_cost_max']);
        }
        /*****END RANGE CONDITION*****/
        /*****START KEYWORD CONDITION*****/
        if(isset($where['keyword'])){
            if(!empty($where['keyword'])){
                $whereRaw .= "(";
                $whereRaw .= "car_chassis_no like '%".$where['keyword']."%'";
                $whereRaw .= "OR car_stock_no='".$where['keyword']."'";
                $whereRaw .=")";
            }
            unset($where['keyword']);
        }
        /*****END KEYWORD CONDITION*****/

        /*****END DEFAULT PARAMETERS*****/
        if(!$limit_s) $limit_s =0;
        $order = "icon_status ASC, iw_bbs_attach.is_image DESC, created_dt DESC";
        if($where){
            foreach($where as $key=>$value){
                if($value==''){
                    unset($where[$key]);
                }
            }
        }

        $get_url = $this->unsetGetUrl(array("offset"));
        $total_rows = $this->ci->user_bbs->user_total_select($where, $whereRaw);
        $per_page = $this->ci->input->get_post("offset");
        
        /*****START PAGINATION*****/
        $config['base_url'] = site_url()."/?".$get_url;
        $config['uri_segment'] = 3;
        $config['num_links'] = 5;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = 20;
        $config['last_tag_open'] = '<div style="display:none">';
        $config['last_tag_close'] = '</div>';
        $config['query_string_segment'] = "offset";

        $item_perpage = $config['per_page'];
        $total_page =ceil($total_rows/$item_perpage);
        $cur_page =$per_page;
        if ($cur_page == NULL || $cur_page == ''){
            $cur_page = 1;
        }elseif ($cur_page>2) {
            $cur_page = ceil($cur_page/$item_perpage+1);
        }

        $this->ci->pagination->initialize($config);
        $pagination = $this->ci->pagination->create_links();
        /*****END PAGINATION*****/
        /*****Search Variable*****/
        $country_list = $this->ci->mo_bbs->getLocation();
        $make_list = $this->ci->mo_bbs->getMake(array(), true);
        func_set_data($this->ci, 'country_list', $country_list); //왼쪽 메뉴 영역
        func_set_data($this->ci, 'total_rows', $total_rows);
        func_set_data($this->ci, 'total_page', $total_page);
        func_set_data($this->ci, 'cur_page', $cur_page);
        func_set_data($this->ci, 'make_list', $make_list); //왼쪽 메뉴 영역
        func_set_data($this->ci, 'searched_fields', $searched_fields); //왼쪽 메뉴 영역
        /*****End Search Variable*****/
        $bbs_list = $this->ci->user_bbs->user_select($where, $whereRaw, $limit_s, $config['per_page'], $order);

        func_set_data($this->ci, 'pagination', $pagination); //전체 페이지
        func_set_data($this->ci, 'bbs_list', $bbs_list); //전체 페이지
        
        // if ($this->ci->input->get_post('layout') == 'wishlist') {

        //     return $this->ci->load->view('user/bbs/' . $this->mcd . '/wishlist', $this->ci->data, true);
        // } else {

            return $this->ci->load->view('user/bbs/' . $this->mcd . '/list', $this->ci->data, true);
        // }
    }

    //게시판 상세 조회
    function email_product_inquiry() {
        //GET CURRENT URL 
        //$CI =& get_instance();
        //$url = $CI->config->site_url($CI->uri->uri_string());
        // $page_url = $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
        //GET SELLER EMAIL
        // $seller_email = $this->ci->member->exist_member_id_check($inData['member_id']);


        $buyer_email = $this->ci->input->get_post('buyer_email');
        $message = $this->ci->input->get_post('message');
        $seller_email = $this->ci->input->get_post('seller_email');
        //$seller_email = "kysengkean@gmail.com";
        $item_link = $this->ci->input->get_post('item_link');
        $item_name = $this->ci->input->get_post('item_name');
        $item_image = $this->ci->input->get_post('item_image');

        $html = '<html><head></head><body>';
        $html.= '<a href="' . $item_link . '"><img src="' . $item_image . '"/></a><br/>';
        $html.= $message . '<br/>';
        $html.= '<a href="' . $item_link . '">' . $item_name . '</a>';
        $html.= '</body></html>';
        $config = Array(
            'protocol' => 'mail',
            'smtp_host' => 'ssl://kangaroo.arvixe.com',
            'smtp_port' => '465',
            'smtp_user' => 'noreply@iblauda.com',
            'smtp_pass' => '#Kx4Qh_]})eh',
            'charset' => 'utf-8',
            'mailtype' => 'html',
            'newline' => "\r\n",
            'crlf' => "\n",
            'wordwrap' => TRUE
        );
        $this->ci->load->library('email');
        $this->ci->email->initialize($config);

        $this->ci->email->from('noreply@iblauda.com', 'IBLAUDA');
        $this->ci->email->reply_to($buyer_email, $buyer_email);
        $this->ci->email->to($seller_email);
        $this->ci->email->subject('Item Inquiry from ' . $buyer_email);
        $this->ci->email->message($html);
        if ($this->ci->input->get_post('send_button')) {
            $this->ci->email->send();
            //echo $this->ci->email->print_debugger();
        }
    }

    function bbs_detail($passwd_chk = 'N') {

        $this->email_product_inquiry();
        //권한체크

        $this->_bbs_certi_check('member_grade_view');

        //GET CURRENT URL 
        $CI = & get_instance();
        $url = $CI->config->site_url($CI->uri->uri_string());
        $page_url = $_SERVER['QUERY_STRING'] ? $url . '?' . $_SERVER['QUERY_STRING'] : $url;

        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');



        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;



        $sParam = $this->ci->input->get_post('sParam');  //검색 string



        $bbs_detail_list = $this->ci->user_bbs->select_detail($idx, $this->mcd); //상세 조회

        if($bbs_detail_list->num_rows() == 0) {

            echo func_jsAlertReplace('The car does not exist!', '/');
            
        }

        $bbs_attach_list = $this->ci->user_bbs->select_attach($idx, $this->mcd); //첨부파일 조회

        $rows = $bbs_detail_list->row();

        $member_id = $rows->member_id;

        $seller_detail = $this->ci->user_member->exist_member_id_check($member_id)->row();
        
        $this->ci->user_bbs->update_visit_cnt($idx, $this->mcd);  //조회수 업데이트
        //코멘트 사용 여부

        $bbs_comment_list = '';



        if (func_get_config($this->bbs_config, 'comment_yn') == 'Y') {

            //$bbs_comment_list = $this->ci->user_bbs->select_comment($idx, $this->mcd); //코멘트 조회

            func_set_data($this->ci, 'bbs_comment_list', $bbs_comment_list->result());
        }



        // 관리자는 무조건 비밀글 읽을 수 있음

        if (isset($_SESSION['USER'])) {

            if ($_SESSION['USER']['grade_no'] == 1) {

                $passwd_chk = 'Y';
            }
        }



        // 댓글달고 돌아올때 인증 안함

        if (isset($_SESSION['bbs_password'])) {

            $query = $this->ci->user_bbs->password_check($idx, $this->mcd, $_SESSION['bbs_password']);

            if ($query->num_rows() > 0) {

                $passwd_chk = 'Y';
            }
        }



        if ($passwd_chk == 'N') { //Y이면 pass, 일단 체크
            //비밀기능 설정 여부..
            if (func_get_config($this->bbs_config, 'secret_yn') == 'Y') {

                if ($bbs_detail_list->num_rows() > 0) {

                    $passwd_chk_type = 'DETAIL';



                    //비밀글이고...공지글이 아닌 경우

                    if ($rows->secret_yn == 'Y' && $rows->notice_yn != 'Y') {



                        //로그인 상태이고..

                        if ($this->ci->data['session_member_id'] != '') {



                            //비밀글이고 글 등록아이디와 현재 세션 아이디가 같지 않다면...

                            if ($rows->member_id != $this->ci->data['session_member_id']) {

                                return $this->bbs_password($passwd_chk_type);
                            }
                        } else {

                            return $this->bbs_password($passwd_chk_type);
                        }
                    }
                }
            }
        }



        //이전글 다음글 설정 여부

        if (func_get_config($this->bbs_config, 'pre_next_view_yn') == 'Y') {



            //리스트에서 이전글 다음글을 찾는다.

            $prevList = $this->bbs_list('Y');



            $prev = NULL;

            $next = NULL;

            $current = false;

            $preCnt = 1;

            $prevPage = $prevList['cur_page'];

            $nextPage = $prevList['cur_page'];



            foreach ($prevList['prevList'] as $row) {

                if ($current == true) {

                    $next = $row;

                    if ($prevList['isPrev'] == "PREV") {

                        if ($preCnt >= ($prevList['page_rows'] + 2))
                            $nextPage++;
                    }

                    else {

                        if ($preCnt >= ($prevList['page_rows'] + 1))
                            $nextPage++;
                    }



                    break;
                }



                if ($row->idx == $idx) {

                    if ($prevList['isPrev'] == "PREV" && $preCnt == 2)
                        $prevPage--;



                    $current = true;
                }

                else {

                    $prev = $row;
                }



                $preCnt++;
            }



            func_set_data($this->ci, "prev_subject", '');

            func_set_data($this->ci, "next_subject", '');



            if ($prev != NULL) {

                $prev_link = "/?c=user&mcd=$this->mcd&me=bbs_detail&idx=$prev->idx&cur_page=$prevPage" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);

                func_set_data($this->ci, "prev_subject", "<a href=\"" . $prev_link . "\">" . $prev->subject . "</a>");
            }



            if ($next != NULL) {

                $next_link = "/?c=user&mcd=$this->mcd&me=bbs_detail&idx=$next->idx&cur_page=$nextPage" . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . urlencode($sParam);

                func_set_data($this->ci, "next_subject", "<a href=\"" . $next_link . "\">" . $next->subject . "</a>");
            }
        }



        //이전글 다음글 끝
        // 상황별 권한 읽어옴

        $grade = $this->_bbs_certi_check();



        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

            func_set_data($this->ci, 'survey_dup', false);



            // 응모가능 체크(IP)

            if (func_get_config($this->bbs_config, 'survey_dup_ip_yn') == 'Y') {

                if ($this->ci->survey->chk_entry_ip($idx, $this->mcd, $_SERVER['REMOTE_ADDR'])) {

                    //echo func_jsAlertReplace('중복 응모는 불가능합니다.', '/?c=user&mcd=' . $this->mcd);
                    //exit;
                    //$grade->survey_entry = false;

                    func_set_data($this->ci, 'survey_dup', true);
                }
            }



            // 응모가능 체크(cookie)

            if (func_get_config($this->bbs_config, 'survey_dup_ck_yn') == 'Y') {

                if (isset($_COOKIE['IW_SURVEY'][$idx]) && $_COOKIE['IW_SURVEY'][$idx] != '') {

                    //echo func_jsAlertReplace('중복 응모는 불가능합니다.', '/?c=user&mcd=' . $this->mcd);
                    //exit;
                    //$grade->survey_entry = false;

                    func_set_data($this->ci, 'survey_dup', true);
                }
            }



            // 응모가능 체크(아이디)

            if (func_get_config($this->bbs_config, 'survey_dup_id_yn') == 'Y') {

                if ($this->ci->data['session_member_id'] != '') { // 로그인일때
                    if ($this->ci->survey->chk_entry_id($idx, $this->mcd, $this->ci->data['session_member_id'])) {



                        //echo func_jsAlertReplace('중복 응모는 불가능합니다.', '/?c=user&mcd=' . $this->mcd);
                        //exit;
                        //$grade->survey_entry = false;



                        func_set_data($this->ci, 'survey_dup', true);
                    }
                }
            }



            $SurveyCfg = $this->ci->survey->config($idx, $this->mcd);



            if ($grade->survey_result) {

                $SurveyResult = $this->ci->survey->select($idx, $this->mcd);

                func_set_data($this->ci, 'survey_result', $SurveyResult);
            }



            func_set_data($this->ci, 'survey', $SurveyCfg);



            // 시행일 비교

            $publish_start = date_parse($rows->publish_start);

            $publish_start_mktime = mktime($publish_start['hour'], $publish_start['minute'], $publish_start['second'], $publish_start['month'], $publish_start['day'], $publish_start['year']);

            $publish_end = date_parse($rows->publish_end);

            $publish_end_mktime = mktime($publish_end['hour'], $publish_end['minute'], $publish_end['second'], $publish_end['month'], $publish_end['day'], $publish_end['year']);



            //var_dump(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
            //var_dump($publish_start_mktime, mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')), $publish_end_mktime);



            if (($publish_start_mktime <= mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'))) && (mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')) <= $publish_end_mktime)) {

                func_set_data($this->ci, 'survey_stat', true);
            } else {

                func_set_data($this->ci, 'survey_stat', false);
            }
        }

        $user_session = $this->ci->phpsession->get('member_no', 'USER');

        $bbs_attach_files = $this->ci->mo_bbs->select_attach_files($idx);

        $bbs_attach_list_bl = $this->ci->mo_bbs->select_attach_bl($rows->bl_no);

        func_set_data($this->ci, 'grade', $grade);   //권한

        func_set_data($this->ci, 'user_session', $user_session);

        func_set_data($this->ci, 'idx', $idx);

        func_set_data($this->ci, 'cur_page', $cur_page); //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);  //검색 string

        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);

        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list->result());

        func_set_data($this->ci, 'bbs_attach_files', $bbs_attach_files->result());

        func_set_data($this->ci, 'bbs_attach_list_bl', $bbs_attach_list_bl->result());

        func_set_data($this->ci, 'page_url', $page_url);

        func_set_data($this->ci, 'seller_detail', $seller_detail);

        return $this->ci->load->view('user/bbs/' . $this->mcd . '/view', $this->ci->data, true);
    }

    //게시판 비밀번호 HTML

    function bbs_password($passwd_chk_type = NULL) {

        if ($passwd_chk_type == NULL) {

            $passwd_chk_type = $this->ci->input->get_post('passwd_chk_type');
        }



        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');



        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;



        $sParam = $this->ci->input->get_post('sParam');  //검색 string



        func_set_data($this->ci, 'idx', $idx);

        func_set_data($this->ci, 'cur_page', $cur_page);   //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);    //검색 string

        func_set_data($this->ci, 'passwd_chk_type', $passwd_chk_type); //비밀번호 체크 구분(상세, 수정, 삭제)



        return $this->ci->load->view('user/bbs/' . $this->mcd . '/password', $this->ci->data, true);
    }

    //게시판 비밀번호 처리

    function bbs_password_exec() {

        $passwd_chk_type = $this->ci->input->get_post('passwd_chk_type');

        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');



        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;



        $sParam = $this->ci->input->get_post('sParam');  //검색 string

        $password = $this->ci->input->get_post('passwd');

        $query = $this->ci->user_bbs->password_check($idx, $this->mcd, $password);



        if ($query->num_rows() > 0) {

            $passwd_chk = 'Y';



            //게시판 상세

            if ($passwd_chk_type == 'DETAIL') {

                return $this->bbs_detail($passwd_chk);
            }



            //게시판 수정
            else if ($passwd_chk_type == 'UPDATE') {

                return $this->bbs_update($passwd_chk);
            }



            //게시판 삭제
            else if ($passwd_chk_type == 'DELETE') {

                return $this->bbs_delete($passwd_chk);
            }
        } else {

            echo func_jsAlertReplace('비밀번호가 일치하지 않습니다.', '/?c=user&mcd=' . $this->mcd . '&me=bbs_password&passwd_chk_type=' . $passwd_chk_type . '&idx=' . $idx . '&cur_page=' . $cur_page . '&sParam=' . urlencode($sParam));
        }
    }

    //admin 페이지 추가

    function bbs_admin() {



        // 상황별 권한 읽어옴

        $grade = $this->_bbs_certi_check();



        //권한체크

        $this->_bbs_certi_check('member_grade_write');

        func_set_data($this->ci, 'grade', $grade);



        return $this->ci->load->view('user/bbs/' . $this->mcd . '/admin', $this->ci->data, true);
    }

    //게시판 등록 HTML

    function bbs_write() {

        // 상황별 권한 읽어옴

        $grade = $this->_bbs_certi_check();



        //권한체크

        $this->_bbs_certi_check('member_grade_write');

        func_set_data($this->ci, 'grade', $grade);



        return $this->ci->load->view('user/bbs/' . $this->mcd . '/write', $this->ci->data, true);
    }

    //게시판 등록 처리

    function bbs_write_exec() {

        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴
        $inData['icon_status'] = "sale";
        $inData['menu_code'] = $this->mcd;    //메뉴코드

        $inData['member_id'] = $this->ci->data['session_member_id'];  //아이디

        $inData['writer_ip'] = $this->ci->input->ip_address();  //작성자 아이피

        $inData['visit_cnt'] = 0;    //조회수

        $inData['grp_idx'] = 0;    //그룹 idx

        $inData['order_no'] = 0;    //정렬 순서

        $inData['depth'] = 0;    //깊이



        $this->ci->load->library('sendsms'); //SMS
        // 사용자가 업로드 한 파일을 /static/user/ 디렉토리에 저장한다.

        $config['upload_path'] = './static/user';

        // git,jpg,png 파일만 업로드를 허용한다.

        $config['allowed_types'] = 'gif|jpg|png';

        // 허용되는 파일의 최대 사이즈

        $config['max_size'] = '100';

        // 이미지인 경우 허용되는 최대 폭

        $config['max_width'] = '1024';

        // 이미지인 경우 허용되는 최대 높이

        $config['max_height'] = '768';

        $this->ci->load->library('upload', $config);



        if (isset($inData['publish_start']) && isset($inData['publish_end'])) {

            $inData['publish_start'] = $inData['publish_start'] . ' 00:00:00';

            $inData['publish_end'] = $inData['publish_end'] . ' 23:59:59';
        }



        //우편번호

        if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

            $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

            unset($inData['zipcode1']);

            unset($inData['zipcode2']);
        }



        if (!isset($inData['notice_yn'])) {    //공지여부
            $inData['notice_yn'] = 'N';
        }



        if (!isset($inData['created_dt'])) {  //작성일
            $inData['created_dt'] = func_get_date_time();
        }



        // var_dump($inData);
        // exit;
        //DB 등록

        $idx = $this->ci->user_bbs->insert($inData);


        $upload_str_tr = '';

        $file_type_tr = '';


        // count image upload
        //$num_imageUpload = count($_FILES['file_type_sheet0']['name']);
        //$file_type_export = count($_FILES['file_type_export']['name']);
        //첨부파일을 사용한다면...
        //if ($attach_file_yn != 'N') {

        for ($i = 0; $i < 6; $i++) {

            //선택된 첨부 파일만...

            if ($_FILES['file_type_sheet' . $i]['tmp_name'][0] != '') {

                //파일 업로드

                $upload_str_tr = $this->ci->iwc_common->upload('file_type_sheet' . $i, 0, 'carItem', $this->bbs_config);



                //파일 업로드 실패

                if ($upload_str_tr != TRUE) {

                    show_error($this->ci->iwc_common->upload_error_msg());
                }

                //파일 업로드 성공
                else {

                    $upload_file_info_car = array();

                    $upload_file_info_car = $this->ci->iwc_common->upload_success_msg();

                    if ($_FILES['file_type_sheet0']['tmp_name'][0] != '' && $i == 0 || $i == 0) {
                        $upload_file_info_car['file_type'] = "inspect";
                    }
                    if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 1 || $i == 1) {
                        $upload_file_info_car['file_type'] = "export";
                    }
                    if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 2 || $i == 2) {
                        $upload_file_info_car['file_type'] = "cancel";
                    }
                    if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 3 || $i == 3) {
                        $upload_file_info_car['file_type'] = "register";
                    }
                    if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 4 || $i == 4) {

                        $upload_file_info_car['file_type'] = "original";
                    } 
                    if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 5 || $i == 5) {

                        $upload_file_info_car['file_type'] = "invoice";
                    }

                    $upload_file_info_car['bbs_idx'] = $idx;

                    $upload_file_info_car['created_dt'] = func_get_date_time();
                    $this->ci->user_bbs->add_detail_car($upload_file_info_car);
                }
            }
        }





        // 일렬번호 업데이트

        $serial = 0;



        /*

          if($this->mcd == 'product')

          {

          $serial = 120701;

          }

         */



        $Qry = "UPDATE iw_bbs

				SET car_stock_no =

					(SELECT CASE COALESCE(`car_stock_no`, 0)

						WHEN 0 THEN " . $serial . "

						ELSE `car_stock_no`

						END AS `car_stock_no`

						FROM

							(SELECT

								(MAX(car_stock_no) + 1) as `car_stock_no`

							FROM iw_bbs WHERE menu_code = '" . $this->mcd . "')

							AS `dumy`)

				WHERE idx = " . $idx . ";";

        //echo $Qry;
        //exit;



        $this->ci->db->query($Qry);



        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {



            // 설문조사 항목 저장

            $Survey = $this->ci->input->get_post('survey');



            if (isset($Survey)) {

                $this->ci->survey->config_insert($idx, $this->mcd, $Survey);
            }
        }



        //첨부파일 등록

        $attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn'); //첨부파일 사용여부

        $upload_str = '';



        //첨부파일을 사용한다면...

        if ($attach_file_yn != 'N') {

            for ($i = 0; $i < count($_FILES['attach1']['tmp_name']); $i++) {

                $upload_str = $this->ci->iwc_common->upload('attach1', $i, $this->mcd, $this->bbs_config);



                //파일 업로드 실패

                if ($upload_str != TRUE) {

                    show_error($this->ci->iwc_common->upload_error_msg());
                }

                //파일 업로드 성공
                else {

                    $upload_file_info = array();

                    $upload_file_info = $this->ci->iwc_common->upload_success_msg();



                    //첨부파일 DB 등록

                    $upload_file_info['idx'] = $idx;

                    $upload_file_info['menu_code'] = $this->mcd;



                    $this->ci->user_bbs->insert_attach($upload_file_info);
                }
            }
        }



        /*

          //첨부파일을 사용한다면...

          if ($attach_file_yn != 'N') {

          for ($i = 1; $i <= count($_FILES); $i++) {

          //선택된 첨부 파일만...



          if ($_FILES['attach' . $i]['tmp_name'] != '') {



          //파일 업로드

          $upload_str = $this->ci->iwc_common->upload('attach' . $i, $this->mcd, $this->bbs_config);



          //파일 업로드 실패

          if ($upload_str != TRUE) {

          show_error($this->ci->iwc_common->upload_error_msg());

          }

          //파일 업로드 성공

          else {

          $upload_file_info = array();

          $upload_file_info = $this->ci->iwc_common->upload_success_msg();



          //첨부파일 DB 등록

          $upload_file_info['idx'] = $idx;

          $upload_file_info['menu_code'] = $this->mcd;



          $this->ci->user_bbs->insert_attach($upload_file_info);

          }

          }

          }



          //print_r(count($_FILES));

          //var_dump($_FILES);

          //break;

          }

         */



        //이메일 사용 여부

        if (func_get_config($this->bbs_config, 'user_email_yn') == 'Y') {

            $this->ci->load->library('sendmail');  //메일
            //등록글

            func_set_data($this->ci, 'http_url', $this->ci->config->item('base_url'));

            func_set_data($this->ci, 'subject', '[' . func_get_config($this->bbs_config, 'bbs_code_name') . '] ' . $inData['subject']);

            func_set_data($this->ci, 'menu_code', $inData['menu_code']);

            func_set_data($this->ci, 'writer', $inData['writer']);

            func_set_data($this->ci, 'created_dt', func_get_date_format($inData['created_dt'], 'Y-m-d'));

            func_set_data($this->ci, 'branch', $inData['branch']);

            func_set_data($this->ci, 'country', $inData['country']);

            func_set_data($this->ci, 'email', $inData['email']);

            func_set_data($this->ci, 'contents', nl2br($inData['contents']));



            //func_set_data($this->ci, 'mobile_no1', nl2br($inData['mobile_no1']));
            //func_set_data($this->ci, 'mobile_no2', nl2br($inData['mobile_no2']));
            //func_set_data($this->ci, 'mobile_no3', nl2br($inData['mobile_no3']));



            $to_mails = array();

            $to_mails = explode('|', func_get_config($this->bbs_config, 'user_email_addr')); //설정 정보

            $to_mail = '';



            if (count($to_mails) > 0) {

                if ($inData['branch'] == 'ALL') {

                    for ($i = 0; $i < count($to_mails); $i++) {

                        if ($to_mails[$i] != '') {

                            $to_mail = $to_mails[$i];   //받는 관리자 이메일

                            $send_mail = isset($inData['email']) ? $inData['email'] : $to_mail;   //보내는 메일

                            $send_name = $inData['writer'];     //보내는 사람

                            $subject = '[' . func_get_config($this->bbs_config, 'bbs_code_name') . '] ' . $inData['writer'] . ' 님의 글이 등록되었습니다.';



                            //이메일 본문

                            $contents = $this->ci->load->view("common/user_writer_email", $this->ci->data, true);



                            //답변메일

                            $this->ci->sendmail->clear(TRUE);   //모든 이메일 변수들을 초기화  TRUE 시 첨부파일도 초기화

                            $this->ci->sendmail->setMailType("html");

                            $this->ci->sendmail->setInit();

                            $this->ci->sendmail->setFrom($send_mail, $send_name);

                            $this->ci->sendmail->setTo($to_mail);

                            $this->ci->sendmail->setSubject($subject);

                            $this->ci->sendmail->setMessage($contents);

                            $mail_result = $this->ci->sendmail->send(); //전송여부 TRUE FALSE 반환
                        }
                    }
                }



                if ($inData['branch'] == 'KOREA') {

                    $to_mail = $to_mails[0];   //받는 관리자 이메일
                }



                if ($inData['branch'] == 'USA') {

                    $to_mail = $to_mails[1];   //받는 관리자 이메일
                }



                if ($inData['branch'] == 'GERMANY') {

                    $to_mail = $to_mails[2];   //받는 관리자 이메일
                }



                if ($inData['branch'] == 'ENGLAND') {

                    $to_mail = $to_mails[3];   //받는 관리자 이메일
                }



                $send_mail = isset($inData['email']) ? $inData['email'] : $to_mail;   //보내는 메일

                $send_name = $inData['writer'];     //보내는 사람

                $subject = '[' . func_get_config($this->bbs_config, 'bbs_code_name') . '] ' . $inData['writer'] . ' 님의 글이 등록되었습니다.';



                //이메일 본문

                $contents = $this->ci->load->view("common/user_writer_email", $this->ci->data, true);



                //답변메일



                $this->ci->sendmail->clear(TRUE);   //모든 이메일 변수들을 초기화  TRUE 시 첨부파일도 초기화

                $this->ci->sendmail->setMailType("html");

                $this->ci->sendmail->setInit();

                $this->ci->sendmail->setFrom($send_mail, $send_name);

                $this->ci->sendmail->setTo($to_mail);

                $this->ci->sendmail->setSubject($subject);

                $this->ci->sendmail->setMessage($contents);

                $mail_result = $this->ci->sendmail->send(); //전송여부 TRUE FALSE 반환
                //echo $this->ci->sendmail->printDebugger();
                //} if문 생략
                //} for문 생략
            }
        }



        //SMS 사용 여부

        if (func_get_config($this->bbs_config, 'user_sms_yn') == 'Y') {

            $this->ci->load->library('sendsms'); //SMS

            for ($i = 1; $i <= 2; $i++) {

                $callphone1 = func_get_config($this->bbs_config, 'user_mobile_no1_' . $i); //호출번호 EX)"011"

                $callphone2 = func_get_config($this->bbs_config, 'user_mobile_no2_' . $i); //"234"

                $callphone3 = func_get_config($this->bbs_config, 'user_mobile_no3_' . $i); //"5678"



                if ($callphone1 != '' && $callphone2 != '' && $callphone3 != '') {

                    $callmessage = '[' . func_get_config($this->bbs_config, 'bbs_code_name') . '] ' . $inData['writer'] . ' 님의 글이 등록되었습니다.'; //80Byte

                    $rdate = "00000000";  //예약 날짜 EX) "20030617" 즉시 전송시 "00000000"

                    $rtime = "000000";  //예약 시간 EX) "190000"	 즉시 전송시 "000000"

                    $reqphone1 = isset($inData['mobile_no1']) ? $inData['mobile_no1'] : '';  //회신번호 EX) "011"

                    $reqphone2 = isset($inData['mobile_no2']) ? $inData['mobile_no2'] : ''; //회신번호 EX) "1111"

                    $reqphone3 = isset($inData['mobile_no3']) ? $inData['mobile_no3'] : ''; //회신번호 EX) "1111"

                    $callname = $inData['writer'];  //호출명



                    if ($reqphone1 != '' && $reqphone2 != '' && $reqphone3 != '') {

                        $sms_result = $this->ci->sendsms->sms_surem($callphone1, $callphone2, $callphone3, $callmessage, $rdate, $rtime, $reqphone1, $reqphone2, $reqphone3, $callname);



                        if ($sms_result) {

                            //SMS 발송 정보 DB 등록

                            $sms_data = array();

                            $sms_data['menu_code'] = $this->mcd;

                            $sms_data['menu_code_name'] = func_get_config($this->bbs_config, 'bbs_code_name');

                            $sms_data['call_phone'] = $callphone1 . '-' . $callphone2 . '-' . $callphone3;

                            $sms_data['req_phone'] = $reqphone1 . '-' . $reqphone2 . '-' . $reqphone3;

                            $sms_data['message'] = $inData['subject'];

                            $sms_data['result_code'] = $sms_result;

                            $sms_data['created_dt'] = func_get_date_time();



                            $this->ci->user_bbs->insert_sms($sms_data);
                        }
                    }
                }
            }
        }

        echo func_jsAlertReplace('Registration Completed', '/?c=user&mcd=' . $this->mcd);
    }

    //게시판 수정 HTML



    function bbs_update($passwd_chk = 'N') {



        //권한체크

        $this->_bbs_certi_check('member_grade_update');

        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');

        $sParam = $this->ci->input->get_post('sParam');   //검색 string

        $only_attach = $this->ci->input->get_post('only_attach'); //첨부파일 상세 삭제 여부

        $only_survey = $this->ci->input->get_post('only_survey'); //문항 상세 삭제 여부

        $bbs_detail_list = $this->ci->user_bbs->select_detail($idx, $this->mcd); //게시판 상세 조회
        // 관리자는 무조건 비밀글 읽을 수 있음

        if (isset($_SESSION['USER'])) {

            if ($_SESSION['USER']['grade_no'] == 1) {

                $passwd_chk = 'Y';
            }
        }



        if ($passwd_chk == 'N' && $only_attach == '') { //Y이면 패스
            //답변기능, 비밀기능 설정 여부..
            if (func_get_config($this->bbs_config, 'reply_yn') == 'Y' || func_get_config($this->bbs_config, 'secret_yn') == 'Y') {

                if ($bbs_detail_list->num_rows() > 0) {

                    $passwd_chk_type = 'UPDATE';



                    $rows = $bbs_detail_list->row();



                    //답변, 비밀 기능이고...공지글이 아닌 경우

                    if ($rows->notice_yn != 'Y') {

                        //로그인 상태이고..

                        if ($this->ci->data['session_member_id'] != '') {

                            //비밀글이고 글 등록아이디와 현재 세션 아이디가 같지 않다면...

                            if ($rows->member_id != $this->ci->data['session_member_id']) {

                                return $this->bbs_password($passwd_chk_type);
                            }
                        } else {

                            return $this->bbs_password($passwd_chk_type);
                        }
                    }
                }
            }
        }



        //수정폼에서 첨부 파일만 삭제 할 경우....

        if ($only_attach != 'N') {



            $att_idx = $this->ci->input->get_post('att_idx');

            $bbs_attach_detail_list = $this->ci->user_bbs->select_attach_detail($idx, $att_idx, $this->mcd); //첨부파일 상세 조회

            $this->ci->user_bbs->delete_attach($idx, $att_idx, $this->mcd); //첨부파일 삭제
            //업로드 실제 첨부파일 삭제

            if ($bbs_attach_detail_list->num_rows() > 0) {

                foreach ($bbs_attach_detail_list->result() as $rows) {



                    //첨부파일이 이미지인 경우 생성된 썸네일 까지 삭제

                    if ($rows->is_image) {

                        @unlink($rows->full_path); //원본 이미지 삭제



                        for ($i = 1; $i < 6; $i++) {  //썸네일 삭제
                            $thumb_path = $rows->file_path . $rows->raw_name . '_' . func_get_config($this->bbs_config, 'attach_thumbnail_size' . $i) . $rows->file_ext;



                            if (file_exists($thumb_path)) {

                                @unlink($thumb_path);
                            }
                        }
                    } else {

                        @unlink($rows->full_path);
                    }
                }
            }
        }



        $bbs_attach_list = $this->ci->user_bbs->select_attach($idx, $this->mcd); //첨부파일 조회(삭제 후 조회)
        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

            if ($only_survey == 'Y') { // 문항 삭제일 경우
                $survey_no = $this->ci->input->get_post('survey_no');

                $this->ci->survey->config_delete($idx, $this->mcd, $survey_no);
            }



            // 항목수 수정시

            $surveyCnt = $this->ci->input->get_post('survey_cnt');



            if (!is_numeric($surveyCnt)) {

                $surveyCnt = 0;
            }



            $SurveyCfg = $this->ci->survey->config($idx, $this->mcd, $surveyCnt);



            func_set_data($this->ci, 'survey', $SurveyCfg);
        }


        $bbs_attach_files = $this->ci->user_bbs->select_attach_files($idx);

        func_set_data($this->ci, 'idx', $idx);

        func_set_data($this->ci, 'cur_page', $cur_page); //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);  //검색 string

        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);

        func_set_data($this->ci, 'bbs_attach_files', $bbs_attach_files->result());

        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list->result());



        return $this->ci->load->view('user/bbs/' . $this->mcd . '/update', $this->ci->data, true);
    }

    //게시판 수정 처리

    function bbs_update_exec() {

        $cur_page = $this->ci->input->get_post('cur_page');

        $sParam = $this->ci->input->get_post('sParam');   //검색 string




        $idx = $this->ci->input->get_post('idx');

        for ($i = 0; $i < 5; $i++) {

            //선택된 첨부 파일만...

            if ($_FILES['file_type_sheet' . $i]['tmp_name'][0] != '') {
                $file_type = 'inspect';
                if ($i == 1) {
                    $file_type = 'export';
                } elseif ($i == 2) {
                    $file_type = 'cancel';
                } elseif ($i == 3) {
                    $file_type = 'register';
                } elseif ($i == 4) {
                    $file_type = 'original';
                } elseif ($i == 5) {
                    $file_type = 'invoice';
                }
                //======= create delete image========            

                $bbs_attach_detail_list = $this->ci->user_bbs->select_attach_files($idx, $file_type);   //첨부파일 상세 조회
                //업로드 실제 첨부파일 삭제

                if ($bbs_attach_detail_list->num_rows() > 0) {

                    foreach ($bbs_attach_detail_list->result() as $rows) {

                        //첨부파일이 이미지인 경우 생성된 썸네일 까지 삭제

                        $this->ci->user_bbs->delete_attach_file($rows->id, $idx);   //첨부파일 삭제

                        @unlink($rows->full_path);
                    }
                }


                //파일 업로드

                $upload_str_tr = $this->ci->iwc_common->upload('file_type_sheet' . $i, 0, 'carItem', $this->bbs_config);



                //파일 업로드 실패

                if ($upload_str_tr != TRUE) {

                    show_error($this->ci->iwc_common->upload_error_msg());
                }

                //파일 업로드 성공
                else {

                    $upload_file_info_car = array();

                    $upload_file_info_car = $this->ci->iwc_common->upload_success_msg();

                    if ($_FILES['file_type_sheet0']['tmp_name'][0] != '' && $i == 0 || $i == 0) {
                        $upload_file_info_car['file_type'] = "inspect";
                    }
                    if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 1 || $i == 1) {
                        $upload_file_info_car['file_type'] = "export";
                    }
                    if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 2 || $i == 2) {
                        $upload_file_info_car['file_type'] = "cancel";
                    }
                    if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 3 || $i == 3) {
                        $upload_file_info_car['file_type'] = "register";
                    }
                    if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 4 || $i == 4) {

                        $upload_file_info_car['file_type'] = "original";
                    }
                    if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 5 || $i == 5) {

                        $upload_file_info_car['file_type'] = "invoice";
                    }

                    $upload_file_info_car['bbs_idx'] = $idx;

                    $upload_file_info_car['created_dt'] = func_get_date_time();
                    $this->ci->user_bbs->add_detail_car($upload_file_info_car);
                }
            }
        }


        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴

        $inData['idx'] = $this->ci->input->get_post('idx');  //게시물 일련번호

        $inData['menu_code'] = $this->mcd;    //메뉴코드
        //var_dump($inData['car_sale_yn']);
        //exit;
        //$inData['member_id']  = $this->ci->data['session_member_id'];	 //아이디

        $inData['writer_ip'] = $this->ci->input->ip_address();  //작성자 아이피
        //우편번호

        if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

            $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

            unset($inData['zipcode1']);

            unset($inData['zipcode2']);
        }

        if (isset($inData['publish_start']) && isset($inData['publish_end'])) {

            $inData['publish_start'] = $inData['publish_start'] . ' 00:00:00';

            $inData['publish_end'] = $inData['publish_end'] . ' 23:59:59';
        }



        // var_dump($inData);
        // exit;
        //DB 수정

        $this->ci->user_bbs->update($inData);



        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

            // 설문조사 항목 저장

            $Survey = $this->ci->input->get_post('survey');

            if (isset($Survey)) {

                $this->ci->survey->config_insert($inData['idx'], $this->mcd, $Survey);
            }
        }





        //첨부파일 등록

        $attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn'); //첨부파일 사용여부

        $upload_str = '';



        if ($attach_file_yn != 'N') {



            if ($_FILES['attach1']['error'][0] == 0) {

                for ($i = 0; $i < count($_FILES['attach1']['tmp_name']); $i++) {

                    $upload_str = $this->ci->iwc_common->upload('attach1', $i, $this->mcd, $this->bbs_config);



                    //파일 업로드 실패

                    if ($upload_str != TRUE) {

                        show_error($this->ci->iwc_common->upload_error_msg());
                    }

                    //파일 업로드 성공
                    else {

                        $upload_file_info = array();

                        $upload_file_info = $this->ci->iwc_common->upload_success_msg();



                        //첨부파일 DB 등록

                        $upload_file_info['idx'] = $inData['idx']; //게시물 일련번호

                        $upload_file_info['menu_code'] = $this->mcd;



                        $this->ci->user_bbs->insert_attach($upload_file_info);
                    }
                }
            }
        }

        /*

          //첨부파일을 사용한다면...

          if ($attach_file_yn != 'N') {

          for ($i = 1; $i <= count($_FILES); $i++) {

          //선택된 첨부 파일만...

          if ($_FILES['attach' . $i]['tmp_name'] != '') {



          //파일 업로드

          $upload_str = $this->ci->iwc_common->upload('attach' . $i, 'update', $this->mcd, $this->bbs_config);



          //파일 업로드 실패

          if ($upload_str != TRUE) {

          show_error($this->ci->iwc_common->upload_error_msg());

          }



          //파일 업로드 성공

          else {

          $upload_file_info = array();

          $upload_file_info = $this->ci->iwc_common->upload_success_msg();



          //첨부파일 DB 등록

          $upload_file_info['idx'] = $inData['idx']; //게시물 일련번호

          $upload_file_info['menu_code'] = $this->mcd;



          $this->ci->user_bbs->insert_attach($upload_file_info);

          }

          }

          }

          }

         */



        echo func_jsAlertReplace('Modification Completed.', '/?c=user&mcd=' . $this->mcd . '&me=bbs_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
    }

    //게시판 답변 HTML

    function bbs_reply() {



        //권한체크

        $this->_bbs_certi_check('member_grade_reply');



        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');

        $sParam = $this->ci->input->get_post('sParam');  //검색 string



        $bbs_detail_list = $this->ci->user_bbs->select_detail($idx, $this->mcd); //상세 조회

        $bbs_attach_list = $this->ci->user_bbs->select_attach($idx, $this->mcd); //첨부파일 조회



        func_set_data($this->ci, 'idx', $idx);

        func_set_data($this->ci, 'cur_page', $cur_page); //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);  //검색 string

        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);

        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list->result());



        return $this->ci->load->view('user/bbs/' . $this->mcd . '/reply', $this->ci->data, true);
    }

    //게시판 답변 처리 bbs_update_exec

    

    function bbs_updatepassword_exec() {
        $member_id = $this->ci->input->get_post('namemember');
        $oldpassword = func_base64_encode($this->ci->input->get_post('oldpassword'));
        $newpassword = func_base64_encode($this->ci->input->get_post('newpassword'));
        $confirmpassword = func_base64_encode($this->ci->input->get_post('confirmpassword'));

        if ($oldpassword == '' || $newpassword == '' || $confirmpassword == '') {
            echo func_jsAlertReplace('Please fill all data.', '/?c=user&mcd=edit#change_password');
            exit('</center></h1>Please fill all data.</center></h1>');
        }

        $check = $this->ci->user_bbs->getoldpassword($member_id, $oldpassword);

        if ($check >= 1) {

            if ($newpassword == $confirmpassword) {
                $sqlpassword = mysql_query("UPDATE iw_member SET member_pwd = '{$newpassword}' WHERE member_id = '{$member_id}'");
                echo func_jsAlertReplace('Password has been modified.', '/?c=user&mcd=edit');
            } else {
                echo func_jsAlertReplace('Your Password not match.', '/?c=user&mcd=edit#change_password');
                exit('</center></h1>Your Password not match.</center></h1>');
            }
        } else {
            echo func_jsAlertReplace('Your Current password not match.', '/?c=user&mcd=edit#change_password');
            exit('</center></h1>Your Current password not match.</center></h1>');
        }
    }

    function bbs_updateuser_exec() {

        $member_id = $this->ci->input->get_post('sessionname');

        $inData = array();

        //$password = $this->ci->input->get_post('member_pwd');
        $inData = func_get_post_array($this->ci->input->post('values'));
        
        $values = $this->ci->input->post('values');
        
        $mobile_no = $values['mobile_no1'];
        $length = strpos($mobile_no," ");
        $mobile1 = substr($mobile_no,1,$length);        
        $inData['mobile_no1'] = $mobile1;
        $inData['mobile_no2']=substr(preg_replace('/\D/','',$mobile_no),$length - 1,$mobile1 - ($length - 20));

        $phone_no = $values['phone_no1'];
        $length1 = strpos($phone_no," ");
        $phone1 = substr($phone_no,1,$length1); 
        $inData['phone_no1'] = $phone1;       
        $inData['phone_no2']=substr(preg_replace('/\D/','',$phone_no),$length1 - 1,$mobile1 - ($length1 - 20));


        if ($inData['sex'] == '' || $inData['member_first_name'] == '' || $inData['member_country'] == '') {

            echo func_jsAlertReplace('Please fill all data.', '/?c=user&amp;mcd=edit');
            exit('<h1><center>Please fill all data</center></h1>');
        } else {

            $inData['grade_no'] = func_base64_decode($inData['grade_no']);

            //check value country 
            $CC = $inData['member_country'];
            $getcc = mysql_query("SELECT id FROM iw_country_list WHERE cc = '{$CC}'");
            $row_cnt = mysql_num_rows($getcc);

            if ($row_cnt == 0) {
                echo func_jsAlertReplace('Value Country has been change can not signup.', '/?c=user&mcd=edit');
                exit('<h1><center>Value Country has been change can not signup</center></h1>');
            }
            //end check

            $letter = $inData['member_first_name'];
            if (!preg_match('/^[\p{L} ]+$/u', $letter) == 1) {
                exit('<h1><center>Name allow only letter</center></h1>');
            }

            if ($inData['sex'] == 'M' || $inData['sex'] == 'W') {

                //$inData['member_pwd'] = func_base64_encode($inData['member_pwd']);

                $inData['created_dt'] = func_get_date_time();

                $this->ci->user_bbs->updateuser($inData, $member_id);

                echo func_jsAlertReplace('User has been modified successfully.', './');
            } else {
                echo func_jsAlertReplace('Value Gender has been change can not signup.', '/?c=user&mcd=edit');
                exit('</center></h1>Value Gender has been change can not signup</center></h1>');
            }
        }
    }

    
    
    function bbs_reply_exec() {

        $idx = $this->ci->input->get_post('idx');   //부모 idx

        $cur_page = $this->ci->input->get_post('cur_page'); //현재 페이지

        $sParam = $this->ci->input->get_post('sParam');   //검색 string
        //부모글 상세 조회

        $bbs_detail_list = $this->ci->user_bbs->select_detail($idx, $this->mcd);

        $par_rows = '';



        if (count($bbs_detail_list) > 0) {

            $par_rows = $bbs_detail_list->row();
        }



        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴

        $inData['menu_code'] = $this->mcd;    //메뉴코드

        $inData['member_id'] = func_decode(func_get_config($this->bbs_config, 'reply_id_yn'), 'N', $this->ci->data['session_member_id'], $par_rows->member_id);  //아이디

        $inData['writer_ip'] = $this->ci->input->ip_address();  //작성자 아이피

        $inData['visit_cnt'] = 0;    //조회수

        $inData['grp_idx'] = $par_rows->grp_idx;  //그룹 idx

        $inData['order_no'] = $par_rows->order_no + 1;   //정렬 순서

        $inData['depth'] = $par_rows->depth + 1;   //깊이

        $inData['created_dt'] = func_get_date_time();   //작성일

        $inData['notice_yn'] = 'N';     //공지글여부(고정) - 답변등록시
        //우편번호

        if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

            $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

            unset($inData['zipcode1']);

            unset($inData['zipcode2']);
        }



        //게시판 하위글 등록시 정렬 순서 업데이트

        $this->ci->user_bbs->update_order_no($par_rows->grp_idx, $par_rows->order_no);



        //DB 등록(답변 등록시 부모글 reply_yn = 'Y'로 변경

        $idx = $this->ci->user_bbs->insert_reply($inData);



        //첨부파일 등록

        $attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn'); //첨부파일 사용여부

        $upload_str = '';



        //첨부파일을 사용한다면...

        if ($attach_file_yn != 'N') {

            for ($i = 1; $i <= count($_FILES); $i++) {

                //선택된 첨부 파일만...

                if ($_FILES['attach' . $i]['tmp_name'] != '') {

                    //파일 업로드

                    $upload_str = $this->ci->iwc_common->upload('attach' . $i, $this->mcd, $this->bbs_config);



                    //파일 업로드 실패

                    if ($upload_str != TRUE) {

                        show_error($this->ci->iwc_common->upload_error_msg());
                    }

                    //파일 업로드 성공
                    else {

                        $upload_file_info = array();

                        $upload_file_info = $this->ci->iwc_common->upload_success_msg();



                        //첨부파일 DB 등록

                        $upload_file_info['idx'] = $idx;

                        $upload_file_info['menu_code'] = $this->mcd;



                        $this->ci->user_bbs->insert_attach($upload_file_info);
                    }
                }
            }
        }

        echo func_jsAlertReplace('답변이 등록 되었습니다.', '/?c=user&mcd=' . $this->mcd . '&me=bbs_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
    }

    //게시판 삭제

    function bbs_delete($passwd_chk = 'N') {

        //권한체크

        $this->_bbs_certi_check('member_grade_delete');



        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');

        $sParam = $this->ci->input->get_post('sParam');   //검색 string



        $bbs_detail_list = $this->ci->user_bbs->select_detail($idx, $this->mcd); //상세 조회
        // 관리자는 무조건 삭제 할 수 있음

        if ($this->ci->data['session_grade_no'] == 1) {

            $passwd_chk = 'Y';
        }



        if ($passwd_chk != 'Y') { //Y이면 pass, 일단 체크
            //답변기능, 비밀글 기능 설정 여부..
            if (func_get_config($this->bbs_config, 'reply_yn') == 'Y' || func_get_config($this->bbs_config, 'secret_yn') == 'Y') {

                if ($bbs_detail_list->num_rows() > 0) {

                    $passwd_chk_type = 'DELETE';



                    $rows = $bbs_detail_list->row();



                    //공지글이 아닌 경우

                    if ($rows->notice_yn != 'Y') {

                        //로그인 상태이고..

                        if ($this->ci->data['session_member_id'] != '') {

                            //비밀글이고 글 등록아이디와 현재 세션 아이디가 같지 않다면...

                            if ($rows->member_id != $this->ci->data['session_member_id']) {

                                return $this->bbs_password($passwd_chk_type);
                            }
                        } else {

                            return $this->bbs_password($passwd_chk_type);
                        }
                    }
                }
            }
        }



        $par_rows = '';



        if ($bbs_detail_list->num_rows() > 0) {

            $par_rows = $bbs_detail_list->row();
        }



        $child_list = $this->ci->user_bbs->delete_child_yn($par_rows->grp_idx, $this->mcd, $par_rows->depth); //하위글 조회

        $child_rows = '';



        //해당 글의 하위 글이 있으면...

        if ($child_list->num_rows() > 0) {

            $child_rows = $child_list->row();



            if ($child_rows->depth > $par_rows->depth) {

                echo func_jsAlertReplace('하위글이 존재하여 삭제할 수 없습니다.', '/?c=user&mcd=' . $this->mcd . '&me=bbs_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

                return;
            }
        }



        $bbs_attach_list = $this->ci->user_bbs->select_attach($idx, $this->mcd); //첨부파일 조회

        $this->ci->user_bbs->delete($idx, $this->mcd);   //게시판 삭제(첨부파일 DB 자동삭제)
        //업로드 실제 첨부파일 삭제

        if ($bbs_attach_list->num_rows() > 0) {

            foreach ($bbs_attach_list->result() as $rows) {

                //첨부파일이 이미지인 경우 생성된 썸네일 까지 삭제

                $bbs_attach_detail_list = $this->ci->user_bbs->select_attach_files($idx);   //???? ?? ??

                if ($bbs_attach_detail_list->num_rows() > 0) {

                    foreach ($bbs_attach_detail_list->result() as $rowstype) {

                        @unlink($rowstype->full_path);
                    }
                }

                $this->ci->user_bbs->delete_file_images($idx);



                if ($rows->is_image) {

                    unlink($rows->full_path); //원본 이미지 삭제

                    for ($i = 1; $i < 6; $i++) {  //썸네일 삭제
                        $thumb_path = $rows->file_path . $rows->raw_name . '_' . func_get_config($this->bbs_config, 'attach_thumbnail_size' . $i) . $rows->file_ext;

                        if (file_exists($thumb_path)) {

                            unlink($thumb_path);
                        }
                    }
                } else {

                    unlink($rows->full_path);
                }
            }
        }



        echo func_jsAlertReplace('Deletion Completed.', '/?c=user&mcd=' . $this->mcd . '&me=bbs_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
    }

    //게시판 코멘트 등록 처리 - 기본버전

    function bbs_comment_write_exec() {



        //권한체크



        $this->_bbs_certi_check('member_grade_comment');

        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');



        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;



        $sParam = $this->ci->input->get_post('sParam');  //검색 string

        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values', TRUE));  //POST 배열을 array['key'] = value 형태로 리턴

        $inData['idx'] = $idx;   //게시판 일련번호

        $inData['menu_code'] = $this->mcd;   //메뉴코드

        $inData['member_id'] = $this->ci->data['session_member_id']; //아이디

        $inData['writer_ip'] = $this->ci->input->ip_address(); //작성자 아이피

        $inData['created_dt'] = func_get_date_time();  //작성일



        $comment_reply_yn = func_get_config($this->bbs_config, 'comment_reply_yn'); //코멘트 등록시 답변처리로 간주 여부
        //DB 등록

        $this->ci->user_bbs->insert_comment($inData, $comment_reply_yn);

        $link = "/?c=user&mcd=" . $this->mcd . "&me=bbs_detail&idx=" . $idx . "&cur_page=" . $cur_page . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . $sParam;



        echo func_jsAlertReplace('등록 되었습니다.', $link);
    }

    //게시판 코멘트 삭제

    function bbs_comment_delete() {

        $password_chk = $this->ci->input->get_post('password_chk');  //비밀번호

        $cm_idx = $this->ci->input->get_post('cm_idx');

        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');



        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;



        $sParam = $this->ci->input->get_post('sParam');  //검색 string
        //비밀번호

        $query = $this->ci->user_bbs->select_password($cm_idx, $idx, $this->mcd, $password_chk);

        $link = "/?c=user&mcd=" . $this->mcd . "&me=bbs_detail&idx=" . $idx . "&cur_page=" . $cur_page . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . $sParam;



        $have_authority = false;

        if (isset($_SESSION['USER'])) {

            if ($_SESSION['USER']['grade_no'] == 1) {

                $have_authority = true;
            }
        }



        //비밀번호 정보 OK

        if ($query->num_rows() > 0 || $have_authority) {

            //DB 삭제

            $this->ci->user_bbs->delete_comment($cm_idx, $idx, $this->mcd);

            echo func_jsAlertReplace('삭제 되었습니다.', $link);
        } else {

            echo func_jsAlertReplace('비밀번호가 올바르지 않습니다.', $link);
        }
    }

    //게시판 Summary를 조회한다.

    function get_summary($menu_code = NULL) {


        //해당 모듈의 생성자에서 셋팅된 정보 리셋

        $this->mcd = '';

        $this->bbs_config = '';

        unset($this->ci->data['bbs_config_result']);



        //게시판 Summary를 메뉴코드

        $this->mcd = $menu_code;



        //게시판 Summary를 설정정보

        $this->bbs_config = $this->ci->iw_config->select_bbs_config($this->mcd)->result();

        $where = array(); //sql where 조건
        //페이지 조건



        $cur_page = $this->ci->input->get_post("cur_page");



        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;



        $page_rows = (integer) func_get_config($this->bbs_config, 'summary_page_rows');  //페이지 로우수
        //게시판 일반 목록 조회



        $bbs_list = $this->ci->user_bbs->select($this->mcd, 'N', ($cur_page - 1) * $page_rows, $page_rows, $where);







        func_set_data($this->ci, 'mcd', $this->mcd);



        func_set_data($this->ci, 'bbs_config_result', $this->bbs_config);



        func_set_data($this->ci, 'bbs_list', $bbs_list->result());







        return $this->ci->load->view('user/bbs/summary_' . $this->mcd . '/summary', $this->ci->data, true);
    }

    //게시판 권한 체크



    function _bbs_certi_check($var = '') {



        if ($var == '') { // 권한별 ture / false 반환함
            foreach ($this->bbs_config as $Gkey => $Gval) {



                if (strstr($Gval->key, 'member_grade_')) {



                    $Key = substr($Gval->key, 13);


                    //$Gval->key = substr($Gval->key, 13);



                    $Gval_exp = explode('|', $Gval->value);

                    ///////////////////////////////////////
                    if (!isset($grade))
                        $grade = new stdClass();
                    ///////////////////////////////////////

                    $grade->{$Key} = false;

                    if ($Gval_exp[0] == '') {



                        $grade->{$Key} = true;
                    } else {



                        if ($this->ci->data['session_grade_no'] != '') { // 로그인상태
                            if (func_get_config($this->bbs_config, 'member_grade_fix') == 'Y') { // 고정권한 사용시
                                if ($Gval_exp[0] == $this->ci->data['session_grade_no']) {



                                    $grade->{$Key} = true;
                                }
                            } else {



                                if ($Gval_exp[0] >= $this->ci->data['session_grade_no']) {



                                    $grade->{$Key} = true;
                                }
                            }
                        }
                    }
                }



                unset($Gkey, $Gval, $Gval_exp);
            }



            reset($this->bbs_config);







            if (isset($_REQUEST['mcd']) && $_REQUEST['mcd'] == 'product' && isset($_REQUEST['category']) && $_REQUEST['category'] == 'auction' && @$_SESSION['USER']['grade_no'] == 10) {



                $grade->write = false;
            }







            if (isset($grade)) {



                return $grade;
            } else {



                return false;
            }
        }







        //var_dump($grade);
        //exit;
        //권한체크



        if ($this->ci->data['session_grade_no'] != 1) { //관리자만 아니면.. 모두 체크
            if (!func_bbs_certi_check($this->bbs_config, $var, $this->ci->data['session_grade_no'])) {



                $msg = '';



                $tmp = explode('|', func_get_config($this->bbs_config, $var));







                //권한 선택 했을 경우



                if ($tmp[0] != '') {



                    $grade_name = $tmp[1];



                    $pre_url = func_get_referer_url();



                    //$pre_url	= func_get_current_url();
                    //회원권한 고정이 아니면..



                    if (func_get_config($this->bbs_config, 'member_grade_fix') != 'Y') {



                        //$msg = '권한이 ['.$grade_name.'] 이상인 회원님에게 존재합니다.';



                        $msg = '로그인 후 이용하세요.';
                    } else {

                        //$msg = '권한이 ['.$grade_name.'] 회원님에게만 존재합니다.';

                        $msg = '로그인 후 이용하세요.';
                    }











                    //권한 체크 후 리턴 URL.. 로그인 페이지로 설정 되었다면..



                    if (func_get_config($this->bbs_config, 'member_login_return_url') != 'N') {



                        if ($this->ci->data['is_login'] == 'Y') {



                            if ($var == 'member_grade_list') {



                                echo func_jsAlertReplace($msg, $pre_url != '' ? $pre_url : '/');
                            } else {



                                echo func_jsAlertReplace($msg, $pre_url != '' ? $pre_url : '/');



                                //echo func_jsAlertHistoryGo($msg);
                            }
                        } else {



                            echo func_jsAlertReplace($msg, '/?c=user&mcd=' . $this->ci->data['member_login_mcd'] . '&return_url=' . func_get_current_url());
                        }



                        exit;
                    } else {



                        //리스트 페이지



                        if ($var == 'member_grade_list') {



                            echo func_jsAlertReplace($msg, $pre_url != '' ? $pre_url : '/');
                        } else { // 권한 없음
                            echo func_jsAlertReplace($msg, $pre_url != '' ? $pre_url : '/');



                            //echo func_jsAlertHistoryGo($msg);
                        }



                        exit;
                    }
                }
            }
        }
    }

    // 설문조사 등록 처리



    function survey_write_exec() {



        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {



            // 설문조사 항목 저장

            $Survey = $this->ci->input->get_post('survey');

            $inData['bbs_idx'] = $this->ci->input->get_post('idx');

            $inData['menu_code'] = $this->mcd;

            $inData['member_id'] = $this->ci->data['session_member_id'];  //아이디
            // 응모가능 체크(IP)

            if (func_get_config($this->bbs_config, 'survey_dup_ip_yn') == 'Y') {

                if ($this->ci->survey->chk_entry_ip($inData['bbs_idx'], $inData['menu_code'], $_SERVER['REMOTE_ADDR'])) {

                    echo func_jsAlertReplace('중복 응모는 불가능합니다.', '/?c=user&mcd=' . $this->mcd);

                    exit;
                }
            }



            // 응모가능 체크(cookie)

            if (func_get_config($this->bbs_config, 'survey_dup_ck_yn') == 'Y') {

                if (isset($_COOKIE[$inData['bbs_idx']]) && $_COOKIE[$inData['bbs_idx']] != '') {

                    echo func_jsAlertReplace('중복 응모는 불가능합니다.', '/?c=user&mcd=' . $this->mcd);

                    exit;
                }
            }



            // 응모가능 체크(아이디)

            if (func_get_config($this->bbs_config, 'survey_dup_id_yn') == 'Y') {

                if ($this->ci->data['session_member_id'] != '') { // 로그인일때
                    if ($this->ci->survey->chk_entry_ip($inData['bbs_idx'], $inData['menu_code'], $this->ci->data['session_member_id'])) {

                        echo func_jsAlertReplace('중복 응모는 불가능합니다.', '/?c=user&mcd=' . $this->mcd);

                        exit;
                    }
                }
            }



            if (isset($Survey['custom']) && is_array($Survey['custom'])) {

                foreach ($Survey['custom'] as $Skey => $Sval) {

                    $inData['custom' . $Skey] = $Sval;
                }

                unset($Skey, $Sval);
            }



            foreach ($Survey['field'] as $Skey => $Sval) {



                // 단일선택의 기타

                if (isset($Sval[0]) && $Sval[0] == '__ETC__') {

                    $inData['field' . $Skey] = $Sval['__ETC__'];
                } else {

                    $inData['field' . $Skey] = '';



                    // 단일이든 다중이든 주관식이든 싸잡아서

                    foreach ($Sval as $Vkey => $Vval) {

                        if ($inData['field' . $Skey] != '' && $Vval != '') {

                            $inData['field' . $Skey] .= ',';
                        }

                        $inData['field' . $Skey] .= $Vval;
                    }



                    // 기타

                    /*

                      if(isset($Sval['__ETC__']) && $Sval['__ETC__'] != '') {

                      if($inData['field'.$Skey] != '') {

                      $inData['field'.$Skey] .= ',';

                      }

                      $inData['field'.$Skey] .= $Sval['__ETC__'];

                      }

                     */



                    // 결과 없을경우 unset

                    if (isset($inData['field' . $Skey]) && $inData['field' . $Skey] == '') {

                        unset($inData['field' . $Skey]);
                    }

                    unset($Vkey, $Vval);
                }

                unset($Skey, $Sval);
            }

            unset($Survey);



            //var_dump($inData);
            //exit;
            // DB 등록

            $this->ci->survey->insert($inData);



            // 쿠키등록

            setcookie('IW_SURVEY[' . $inData['bbs_idx'] . ']', date('Y-m-d H:i:s'), time() + (3600 * 24 * 365), '/', $_SERVER['SERVER_NAME']);  /* expire in 1 year */
        }

        echo func_jsAlertReplace('등록되었습니다.', '/?c=user&mcd=' . $this->mcd);
    }

    // wishlist html(조회)

    function bbs_wishlist($prev_next = 'N') {

        //권한체크

        $this->_bbs_certi_check('member_grade_list');

        $where = array(); //sql where 조건

        $where2 = array(); //sql where 조건

        $sParam = '';   //검색 string

        $order_by = '';



        $member_id = $this->ci->data['session_member_id'];



        $member_detail_list = $this->ci->user_member->member_select_detail($member_id);

        $rows = '';

        if (count($member_detail_list) > 0) {

            $rows = $member_detail_list->row();
        }



        $member_wishlist = $rows->member_wishlist;

        if ($member_wishlist != '') {

            $inValues = str_replace('^', ',', $member_wishlist);



            // syntax 관리 >>>

            $inValues = preg_replace('/,{2,}/', ',', $inValues);



            // 앞뒤에 ',' 제거 >>>

            $i = 0;

            while (substr($inValues, 0, 1) == ',') {

                $inValues = substr_replace($inValues, '', 0, 1);



                // 혹시 몰라서.. 무한루프 방지.

                $i++;

                if ($i > 100) {

                    break;
                }
            }

            $i = 0;

            while (substr($inValues, -1) == ',') {

                $inValues = substr_replace($inValues, '', -1, 1);



                // 혹시 몰라서.. 무한루프 방지.

                $i++;

                if ($i > 100) {

                    break;
                }
            }

            // 앞뒤에 ',' 제거 <<<
            // syntax 관리 <<<



            $where2['car_stock_no'] = 'car_stock_no in (' . $inValues . ')';

            $order_by = 'FIELD(car_stock_no, ' . $inValues . ') desc';
        }



        //페이지 조건

        $cur_page = $this->ci->input->get_post("cur_page");

        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;



        $page_rows = 10;  //페이지 로우수

        $page_views = (integer) func_get_config($this->bbs_config, 'page_views'); //페이지 네비게이션 수
        //게시판 일반 목록 조회

        $bbs_list = $this->ci->user_bbs->select_wishlist($this->mcd, 'N', ($cur_page - 1) * $page_rows, $page_rows, $where2, $order_by, $member_id);



        $total_rows = $this->ci->user_bbs->total_select($this->mcd, $where, $where2, $member_id); //조회 전체 목록수

        $row_cnt = $total_rows - ($cur_page - 1) * $page_rows;  //글 번호
        //페이지정보 조회

        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



        //이전글 다음글 조회

        if ($prev_next == 'Y') {

            $pn_data = array();

            $pn_data['isPrev'] = '';



            $prev = ($cur_page - 1) * $page_rows;



            if ($prev > 0) {

                $pn_data['isPrev'] = "PREV";

                $prev = $prev - 1;
            }



            $pn_data['cur_page'] = $cur_page;

            $pn_data['page_views'] = $page_views;

            $pn_data['page_rows'] = $page_rows;

            $pn_data['total_rows'] = $total_rows;

            $pn_data['row_cnt'] = $row_cnt;



            if ($pn_data['isPrev'] == 'PREV') {

                $page_rows = $page_rows + 2;
            } else {

                $page_rows = $page_rows + 1;
            }



            $pn_data['prevList'] = $this->ci->user_bbs->select_wishlist($this->mcd, 'N', $prev, $page_rows, $where, $order_by, $member_id)->result();



            return $pn_data;
        }



        func_set_data($this->ci, 'grade', $this->_bbs_certi_check());   //권한

        func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수

        func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지

        func_set_data($this->ci, 'row_cnt', $row_cnt);   //글번호

        func_set_data($this->ci, 'cur_page', $cur_page); //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);  //검색 string
        //func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list->result());

        func_set_data($this->ci, 'bbs_list', $bbs_list->result());

        func_set_data($this->ci, 'page_info', $page_info);



        return $this->ci->load->view('user/bbs/' . $this->mcd . '/wishlist', $this->ci->data, true);
    }

    // wishlist 추가

    function add_wishlist() {



        $inData = array();

        $inData['member_id'] = $this->ci->data['session_member_id'];

        $car_stock_no = $this->ci->input->get('car_stock_no');



        if ($inData['member_id'] != '' && $car_stock_no != '') {

            // member의 wishlist를 가져온다 >>>

            $member_detail_list = $this->ci->user_member->member_select_detail($inData['member_id']);

            $rows = '';

            if (count($member_detail_list) > 0) {

                $rows = $member_detail_list->row();
            }

            $inData['member_wishlist'] = $rows->member_wishlist;

            // member의 wishlist를 가져온다 <<<
            // 저장할 위시리스트 만들기 >>>

            if ($inData['member_wishlist'] == '') {

                $inData['member_wishlist'] = $car_stock_no;
            } else {

                if (strpos($inData['member_wishlist'], $car_stock_no) === false) {

                    $inData['member_wishlist'] .= '^' . $car_stock_no;
                } else {

                    echo func_jsAlertReplace('This product is already added in wishlist.', '/?c=user&mcd=' . $this->mcd . '&me=bbs_wishlist');
                }
            }

            // 저장할 위시리스트 만들기 <<<
            // DB iw_member table에 wishlist 추가

            $this->ci->user_member->member_update($inData);



            /*

              $member_detail_list = $this->ci->user_member->member_select_detail($inData['member_id']);

              $rows = '';

              if (count($member_detail_list) > 0) {

              $rows = $member_detail_list->row();

              }

             */



            echo func_jsAlertReplace('This product is added in wishlist.', '/?c=user&mcd=' . $this->mcd . '&me=bbs_wishlist');
        } else {

            // echo func_jsAlertReplace('This product are added in wishlist.', '/?c=user&mcd=' . $this->mcd . '&me=bbs_list&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

            echo func_jsAlertReplace('failed to add wishlist.', '/');
        }
    }

    // wishlist 삭제

    function delete_wishlist() {



        $inData = array();

        $inData['member_id'] = $this->ci->data['session_member_id'];



        //$inData = func_get_post_array($this->ci->input->post('car_stock_no'));

        $car_stock_no = $this->ci->input->post('car_stock_no');



        // member의 wishlist를 가져온다 >>>

        $member_detail_list = $this->ci->user_member->member_select_detail($inData['member_id']);

        $rows = '';

        if (count($member_detail_list) > 0) {

            $rows = $member_detail_list->row();
        }

        $inData['member_wishlist'] = $rows->member_wishlist;

        // member의 wishlist를 가져온다 <<<
        // 삭제할 차 삭제

        for ($i = 0; $i < count($car_stock_no); $i++) {

            $inData['member_wishlist'] = str_replace($car_stock_no[$i], '', $inData['member_wishlist']);

            $inData['member_wishlist'] = preg_replace('/\^{2,}/', '^', $inData['member_wishlist']);
        }



        $i = 0;

        while (substr($inData['member_wishlist'], 0, 1) == '^') {

            $inData['member_wishlist'] = substr_replace($inData['member_wishlist'], '', 0, 1);



            // 혹시 몰라서.. 무한루프 방지.

            $i++;

            if ($i > 100) {

                break;
            }
        }



        $i = 0;

        while (substr($inData['member_wishlist'], -1) == '^') {

            $inData['member_wishlist'] = substr_replace($inData['member_wishlist'], '', -1, 1);



            // 혹시 몰라서.. 무한루프 방지.

            $i++;

            if ($i > 100) {

                break;
            }
        }



        // DB 적용

        $this->ci->user_member->member_update($inData);



        echo func_jsAlertReplace('This product is deleted from wishlist.', '/?c=user&mcd=' . $this->mcd . '&me=bbs_wishlist');
    }

    // 그누보드

    function board_qna_list() {



        $query_string = $this->ci->input->get('iurl');



        $iframe_url = 'http://blauda.com/gnuboard4/bbs/board.php?bo_table=QnA';



        if ($query_string == 'write') {

            $iframe_url = 'http://blauda.com/gnuboard4/bbs/write.php?bo_table=QnA';
        }



        func_set_data($this->ci, 'iframe_url', $iframe_url);



        return $this->ci->load->view('user/bbs/' . $this->mcd . '/qnaboard', $this->ci->data, true);
    }

    // 문의 게시판 문의 HTML

    function board_counsel() {

        return $this->ci->load->view('user/bbs/' . $this->mcd . '/free_counsel', $this->ci->data, true);
    }

    // 문의게시판 글쓰기

    function board_counsel_insert_exec() {

        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴
        // 제일 최근글 글번호 가져온다

        $max_board_num = $this->ci->user_bbs->select_board_counsel_max_board_num();

        if (count($max_board_num) > 0) {

            $rows = $max_board_num->row();
        }

        $inData['board_num'] = $rows->board_num;



        if ($inData['board_num'] == '') {

            $inData['board_num'] = 1;
        } else {

            $inData['board_num'] += 1;
        }



        $idx = $this->ci->user_bbs->insert_board_counsel($inData);



        echo func_jsAlertReplace('We will give you a prompt reply about the inquiry.', '/?c=user&mcd=' . $this->mcd . '&me=board_counsel_list');
    }

    // 문의 게시판 문의 HTML

    function board_counsel_list($prev_next = 'N') {



        $sParam = '';   //검색 string

        $where = array();



        //권한체크

        $this->_bbs_certi_check('member_grade_list');



        //페이지 조건

        $cur_page = $this->ci->input->get_post("cur_page");

        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;



        $page_rows = 10;  //페이지 로우수

        $page_views = (integer) func_get_config($this->bbs_config, 'page_views'); //페이지 네비게이션 수



        $search_title = $this->ci->input->get('search_title');

        $search_content = $this->ci->input->get('search_content');

        $search_name = $this->ci->input->get('search_name');

        $search_title_content = $this->ci->input->get('search_title_content');



        if ($search_title != '') {

            $where['board_title like '] = '%' . $search_title . '%';
        } else if ($search_content != '') {

            $where['board_content like '] = '%' . $search_content . '%';
        } else if ($search_name != '') {

            $where['member_first_name like '] = '%' . $search_name . '%';
        } else if ($search_title_content != '') {

            $where['board_title like '] = '%' . $search_title_content . '%';

            $where['board_content like '] = '%' . $search_title_content . '%';
        }



        //게시판 일반 목록 조회

        $board_counsel_list = $this->ci->user_bbs->select_board_counsel($where);



        $total_rows = $board_counsel_list->num_rows(); //조회 전체 목록수

        $row_cnt = $total_rows - ($cur_page - 1) * $page_rows;  //글 번호
        //페이지정보 조회

        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



        //이전글 다음글 조회

        if ($prev_next == 'Y') {

            $pn_data = array();

            $pn_data['isPrev'] = '';



            $prev = ($cur_page - 1) * $page_rows;



            if ($prev > 0) {

                $pn_data['isPrev'] = "PREV";

                $prev = $prev - 1;
            }



            $pn_data['cur_page'] = $cur_page;

            $pn_data['page_views'] = $page_views;

            $pn_data['page_rows'] = $page_rows;

            $pn_data['total_rows'] = $total_rows;

            $pn_data['row_cnt'] = $row_cnt;



            if ($pn_data['isPrev'] == 'PREV') {

                $page_rows = $page_rows + 2;
            } else {

                $page_rows = $page_rows + 1;
            }



            $pn_data['prevList'] = $this->ci->user_bbs->select_wishlist($this->mcd, 'N', $prev, $page_rows, $where, $order_by, $member_id)->result();



            return $pn_data;
        }



        func_set_data($this->ci, 'grade', $this->_bbs_certi_check());   //권한

        func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수

        func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지

        func_set_data($this->ci, 'row_cnt', $row_cnt);   //글번호

        func_set_data($this->ci, 'cur_page', $cur_page); //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);  //검색 string

        func_set_data($this->ci, 'board_counsel_list', $board_counsel_list->result());

        func_set_data($this->ci, 'page_info', $page_info);



        return $this->ci->load->view('user/bbs/' . $this->mcd . '/free_counsel_list', $this->ci->data, true);
    }

    // 문의 게시판 문의 HTML

    function board_counsel_read() {

        /*

          var_dump($this->ci->input->get_post('board_idx'));

          exit; */



        $board_idx = $this->ci->input->get_post('board_idx');

        $board_num = $this->ci->input->get_post('board_num');

        $input_pwd = $this->ci->input->get_post('input_pwd');

        $password_check = false;



        //게시글, 코맨트 조회 >>>

        $board_counsel_read_content = $this->ci->user_bbs->select_board_counsel_read($board_num);   //검색조건); //상세 조회

        $where = array();

        $where['board_idx ='] = $board_idx;

        $comment_list = $this->ci->user_bbs->select_comment($where);

        //게시글, 코맨트 조회 <<<
        //$total_rows = $comment_list->num_rows(); //조회 전체 목록수 - 페이징
        //$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	 //글 번호 - 페이징
        // get counsel board password

        $board_pwd = $board_counsel_read_content->row()->board_pwd;







        // password_check

        if ($board_pwd == '' || $input_pwd == $board_pwd) {

            $password_check = true;
        }

        if (isset($_SESSION['USER'])) {

            if ($_SESSION['USER']['grade_no'] == 1) {

                $password_check = true;
            }
        }



        if ($password_check) {



            func_set_data($this->ci, 'board_counsel_read_content', $board_counsel_read_content);

            func_set_data($this->ci, 'comment_list', $comment_list->result());

            // func_set_data($this->ci, 'total_rows', $total_rows);	// 페이징
            // func_set_data($this->ci, 'row_cnt', $row_cnt);   //글번호 - 페이징



            return $this->ci->load->view('user/bbs/' . $this->mcd . '/free_counsel_read', $this->ci->data, true);
        } else {

            echo func_jsAlertReplace('Please input correct password.', '/?c=user&mcd=' . $this->mcd . '&me=board_counsel_list');
        }
    }

    // board counsel 삭제

    function board_counsel_delete_exec() {



        $board_idx = $this->ci->input->post('board_idx');



        // 삭제할 차 삭제

        for ($i = 0; $i < count($board_idx); $i++) {

            $this->ci->user_bbs->delete_board_counsel($board_idx[$i]);
        }



        echo func_jsAlertReplace('This inquiry is deleted from list.', '/?c=user&mcd=' . $this->mcd . '&me=board_counsel_list');
    }

    // board counsel 개별 삭제

    function board_counsel_delete_exec2() {



        $board_idx = $this->ci->input->get('board_idx');



        $this->ci->user_bbs->delete_board_counsel($board_idx);



        echo func_jsAlertReplace('This inquiry is deleted from list.', '/?c=user&mcd=' . $this->mcd . '&me=board_counsel_list');
    }

    // board counsel 수정 폼

    function board_counsel_update() {



        $input_pwd = $this->ci->input->get_post('input_pwd');

        $board_num = $this->ci->input->get_post('board_num');

        $board_idx = $this->ci->input->get_post('board_idx');



        $password_check = false;



        $board_counsel_read_content = $this->ci->user_bbs->select_board_counsel_read($board_num);   //검색조건); //상세 조회

        $board_pwd = $board_counsel_read_content->row()->board_pwd; // get counsel board password

        func_set_data($this->ci, 'board_counsel_read_content', $board_counsel_read_content);



        // password_check

        if ($board_pwd == '' || $input_pwd == $board_pwd) {

            $password_check = true;
        }

        if (isset($_SESSION['USER'])) {

            if ($_SESSION['USER']['grade_no'] == 1) {

                $password_check = true;
            }
        }



        if ($password_check) {

            return $this->ci->load->view('user/bbs/' . $this->mcd . '/free_counsel_update', $this->ci->data, true);
        } else {

            echo func_jsAlertReplace('Please input correct password.', '/?c=user&mcd=' . $this->mcd . '&me=board_counsel_list');
        }
    }

    // board counsel 수정 실행

    function board_counsel_update_exec() {



        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴



        $idx = $this->ci->user_bbs->update_board_counsel($inData);



        echo func_jsAlertReplace('We will give you a prompt reply about the inquiry.', '/?c=user&mcd=' . $this->mcd . '&me=board_counsel_list');
    }

    // board counsel 수정 실행

    function comment_insert_exec_ajax() {



        // insert ajax parameter to inData

        $inData = array();

        $inData['board_idx'] = $this->ci->input->post('board_idx');

        $inData['member_first_name'] = $this->ci->input->post('member_first_name');

        $inData['comment_pwd'] = $this->ci->input->post('comment_pwd');

        $inData['comment_content'] = $this->ci->input->post('comment_content');

        $inData['comment_ip'] = $this->ci->input->post('comment_ip');



        // insert comment to DB

        $comment_idx = $this->ci->user_bbs->insert_comment($inData);



        // select comment that inserted now >>>
        //$inserted_comment = $this->ci->user_bbs->select_comment($comment_idx, 'comment_idx');

        $where = array();

        $where['comment_idx ='] = $comment_idx;

        $inserted_comment = $this->ci->user_bbs->select_comment($where);

        //게시글, 코맨트 조회 <<<



        $clist = '';

        if (count($inserted_comment) > 0) {

            $clist = $inserted_comment->row();
        }



        /*

          $member_first_name = $clist->member_first_name;

          $comment_content = $clist->comment_content;

          $comment_ip = $clist->comment_ip;

         */

        // select comment that inserted now <<<



        /*

          echo '

          <div id="" class="comment_div_list" style="border:1px solid #999; padding:10px; margin-bottom:20px;">

          <!-- board-list -->

          <table class="comment_form" summary="코멘트폼" border="0" cellspacing="0" width="100%">

          <caption>

          Answer

          </caption>

          <colgroup>

          <col width="90%" />

          <col width="10%" />

          </colgroup>

          <tbody>

          <tr>

          <td>

          <p style="float:left; margin-left:15px; margin-bottom:5px;">

          <label for="">name</label>&nbsp;&nbsp;'.$member_first_name.'&nbsp;&nbsp;&nbsp;&nbsp;

          </p>

          </td>

          <td></td>

          </tr>

          <tr>

          <th><textarea name="values[comment_contents]" style="width:850px; height:100px;" readonly>'.$comment_content.'</textarea></th>

          <td><img src="../../../../images/user/btn_cosubmit.jpg" /></td>

          </tr>

          </tbody>

          </table>

          </div>';

         */



        include_once realpath(dirname(BASEPATH)) . '/views/user/bbs/product/comment_ajax.php';

        exit;
    }

    function comment_delete_exec() {



        /*

          $this->ci->load->helper('url');

          var_dump($this->ci->input->server('REMOTE_HOST'));

          var_dump($this->ci->input->server('QUERY_STRING'));

          exit;

         */



        // get query string value

        $comment_idx = $this->ci->input->get('comment_idx');

        $input_pwd = $this->ci->input->get('comment_pwd');

        $board_idx = $this->ci->input->get('board_idx');

        $board_num = $this->ci->input->get('board_num');



        // get comment password in DB

        $where = array();

        $where['comment_idx ='] = $comment_idx;

        $comment_pwd = $this->ci->user_bbs->select_comment($where)->row()->comment_pwd;



        $have_authority = false;

        if (isset($_SESSION['USER'])) {

            if ($_SESSION['USER']['grade_no'] == 1) {

                $have_authority = true;
            }
        }



        if ($input_pwd == $comment_pwd || $have_authority) {

            $this->ci->user_bbs->delete_comment2($comment_idx, $board_idx);



            echo func_jsAlertReplace('This comment is deleted from inquiry.', '/?c=user&mcd=' . $this->mcd . '&me=board_counsel_read&board_num=' . $board_num . '&board_idx=' . $board_idx);
        } else {

            echo func_jsAlertReplace('please input correct password', '/?c=user&mcd=' . $this->mcd . '&me=board_counsel_read&board_num=' . $board_num . '&board_idx=' . $board_idx);
        }



        /*

          $comment_row = $comment->row();

          $comment_pwd = $comment_row->comment_pwd;



          if($comment_pwd



          echo func_jsAlertReplace('We will give you a prompt reply about the inquiry.', '/?c=user&mcd=' . $this->mcd . '&me=board_counsel_read&board_num='.$board_num.'&board_idx='.$board_idx);

         */
    }

    /*

      //게시판 코멘트 삭제

      function bbs_comment_delete() {





      echo func_jsAlertReplace('success!', '.');

      $password_chk = $this->ci->input->get_post('password_chk');  //비밀번호

      $cm_idx = $this->ci->input->get_post('cm_idx');

      $idx = $this->ci->input->get_post('idx');

      $cur_page = $this->ci->input->get_post('cur_page');



      if ($cur_page == NULL || $cur_page == '')

      $cur_page = 1;



      $sParam = $this->ci->input->get_post('sParam');  //검색 string



      //비밀번호

      $query = $this->ci->user_bbs->select_password($cm_idx, $idx, $this->mcd, $password_chk);

      $link = "/?c=user&mcd=" . $this->mcd . "&me=bbs_detail&idx=" . $idx . "&cur_page=" . $cur_page . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . $sParam;



      //비밀번호 정보 OK

      if ($query->num_rows() > 0) {

      //DB 삭제

      $this->ci->user_bbs->delete_comment($cm_idx, $idx, $this->mcd);

      echo func_jsAlertReplace('삭제 되었습니다.', $link);

      } else {

      echo func_jsAlertReplace('비밀번호가 올바르지 않습니다.', $link);

      }



      }

     */
	 
	 
	 function bbs_order(){
		 
		$idx = $this->ci->input->get_post('idx');

        $page_url = "?c=user&mcd=product&me=bbs_detail&idx=".$idx."&category=offer&sParam=%26category%3Doffer";

        $mcd = $this->ci->input->get_post('mcd');
		
        $query_icon_status = mysql_query("SELECT icon_status FROM iw_bbs WHERE idx='{$idx}'");
        $row_icon_status = mysql_fetch_array($query_icon_status);
        if ($row_icon_status['icon_status']=="sale" || $row_icon_status['icon_status']=="order") {

    		if($idx !=''){
    			
    			$inData = array();
    			
    			$inData['stock_idx']= $idx;
    			
    			$inData['customer_no']= $_SESSION['USER']['member_no'];
    			
    			
    			if (!isset($inData['order_dt'])) {  //작성일
    				$inData['order_dt'] = func_get_date_time();
    			}
    			$inData['order_status']= 'order';
    			
    			$where['customer_no ='] = $inData['customer_no'];
    			$where['stock_idx ='] = $inData['stock_idx'];
    			
    			
    			$lst_car = $this->ci->user_bbs->total_select_car_order($where);
    			//echo $lst_car;
    			//var_dump($lst_car);
    			if($lst_car == 0){
    				$this->ci->user_bbs->insert_car_order($inData);
    				$inData1 = array();
    				$inData1['idx']= $idx;
    				$inData1['menu_code']=  $mcd;
    				//$inData1['customer_no']= $inData['customer_no'];
    				$inData1['icon_status']= 'order';
    				$result = $this->ci->user_bbs->update($inData1);
    				if($result == true){
                        $this->ci->activity->insert('12', 'User order a car');
    				    echo func_jsAlertReplace('Your car order success.', $page_url);
    				}
    			}else{
    				echo func_jsAlertReplace('You can order only one not order twice.', $page_url);
    			}
    		}
		
		
        //return $this->ci->load->view('user/bbs/' . $this->mcd . '/view', $this->ci->data, true);
        }
        echo func_jsAlertReplace('This car reserved by another customer.', $page_url);
	 }
	 

    function bbs_reserve(){
         
        $idx = $this->ci->input->get_post('idx');

        $page_url = "?c=user&mcd=product&me=bbs_detail&idx=".$idx."&category=offer&sParam=%26category%3Doffer";

        $mcd = $this->ci->input->get_post('mcd');
        
        if($idx !=''){
            
            $inData = array();
            
            $list_car = $this->ci->user_bbs->select_detail($idx,$mcd);
            
            if(count($list_car) == 1){
                // $this->ci->user_bbs->insert_car_order($inData);
                $inData1 = array();
                $inData1['idx']= $idx;
                $inData1['menu_code']=  $mcd;
                if($inData1['icon_status']= 'sale'){
                    $inData1['customer_no']= $_SESSION['USER']['member_no'];
                }
                $inData1['icon_status']= 'reserved';
                $result = $this->ci->user_bbs->update($inData1);
                if($result == true){
                echo func_jsAlertReplace('Your car reserve success.', $page_url);
                }
            }else{
                echo func_jsAlertReplace('You can reserve only once not reserve twice.', $page_url);
            }
        }
        
        
        //return $this->ci->load->view('user/bbs/' . $this->mcd . '/view', $this->ci->data, true);
     }

	 //게시판 목록 조회

    function reserved($prev_next = 'N') {
	//권한체크
		

        $where = array(); //sql where 조건

        $where2 = array(); //sql where 조건2

        $sParam = '';   //검색 string
        //카테고리 검색
		
		if(isset($_SESSION['USER'])){
			
			$where['customer_no =']=$_SESSION['USER']['member_no'];	
		}

        $category_yn = func_get_config($this->bbs_config, 'category_yn'); //카테고리 검색

        $category = $this->ci->input->get_post('category'); //카테고리 코드

        //페이지 조건

        $cur_page = $this->ci->input->get_post("cur_page");

        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;


        $page_rows = (integer) func_get_config($this->bbs_config, 'page_rows');  //페이지 로우수

        $page_views = (integer) func_get_config($this->bbs_config, 'page_views'); //페이지 네비게이션 수
       
	    $bbs_notice_list = $this->ci->user_bbs->select_reserved($this->mcd, 'Y', ($cur_page - 1) * $page_rows, $page_rows, $where, $where2);

		//var_dump($bbs_notice_list);
        //게시판 일반 목록 조회

        $bbs_list = $this->ci->user_bbs->select_reserved($this->mcd, 'N', ($cur_page - 1) * $page_rows, $page_rows, $where, $where2);

        $total_rows = $this->ci->user_bbs->total_select_reserved($this->mcd, $where, $where2); //조회 전체 목록수
	   
	    $row_cnt = $total_rows - ($cur_page - 1) * $page_rows;  //글 번호

        //var_dump($bbs_list);
		
		//페이지정보 조회

        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



        //이전글 다음글 조회

        if ($prev_next == 'Y') {

            $pn_data = array();

            $pn_data['isPrev'] = '';



            $prev = ($cur_page - 1) * $page_rows;



            if ($prev > 0) {

                $pn_data['isPrev'] = "PREV";

                $prev = $prev - 1;
            }



            $pn_data['cur_page'] = $cur_page;

            $pn_data['page_views'] = $page_views;

            $pn_data['page_rows'] = $page_rows;

            $pn_data['total_rows'] = $total_rows;

            $pn_data['row_cnt'] = $row_cnt;



            if ($pn_data['isPrev'] == 'PREV') {

                $page_rows = $page_rows + 2;
            } else {

                $page_rows = $page_rows + 1;
            }


            $pn_data['prevList'] = $this->ci->user_bbs->select_reserved($this->mcd, 'N', $prev, $page_rows, $where, $where2, $member_id)->result();



            return $pn_data;
        }

	

        func_set_data($this->ci, 'grade', $this->_bbs_certi_check());   //권한

        func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수

        func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지

        func_set_data($this->ci, 'row_cnt', $row_cnt);   //글번호

        func_set_data($this->ci, 'cur_page', $cur_page); //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);  //검색 string
		
		func_set_data($this->ci,'category',$category);

        func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list->result());

        func_set_data($this->ci, 'bbs_list', $bbs_list->result());

        func_set_data($this->ci, 'page_info', $page_info);
		
		
		if ($this->ci->input->get_post('layout') == 'wishlist') {

            return $this->ci->load->view('user/bbs/' . $this->mcd . '/wishlist', $this->ci->data, true);
        } else {

            return $this->ci->load->view('user/bbs/reserved/list', $this->ci->data, true);
        }

    }

}

?>