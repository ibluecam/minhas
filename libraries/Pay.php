<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : pay.php
 * 개  요 : 결제 Class
  ------------------------------------------------ */

class Pay {

	var $ci;
	var $mcd;

//constructor
	function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();

		/* ------------------------------------------------------------
		  - Model Load
		  ------------------------------------------------------------ */
		$this->ci->load->model('mo_pay', 'payment'); //class명과 동일하면 error 원인 모름..
//결제 메뉴코드
		$this->mcd = $this->ci->data['mcd'];

//로그인 되어 있지 않다면...
		if ($this->ci->data['is_login'] != 'Y') {
			redirect($this->ci->data['base_url'] . '&mcd=' . $this->ci->data['member_login_mcd'], 'refresh');
		}
	}

//인덱스
	function index() {
		return $this->pay_req();
	}

//결제 요청 HTM
	function pay_req() {

		/*
		 * [상점결제요청 페이지(ActiveX)]
		 *
		 * 기본 파라미터만 예시되어 있으며, 별도로 필요하신 파라미터는 연동메뉴얼을 참고하시어 추가하시기 바랍니다.
		 * hashdata 암호화는 거래 위변조를 막기위한 방법입니다.
		 *
		 */


		/*
		 * 1. 기본결제정보 변경
		 *
		 * 결제기본정보를 변경하여 주시기 바랍니다.
		 */
		$platform = 'test';											 //LG텔레콤 결제서비스 선택(test:테스트, service:서비스)
		if ($platform == '')
			$platform;
		$CST_MID = 'conys10';										  //상점아이디(LG텔레콤으로 부터 발급받으신 상점아이디를 입력하세요)
		//테스트 아이디는 't'를 제외하고 입력하세요.
		$LGD_MID = (('test' == $platform) ? 't' : '') . $CST_MID;		 //상점아이디(자동생성)
		$LGD_OID = $CST_MID . '_' . func_get_today('YmdHis');			  //주문번호(상점정의 유니크한 주문번호를 입력하세요)
		$LGD_MERTKEY = 'bad462dcf36c12c88eb947ff9e1a22d6';				 //상점MertKey(mertkey는 상점관리자 -> 계약정보 -> 상점정보관리에서 확인하실수 있습니다)
		$LGD_TIMESTAMP = time();											 //타임스탬프
//구매자 아이디
		$LGD_BUYERID = $this->ci->data['session_member_id'];
//구매자명
		$LGD_BUYER = $this->ci->input->get_post('writer') != '' ? $this->ci->input->get_post('writer') : $this->ci->data['session_member_name'];
//구매자 이메일
		$LGD_BUYEREMAIL = $this->ci->input->get_post('email') != '' ? $this->ci->input->get_post('email') : $this->ci->data['session_email'];

		$LGD_PRODUCTCODE = $this->ci->input->get_post('LGD_PRODUCTCODE');	  //상품코드
		$LGD_PRODUCTINFO = $this->ci->input->get_post('LGD_PRODUCTINFO');	  //상품명
		$LGD_BUYERPHONE = $this->ci->input->get_post('LGD_BUYERPHONE');	   //구매자 휴대폰번호
		$LGD_BUYERADDRESS = $this->ci->input->get_post('LGD_BUYERADDRESS');	 //구매자 주소
//해당 아이디와 상품코드로 재수강 여부를 판단한다.(수강료 변동)
		$re_course_yn = '';
		if ($LGD_PRODUCTCODE == '0001' || $LGD_PRODUCTCODE == '0002' || $LGD_PRODUCTCODE == '0003')
			$re_course_yn = $this->ci->payment->select_re_course_yn($this->ci->data['session_member_id'], $this->ci->input->get_post('LGD_PRODUCTCODE'));

//재수강이 아니면...
		if ($re_course_yn != 'Y') {
			$LGD_AMOUNT = $this->ci->input->get_post('LGD_AMOUNT');		   //결제금액("," 를 제외한 결제금액을 입력하세요)
		} else {
			if ($LGD_PRODUCTCODE == '0001')
				$LGD_AMOUNT = '20000';
			if ($LGD_PRODUCTCODE == '0002')
				$LGD_AMOUNT = '30000';
			if ($LGD_PRODUCTCODE == '0003')
				$LGD_AMOUNT = '100000';
		}

		$LGD_CUSTOM_SKIN = "red";						   //상점정의 결제창 스킨 (red, blue, cyan, green, yellow)

		/*
		 * 2. 결제결과 DB처리 페이지 링크 변경
		 *
		 * LGD_NOTEURL : 상점결제결과 처리(DB) 페이지 URL을 넘겨주세요.
		 * LGD_CASNOTEURL : 가상계좌(무통장) 결제 연동을 하시는 경우 아래 LGD_CASNOTEURL 을 설정하여 주시기 바랍니다.
		 */

		$LGD_NOTEURL = $this->ci->config->item('base_url') . '?c=common&m=note_url';	 //상점결제결과 처리(DB) 페이지(URL을 변경해 주세요)
		$LGD_CASNOTEURL = $this->ci->config->item('base_url') . '?c=common&m=cas_noteurl';  //가상계좌(무통장) 결제연동

		/*
		 * 3. hashdata 암호화 (수정하지 마세요)
		 *
		 * hashdata 암호화 적용( LGD_MID + LGD_OID + LGD_AMOUNT + LGD_TIMESTAMP + LGD_MERTKEY )
		 * LGD_MID : 상점아이디
		 * LGD_OID : 주문번호
		 * LGD_AMOUNT : 금액
		 * LGD_TIMESTAMP : 타임스탬프
		 * LGD_MERTKEY : 상점키(mertkey)
		 *
		 * hashdata 검증을 위한
		 * LG텔레콤에서 발급한 상점키(MertKey)를 반드시 입력해 주시기 바랍니다.
		 */
		$LGD_HASHDATA = md5($LGD_MID . $LGD_OID . $LGD_AMOUNT . $LGD_TIMESTAMP . $LGD_MERTKEY);

		func_set_data($this->ci, 'platform', $platform);
		func_set_data($this->ci, 'CST_MID', $CST_MID);
		func_set_data($this->ci, 'LGD_MID', $LGD_MID);
		func_set_data($this->ci, 'LGD_OID', $LGD_OID);
		func_set_data($this->ci, 'LGD_AMOUNT', $LGD_AMOUNT);
		func_set_data($this->ci, 'LGD_MERTKEY', $LGD_MERTKEY);
		func_set_data($this->ci, 'LGD_TIMESTAMP', $LGD_TIMESTAMP);
		func_set_data($this->ci, 'LGD_BUYERID', $LGD_BUYERID);
		func_set_data($this->ci, 'LGD_BUYER', $LGD_BUYER);
		func_set_data($this->ci, 'LGD_PRODUCTINFO', $LGD_PRODUCTINFO);
		func_set_data($this->ci, 'LGD_PRODUCTCODE', $LGD_PRODUCTCODE);
		func_set_data($this->ci, 'LGD_BUYEREMAIL', $LGD_BUYEREMAIL);
		func_set_data($this->ci, 'LGD_BUYERPHONE', $LGD_BUYERPHONE);
		func_set_data($this->ci, 'LGD_BUYERADDRESS', $LGD_BUYERADDRESS);
		func_set_data($this->ci, 'LGD_CUSTOM_SKIN', $LGD_CUSTOM_SKIN);
		func_set_data($this->ci, 'LGD_NOTEURL', $LGD_NOTEURL);
		func_set_data($this->ci, 'LGD_CASNOTEURL', $LGD_CASNOTEURL);
		func_set_data($this->ci, 'LGD_HASHDATA', $LGD_HASHDATA);
		func_set_data($this->ci, 're_course_yn', $re_course_yn);

		return $this->ci->load->view('user/pay/' . $this->mcd . '/pay_req', $this->ci->data, true);
	}

//결제 결과 HTM
	function pay_res() {
		/*
		 * [결제결과 화면페이지]
		 */

		$LGD_TID = $this->ci->input->get_post('LGD_TID');   //LG텔레콤 거래번호
		$LGD_OID = $this->ci->input->get_post('LGD_OID');   //주문번호
		$LGD_PAYTYPE = $this->ci->input->get_post('LGD_PAYTYPE');  //결제수단
		$LGD_PAYDATE = $this->ci->input->get_post('LGD_PAYDATE');  //결제일자
		$LGD_FINANCECODE = $this->ci->input->get_post('LGD_FINANCECODE'); //결제기관코드
		$LGD_FINANCENAME = $this->ci->input->get_post('LGD_FINANCENAME'); //결제기관이름
		$LGD_FINANCEAUTHNUM = $this->ci->input->get_post('LGD_FINANCEAUTHNUM'); //결제사승인번호
		$LGD_ACCOUNTNUM = $this->ci->input->get_post('LGD_ACCOUNTNUM');  //입금할 계좌 (가상계좌)
		$LGD_BUYER = $this->ci->input->get_post('LGD_BUYER');   //구매자명
		$LGD_PRODUCTINFO = $this->ci->input->get_post('LGD_PRODUCTINFO'); //상품명
		$LGD_AMOUNT = $this->ci->input->get_post('LGD_AMOUNT');   //결제금액
		$LGD_RESPCODE = $this->ci->input->get_post('LGD_RESPCODE');  //결과코드
		$LGD_RESPMSG = $this->ci->input->get_post('LGD_RESPMSG');  //결과메세지
//결제성공시
		if ("0000" == $LGD_RESPCODE) {
			/*
			  echo "* Xpay-lite (화면)결과리턴페이지 예제입니다." . "<p>";

			  echo "결과코드 : " . $LGD_RESPCODE . "<br>";
			  echo "결과메세지 : " . $LGD_RESPMSG . "<br>";
			  echo "거래번호 : " . $LGD_TID . "<br>";
			  echo "주문번호 : " . $LGD_OID . "<br>";
			  echo "구매자 : " . $LGD_BUYER . "<br>";
			  echo "상품명 : " . $LGD_PRODUCTINFO . "<br>";
			  echo "결제금액 : " . $LGD_AMOUNT . "<br>";
			  echo "결제수단 : " . $LGD_PAYTYPE . "<br>";
			  echo "결제일시 : " . $LGD_PAYDATE . "<br>";
			  echo "결제사코드 : " . $LGD_FINANCECODE . "<br>";

			  if("SC0010" == $LGD_PAYTYPE) {			//신용카드 결제시
			  echo "카드사명 : " . $LGD_FINANCENAME . "<br>";
			  echo "승인번호 : " . $LGD_FINANCEAUTHNUM . "<br>";
			  }
			  else if("SC0030" == $LGD_PAYTYPE) {		//계좌이체 결제시
			  echo "결제은행 : " . $LGD_FINANCENAME . "<br>";
			  }
			  else if("SC0040" == $LGD_PAYTYPE) {		//가상계좌 결제시 (할당)
			  echo "입금은행 : " . $LGD_FINANCENAME . "<br>";
			  echo "입금계좌번호 : " . $LGD_ACCOUNTNUM . "<br>";
			  }                                           //기타 결제시
			  else
			  {
			  echo "결제사명 : " . $LGD_FINANCENAME . "<br>";
			  echo "결제사승인번호 : " . $LGD_FINANCEAUTHNUM . "<br>";
			  }
			 */
			func_set_data($this->ci, 'LGD_TID', $LGD_TID);
			func_set_data($this->ci, 'LGD_OID', $LGD_OID);
			func_set_data($this->ci, 'LGD_PAYTYPE', $LGD_PAYTYPE);
			func_set_data($this->ci, 'LGD_PAYDATE', $LGD_PAYDATE);
			func_set_data($this->ci, 'LGD_FINANCECODE', $LGD_FINANCECODE);
			func_set_data($this->ci, 'LGD_FINANCENAME', $LGD_FINANCENAME);
			func_set_data($this->ci, 'LGD_FINANCEAUTHNUM', $LGD_FINANCEAUTHNUM);
			func_set_data($this->ci, 'LGD_ACCOUNTNUM', $LGD_ACCOUNTNUM);
			func_set_data($this->ci, 'LGD_BUYER', $LGD_BUYER);
			func_set_data($this->ci, 'LGD_PRODUCTINFO', $LGD_PRODUCTINFO);
			func_set_data($this->ci, 'LGD_AMOUNT', $LGD_AMOUNT);
		}
//결제실패시
		else {
			/*
			  echo "결제가 실패되었습니다." . "<p>";
			  echo "결과코드 : " . $LGD_RESPCODE . "<br>";
			  echo "결과메세지 : " . $LGD_RESPMSG . "<br>";
			 */
		}

		func_set_data($this->ci, 'LGD_RESPCODE', $LGD_RESPCODE);
		func_set_data($this->ci, 'LGD_RESPMSG', $LGD_RESPMSG);


		return $this->ci->load->view('user/pay/' . $this->mcd . '/pay_res', $this->ci->data, true);
	}

//결제 마이페이지 HTML
	function pay_mypage() {
		$member_id = $this->ci->data['session_member_id'];

		$select_de_pay_result = $this->ci->payment->payment_select_detail($member_id);

		func_set_data($this->ci, 'select_de_pay_result', $select_de_pay_result->result());

		return $this->ci->load->view('user/pay/' . $this->mcd . '/pay_mypage', $this->ci->data, true);
	}

}
?>