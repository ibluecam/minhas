<?php

/*if (!defined('BASEPATH'))
    exit('No direct script access allowed');*/


/* ------------------------------------------------

 * 파일명 : Adm_bbs.php

 * 개  요 : 관리자 게시판 Class

  ------------------------------------------------ */
require 'controllers/coludinary/main.php';
require 'controllers/resize_class/resize_class.php';
require 'controllers/resize_class/ImageManipulator.php';


class Adm_bbs {

    var $ci;
    var $mcd;
    var $bbs_config;
    var $auth_member_id;
    var $fob_cost_plus = 0;
    var $frieght_cost_plus    = 250;
    var $inspection_cost_plus = 100;
    var $insurance_cost_plus  = 50;
    //constructor

    function __construct() {

        //CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당

        $this->ci = &get_instance();

        /* ------------------------------------------------------------



          - Model Load



          ------------------------------------------------------------ */

        $this->ci->load->library('phpsession');

        $this->ci->load->model('mo_bbs', 'user_bbs'); //class명과 동일해서 user_bbs

        $this->ci->load->model('mo_car_email', 'car_email'); 
        $this->ci->load->model('mo_shipping', 'mo_shipping'); 
        $this->ci->load->model('mo_customer', 'mo_customer'); 
        $this->ci->load->model('mo_survey', 'survey'); // 설문조사
        $this->ci->load->model('mo_email', 'mo_email'); 
        $this->ci->load->model('mo_member', 'user_member');
        $this->ci->load->model('mo_message', 'mo_message');
        $this->ci->load->model('mo_webscrape', 'mo_webscrape');
        $this->ci->load->model('mo_file', 'mo_file');
        $this->ci->load->model('mo_shipping', 'mo_shipping');
        $this->ci->load->model('mo_activity_log','mo_activity_log');
        $this->ci->load->model('mo_parse', 'mo_parse');
        // 맴버
        //게시판 메뉴코드


        $this->mcd = $this->ci->data['mcd'];
        //If no route define show item page (product)
        if (empty($this->ci->data['m'])) {
            func_set_data($this->ci, 'm', 'adm_bbs_list');
            $this->mcd = "product";
           
            
        }

        //if(!empty($this->m)) func_set_data($this->ci, 'm', 'adm_bbs_list');       

        if (!empty($this->mcd))
            func_set_data($this->ci, 'mcd', 'product');


        //게시판 설정정보

        $this->bbs_config = $this->ci->iw_config->select_bbs_config($this->mcd)->result();



        //IWC 게시판 설정을 하였다면..

        if (count($this->bbs_config) > 0) {

            func_set_data($this->ci, 'bbs_config_result', $this->bbs_config);



            //스킨 이미지 경로(해당 게시판 이미지 스킨 경로에 존재)

            $skin_images_url = '/views/user/bbs/' . $this->mcd . '/images';

            func_set_data($this->ci, 'skin_images_url', $skin_images_url);
        }
        func_set_data($this->ci, 'navigation_bar', '');
        func_set_data($this->ci, 'is_login', 'Y');
        func_set_data($this->ci, 'session_bussines_type', $this->ci->phpsession->get('business_type', 'ADMIN'));
        func_set_data($this->ci, 'session_member_id', $this->ci->phpsession->get('member_id', 'ADMIN'));
        

        /*

          else

          {

          show_error('해당 게시판이 설정되지 않았습니다.<br/>관리자에게 문의하세요.');

          //echo func_jsAlertReplace('해당 게시판이 설정되지 않았습니다.\n\n관리자에게 문의하세요.', '/?c=iwc', TRUE);

          exit;

          }

         */
        $this->auth_member_id = $this->ci->phpsession->get('member_id', 'ADMIN');
    }
    function _isAdminGroup(){
        $grad_no_group = array('1', '2', '3');
        if(in_array($this->ci->grade_no, $grad_no_group)){
            return true;
        }
    }
    function current_url(){
        $CI = & get_instance();
        $url = $CI->config->site_url($CI->uri->uri_string());
        $current_url = $_SERVER['QUERY_STRING'] ? $url . '?' . $_SERVER['QUERY_STRING'] : $url;
        return $current_url;
    }
    function get_pagination_url($gets, $page_param_name){
       
        if(isset($gets[$page_param_name])){
            unset($gets[$page_param_name]);
        }
        $url_arr = array();
        foreach($gets as $key=>$value){
            $url_arr[]=$key.'='.$value;
        }
        $url = implode('&', $url_arr);
        return $url;
    }
    /*** EXEC EMAIL COMPAIGN ***/




    function exec_car_email_compaign(){
        // $idx = $this->ci->input->post('idx');
        //$compaign_id = $this->ci->input->post('compaign_id');
        $this->send_car_offer($_POST);
    }

    function exec_multiple_car_email_compaign(){

        $compaign_id = $this->ci->input->post('compaign_id');
        
        $this->multiple_send_car_offer($compaign_id);
    }

    function multiple_send_car_offer(){
        $recipient_name = $this->ci->input->post('recipient_name');
        $subject = $this->ci->input->post('subject');
        $date = $this->ci->input->post('date');
        $hour = $this->ci->input->post('hour');
        $minute = $this->ci->input->post('minute');
        $send_now = $this->ci->input->post('send_schedule');

        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';

        $car_detail = '';
        $detail_html = '';
        $i = 0;

        if(count($_POST['idx'])>0){

            $rows = $this->ci->car_email->select_car_by_multiple_idx($_POST['idx']);
            $detail_html = $this->email_html_motorbb($rows);
            // foreach ($compaign_multiple as $rows) {

            //     // if(!empty($rows->car_fob_cost)){
            //     //     $car_price=number_format($rows->car_fob_cost).' USD';
            //     // }else{
            //     //     $car_price="ASK";
            //     // }
            //     // $car_country=$rows->country;
               
            //     // $link = $protocol.'://'.$_SERVER['HTTP_HOST']."/?c=user&m=showdetailcar&idx=".$rows->idx;
            //     // $get_send_idx[] = $rows->idx;      

            //     // $car_title=$rows->car_model_year.' '.$rows->car_make.' '.$rows->car_model;
            //     // $i ++; 
            //     // if($i%4==1){
            //     //     $detail_html.='<tr>';
            //     // }
            //     // $detail_html.='<td style="border:none;width:137px;height:0x;background:#fff;">
            //     //                     <table>
            //     //                         <tr>
            //     //                             <td colspan="2" align="center" style="vertical-align: top;">
            //     //                                 <a href="'.$link.'">
            //     //                                     <img src="'.str_replace("upload", "upload/h_89/w_128", $_POST['car_image'][$rows->idx]).'" />
            //     //                                 </a>
            //     //                             </td>
            //     //                         </tr><tr>
            //     //                             <td colspan="2" align="center" style="color: rgb(0, 90, 148); font-weight: bold;font-size: 14px;margin-top: 4px;height:50px;">
            //     //                                 '.$car_title.'
            //     //                             </td>
            //     //                         </tr><tr>
            //     //                             <td colspan="2" align="center" style="color: red;font-size: 15px;">'.$car_price.'</td>
            //     //                         </tr><tr>
            //     //                             <td colspan="2" align="center">
            //     //                                 <a href="'.$link.'"><img src="https://motorbb.com/email_template/special_offer/contact_btn.png"/></a>
            //     //                             </td>
            //     //                         </tr>
            //     //                     </table>
            //     //         </td>';
            //     // $remain = $i%4;        
            //     // if($remain==0){
            //     //     $detail_html.='</tr>';
            //     // }else{
                   
            //     //     if($i>=count($_POST['idx'])){
                        
            //     //         for($j=4;$j>$remain;$j--){
            //     //             $detail_html.= '<td style="width:137px;"></td>';
            //     //         }
            //     //         $detail_html.='</tr>';
            //     //     }
                    
            //     // }
                
            // }
            
           
            //$link_detail= $protocol.'://'.$_SERVER['HTTP_HOST']."/?c=user&m=car_list";
            $html = file_get_contents('email_template/motorbb_newsletter.html', FILE_USE_INCLUDE_PATH);
           
            $html = str_replace('[::CAR_DETAIL::]', $detail_html, $html);
            //$html = str_replace('[::LINKDETAIL::]', $link_detail, $html);

            
            $name = 'Motorbb Newsletter '.date('Y-m-d H:i:s');

            $this->ci->mo_email->insert_marketing_email($html,$subject,$name);

            $this->ci->mo_email->insert_recipient_to_marketing_email($recipient_name,$name);

            $this->ci->mo_email->insert_schedule_to_marketing_email($date,$hour,$minute,$name,$send_now);

            

            $log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'A Marketing email (name: '.$name.') has been inserted with recipient: <b>'.$recipient_name.'</b> by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'marketing_email',
                    'created_dt'=>func_get_date_time()
                ); 
            $this->ci->mo_activity_log->insert($log_data);
        }
    }

    function email_html_motorbb($rows){
        $html = '';
        //$total_tr = ceil(count($rows)/4);
        $strike_fob = '';
        $i=0;
        $html .= '<TR>
            <TD style="BORDER-TOP: none; BORDER-RIGHT: #dbdbdb 1px solid; BORDER-BOTTOM: medium none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-LEFT: #dbdbdb 1px solid; PADDING-RIGHT: 0px; BACKGROUND-COLOR: #feffff">
            <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 align=left>

            <TR style="HEIGHT: 20px">';
        foreach($rows as $row){
            
     
            if($i%4==0){
                $html .= '</TR></TABLE></TD></TR><TR>
                    <TD style="BORDER-TOP: none; BORDER-RIGHT: #dbdbdb 1px solid; BORDER-BOTTOM: medium none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-LEFT: #dbdbdb 1px solid; PADDING-RIGHT: 0px; BACKGROUND-COLOR: #feffff">
                    <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 align=left>

                    <TR style="HEIGHT: 20px">';
            }
            if(!empty($row->car_fob_cost)){
                $car_fob_cost = number_format($row->car_fob_cost).' USD';
                if($row->old_fob_cost>$row->car_fob_cost){
                    $strike_fob = '<strike style="color:#888; font-size:12px">'.number_format($row->car_fob_cost).' USD</strike><br/>';
                }
            }else{
                $car_fob_cost = "ASK PRICE";
            }

            $html .= '<TD style="BORDER-TOP: medium none; BORDER-RIGHT: medium none; WIDTH: 25%; VERTICAL-ALIGN: top; BORDER-BOTTOM: medium none; PADDING-BOTTOM: 5px; TEXT-ALIGN: center; PADDING-TOP: 5px; PADDING-LEFT: 5px; BORDER-LEFT: medium none; PADDING-RIGHT: 5px; BACKGROUND-COLOR: #feffff">
                    <TABLE cellSpacing=0 cellPadding=0 align=center border=0>
                    <TR>
                    <TD style="PADDING-BOTTOM: 2px; PADDING-TOP: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px" align=center>
                    <TABLE cellSpacing=0 cellPadding=0 border=0>
                    <TR>
                    <TD style="BORDER-TOP: medium none; BORDER-RIGHT: medium none; BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; BACKGROUND-COLOR: transparent"><A href="https://motorbb.com/?c=user&m=cardetail&idx='.$row->idx.'"><IMG alt="Click to view car detail" title="Click to view car detail" style="BORDER-TOP: medium none; BORDER-RIGHT: medium none; BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; DISPLAY: block; BACKGROUND-COLOR: transparent" border=0 src="'.$row->car_thumb.'" width=156 height=98 hspace="0" vspace="0"></A></TD></TR></TABLE></TD></TR></TABLE> 
                    <P style="MARGIN-BOTTOM: 1em; FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; MARGIN-TOP: 0px; LINE-HEIGHT: 155%; BACKGROUND-COLOR: transparent; mso-line-height-rule: exactly" align=center><A title="Go to Motorbb Car List" style="TEXT-DECORATION: none; FONT-WEIGHT: bold; COLOR: #000000" href="https://motorbb.com/?c=user&m=cardetail&idx='.$row->idx.'">'.$row->car_model_year.' '.$row->car_make.' '.$row->car_model.'</A></P>
                    <P style="MARGIN-BOTTOM: 1em; FONT-SIZE: 18px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: red; MARGIN-TOP: 0px; LINE-HEIGHT: 125%; BACKGROUND-COLOR: transparent; mso-line-height-rule: exactly" align=center>'.$strike_fob.'<STRONG>'.$car_fob_cost.'</STRONG></P></TD>';
            $i++;
        }
        $mod = count($row)%4;
        if($mod>0){
            $count_blank = 4-$mod;
        }else{
            $count_blank = 0;
        }
        for($i=1;$i<=$count_blank;$i++){
            $html .= '<TD style="BORDER-TOP: medium none; BORDER-RIGHT: medium none; WIDTH: 25%; VERTICAL-ALIGN: top; BORDER-BOTTOM: medium none; PADDING-BOTTOM: 5px; TEXT-ALIGN: center; PADDING-TOP: 5px; PADDING-LEFT: 5px; BORDER-LEFT: medium none; PADDING-RIGHT: 5px; BACKGROUND-COLOR: #feffff">
                    <TABLE cellSpacing=0 cellPadding=0 align=center border=0>
                    <TR>
                    <TD style="PADDING-BOTTOM: 2px; PADDING-TOP: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px" align=center>
                    <TABLE cellSpacing=0 cellPadding=0 border=0>
                    <TR>
                    <TD style="BORDER-TOP: medium none; BORDER-RIGHT: medium none; BORDER-BOTTOM: medium none; BORDER-LEFT: medium none; BACKGROUND-COLOR: transparent"></TD></TR></TABLE></TD></TR></TABLE> 
                    <P style="MARGIN-BOTTOM: 1em; FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; MARGIN-TOP: 0px; LINE-HEIGHT: 155%; BACKGROUND-COLOR: transparent; mso-line-height-rule: exactly" align=center></P>
                    <P style="MARGIN-BOTTOM: 1em; FONT-SIZE: 18px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: red; MARGIN-TOP: 0px; LINE-HEIGHT: 125%; BACKGROUND-COLOR: transparent; mso-line-height-rule: exactly" align=center></P></TD>';
        }    
        $html .= '</TR></TABLE></TD></TR>';
        return $html;
    }
/*** SENDGRID FUNCTIONS ***/    
    function recipient_list(){
        $rec_list = $this->ci->mo_email->select_recipient_list();

        func_set_data($this->ci, 'rec_list', $rec_list);
        $this->_load_view_layout('admin/marketing_email/rec_list',$this->ci->data, false);
    }

    function recipient_email(){
        $list_name = $this->ci->input->get('list');
        $emails = $this->ci->mo_email->select_recipient_email($list_name);
        func_set_data($this->ci, 'list_name', $list_name);
        func_set_data($this->ci, 'emails', $emails);
        $this->_load_view_layout('admin/marketing_email/email_list',$this->ci->data, false);
    }
    function delete_recipient_email(){
        $list_name = $this->ci->input->get_post('list');
        $emails = $this->ci->input->get_post('email');
        $delete_result = $this->ci->mo_email->delete_recipient_email($list_name, $emails);
        
        redirect('/?c=admin&m=recipient_email&list='.$list_name);
    }


/*** END SENDGRID FUNCTIONS ***/
    function send_car_offer(){

        $recipient_name = $this->ci->input->post('recipient_name');
        $subject = $this->ci->input->post('subject');
        $date = $this->ci->input->post('date');
        $hour = $this->ci->input->post('hour');
        $minute = $this->ci->input->post('minute');
        $send_now = $this->ci->input->post('send_schedule');

        if(count($_POST)>0){

            foreach ($_POST['idx'] as $bbs_idx) {
                
                $car_detail = '';
                $detail_html = '';
                $car_thumb = '';

                $car_detail = $this->ci->user_bbs->select_car_detail($bbs_idx);
                $car_image = $this->ci->user_bbs->select_car_image($bbs_idx); 

                if(COUNT($car_image) > 0){
                    $count = 0;
                    foreach ($car_image as $row) {
                        $count ++; 
                        $car_thumb .= '<a href="'.$row->base_url.$row->public_id.'">
                        <img width="108" height="75" src="'.$row->base_url.'w_108/'.$row->public_id.'">
                        </a>';
                    }                   
                }

                $big_car_image = str_replace("/upload/", "/upload/w_444/", $_POST['car_image'][$bbs_idx]);
                $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
                $html = file_get_contents('email_template/special_offer.html', FILE_USE_INCLUDE_PATH);
                $detail_html = $this->_generate_car_detail_html($car_detail);
                $link = $protocol.'://'.$_SERVER['HTTP_HOST']."/?c=user&m=showdetailcar&idx=".$bbs_idx;
                $html = str_replace('[::CAR_IMAGE_THUMB::]', $car_thumb, $html);
                $html = str_replace('[::CAR_IMAGE::]', $big_car_image, $html);
                $html = str_replace('[::CAR_DETAIL::]', $detail_html, $html);
                $html = str_replace('[::CAR_LINK::]', $link, $html);

                $name = 'Single Car Offer '.date('Y-m-d H:i:s');

                $this->ci->mo_email->insert_marketing_email($html,$subject,$name);

                $this->ci->mo_email->insert_recipient_to_marketing_email($recipient_name,$name);

                $this->ci->mo_email->insert_schedule_to_marketing_email($date,$hour,$minute,$name,$send_now);

                $log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'A Marketing email (name: '.$name.') has been inserted with recipient: <b>'.$recipient_name.'</b> by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'marketing_email',
                    'created_dt'=>func_get_date_time()
                );
                $this->ci->mo_activity_log->insert($log_data);
            }
        }
    }

    function _generate_car_detail_html($car_detail){
        $html = '';
        $i = 0;
        if(!empty($car_detail->car_stock_no)){
            $i++;
            $html .= $this->_tr_for_special_offer('Stock ID', $car_detail->car_stock_no, $i);
        }
        if($car_detail->icon_new_yn=='Y'){
            $i++;
            $html .= $this->_tr_for_special_offer('Condition', 'New', $i);
        }else{
            $i++;
            $html .= $this->_tr_for_special_offer('Condition', 'Used', $i);
        }
        if(!empty($car_detail->car_body_type)){
            $i++;
            $html .= $this->_tr_for_special_offer('Body Type', ucfirst($car_detail->car_body_type), $i);
        }
        if(!empty($car_detail->car_make)){
            $i++;
            $html .= $this->_tr_for_special_offer('Make', $car_detail->car_make, $i);
        }
        if(!empty($car_detail->car_model)){
            $i++;
            $html .= $this->_tr_for_special_offer('Model', $car_detail->car_model, $i);
        }
        if(!empty($car_detail->car_chassis_no)){
            $i++;
            $html .= $this->_tr_for_special_offer('Chassis No', $car_detail->car_chassis_no, $i);
        }
        if(!empty($car_detail->car_model_year)){
            $i++;
            $html .= $this->_tr_for_special_offer('Model Year', $car_detail->car_model_year, $i);
        }
        if(!empty($car_detail->car_mileage)){
            $i++;
            $html .= $this->_tr_for_special_offer('Mileage', number_format($car_detail->car_mileage)." Km", $i);
        }
        if($car_detail->car_steering=='LHD'){
            $i++;
            $html .= $this->_tr_for_special_offer('Steering', 'Left Hand Drive', $i);
        }elseif($car_detail->car_steering=='RHD'){
            $i++;
            $html .= $this->_tr_for_special_offer('Steering', 'Right Hand Drive', $i);
        }
        if(!empty($car_detail->car_transmission)){
            $i++;
            $html .= $this->_tr_for_special_offer('Transmission', $car_detail->car_transmission, $i);
        }
        if(!empty($car_detail->car_cc)){
            $i++;
            $html .= $this->_tr_for_special_offer('Engine Size', number_format($car_detail->car_cc)." CC", $i);
        }
        if(!empty($car_detail->fuel_type)){
            $i++;
            $html .= $this->_tr_for_special_offer('Fuel Type', $car_detail->fuel_type, $i);
        }
        if(!empty($car_detail->country_name)){
            $i++;
            $html .= $this->_tr_for_special_offer('Location / City', $car_detail->country_name.', '.$car_detail->car_city, $i);
        }
        if(!empty($car_detail->car_color)){
            $i++;
            $html .= $this->_tr_for_special_offer('Exterior Color', $car_detail->car_color, $i);
        }
        if(!empty($car_detail->manu_year)){
            $i++;
            $html .= $this->_tr_for_special_offer('Manufactured Date', $car_detail->manu_year, $i);
        }
        if(!empty($car_detail->first_registration_year)){
            $i++;
            $html .= $this->_tr_for_special_offer('Registration Date', $car_detail->first_registration_year, $i);
        }
        if(!empty($car_detail->car_width)&&!empty($car_detail->car_height)&&!empty($car_detail->car_length)){
            $i++;
            $html .= $this->_tr_for_special_offer('Dimension (L*W*H)', number_format($car_detail->car_length).'*'. number_format($car_detail->car_width).'*'. number_format($car_detail->car_height), $i);
        }
        if(!empty($car_detail->car_fob_cost)){
            $i++;
            $html .= $this->_tr_for_special_offer('FOB', number_format($car_detail->car_fob_cost).' '.$car_detail->car_fob_currency, $i);
        }else{
            $i++;
            $html .= $this->_tr_for_special_offer('FOB', "Contact Us", $i);
        }
        return $html;

    }

    function _tr_for_special_offer($field_label, $field_value, $i=0){
        if($i%2 == 0){
            $style = "";
        }else{
            $style = "background-color: #e7f2f8;";
        }

        $html = '
                <tr>
                    <td style="width: 50%; padding: 5px; '.$style.'">'.$field_label.'</td>
                    <td style="width: 50%; padding: 5px; '.$style.' text-align: right;">'.$field_value.'</td>
                </tr>';
        return $html;
    }
    /*** END EXEC EMAIL COMPAIGN ***/
    function _action_adm_item_supporter(){
        $support_member_no = $this->ci->input->post('support_member_no');
        if($this->ci->input->post('submit_remove_idx')){
            $idxs = $this->ci->input->post('idx');
            $this->ci->user_bbs->remove_support_by_idx($idxs);
            redirect($this->current_url());
        }elseif($this->ci->input->post('submit_support_default')){
            $default_member_no = $this->ci->input->post('default_member_no');
            $this->ci->user_bbs->insert_support_default($default_member_no);
        }else{
            if(!empty($support_member_no)){
                $idxs = $this->ci->input->post('idx');
                //var_dump($support_member_no);
                //var_dump($idxs);
                $this->ci->user_bbs->insert_support_by_car_idx($idxs, $support_member_no);
                //redirect($this->current_url());
            }
        }
    }

    function adm_delete_supporter_car(){
        $idxs= explode(";", $this->ci->input->get_post('idx'));
        $this->ci->user_bbs->delete_by_bbs_idx($idxs);
    }

    function adm_item_supporter(){

        //$list_item_support=  $this->ci->user_bbs->adm_select_support_bbs();
        $sParam = '';
        $num_rows=20;
        $where = array();
        $model_list = array();
        /*** If post form ***/
        $this->_action_adm_item_supporter();
        $support_default = $this->ci->user_bbs->select_support_default();
        $customer_services = $this->ci->user_member->adm_select_customer_service();
        $offset = intVal($this->ci->input->get_post('offset'));
        $list = $this->ci->input->get_post('list');
        $search_keyword = $this->ci->input->get_post('search_keyword');
        $search_make = $this->ci->input->get_post('search_make');
        $search_model = $this->ci->input->get_post('search_model');
        $search_year = $this->ci->input->get_post('search_year'); 
        $search_mode =$this->ci->input->get_post('search_mode'); 

        if (empty($offset))
            $offset=0;
        /*** Get make list ***/
        $make_list = $this->ci->user_bbs->getMake(array(), true);

        $get_model_list=$this->ci->user_bbs->carmodeltodropdownsearch();


        if(!empty($search_make)){
            $where_model['car_make']=$search_make;
            $model_list=$this->ci->user_bbs->getModel($where_model, true);
        }
        /*** If button Search is pressed ***/
        if($this->ci->input->get_post('search_btn')){
            if(!empty($search_keyword)){
                $where['search_keyword'] = $search_keyword;
            }
            if(!empty($search_make)){
                $where['car_make'] = $search_make;
            }
            if(!empty($search_model)){
                $where['car_model'] = $search_model;
            }
            if(!empty($search_year)){
                $where['car_model_year'] = $search_year;
            }
        } 
        //var_dump($country_list);

        //$total_support_country_list = $this->ci->user_bbs->get_total_bbs_support_country();
        $support_country_list = $this->ci->user_bbs->get_bbs_support_country();
        if($search_mode=='exist'){
            $support_car_list = $this->ci->user_bbs->adm_select_bbs_for_support(true, $where, $offset, $num_rows);
        }else{
            $support_car_list = $this->ci->user_bbs->adm_select_bbs_for_support(false, $where, $offset, $num_rows);
        }
            
        /*** Pagination for custom list ***/
        $gets = $_GET;
        if(isset($gets['list'])){
            unset($gets['list']);
        }

        $this->ci->load->library('pagination');
        $config['base_url'] = '/?'.$this->get_pagination_url($gets, 'offset');
        $config['total_rows'] = $support_car_list->total_rows;
        $config['per_page'] = $num_rows;
        $config['query_string_segment'] = 'offset';
        $this->ci->pagination->initialize($config);
        $pagination = $this->ci->pagination->create_links();
        func_set_data($this->ci, 'pagination', $pagination);

        $support_default_user = $support_default->result();
        //var_dump($support_default_user);
        //sets search data
        if(count($support_default_user)>0){
            func_set_data($this->ci, 'support_default', $support_default_user[0]);
        }else{
            func_set_data($this->ci, 'support_default', NULL);
        }
        func_set_data($this->ci, 'search_keyword', $search_keyword);
        func_set_data($this->ci, 'search_make', $search_make);
        func_set_data($this->ci, 'search_model', $search_model);
        func_set_data($this->ci, 'search_year', $search_year);
        func_set_data($this->ci, 'search_mode', $search_mode);

        func_set_data($this->ci, 'customer_services', $customer_services);
        func_set_data($this->ci, 'list',$list);
        func_set_data($this->ci, 'make_list', $make_list);
        
        func_set_data($this->ci, 'model_list', $model_list);

        func_set_data($this->ci, 'get_model_list', $get_model_list);

        func_set_data($this->ci, 'search_keyword', $search_keyword);
        func_set_data($this->ci, 'support_car_list',$support_car_list->result());
        func_set_data($this->ci, 'support_country_list',$support_country_list);
        $this->_load_view_layout('admin/adm_item_supporter',$this->ci->data, false);
    }

    function adm_add_item_country_supporter(){
        ob_clean();
        $where = array();

        /*** If button Set is pressed ***/
        if($this->ci->input->get_post('btn_set_country')){
            //var_dump($this->ci->input->get_post('selected_countries'));
            $selected_countries = $this->ci->input->get_post('selected_countries');
            $member_no = $this->ci->input->get_post('selected_member');
            if(count($selected_countries)>0 && $member_no!=''){
                $is_support_country = $this->ci->user_bbs->is_insert_support($selected_countries, $member_no);
                if($is_support_country[0]->support_member_no < 1 ){
                    $this->ci->user_bbs->insert_support_by_country($selected_countries, $member_no);

                    echo func_jsAlertReplace('You have added the supporters successfully!', $this->current_url());

                }else{
                    echo func_jsAlertReplace('The supporter you has set is already!', $this->current_url());
                }
            }
            //redirect($this->current_url());
        }
        $country_list = $this->ci->user_bbs->get_support_country(false);
        $customer_services = $this->ci->user_member->adm_select_customer_service();
        //var_dump($country_list);
        func_set_data($this->ci, 'country_list', $country_list);
        func_set_data($this->ci, 'customer_services', $customer_services);
        $this->ci->load->view('admin/adm_add_item_country_supporter', $this->ci->data, false);
    }

    function test_page(){
        // Signup
        //$this->ci->mo_parse->update(1027);
        //$this->ci->mo_parse->delete_by_idx(1027);
        //$this->ci->mo_parse->insert();
        if(isset($_GET['p_num'])) $page_num = $_GET['p_num']; else $page_num=674824;
        require_once 'libraries/class-scrap-sbtjapan.php';
        // require_once 'libraries/class-scrap-broomjapan.php';
        // require_once 'libraries/class-scrap-primegt.php';
        // $items = ScrapSbtjapan::getAllItemsFromHtml($page_num);

        
        // $items = ScrapPrimegt::getSearchResult($page_num);

        
        // $items = ScrapBroomjapan::getAllItemsFromHtml($page_num);

        // $items = ScrapSbtjapan::getAllAvailableItems($page_num);

        $items = ScrapSbtjapan::getItemDetailById($page_num);
        // $items = ScrapBroomjapan::getItemDetailById(10012765);


        $json = json_encode($items);
        echo $json;
    }
    
    function adm_validate_change_supporter(){
        $country = $this->ci->input->get_post('country');
        $member_no = $this->ci->input->get_post('member_no');
        $change_supporter = $this->ci->user_bbs->validate_change_supporter($country, $member_no);
        if ($change_supporter[0]->num_rows>0) {
            echo $msg = 1;
        }

        echo $msg = 0;
    }

    function adm_change_supporter(){
        $msg ="";
        $data = array();
        $id = $this->ci->input->get_post('id');
        $data['bbs_country'] = $this->ci->input->get_post('country');
        $selected_member =$this->ci->input->get_post('selected_member');
        $data['support_member_no'] = $this->ci->input->get_post('selected_member');

        if (isset($_POST['change']))   {

            if($selected_member !=''){

                $this->ci->user_bbs->update_supporter($id,$data);
                $msg ="You have change the supporters successfully!";

            }else{

                $msg ="Please select supporters before you save!";
                
            }
        }

        $selected_supporter = $this->ci->user_bbs->select_supporter($id);
        $customer_services = $this->ci->user_member->adm_select_customer_service();
        func_set_data($this->ci, 'customer_services', $customer_services);
        func_set_data($this->ci, 'msg', $msg);
        func_set_data($this->ci, 'selected_supporter', $selected_supporter);
        $this->ci->load->view('admin/adm_change_supporter', $this->ci->data, false);
    }

    function send_recommend_car(){
        
        if(isset($_SESSION['idx_for_newsletter'])) $idxs =explode("^", $_SESSION['idx_for_newsletter']); else $idxs = array();
        if(count($idxs)>1){
            $send_mode = 'multiple';
        }else{
            $send_mode = 'single';
        }
       // $more_email =$_SESSION['more_email'];

        $where = array();
        $where['member.business_type ='] = 'buyer';
        $car_list = $this->ci->user_bbs->select_by_idxs($idxs);
        //$customer_list = $this->ci->user_bbs->select_by_email($more_email);
        // $customer_list = $this->ci->mo_customer->customer_select($where, 0, 10000);

        $rec_list = $this->ci->mo_email->select_recipient_list();

        func_set_data($this->ci, 'rec_list', $rec_list);
        
        func_set_data($this->ci, 'send_mode', $send_mode);
        func_set_data($this->ci, 'car_list', $car_list);
        // func_set_data($this->ci, 'customer_list', $customer_list->result());
        $this->_load_view_layout('admin/adm_send_recommend_car', $this->ci->data, false);
    }

    function adm_add_email_bbs(){
        $where = array();
        $sParam = '';
        $cur_page = $this->ci->input->get_post("cur_page");

        if ($cur_page == NULL || $cur_page == '')

        $cur_page = 1;

        $page_rows = 20; 

        $page_views = 10;

        $where['member.business_type ='] = 'buyer';

        $sch_condition_customer_name_email = $this->ci->input->get_post('sch_condition_customer_name_email');
        $sch_country = $this->ci->input->get_post('sch_country');

        if ($sch_country !='') {
            $where['sch_country'] = $sch_country;
        }

        if ($sch_condition_customer_name_email !='') {
            $where['keyword_member_name_email'] = $sch_condition_customer_name_email;
        }

        $customer_list = $this->ci->mo_customer->customer_select($where,($cur_page - 1) * $page_rows);
        $country_list = $this->ci->mo_customer->list_country_buyer();
        $total_rows = $customer_list->total_count;

        $row_cnt = $total_rows - ($cur_page - 1) * $page_rows;

        /*** Set variable for view ***/
        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);

        func_set_data($this->ci, 'total_rows', $total_rows);  

        func_set_data($this->ci, 'total_page', $page_info[0]); 

        func_set_data($this->ci, 'row_cnt', $row_cnt);

        func_set_data($this->ci, 'cur_page', $cur_page);

        func_set_data($this->ci, 'page_info', $page_info);

        func_set_data($this->ci, 'sParam', $sParam);

        func_set_data($this->ci, 'country_list', $country_list);

        func_set_data($this->ci, 'customer_list', $customer_list->result());

        // /*** Load View ***/
        $this->ci->load->view('admin/adm_add_email_bbs', $this->ci->data, false);
    }

    function adm_add_customer_bbs(){
        $where = array();
        $sParam = '';
        $cur_page = $this->ci->input->get_post("cur_page");

        if ($cur_page == NULL || $cur_page == '')

        $cur_page = 1;

        $page_rows = 20; 

        $page_views = 10;

        $where['member.business_type ='] = 'buyer';

        $sch_condition_customer_name_email = $this->ci->input->get_post('sch_condition_customer_name_email');
        $sch_country = $this->ci->input->get_post('sch_country');

        if ($sch_country !='') {
            $where['sch_country'] = $sch_country;
        }

        if ($sch_condition_customer_name_email !='') {
            $where['keyword_member_name_email'] = $sch_condition_customer_name_email;
        }

        $customer_list = $this->ci->mo_customer->customer_select($where,($cur_page - 1) * $page_rows);
        $country_list = $this->ci->mo_customer->list_country_buyer();
        $total_rows = $customer_list->total_count;

        $row_cnt = $total_rows - ($cur_page - 1) * $page_rows;

        /*** Set variable for view ***/
        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);

        func_set_data($this->ci, 'total_rows', $total_rows);  

        func_set_data($this->ci, 'total_page', $page_info[0]); 

        func_set_data($this->ci, 'row_cnt', $row_cnt);

        func_set_data($this->ci, 'cur_page', $cur_page);

        func_set_data($this->ci, 'page_info', $page_info);

        func_set_data($this->ci, 'sParam', $sParam);

        func_set_data($this->ci, 'country_list', $country_list);

        func_set_data($this->ci, 'customer_list', $customer_list->result());

        // /*** Load View ***/
        $this->ci->load->view('admin/adm_add_customer_bbs', $this->ci->data, false);
    }

    function add_more_email(){
         $where=array();
        $where['grade_no']='11';
        $where['business_type']='buyer';
         $more_email = $this->ci->input->get_post('more_email');
        $customer_list = $this->ci->user_bbs->select_by_email($more_email);
        $select_email=$this->ci->bbs->selct_add_more_email($where);
        func_set_data($this->ci, 'select_email', $select_email);
       $this->ci->load->view('admin/adm_add_more', $this->ci->data, false);
    }

    function send_recommend_car_exec(){
        $this->ci->car_email->delete_dll_compaign();
        $idxs = $this->ci->input->get_post('idx');
        // $email = $this->ci->input->get_post('email');

        $recipient = $this->ci->input->get_post('recipient');
        $subject = $this->ci->input->get_post('subject');
        $date = $this->ci->input->get_post('date');
        $hour = $this->ci->input->get_post('hour');
        $minute = $this->ci->input->get_post('minute');
        $date_schedule_am_pm = $this->ci->input->get_post('date_schedule_am_pm');

        $car_images = $this->ci->input->get_post('car_image');
        $batch_data = array();
        // foreach ($email as $value) {
            // if(!empty($value)){
                // if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $batch['customer_email'] = $value;
                    $batch['created_dt'] = func_get_date_time();
                    foreach ($idxs as $bbs_idx) {
                        $email_compaign = $this->ci->car_email->check_exist($bbs_idx,$value);
                        if($email_compaign->num_rows==1){
                            $validate = false;
                        }else{
                            $batch['bbs_idx'] = $bbs_idx;
                            $json_param['car_image'] = '';

                            if(isset($car_images[$bbs_idx])){
                                $json_param['car_image'] = $car_images[$bbs_idx];
                            }
                            $validate = true;
                            $batch['json_param'] = json_encode($json_param, JSON_UNESCAPED_SLASHES);
                            $batch_data[] = $batch;
                        }
                    }
                // }
            // }
        // }
        
        // if($validate == true){
        //     $this->ci->user_bbs->insert_car_email_batch($batch_data);
        // }
        // $compagn_list = $this->ci->car_email->select_all();
        // echo json_encode($compagn_list, JSON_UNESCAPED_SLASHES);
        
    }

    function adm_scrape_item(){
        $this->_load_view_layout('admin/adm_scrape_item',$this->ci->data, false);
    }

    function adm_scrape_total_page(){
        ob_clean();
        header('Content-type: application/json');
        if(isset($_POST['website_name'])) $website_name = $_POST['website_name']; else $website_name='';
        if($website_name=='primegt'){
            require_once 'libraries/class-scrap-primegt.php';
            $data['total_page'] = ScrapPrimegt::getNumberOfPage();
            
        }elseif($website_name=='broomjapan'){
            require_once 'libraries/class-scrap-broomjapan.php';
            $data['total_page'] = ScrapBroomjapan::getNumberOfPage();
            
        }elseif($website_name=='sbtjapan'){
            require_once 'libraries/class-scrap-sbtjapan.php';
            $data['total_page'] = ScrapSbtjapan::getNumberOfPage();
        }
        
        $json = json_encode($data);
        echo $json;
    }

    function adm_scrape_json_item(){
        ob_clean();
        header('Content-type: application/json');

        if(isset($_POST['website_name'])) $website_name = $_POST['website_name']; else $website_name='';
        if(isset($_POST['status'])) $status = $_POST['status']; else $status='';
        if(isset($_POST['page_num'])) $page_num = $_POST['page_num']; else $page_num=1;
        if($website_name=='primegt'){
            require_once 'libraries/class-scrap-primegt.php';
            if($status=='sold'){
                $items = ScrapPrimegt::getSoldoutItems($page_num);
            }else{
                $items = ScrapPrimegt::getAllAvailableItems($page_num);
            }
        }elseif($website_name=='broomjapan'){
            require_once 'libraries/class-scrap-broomjapan.php';
            if($status=='sold'){
                $items = ScrapBroomjapan::getSoldoutItems($page_num);
            }else{
                $items = ScrapBroomjapan::getAllAvailableItems($page_num);
            }
        }elseif($website_name=='sbtjapan'){
            require_once 'libraries/class-scrap-sbtjapan.php';
            $items = ScrapSbtjapan::getAllAvailableItems($page_num);
        }
        $json = json_encode($items);
        echo $json;
    }

    function adm_scrape_json_item_detail(){
        ob_clean();
        header('Content-type: application/json');
        
        if(isset($_GET['website_name'])) $website_name = $_GET['website_name']; else $website_name='';
        $id = $_GET['id'];

        if($website_name=='primegt'){
            require_once 'libraries/class-scrap-primegt.php';
            $item_detail = ScrapPrimegt::getItemDetailById($id);
        }elseif($website_name=='broomjapan'){
            require_once 'libraries/class-scrap-broomjapan.php';
            $item_detail = ScrapBroomjapan::getItemDetailById($id);
        }elseif($website_name=='sbtjapan'){
            require_once 'libraries/class-scrap-sbtjapan.php';
            $item_detail = ScrapSbtjapan::getItemDetailById($id);
        }

        $json = json_encode($item_detail);
        echo $json;
    }

    function adm_json_scrape_count_by_chassis(){
        ob_clean();
        header('Content-type: application/json');
        $arr_chassis = array();
        $chassis_no_str = $this->ci->input->post('chassis_no');
        $chassis_no = explode('^', $chassis_no_str);
        //if(isset($_POST['chassis_no'])) $chassis_no = $_POST['chassis_no']; else $chassis_no=array();
        if(count($chassis_no)>0){
            $arr_chassis = $this->ci->bbs->count_by_chassis($chassis_no);
        }
        foreach($arr_chassis as $key=>$value){

            $arr_chassis[$key]->timestamp = strtotime($value->created_dt);
        }
        $json = json_encode($arr_chassis);
        echo $json;
    }

    function adm_json_scrape_update_all(){
        ob_clean();
        header('Content-type: application/json');
        $allowed_drive = array('4WD', '2WD');
        $valid_trans = false;
        $transmission_dic = array('AT'=>"Auto", 'MT'=>'Manual', 'CVT'=>'Auto', '5F'=>'Manual');
        //$update_mode = $this->ci->input->get_post('update_mode');
        $website_name = $this->ci->input->post('website_name');
        $data['idx'] = $this->ci->input->post('idx');
        $data['car_chassis_no'] = $this->ci->input->post('car_chassis_no');
        $data['icon_status'] = $this->ci->input->post('icon_status');
        $data['car_body_type'] = $this->ci->input->post('car_body_type');
        $data['car_make'] = $this->ci->input->post('car_make');
        $data['car_color'] = $this->ci->input->post('car_color');
        $data['car_model'] = $this->ci->input->post('car_model');
        $data['car_steering'] = $this->ci->input->post('car_steering');
        $data['car_drive_type'] = $this->ci->input->post('car_drive_type');
        $data['car_transmission'] = $this->ci->input->post('car_transmission');
        $data['car_cc'] = $this->ci->input->post('car_cc');
        $data['door'] = $this->ci->input->post('door');
        $data['car_fuel'] = $this->ci->input->post('car_fuel');
        $data['car_seat'] = $this->ci->input->post('car_seat');
        $data['car_length'] = $this->ci->input->post('car_length');
        $data['car_width'] = $this->ci->input->post('car_width');
        $data['car_height'] = $this->ci->input->post('car_height');
        $data['first_registration_year'] = $this->ci->input->post('first_registration_year');
        $data['first_registration_month'] = $this->ci->input->post('first_registration_month');
        $data['car_mileage'] = $this->ci->input->post('car_mileage');
        $car_fob_cost = $this->ci->input->post('car_fob_cost');
        $data['car_fob_currency'] = $this->ci->input->post('car_fob_currency');

        $data['car_model_year'] = $this->ci->input->post('first_registration_year');
        $data['manu_year'] = $this->ci->input->post('manu_year');
        $data['manu_month'] = $this->ci->input->post('manu_month');
        $data['icon_new_yn'] = $this->ci->input->post('icon_new_yn');
        $data['old_fob_cost'] = $this->ci->input->post('old_fob_cost');

        if(!in_array($data['car_drive_type'], $allowed_drive)){
            $data['car_drive_type']='2WD';
        }

        // if(isset($transmission_dic[$data['car_transmission']])){
        //     $data['car_transmission'] = $transmission_dic[$data['car_transmission']];
        // }else{
        //     unset($data['car_transmission']);
        // }
        foreach($transmission_dic as $key=>$value){
            if (strpos($data['car_transmission'],$key) !== false) {
                $valid_trans = true;
                $data['car_transmission'] = $value;
            }
        }
        if($valid_trans==false){
            unset($data['car_transmission']);
        }

        /*** Loop each rows ***/
        //echo "Update Mode: ".$update_mode;
        $where_exist['car_chassis_no'] = $data['car_chassis_no'];
        $is_car_exist = $this->ci->bbs->is_car_exist($where_exist);

        if(!empty($data['car_chassis_no'])){   

            $member_detail = $this->ci->mo_webscrape->get_member_detail_by_webname($website_name);
            $fob_commission = $member_detail->fob_commission;
            if($is_car_exist){
                $where['ignore_scan_update'] = 0;
                $where['car_chassis_no'] = $data['car_chassis_no'];
                
                $data['car_owner'] = $member_detail->member_no;
                $data['country'] = $member_detail->member_country;
                if(!empty($car_fob_cost)){

                    $data['old_fob_cost']+=$fob_commission;
                    $data['car_fob_cost'] =  $car_fob_cost+$fob_commission;
                }else{
                    $data['car_fob_cost'] =  $car_fob_cost;
                }
                $data['car_fob_cost_orig'] =  $car_fob_cost;

                $result = $this->ci->bbs->adm_update($data, $where);
                //$this->updateSQLite($data);
                /*$json = json_encode($result);
                echo $json;*/
            }else{
                $data['car_owner'] = $member_detail->member_no;

                $data['country'] = $member_detail->member_country;
                
                if(!empty($car_fob_cost)){
                    $data['old_fob_cost']+=$fob_commission;
                    $data['car_fob_cost'] =  $car_fob_cost+$fob_commission;
                }else{
                    $data['car_fob_cost'] =  $car_fob_cost;
                }
                $data['car_fob_cost_orig'] =  $car_fob_cost;

                $car_id = $this->ci->bbs->insert($data);
                $stock_id=$this->ci->bbs->getStockID($car_id);
                // $created = date('Y-m-d H:i:s');

                $data['idx']=$car_id;
                $data['stock_id']=$stock_id[0]->stock_id;
                // $data['created']=$created;
                //$this->insertSQLite($data);
                /*$json = json_encode($result);
                echo $json;*/
            }

            
        }
    }

    function adm_upload_scrape_images(){
        ob_clean();
        header('Content-type: application/json');

        $chassis_no = $this->ci->input->post('chassis_no');
        $images = $this->ci->input->post('images');
        $arr=array();
        
        $products = $this->ci->bbs->get_detail_by_chassis($chassis_no);

        $product_id = $products[0]->idx;
        $owner = $products[0]->car_owner;
        $data=array();

        //$cloud_options = array('tags'=>array('item', 'test'));
        $count_image = $this->ci->bbs->count_image(array('bbs_idx'=>$product_id));
        if($count_image==0&&!empty($product_id)){
            $cloud_options = array('tags'=>array('item'));
            $i=0;
            foreach($images as $image){
                $upload_result = \Cloudinary\Uploader::upload($image, $cloud_options);
                /***GET BASE URL***/
                $exp_url = explode('/', $upload_result['secure_url']);
                $exp_last_key = count($exp_url)-1;
                $exp_url[$exp_last_key]='';
                unset($exp_url[$exp_last_key-1]);
                $data['base_url'] = implode('/', $exp_url);
               // $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);

                $data['bbs_idx'] = $product_id;
                $data['public_id']          = $upload_result['public_id'];
                $data['resource_type']      = $upload_result['resource_type'];
                $data['secure_url']         = $data['base_url'].$data['public_id'];
                $data['original_filename']  = basename($image);
                $data['sort'] = $i;
                $batch_data[] = $data;
                $i++;
                // $data[] = array(
                //         'source' => $image,
                //         'thumb' => $baseUrl.$abDirPath.$filename,
                //         'primary' => 0
                //     );

            }
            

            $this->ci->bbs->insert_image_batch($batch_data);
            //$this->insertImageSQLite($batch_data);
            $count_image = $this->ci->bbs->count_image(array('bbs_idx'=>$product_id));
            
        }
        $arr[] = $count_image;
        // //var_dump($data);
        $json = json_encode($arr);
        echo $json;
    }
    function _load_view_layout($view_path, $css, $js){

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view($view_path, $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }

    function _load_member_view($view_path, $arr_ex_js=array(), $arr_ex_css=array()){
        $sidebar = $this->ci->load->view('member_site/product/sidebar.php', $this->ci->data, true);
        func_set_data($this->ci,'sidebar', $sidebar);

        func_set_data($this->ci, 'arr_ex_css', $arr_ex_css);
        func_set_data($this->ci, 'arr_ex_js', $arr_ex_js);
        $total_bbs_count = $this->ci->user_bbs->user_total_select();
        func_set_data($this->ci, 'total_bbs_count', $total_bbs_count);
        func_set_data($this->ci, 'page_content', $this->ci->load->view($view_path, $this->ci->data, true));
        $this->ci->load->view('member_site/layout/template.php', $this->ci->data, false);
    }
	
	
	function _load_member_mobile_view($view_path, $arr_ex_js=array(), $arr_ex_css=array()){
        func_set_data($this->ci, 'arr_ex_css', $arr_ex_css);
        func_set_data($this->ci, 'arr_ex_js', $arr_ex_js);
        
        $total_bbs_count = $this->ci->bbs->user_total_select();
        $total_statistics_cnt = $this->ci->statistics->select_total_cnt();
        $total_statistics = $total_statistics_cnt[0]->total_cnt;
        func_set_data($this->ci, 'total_bbs_count', $total_bbs_count);
        func_set_data($this->ci, 'total_statistics', $total_statistics); 

        //========== Get Make to search box ============//
        $carmaketodropdownsearch = $this->ci->bbs->carmaketodropdownsearch();
        func_set_data($this->ci, 'carmaketodropdownsearch',$carmaketodropdownsearch);

        //========== Get Model to search box ============//
        $carmodeltodropdownsearch = $this->ci->bbs->carmodeltodropdownsearch();
        func_set_data($this->ci, 'carmodeltodropdownsearch',$carmodeltodropdownsearch);

        func_set_data($this->ci, 'page_content', $this->ci->load->view($view_path, $this->ci->data, true));
        $this->ci->load->view('member_mobile_site/layout/template.php', $this->ci->data, false);
    }
	

    function test_member_view(){
        
        $arr_js = array('/js/member_site/test.js');
        $arr_css = array('/css/member_site/test.css');
        $this->_load_member_view('member_site/tests/test.php', $arr_js, $arr_css);
    }
	
	function mobile_list_car(){
        $this->ci->load->helper('url');
        $this->ci->load->library('pagination'); 
        $where=array();
        $gets = $_GET;
        $get_values = array();
        $order_by = $this->ci->input->get_post("order_by");
        $order_d = $this->ci->input->get_post("order_d");
        $order='icon_status ASC, created_dt DESC';
        if(!empty($order_by)){
            $order = $order_by." ".$order_d;
        }
        // search condition
        $grade_no_session = $this->ci->grade_no;
        $member_no_session = $this->ci->member_no;
          if ($grade_no_session == 11){
             $where['car_owner'] = $member_no_session;
             if(isset($where['car_keyword'])){
                unset($where['car_keyword']);
           }

          }
           $carmake = $this->ci->input->get_post('carmake');
         if ($carmake !='') {
            $where['car_make'] = $carmake;
         }
        $carmodel = $this->ci->input->get_post('carmodel');
          if ($carmodel  !='') {
            $where['car_model'] = $carmodel;
        }
          $year = $this->ci->input->get_post('year');
        if ($year !='') {
            $where['car_model_year'] = $year;
        }
        $sch_icon_status = $this->ci->input->get_post('sch_icon_status');
        if ($sch_icon_status) {
            $where['icon_status'] = $sch_icon_status;
        }

       $sch_word = $this->ci->input->get_post('sch_word');
        if ($sch_word !='') {
            $where['car_keyword'] ='%'.$sch_word.'%';  
           
        }
        // end search
        $customer_no= $_SESSION['ADMIN']['member_no'];
        $limit_s = $this->ci->input->get_post("offset");
        if(!$limit_s) $limit_s =0;
        $order = "icon_status ASC, created_dt DESC";
        $get_url = $this->unsetGetUrl(array("offset"));
        $result_total_rows = $this->ci->bbs->adm_total_select($where);
        $total_rows = $result_total_rows['0']->num_rows;
        $cur_page = $this->ci->input->get_post("offset");
        if ($cur_page == NULL || $cur_page == '')
        $cur_page = 1;


        /*****START PAGINATION*****/
        $config['base_url'] = site_url()."/?".$get_url;
        $config['uri_segment'] = 3;
        $config['num_links'] = 4;
        $config['total_rows'] = $total_rows;
        $config['per_page'] =10;
        $config['first_link'] = '&#60;&#60;';
        $config['first_tag_open'] = '<div class="first_page_paginate">';
        $config['first_tag_close'] = '</div>';
        $config['prev_link']='Previous';
        $config['next_link']='Next';
        $config['last_link'] = '&#62;&#62;';
        $config['last_tag_open'] = '<div class="last_page_paginate">';
        $config['last_tag_close'] = '</div>';
        $config['full_tag_open'] = '<div class="list_page_paginate">';
        $config['full_tag_close'] = '</div><div class="clear"></div>'; 
        $config['query_string_segment'] = "offset";
        $this->ci->pagination->initialize($config);
        $pagination = $this->ci->pagination->create_links();
        $cuurent=$config['use_page_numbers'] = TRUE;
        /*****END PAGINATION*****/
        $cur_page =$limit_s/10;
        $total_page=$total_rows;
       
        $bbs_list_sale = $this->ci->bbs->adm_seller_select($customer_no,$where,$limit_s,$config['per_page'],$order);
        $select_make_all = $this->ci->bbs->carmaketodropdownsearch();
        $select_model_all = $this->ci->bbs->carmodeltodropdownsearch();

        $model_list = array();
        $where_model['car_make']=$carmake;
        $model_list = $this->ci->bbs->getModel($where_model, true);
        
        $show_search_box = true;
        func_set_data($this->ci, 'show_search_box',$show_search_box);   

        func_set_data($this->ci, 'bbs_list_sale', $bbs_list_sale);
        func_set_data($this->ci, 'select_make_all',$select_make_all);

        func_set_data($this->ci, 'select_model_all',$select_model_all);

        func_set_data($this->ci, 'model_list',$model_list);
        func_set_data($this->ci, 'pagination', $pagination);
        func_set_data($this->ci, 'total_rows', $total_rows);
        func_set_data($this->ci, 'cur_page', $cur_page);
        func_set_data($this->ci, 'total_page', $total_page);
        // func_set_data($this->ci, 'count', $count);

      	$arr_js = array('/js/member_mobile_site/car_list.js','/js/admin/bootstrap.min.js');
        $arr_css = array('/css/member_mobile_site/car_list.css','/css/member_mobile_site/bootstrap.min.css');
        $this->_load_member_mobile_view('member_mobile_site/product/list_car.php', $arr_js, $arr_css);
		
    }
	
    function mobile_buyer_list_car(){
        $this->ci->load->helper('url');
        $this->ci->load->library('pagination'); 
        $member_log= $_SESSION['ADMIN']['member_no'];
        $where=array();
        $gets = $_GET;
        $get_values = array();
        $order_by = $this->ci->input->get_post("order_by");
        $order_d = $this->ci->input->get_post("order_d");
        $order='icon_status ASC, created_dt DESC';
        if(!empty($order_by)){
            $order = $order_by." ".$order_d;
        }
        // search condition
        $grade_no_session = $this->ci->grade_no;
          if ($grade_no_session == 11){
             if(isset($where['car_keyword'])){
                unset($where['car_keyword']);
           }

          }
           $carmake = $this->ci->input->get_post('carmake');
         if ($carmake !='') {
            $where['car_make'] = $carmake;
         }
        $carmodel = $this->ci->input->get_post('carmodel');
          if ($carmodel  !='') {
            $where['car_model'] = $carmodel;
        }
          $year = $this->ci->input->get_post('year');
        if ($year !='') {
            $where['car_model_year'] = $year;
        }
        $sch_icon_status = $this->ci->input->get_post('sch_icon_status');
        if ($sch_icon_status) {
            $where['icon_status'] = $sch_icon_status;
        }

       $sch_word = $this->ci->input->get_post('sch_word');
        if ($sch_word !='') {
            $where['car_keyword'] ='%'.$sch_word.'%';  
           
        }
        // end search
        $limit_s = $this->ci->input->get_post("offset");
        if(!$limit_s) $limit_s =0;
        $order = "icon_status ASC, created_dt DESC";
        $get_url = $this->unsetGetUrl(array("offset"));
        $result_total_rows = $this->ci->bbs->adm_buyers_total_select($where,$member_log);
        $total_rows = $result_total_rows['0']->num_rows;
        $cur_page = $this->ci->input->get_post("offset");
        if ($cur_page == NULL || $cur_page == '')
        $cur_page = 1;


        /*****START PAGINATION*****/
        $config['base_url'] = site_url()."/?".$get_url;
        $config['uri_segment'] = 3;
        $config['num_links'] = 4;
        $config['total_rows'] = $total_rows;
        $config['per_page'] =10;
        $config['first_link'] = '&#60;&#60;';
        $config['first_tag_open'] = '<div class="first_page_paginate">';
        $config['first_tag_close'] = '</div>';
        $config['prev_link']='Previous';
        $config['next_link']='Next';
        $config['last_link'] = '&#62;&#62;';
        $config['last_tag_open'] = '<div class="last_page_paginate">';
        $config['last_tag_close'] = '</div>';
        $config['full_tag_open'] = '<div class="list_page_paginate">';
        $config['full_tag_close'] = '</div><div class="clear"></div>'; 
        $config['query_string_segment'] = "offset";
        $this->ci->pagination->initialize($config);
        $pagination = $this->ci->pagination->create_links();
        $cuurent=$config['use_page_numbers'] = TRUE;
        /*****END PAGINATION*****/
        $cur_page =$limit_s/10;
        $total_page=$total_rows;
       
        $bbs_list_buyer = $this->ci->bbs->adm_buyers_select($member_log,$where,$limit_s,$config['per_page'],$order);
        $select_make_all = $this->ci->bbs->carmaketodropdownsearch();
        $select_model_all = $this->ci->bbs->carmodeltodropdownsearch();

        $model_list = array();
        $where_model['car_make']=$carmake;
        $model_list = $this->ci->bbs->getModel($where_model, true);
        
        $show_search_box = true;
        func_set_data($this->ci, 'show_search_box',$show_search_box);   

        func_set_data($this->ci, 'bbs_list_buyer', $bbs_list_buyer);
        func_set_data($this->ci, 'select_make_all',$select_make_all);

        func_set_data($this->ci, 'select_model_all',$select_model_all);

        func_set_data($this->ci, 'model_list',$model_list);
        func_set_data($this->ci, 'pagination', $pagination);
        func_set_data($this->ci, 'total_rows', $total_rows);
        func_set_data($this->ci, 'cur_page', $cur_page);
        func_set_data($this->ci, 'total_page', $total_page);
        // func_set_data($this->ci, 'count', $count);

        $arr_js = array('/js/member_mobile_site/buyer_car_list.js','/js/admin/bootstrap.min.js');
        $arr_css = array('/css/member_mobile_site/buyer_car_list.css','/css/member_mobile_site/bootstrap.min.css');
        $this->_load_member_mobile_view('member_mobile_site/product/buyer_list_car.php', $arr_js, $arr_css);
        
    }

    function mobile_buyer_car_detail(){


        if(isset($_GET["idx"])) $id = $_GET["idx"];
        $customer_no = $_SESSION['ADMIN']['member_no'];
        $where['customer_no'] = $customer_no;
            // echo $id; die();
            
        $cardetail = $this->ci->bbs->getCarSpecification($id,$where);

        if ($cardetail[0]->customer_no!= $customer_no) {
            header("Location: /?c=user&m=mobile_home");
        }
        $getprimaryImage = $this->ci->bbs->getCarDetailPrimaryImage($id);
        $getDetailImage = $this->ci->bbs->getCarDetailImage($id);
        
        $list_country=  $this->ci->bbs->get_country_list();

        /*** GET CUSTOMER SUPPORT (SALES TEAM) ***/

        $where['idx'] = $id;

        $bbs_attach_files = $this->ci->bbs->select_attach_files($id);

        func_set_data($this->ci, 'bbs_attach_files', $bbs_attach_files->result());
            
        func_set_data($this->ci, 'cardetail',$cardetail);

        func_set_data($this->ci, 'getDetailImage',$getDetailImage);
        
        func_set_data($this->ci, 'getPrimaryImage',$getprimaryImage);

        $arr_js = array('/js/tooltipster/jquery.tooltipster.min.js',
            '/js/member_mobile_site/buyer_car_detail.js',
            '/js/member_mobile_site/lightbox.js');

        $arr_css = array('/js/tooltipster/tooltipster.css',
            '/css/member_mobile_site/buyer_car_detail.css');

        $show_search_box = true;
        func_set_data($this->ci, 'show_search_box',$show_search_box);

        func_set_data($this->ci, 'list_country',$list_country);

        func_set_data($this->ci, 'arr_js',$arr_js);
        func_set_data($this->ci, 'arr_css',$arr_css);
        $this->_load_member_mobile_view('member_mobile_site/product/buyer_car_detail.php', $arr_js, $arr_css);  
    }
	
	 function mobile_addcar() {
        $this->ci->load->helper('url');
        $insert_id = $this->ci->bbs->insert_draft();
        ob_clean();
        header("Location: /?c=admin&m=mobile_modify_car&mcd=product&category=offer&create_mode=new&idx=".$insert_id);
    }

	function mobile_modify_car(){
        $idx = $this->ci->input->get_post('idx');
        $bbs_detail_list = $this->ci->bbs->select_detail_for_seller($idx);
        
         if($_SESSION['ADMIN']['session_grade_no'] ==11){
            $car_owner = $this->ci->user_member->iscar_owner($idx);
            if($car_owner[0]->num_row == 0){

                redirect('/?c=admin', 'refresh');
            }

        }
        $where_model['make_name'] = $bbs_detail_list[0]->car_make;
        $bodytype_list = $this->ci->bbs->get_bodytype_list();
        $country_list = $this->ci->bbs->admGetCountry_list();
        $member_logged = $this->ci->user_member->select_member_login();
        $color_list = $this->ci->bbs->get_color_options();
        $make_list = $this->ci->bbs->admGetMake_table_seller();
        $model_list = $this->ci->bbs->getModel_table($where_model);

        $all_model_list = $this->ci->bbs->getModel_table();

        func_set_data($this->ci, 'bodytype_list', $bodytype_list);
        func_set_data($this->ci, 'member_logged', $member_logged);
        func_set_data($this->ci, 'country_list', $country_list);
        func_set_data($this->ci, 'color_list', $color_list);
        func_set_data($this->ci, 'make_list', $make_list);
        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);
        func_set_data($this->ci, 'model_list', $model_list);
        func_set_data($this->ci, 'all_model_list', $all_model_list);

      	$arr_js = array('/js/member_mobile_site/add_car.js','//widget.cloudinary.com/global/all.js','/editor/tiny_mce/tiny_mce.js','/editor/tiny_mce/mobile_tiny_mce_load.js');
        $arr_css = array('/css/member_mobile_site/addcar.css');
        $this->_load_member_mobile_view('member_mobile_site/product/add-car', $arr_js, $arr_css);
		
    }
	
	function mobile_modify_car_exec(){

        /*** GET PARAMETERS ***/
        $idx = $this->ci->input->get_post('idx');
        $create_mode = $this->ci->input->get_post('create_mode');

         /*** Validate ChassisNo ***/
        $chassis_no = $_POST['values']['car_chassis_no'];
        $vali = mysql_query("SELECT idx FROM iw_bbs WHERE car_chassis_no = '{$chassis_no}' AND idx != '{$idx}' ");
        $chassis_check = mysql_num_rows($vali);

        if ($chassis_check > 0) {
            echo "<script language='javascript'>";
            echo "confirm('Please change ChassisNo. There is same ChassisNo.'); ";
            echo "window.location.href = '/?c=admin&m=mobile_modify_car&mcd=product&category=offer&idx={$idx}';";
            echo "</script>";
        }
        else{
            $bbs_detail = $this->ci->bbs->select_detail_for_seller($idx);

            $inData = array();
            $inData = func_get_post_array($this->ci->input->post('values'));
            $inData['idx'] = $idx;
            $inData['menu_code'] = 'product';       
            $inData['category'] = 'offer';
            $inData['draft']  = 'N';

            /***SET DEFAULT VISIBLE TO YES***/
            if(!isset($inData['publish'])){
                $inData['publish'] = 'Y';
            }
            if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

                $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

                unset($inData['zipcode1']);

                unset($inData['zipcode2']);
            }

            if (!isset($inData['notice_yn'])) {        //공지여부
                $inData['notice_yn'] = 'N';
            }

            if (!isset($inData['updated_dt']) || ( $inData['updated_dt'] == '')) {         //작성일
                $inData['updated_dt'] = func_get_date_time();
            }

            if(empty($bbs_detail[0]->car_stock_no)){
                $inData['car_stock_no'] = $this->ci->bbs->get_max_car_stock_no() +1;
            }

            $inData['writer_ip'] = $this->ci->input->ip_address();

            if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

                $inData['publish_start'] = $inData['publish_end'] . ' 00:00:00';

                $inData['publish_end'] = $inData['publish_end'] . ' 23:59:59';
            }

            /*** UPDATE CAR ***/
            if(isset($_POST['values']['create_mode']))
            {
                $create_mode=$_POST['values']['create_mode'];
                unset($inData['create_mode']);
            }
            $this->ci->bbs->update($inData);

            $data = array();
            $data['make_name'] = $inData['car_make'];
            $insert_id = $this->ci->bbs->car_make_insert($data);

            $data_model = array();
            $data_model['make_id'] = $insert_id;
            $data_model['model_name'] = $inData['car_model'];
            $this->ci->bbs->car_model_insert($data_model);

            // SEND GCM AND UPDATE SQLITE FOR ANDROID

            $inData['icon_status']=$inData['icon_new_yn'];
            $inData['car_owner']=$bbs_detail[0]->car_owner;

            if(isset($create_mode) && $create_mode=='new')
            {
                //$this->pushNewAndUpdateNotification($inData,0);
                //$this->insertSQLite($inData);

                $mobile_insert_log = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no.' ) has been inserted by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'car',
                    'created_dt'=>func_get_date_time()
                );
                $this->ci->mo_activity_log->insert($mobile_insert_log);
            }
            else
            {
                //$this->pushNewAndUpdateNotification($inData,1);
                //$this->updateSQLite($inData);

                $mobile_modify_log = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no.' ) has been updated by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'car',
                    'created_dt'=>func_get_date_time()
                );
                $this->ci->mo_activity_log->insert($mobile_modify_log);
            }

            redirect('/?c=admin&m=mobile_list_car', 'refresh');
        }

    }
	
	function member_info(){
        $grade_no = 10;
        $country_list = $this->ci->bbs->admGetCountry_list();
        $grade_no_list = $this->ci->member->select_grade_no($grade_no);
        $row = $this->ci->member->select_member_login();

        $business_type = $_SESSION['ADMIN']['business_type'];
        $access_type = true;
        if($business_type=='buyer'){
            $access_type = false;
        }
        func_set_data($this->ci, 'access_type', $access_type);

        $nav_array = array(
            "Home"=>"/",
            "Dashboard"=>"/?c=admin",
            "My Info"=>"#",
        );
        $navigation_bar = func_get_nav($nav_array);
        func_set_data($this->ci, 'navigation_bar', $navigation_bar);

        func_set_data($this->ci, 'row', $row);
        func_set_data($this->ci, 'country_list', $country_list);
        func_set_data($this->ci, 'grade_no_list', $grade_no_list->result());

        
      	$arr_js = array('/intlTelInput/build/js/intlTelInput.js','/js/member_site/member_info.js');
        $arr_css = array('/intlTelInput/build/css/intlTelInput.css','/css/member_site/mem_info.css');
        $this->_load_member_view('member_site/member/info.php', $arr_js, $arr_css);
		
    }
    function mobile_info(){
        $grade_no = 10;
        $country_list = $this->ci->bbs->admGetCountry_list();
        $grade_no_list = $this->ci->member->select_grade_no($grade_no);
        $row = $this->ci->member->select_member_login();
        func_set_data($this->ci, 'row', $row);
        func_set_data($this->ci, 'country_list', $country_list);
        func_set_data($this->ci, 'grade_no_list', $grade_no_list->result());

        
        $arr_js = array('/intlTelInput/build/js/intlTelInput.js','/js/member_mobile_site/mobile_info.js');
        $arr_css = array('/intlTelInput/build/css/intlTelInput.css','/css/member_mobile_site/mobile_info.css');
        func_set_data($this->ci, 'arr_js', $arr_js);
        func_set_data($this->ci, 'arr_css', $arr_css);
        
        $this->_load_member_mobile_view('member_mobile_site/member/info.php', $arr_js, $arr_css);
        
    }

    function mobile_info_buyer(){
        $grade_no = 10;
        $country_list = $this->ci->bbs->admGetCountry_list();
        $grade_no_list = $this->ci->member->select_grade_no($grade_no);
        $row = $this->ci->member->select_member_login();
        func_set_data($this->ci, 'row', $row);
        func_set_data($this->ci, 'country_list', $country_list);
        func_set_data($this->ci, 'grade_no_list', $grade_no_list->result());

        
        $arr_js = array('/intlTelInput/build/js/intlTelInput.js','/js/member_mobile_site/mobile_info.js');
        $arr_css = array('/intlTelInput/build/css/intlTelInput.css','/css/member_mobile_site/mobile_info.css');
        func_set_data($this->ci, 'arr_js', $arr_js);
        func_set_data($this->ci, 'arr_css', $arr_css);
        
        $this->_load_member_mobile_view('member_mobile_site/member/info.php', $arr_js, $arr_css);
        
    }
	
    // function addcar() {
    //     $this->ci->load->helper('url');
    //     $insert_id = $this->ci->bbs->insert_draft();
    //     ob_clean();
    //     header("Location: /?c=admin&m=modify_car&mcd=product&category=offer&create_mode=new&idx=".$insert_id);
    // }

    function addcar(){

        $protocol   = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
        $host       = $_SERVER['HTTP_HOST'];
        $params     = $_SERVER['QUERY_STRING'];
        $currentUrl = $protocol . '://' . $host . '?' . $params;

            $nav_array = array(
                "Home"=>"/?c=admin",
                "Dashboard"=>"/?c=admin",
                "My Car List"=>"/?c=admin&m=list_car",
                "Post Car"=>$currentUrl
            );

        $business_type = $_SESSION['ADMIN']['business_type'];
        $access_type = true;
        if($business_type=='buyer'){
            $access_type = false;
        }
        func_set_data($this->ci, 'access_type', $access_type);

        $idx = $this->ci->input->get_post('idx');
        $bodytype_list = $this->ci->bbs->get_bodytype_list();
        $country_list = $this->ci->bbs->admGetCountry_list();
        $member_logged = $this->ci->user_member->select_member_login();
        $color_list = $this->ci->bbs->get_color_options();
        $make_list = $this->ci->bbs->admGetMake_table_seller();
        $all_model_list = $this->ci->bbs->getModel_table();

        $navigation_bar = func_get_nav($nav_array);
        func_set_data($this->ci, 'navigation_bar', $navigation_bar);
        func_set_data($this->ci, 'bodytype_list', $bodytype_list);
        func_set_data($this->ci, 'member_logged', $member_logged);
        func_set_data($this->ci, 'country_list', $country_list);
        func_set_data($this->ci, 'color_list', $color_list);
        func_set_data($this->ci, 'make_list', $make_list);

        func_set_data($this->ci, 'all_model_list', $all_model_list);

        $arr_js = array('/js/member_site/jquery.chained.min.js','/js/member_site/add_car.js','//widget.cloudinary.com/global/all.js','/editor/tiny_mce/tiny_mce.js','/editor/tiny_mce/tiny_mce_load.js');
        $arr_css = array('/css/member_site/global.css');

        $this->_load_member_view('member_site/product/add_car.php', $arr_js, $arr_css);
        
    }

    function addcar_exec(){

        $this->ci->load->helper('url');

        if(isset($_POST['create_mode']) && $_POST['create_mode']=="add_new"){
            
            $insert_id = $this->ci->bbs->insert_draft();
            echo $insert_id;

        }else{

            if(isset($_POST['idx']) && $_POST['idx']!=''){
                $idx = $this->ci->input->get_post('idx');
            }else{
                $idx = $this->ci->bbs->insert_draft();
            }
            
            $bbs_detail = $this->ci->bbs->select_detail_for_seller($idx);
            $inData = array();
            $inData = func_get_post_array($this->ci->input->post('values'));
            $inData['idx'] = $idx;
            $inData['menu_code'] = 'product';       
            $inData['category'] = 'offer';
            $inData['draft']  = 'N';

            /***SET DEFAULT VISIBLE TO YES***/
            if(!isset($inData['publish'])){
                $inData['publish'] = 'Y';
            }
            if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

                $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

                unset($inData['zipcode1']);

                unset($inData['zipcode2']);
            }

            if (!isset($inData['notice_yn'])) {    
                $inData['notice_yn'] = 'N';
            }

            if (!isset($inData['updated_dt']) || ( $inData['updated_dt'] == '')) {    
                $inData['updated_dt'] = func_get_date_time();
            }

            if(empty($bbs_detail[0]->car_stock_no)){
                $inData['car_stock_no'] = $this->ci->bbs->get_max_car_stock_no() +1;
            }

            $inData['writer_ip'] = $this->ci->input->ip_address();

            if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

                $inData['publish_start'] = $inData['publish_end'] . ' 00:00:00';

                $inData['publish_end'] = $inData['publish_end'] . ' 23:59:59';
            }

            if(isset($_POST['normal_discount']) && $_POST['normal_discount']=="normal"){
                $inData['old_fob_cost'] = 0;
            }

            if($this->ci->bbs->update($inData)){

                $data = array();
                $data['make_name'] = $inData['car_make'];
                $insert_id = $this->ci->bbs->car_make_insert($data);

                $data_model = array();
                $data_model['make_id'] = $insert_id;
                $data_model['model_name'] = $inData['car_model'];
                $this->ci->bbs->car_model_insert($data_model);

                //**************INSERT LOG ACTIVITY******************//
                 $chassis_no = $inData['car_chassis_no'];
                 
                $log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no.' ) has been inserted by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'car',
                    'created_dt'=>func_get_date_time()
                );
                    
                $this->ci->mo_activity_log->insert($log_data);

                $this->ci->session->set_flashdata('msg_data', 'Post Car Successful!');
                $this->ci->session->set_flashdata('msg_class', 'message_success');
                redirect('/?c=admin&m=list_car', 'refresh');
            }else{

                $this->ci->session->set_flashdata('msg_data', 'Post Car Failed!');
                $this->ci->session->set_flashdata('msg_class', 'message_fail');
                redirect('/?c=admin&m=list_car', 'refresh');

            }
            
        }
    }

	function modify_car(){

        $protocol   = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
        $host       = $_SERVER['HTTP_HOST'];
        $params     = $_SERVER['QUERY_STRING'];
        $currentUrl = $protocol . '://' . $host . '?' . $params;

            $nav_array = array(
                "Home"=>"/?c=admin",
                "Dashboard"=>"/?c=admin",
                "My Car List"=>"/?c=admin&m=list_car",
                "Modify Car"=>$currentUrl
            );

        $idx = $this->ci->input->get_post('idx');
        $bbs_detail_list = $this->ci->bbs->select_detail_for_seller($idx);
        
         if($_SESSION['ADMIN']['session_grade_no'] ==11){
            $car_owner = $this->ci->user_member->iscar_owner($idx);
            if($car_owner[0]->num_row == 0){

                redirect('/?c=admin', 'refresh');
            }

        }

        $business_type = $_SESSION['ADMIN']['business_type'];
        $access_type = true;
        if($business_type=='buyer'){
            $access_type = false;
        }
        func_set_data($this->ci, 'access_type', $access_type);

        $where_model['make_name'] = $bbs_detail_list[0]->car_make;
        $bodytype_list = $this->ci->bbs->get_bodytype_list();
        $country_list = $this->ci->bbs->admGetCountry_list();
        $member_logged = $this->ci->user_member->select_member_login();
        $color_list = $this->ci->bbs->get_color_options();
        $make_list = $this->ci->bbs->admGetMake_table_seller();
        $model_list = $this->ci->bbs->getModel_table($where_model);

        $all_model_list = $this->ci->bbs->getModel_table();
        $navigation_bar = func_get_nav($nav_array);

        func_set_data($this->ci, 'navigation_bar', $navigation_bar);
        func_set_data($this->ci, 'bodytype_list', $bodytype_list);
        func_set_data($this->ci, 'member_logged', $member_logged);
        func_set_data($this->ci, 'country_list', $country_list);
        func_set_data($this->ci, 'color_list', $color_list);
        func_set_data($this->ci, 'make_list', $make_list);
        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);
        func_set_data($this->ci, 'model_list', $model_list);
        func_set_data($this->ci, 'all_model_list', $all_model_list);

        $arr_js = array('/js/member_site/jquery.chained.min.js','/js/member_site/add_car.js','/js/admin/bootstrap.min.js','/js/colorbox/jquery.colorbox-min.js','//widget.cloudinary.com/global/all.js');
        $arr_css = array('/css/member_site/car_list.css','/css/member_site/bootstrap.min.css','/js/colorbox/colorbox.css');

        $this->_load_member_view('member_site/product/modify_car.php', $arr_js, $arr_css);
		
    }
   
    function modify_car_exec(){

        /*** GET PARAMETERS ***/
        $idx = $this->ci->input->get_post('idx');
        $create_mode = $this->ci->input->get_post('create_mode');

         /*** Validate ChassisNo ***/
        $chassis_no = $_POST['values']['car_chassis_no'];
        $vali = mysql_query("SELECT idx FROM iw_bbs WHERE car_chassis_no = '{$chassis_no}' AND idx != '{$idx}' ");
        $chassis_check = mysql_num_rows($vali);

        if ($chassis_check > 0) {
            echo "<script language='javascript'>";
            echo "confirm('Please change ChassisNo. There is same ChassisNo.'); ";
            echo "window.location.href = '/?c=admin&m=modify_car&mcd=product&category=offer&idx={$idx}';";
            echo "</script>";
        }
        else{
            $bbs_detail = $this->ci->bbs->select_detail_for_seller($idx);

            $inData = array();
            $inData = func_get_post_array($this->ci->input->post('values'));
            $inData['idx'] = $idx;
            $inData['menu_code'] = 'product';       
            $inData['category'] = 'offer';
            $inData['draft']  = 'N';

            /***SET DEFAULT VISIBLE TO YES***/
            if(!isset($inData['publish'])){
                $inData['publish'] = 'Y';
            }
            if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

                $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

                unset($inData['zipcode1']);

                unset($inData['zipcode2']);
            }

            if (!isset($inData['notice_yn'])) {        //공지여부
                $inData['notice_yn'] = 'N';
            }

            if (!isset($inData['updated_dt']) || ( $inData['updated_dt'] == '')) {         //작성일
                $inData['updated_dt'] = func_get_date_time();
            }

            if(empty($bbs_detail[0]->car_stock_no)){
                $inData['car_stock_no'] = $this->ci->bbs->get_max_car_stock_no() +1;
            }

            $inData['writer_ip'] = $this->ci->input->ip_address();

            if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

                $inData['publish_start'] = $inData['publish_end'] . ' 00:00:00';

                $inData['publish_end'] = $inData['publish_end'] . ' 23:59:59';
            }

            if(isset($_POST['normal_discount']) && $_POST['normal_discount']=="normal"){
                $inData['old_fob_cost'] = 0;
            }

            /*** UPDATE CAR ***/
            if(isset($_POST['values']['create_mode']))
            {
                $create_mode=$_POST['values']['create_mode'];
                unset($inData['create_mode']);
            }
            
            if($this->ci->bbs->update($inData)){

                $data = array();
                $data['make_name'] = $inData['car_make'];
                $insert_id = $this->ci->bbs->car_make_insert($data);

                $data_model = array();
                $data_model['make_id'] = $insert_id;
                $data_model['model_name'] = $inData['car_model'];
                $this->ci->bbs->car_model_insert($data_model);

                // SEND GCM AND UPDATE SQLITE FOR ANDROID
                $icon_status = $inData['icon_status'];
                $inData['icon_status']=$inData['icon_new_yn'];
                $inData['car_owner']=$bbs_detail[0]->car_owner;
                if(isset($create_mode) && $create_mode=='new')
                {
                    //$this->pushNewAndUpdateNotification($inData,0);
                    //$this->insertSQLite($inData);

                    //**************INSERT LOG ACTIVITY******************//
                    $log_data = array(
                        'author'=>$_SESSION['ADMIN']['member_no'],
                        'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no.' ) has been inserted by '.$_SESSION['ADMIN']['member_id'],
                        'log_type'=>'car',
                        'created_dt'=>func_get_date_time()
                    );
                    
                    $this->ci->mo_activity_log->insert($log_data);
                    $inData['icon_status'] = $icon_status;
                    //$this->ci->mo_parse->insert($inData);
                }
                else
                {
                    //$this->pushNewAndUpdateNotification($inData,1);
                    //$this->updateSQLite($inData);
                    //**************INSERT LOG ACTIVITY******************//
                    $log_data = array(
                        'author'=>$_SESSION['ADMIN']['member_no'],
                        'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no.' ) has been updated by '.$_SESSION['ADMIN']['member_id'],
                        'log_type'=>'car',
                        'created_dt'=>func_get_date_time()
                    );
                   
                    $this->ci->mo_activity_log->insert($log_data);
                    /*** SAVE TO PARSE.com ****/
                    $inData['icon_status'] = $icon_status;
                    // $saved_car = $this->ci->mo_parse->select_by_idx($idx);
                    // if(count($saved_car)>0){
                    //     $this->ci->mo_parse->update($idx, $inData);
                    // }else{
                    //     $this->ci->mo_parse->insert($inData);
                    // }
                    /*** END SAVE TO PARSE.com ****/
                }

                $this->ci->session->set_flashdata('msg_data', 'Success! The car has been modified.');
                $this->ci->session->set_flashdata('msg_class', 'message_success');
                redirect('/?c=admin&m=list_car', 'refresh');
            }else{

                $this->ci->session->set_flashdata('msg_data', 'Modification Failed!');
                $this->ci->session->set_flashdata('msg_class', 'message_fail');
                redirect('/?c=admin&m=list_car', 'refresh');

            }

        }

    }

    function detail_car(){
        $protocol   = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
        $host       = $_SERVER['HTTP_HOST'];
        $params     = $_SERVER['QUERY_STRING'];
        $currentUrl = $protocol . '://' . $host . '?' . $params;

            $nav_array = array(
                "Home"=>"/?c=admin",
                "Dashboard"=>"/?c=admin&m=list_car",
                "Car Detail"=>$currentUrl
            );

        $idx = $this->ci->input->get_post('idx');
        $bbs_detail_list = $this->ci->bbs->select_detail_for_seller($idx);

         if($_SESSION['ADMIN']['session_grade_no'] ==11){
            $car_owner = $this->ci->user_member->iscar_owner($idx);
            if($car_owner[0]->num_row == 0){

                redirect('/?c=admin', 'refresh');
            }

        }

        $business_type = $_SESSION['ADMIN']['business_type'];
        $access_type = true;
        if($business_type=='buyer'){
            $access_type = false;
        }
        func_set_data($this->ci, 'access_type', $access_type);

        $bbs_cloud_list = $this->ci->mo_file->select_bbs_cloud($idx);

        $where_model['make_name'] = $bbs_detail_list[0]->car_make;
        $bodytype_list = $this->ci->bbs->get_bodytype_list();
        $country_list = $this->ci->bbs->admGetCountry_list();
        $member_logged = $this->ci->user_member->select_member_login();
        $color_list = $this->ci->bbs->get_color_options();
        $make_list = $this->ci->bbs->admGetMake_table_seller();
        $model_list = $this->ci->bbs->getModel_table($where_model);

        $bbs_attach_files = $this->ci->bbs->select_attach_files($idx);
        func_set_data($this->ci, 'bbs_attach_files', $bbs_attach_files->result());

        $all_model_list = $this->ci->bbs->getModel_table();
        $navigation_bar = func_get_nav($nav_array);

        func_set_data($this->ci, 'bbs_cloud_list', $bbs_cloud_list->result());

        func_set_data($this->ci, 'navigation_bar', $navigation_bar);
        func_set_data($this->ci, 'bodytype_list', $bodytype_list);
        func_set_data($this->ci, 'member_logged', $member_logged);
        func_set_data($this->ci, 'country_list', $country_list);
        func_set_data($this->ci, 'color_list', $color_list);
        func_set_data($this->ci, 'make_list', $make_list);
        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);
        func_set_data($this->ci, 'model_list', $model_list);
        func_set_data($this->ci, 'all_model_list', $all_model_list);

        $arr_js = array('/js/member_site/lightbox.js','/js/member_site/lightbox.min.js');
        $arr_css = array('/css/member_site/global.css','/css/member_site/lightbox.css');

        $this->_load_member_view('member_site/product/detail_car.php', $arr_js, $arr_css);
    }

     function unsetGetUrl($unset_p=array()){
        $get = $_GET;
        if(count($unset_p)>0){
            foreach($unset_p as $value){
                if(isset($get[$value])) unset($get[$value]);
            }
        }
        $updatedUrl_arr = array();
        $updatedUrl = http_build_query($get);
        return $updatedUrl;
    }

	function list_car(){
        ob_clean();
        $this->ci->load->helper('url');
        $this->ci->load->library('pagination'); 
        $where=array();
        $searched = array();
        $quick_count = array();
        $quick_count_pub = array();
        $quick_count_total= 0;
        $customer_no= $_SESSION['ADMIN']['member_no'];
        /*** GET SEARCH FIELDS ***/
        $searched['sch_word'] = $this->ci->input->get_post("sch_word");
        $searched['carmake'] = $this->ci->input->get_post("carmake");
        $searched['carmodel'] = $this->ci->input->get_post("carmodel");
        $searched['year'] = $this->ci->input->get_post("year");

        /*** GET ORDER PARAM ***/
        $order_by = $this->ci->input->get_post("order_by");
        $order_d = $this->ci->input->get_post("order_d");
        $order='icon_status ASC, created_dt DESC';
        if(!empty($order_by)){
            $order = $order_by." ".$order_d;
        }
        // search condition
        $grade_no_session = $this->ci->grade_no;
        $member_no_session = $this->ci->member_no;
            if($grade_no_session == 11){
                $where['car_owner'] = $member_no_session;
                if(isset($where['car_keyword'])){
                unset($where['car_keyword']);
            }

        }

        $carmake = $this->ci->input->get_post('carmake');
        if ($carmake !='') {
            $where['car_make'] = $carmake;
        }
        $carmodel = $this->ci->input->get_post('carmodel');
          if ($carmodel  !='') {
            $where['car_model'] = $carmodel;
        }
          $year = $this->ci->input->get_post('year');
        if ($year !='') {
            $where['car_model_year'] = $year;
        }
        $sch_icon_status = $this->ci->input->get_post('sch_icon_status');
        if ($sch_icon_status) {
            $where['icon_status'] = $sch_icon_status;
        }

        $sch_word = $this->ci->input->get_post('sch_word');
        if ($sch_word !='') {
            $where['car_keyword'] = $sch_word;  
           
        }
     
        $where_pub['car_owner'] = $member_no_session;
        $count_publish = $this->ci->bbs->count_by_publish($where_pub);
        foreach($count_publish as $row){
            $quick_count_pub['not_published'] = $row->num_rows;
        }

        $count_status = $this->ci->bbs->count_by_status($where);
        foreach($count_status as $row){
            $status = $row->icon_status;
            $quick_count[$status] = $row->num_rows;
            $quick_count_total += $row->num_rows;
        }

        $quick_count['all'] = $quick_count_total;


        $sch_publish = $this->ci->input->get_post('sch_publish');
        if ($sch_publish=='not_published') {
            $where['publish'] = 'N';
        }
        
        // end search
        
        $limit_s = $this->ci->input->get_post("offset");
        if(!$limit_s) $limit_s =0;
        $order = "icon_status ASC, created_dt DESC";
        $get_url = $this->unsetGetUrl(array("offset"));
        $result_total_rows = $this->ci->bbs->adm_total_select($where);

        $total_rows = $result_total_rows['0']->num_rows;
        $cur_page = $this->ci->input->get_post("offset");
        if ($cur_page == NULL || $cur_page == '')
        $cur_page = 1;

        /*****START PAGINATION*****/
        $config['base_url'] = site_url()."/?".$get_url;
        $config['uri_segment'] = 3;
        $config['num_links'] = 4;
        $config['total_rows'] = $total_rows;
        $config['per_page'] =10;
        $config['prev_link']='PREVIOUS';
        $config['next_link']='NEXT';
        $config['last_tag_open'] = '<div style="display:none;">';
        $config['last_tag_close'] = '</div>';
        $config['query_string_segment'] = "offset";
        $this->ci->pagination->initialize($config);
        $pagination = $this->ci->pagination->create_links();
        $cuurent=$config['use_page_numbers'] = TRUE;
        /*****END PAGINATION*****/
        $cur_page =$limit_s/10;
        $total_page=$total_rows;
        /*** GET QUICK LINK ***/
        $append_url = '';
        foreach($searched as $key=>$value){
            if(!empty($value)){
                $append_url .= "&".$key."=".$value."&";
            }
        }
        $append_url = rtrim($append_url, '&');
        $quick_link['all'] = "/?c=admin&m=list_car".$append_url;
        $quick_link['reserved'] = "/?c=admin&m=list_car&sch_icon_status=reserved".$append_url;
        $quick_link['soldout'] = "/?c=admin&m=list_car&sch_icon_status=soldout".$append_url;
        $quick_link['not_published'] = "/?c=admin&m=list_car&sch_publish=not_published".$append_url;

        /*** END GET QUICK LINK ***/

        /***  START CHECK SAVE  ***/
        if (isset($_POST['btn_save_discount']) && $_POST['btn_save_discount']!='')   {
            $dataDiscount = array();
            // var_dump($_POST); die();
            /***  SELECT FOB BEFORE MODIFY  ***/
            $idx = $this->ci->input->post('idx');
            $current_fob = $this->ci->bbs->select_detail_for_seller($idx);
            $car_fob_cost = $current_fob[0]->car_fob_cost;
            $old_fob_cost = $current_fob[0]->old_fob_cost;
            $chassis_no   = $current_fob[0]->car_chassis_no; 

            /***  SET DEFAULT OLD FOB COST***/
            $dataDiscount['old_fob_cost'] = 0;
            $dataDiscount['idx']          = $this->ci->input->get_post('idx');
            $dataDiscount['car_fob_cost'] = $this->ci->input->get_post('car_fob_cost');
            /*** SELECT RADIO VALUE & CHECK DISCOUNT TYPE ***/
            $normal_discount      = $this->ci->input->get_post('normal_discount');

            if($dataDiscount['idx'] !=''){

                if($normal_discount=="discount"){
                    $this->ci->session->set_flashdata('msg_data', 'Success! The car price has been discounted.');
                    $dataDiscount['old_fob_cost'] = $this->ci->input->get_post('old_fob_cost');
                }else{
                    $this->ci->session->set_flashdata('msg_data', 'Success! The car price has been modified.');
                }
                if($normal_discount=="normal" || $dataDiscount['old_fob_cost']>$dataDiscount['car_fob_cost']){
                    $this->ci->bbs->update($dataDiscount);

                    $this->ci->session->set_flashdata('msg_class', 'message_success');

                    $fob_cost = '';
                    if($car_fob_cost != $dataDiscount['car_fob_cost']){
                        $fob_cost = ' car_fob_cost ('.$car_fob_cost.' => '.$dataDiscount['car_fob_cost'].')';
                    }

                    /***  SAVE LOG  ACTIVITY  ***/
                    if($normal_discount=="normal"){
                        if($car_fob_cost == $dataDiscount['car_fob_cost']){
                            $fob_cost = ' old_fob_cost equal zero and use car fob cost';
                        }

                        $log_data = array(
                            'author'=>$_SESSION['ADMIN']['member_no'],
                            'message'=>'Car idx: '.$dataDiscount['idx'].' (chassis_no: '.$chassis_no.') has been modified '.$fob_cost.' by '.$_SESSION['ADMIN']['member_id'],
                            'log_type'=>'car',
                            'created_dt'=>func_get_date_time()
                        );

                    }else{

                        if($old_fob_cost != $dataDiscount['old_fob_cost']){
                            if($car_fob_cost != $dataDiscount['car_fob_cost']){
                                $fob_cost .=' and ';
                            }

                            $fob_cost .= ' old_fob_cost ('.$old_fob_cost.' => '.$dataDiscount['old_fob_cost'].')';
                        }

                        $log_data = array(
                            'author'=>$_SESSION['ADMIN']['member_no'],
                            'message'=>'Car idx: '.$dataDiscount['idx'].' (chassis_no: '.$chassis_no.') has been modified '.$fob_cost.' by '.$_SESSION['ADMIN']['member_id'],
                            'log_type'=>'car',
                            'created_dt'=>func_get_date_time()
                        );
                        
                    }
                    if($old_fob_cost != $dataDiscount['old_fob_cost'] || $car_fob_cost != $dataDiscount['car_fob_cost'])
                    {
                        $this->ci->mo_activity_log->insert($log_data);
                    }

                }
                else
                {

                    $this->ci->session->set_flashdata('msg_data', 'Discount Failed!');
                    $this->ci->session->set_flashdata('msg_class', 'message_fail');
                }

            }
            redirect('/?c=admin');

        }

        /***  START CHECK SAVE  ***/

        $bbs_list_sale = $this->ci->bbs->adm_seller_select($customer_no,$where,$limit_s,$config['per_page'],$order);
        $select_make_all = $this->ci->bbs->carmaketodropdownsearch();
        $select_model_all = $this->ci->bbs->carmodeltodropdownsearch();

        $model_list = array();
        $where_model['car_make']=$carmake;
        $model_list = $this->ci->bbs->getModel($where_model, true);

        $business_type = $_SESSION['ADMIN']['business_type'];
        $access_type = true;
        if($business_type=='buyer'){
            $access_type = false;
        }
        func_set_data($this->ci, 'access_type', $access_type);

        /*** NAVIGATION BAR ***/
        $nav_array = array(
            "Home"=>"/",
            "Dashboard"=>"/?c=admin",
            "My Car List"=>"/?c=admin",
        );
        $navigation_bar = func_get_nav($nav_array);
        func_set_data($this->ci, 'navigation_bar', $navigation_bar);

        func_set_data($this->ci, 'full_url', $this->current_url());
        func_set_data($this->ci, 'quick_count', $quick_count);
        func_set_data($this->ci, 'quick_count_pub', $quick_count_pub);
        func_set_data($this->ci, 'quick_link', $quick_link);
        func_set_data($this->ci, 'searched', $searched);
        func_set_data($this->ci, 'bbs_list_sale', $bbs_list_sale);
        func_set_data($this->ci, 'select_make_all',$select_make_all);

        func_set_data($this->ci, 'select_model_all',$select_model_all);

        func_set_data($this->ci, 'model_list',$model_list);
        func_set_data($this->ci, 'pagination', $pagination);
        func_set_data($this->ci, 'total_rows', $total_rows);
        func_set_data($this->ci, 'cur_page', $cur_page);
        func_set_data($this->ci, 'total_page', $total_page);
        // func_set_data($this->ci, 'count', $count);

        $msg_data  = $this->ci->session->flashdata('msg_data');
        $msg_class = $this->ci->session->flashdata('msg_class');
        func_set_data($this->ci, 'msg_data', $msg_data);
        func_set_data($this->ci, 'msg_class', $msg_class);

      	$arr_js = array('/js/admin/jquery.min.js','/js/member_site/jquery.chained.min.js','/js/member_site/car_list.js','/js/admin/bootstrap.min.js','/js/colorbox/jquery.colorbox-min.js');
        $arr_css = array('/css/member_site/car_list.css','/css/member_site/bootstrap.min.css','/js/colorbox/colorbox.css');
        $this->_load_member_view('member_site/product/list_car.php', $arr_js, $arr_css);
		
    }
    /*** DISCOUNT ***/
    // function discount_car(){
        // ob_clean();
        // /***  CHECK SAVE  ***/
        // if (isset($_POST['btn_save_discount']))   {

        //     $dataDiscount = array();
        //     /***  SELECT FOB BEFORE MODIFY  ***/
        //     $idx = $this->ci->input->post('idx');
        //     $current_fob = $this->ci->bbs->select_detail_for_seller($idx);
        //     $car_fob_cost = $current_fob[0]->car_fob_cost;
        //     $old_fob_cost = $current_fob[0]->old_fob_cost;
        //     $chassis_no   = $current_fob[0]->car_chassis_no; 
        //     /***  SET DEFAULT OLD FOB COST***/
        //     $dataDiscount['old_fob_cost'] = 0;
        //     $dataDiscount['idx']          = $this->ci->input->get_post('idx');
        //     $dataDiscount['car_fob_cost'] = $this->ci->input->get_post('car_fob_cost');
        //     /*** SELECT RADIO VALUE & CHECK DISCOUNT TYPE ***/
        //     $normal_discount      = $this->ci->input->get_post('normal_discount');

        //     if($dataDiscount['idx'] !=''){

        //         if($normal_discount=="discount"){
        //             $this->ci->session->set_flashdata('msg_data', 'Success! The car price has been discounted.');
        //             $dataDiscount['old_fob_cost'] = $this->ci->input->get_post('old_fob_cost');
        //         }else{
        //             $this->ci->session->set_flashdata('msg_data', 'Success! The car price has been modified.');
        //         }
        //         if($normal_discount=="normal" || $dataDiscount['old_fob_cost']>$dataDiscount['car_fob_cost']){
        //             $this->ci->bbs->update($dataDiscount);

        //             $this->ci->session->set_flashdata('msg_class', 'message_success');

        //             $fob_cost = '';
        //             if($car_fob_cost != $dataDiscount['car_fob_cost']){
        //                 $fob_cost = ' car_fob_cost ('.$car_fob_cost.' => '.$dataDiscount['car_fob_cost'].')';
        //             }

        //             /***  SAVE LOG  ACTIVITY  ***/
        //             if($normal_discount=="normal"){
        //                 if($car_fob_cost == $dataDiscount['car_fob_cost']){
        //                     $fob_cost = ' old_fob_cost equal zero and use car fob cost';
        //                 }

        //                 $log_data = array(
        //                     'author'=>$_SESSION['ADMIN']['member_no'],
        //                     'message'=>'Car idx: '.$dataDiscount['idx'].' (chassis_no: '.$chassis_no.') has been modified '.$fob_cost.' by '.$_SESSION['ADMIN']['member_id'],
        //                     'log_type'=>'car',
        //                     'created_dt'=>func_get_date_time()
        //                 );

        //             }else{

        //                 if($old_fob_cost != $dataDiscount['old_fob_cost']){
        //                     if($car_fob_cost != $dataDiscount['car_fob_cost']){
        //                         $fob_cost .=' and ';
        //                     }

        //                     $fob_cost .= ' old_fob_cost ('.$old_fob_cost.' => '.$dataDiscount['old_fob_cost'].')';
        //                 }

        //                 $log_data = array(
        //                     'author'=>$_SESSION['ADMIN']['member_no'],
        //                     'message'=>'Car idx: '.$dataDiscount['idx'].' (chassis_no: '.$chassis_no.') has been modified '.$fob_cost.' by '.$_SESSION['ADMIN']['member_id'],
        //                     'log_type'=>'car',
        //                     'created_dt'=>func_get_date_time()
        //                 );
                        
        //             }
        //             if($old_fob_cost != $dataDiscount['old_fob_cost'] || $car_fob_cost != $dataDiscount['car_fob_cost'])
        //             {
        //                 $this->ci->mo_activity_log->insert($log_data);
        //             }

        //         }
        //         else
        //         {

        //             $this->ci->session->set_flashdata('msg_data', 'Discount Failed!');
        //             $this->ci->session->set_flashdata('msg_class', 'message_fail');
        //         }

        //     }
        // }

        // $this->ci->load->view('/member_site/product/discount.php', $this->ci->data, false);
    // }

    function buyer_car_list(){
        $this->ci->load->helper('url');
        $this->ci->load->library('pagination'); 
        $where=array();
        $searched = array();
        $quick_count = array();
        $quick_count_pub = array();
        $quick_count_total= 0;
        $customer_no= $_SESSION['ADMIN']['member_no'];
        /*** GET SEARCH FIELDS ***/
        $searched['sch_word'] = $this->ci->input->get_post("sch_word");
        $searched['carmake'] = $this->ci->input->get_post("carmake");
        $searched['carmodel'] = $this->ci->input->get_post("carmodel");
        $searched['year'] = $this->ci->input->get_post("year");

        /*** GET ORDER PARAM ***/
        $order_by = $this->ci->input->get_post("order_by");
        $order_d = $this->ci->input->get_post("order_d");
        $order='icon_status ASC, created_dt DESC';
        if(!empty($order_by)){
            $order = $order_by." ".$order_d;
        }
        // search condition
        $grade_no_session = $this->ci->grade_no;
        $member_no_session = $this->ci->member_no;
            if($grade_no_session == 11){
                $where['car_owner'] = $member_no_session;
                if(isset($where['car_keyword'])){
                unset($where['car_keyword']);
            }

        }

        $carmake = $this->ci->input->get_post('carmake');
        if ($carmake !='') {
            $where['car_make'] = $carmake;
        }
        $carmodel = $this->ci->input->get_post('carmodel');
          if ($carmodel  !='') {
            $where['car_model'] = $carmodel;
        }
          $year = $this->ci->input->get_post('year');
        if ($year !='') {
            $where['car_model_year'] = $year;
        }
        $sch_icon_status = $this->ci->input->get_post('sch_icon_status');
        if ($sch_icon_status) {
            $where['icon_status'] = $sch_icon_status;
        }

        $sch_word = $this->ci->input->get_post('sch_word');
        if ($sch_word !='') {
            $where['car_keyword'] ='%'.$sch_word.'%';  
           
        }
     
        $where_pub['customer_no'] = $member_no_session;
        $count_publish = $this->ci->bbs->count_by_publish($where_pub);
        foreach($count_publish as $row){
            $quick_count_pub['not_published'] = $row->num_rows;
        }

        $count_status = $this->ci->bbs->count_by_status($where);
        foreach($count_status as $row){
            $status = $row->icon_status;
            $quick_count[$status] = $row->num_rows;
            $quick_count_total += $row->num_rows;
        }

        $quick_count['all'] = $quick_count_total;


        $sch_publish = $this->ci->input->get_post('sch_publish');
        if ($sch_publish=='not_published') {
            $where['publish'] = 'N';
        }
        
        // end search
        
        $limit_s = $this->ci->input->get_post("offset");
        if(!$limit_s) $limit_s =0;
        $order = "icon_status ASC, created_dt DESC";
        $get_url = $this->unsetGetUrl(array("offset"));
        $result_total_rows = $this->ci->bbs->adm_total_select($where);

        $total_rows = $result_total_rows['0']->num_rows;
        $cur_page = $this->ci->input->get_post("offset");
        if ($cur_page == NULL || $cur_page == '')
        $cur_page = 1;

        /*****START PAGINATION*****/
        $config['base_url'] = site_url()."/?".$get_url;
        $config['uri_segment'] = 3;
        $config['num_links'] = 4;
        $config['total_rows'] = $total_rows;
        $config['per_page'] =10;
        $config['prev_link']='PREVIOUS';
        $config['next_link']='NEXT';
        $config['last_tag_open'] = '<div style="display:none;">';
        $config['last_tag_close'] = '</div>';
        $config['query_string_segment'] = "offset";
        $this->ci->pagination->initialize($config);
        $pagination = $this->ci->pagination->create_links();
        $cuurent=$config['use_page_numbers'] = TRUE;
        /*****END PAGINATION*****/
        $cur_page =$limit_s/10;
        $total_page=$total_rows;
        /*** GET QUICK LINK ***/
        $append_url = '';
        foreach($searched as $key=>$value){
            if(!empty($value)){
                $append_url .= "&".$key."=".$value."&";
            }
        }
        $append_url = rtrim($append_url, '&');
        $quick_link['all'] = "/?c=admin&m=list_car".$append_url;
        $quick_link['reserved'] = "/?c=admin&m=list_car&sch_icon_status=reserved".$append_url;
        $quick_link['soldout'] = "/?c=admin&m=list_car&sch_icon_status=soldout".$append_url;
        $quick_link['not_published'] = "/?c=admin&m=list_car&sch_publish=not_published".$append_url;

        /*** END GET QUICK LINK ***/
        $bbs_list_sale = $this->ci->bbs->adm_seller_select($customer_no,$where,$limit_s,$config['per_page'],$order);
        $select_make_all = $this->ci->bbs->carmaketodropdownsearch();
        $select_model_all = $this->ci->bbs->carmodeltodropdownsearch();

        $model_list = array();
        $where_model['car_make']=$carmake;
        $model_list = $this->ci->bbs->getModel($where_model, true);

        $business_type = $_SESSION['ADMIN']['business_type'];
        $access_type = true;

        if($business_type=='buyer'){
            $access_type = false;
        }
        func_set_data($this->ci, 'access_type', $access_type);

        /*** NAVIGATION BAR ***/
        $nav_array = array(
            "Home"=>"/",
            "Dashboard"=>"/?c=admin",
            "My Car List"=>"/?c=admin",
        );
        $navigation_bar = func_get_nav($nav_array);
        func_set_data($this->ci, 'navigation_bar', $navigation_bar);

        func_set_data($this->ci, 'full_url', $this->current_url());
        func_set_data($this->ci, 'quick_count', $quick_count);
        func_set_data($this->ci, 'quick_count_pub', $quick_count_pub);
        func_set_data($this->ci, 'quick_link', $quick_link);
        func_set_data($this->ci, 'searched', $searched);
        func_set_data($this->ci, 'bbs_list_sale', $bbs_list_sale);
        func_set_data($this->ci, 'select_make_all',$select_make_all);

        func_set_data($this->ci, 'select_model_all',$select_model_all);

        func_set_data($this->ci, 'model_list',$model_list);
        func_set_data($this->ci, 'pagination', $pagination);
        func_set_data($this->ci, 'total_rows', $total_rows);
        func_set_data($this->ci, 'cur_page', $cur_page);
        func_set_data($this->ci, 'total_page', $total_page);
        // func_set_data($this->ci, 'count', $count);

        $arr_js = array('/js/admin/jquery.min.js','/js/member_site/jquery.chained.min.js','/js/member_site/car_list.js','/js/admin/bootstrap.min.js','/js/colorbox/jquery.colorbox-min.js');
        $arr_css = array('/css/member_site/car_list.css','/css/member_site/bootstrap.min.css','/js/colorbox/colorbox.css');
        $this->_load_member_view('member_site/product/list_car.php', $arr_js, $arr_css);
        
    }

    function change_password(){
        $arr_css = array('/css/member_site/changepass.css');
        $arr_js = array();
        /*** NAVIGATION BAR ***/
        $nav_array = array(
            "Home"=>"/",
            "Dashboard"=>"/?c=admin",
            "Change Password"=>"#",
        );
        $navigation_bar = func_get_nav($nav_array);
        func_set_data($this->ci, 'navigation_bar', $navigation_bar);
        /*** END NAVIGATION BAR ***/
        /*** BUSINESS TYPE ***/
        $business_type = $_SESSION['ADMIN']['business_type'];
        $access_type = true;
        if($business_type=='buyer'){
            $access_type = false;
        }
        func_set_data($this->ci, 'access_type', $access_type);
        /*** END BUSINESS TYPE ***/
        $this->_load_member_view('member_site/product/change_pass.php',$arr_js, $arr_css);
         // $this->ci->load->view('member_site/product/change_pass', $this->ci->data, false);
    }

    // rado start mobile change password

    function mobile_change_password(){
         $arr_css = array('/css/member_mobile_site/mobile_change_password.css');
         $arr_js = array();
         $this->_load_member_mobile_view('member_mobile_site/product/mobile_change_password.php',$arr_js, $arr_css);
         // $this->ci->load->view('member_site/product/change_pass', $this->ci->data, false);
    }

    // rado end mobile change password

      function view_document_buyer(){
         $member_log=$_SESSION['ADMIN']['member_no'];
         $idx = $this->ci->input->get_post('idx');
         $select_bbs_cloud_list_buyer=$this->ci->mo_file->select_bbs_cloud($idx);
         $bbs_info_buyer_detail=$this->ci->bbs->select_detail_for_buyer($idx, $this->mcd,$member_log);
         $bbs_attach_files = $this->ci->bbs->select_attach_files($idx);
         $bbs_detail_list_get_owner=$this->ci->bbs->select_owner_list();
         // view images
         $getDetailImage = $this->ci->bbs->getCarDetailImage($idx);
         func_set_data($this->ci, 'getDetailImage',$getDetailImage);
         //end view images
         func_set_data($this->ci, 'bbs_detail_list_get_owner', $bbs_detail_list_get_owner);
         func_set_data($this->ci, "bbs_info_buyer_detail",$bbs_info_buyer_detail);
         func_set_data($this->ci, "select_bbs_cloud_list_buyer",$select_bbs_cloud_list_buyer->result());
         func_set_data($this->ci, 'bbs_attach_files', $bbs_attach_files->result());
         $arr_css = array('/css/member_site/view_list.css','/css/member_site/lightbox.css');
          $arr_js = array('/js/member_site/lightbox.js','/js/member_site/lightbox.min.js');
         $this->_load_member_view('member_site/product/view_document_buyer.php',$arr_js,$arr_css);
    }
	function download_document(){
        $getfile = array();
        $i=0;
        $destination= time().'.zip';
        $filelimit = 255;
        if(isset($_POST['download_image'])){
            // if ($_SERVER['SERVER_NAME'] == 'motorbb.dev') {
            //     $host="ratha-it";
            // }
            // else{
            //     $host="softbloom";
            // }
            // $file_folder ='https://res.cloudinary.com/'.$host.'/image/upload/';

            $file_folder ='/uploads/cars/';
        
            ini_set('max_execution_time', 10000);
            ini_set("memory_limit", "6400M");
            $zip = new ZipArchive();    
            $zip->open($destination, ZIPARCHIVE::CREATE);
            $getfile = $_POST["files"];
            $countfile = count($_POST['files']);
            $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';

            foreach($getfile as $file){

                $full_url = $protocol.$_SERVER['SERVER_NAME'].$file_folder.$file;
        
                if(copy($full_url, "uploads/".basename($full_url))){   
                    if(file_exists("uploads/".basename($full_url))){
                        
                                $zip->addFile("uploads/".basename($full_url));
                        
                    }
                }

                
                
            }
                
            $zip->close();


           if(file_exists($destination)){


                ob_clean();
                ob_end_flush();
                header('Content-type: application/zip');
                header('Content-Disposition: attachment; filename="'.$destination.'"');
                readfile($destination);
                // remove zip file is exists in temp path
                unlink($destination);


                foreach (new DirectoryIterator('uploads/') as $fileInfo) {
                    if(!$fileInfo->isDot()) {
                        unlink($fileInfo->getPathname());

                        $myfile = fopen("uploads/newfile.txt", "w") or die("Unable to open file!");
                        $txt = "Hello World\n";
                        fwrite($myfile, $txt);
                        fclose($myfile);

                    }
                }

                
            }




        }

    }
	

    function adm_premium_bbs(){
        $num_row = 5;
        $offset = 0;
        $where=array();
        $country = $this->ci->input->get('country');
        if(!empty($country)){
            $where['iw_bbs.country'] = $country;
        }
        
        $total_num_rows = $this->ci->bbs->get_total_premium_car($where);
        
        $get_adm_premium_bbs = $this->ci->bbs->get_premium_car($where,$num_row,$offset);
        $current_num_rows = count($get_adm_premium_bbs)+$offset;
        func_set_data($this->ci, 'country',$country);
        func_set_data($this->ci, 'total_num_rows',$total_num_rows);
        func_set_data($this->ci, 'current_num_rows',$current_num_rows);
        func_set_data($this->ci, 'get_adm_premium_bbs',$get_adm_premium_bbs);
        $this->_load_view_layout('admin/adm_premium_bbs',$this->ci->data, false);
    }
    
    function adm_json_premium_bbs(){
        $json = array();
        $num_row=5;
        $where=array();
        $offset = $this->ci->input->get_post('offset');
        $country = $this->ci->input->get('country');
        
        if(empty($offset)){
            $offset = 0;
        }
        if(!empty($country)){
            $where['iw_bbs.country'] = $country;
        }
        $total_num_rows = $this->ci->bbs->get_total_premium_car($where);
        $premium_bbs = $this->ci->bbs->get_premium_car($where,$num_row,$offset);
        foreach($premium_bbs as $key=>$value){
            $car_name = '';
            if(!empty($value->car_make)){
                $car_name.= $value->car_make;
            }
            if(!empty($value->car_model)){
                $car_name.= " ".$value->car_model;
            }
            if(!empty($value->car_model_year)){
                $car_name.= ', '.$value->car_model_year;
            }
            $premium_bbs[$key]->car_name = $car_name;
        }
        $json['premium_bbs'] = $premium_bbs;
        $json['total_num_rows'] = $total_num_rows;
        $json['current_num_rows'] = count($premium_bbs)+$offset;
        //$premium_bbs->total_num_rows = $total_num_rows;
        echo json_encode($json);
    }

    function adm_premium_view(){
         $idx = $this->ci->input->get('idx');
         $member_view_list = $this->ci->bbs->premium_select_detail($idx);
         func_set_data($this->ci, 'member_view_list',$member_view_list);
         $this->_load_view_layout('admin/adm_premium_view');
    }
            
    function adm_add_premium_bbs(){
        $success_msg = ''; 
        $where= array();
        /*** If user add premium car***/
        if($this->ci->input->post('btn_select')){
            $data['bbs_idx'] = $this->ci->input->post('bbs_idx');
            if($this->ci->bbs->insert_premium_car($data)){
                //$this->insertPremiumSQLite($data['bbs_idx']);
                $success_msg = 'You have successfully added a premium car.';
            }
        }
        /*** If user search for a car***/
        if($this->ci->input->post('btn_search')){
            $sch_car_chassis_no = $this->ci->input->get_post('sch_car_chassis_no');
            if(!empty($sch_car_chassis_no)){
                $where['car_chassis_no LIKE'] = '%'.$sch_car_chassis_no.'%';
            }

        }
        /*** Get car list to insert as premium ***/
        $available_cars = $this->ci->bbs->get_available_besides_premium($where);
        /*** Set variable for view ***/
        func_set_data($this->ci, 'success_msg', $success_msg);
        func_set_data($this->ci, 'available_cars', $available_cars);
        /*** Load View ***/
        $this->ci->load->view('admin/adm_add_premium_bbs', $this->ci->data, false);
        
    }
    
    function adm_premium_all_delete(){    
       
        $premium = $this->ci->input->post('idx');

        $this->ci->bbs->adm_premium_delete_adm($premium);
        //$this->removePremiumSQLite($premium);
        echo func_jsAlertReplace('The car has been removed from Premium List.', '/?c=admin&m=adm_premium_bbs');      
    }
        
    function adm_supporter_all_delete(){    
        $id = '';
        $id=explode(";", $this->ci->input->get_post('idx'));
        foreach ($id as $key => $value) {
            $id = $value;
            if ($id != '') {
                $this->ci->bbs->adm_supporter_delete_adm($id);
            }

        }

        echo func_jsAlertReplace('Deletion Complete.', '/?c=admin&m=adm_item_supporter');      
          
    }


     function adm_supporter_all_customer_delete(){    
        $id = '';
        $id = $this->ci->input->get_post('idx');
        foreach ($id as $key => $value) {
            $id = $value;
            if ($id != '') {
                $this->ci->bbs->adm_supporter_delete_adm($id);
            }

        }

        echo func_jsAlertReplace('Deletion Complete.', '/?c=admin&m=adm_item_supporter');      
          
    }
    
    
    function checkoldpassword() {
        $member_id = $this->ci->input->get_post('namemember');
        $oldpassword = func_base64_encode($this->ci->input->get_post('oldpassword'));

        $sql = mysql_query("SELECT member_no FROM iw_member WHERE member_id = '{$member_id}' AND member_pwd = '{$oldpassword}' LIMIT 1 ");


        $check = mysql_num_rows($sql);

        if ($check == 1) {
            echo '1';
        } else {
            echo '0';
        }
    }

    function adm_get_car_to_allocate_deposit(){
        $where = array();
        /*** Do not get selected car again ***/
        $excluded_cars = $this->ci->input->get_post('selected_cars');
        //$where['a.customer_no'] = $_GET['customer_no'];
        if($this->ci->input->get_post('car_actual_price_currency')!=''){
            $where['a.car_actual_price_currency'] = $this->ci->input->get_post('car_actual_price_currency');
        }
        if($this->ci->input->get_post('car_model')!=''){
            $where['a.car_model LIKE'] = "%".$this->ci->input->get_post('car_model')."%";
        }
        if($this->ci->input->get_post('car_chassis_no')!=''){
            $where['a.car_chassis_no LIKE'] = "%".$this->ci->input->get_post('car_chassis_no')."%";
        }
        //car_stock_no, car_model, car_chassis_no, 
        $cars = $this->ci->bbs->getCarToAllocateDeposit($where, $excluded_cars);
        foreach($cars as $car){
            echo '<option value="'.$car->car_stock_no.'">';
            /*** Car Stock No ***/
            echo $car->car_stock_no."/";
            /*** Car Model ***/
            echo $car->car_model."/";
            /*** Car Chassis_no ***/
            echo $car->car_chassis_no."/";
            /*** Total Sell Price ***/
            echo "(Price) ";
            switch ($car->car_actual_price_currency) {  
                case 'USD':
                    echo 'USD';
                    break;
                case 'KRW':
                    echo 'KRW';
                    break;    
                case 'JPY':
                    echo 'JPY';
                    break;
                case 'EUR':
                    echo 'EUR';
                    break;   
                default:
                    echo '';
                    break;
            }
            if(!empty($car->total_sell_price)) { 
                echo " ".$car->total_sell_price;
            }else{
                echo "0";
            }
            /*** Allocate Sum ***/
            echo "/ (Deposit) ";
            switch ($car->currency_type) {
                case 'USD':
                    echo 'USD';
                    break;
                case 'KRW':
                    echo 'KRW';
                    break;    
                case 'JPY':
                    echo 'JPY';
                    break;
                case 'EUR':
                    echo 'EUR';
                    break;   
                default:
                    echo '';
                    break;
            } 
            if(!empty($car->allocate_sum)) {     
                echo " ".$car->allocate_sum;
            }else{
                echo "0";
            }
            echo "</option>";
        }
    }

    function checkmemember() {
        ob_clean();
        $member_id = $this->ci->input->get_post('id');



        $getmember_id = mysql_query("SELECT member_no FROM iw_member WHERE member_id = '{$member_id}'");
        $row_member = mysql_num_rows($getmember_id);

        if ($row_member > 0) {

            echo '1';
        } else {
            echo '0';
        }
    }

    function check_chassis(){
        $car_chassis_no = $this->ci->input->get_post('chass_no');
        $getchassis = mysql_query("SELECT idx FROM iw_bbs WHERE car_chassis_no = '{$car_chassis_no}'");
        $row = mysql_num_rows($getchassis);

        if ($row > 0) {

            echo '1';
        } else {
            echo '0';
        }
    }

    
    function checkemail() {

        $email = $this->ci->input->get_post('id');

        $getemail = mysql_query("SELECT email FROM iw_member WHERE email = '{$email}' AND grade_no=11");
        $row_email = mysql_num_rows($getemail);

        if ($row_email > 0) {

            echo '1';
        } else {
            echo '0';
        }
    }


    function adm_get_color_options(){
        $colors = $this->ci->bbs->get_color_options();
        foreach($colors as $color){
            echo '<option value="'.$color->color_name.'">'.$color->color_name.'</option>';
        }
    }
    function adm_set_primary_image() {
        $idx = $this->ci->input->get_post('idx');
        $att_idx = $this->ci->input->get_post('att_idx');
        //$sort = $this->ci->input->get_post('sort');

        $data["att_idx"] = $att_idx;
        //clear last primary image

        $where_c["idx"] = $idx;
        $where_c['att_idx'] = $att_idx;
        $this->ci->bbs->resortExceptSelect($where_c);

        //sort image
        if ($this->ci->bbs->set_primary_image($data)) {
            echo "Success";
        } else {
            echo "Fail";
        }
    }
    function adm_set_primary_photo() {
        $idx = $this->ci->input->get_post('idx');
        $id = $this->ci->input->get_post('id');
        //$sort = $this->ci->input->get_post('sort');

        $data["id"] = $id;
        //clear last primary image

        $where_c["bbs_idx"] = $idx;
        $where_c['id'] = $id;
        $this->ci->bbs->resortExceptSelect($where_c);

        //sort image
        if ($this->ci->bbs->set_primary_photo($data)) {
            echo "Success";
        } else {
            echo "Fail";
        }
    }

    function adm_set_image_sort() {
        $idx = $this->ci->input->get_post('idx');
        $att_idx = $this->ci->input->get_post('att_idx');
        $nav = $this->ci->input->get_post('nav');
        $sort = $this->ci->input->get_post('sort');

        $data['idx'] = $idx;
        $data['att_idx'] = $att_idx;
        $data['nav'] = $nav;
        $data['sort'] = $sort;
        //$this->ci->bbs->resetSort($data);
        //$this->ci->bbs->resetSort($data);
        if ($this->ci->bbs->sortMove($data)) {
            echo "Success";
        } else {
            echo "Fail";
        }
    }
    function adm_set_all_image_sort() {
        $idx = $this->ci->input->get_post('idx');
        $att_idx = $this->ci->input->get_post('att_idx');
        $sort = $this->ci->input->get_post('sort');

        // $data['idx'] = $idx;
        // $data['att_idx'] = $att_idx;
        // $data['sort'] = $sort;
        //$this->ci->bbs->resetSort($data);
        //$this->ci->bbs->resetSort($data);
        $data = array();
        foreach($sort as $key=>$value){
            $data[]=array('att_idx'=>$key, 'sort'=>$value);
        }
        if ($this->ci->bbs->sortAllImages($data)) {
            echo "Success";
        } else {
            echo "Fail";
        }
    }
    function adm_set_profile_image() {
        $member_no = $this->ci->input->get_post('member_no');
        $profile_image_id = $this->ci->input->get_post('profile_image_id');


        $data["profile_image_id"] = $profile_image_id;
        //$this->updateSellerImageSQLite($profile_image_id,$member_no);
        //set profile image
        if ($this->ci->mo_file->set_profile_image($data,$member_no)) {
            echo "Success";
        } else {
            echo "Fail";
        }
    }
    function user_cloud_upload(){

        $select_cphoto_list = $this->ci->mo_file->select_cphoto($_SESSION['ADMIN']['member_no']);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        
        $this->ci->load->view('admin/ucloud_uploader.php', $this->ci->data, false);
    }
    function mobile_user_cloud_upload(){

        $select_cphoto_list = $this->ci->mo_file->select_cphoto($_SESSION['ADMIN']['member_no']);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        
        $this->ci->load->view('admin/mobile_ucloud_uploader.php', $this->ci->data, false);
    }
    function user_cloud_upload_exec(){
        $select_cphoto_list = $this->ci->mo_file->select_cphoto($_SESSION['ADMIN']['member_no']);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());

             $this->_user_cloud_upload_exec($_SESSION['ADMIN']['member_no'], 'file');
    }
    function user_delete_attach_cloud() {
        ob_clean();
        $public_id = $this->ci->input->get_post('public_id');
        $owner_file = $_SESSION['ADMIN']['member_no'];

        if($public_id !=''){

            $file_owner = $this->ci->mo_file->is_owner_file($public_id,$owner_file);
            
            if($file_owner->num_rows() > 0){

            $select_file = $this->ci->mo_file->select_owner_file($public_id,$owner_file);

            $full_path  = $_SERVER["DOCUMENT_ROOT"].$select_file[0]->base_url.$select_file[0]->public_id;
            $thumb_path = $_SERVER["DOCUMENT_ROOT"].$select_file[0]->base_url.'thumb/'.$select_file[0]->public_id;
            
            @unlink($full_path);
            @unlink($thumb_path);

                $this->ci->mo_file->delete_cloud_file($public_id);
                // \Cloudinary\Uploader::destroy($public_id);

            }else{
                echo func_jsAlertReplace('The photo does not exit.', '?c=admin&m=user_cloud_upload' . $this->mcd);
            }
        }

        $select_cphoto_list = $this->ci->mo_file->select_cphoto($owner_file);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        
        $this->ci->load->view('admin/ucloud_uploader', $this->ci->data, false);

    }
    function mobile_user_delete_attach_cloud() {
        $public_id = $this->ci->input->get_post('public_id');
        $owner_file = $_SESSION['ADMIN']['member_no'];

        if($public_id !=''){

            $file_owner = $this->ci->mo_file->is_owner_file($public_id,$owner_file);

            if($file_owner->num_rows() > 0){

                $this->ci->mo_file->delete_cloud_file($public_id);
                \Cloudinary\Uploader::destroy($public_id);

            }else{
                echo func_jsAlertReplace('The photo does not exit.', '?c=admin&m=user_cloud_upload' . $this->mcd);
            }
        }

        $select_cphoto_list = $this->ci->mo_file->select_cphoto($owner_file);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        
        $this->ci->load->view('admin/mobile_ucloud_uploader', $this->ci->data, false);

    }
    function _user_cloud_upload_exec($member_no, $element_file_name){
        $batch_data = array();
        for ($i = 0; $i < count($_FILES[$element_file_name]['name']); $i++) {
            $default_options = array('tags'=>array("member_image"), 'public_id'=>uniqid($member_no, true));
            $upload_result = \Cloudinary\Uploader::upload($_FILES[$element_file_name]['tmp_name'][$i], $default_options);
            /***GET BASE URL***/
            $exp_url = explode('/', $upload_result['secure_url']);
            $exp_last_key = count($exp_url)-1;
            $exp_url[$exp_last_key]='';
            unset($exp_url[$exp_last_key-1]);
            $data['base_url'] = implode('/', $exp_url);

            $data['public_id']          = $upload_result['public_id'];
            $data['file_owner']         = $member_no; 
            $data['resource_type']      = $upload_result['resource_type'];
            $data['secure_url']         = $data['base_url'].$data['public_id'];
            $data['original_filename']  = $_FILES[$element_file_name]['name'][$i];

            $batch_data[] = $data;
        }

        $this->ci->mo_file->insert_batch($batch_data);

        $member_no = $this->ci->input->get_post('member_no');
        $select_cphoto_list = $this->ci->mo_file->select_cphoto($member_no);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        $this->ci->load->view('admin/ucloud_uploader', $this->ci->data, false);
    }
    function adm_cloud_upload(){

        $select_cphoto_list = $this->ci->mo_file->select_cphoto($_GET['member_no']);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        
        $this->ci->load->view('admin/cloud_uploader', $this->ci->data, false);
    }
    function adm_cloud_upload_exec(){
        $select_cphoto_list = $this->ci->mo_file->select_cphoto($_GET['member_no']);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        $this->_adm_cloud_upload_exec($_GET['member_no'], 'file');
    }

    function adm_member_photo_insert(){

        $data['base_url']           = $this->ci->input->post('base_url');
        $data['public_id']          = $this->ci->input->post('public_id');
        $data['file_owner']         = $this->ci->input->post('file_owner');
        $data['resource_type']      = $this->ci->input->post('resource_type');
        $data['secure_url']         = $this->ci->input->post('secure_url');
        $data['original_filename']  = $this->ci->input->post('original_filename');
        $batch_data[] = $data;
        $result = $this->ci->mo_file->insert_batch($batch_data);
        if($result){
            $select_cphoto_list = $this->ci->mo_file->select_cphoto($data['file_owner']);
            return $select_cphoto_list;
        }
        // $member_no = $this->ci->input->get_post('member_no');
        // $select_cphoto_list = $this->ci->mo_file->select_cphoto($member_no);

        // func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        
    }

    function mobile_adm_cloud_upload_exec(){
        $select_cphoto_list = $this->ci->mo_file->select_cphoto($_GET['member_no']);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());

             $this->_mobile_adm_cloud_upload_exec($_GET['member_no'], 'file');
    }
    function _adm_cloud_upload_exec($member_no, $element_file_name){

        for ($i = 0; $i < count($_FILES[$element_file_name]['name']); $i++) {
            $tempFile = $_FILES[$element_file_name]['tmp_name'][$i];
            $ext =$_FILES[$element_file_name]['name'][$i];
            $ext = explode('.', $ext);
            $ext = end($ext);
            $ext = strtolower($ext);
            $fname = date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;
            $fileName = $fname;
            $targetPath = $_SERVER["DOCUMENT_ROOT"]. '/uploads/user/';
            $targetPath_thumb = $_SERVER["DOCUMENT_ROOT"]. '/uploads/user/thumb/';
            $targetFile = $targetPath . $fileName ;
            if(move_uploaded_file($tempFile, $targetFile)){
                
                $watermake_image = new ImageManipulator($targetPath.$fileName);
                $watermake_thumb = new ImageManipulator($targetPath.$fileName);
                $watermake_image->resize(800, 600);
                $watermake_thumb->resize(200, 140);
                $watermake_image->save($targetPath.$fileName);
                $watermake_thumb->save($targetPath_thumb.$fileName);
                $data['file_owner'] = $member_no;
                $data['public_id'] = $fileName;
                $data['base_url']  = '/uploads/user/';
                $data['resource_type'] = "image";
                $data['created_dt'] = date('Y-m-d H:i:s');
                // if you want to save in db,where here
                $batch_data[] = $data; 

                $this->ci->mo_file->insert_batch($batch_data);

            }
        }

        

        $member_no = $this->ci->input->get_post('member_no');
        $select_cphoto_list = $this->ci->mo_file->select_cphoto($member_no);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        $this->ci->load->view('admin/cloud_uploader', $this->ci->data, false);
    }
    function _mobile_adm_cloud_upload_exec($member_no, $element_file_name){
        $batch_data = array();
        for ($i = 0; $i < count($_FILES[$element_file_name]['name']); $i++) {
            $default_options = array('tags'=>array("member_image"), 'public_id'=>uniqid($member_no, true));
            $upload_result = \Cloudinary\Uploader::upload($_FILES[$element_file_name]['tmp_name'][$i], $default_options);
            /***GET BASE URL***/
            $exp_url = explode('/', $upload_result['secure_url']);
            $exp_last_key = count($exp_url)-1;
            $exp_url[$exp_last_key]='';
            unset($exp_url[$exp_last_key-1]);
            $data['base_url'] = implode('/', $exp_url);

            $data['public_id']          = $upload_result['public_id'];
            $data['file_owner']         = $member_no; 
            $data['resource_type']      = $upload_result['resource_type'];
            $data['secure_url']         = $data['base_url'].$data['public_id'];
            $data['original_filename']  = $_FILES[$element_file_name]['name'][$i];

            $batch_data[] = $data;
        }

        $this->ci->mo_file->insert_batch($batch_data);

        $member_no = $this->ci->input->get_post('member_no');
        $select_cphoto_list = $this->ci->mo_file->select_cphoto($member_no);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        $this->ci->load->view('admin/mobile_cloud_uploader', $this->ci->data, false);
    }
    function adm_delete_attach_cloud() {
        ob_clean();
        $public_id = $this->ci->input->get_post('public_id');
        $owner_file = $this->ci->input->get_post('member_no');

        if($public_id !=''){

            $select_file = $this->ci->mo_file->select_owner_file($public_id,$owner_file);

            $full_path  = $_SERVER["DOCUMENT_ROOT"].$select_file[0]->base_url.$select_file[0]->public_id;
            $thumb_path = $_SERVER["DOCUMENT_ROOT"].$select_file[0]->base_url.'thumb/'.$select_file[0]->public_id;
            
            @unlink($full_path);
            @unlink($thumb_path);

                $this->ci->mo_file->delete_cloud_file($public_id);
                // \Cloudinary\Uploader::destroy($public_id);
            // $this->ci->mo_file->delete_cloud_file($public_id);
            // \Cloudinary\Uploader::destroy($public_id);

        }

        $select_cphoto_list = $this->ci->mo_file->select_cphoto($owner_file);

        func_set_data($this->ci, 'select_cphoto_list', $select_cphoto_list->result());
        
        $this->ci->load->view('admin/cloud_uploader', $this->ci->data, false);

    }
    
    function adm_cloud_upload_list(){
        $this->ci->load->view('admin/upload_complete', $this->ci->data, false);
    }
    function adm_image_uploader(){

        $this->ci->load->view('admin/image_uploader', $this->ci->data, false);
        
    }
    function adm_image_uploader_exec(){

        require('controllers/UploadHandler.php');
        $upload_handler = new UploadHandler();

    }
    function adm_bbs_cloud_upload() {

        $idx = $this->ci->input->get_post('idx');
        func_set_data($this->ci, 'idx', $idx);
        $bbs_cloud_list = $this->ci->mo_file->select_bbs_cloud($idx);
        func_set_data($this->ci, 'bbs_cloud_list', $bbs_cloud_list->result());
        //set = start
        $this->ci->phpsession->save('uploading_status', 'stop'); //add grade_no
        $this->ci->load->view('admin/adm/bbs/product/bbs_cloud_uploader', $this->ci->data, false);
    }
	 function member_mobile_bbs_cloud_upload() {

        $idx = $this->ci->input->get_post('idx');
        func_set_data($this->ci, 'idx', $idx);
        $bbs_cloud_list = $this->ci->mo_file->select_bbs_cloud($idx);
        func_set_data($this->ci, 'bbs_cloud_list', $bbs_cloud_list->result());
        //set = start
        $this->ci->phpsession->save('uploading_status', 'stop'); //add grade_no
        $this->ci->load->view('member_mobile_site/product/bbs_mobile_cloud_uploader', $this->ci->data, false);
    }
    function adm_bbs_cloud_upload_exec(){

        $this->ci->load->helper('url');
        $idx = $this->ci->input->get_post('idx');

            $batch_data = array();
            $inData = array();
            $batch = array();

            $secure_url = $this->ci->input->get_post('secure_url');
            /***GET BASE URL***/
            $exp_url = explode('/', $secure_url);
            $exp_last_key = count($exp_url)-1;
            $exp_url[$exp_last_key]='';
            unset($exp_url[$exp_last_key-1]);

            $data['base_url']           = implode('/', $exp_url);
            $data['public_id']          = $this->ci->input->get_post('public_id');
            $data['bbs_idx']            = $idx; 
            $data['resource_type']      = $this->ci->input->get_post('resource_type');
            $data['secure_url']         = $secure_url;
            $data['original_filename']  = $this->ci->input->get_post('original_filename');
            $data['sort'] = 99; 
            $batch_data[] = $data;

            $insert_id = $this->ci->mo_file->insert_bbs_batch_images($batch_data);
            //$this->insertImageSQLite($batch_data);

            $data['idxs'] = $insert_id; 
            $batch[] = $data;
            $id_bbs_cloud = $this->ci->mo_file->select_id_bbs_cloud(); 

            $inData['bbs_idx'] = $idx;

            echo json_encode($batch);
            
            $this->ci->mo_file->resort_image($inData);
            //$this->updateImageReSort(null,$idx,0);
    }

    function adm_upload_image_car() {
        $idx = $this->ci->input->get_post('idx');
        $bbs_image_upload = $this->ci->mo_file->select_bbs_image_upload($idx);
        
        func_set_data($this->ci, 'idx', $idx);
        func_set_data($this->ci, 'bbs_image_upload', $bbs_image_upload);

        $this->ci->load->view('admin/adm/bbs/product/image_upload_car', $this->ci->data, false);
    }

    function adm_upload_image_car_exec(){

        $this->ci->load->helper('url','html','form');
        $idx = $this->ci->input->get_post('idx');
        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $ext =$_FILES['file']['name'];
            $ext = explode('.', $ext);
            $ext = end($ext);
            $ext = strtolower($ext);
            $fname = date('Ymd_His') . '_' . mt_rand('0000000','9999999') . '.' . $ext;
            $fileName = $fname;
            $targetPath = $_SERVER["DOCUMENT_ROOT"]. '/uploads/cars/';
            $targetPath_thumb = $_SERVER["DOCUMENT_ROOT"]. '/uploads/cars/thumb/';
            $targetFile = $targetPath . $fileName ;
            if(move_uploaded_file($tempFile, $targetFile)){
                
                $watermake_image = new ImageManipulator($targetPath.$fileName);
                $watermake_thumb = new ImageManipulator($targetPath.$fileName);
                $watermake_image->resize(800, 600);
                $watermake_image->watermark($targetPath.'watermark.png', 620, -20);
                $watermake_thumb->resize(200, 140);
                $watermake_image->save($targetPath.$fileName);
                $watermake_thumb->save($targetPath_thumb.$fileName);
                $data['bbs_idx'] = $idx;
                $data['public_id'] = $fileName;
                $data['base_url']  = '/uploads/cars/';
                $data['resource_type'] = "image";
                $data['created_dt'] = date('Y-m-d H:i:s');
                // if you want to save in db,where here
                $this->ci->bbs->insert_iw_cloud_bbs_images($data); 

                $inData['bbs_idx'] = $idx; 

                $this->ci->mo_file->resort_image($inData);


                //*************************INSERT LOG ACTIVITY*************************//
                $log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>' Images of the car (idx: '.$idx.') have been inserted by '.$_SESSION['ADMIN']['member_id'].'. Images: <br/>'. $targetPath . $fileName,
                    'log_type'=>'car_image',
                    'created_dt'=>func_get_date_time()
                );
                $this->ci->mo_activity_log->insert($log_data);


            }
        }

    }

    function adm_set_all_image_sort_car() {
        $bbs_idx = $this->ci->input->get_post('bbs_idx');
        $id = $this->ci->input->get_post('id');
        $sort = $this->ci->input->get_post('sort');

        // $data['idx'] = $idx;
        // $data['att_idx'] = $att_idx;
        // $data['sort'] = $sort;
        //$this->ci->bbs->resetSort($data);
        //$this->ci->bbs->resetSort($data);
        $data = array();
        foreach($sort as $key=>$value){
            $data[]=array('id'=>$key, 'sort'=>$value);
        }
        if ($this->ci->bbs->sortAllImages_car($data)) {
            echo "Success";
        } else {
            echo "Fail";
        }
    }

    function adm_insert_activity_log_images_for_car(){
        $idx = $this->ci->input->get_post('idx');
        $count_url = $this->ci->input->get_post('count_url');
        $url_string = $this->ci->input->get_post('url_string');
        $batch_url = substr($url_string, 0, - 2);
        //*************INSERT LOG ACTIVITY************//

        $log_data = array(
            'author'=>$_SESSION['ADMIN']['member_no'],
            'message'=>$count_url.' Images of the car (idx: '.$idx.') have been inserted by '.$_SESSION['ADMIN']['member_id'].'. Images: <br/>'.$batch_url,
            'log_type'=>'car_image',
            'created_dt'=>func_get_date_time()
        );
        $this->ci->mo_activity_log->insert($log_data);
    }
    
    function adm_cloud_upload_thumbnail(){

            $url = $this->ci->input->get_post('url');
            $public_id = $this->ci->input->get_post('public_id');
            /***GET BASE URL***/
            $exp_url = explode('/', $url);
            $exp_last_key = count($exp_url)-1;
            $exp_url[$exp_last_key]='';
            unset($exp_url[$exp_last_key-1]);

            $data['car_image'] = implode('/', $exp_url)."".$public_id;
            echo json_encode($data,JSON_UNESCAPED_SLASHES);
            
    }
    // function adm_bbs_cloud_upload_exec() {
        
    //     $idx = $_GET['idx'];
        
    //     $uploading_status = $this->ci->phpsession->get('uploading_status');
    //     if($uploading_status =='stop'){
    //         $this->ci->phpsession->save('uploading_status', 'start'); //add grade_no
    //         $this->_adm_bbs_cloud_upload_exec($idx, 'file');
    //     }
    // }
    // function _adm_bbs_cloud_upload_exec($idx, $element_file_name){
    //     ob_clean();
    //     $this->ci->load->helper('url');

        
    //     if (count($_FILES[$element_file_name]['name']) >50) {
    //         header("Location: /?c=admin&m=adm_bbs_cloud_upload&idx=".$idx);
    //     }
    //     else{
    //         $batch_data = array();
    //         $inData = array();
    //         for ($i = 0; $i < count($_FILES[$element_file_name]['name']); $i++) {
    //             $default_options = array('tags'=>array("item"), 'public_id'=>uniqid($idx, true));
    //             $upload_result = \Cloudinary\Uploader::upload($_FILES[$element_file_name]['tmp_name'][$i], $default_options);
    //             /***GET BASE URL***/
    //             $exp_url = explode('/', $upload_result['secure_url']);
    //             $exp_last_key = count($exp_url)-1;
    //             $exp_url[$exp_last_key]='';
    //             unset($exp_url[$exp_last_key-1]);
    //             $data['base_url'] = implode('/', $exp_url);

    //             $data['public_id']          = $upload_result['public_id'];
    //             $data['bbs_idx']            = $idx; 
    //             $data['resource_type']      = $upload_result['resource_type'];
    //             $data['secure_url']         = $data['base_url'].$data['public_id'];
    //             $data['original_filename']  = $_FILES[$element_file_name]['name'][$i];
    //             $data['sort'] = 99; 
    //             $batch_data[] = $data;

    //         }

    //         $idx = $this->ci->input->get_post('idx');

    //         $this->ci->mo_file->insert_bbs_batch_images($batch_data);
    //         $id_bbs_cloud = $this->ci->mo_file->select_id_bbs_cloud(); 

    //         $inData['bbs_idx'] = $idx;
            
    //         $this->ci->mo_file->resort_image($inData);
    //         header("Location: /?c=admin&m=adm_bbs_cloud_upload&idx=".$idx, 'refresh');
    //     }

    // }
    function adm_bbs_delete_attach_cloud() {
        $this->ci->load->helper('url');
        $public_id = $this->ci->input->get_post('public_id');
        $idx = $this->ci->input->get_post('idx');
        //$image=$this->ci->bbs->sqliteCarImage($public_id);
        if($public_id !=''){
            
            $this->ci->mo_file->delete_bbs_cloud_file($public_id);
            //$this->deletePublicImageSQLite($image[0]->image);
            \Cloudinary\Uploader::destroy($public_id);

            //*************************INSERT LOG ACTIVITY*************************//
            $path_image = "https://res.cloudinary.com/softbloom/image/upload/".$public_id;
            $log_data = array(
                'author'=>$_SESSION['ADMIN']['member_no'],
                'message'=>count($public_id).' Images of the car (idx: '.$idx.') have been deleted by '.$_SESSION['ADMIN']['member_id'].'. Images: <br/>'. $path_image,
                'log_type'=>'car_image',
                'created_dt'=>func_get_date_time()
            );
            $this->ci->mo_activity_log->insert($log_data);
        }
        
        $inData = array();
        $inData['bbs_idx'] = $idx;

        $this->ci->mo_file->resort_image($inData);

        $bbs_cloud_list = $this->ci->mo_file->select_bbs_cloud($idx);
        func_set_data($this->ci, 'bbs_cloud_list', $bbs_cloud_list->result());

        $this->ci->load->view('admin/adm/bbs/product/bbs_cloud_uploader', $this->ci->data, false);
    }


    function adm_bbs_delete_attach() {
        ob_clean();
        $this->ci->load->helper('url');
        $only_attach = $this->ci->input->get_post('only_attach');
        $idx = $this->ci->input->get_post('idx');
        if ($only_attach != '' && $only_attach != 'N') {

            $att_idx = $this->ci->input->get_post('att_idx');

            if (isset($att_idx) && $att_idx != '') {

            $where = array('id'=>$att_idx);
            $detail_image = $this->ci->mo_file->select_detail_image($where);
            
            $full_path  = $_SERVER["DOCUMENT_ROOT"].$detail_image[0]->base_url.$detail_image[0]->public_id;
            $thumb_path = $_SERVER["DOCUMENT_ROOT"].$detail_image[0]->base_url.'thumb/'.$detail_image[0]->public_id;
            
            @unlink($full_path);
            @unlink($thumb_path);

            $this->ci->mo_file->delete_bbs_file($att_idx);

            //*************************INSERT LOG ACTIVITY*************************//
            $log_data = array(
                'author'=>$_SESSION['ADMIN']['member_no'],
                'message'=>count($att_idx).' Images of the car (idx: '.$idx.') have been deleted by '.$_SESSION['ADMIN']['member_id'].'. Images: <br/>'. $full_path,
                'log_type'=>'car_image',
                'created_dt'=>func_get_date_time()
            );
            $this->ci->mo_activity_log->insert($log_data);

            }
        }

        $inData = array();
        $inData['bbs_idx'] = $idx;

        $this->ci->mo_file->resort_image($inData);

        header('Location: /?c=admin&m=adm_upload_image_car&idx='.$idx.'&mcd=product');

    }

    function adm_bbs_delete_attach_check(){
        ob_clean();
        $id = $this->ci->input->get_post('idx');
        $idx = $this->ci->input->get_post('idxs');

        foreach ($idx as $key =>$value){
            $idx_items[] = $value; 
            $where = array('id'=>$value);
            $detail_image = $this->ci->mo_file->select_detail_image($where); 

            $full_path  = $_SERVER["DOCUMENT_ROOT"].$detail_image[0]->base_url.$detail_image[0]->public_id;
            $thumb_path = $_SERVER["DOCUMENT_ROOT"].$detail_image[0]->base_url.'thumb/'.$detail_image[0]->public_id;
            
            @unlink($full_path);
            @unlink($thumb_path);

            $batch_path_image[] = $detail_image[0]->base_url.$detail_image[0]->public_id;

        }

        //*************************INSERT LOG ACTIVITY*************************//

        $path_image = implode(',', $batch_path_image);

        $log_data = array(
            'author'=>$_SESSION['ADMIN']['member_no'],
            'message'=>count($idx).' Images of the car (idx: '.$id.') have been deleted by '.$_SESSION['ADMIN']['member_id'].'. Images: <br/>'. $path_image,
            'log_type'=>'car_image',
            'created_dt'=>func_get_date_time()
        );
        $this->ci->mo_activity_log->insert($log_data);

        $this->ci->mo_file->delete_bbs_file($idx_items);

        $inData = array();
        $inData['bbs_idx'] = $id;

        $this->ci->mo_file->resort_image($inData);

        header('Location: /?c=admin&m=adm_upload_image_car&idx='.$id.'&mcd=product');
        
    }

    function adm_bbs_delete_attach_check_seller(){
        ob_clean();
        $idx = $this->ci->input->get_post('idx');

              if(!empty($idx)){
                /** SELETE DETAIL INFO IMAGE **/
                $where = array('bbs_idx'=>$idx);
                $detail_image = $this->ci->mo_file->select_detail_image($where);

                if (!empty($detail_image)) {
                    foreach ($detail_image as $row) {
                        $bbs_idx[] = $row->id;
                        $full_path  = $_SERVER["DOCUMENT_ROOT"].$row->base_url.$row->public_id;
                        $thumb_path = $_SERVER["DOCUMENT_ROOT"].$row->base_url.'thumb/'.$row->public_id;
                        /** DELETE IMAGE FILES **/
                        @unlink($full_path);
                        @unlink($thumb_path);

                    }
                    /** DELETE IMAGE DATA **/
                    $this->ci->mo_file->delete_bbs_file($bbs_idx);
                }

                $chassis_no = $this->ci->bbs->get_chassis_by_idx($idx);
                
                //******************INSERT LOG ACTIVITY*********************//
                $log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no[0]->car_chassis_no.') has been deleted by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'car',
                    'created_dt'=>func_get_date_time()
                );
                $this->ci->mo_activity_log->insert($log_data);
                /** DELETE DATA **/
                $this->ci->bbs->delete($idx,'product');
                /** SHOE FLASH MESSAGE **/
                $this->ci->session->set_flashdata('msg_data', 'Success! The car has been deleted.');
                $this->ci->session->set_flashdata('msg_class', 'message_success');
                /** CHECK SERVER NAME **/
                if ($_SERVER['SERVER_NAME'] == 'motorbb.com' ){
                    $this->ci->mo_parse->delete_by_idx($idx);
                }

           }else{

                $this->ci->session->set_flashdata('msg_data', 'Deletion Failed!');
                $this->ci->session->set_flashdata('msg_class', 'message_fail');
           }
           
        redirect('/?c=admin&m=list_car', 'refresh');

    }

     function adm_bbs_delete_attach_click_seller(){
        ob_clean();
        $idxs = $this->ci->input->get_post('idx');
       
        foreach ($idxs as $key => $value) {
            $idx=$value;
            if($idx !=''){
                /** SELETE DETAIL INFO IMAGE **/
                $where = array('bbs_idx'=>$idx);
                $detail_image = $this->ci->mo_file->select_detail_image($where);

                if (!empty($detail_image)) {
                    foreach ($detail_image as $row) {
                        $bbs_idx[] = $row->id;
                        $full_path  = $_SERVER["DOCUMENT_ROOT"].$row->base_url.$row->public_id;
                        $thumb_path = $_SERVER["DOCUMENT_ROOT"].$row->base_url.'thumb/'.$row->public_id;
                        /** DELETE IMAGE FILES **/
                        @unlink($full_path);
                        @unlink($thumb_path);

                    }
                    /** DELETE IMAGE DATA **/
                    $this->ci->mo_file->delete_bbs_file($bbs_idx);
                }
                
                $chassis_no = $this->ci->bbs->get_chassis_by_idx($idx);
                  
                //*******************INSERT DELETE LOG ACTIVITY*******************//
                    $data = array(
                        'author'=>$_SESSION['ADMIN']['member_no'],
                        'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no[0]->car_chassis_no.') has been deleted by '.$_SESSION['ADMIN']['member_id'],
                        'log_type'=>'car',
                        'created_dt'=>func_get_date_time()
                    );

                $this->ci->mo_activity_log->insert($data);

                $this->ci->bbs->delete($idx,'product');

                $this->ci->session->set_flashdata('msg_data', 'Success! The car has been deleted.');
                $this->ci->session->set_flashdata('msg_class', 'message_success');
                
            }else{

                $this->ci->session->set_flashdata('msg_data', 'Deletion Failed!');
                $this->ci->session->set_flashdata('msg_class', 'message_fail');
            }
        }
 
        redirect('/?c=admin&m=list_car', 'refresh');
      
    }
    
    function adm_bbs_delete_attach_cloud_check(){
        ob_clean();
        $get_url = $this->ci->input->get_post('idx');
        $idx = $this->ci->input->get_post('idxs');
        foreach ($idx as $key =>$value){
            $public_ids[] = $value;
            $idx_items[] = $value;            
            //$images[]=$this->ci->bbs->sqliteCarImage($value);
            $batch_path_image[] = "https://res.cloudinary.com/softbloom/image/upload/".$value;
        }

        //*************************INSERT LOG ACTIVITY*************************//

        $path_image = implode(',', $batch_path_image);

        $log_data = array(
            'author'=>$_SESSION['ADMIN']['member_no'],
            'message'=>count($idx).' Images of the car (idx: '.$get_url.') have been deleted by '.$_SESSION['ADMIN']['member_id'].'. Images: <br/>'. $path_image,
            'log_type'=>'car_image',
            'created_dt'=>func_get_date_time()
        );
        $this->ci->mo_activity_log->insert($log_data);

        $api = new \Cloudinary\Api();

        $api->delete_resources($public_ids);

        $this->ci->mo_file->delete_bbs_cloud_file($idx_items);
        // foreach ($images as $image) {   
        //     $this->deletePublicImageSQLite($image[0]->image);
        // }
        // header("Location: /?c=admin&m=adm_bbs_cloud_upload&idx=".$get_url);
    }


    function adm_bbs_delete_attach_cloud_check_seller(){
        ob_clean();

        $get_url = $this->ci->input->get_post('idx');
        $idxp =explode(";", $this->ci->input->get_post('id'));// $this->ci->input->get_post('idxs');

       foreach ($idxp as $key =>$value){

            $idx=$value;
            if($idx !=''){
                $public_id = $this->ci->mo_file->select_bbs_public_id($idx) ;


                if(count($public_id) >0){
                    foreach ($public_id as $id){
                        $idx_items[]=$id->public_id;
                        $public_ids[] = $id->public_id;
                    }

                    $api = new \Cloudinary\Api();
                    $api->delete_resources($public_ids);
                    $this->ci->mo_file->delete_bbs_cloud_file($idx_items);
                       
               }

                $chassis_no = $this->ci->bbs->get_chassis_by_idx($idx);
                
                //******************INSERT LOG ACTIVITY*********************//
                $log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no[0]->car_chassis_no.') has been deleted by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'car',
                    'created_dt'=>func_get_date_time()
                );
                $this->ci->mo_activity_log->insert($log_data);

                $this->ci->bbs->delete($idx,'product');

                $this->ci->session->set_flashdata('msg_data', 'Success! The car has been deleted.');
                $this->ci->session->set_flashdata('msg_class', 'message_success');
                
                if ($_SERVER['SERVER_NAME'] == 'motorbb.com' ){
                    $this->ci->mo_parse->delete_by_idx($idx);
                }
                //$this->deleteSQLite($idx);
           }else{

                $this->ci->session->set_flashdata('msg_data', 'Deletion Failed!');
                $this->ci->session->set_flashdata('msg_class', 'message_fail');
           }
           
       }  

        redirect('/?c=admin&m=list_car', 'refresh');

    }

 function adm_bbs_delete_attach_cloud_click_seller(){
        ob_clean();
        $idxs = $this->ci->input->get_post('idx');
       
        foreach ($idxs as $key => $value) {
            $idx=$value;
            if($idx !=''){
                 $public_id = $this->ci->mo_file->select_bbs_public_id($idx);
                 if(count($public_id)>0){
                    foreach ($public_id as $id) {
                       $idx_items[]=$id->public_id;
                       $public_ids[] = $id->public_id;
                    }
                     $api=new \Cloudinary\Api();
                     $api->delete_resources($public_ids);
                     $this->ci->mo_file->delete_bbs_cloud_file($idx_items);
                 }
                  $chassis_no = $this->ci->bbs->get_chassis_by_idx($idx);
                  
                 // $this->deleteSQLite($idx);
                  //*******************INSERT DELETE LOG ACTIVITY*******************//
                    $data = array(
                        'author'=>$_SESSION['ADMIN']['member_no'],
                        'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no[0]->car_chassis_no.') has been deleted by '.$_SESSION['ADMIN']['member_id'],
                        'log_type'=>'car',
                        'created_dt'=>func_get_date_time()
                    );

                $this->ci->mo_activity_log->insert($data);

                $this->ci->bbs->delete($idx,'product');

                $this->ci->session->set_flashdata('msg_data', 'Success! The car has been deleted.');
                $this->ci->session->set_flashdata('msg_class', 'message_success');
                
            }else{

                $this->ci->session->set_flashdata('msg_data', 'Deletion Failed!');
                $this->ci->session->set_flashdata('msg_class', 'message_fail');
            }
        }
 
        redirect('/?c=admin&m=list_car', 'refresh');
      
    }
                      
    
    function delete_attach_cloud_images(){
          //$public_id = $this->ci->input->get_post('public_id');
          $idx = $this->ci->input->get_post('idx');
          foreach ($idx as $key =>$values){
            $idx=$values;
     
          // if($public_id !=''){
            
            $this->ci->mo_file->delete_bbs_cloud_file_items($idx);
            //$this->deleteImageSQLite($idx);
            \Cloudinary\Uploader::destroy($idx);

        //}
          }
    }
            
    function adm_set_primary_cloud_image() {
        $bbs_idx = $this->ci->input->get_post('bbs_idx');
        $idx = $this->ci->input->get_post('idx');

        $where_c["id"] = $idx;
        $where_c['bbs_idx'] = $bbs_idx;

        $this->ci->mo_file->resort_except_select($where_c);
        $this->admin_log("member_id: ".$this->auth_member_id.", action: setting car primary image, image_id: ".$idx.", bbs_idx: ".$bbs_idx);
        $data['id'] = $idx;
        $data['bbs_idx'] = $bbs_idx;
        //set profile image
        if ($this->ci->mo_file->set_primary_cloud_image($data)) {
            //$image=$this->ci->bbs->sqliteCarImageByID($idx);
            //$this->updateImagePrimay($image,$bbs_idx);
           // $this->updateImageReSort($idx,$bbs_idx,1);
            echo "1";
        } else {
            echo "0";
        }
    }

    function admin_log($text){
        $date = date('l jS \of F Y h:i:s A');
        file_put_contents('admin_log.txt', "\n".'['.$date.'] '.$text, FILE_APPEND);
    }

    function adm_set_all_bbs_image_sort() {
        $idx = $this->ci->input->get_post('primary');
        $bbs_idx = $this->ci->input->get_post('idx');
        $sort = $this->ci->input->get_post('sort');
        
        $where_c["id"] = $idx;
        $where_c['bbs_idx'] = $bbs_idx;
       
        $this->ci->mo_file->resort_except_select($where_c);
        $data = array();
        foreach($sort as $key=>$value){
            $data[]=array('id'=>$key, 'sort'=>$value);
        }
        if ($this->ci->mo_file->sort_all_bbs_images($data, $bbs_idx)) {
            //$this->updateImageSort($data);
            $this->admin_log("member_id: ".$this->auth_member_id.", action: sorting car image, result: success, bbs_idx: ".$bbs_idx);
            echo "Success";
        } else {
            $this->admin_log("member_id: ".$this->auth_member_id.", action: sorting car image, result: fail, bbs_idx: ".$bbs_idx);
            echo "Fail";
        }
    }
    function adm_bbs_set_image_sort() {
        $bbs_idx = $this->ci->input->get_post('idx');
        $idx = $this->ci->input->get_post('att_idx');
        $nav = $this->ci->input->get_post('nav');
        $sort = $this->ci->input->get_post('sort');

        $data['idx'] = $idx;
        $data['att_idx'] = $bbs_idx;
        $data['nav'] = $nav;
        $data['sort'] = $sort;

        if ($this->ci->mo_file->sortMove_image($data)) {
            echo "Success";
        } else {
            echo "Fail";
        }
    }
    function adm_image_upload() {
        $idx = $this->ci->input->get_post('idx');

        func_set_data($this->ci, 'idx', $idx);
        $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);
        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list->result());

        $this->ci->load->view('admin/adm/bbs/product/image_upload', $this->ci->data, false);
    }

    function adm_image_upload_exec() {
        $idx = $this->ci->input->get_post('idx');

        $num_imageUpload = 0;

        $idx_image = array();

        $id_array = 0;



        if (is_array($idx)) {

            $idx = $idx[0];
        }

        $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);   //첨부파일 조회

        if (count($bbs_attach_list) > 0) {

            foreach ($bbs_attach_list->result() as $att_rows) {

                $idx_image[$id_array][0] = $att_rows->att_idx;
            }
        }

        $attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn');   //첨부파일 사용여부

        $upload_str = '';

        // count image upload

        $num_imageUpload = count($_FILES['attach1']['name']);


        //첨부파일을 사용한다면...

        if ($attach_file_yn != 'N') {

            for ($i = 0; $i < $num_imageUpload; $i++) {

                //선택된 첨부 파일만...

                if ($_FILES['attach1']['tmp_name'][$i] != '') {

                    //파일 업로드

                    $upload_str = $this->ci->iwc_common->upload('attach1', $i, $this->mcd, $this->bbs_config);



                    //파일 업로드 실패

                    if ($upload_str != TRUE) {

                        show_error($this->ci->iwc_common->upload_error_msg());
                    }

                    //파일 업로드 성공
                    else {

                        $upload_file_info = array();

                        $upload_file_info = $this->ci->iwc_common->upload_success_msg();



                        //첨부파일 DB 등록

                        $upload_file_info['idx'] = $idx;

                        //$upload_file_info['menu_code'] = $this->mcd;

                        
                        $countPrimary = $this->ci->bbs->countPrimaryImage($idx);
                        //Set primary auto
                        if($countPrimary<=0 && $i==0){
                            $upload_file_info['sort'] = 0;
                        }
                        else{
                            $upload_file_info['sort'] = 99;
                        }

                        $this->ci->bbs->insert_attach($upload_file_info);

                        $this->ci->bbs->resortExceptPrimary($upload_file_info);
                    }
                }
            }
        }

        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴

        $inData['idx'] = $idx;    //게시물 일련번호

        $inData['menu_code'] = $this->mcd;        //메뉴코드
        //$inData['member_id']  = $this->ci->member_id;                 //아이디
        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

            // 시행일

            $inData['publish_start'] = $inData['publish_end'] . ' 00:00:00';

            $inData['publish_end'] = $inData['publish_end'] . ' 23:59:59';
        }

        //우편번호
        //DB 수정
        // $bbs_attach_list = $this->ci->bbs->select_attach_files($idx);
        func_set_data($this->ci, 'idx', $idx);
        $bbs_attach_list = $this->ci->bbs->select_attach($idx);
        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list->result());

        $this->ci->load->view('admin/adm/bbs/product/image_upload', $this->ci->data, false);
//var_dump($inData);
        //echo func_jsAlertReplace('Car Information has beed modified.', '?c=admin&m=adm_bbs_list&mcd=' . $this->mcd);
    }

    //게시판 목록 조회

    function adm_bbs_list() {
        $customer_no= $_SESSION['ADMIN']['member_no'];
        $export_file = $this->ci->input->get_post("export_file");
        $last_query = $this->ci->input->get_post("last_query");
        $order_by = $this->ci->input->get_post("order_by");
        $order_d = $this->ci->input->get_post("order_d");
        $order='icon_status ASC, created_dt DESC';
        if(!empty($order_by)){
            $order = $order_by." ".$order_d;
        }
        //bbs_excel_exec

       
        $where = array();   //sql where 조건
        $sParam = '';     //검색 string
        //카테고리 검색

        $category_yn = func_get_config($this->bbs_config, 'category_yn');   //카테고리 검색

        $category = $this->ci->input->get_post('category');   //카테고리 코드

        if ($category_yn != 'N' && $category != '') {

            $sParam .= '&category=' . $category;

            $where['category ='] = $category;
        }

        $sch_car_owner = $this->ci->input->get_post('sch_car_owner');
        if (!empty($sch_car_owner)) {
            $where['car_owner'] = $sch_car_owner;
        }

        $sch_car_location = $this->ci->input->get_post('sch_car_location');
        if (!empty($sch_car_location)) {
            $where['country'] = $sch_car_location;
        }
        
        
         $carmodel = $this->ci->input->get_post('carmodel');
        if ($carmodel  !='') {
            $where['car_model'] = $carmodel;
        }
         $carmake = $this->ci->input->get_post('carmake');
        if ($carmake !='') {
            $where['car_make'] = $carmake;
        }
        
         $year = $this->ci->input->get_post('year');
        if ($year !='') {
            $where['car_model_year'] = $year;
        }


        $sch_icon_status = $this->ci->input->get_post('sch_icon_status');
        if ($sch_icon_status !='') {
            $where['icon_status'] = $sch_icon_status;
            // if(empty($sch_icon_status)){
            //  $where['icon_status=']=null;
            // }
        }

        //날짜 검색

        $sch_create_dt_s = $this->ci->input->get_post('sch_create_dt_s');
        $sch_create_dt_e = $this->ci->input->get_post('sch_create_dt_e');   //등록일 종료

        //등록일 시작

        if ($sch_create_dt_s != '') {

            $sParam .= '&sch_create_dt_s=' . $sch_create_dt_s;

           // $where['car_buying_dt >='] = $sch_create_dt_s . " 00:00:01";
            $where['car_buying_dt >='] = $sch_create_dt_s;
        }
        

        if ($sch_create_dt_e != '') {

            $sParam .= '&sch_create_dt_e=' . $sch_create_dt_e;

           $where['car_buying_dt <='] = $sch_create_dt_e;
        
         
        }
        
       if($sch_create_dt_s =='' && $sch_create_dt_e !=''){
            $sParam .= '&sch_create_dt_e=' . $sch_create_dt_e;

           $where['car_buying_dt '] = $sch_create_dt_e;
       }
       if($sch_create_dt_e =='' && $sch_create_dt_s !='' ){
           $sParam .= '&sch_create_dt_s=' . $sch_create_dt_s;

           // $where['car_buying_dt >='] = $sch_create_dt_s . " 00:00:01";
            $where['car_buying_dt '] = $sch_create_dt_s;
       }
        



        $sch_condition = $this->ci->input->get_post('sch_condition');     //검색조건

        $sch_word = $this->ci->input->get_post('sch_word');    //검색어
        $sch_words = $this->ci->input->get_post('sch_word');

	$sch_words = $this->ci->input->get_post('sch_word');
        if ($sch_words !='') {
            $where['car_keyword'] ='%'.$sch_words.'%';
            //$where['car_stock_no or car_stock_no like'] = '%' .$sch_words. '%';
           
           
        }	
		
		 if($sch_condition == ''){
            $space = substr_count($sch_word, ' ');
            if($space == 2){
    		 	$sch_word = trim($sch_word);
    		  	$result = explode(' ', $sch_word, 3);
    			$where["car_make like"] = '%' . $result[1] . '%';
    			$where["car_model like"] = '%' . $result[2] . '%';
    			$where["car_model_year like"] = '%' . $result[0] . '%';
                        $where["car_model_year like"] = '%' . $result[0] . '%';
            }
         }


        if ($sch_condition != '' && $sch_word != '') {

            $sch_condition_tmp = explode('|', $sch_condition);


            //다중 조건인 경우(제목 + 내용)

            if (count($sch_condition_tmp) > 1) {

                $tmpStr = '';

                for ($i = 0; $i < count($sch_condition_tmp); $i++) {

                    if ($tmpStr != '') {

                        $tmpStr .= ' or ';
                    }



                    $tmpStr .= $sch_condition_tmp[$i] . ' ? ';

                    $where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';
                }



                $tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

                $where[$tmpStr] = '';


                $sParam .= '&sch_condition=' . $sch_condition;

                $sParam .= '&sch_word=' . $sch_word;
            } else {


                // validate if condition is customer

                if ($sch_condition == 'customer_name like') {

                    $sql = mysql_query("SELECT iw_customer.customer_no FROM iw_customer WHERE customer_name='" . $sch_word . "'");

                    while ($row = mysql_fetch_array($sql)) {
                        $sch_word = $row['customer_no'];
                        $sch_condition = 'customer_no =';
                    }mysql_free_result($sql);
                }

                if ($sch_condition == 'customer_name like') {

                    $sch_word = 'N';

                    $sch_condition = 'customer_no like';
                }
                if (($sch_condition != 'customer_no =') || ($sch_condition != 'customer_name like')) {

                    $where[$sch_condition] = '%' . $sch_word . '%';
                } else {

                    $where[$sch_condition] = '%' . $sch_word . '%';
                }

                $sParam .= '&sch_condition=' . $sch_condition;

                $sParam .= '&sch_word=' . $sch_word;
            }
        }


        //icon_status search

        $status_condition = $this->ci->input->get_post('status_condition');

        if ($status_condition != '') {

            $sParam .= '&status_condition=' . $status_condition;

            $where['icon_status = '] = $status_condition;
          
        }

        /* var_dump($where);
          var_dump($status_condition); */


        //체크박스 조건
        $sch_checkbox = array();
        $sch_checkbox = ($this->ci->input->get_post('sch_checkbox'));

        if ($sch_checkbox != '') {
            //var_dump(count($sch_checkbox));
            //var_dump($_POST); exit();

            if (count($sch_checkbox) > 0) {
                $tmpStr = '';

                for ($i = 0; $i < count($sch_checkbox); $i++) {
                    if ($tmpStr != '') {

                        $tmpStr .= ' or ';
                    }
                    //echo $sch_checkbox[$i];
                    $chk_condition = $sch_checkbox[$i];

                    //$tmpStr .= $chk_condition[$i] . ' ? ';

                    $where[$chk_condition] = 'Y';

                    $sParam .= '&' . $chk_condition . 'Y';
                }

                //var_dump($where);
                //var_dump($tmpStr); exit();
                //var_dump($where[$chk_condition]); exit();
            }
        }


        

        //페이지 조건

        $cur_page = $this->ci->input->get_post("cur_page");

        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;

        $page_rows = (integer) func_get_config($this->bbs_config, 'page_rows');    //페이지 로우수
        //var_dump($page_rows); exit();

        $page_views = (integer) func_get_config($this->bbs_config, 'page_views');   //페이지 네비게이션 수
        //게시판 공지 목록 조회

        //$bbs_notice_list = $this->ci->bbs->adm_select($where, ($cur_page - 1) * $page_rows, $page_rows);

        $grade_no_session = $this->ci->grade_no;
        $member_no_session = $this->ci->member_no;
        if ($grade_no_session == 11){
            $where['car_owner'] = $member_no_session;
            $bbs_list_sale = $this->ci->bbs->adm_sale_select($customer_no,$where, ($cur_page - 1) * $page_rows, $page_rows,  $order);
            if(isset($where['car_keyword'])){
                unset($where['car_keyword']);
            }
            func_set_data($this->ci, 'bbs_list_sale', $bbs_list_sale);
        }
		
        //게시판 일반 목록 조회
		$list_destination = $this->ci->bbs->select_destination();

        $country_list = $this->ci->bbs->list_country_location_item();

        $sch_car_image = $this->ci->input->get_post('sch_car_image');

        if(!empty($sch_car_image)){
             $bbs_list = $this->ci->bbs->adm_select_not_image($where, ($cur_page - 1) * $page_rows, $page_rows,  $order);
             $result_total_rows = $this->ci->bbs->adm_total_select_not_image($where);
        }else{
            $bbs_list = $this->ci->bbs->adm_select($where, ($cur_page - 1) * $page_rows, $page_rows,  $order);
            $result_total_rows = $this->ci->bbs->adm_total_select($where);
        }
		
        $total_rows = $result_total_rows['0']->num_rows;
        $row_cnt = $total_rows - ($cur_page - 1) * $page_rows;      //글 번호
        //페이지정보 조회

        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);
        
        $select_make_all = $this->ci->bbs->carmaketodropdownsearch();

        $select_model_all = $this->ci->bbs->carmodeltodropdownsearch();

        $model_list = array();
        $where_model['car_make']=$carmake;
        $model_list = $this->ci->bbs->getModel($where_model, true);

        $list_car_owner = $this->ci->bbs->get_carOwner();

        //이전글 다음글 조회

        // if ($prev_next == 'Y') {

        //     $pn_data = array();

        //     $pn_data['isPrev'] = '';



        //     $prev = ($cur_page - 1) * $page_rows;



        //     if ($prev > 0) {

        //         $pn_data['isPrev'] = "PREV";

        //         $prev = $prev - 1;
        //     }



        //     $pn_data['cur_page'] = $cur_page;

        //     $pn_data['page_views'] = $page_views;

        //     $pn_data['page_rows'] = $page_rows;

        //     $pn_data['total_rows'] = $total_rows;

        //     $pn_data['row_cnt'] = $row_cnt;



        //     if ($pn_data['isPrev'] == 'PREV') {

        //         $page_rows = $page_rows + 2;
        //     } else {

        //         $page_rows = $page_rows + 1;
        //     }

        //     $pn_data['prevList'] = $this->ci->bbs->select($this->mcd, 'N', $prev, $page_rows, $where)->result();



        //     return $pn_data;
        // }

        func_set_data($this->ci, 'list_car_owner', $list_car_owner);
       
        func_set_data($this->ci, 'select_make_all',$select_make_all);
        func_set_data($this->ci, 'model_list',$model_list);

        func_set_data($this->ci, 'select_model_all',$select_model_all);

        func_set_data($this->ci, 'list_destination',$list_destination);

        func_set_data($this->ci, 'total_rows', $total_rows);     //조회 전체 목록수

        func_set_data($this->ci, 'total_page', $page_info[0]);   //전체 페이지

        func_set_data($this->ci, 'row_cnt', $row_cnt);     //글번호

        func_set_data($this->ci, 'cur_page', $cur_page);      //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);       //검색 string

     

        func_set_data($this->ci, 'bbs_list', $bbs_list);

        func_set_data($this->ci, 'country_list', $country_list);

        func_set_data($this->ci, 'page_info', $page_info);
       // $bbs_cloud_list_items = $this->ci->mo_file->select_bbs_cloud_all();
        //func_set_data($this->ci, 'bbs_cloud_list_items', $bbs_cloud_list_items->result());

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/list', $this->ci->data, true));
       


        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    
    }
    
    
    function adm_bbs_list_sender(){
      

       
        $where = array();   //sql where 조건

        $sParam = '';     //검색 string
        //카테고리 검색

        $category_yn = func_get_config($this->bbs_config, 'category_yn');   //카테고리 검색

        $category = $this->ci->input->get_post('category');   //카테고리 코드

        if ($category_yn != 'N' && $category != '') {

            $sParam .= '&category=' . $category;

            $where['category ='] = $category;
        }


        $sch_condition_sender = $this->ci->input->get_post('sch_condition_sender');     //검색조건

        $sch_word_sender = $this->ci->input->get_post('sch_word_sender');    //검색어
        //검색조건 + 검색어

        if ($sch_condition_sender != '' && $sch_word_sender != '') {

            $sch_condition_tmp = explode('|', $sch_condition_sender);


            //다중 조건인 경우(제목 + 내용)

            if (count($sch_condition_tmp) > 1) {

                $tmpStr = '';

                for ($i = 0; $i < count($sch_condition_tmp); $i++) {

                    if ($tmpStr != '') {

                        $tmpStr .= ' or ';
                    }



                    $tmpStr .= $sch_condition_tmp[$i] . ' ? ';

                    $where['IWSEARCHVALS' . $i] = '%' . $sch_word_sender . '%';
                }



                $tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

                $where[$tmpStr] = '';


                $sParam .= '&sch_condition_sender=' . $sch_condition_sender;

                $sParam .= '&sch_word_sender=' . $sch_word_sender;
            } else {


                // validate if condition is customer

                if ($sch_condition_sender == 'sender like') {

                    $sql = mysql_query("SELECT iw_deposit.sender FROM iw_deposit WHERE sender='" . $sch_word_sender . "'");

                    while ($row = mysql_fetch_array($sql)) {
                        $sch_word_sender = $row['sender'];
                        $sch_condition_sender = 'sender =';
                    }mysql_free_result($sql);
                }

                if ($sch_condition_sender == 'sender like') {

                    $sch_word = 'N';

                    $sch_condition_sender = 'sender like';
                }
                if (($sch_condition_sender != 'sender =') || ($sch_condition_sender != 'sender like')) {

                    $where[$sch_condition_sender] = '%' . $sch_word_sender . '%';
                } else {

                    $where[$sch_condition_sender] = '%' . $sch_word_sender . '%';
                }

                $sParam .= '&sch_condition=' . $sch_condition;

                $sParam .= '&sch_word=' . $sch_word;
            }
        }


        //icon_status search

        $status_condition = $this->ci->input->get_post('status_condition');

        if ($status_condition != '') {

            $sParam .= '&status_condition=' . $status_condition;

            $where['icon_status = '] = $status_condition;
          
        }

        /* var_dump($where);
          var_dump($status_condition); */


        //체크박스 조건
        $sch_checkbox = array();
        $sch_checkbox = ($this->ci->input->get_post('sch_checkbox'));

        if ($sch_checkbox != '') {
            //var_dump(count($sch_checkbox));
            //var_dump($_POST); exit();

            if (count($sch_checkbox) > 0) {
                $tmpStr = '';

                for ($i = 0; $i < count($sch_checkbox); $i++) {
                    if ($tmpStr != '') {

                        $tmpStr .= ' or ';
                    }
                    //echo $sch_checkbox[$i];
                    $chk_condition = $sch_checkbox[$i];

                    //$tmpStr .= $chk_condition[$i] . ' ? ';

                    $where[$chk_condition] = 'Y';

                    $sParam .= '&' . $chk_condition . 'Y';
                }

                //var_dump($where);
                //var_dump($tmpStr); exit();
                //var_dump($where[$chk_condition]); exit();
            }
        }


        

        //페이지 조건

        $cur_page = $this->ci->input->get_post("cur_page");

        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;

        $page_rows = (integer) func_get_config($this->bbs_config, 'page_rows');    //페이지 로우수
        //var_dump($page_rows); exit();

        $page_views = (integer) func_get_config($this->bbs_config, 'page_views');   //페이지 네비게이션 수
        //게시판 공지 목록 조회

        //$bbs_notice_list = $this->ci->bbs->adm_select($where, ($cur_page - 1) * $page_rows, $page_rows);



        //게시판 일반 목록 조회

        $bbs_list = $this->ci->bbs->adm_select($where, ($cur_page - 1) * $page_rows, $page_rows,  $order);

        //var_dump($bbs_list); exit();

        $result_total_rows = $this->ci->bbs->adm_total_select($where);
        $total_rows = $result_total_rows['0']->num_rows;
        $row_cnt = $total_rows - ($cur_page - 1) * $page_rows;      //글 번호
        //페이지정보 조회

        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



        //이전글 다음글 조회

        // if ($prev_next == 'Y') {

        //     $pn_data = array();

        //     $pn_data['isPrev'] = '';



        //     $prev = ($cur_page - 1) * $page_rows;



        //     if ($prev > 0) {

        //         $pn_data['isPrev'] = "PREV";

        //         $prev = $prev - 1;
        //     }



        //     $pn_data['cur_page'] = $cur_page;

        //     $pn_data['page_views'] = $page_views;

        //     $pn_data['page_rows'] = $page_rows;

        //     $pn_data['total_rows'] = $total_rows;

        //     $pn_data['row_cnt'] = $row_cnt;



        //     if ($pn_data['isPrev'] == 'PREV') {

        //         $page_rows = $page_rows + 2;
        //     } else {

        //         $page_rows = $page_rows + 1;
        //     }

        //     $pn_data['prevList'] = $this->ci->bbs->select($this->mcd, 'N', $prev, $page_rows, $where)->result();



        //     return $pn_data;
        // }



        func_set_data($this->ci, 'total_rows', $total_rows);     //조회 전체 목록수

        func_set_data($this->ci, 'total_page', $page_info[0]);   //전체 페이지

        func_set_data($this->ci, 'row_cnt', $row_cnt);     //글번호

        func_set_data($this->ci, 'cur_page', $cur_page);      //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);       //검색 string

        //func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list);

        func_set_data($this->ci, 'bbs_list', $bbs_list);

        func_set_data($this->ci, 'page_info', $page_info);

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/list', $this->ci->data, true));




        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    
    }

    //게시판 상세 조회

    function adm_bbs_detail() {

        $idx = $this->ci->input->get_post('idx');
        //echo $this->ci->mo_shipping->get_total_price(1, $idx);
        $cur_page = $this->ci->input->get_post('cur_page');

        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;

        $sParam = $this->ci->input->get_post('sParam');    //검색 string

        $bbs_detail_list = $this->ci->bbs->select_detail($idx, $this->mcd);   //상세 조회
        $bbs_detail_list_get_owner=$this->ci->bbs->select_owner_list();

        $grade_no_session = $_SESSION['ADMIN']['session_grade_no'];

        if($grade_no_session == 11) {

            $car_owner = $this->ci->user_member->iscar_owner($idx);

            if($car_owner[0]->num_row == 0){

                redirect('/?c=admin', 'refresh');
            }
            
        }

        $where['stock_idx'] = $idx;
        $count_order = $this->ci->customer->adm_count_order($where);
        func_set_data($this->ci, "count_order", $count_order);

        $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);   //첨부파일 조회

        $this->ci->bbs->update_visit_cnt($idx, $this->mcd);       //조회수 업데이트
        //코멘트 사용 여부

        $bbs_comment_list = '';

        if (func_get_config($this->bbs_config, 'comment_yn') == 'Y') {

            $bbs_comment_list = $this->ci->bbs->select_comment($idx, $this->mcd);   //코멘트 조회

            func_set_data($this->ci, 'bbs_comment_list', $bbs_comment_list->result());
        }

        //이전글 다음글 끝
        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

            $this->ci->load->model('mo_survey', 'survey'); // 설문조사



            $SurveyCfg = $this->ci->survey->config($idx, $this->mcd);

            $SurveyResult = $this->ci->survey->select($idx, $this->mcd);



            func_set_data($this->ci, 'survey_result', $SurveyResult);

            func_set_data($this->ci, 'survey', $SurveyCfg);
        }
        $select_bbs_cloud_list = $this->ci->mo_file->select_bbs_cloud($idx);

        $bbs_attach_files = $this->ci->bbs->select_attach_files($idx);   //첨부파일 조회

        func_set_data($this->ci, 'select_bbs_cloud_list', $select_bbs_cloud_list->result());

        func_set_data($this->ci, 'idx', $idx);

        func_set_data($this->ci, 'cur_page', $cur_page);      //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);       //검색 string

        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);
        func_set_data($this->ci, 'bbs_detail_list_get_owner', $bbs_detail_list_get_owner);

        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list->result());

        func_set_data($this->ci, 'bbs_attach_files', $bbs_attach_files->result());

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/view', $this->ci->data, true));

        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }

    //게시판 등록 HTML

    function adm_bbs_write() {
        $this->ci->load->helper('url');
        //func_set_data($this->ci, 'domain_name', base_url());      //현
        /* Test */
        $insert_id = $this->ci->bbs->insert_draft();
        ob_clean();
        header("Location: /?c=admin&m=adm_bbs_modify&mcd=product&category=offer&create_mode=new&idx=".$insert_id);
        // func_set_data($this->ci, 'idx', $insert_id);
        // /* End Test */
        // func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/write', $this->ci->data, true));

        // $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }
    
    function buyer_product(){  
         $order_by = $this->ci->input->get_post("order_by");
         $order_d = $this->ci->input->get_post("order_d");
         $member_log=$_SESSION['ADMIN']['member_no'];
         $order='icon_status ASC, created_dt DESC';
         if(!empty($order_by)){
            $order = $order_by." ".$order_d;
        }
         $where = array();
          $sParam = ''; 
         $cur_page = $this->ci->input->get_post("cur_page");
         if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;
        $page_rows = (integer) func_get_config($this->bbs_config, 'page_rows');
        $page_views = (integer) func_get_config($this->bbs_config, 'page_views'); 
        $sch_create_dt_s = $this->ci->input->get_post('sch_create_dt_s');
        $sch_create_dt_e = $this->ci->input->get_post('sch_create_dt_e');
        
        
        if(!empty($sch_create_dt_s)){
           $where['reserved_dt >='] = $sch_create_dt_s; 
        }
        if(!empty($sch_create_dt_e)){
           $where['reserved_dt <='] = $sch_create_dt_e; 
        }
        
        $sch_icon_status = $this->ci->input->get_post('sch_icon_status');
        if ($sch_icon_status) {
            $where['icon_status'] = $sch_icon_status;
        }
        
        
        
         $sch_condition = $this->ci->input->get_post('sch_condition');     //검색조건

        $sch_word = $this->ci->input->get_post('sch_word');    //검색어

		
		
		 if($sch_condition == ''){
            $space = substr_count($sch_word, ' ');
            if($space == 2){
    		 	$sch_word = trim($sch_word);
    		  	$result = explode(' ', $sch_word, 3);
    			$where["car_make like"] = '%' . $result[1] . '%';
    			$where["car_model like"] = '%' . $result[2] . '%';
    			$where["car_model_year like"] = '%' . $result[0] . '%';
            }
         }


        if ($sch_condition != '' && $sch_word != '') {

            $sch_condition_tmp = explode('|', $sch_condition);


            //다중 조건인 경우(제목 + 내용)

            if (count($sch_condition_tmp) > 1) {

                $tmpStr = '';

                for ($i = 0; $i < count($sch_condition_tmp); $i++) {

                    if ($tmpStr != '') {

                        $tmpStr .= ' or ';
                    }



                    $tmpStr .= $sch_condition_tmp[$i] . ' ? ';

                    $where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';
                }



                $tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

                $where[$tmpStr] = '';


                $sParam .= '&sch_condition=' . $sch_condition;

                $sParam .= '&sch_word=' . $sch_word;
            } else {



                if ($sch_condition == 'customer_name like') {

                    $sql = mysql_query("SELECT iw_customer.customer_no FROM iw_customer WHERE customer_name='" . $sch_word . "'");

                    while ($row = mysql_fetch_array($sql)) {
                        $sch_word = $row['customer_no'];
                        $sch_condition = 'customer_no =';
                    }mysql_free_result($sql);
                }

                if ($sch_condition == 'customer_name like') {

                    $sch_word = 'N';

                    $sch_condition = 'customer_no like';
                }
                if (($sch_condition != 'customer_no =') || ($sch_condition != 'customer_name like')) {

                    $where[$sch_condition] = '%' . $sch_word . '%';
                } else {

                    $where[$sch_condition] = '%' . $sch_word . '%';
                }

                $sParam .= '&sch_condition=' . $sch_condition;

                $sParam .= '&sch_word=' . $sch_word;
            }
        }


        //icon_status search

        $status_condition = $this->ci->input->get_post('status_condition');

        if ($status_condition != '') {

            $sParam .= '&status_condition=' . $status_condition;

            $where['icon_status = '] = $status_condition;
          
        }
        
        $buyer_bbs_list=  $this->ci->bbs->adm_buyer_select($where, ($cur_page - 1) * $page_rows, $page_rows,$order,$member_log);
        func_set_data($this->ci,'buyer_bbs_list',$buyer_bbs_list);
        $result_total_rows = $this->ci->bbs->adm_buyer_total_select($where,$member_log);
        $total_rows = $result_total_rows['0']->num_rows;
        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);
        func_set_data($this->ci, 'total_rows', $total_rows);
        func_set_data($this->ci, 'total_page', $page_info[0]);
        func_set_data($this->ci, 'cur_page', $cur_page); 
        func_set_data($this->ci, 'sParam', $sParam); 
        func_set_data($this->ci, 'page_info', $page_info);
        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/product/' . $this->mcd . '/buyer_bbs', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false); 
    }
    
    
    function buyer_bbs_info(){
        $member_log=$_SESSION['ADMIN']['member_no'];
        $idx = $this->ci->input->get_post('idx');
        $select_bbs_cloud_list_buyer=$this->ci->mo_file->select_bbs_cloud($idx);
        $bbs_info_buyer_detail=$this->ci->bbs->select_detail_for_buyer($idx, $this->mcd,$member_log);
        $bbs_attach_files = $this->ci->bbs->select_attach_files($idx);
        $bbs_detail_list_get_owner=$this->ci->bbs->select_owner_list();
        func_set_data($this->ci, 'bbs_detail_list_get_owner', $bbs_detail_list_get_owner);
        func_set_data($this->ci, "bbs_info_buyer_detail",$bbs_info_buyer_detail);
        func_set_data($this->ci, "select_bbs_cloud_list_buyer",$select_bbs_cloud_list_buyer->result());
        func_set_data($this->ci, 'bbs_attach_files', $bbs_attach_files->result());
        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/product/' . $this->mcd . '/buyer_bbs_info', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }


    
    
    function make_list(){
        $where=array();
        $sParam = '';
        $cur_page = $this->ci->input->get_post("cur_page");
         if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;
        $page_rows = (integer) func_get_config($this->bbs_config, 'page_rows');
        $page_views = (integer) func_get_config($this->bbs_config, 'page_views'); 
        
        $adm_make_select=$this->ci->bbs->adm_make_select($where, ($cur_page - 1) * $page_rows, $page_rows);
        $result_total_rows = $this->ci->bbs->get_total_make($where);
        $total_rows = $result_total_rows['0']->num_rows;
        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);
        func_set_data($this->ci, 'total_rows', $total_rows);
        func_set_data($this->ci, 'total_page', $page_info[0]);
        func_set_data($this->ci, 'cur_page', $cur_page); 
        func_set_data($this->ci, 'sParam', $sParam); 
        func_set_data($this->ci, 'page_info', $page_info);
        func_set_data($this->ci, 'adm_make_select',$adm_make_select);
        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/make/' . $this->mcd . '/make_list', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }
    
    function adm_json_make(){
         $json = array();
        $num_row=4;
        $where=array();
        $offset = $this->ci->input->get_post('offset');
        if(empty($offset)){
            $offset = 0;
        }
        $total_num_rows = $this->ci->bbs->get_total_make($where);
        $adm_make=$this->ci->bbs->adm_make_select($where,$num_row,$offset);
        foreach($adm_make as $key=>$value){
            $make_name = '';
            if(!empty($value->icon_name)){
                $make_name.= $value->icon_name;
            }
            if(!empty($value->make_name)){
                $make_name.= " ".$value->make_name;
            }
             if(!empty($value->model_name)){
                $make_name.= " ".$value->model_name;
            }
           
            $adm_make[$key]->make_name = $make_name;
        }
        $json['adm_make'] = $adm_make;
        $json['total_num_rows'] = $total_num_rows;
        $json['current_num_rows'] = count($adm_make)+$offset;
        echo json_encode($json); 
       
    }
    
    function adm_make_detail(){
        
    }
    function add_make(){
        $member_no= $_SESSION['ADMIN']['member_no'];       
        $this->_add_make($member_no, 'file');
      
    }       
    function _add_make($member_no,$element_file_name){
        $success_msg = ''; 
        if($this->ci->input->post('submit')){

            $batch_data = array();

            for ($i = 0; $i < count($_FILES[$element_file_name]['name']); $i++) {

                $errors= array();
                $file_size =$_FILES[$element_file_name]['size'][$i];
                $file_tmp =$_FILES[$element_file_name]['tmp_name'][$i];

                $file_ext=strtolower(end(explode('.',$_FILES[$element_file_name]['name'][$i])));
                $expensions= array("jpeg","jpg","png");
              
                if(in_array($file_ext,$expensions)=== false){
                    $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }
              
                $make = strtoupper($this->ci->input->get_post('make_name'));
                $file_name = strtolower($make);
                $new_file_name = substr($file_name, 0, 2).'.'.$file_ext; 

                if(empty($errors)==true){

                    move_uploaded_file($file_tmp,"images/user/compo/brand/".$new_file_name);
                    $icon_name = "images/user/compo/brand/".$new_file_name;
                    $data['public_id']     = '';
                    $data['secure_url']    = '/'.$icon_name;
                    $batch_data[] = $data;

                    if($file_ext=='jpg' && $file_size > 5000 || $file_ext=='jpeg' && $file_size > 5000 || $file_ext=='png' && $file_size > 5000){

                        $resizeObj = new ImageManipulator($icon_name); 
                        $resizeObj -> resize(40, 28);
                        $resizeObj -> save($icon_name); 
                    }

                }
                else{
                    print_r($errors);
                }          
        
                $make_name=strtoupper($this->ci->input->get_post('make_name'));
               
                if($this->ci->bbs->insert_make($batch_data,$make_name)) {
                  $success_msg = 'You have successfully added a make car.';
                }

            }
        }

        func_set_data($this->ci, 'success_msg', $success_msg);
        $this->ci->load->view('admin/make/add_make', $this->ci->data, false);
       
    }
    
    function adm_make_delete(){
        
    $idx = $this->ci->input->get_post('idx');
    if (!empty($idx)) { 
        foreach ($idx as $id) {
        /** SELECT MAKE ICON PATH **/
        $select_make = $this->ci->bbs->select_make_icon_name($id);
            /** COUNT MAKE DETAIL **/
            if(count($select_make)>0){
                foreach ($select_make as $rows){
                    /** DELETE MAKE IMAGE **/
                    @unlink($_SERVER["DOCUMENT_ROOT"].$rows->icon_name);
                }
                /** DELETE MAKE DATA **/
                $this->ci->bbs->adm_make_delete($id);
            }     
        }
    }  

    redirect('/?c=admin&m=make_list', 'refresh');

    }
  
    function adm_make_unlik($id){
        $select_make =$this->ci->bbs->select_make_icon_name($id);
        if(count($select_make)>0){
            foreach ($select_make as $id_make){
                @unlink($_SERVER["DOCUMENT_ROOT"].$id_make->icon_name);
            }       
        }   
    }
          
   function adm_change_make(){
     $id = $this->ci->input->get_post('id');
     $member= $_SESSION['ADMIN']['member_no'];       
     $this->_adm_change_make($member, 'file');
     $make_change=$this->ci->user_bbs->make_select_id($id);
     func_set_data($this->ci, 'make_change',$make_change);
     $this->ci->load->view('admin/make/make_modify', $this->ci->data, false); 
   }

   function _adm_change_make($member,$element_file_name){
     $id = $this->ci->input->get_post('id');
     if(isset($_FILES[$element_file_name])){
           if (!empty($_FILES[$element_file_name]['name'])) {
                
                $errors= array();
                $file_size =$_FILES[$element_file_name]['size'];
                $file_tmp =$_FILES[$element_file_name]['tmp_name'];
                $file_ext=strtolower(end(explode('.',$_FILES[$element_file_name]['name'])));

                $expensions= array("jpeg","jpg","png");
              
                if(in_array($file_ext,$expensions)=== false){
                    $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }
              
                $make = strtoupper($this->ci->input->get_post('make_name'));
                $file_name = strtolower($make);
                $new_file_name = substr($file_name, 0, 2).'.'.$file_ext;

                if(empty($errors)==true){

                    move_uploaded_file($file_tmp,"images/user/compo/brand/".$new_file_name);
                    $icon_name = "images/user/compo/brand/".$new_file_name;

                    $datas = array('make_name'=>$make,'icon_name'=>'/'.$icon_name);

                    if($file_ext=='jpg' && $file_size > 5000 || $file_ext=='jpeg' && $file_size > 5000 || $file_ext=='png' && $file_size > 5000){

                        $resizeObj = new ImageManipulator($icon_name); 
                        $resizeObj -> resize(40, 28);
                        $resizeObj -> save($icon_name); 
                    }

                    $this->adm_make_unlik($id);

                    $updatemake =$this->ci->user_bbs->update_make($id,$datas);
                 
                    return $updatemake;

                }
                else{
                    print_r($errors);
                }

            }else{
                $this->update_make_name();
                return false;
            }
        }else{
            return false;
        }
     
     $make_change=$this->ci->user_bbs->make_select_id($id);
     func_set_data($this->ci, 'make_change',$make_change);
     $this->ci->load->view('admin/make/make_modify', $this->ci->data, false); 
 }
         
 function update_make_name(){
     $id = $this->ci->input->get_post('id');
     $make= strtoupper($this->ci->input->get_post('make_name'));
     $data1 = array('make_name'=>$make);
     //$oldmake=$this->ci->bbs->sqliteGetMake($id);                
     $this->ci->user_bbs->update_make($id,$data1);
     //$this->updateMake($oldmake,$make,null);
                
 }
         
        function delete_file_image() {
        $idx = $this->ci->input->get_post('bbs_idx');
        $file_type = $this->ci->input->get_post('file_type');

        $delete_file_image = $this->ci->bbs->select_attach_files($idx, $file_type);   //첨부파일 상세 조회
        //업로드 실제 첨부파일 삭제

        if ($delete_file_image->num_rows() > 0) {

            foreach ($delete_file_image->result() as $rows) {

                @unlink($rows->full_path);
            }
        }
        $this->ci->bbs->delete_file_image($idx, $file_type);

        echo ('File has been deleted.');
    }
    
    
    function model_list(){
        $where=array();
        $sParam = '';
        $cur_page = $this->ci->input->get_post("cur_page");
         if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;
        $page_rows = (integer) func_get_config($this->bbs_config, 'page_rows');
        $page_views = (integer) func_get_config($this->bbs_config, 'page_views'); 
        
         $select_make_seach = $this->ci->input->get_post('select_make');
          $sch_word =strtoupper($this->ci->input->get_post('sch_word')); 
          if ($select_make_seach != '') {
            $where['make_name = '] =$select_make_seach; 
        }
          if($sch_word !=''){
              $where['model_name like']='%'.$sch_word.'%';
          }
         
        $select_make_for_model=$this->ci->bbs->select_make_for_model();
        $adm_model_select=$this->ci->bbs->adm_model_select($where, ($cur_page - 1) * $page_rows, $page_rows);
        $result_total_rows = $this->ci->bbs->get_total_model($where);
        $total_rows = $result_total_rows['0']->num_rows;
        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);
        func_set_data($this->ci, 'total_rows', $total_rows);
        func_set_data($this->ci, 'total_page', $page_info[0]);
        func_set_data($this->ci, 'cur_page', $cur_page); 
        func_set_data($this->ci, 'sParam', $sParam); 
        func_set_data($this->ci, 'page_info', $page_info);
        func_set_data($this->ci, 'select_make_for_model', $select_make_for_model);
        func_set_data($this->ci, 'adm_model_select',$adm_model_select);
        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/model/' . $this->mcd . '/model_list', $this->ci->data, true));
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }
    
    function add_model(){
         $success_msg ='';
        if($this->ci->input->post('submit')){
            $make=  $this->ci->input->post('select_make');
            $model=strtoupper($this->ci->input->post('model_name'));
            $data=array('make_id'=>$make,'model_name'=>$model);
            if($this->ci->bbs->insert_model($data)){
                $success_msg = 'Model was added.';  
            }
        }
        $select_make_for_model=$this->ci->bbs->select_make_for_model();
        func_set_data($this->ci, 'select_make_for_model', $select_make_for_model);
        func_set_data($this->ci, 'success_msg', $success_msg);
        $this->ci->load->view('admin/model/add_model', $this->ci->data, false);
    }
    function adm_change_model(){
       $id = $this->ci->input->get_post('id');
         if($this->ci->input->post('change_make')){
           $make_id= $this->ci->input->get_post('select_make');
           $model=  strtoupper($this->ci->input->get_post('model_name'));
           $data = array('make_id'=>$make_id,'model_name'=>$model);
           if($this->ci->bbs->update_model($id,$data));
           
         }
       $select_make_for_model=$this->ci->bbs->select_make_for_model();
       $select_model_id=$this->ci->bbs->select_model_id($id);
       func_set_data($this->ci, 'select_model_id',$select_model_id);
       func_set_data($this->ci, 'select_make_for_model', $select_make_for_model);
       $this->ci->load->view('admin/model/model_modify', $this->ci->data, false);
    }
    
    
     function add_delete_model(){
       
       $idxs = $this->ci->input->get_post('idx');
        foreach ($idxs as $key => $value) {
        $idx = $value;
        if ($idx != '') {  
          $this->ci->bbs->add_delete_model($idx);
               
        }
     } 
     redirect('/?c=admin&m=model_list', 'refresh');
  }
    

    //게시판 등록 처리
//     function adm_bbs_write_exec() {
//         $idx = $this->ci->input->get_post('idx');

//         for ($i = 0; $i < 5; $i++) {

//             //선택된 첨부 파일만...
            
//             if ($_FILES['file_type_sheet' . $i]['tmp_name'][0] != '') {
//                 $file_type = 'inspect';
//                 if ($i == 1) {
//                     $file_type = 'export';
//                 } elseif ($i == 2) {
//                     $file_type = 'cancel';
//                 } elseif ($i == 3) {
//                     $file_type = 'register';
//                 } elseif ($i == 4) {
//                     $file_type = 'original';
//                 }
//                 //======= create delete image========            

//                 $bbs_attach_detail_list = $this->ci->bbs->select_attach_files($idx, $file_type);   //첨부파일 상세 조회
//                 //업로드 실제 첨부파일 삭제

//                 if ($bbs_attach_detail_list->num_rows() > 0) {

//                     foreach ($bbs_attach_detail_list->result() as $rows) {

//                         //첨부파일이 이미지인 경우 생성된 썸네일 까지 삭제

//                         $this->ci->bbs->delete_attach_file($rows->id, $idx);   //첨부파일 삭제

//                         @unlink($rows->full_path);
//                     }
//                 }


//                 //파일 업로드

//                 $upload_str_tr = $this->ci->iwc_common->upload('file_type_sheet' . $i, 0, 'carItem', $this->bbs_config);



//                 //파일 업로드 실패

//                 if ($upload_str_tr != TRUE) {

//                     show_error($this->ci->iwc_common->upload_error_msg());
//                 }

//                 //파일 업로드 성공
//                 else {

//                     $upload_file_info_car = array();

//                     $upload_file_info_car = $this->ci->iwc_common->upload_success_msg();

//                     if ($_FILES['file_type_sheet0']['tmp_name'][0] != '' && $i == 0 || $i == 0) {
//                         $upload_file_info_car['file_type'] = "inspect";
//                     }
//                     if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 1 || $i == 1) {
//                         $upload_file_info_car['file_type'] = "export";
//                     }
//                     if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 2 || $i == 2) {
//                         $upload_file_info_car['file_type'] = "cancel";
//                     }
//                     if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 3 || $i == 3) {
//                         $upload_file_info_car['file_type'] = "register";
//                     }
//                     if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 4 || $i == 4) {

//                         $upload_file_info_car['file_type'] = "original";
//                     }

//                     $upload_file_info_car['bbs_idx'] = $idx;

//                     $upload_file_info_car['created_dt'] = func_get_date_time();
                    
//                     $this->ci->bbs->add_detail_car($upload_file_info_car);
//                 }
//             }
//         }

//         $num_imageUpload = 0;

//         $idx_image = array();

//         $id_array = 0;



//         if (is_array($idx)) {

//             $idx = $idx[0];
//         }

//         $bbs_detail_list = $this->ci->bbs->select_detail($idx, $this->mcd);   //상세 조회

//         $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);   //첨부파일 조회

//         if (count($bbs_attach_list) > 0) {

//             foreach ($bbs_attach_list->result() as $att_rows) {

//                 $idx_image[$id_array][0] = $att_rows->att_idx;
//             }
//         }

//         $attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn');   //첨부파일 사용여부

//         $upload_str = '';

//         // count image upload
//         //$num_imageUpload = count($_FILES['attach1']['name']);
//         //첨부파일을 사용한다면...



//         $inData = array();

//         $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴

//         $inData['idx'] = $idx;    //게시물 일련번호

//         $inData['menu_code'] = $this->mcd;        //메뉴코드

//         $inData['member_id'] = $this->ci->member_id;                 //아이디

//         $inData['writer_ip'] = $this->ci->input->ip_address();   //작성자 아이피
//         // 설문조사 사용할 경우

//         if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

//             // 시행일

//             $inData['publish_start'] = $inData['publish_end'] . ' 00:00:00';

//             $inData['publish_end'] = $inData['publish_end'] . ' 23:59:59';
//         }

//         //우편번호

//         if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

//             $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

//             unset($inData['zipcode1']);

//             unset($inData['zipcode2']);
//         }

//         if (!isset($inData['notice_yn'])) {        //공지여부
//             $inData['notice_yn'] = 'N';
//         }

//         if (!isset($inData['created_dt']) || ( $inData['created_dt'] == '')) {         //작성일
//             $inData['created_dt'] = func_get_date_time();
//         }

//         if (!isset($inData['car_buying_dt']) || ( $inData['car_buying_dt'] == '')) {         //작성일
//             $inData['car_buying_dt'] = func_get_date_time();
//         }

//         if ($this->ci->input->post('sch_checkbox') != "") {

//             $inData['country_options'] = func_get_post_array_checkbox($this->ci->input->post('sch_checkbox'));
//         }
//         if ($this->ci->input->post('op_checkbox') != "") {

//             $inData['car_options'] = func_get_post_array_checkbox($this->ci->input->post('op_checkbox'));
//         }

//         //DB 수정

//         $this->ci->bbs->insert($inData);

//         //Input unit_price in iw_account

//         if(isset($inData['unit_price'])||$inData['unit_price'] != '')
//         {   
//             $inAccount = array();
            
//             $max_stock_no = $this->ci->bbs->get_max_car_stock_no();

//             $inAccount['car_idx'] = $inData['idx'];
//             $inAccount['account_id'] = 1; //buying car
//             $inAccount['account_detail'] = $max_stock_no.' / '.$inData['car_model'].' / '.$inData['car_chassis_no'];
//             $inAccount['currency_type'] = $inData['unit_price_currency'];

//             $inAccount['unit_price'] = $inData['unit_price'];
//             $inAccount['unit_count'] = 1;
//             $inAccount['unit_value_price'] = $inData['unit_price'];
//             $inAccount['unit_total_price'] = $inData['unit_price'] + $inData['tax_price'];
            
//             $inAccount['tax_flg'] = 'Tax';
//             $inAccount['tax_price'] = $inData['tax_price'];
            
//             $inAccount['account_memo'] = '';
//             $inAccount['account_date'] = $inData['car_buying_dt'];
            

//             $this->ci->account->insert_account($inAccount);
//         }
        

//         // 설문조사 사용할 경우

//         if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

//             $this->ci->load->model('mo_survey', 'survey'); // 설문조사
//             // 설문조사 항목 저장

//             $Survey = $this->ci->input->get_post('survey');

//             if (isset($Survey)) {

//                 $this->ci->survey->config_insert($inData['idx'], $this->mcd, $Survey);
//             }
//         }

//         func_set_data($this->ci, 'idx', $idx);

//         func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);

//         func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list);

// //var_dump($inData);

//         echo func_jsAlertReplace('Car Information has beed added.', '?c=admin&m=adm_bbs_list&mcd=' . $this->mcd);
//     }

    // function adm_bbs_write_exec_backup() {
    //     $num_imageUpload = 0;
    //     $inData = array();
    //     $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴
    //     $inData['icon_status'] = 'sale';
    //     $inData['menu_code'] = $this->mcd;        //메뉴코드
    //     $inData['member_id'] = $this->ci->member_id;       //아이디
    //     $inData['writer_ip'] = $this->ci->input->ip_address();   //작성자 아이피
    //     $inData['visit_cnt'] = 0;           //조회수
    //     $inData['grp_idx'] = 0;           //그룹 idx
    //     $inData['order_no'] = 0;           //정렬 순서
    //     $inData['depth'] = 0;           //깊이
    //     if ($this->ci->input->post('sch_checkbox') != "") {
    //         $inData['country_options'] = func_get_post_array_checkbox($this->ci->input->post('sch_checkbox'));
    //     }
    //     if ($this->ci->input->post('op_checkbox') != "") {
    //         $inData['car_options'] = func_get_post_array_checkbox($this->ci->input->post('op_checkbox'));
    //     }
    //     // 설문조사 사용할 경우
    //     if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {
    //         // 시행일
    //         $inData['publish_start'] = $inData['publish_end'] . ' 00:00:00';
    //         $inData['publish_end'] = $inData['publish_end'] . ' 23:59:59';
    //     }
    //     //우편번호
    //     if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {
    //         $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];
    //         unset($inData['zipcode1']);
    //         unset($inData['zipcode2']);
    //     }
    //     if (!isset($inData['notice_yn'])) {        //공지여부
    //         $inData['notice_yn'] = 'N';
    //     }
    //     if (!isset($inData['created_dt']) || ($inData['created_dt'] == '')) {         //작성일
    //         $inData['created_dt'] = func_get_date_time();
    //     }
    //     //DB 등록
    //     $idx = $this->ci->bbs->insert($inData);
    //     $upload_str_tr = '';
    //     $file_type_tr = '';
    //     // count image upload
    //     //$num_imageUpload = count($_FILES['file_type_sheet0']['name']);
    //     //$file_type_export = count($_FILES['file_type_export']['name']);
    //     //첨부파일을 사용한다면...
    //     //if ($attach_file_yn != 'N') {
    //     for ($i = 0; $i < 5; $i++) {
    //         //선택된 첨부 파일만...
    //         if ($_FILES['file_type_sheet' . $i]['tmp_name'][0] != '') {
    //             //파일 업로드
    //             $upload_str_tr = $this->ci->iwc_common->upload('file_type_sheet' . $i, 0, 'carItem', $this->bbs_config);
    //             //파일 업로드 실패
    //             if ($upload_str_tr != TRUE) {
    //                 show_error($this->ci->iwc_common->upload_error_msg());
    //             }
    //             //파일 업로드 성공
    //             else {
    //                 $upload_file_info_car = array();
    //                 $upload_file_info_car = $this->ci->iwc_common->upload_success_msg();
    //                 if ($_FILES['file_type_sheet0']['tmp_name'][0] != '' && $i == 0 || $i == 0) {
    //                     $upload_file_info_car['file_type'] = "inspect";
    //                 }
    //                 if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 1 || $i == 1) {
    //                     $upload_file_info_car['file_type'] = "export";
    //                 }
    //                 if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 2 || $i == 2) {
    //                     $upload_file_info_car['file_type'] = "cancel";
    //                 }
    //                 if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 3 || $i == 3) {
    //                     $upload_file_info_car['file_type'] = "register";
    //                 }
    //                 if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 4 || $i == 4) {
    //                     $upload_file_info_car['file_type'] = "original";
    //                 }
    //                 $upload_file_info_car['bbs_idx'] = $idx;
    //                 $upload_file_info_car['created_dt'] = func_get_date_time();
    //                 $this->ci->bbs->add_detail_car($upload_file_info_car);
    //             }
    //         }
    //     }
    //     // 설문조사 사용할 경우
    //     if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {
    //         $this->ci->load->model('mo_survey', 'survey'); // 설문조사
    //         // 설문조사 항목 저장
    //         $Survey = $this->ci->input->get_post('survey');
    //         if (isset($Survey)) {
    //             $this->ci->survey->config_insert($idx, $this->mcd, $Survey);
    //         }
    //     }
    //     //첨부파일 등록
    //     $attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn');   //첨부파일 사용여부
    //     $upload_str = '';
    //     // count image upload
    //     $num_imageUpload = count($_FILES['attach1']['name']);
    //     //첨부파일을 사용한다면...
    //     if ($attach_file_yn != 'N') {
    //         for ($i = 0; $i < $num_imageUpload; $i++) {
    //             //선택된 첨부 파일만...
    //             if ($_FILES['attach1']['tmp_name'][$i] != '') {
    //                 //파일 업로드
    //                 $upload_str = $this->ci->iwc_common->upload('attach1', $i, $this->mcd, $this->bbs_config);
    //                 //파일 업로드 실패
    //                 if ($upload_str != TRUE) {
    //                     show_error($this->ci->iwc_common->upload_error_msg());
    //                 }
    //                 //파일 업로드 성공
    //                 else {
    //                     $upload_file_info = array();
    //                     $upload_file_info = $this->ci->iwc_common->upload_success_msg();
    //                     //첨부파일 DB 등록
    //                     $upload_file_info['idx'] = $idx;
    //                     $upload_file_info['menu_code'] = $this->mcd;
    //                     $this->ci->bbs->insert_attach($upload_file_info);
    //                 }
    //             }
    //         }
    //     }
    //     echo func_jsAlertReplace('Car Information has been added.', '/?c=admin&m=adm_bbs_list&mcd=product');
    // }
    //게시판 수정 HTML


    function adm_bbs_update() {

        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');

        $sParam = $this->ci->input->get_post('sParam');     //검색 string

        $only_attach = $this->ci->input->get_post('only_attach');   //첨부파일 상세 삭제 여부

        $only_survey = $this->ci->input->get_post('only_survey'); //문항 상세 삭제 여부



        $bbs_detail_list = $this->ci->bbs->select_detail($idx, $this->mcd);   //게시판 상세 조회
        //수정폼에서 첨부 파일만 삭제 할 경우....

        if ($only_attach != 'N') {

            $att_idx = $this->ci->input->get_post('att_idx');

            $bbs_attach_detail_list = $this->ci->bbs->select_attach_detail($idx, $att_idx, $this->mcd);   //첨부파일 상세 조회

            $this->ci->bbs->delete_attach($idx, $att_idx, $this->mcd);   //첨부파일 삭제
            //업로드 실제 첨부파일 삭제

            if ($bbs_attach_detail_list->num_rows() > 0) {

                foreach ($bbs_attach_detail_list->result() as $rows) {

                    //첨부파일이 이미지인 경우 생성된 썸네일 까지 삭제

                    if ($rows->is_image) {

                        @unlink($rows->full_path);   //원본 이미지 삭제

                        for ($i = 1; $i < 6; $i++) {    //썸네일 삭제
                            $thumb_path = $rows->file_path . $rows->raw_name . '_' . func_get_config($this->bbs_config, 'attach_thumbnail_size' . $i) . $rows->file_ext;

                            if (file_exists($thumb_path)) {

                                @unlink($thumb_path);
                            }
                        }
                    } else {

                        @unlink($rows->full_path);
                    }
                }
            }
        }



        $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);   //첨부파일 조회(삭제 후 조회)
        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {



            $this->ci->load->model('mo_survey', 'survey'); // 설문조사



            if ($only_survey == 'Y') { // 문항 삭제일 경우
                $survey_no = $this->ci->input->get_post('survey_no');

                $this->ci->survey->config_delete($idx, $this->mcd, $survey_no);
            }



            // 항목수 수정시

            $surveyCnt = $this->ci->input->get_post('survey_cnt');

            if (!is_numeric($surveyCnt)) {

                $surveyCnt = 0;
            }



            $SurveyCfg = $this->ci->survey->config($idx, $this->mcd, $surveyCnt);



            func_set_data($this->ci, 'survey', $SurveyCfg);
        }



        func_set_data($this->ci, 'idx', $idx);

        func_set_data($this->ci, 'cur_page', $cur_page);      //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);       //검색 string

        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);

        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list->result());

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/update', $this->ci->data, true));



        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }

    //게시판 수정 처리

    function adm_bbs_update_exec() {

        $cur_page = $this->ci->input->get_post('cur_page');

        $sParam = $this->ci->input->get_post('sParam');     //검색 string



        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴

        $inData['idx'] = $this->ci->input->get_post('idx');    //게시물 일련번호
        if ($inData['icon_status'] == 'release')
            unset($inData['icon_status']);
        $inData['menu_code'] = $this->mcd;        //메뉴코드
        $inData['member_id'] = $this->ci->member_id;             //아이디

        $inData['writer_ip'] = $this->ci->input->ip_address();   //작성자 아이피
        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

            // 시행일

            $inData['publish_start'] = $inData['publish_start'] . ' 00:00:00';

            $inData['publish_end'] = $inData['publish_end'] . ' 23:59:59';
        }



        //우편번호

        if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

            $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

            unset($inData['zipcode1']);

            unset($inData['zipcode2']);
        }
        if (isset($inData['updated_dt'])) {
            if ($inData['updated_dt'] == '') {         //작성일
                $inData['updated_dt'] = func_get_date_time();
            }
        }


        //DB 수정

        $this->ci->bbs->update($inData);


        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

            $this->ci->load->model('mo_survey', 'survey'); // 설문조사
            // 설문조사 항목 저장

            $Survey = $this->ci->input->get_post('survey');

            if (isset($Survey)) {

                $this->ci->survey->config_insert($inData['idx'], $this->mcd, $Survey);
            }
        }



        //첨부파일 등록

        $attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn');   //첨부파일 사용여부

        $upload_str = '';



        //첨부파일을 사용한다면...

        if ($attach_file_yn != 'N') {

            for ($i = 1; $i <= count($_FILES); $i++) {

                //선택된 첨부 파일만...

                if ($_FILES['attach' . $i]['tmp_name'] != '') {

                    //파일 업로드

                    $upload_str = $this->ci->iwc_common->upload('attach' . $i, $this->mcd, $this->bbs_config);



                    //파일 업로드 실패

                    if ($upload_str != TRUE) {

                        show_error($this->ci->iwc_common->upload_error_msg());
                    }

                    //파일 업로드 성공
                    else {

                        $upload_file_info = array();

                        $upload_file_info = $this->ci->iwc_common->upload_success_msg();



                        //첨부파일 DB 등록

                        $upload_file_info['idx'] = $inData['idx'];     //게시물 일련번호
                        //$upload_file_info['menu_code'] = $this->mcd;



                        $this->ci->bbs->insert_attach($upload_file_info);
                    }
                }
            }
        }



        echo func_jsAlertReplace('Modification Complete.', '/?c=admin&m=adm_bbs_list&mcd=' . $this->mcd . '&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
    }

    //게시판 답변 HTML

    function adm_bbs_reply() {

        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');

        $sParam = $this->ci->input->get_post('sParam');    //검색 string



        $bbs_detail_list = $this->ci->bbs->select_detail($idx, $this->mcd);   //상세 조회

        $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);   //첨부파일 조회



        func_set_data($this->ci, 'idx', $idx);

        func_set_data($this->ci, 'cur_page', $cur_page);      //현재 페이지

        func_set_data($this->ci, 'sParam', $sParam);       //검색 string

        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);

        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list->result());

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/reply', $this->ci->data, true));



        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }

    //게시판 답변 처리

    function adm_bbs_reply_exec() {

        $idx = $this->ci->input->get_post('idx');     //부모 idx

        $cur_page = $this->ci->input->get_post('cur_page');   //현재 페이지

        $sParam = $this->ci->input->get_post('sParam');     //검색 string
        //부모글 상세 조회

        $bbs_detail_list = $this->ci->bbs->select_detail($idx, $this->mcd);

        $par_rows = '';

        if (count($bbs_detail_list) > 0) {

            $par_rows = $bbs_detail_list->row();
        }



        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴

        $inData['menu_code'] = $this->mcd;        //메뉴코드

        $inData['member_id'] = func_decode(func_get_config($this->bbs_config, 'reply_id_yn'), 'N', $this->ci->member_id, $par_rows->member_id);    //아이디
        //$inData['member_id']  = $this->ci->member_id;                 //아이디

        $inData['writer_ip'] = $this->ci->input->ip_address();   //작성자 아이피

        $inData['visit_cnt'] = 0;           //조회수

        $inData['grp_idx'] = $par_rows->grp_idx;      //그룹 idx

        $inData['order_no'] = $par_rows->order_no + 1;    //정렬 순서

        $inData['depth'] = $par_rows->depth + 1;       //깊이
        //우편번호

        if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

            $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

            unset($inData['zipcode1']);

            unset($inData['zipcode2']);
        }



        if ($inData['created_dt'] == '') {         //작성일
            $inData['created_dt'] = func_get_date_time();
        }

        $inData['notice_yn'] = 'N';         //공지글여부(고정) - 답변등록시
        //답변 기능 설정 여부

        if (func_get_config($this->bbs_config, 'reply_yn') == 'Y') {

            $inData['reply_yn'] = 'Y';
        }



        //비밀글 설정 여부

        if (func_get_config($this->bbs_config, 'secret_yn') == 'Y') {

            //부모글의 공개 비공개 여부

            $inData['secret_yn'] = $par_rows->secret_yn;
        }



        //게시판 하위글 등록시 정렬 순서 업데이트

        $this->ci->bbs->update_order_no($par_rows->grp_idx, $par_rows->order_no);



        //DB 등록(부모글 답변 여부 'Y' 업데이트)

        $idx = $this->ci->bbs->insert_reply($inData);



        //첨부파일 등록

        $attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn');   //첨부파일 사용여부

        $upload_str = '';



        //첨부파일을 사용한다면...

        if ($attach_file_yn != 'N') {

            for ($i = 1; $i <= count($_FILES); $i++) {

                //선택된 첨부 파일만...

                if ($_FILES['attach' . $i]['tmp_name'] != '') {

                    //파일 업로드

                    $upload_str = $this->ci->iwc_common->upload('attach' . $i, $this->mcd, $this->bbs_config);



                    //파일 업로드 실패

                    if ($upload_str != TRUE) {

                        show_error($this->ci->iwc_common->upload_error_msg());
                    }

                    //파일 업로드 성공
                    else {

                        $upload_file_info = array();

                        $upload_file_info = $this->ci->iwc_common->upload_success_msg();



                        //첨부파일 DB 등록

                        $upload_file_info['idx'] = $idx;

                        $upload_file_info['menu_code'] = $this->mcd;



                        $this->ci->bbs->insert_attach($upload_file_info);
                    }
                }
            }
        }



        //관리자 답변 이메일 사용 여부

        if (func_get_config($this->bbs_config, 'admin_email_yn') == 'Y') {

            $this->ci->load->library('sendmail');    //메일



            func_set_data($this->ci, 'http_url', $this->ci->config->item('base_url'));



            //부모글 $par_rows

            func_set_data($this->ci, 'par_subject', $par_rows->subject);

            func_set_data($this->ci, 'par_writer', $par_rows->writer);

            func_set_data($this->ci, 'email', $par_rows->email);

            func_set_data($this->ci, 'par_created_dt', func_get_date_format($par_rows->created_dt, 'Y-m-d'));

            func_set_data($this->ci, 'par_contents', nl2br($par_rows->contents));



            //답변글

            func_set_data($this->ci, 'subject', $inData['subject']);

            func_set_data($this->ci, 'writer', $inData['writer']);

            func_set_data($this->ci, 'created_dt', func_get_date_format($inData['created_dt'], 'Y-m-d'));

            func_set_data($this->ci, 'contents', nl2br($inData['contents']));



            $send_mail = func_get_config($this->bbs_config, 'admin_email_addr');   //보내는 메일

            $send_name = func_get_config($this->bbs_config, 'admin_email_name');   //보내는 사람

            $to_mail = $par_rows->email;     //받는 이메일

            $subject = $inData['subject'];   //제목
            //이메일 본문

            $contents = $this->ci->load->view("common/admin_reply_email", $this->ci->data, true);



            //답변메일

            $this->ci->sendmail->clear(TRUE);     //모든 이메일 변수들을 초기화  TRUE 시 첨부파일도 초기화

            $this->ci->sendmail->setMailType("html");

            $this->ci->sendmail->setInit();

            $this->ci->sendmail->setFrom($send_mail, $send_name);

            $this->ci->sendmail->setTo($to_mail);

            $this->ci->sendmail->setSubject($subject);

            $this->ci->sendmail->setMessage($contents);

            $mail_result = $this->ci->sendmail->send();   //전송여부 TRUE FALSE 반환
            //echo $this->ci->sendmail->printDebugger();
        }

        //관리자 답변 SMS 사용 여부

        if (func_get_config($this->bbs_config, 'admin_sms_yn') == 'Y') {



            $this->ci->load->library('sendsms');   //SMS



            $callphone1 = $par_rows->mobile_no1;   //호출번호 EX)"011"

            $callphone2 = $par_rows->mobile_no2;   //"234"

            $callphone3 = $par_rows->mobile_no3;   //"5678"

            $callmessage = $inData['subject'];     //80Byte

            $rdate = "00000000";       //예약 날짜 EX) "20030617" 즉시 전송시 "00000000"

            $rtime = "000000";       //예약 시간 EX) "190000"    즉시 전송시 "000000"

            $reqphone1 = func_get_config($this->bbs_config, 'admin_mobile_no1');    //회신번호 EX) "011"

            $reqphone2 = func_get_config($this->bbs_config, 'admin_mobile_no2');    //회신번호 EX) "1111"

            $reqphone3 = func_get_config($this->bbs_config, 'admin_mobile_no3');   //회신번호 EX) "1111"

            $callname = $inData['writer'];    //호출명



            if ($reqphone1 != '' && $reqphone2 != '' && $reqphone3 != '') {



                $sms_result = $this->ci->sendsms->sms_surem($callphone1, $callphone2, $callphone3, $callmessage, $rdate, $rtime, $reqphone1, $reqphone2, $reqphone3, $callname);



                if ($sms_result) {

                    //SMS 발송 정보 DB 등록

                    $sms_data = array();

                    $sms_data['menu_code'] = $this->mcd;

                    $sms_data['menu_code_name'] = func_get_config($this->bbs_config, 'bbs_code_name');

                    $sms_data['call_phone'] = $callphone1 . '-' . $callphone2 . '-' . $callphone3;

                    $sms_data['req_phone'] = $reqphone1 . '-' . $reqphone2 . '-' . $reqphone3;

                    $sms_data['message'] = $inData['subject'];

                    $sms_data['result_code'] = $sms_result;

                    $sms_data['created_dt'] = func_get_date_time();



                    $this->ci->bbs->insert_sms($sms_data);
                }
            }
        }



        echo func_jsAlertReplace('답변이 등록 되었습니다.', '/?c=admin&m=adm_bbs_list&mcd=' . $this->mcd . '&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
    }

    //게시판 삭제

    function adm_bbs_delete() {

        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');

        $sParam = $this->ci->input->get_post('sParam');     //검색 string



        $bbs_detail_list = $this->ci->bbs->select_detail($idx, $this->mcd);   //상세 조회

        $par_rows = '';

        if ($bbs_detail_list->num_rows() > 0) {

            $par_rows = $bbs_detail_list->row();
        }



        $child_list = $this->ci->bbs->delete_child_yn($par_rows->grp_idx, $this->mcd, $par_rows->depth);   //하위글 조회

        $child_rows = '';



        //해당 글의 하위 글이 있으면...

        if ($child_list->num_rows() > 0) {

            $child_rows = $child_list->row();



            if ($child_rows->depth > $par_rows->depth) {

                echo func_jsAlertReplace('하위글이 존재하여 삭제할 수 없습니다.', '/?c=admin&m=adm_bbs_list&mcd=' . $this->mcd . '&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));

                return;
            }
        }

        $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);   //첨부파일 조회

        $this->ci->bbs->delete($idx, $this->mcd);           //게시판 삭제(첨부파일 DB 자동삭제)
        //업로드 실제 첨부파일 삭제

        if ($bbs_attach_list->num_rows() > 0) {

            foreach ($bbs_attach_list->result() as $rows) {

                //첨부파일이 이미지인 경우 생성된 썸네일 까지 삭제

                if ($rows->is_image) {

                    @unlink($rows->full_path);   //원본 이미지 삭제

                    for ($i = 1; $i < 6; $i++) {    //썸네일 삭제
                        $thumb_path = $rows->file_path . $rows->raw_name . '_' . func_get_config($this->bbs_config, 'attach_thumbnail_size' . $i) . $rows->file_ext;

                        if (file_exists($thumb_path)) {

                            @unlink($thumb_path);
                        }
                    }
                } else {

                    @unlink($rows->full_path);
                }
            }
        }



        echo func_jsAlertReplace('삭제되었습니다.', '/?c=admin&m=adm_bbs_list&mcd=' . $this->mcd . '&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
    }
    //New alternative to adm_bbs_excel_exec
    function adm_bbs_export_excel(){
        /***YOU CAN SET CONDITION WHERE HERE***/
        $where = array();
        $input['created_dt >='] = $this->ci->input->get_post('sch_create_dt_s');
        $input['created_dt <='] = $this->ci->input->get_post('sch_create_dt_e');
        $input['icon_status'] = $this->ci->input->get_post('sch_icon_status');
        $search_key = $this->ci->input->get_post('sch_condition');
        $input[$search_key] = $this->ci->input->get_post('sch_word');

        $order_by = $this->ci->input->get_post("order_by");
        $order_d = $this->ci->input->get_post("order_d");
        $order='ORDER BY icon_status ASC, created_dt DESC';
        if(!empty($order_by)){
            $order = "ORDER BY ".$order_by." ".$order_d;
        }
        /************************************/
        /**************DO NOT CHANGE*********/
        foreach($input as $key=>$value){
            if(!empty($value)){
                $where[$key]=$value;
            }
        }
        $rows=$this->ci->bbs->adm_select_export($where, $order);
        /************************************/
        $today_date = date("Y/m/d");
        //다음 3줄은 엑셀파일로 변화하는 소스이다.
        //http://iblau.com/?c=admin&m=adm_bbs_excel_exec&mcd=product
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=excel_(" . $today_date . ").xls");
        header("Content-Description: PHP4 Generated Data");
        //$idx = $this->ci->input->get_post('idx');
        

        echo "
                <table border='1'>
                    <tr style='background:#FFFF99';>
                        <td>STOCKNO</td>
                        <td>STATUS</td>
                        <td>CHASSISNO</td>
                        <td><center>CAR</center></td>
                        <td>MODEL YEAR</td>
                        <td>FIRST REGISTRATION</td>
                        <td>MANUFACTURED YEAR</td>
                        <td>GRADE</td>
                        <td>FUEL</td>
                        <td>CC</td>
                        <td>SHIFT</td>
                        <td>COLOR</td>
                        <td>DRIVE</td>
                        <td>FOB</td>
                        <td>IMAGELINK</td>

                    </tr>
                ";
        foreach($rows as $row){
            $total_sell_price = $row->car_actual_price + $row->car_freight_fee;
            $date = date_create($row->created_dt);
            $date2 = date_create($row->port_leaved_date);
            $created_dt_format = date_format($date, "Y-m-d");
            $port_leaved_date_format = date_format($date2, "Y-m-d");
            if($total_sell_price > 0){
                $deposit_percent = ($row->allocate_sum / $total_sell_price ) * 100;
            }else{
                $deposit_percent = 0;
            }

            if($row->first_registration_month==0||$row->first_registration_month == Null||$row->first_registration_month == ''){
                $month_eng = '';
            }else{
                $month_eng = date('F', mktime(0, 0, 0, $row->first_registration_month, 1)).', ';
            }

            if($row->first_registration_year==0||$row->first_registration_year == Null||$row->first_registration_year == ''){
                $year_number = '';
            }else{
                $year_number = $row->first_registration_year.' Year';
            }
            


            echo "
                 <tr>
                    <td>{$row->car_stock_no}</td>
                    <td>{$row->icon_status}</td>
                    <td>{$row->car_chassis_no}</td>
                    <td><center>{$row->car_model}</center></td>
                    <td>{$row->car_model_year}</td>
                    <td>{$month_eng} {$year_number}</td>
                    <td>{$row->manu_year}</td>
                    <td>{$row->car_grade}</td>
                    <td>{$row->car_fuel}</td>
                    <td>{$row->car_cc}</td>
                    <td>{$row->car_transmission}</td>
                    <td>{$row->car_color}</td>
                    <td>{$row->car_drive_type}</td>
                    <td>{$row->car_fob_currency} {$row->car_fob_cost}</td>
                    <td><a href='https://www.iblauda.com/?c=user&mcd=product&me=bbs_detail&idx={$row->idx}'>{$row->idx}</a></td>
                 </tr>
                    ";
        }

       
        echo "</table>";

    }
    //엑셀 변환 처리

    function adm_bbs_excel_exec($ls_query) {
       
        //다음 3줄은 엑셀파일로 변화하는 소스이다.
        //http://iblau.com/?c=admin&m=adm_bbs_excel_exec&mcd=product
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=excel_(" . $today_date . ").xls");
        header("Content-Description: PHP4 Generated Data");
         $today_date = date("Y/m/d");
        //$idx = $this->ci->input->get_post('idx');

        $query = $ls_query;
         /*print_r($query);  exit();*/

        //$query="select * from iw_bbs where menu_code='product' and car_visible='true' and icon_release_yn!='Y'  order by car_stock_no desc ";
        $result = mysql_query($query) or die(mysql_error());


        echo "
                <table>
                    <tr>
                        <td>No.</td>
                        <td>RegDate</td>
                        <td>RefNo</td>
                        <td>Status</td>
                        <td><center>car_model</center></td>
                        <td>ChassisNo</td>
                        <td>Year</td>

                        <td>Boss Price</td>
                        <td>TotalSellPrice</td>
                        <td>Deposit</td>
                        <td>Percent</td>

                        <td>Sales</td>
                        <td>Customer</td>
                        <td>Discharge</td>
                        <td>Consignee Info</td>
                        <td>PortLevedDate</td>
                        
                        
                        
                        <td>DHL No.</td>
                        <td>BL No.</td>
                    
                    </tr>
                ";
        while ($array = mysql_fetch_array($result)) {
            $total_sell_price = "$array[car_actual_price]" + "$array[car_freight_fee]";


            if ("$array[created_dt]" != NULL) {
                $date = date_create("$array[created_dt]");
                $created_dt_format = date_format($date, "Y-m-d");
            } else {
                $created_dt_format = "$array[created_dt]";
            }


            if ("$array[port_leaved_date]" != NULL) {
                $date2 = date_create("$array[port_leaved_date]");
                $port_leaved_date_format = date_format($date2, "Y-m-d");
            } else {
                $port_leaved_date_format = "$array[port_leaved_date]";
            }


            //var_dump("$array[port_leaved_date]");
            //echo date_format($date,"Y-m-d"); echo "<br/>";

            if (isset($total_sell_price) && !($total_sell_price == 0)) {
                $deposit_percent = ("$array[allocate_sum]" / $total_sell_price ) * 100;
            } else {
                $deposit_percent = 0;
            }


            echo "
                 <tr>
                    <td>
                      
                     </td>
                     <td>$created_dt_format</td>
                    <td>
                     $array[car_stock_no]
                     </td>
                     <td>$array[icon_status]</td>
                    <td>
                     <center>$array[car_model]</center>
                     </td>
                     <td>
                     $array[car_chassis_no]
                     </td>
                     <td>$array[car_model_year]</td>

                     <td>($array[car_fob_currency]) $array[car_fob_cost]</td>
                     <td>($array[car_actual_price_currency]) $total_sell_price</td>
                     <td>($array[currency_type])$array[allocate_sum]</td>
                     <td>$deposit_percent</td>

                     <td>$array[sales_full_name]</td>
                     <td>$array[customer_full_name]</td>
                     <td>$array[discharge]</td>
                     <td>$array[consignee_info]</td>
                     <td>$port_leaved_date_format</td>
                     
                     
                     
                     <td>$array[dhl_no]</td>
                     <td>$array[bl_no]</td>
                     
                 </tr>
                    ";
        }
        echo "</table>";
        //var_dump($query); exit();
    }

    //선적 정보 수정

    function adm_bbs_ship_update() {


        $idx = $this->ci->input->get_post('idx');


        func_set_data($this->ci, 'idx', $idx);


        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/update', $this->ci->data, true));



        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }

    //선적 수정 처리

    function adm_bbs_ship_update_exec() {


        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴

        if ($inData['port_leaved_date'] == '') {
            $inData['port_leaved_date'] = NULL;
        }


        //var_dump($inData); exit();

        $inData['idx'] = $_GET['idx'];   //게시물 일련번호

        $inData['menu_code'] = $this->mcd;        //메뉴코드
        //$inData['member_id']  = $this->ci->member_id;                 //아이디

        $inData['writer_ip'] = $this->ci->input->ip_address();   //작성자 아이피
        //var_dump($inData); exit();
        //DB 수정

        $this->ci->user_bbs->update($inData);

        //var_dump($this->ci->user_bbs); exit();

        echo func_jsAlertReplace('Modification Complete.', '/?c=admin&m=adm_bbs_list&mcd=' . $this->mcd);
    }

    //선적 정보 수정

    function adm_bbs_ship_popup_update() {

        $idxs = '';

        $idxs = $_GET['idxs']; 
        $idx = explode("'", $idxs);
        $id = implode(",", $idx);
        $grade_no_session = $_SESSION['ADMIN']['session_grade_no'];

        if($grade_no_session == 11) {

            $car_owner = $this->ci->user_member->iscar_owner($id);

            if($car_owner[0]->num_row == 0){

                redirect('/?c=admin', 'refresh');
            }
            
        }
        func_set_data($this->ci, 'idxs', $idxs);

        $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/popup', $this->ci->data, false);
    }

    //선적 수정 처리

    function adm_bbs_ship_popup_update_exec() {


        $idxs = '';

        $result = false;

        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴

        $idxs = $_GET['idxs'];   //게시물 일련번호
        
        $idx = explode("'", $idxs);

        $id = implode(",", $idx);

        $shipmements = substr(substr($id, 0, -1), 2);
        
        $shipment_idx  = str_replace(' , , , ', ',', $shipmements);

        $chassis_nos = $this->ci->bbs->get_chassis_by_idx(explode(",", $shipment_idx));

        foreach ($chassis_nos as $value) {
            
            $chassis_no[] = $value->car_chassis_no;

        }

        $chassis = implode(',', $chassis_no);

        $grade_no_session = $_SESSION['ADMIN']['session_grade_no'];
        
        if($grade_no_session == 11) {

            $car_owner = $this->ci->user_member->iscar_owner($id);

            if($car_owner[0]->num_row == 0){

                redirect('/?c=admin', 'refresh');
            }

        }

        $inData['menu_code'] = $this->mcd;        //메뉴코드

        $menu_code = $inData['menu_code'];

        $popup_type = $this->ci->input->get_post('popup_type');

        if ($popup_type == 'dhl_no') {

            $inData['dhl_no'] = $inData['dhl_no'];
            $inData['icon_status'] = 'soldout';
            $result = $this->ci->bbs->update_car_shipment_exec($inData,$menu_code,$id);

            $log_dhl_no = array(
                'author'=>$_SESSION['ADMIN']['member_no'],
                'message'=>'DHL no. of Car idx: '.$shipment_idx.' (chassis_no: '.$chassis.') has been modified by '.$_SESSION['ADMIN']['member_id'],
                'log_type'=>'car_status',
                'created_dt'=>func_get_date_time()
            );
            $this->ci->mo_activity_log->insert($log_dhl_no);
        }


        if ($popup_type == 'car_shipment') {

            $inData['vessel_name'] = $inData['vessel_name'];
            $inData['discharge'] = $inData['discharge'];
            $inData['port_leaved_date'] = $inData['port_leaved_date'];
            $inData['consignee_info'] = $inData['consignee_info'];
            $inData['shipping_company'] = $inData['shipping_company'];
            $result = $this->ci->bbs->update_car_shipment_exec($inData,$menu_code,$id);

            $log_shipment = array(
                'author'=>$_SESSION['ADMIN']['member_no'],
                'message'=>'Shipment info of Car idx: '.$shipment_idx.' (chassis_no: '.$chassis.') has been modified by '.$_SESSION['ADMIN']['member_id'],
                'log_type'=>'car_status',
                'created_dt'=>func_get_date_time()
            );
            $this->ci->mo_activity_log->insert($log_shipment);
        }

        if ($popup_type == 'car_status') {

            if (isset($inData['icon_status'])) {
                $icon_status = $inData['icon_status'];
            }
            
            $inData['icon_status'] = $inData['icon_status'];
            $result = $this->ci->bbs->update_car_shipment_exec($inData,$menu_code,$id);

            $log_status = array(
                'author'=>$_SESSION['ADMIN']['member_no'],
                'message'=>'Status of Car idx: '.$shipment_idx.' (chassis_no: '.$chassis.') has been modified by '.$_SESSION['ADMIN']['member_id'],
                'log_type'=>'car_status',
                'created_dt'=>func_get_date_time()
            );
            $this->ci->mo_activity_log->insert($log_status);
        }

        if ($popup_type == 'invoice_no') {

            $inData['invoice_no'] = $inData['invoice_no'];
            $result = $this->ci->bbs->update_car_shipment_exec($inData,$menu_code,$id);

            $log_invoice_no = array(
                'author'=>$_SESSION['ADMIN']['member_no'],
                'message'=>'Invoice no. of Car idx: '.$shipment_idx.' (chassis_no: '.$chassis.') has been modified by '.$_SESSION['ADMIN']['member_id'],
                'log_type'=>'car_status',
                'created_dt'=>func_get_date_time()
            );
            $this->ci->mo_activity_log->insert($log_invoice_no);
        }

        if ($popup_type == 'bl_no') {

            $inData['bl_no'] = $inData['bl_no'];
            $result = $this->ci->bbs->update_car_shipment_exec($inData,$menu_code,$id);

            $log_bl_no = array(
                'author'=>$_SESSION['ADMIN']['member_no'],
                'message'=>'BL No. of Car idx: '.$shipment_idx.' (chassis_no: '.$chassis.') has been modified by '.$_SESSION['ADMIN']['member_id'],
                'log_type'=>'car_status',
                'created_dt'=>func_get_date_time()
            );
            $this->ci->mo_activity_log->insert($log_bl_no);
        }


        //var_dump($sql); exit();
        //$this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/popup', $this->ci->data, false);
        //echo func_jsAlertReplace('Modification Complete.', '/?c=admin&m=adm_bbs_ship_popup_update&mcd=' . $this->mcd . '&idxs=' . $idxs );
        if ($result) {

            echo("<script language=\"JavaScript\" type=\"text/JavaScript\">\n");
            echo("alert('Modification Complete!!!!');\n");
            /* echo("window.location = '/?c=admin&m=adm_bbs_ship_popup_update&mcd=product&idx='");
              echo $idxs;
              echo(" ; "); */
            echo("</script>\n");
            $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/popup', $this->ci->data, false);
        } else {
            echo("<script language=\"JavaScript\" type=\"text/JavaScript\">\n");
            echo("alert('Modification Failed!!!!Please try again!!!');\n");

            echo("</script>\n");
            $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/popup', $this->ci->data, false);
        }
    }
    function adm_get_model_list(){
        $car_make = $this->ci->input->get_post('car_make');
        $where['make_name'] = $car_make;
        $model_list = $this->ci->bbs->getModel_table($where);
        echo '<option value="">- Select -</option>';
        if(!empty($where['make_name'])){
            foreach($model_list as $model){
                echo '<option value="'.$model->model_name.'">'.$model->model_name.'</option>';
            }
        }
        echo '<option value="add_new_model">-Add New Model-</option>';
    }
//modify adm_bbs_modify
    function adm_bbs_modify() {

        $idx = $this->ci->input->get_post('idx');
        
        $create_mode = $this->ci->input->get_post('create_mode');

        $only_attach = $this->ci->input->get_post('only_attach');   //첨부파일 상세 삭제 여부

        $member_logged = $this->ci->user_member->select_member_login();

        if ($only_attach != '' && $only_attach != 'N') {

            $att_idx = $this->ci->input->get_post('att_idx');

            if (isset($att_idx) && $att_idx != '') {

                $bbs_attach_detail_list = $this->ci->bbs->select_attach_detail($idx, $att_idx, $this->mcd);   //첨부파일 상세 조회

                $this->ci->bbs->delete_attach($idx, $att_idx, $this->mcd);   //첨부파일 삭제
                //업로드 실제 첨부파일 삭제
                func_bbs_delete_file($bbs_attach_detail_list, 'Y', $this->bbs_config);
            }

            $bbs_idx = $this->ci->input->get_post('bbs_idx');
            $bbs_id = $this->ci->input->get_post('id');
            if (isset($bbs_idx) && $bbs_idx != '') {
                $bbs_attach_detail_file = $this->ci->bbs->select_attach_file_detail($bbs_id, $bbs_idx);
                $this->ci->bbs->delete_attach_file($bbs_id, $bbs_idx);   //첨부파일 삭제
                //업로드 실제 첨부파일 삭제
                func_bbs_delete_file($bbs_attach_detail_file, 'N');
            }
        }

        if ($this->_isAdminGroup()) {
            $is_admin_group = true;
        }else{
            $is_admin_group = false;
        }

        $make_list = $this->ci->bbs->admGetMake();

        $make_list_admin = $this->ci->bbs->admGetMake_table();

        $owner_list= $this->ci->bbs->admGetOwner_list();
        $country_list = $this->ci->bbs->admGetCountry_list();
        $color_list = $this->ci->bbs->get_color_options();
        $bodytype_list = $this->ci->bbs->get_bodytype_list();
        $idx = $_GET['idx'];
        $bbs_detail_list = $this->ci->bbs->select_detail($idx);   //상세 조회

        if(!isset($_GET['create_mode']) || $_GET['create_mode'] !='new'){

        if($bbs_detail_list->num_rows() <=0 ){

            echo func_jsAlertReplace("The car does not exist!", "/?c=admin");
        }

        if($_SESSION['ADMIN']['session_grade_no'] ==11){

            $car_owner = $this->ci->user_member->iscar_owner($idx);

            if($car_owner[0]->num_row == 0){

                redirect('/?c=admin', 'refresh');
            }


        }

        }
         $model_list = array();

        $bbs_detail_result = $bbs_detail_list->result();
        //var_dump($bbs_detail_result);exit();
        $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);   //첨부파일 조회

        $bbs_attach_files = $this->ci->bbs->select_attach_files($idx);   //첨부파일 조회
        
        $where_model['make_name'] = $bbs_detail_result[0]->car_make;
        $model_list_admin = $this->ci->bbs->getModel_table($where_model);

        $model_list_all_admin = $this->ci->bbs->getModel_table();

        


        // $model_where['car_make'] = $bbs_detail_result[0]->car_make;
        // $model_list = $this->ci->bbs->getModel($model_where);


    
        func_set_data($this->ci, 'idx', $idx);
        func_set_data($this->ci, 'is_admin_group', $is_admin_group);
         func_set_data($this->ci, 'create_mode', $create_mode);
        func_set_data($this->ci, 'only_attach', $only_attach);
        func_set_data($this->ci, 'make_list', $make_list);
        func_set_data($this->ci, 'make_list_admin', $make_list_admin);
        func_set_data($this->ci, 'model_list_admin', $model_list_admin);
        
        func_set_data($this->ci, 'model_list_all_admin', $model_list_all_admin);

        func_set_data($this->ci, 'owner_list', $owner_list);
        func_set_data($this->ci, 'country_list', $country_list);
        //func_set_data($this->ci, 'model_list', $model_list);
        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);
        func_set_data($this->ci, 'color_list', $color_list);
        func_set_data($this->ci, 'bodytype_list', $bodytype_list);

        
                    
        func_set_data($this->ci, 'member_logged', $member_logged);
        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list);

        func_set_data($this->ci, 'bbs_attach_files', $bbs_attach_files->result());

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/modify', $this->ci->data, true));

        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }







 function adm_bbs_sale_modify() {

        $idx = $this->ci->input->get_post('idx');
        $create_mode = $this->ci->input->get_post('create_mode');

        $only_attach = $this->ci->input->get_post('only_attach');   //첨부파일 상세 삭제 여부

        $member_logged = $this->ci->user_member->select_member_login();

        if ($only_attach != '' && $only_attach != 'N') {

            $att_idx = $this->ci->input->get_post('att_idx');

            if (isset($att_idx) && $att_idx != '') {

                $bbs_attach_detail_list = $this->ci->bbs->select_attach_detail($idx, $att_idx, $this->mcd);   //첨부파일 상세 조회

                $this->ci->bbs->delete_attach($idx, $att_idx, $this->mcd);   //첨부파일 삭제
                //업로드 실제 첨부파일 삭제
                func_bbs_delete_file($bbs_attach_detail_list, 'Y', $this->bbs_config);
            }

            $bbs_idx = $this->ci->input->get_post('bbs_idx');
            $bbs_id = $this->ci->input->get_post('id');
            if (isset($bbs_idx) && $bbs_idx != '') {
                $bbs_attach_detail_file = $this->ci->bbs->select_attach_file_detail($bbs_id, $bbs_idx);
                $this->ci->bbs->delete_attach_file($bbs_id, $bbs_idx);   //첨부파일 삭제
                //업로드 실제 첨부파일 삭제
                func_bbs_delete_file($bbs_attach_detail_file, 'N');
            }
        }

        if ($this->_isAdminGroup()) {
            $is_admin_group = true;
        }else{
            $is_admin_group = false;
        }
        $make_list = $this->ci->bbs->admGetMake();
        $make_list_admin = $this->ci->bbs->admGetMake_table();
        $owner_list= $this->ci->bbs->admGetOwner_list();
        $country_list = $this->ci->bbs->admGetCountry_list();
        $color_list = $this->ci->bbs->get_color_options();
        $bodytype_list = $this->ci->bbs->get_bodytype_list();
        $idx = $_GET['idx'];
        $bbs_detail_list = $this->ci->bbs->select_detail($idx);   //상세 조회

        if(!isset($_GET['create_mode']) || $_GET['create_mode'] !='new'){

        if($bbs_detail_list->num_rows() <=0 ){

            echo func_jsAlertReplace("The car does not exist!", "/?c=admin");
        }

        if($_SESSION['ADMIN']['session_grade_no'] ==11){

            $car_owner = $this->ci->user_member->iscar_owner($idx);

            if($car_owner[0]->num_row == 0){

                redirect('/?c=admin', 'refresh');
            }


        }

        }
         $model_list = array();

        $bbs_detail_result = $bbs_detail_list->result();
        //var_dump($bbs_detail_result);exit();
        $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);   //첨부파일 조회

        $bbs_attach_files = $this->ci->bbs->select_attach_files($idx);   //첨부파일 조회
        
        $where_model['make_name'] = $bbs_detail_result[0]->car_make;
        $model_list_admin = $this->ci->bbs->getModel_table($where_model);


        // $model_where['car_make'] = $bbs_detail_result[0]->car_make;
        // $model_list = $this->ci->bbs->getModel($model_where);


    
        func_set_data($this->ci, 'idx', $idx);
        func_set_data($this->ci, 'is_admin_group', $is_admin_group);
         func_set_data($this->ci, 'create_mode', $create_mode);
        func_set_data($this->ci, 'only_attach', $only_attach);
        func_set_data($this->ci, 'make_list', $make_list);
        func_set_data($this->ci, 'make_list_admin', $make_list_admin);
        func_set_data($this->ci, 'model_list_admin', $model_list_admin);
        func_set_data($this->ci, 'owner_list', $owner_list);
        func_set_data($this->ci, 'country_list', $country_list);
        //func_set_data($this->ci, 'model_list', $model_list);
        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);
        func_set_data($this->ci, 'color_list', $color_list);
        func_set_data($this->ci, 'bodytype_list', $bodytype_list);

        
                    
        func_set_data($this->ci, 'member_logged', $member_logged);
        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list);

        func_set_data($this->ci, 'bbs_attach_files', $bbs_attach_files->result());

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/modify_sale', $this->ci->data, true));

        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }





    function adm_delete_attach() {
        $idx = $this->ci->input->get_post('idx');

        $only_attach = $this->ci->input->get_post('only_attach');   //첨부파일 상세 삭제 여부

        if ($only_attach != '' && $only_attach != 'N') {

            $att_idx = $this->ci->input->get_post('att_idx');

            if (isset($att_idx) && $att_idx != '') {

                $bbs_attach_detail_list = $this->ci->bbs->select_attach_detail($idx, $att_idx, $this->mcd);   //첨부파일 상세 조회

                $this->ci->bbs->delete_attach($idx, $att_idx, $this->mcd);   //첨부파일 삭제
                //업로드 실제 첨부파일 삭제
                func_bbs_delete_file($bbs_attach_detail_list, 'Y', $this->bbs_config);
            }

            $bbs_idx = $this->ci->input->get_post('bbs_idx');
            $bbs_id = $this->ci->input->get_post('id');
            if (isset($bbs_idx) && $bbs_idx != '') {
                $bbs_attach_detail_file = $this->ci->bbs->select_attach_file_detail($bbs_id, $bbs_idx);
                $this->ci->bbs->delete_attach_file($bbs_id, $bbs_idx);   //첨부파일 삭제
                //업로드 실제 첨부파일 삭제
                func_bbs_delete_file($bbs_attach_detail_file, 'N');
            }
        }

        if (is_array($idx)) {
            $idx = $idx[0];
        }

        $bbs_detail_list = $this->ci->bbs->select_detail($idx, $this->mcd);   //상세 조회

        $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);   //첨부파일 조회

        $bbs_attach_files = $this->ci->bbs->select_attach_files($idx);   //첨부파일 조회

        func_set_data($this->ci, 'idx', $idx);

        func_set_data($this->ci, 'only_attach', $only_attach);

        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);

        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list->result());

        func_set_data($this->ci, 'bbs_attach_files', $bbs_attach_files->result());

        $this->ci->load->view('admin/adm/bbs/product/image_upload', $this->ci->data, false);
    }

    //modify adm_bbs_modify

    function adm_bbs_modify_exec() {
        /*** GET PARAMETERS ***/
        $idx = $this->ci->input->get_post('idx');
        $create_mode = $this->ci->input->get_post('create_mode');

        /*** Validate ChassisNo ***/
        $chassis_no = $_POST['values']['car_chassis_no'];
        $vali = mysql_query("SELECT idx FROM iw_bbs WHERE car_chassis_no = '{$chassis_no}' AND idx != '{$idx}' ");


        $chassis_check = mysql_num_rows($vali);
        //var_dump($_POST); exit();
        if ($chassis_check > 0) {
            //echo func_jsAlertReplace('Please change ChassisNo. There is same ChassisNo.',"/?c=admin&m=adm_bbs_modify&mcd=product&category=offer&idx=$idx");
            echo "<script language='javascript'>";
            echo "confirm('Please change ChassisNo. There is same ChassisNo.'); ";
            echo "window.location.href = '/?c=admin&m=adm_bbs_modify&mcd=product&category=offer&idx={$idx}';";
            echo "</script>";
            /*$this->ci->load->view('admin/adm/bbs/product/modify', $this->ci->data, false);
            exit();*/
        }else{ ////////////////////////////////////////////////////////

        /*** GET FILE UPLOADS ***/
        for ($i = 0; $i < 6; $i++) {
            
            //선택된 첨부 파일만...
            if(isset($_FILES['file_type_sheet' . $i])){
                if ($_FILES['file_type_sheet' . $i]['tmp_name'][0] != '') {
                    $file_type = 'inspect';
                    if ($i == 1) {
                        $file_type = 'export';
                    } elseif ($i == 2) {
                        $file_type = 'cancel';
                    } elseif ($i == 3) {
                        $file_type = 'register';
                    } elseif ($i == 4) {
                        $file_type = 'original';
                    } elseif ($i == 5) {
                        $file_type = 'invoice';
                    }
                    //======= create delete image========            

                    $bbs_attach_detail_list = $this->ci->bbs->select_attach_files($idx, $file_type);   //첨부파일 상세 조회
                    //업로드 실제 첨부파일 삭제

                    if ($bbs_attach_detail_list->num_rows() > 0) {

                        foreach ($bbs_attach_detail_list->result() as $rows) {

                            //첨부파일이 이미지인 경우 생성된 썸네일 까지 삭제

                            $this->ci->bbs->delete_attach_file($rows->id, $idx);   //첨부파일 삭제

                            @unlink($rows->full_path);
                        }
                    }


                    //파일 업로드

                    $upload_str_tr = $this->ci->iwc_common->upload('file_type_sheet' . $i, 0, 'carItem', $this->bbs_config);


                    //파일 업로드 실패

                    if ($upload_str_tr != TRUE) {

                        show_error($this->ci->iwc_common->upload_error_msg());

                    }

                    //파일 업로드 성공
                    else {

                        $upload_file_info_car = array();

                        $upload_file_info_car = $this->ci->iwc_common->upload_success_msg();

                        if ($_FILES['file_type_sheet0']['tmp_name'][0] != '' && $i == 0 || $i == 0) {
                            $upload_file_info_car['file_type'] = "inspect";
                        }
                        if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 1 || $i == 1) {
                            $upload_file_info_car['file_type'] = "export";
                        }
                        if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 2 || $i == 2) {
                            $upload_file_info_car['file_type'] = "cancel";
                        }
                        if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 3 || $i == 3) {
                            $upload_file_info_car['file_type'] = "register";
                        }
                        if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 4 || $i == 4) {

                            $upload_file_info_car['file_type'] = "original";
                        }
                        if ($_FILES['file_type_sheet1']['tmp_name'][0] != '' && $i == 5 || $i == 5) {

                            $upload_file_info_car['file_type'] = "invoice";
                        }

                        $upload_file_info_car['bbs_idx'] = $idx;
                        $upload_file_info_car['orig_name'] = $_FILES['file_type_sheet'.$i]['name'][0];
                        $upload_file_info_car['created_dt'] = func_get_date_time();

                        $this->ci->bbs->add_detail_car($upload_file_info_car);
                    }
                }
            }
        }

        $num_imageUpload = 0;

        $idx_image = array();

        $id_array = 0;



        if (is_array($idx)) {

            $idx = $idx[0];
        }

        $bbs_detail_list = $this->ci->bbs->select_detail($idx, $this->mcd);   //상세 조회
        $bbs_detail = $bbs_detail_list->result();
        $bbs_attach_list = $this->ci->bbs->select_attach($idx, $this->mcd);   //첨부파일 조회

        if (count($bbs_attach_list) > 0) {

            foreach ($bbs_attach_list->result() as $att_rows) {

                $idx_image[$id_array][0] = $att_rows->att_idx;
            }
        }

        $attach_file_yn = func_get_config($this->bbs_config, 'attach_file_yn');   //첨부파일 사용여부

        $upload_str = '';
        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴
        
        $grade_no_session = $this->ci->grade_no;
        $member_no_session = $this->ci->member_no;
        if ($grade_no_session == 11){
            $inData['car_owner'] = $member_no_session; 
        }
        

        $inData['idx'] = $idx;    //게시물 일련번호

        $inData['menu_code'] = $this->mcd;        //메뉴코드\
        $inData['category'] = 'offer';
        $inData['draft']  = 'N';
        //$inData['member_id'] = $this->ci->member_id;             //아이디
        if(!isset($inData['ignore_scan_update'])){
            $inData['ignore_scan_update'] = 0;
        }
        $inData['writer_ip'] = $this->ci->input->ip_address();   //작성자 아이피
        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

            // 시행일

            $inData['publish_start'] = $inData['publish_end'] . ' 00:00:00';

            $inData['publish_end'] = $inData['publish_end'] . ' 23:59:59';
        }

        //우편번호
        /***SET DEFAULT VISIBLE TO YES***/
        if(!isset($inData['publish'])){
            $inData['publish'] = 'Y';
        }
        if (isset($inData['zipcode1']) && isset($inData['zipcode2'])) {

            $inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];

            unset($inData['zipcode1']);

            unset($inData['zipcode2']);
        }

        if (!isset($inData['notice_yn'])) {        //공지여부
            $inData['notice_yn'] = 'N';
        }

        if (!isset($inData['updated_dt']) || ( $inData['updated_dt'] == '')) {         //작성일
            $inData['updated_dt'] = func_get_date_time();
        }

        // if ($this->ci->input->post('sch_checkbox') != "") {

        //     $inData['country_options'] = func_get_post_array_checkbox($this->ci->input->post('sch_checkbox'));
        // }
        if ($this->ci->input->post('op_checkbox') != "") {

            $inData['car_options'] = func_get_post_array_checkbox($this->ci->input->post('op_checkbox'));
        }

        //DB 수정
        //$data['car_stock_no']=$this->get_max_car_stock_no() + 1;
        if(empty($bbs_detail['0']->car_stock_no)){
            $inData['car_stock_no'] = $this->ci->bbs->get_max_car_stock_no() +1;
        }

        /*** Validate FOB COST ***/
        $car_fob_cost = $inData['car_fob_cost'];
        $old_fob_cost = $inData['old_fob_cost'];

        if(!empty($old_fob_cost) && $old_fob_cost<$car_fob_cost ){

           echo func_jsAlertReplace("FOB Cost must less than OLD FOB Cost", "/?c=admin&m=adm_bbs_modify&mcd=product&idx=".$idx );
        
        }else{

        /*** UPDATE CAR ***/

        $this->ci->bbs->update($inData);

        }
        $data = array();
        $data['make_name'] = $inData['car_make'];
        $insert_id = $this->ci->bbs->car_make_insert($data);

        $data_model = array();
        $data_model['make_id'] = $insert_id;
        $data_model['model_name'] = $inData['car_model'];
        $this->ci->bbs->car_model_insert($data_model);



        /*** update unit_price in iw_account ***/

        if(isset($inData['unit_price']))
        {   
            $inAccount = array();

            $inAccount['car_idx'] = $inData['idx'];
            $inAccount['account_id'] = 1; //buying car
            $inAccount['account_detail'] = $inData['car_stock_no'].'/'.$inData['car_model'].'/'.$inData['car_chassis_no'];
            $inAccount['currency_type'] = $inData['unit_price_currency'];

            $inAccount['unit_price'] = $inData['unit_price'];
            $inAccount['unit_count'] = 1;
            $inAccount['unit_value_price'] = $inData['unit_price'];
            $inAccount['unit_total_price'] = $inData['unit_price'] + $inData['tax_price'];
            
            $inAccount['tax_flg'] = 'Tax';
            $inAccount['tax_price'] = $inData['tax_price'];
            
            $inAccount['account_memo'] = '';
            $inAccount['account_date'] = $inData['car_buying_dt'];
            

            $this->ci->account->update_account($inAccount);

            /*echo $this->ci->db->last_query(); exit();*/
        }



        // 설문조사 사용할 경우

        if (func_get_config($this->bbs_config, 'survey_yn') == 'Y') {

            $this->ci->load->model('mo_survey', 'survey'); // 설문조사
            // 설문조사 항목 저장

            $Survey = $this->ci->input->get_post('survey');

            if (isset($Survey)) {

                $this->ci->survey->config_insert($inData['idx'], $this->mcd, $Survey);
            }
        }

        func_set_data($this->ci, 'idx', $idx);

        func_set_data($this->ci, 'bbs_detail_list', $bbs_detail_list);

        func_set_data($this->ci, 'bbs_attach_list', $bbs_attach_list);

//var_dump($inData);
        if($create_mode=='new'){
            // $this->pushNewAndUpdateNotification($inData,0);
            //******************INSERT ACTIVITY LOG********************//
            $insert_data = array(
                'author'=>$_SESSION['ADMIN']['member_no'],
                'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no.' ) has been inserted by '.$_SESSION['ADMIN']['member_id'],
                'log_type'=>'car',
                'created_dt'=>func_get_date_time()
            );
            $this->ci->mo_activity_log->insert($insert_data);
            //$this->ci->mo_parse->insert($inData);
           

            //$this->insertSQLite($inData);
            redirect('/?c=admin&m=adm_bbs_detail&mcd='.$this->mcd.'&idx='.$idx, 'refresh');
           
        }else{
            //echo func_jsAlertReplace('Car Information has beed modified.', '?c=admin&m=adm_bbs_detail&mcd='.$this->mcd.'&idx='.$idx);
            //$this->pushNewAndUpdateNotification($inData,1);
            //******************INSERT ACTIVITY LOG********************//
            $modify_data = array(
                'author'=>$_SESSION['ADMIN']['member_no'],
                'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no.' ) has been updated by '.$_SESSION['ADMIN']['member_id'],
                'log_type'=>'car',
                'created_dt'=>func_get_date_time()
            );
            /*** SAVE TO PARSE.com ****/
            $this->ci->mo_activity_log->insert($modify_data);
            // $saved_car = $this->ci->mo_parse->select_by_idx($idx);
            // if(count($saved_car)>0){
            //     $this->ci->mo_parse->update($idx, $inData);
            // }else{
            //     $this->ci->mo_parse->insert($inData);
            // }
            /*** END SAVE TO PARSE.com ****/
            

            //$this->updateSQLite($inData);
            //if(isset($_POST['address']) && $_POST['address'] != '' ){
                // echo func_jsAlertReplace('Car Information has beed modified.', $_POST['address'] );
            //}else{
                redirect('/?c=admin&m=adm_bbs_detail&mcd=product&idx='.$idx, 'refresh');
                // echo func_jsAlertReplace('Car Information has beed modified.', "/?c=admin&m=adm_bbs_detail&mcd=product&idx=$idx" );
            //}
        }

    }

    }

    /*
    *
    $mode=0-> NEW
    $mode=1-> EDIT
    $mode=2-> DELETE
    $mode=3-> SQLite
    *
    */
// INSERT INTO SQLITE
    // function createSQLite($value='')
    // {
    //     $this->ci->user_bbs->SQLite();
    // }
    // function userInsertSQLite($data)
    // {
    //     $this->ci->user_bbs->insertSQLData('cars',$data);
    // }
    // function insertSQLite($body)
    // {
    //     $result=array($this->getCarData($body,3));
    //     $this->ci->user_bbs->insertSQLData('cars',$result);
    // }
    // function insertPremiumSQLite($car_id)
    // {
    //     $this->ci->user_bbs->insertPremiumSQLite($car_id);
    // }
    // function removePremiumSQLite($car_id)
    // {
    //     foreach ($car_id as $id) {            
    //         $this->ci->user_bbs->removePremiumSQLite($id);
    //     }
    // }
    // function updateSQLite($body)
    // {
    //     $result=array($this->getCarData($body,3));
    //     $this->ci->user_bbs->updateSQLData('cars',$result);
    // }
    // function deleteSQLite($car_id)
    // {
    //     $result[]=array('car_id'=>$car_id);
    //     $this->ci->user_bbs->deleteSQLData('cars',$result);
    // }
    // function insertImageSQLite($body)
    // {
    //     foreach ($body as $data) {            
    //         $result=array($this->getCarImageData($data));
    //         $this->ci->user_bbs->insertSQLData('car_images',$result);
    //     }
    // }
    // function deleteImageSQLite($car_id)
    // {
    //     $result[]=array('car_id'=>$car_id);
    //     $this->ci->user_bbs->deleteSQLData('car_images',$result);
    // }
    // function deletePublicImageSQLite($image)
    // {
    //     // $result[]=array('substr(image,instr(image,"upload/")+7)'=>$public_id);
    //     $result[]=array('image'=>$image);
    //     $this->ci->user_bbs->deleteSQLData('car_images',$result);
    // }
    // function updateSellerImageSQLite($image_id,$seller_id)
    // {
    //     $image=$this->ci->bbs->sqliteSellerImage($image_id);
    //     $result[]=array('seller_id'=>$seller_id,'image'=>$image);
    //     $this->ci->user_bbs->updateSellerSQLData('sellers',$result);        
    // }
    // function updateImagePrimay($image,$car_id)
    // {
    //     $this->ci->user_bbs->updateImagePrimay($image,$car_id);        
    // }
    // function updateImageSort($data)
    // {
    //     foreach ($data as $key => $value) {
    //         $image=$this->ci->bbs->sqliteCarImageByID($value['id']);
    //         $this->ci->user_bbs->updateImageSort($image,$value['sort']);
    //     }
                    
    // }
    // function updateImageReSort($primary_id,$car_id,$index)
    // {
    //     $images=$this->ci->bbs->sqliteReSortImage($primary_id,$car_id);
        
    //     foreach ($images as $key => $value) {
    //         $this->ci->user_bbs->updateImageSort($value->image,$index);
    //         $index++;
    //     }
    // }
    // function insertMake($data)
    // {
    //     $this->ci->bbs->insertSQLData('brands',$data);
    // }
    // function updateMake($oldmake,$make,$iconname)
    // {        
    //     $this->ci->user_bbs->updateMake($oldmake,$make,$iconname);
    // }
    // function deleteMake($make)
    // {        
    //     $data[]=array('name'=>$make);
    //     $this->ci->user_bbs->deleteSQLData('brands',$data);
    // }

    function getCarData($body,$mode='null')
    {
        $result=array();
        if(isset($body['idx']))
            $result['car_id']=$body['idx'];
        if(isset($body['stock_id']))
            $result['stock_id']=$body['stock_id'];
        if(isset($body['car_stock_no']))
            $result['stock_id']=$body['car_stock_no'];
        $result['condition']=$body['icon_new_yn'];
        $result['type']=$body['car_body_type'];
        $result['make']=$body['car_make'];
        $result['model']=$body['car_model'];
        $result['chassis_no']=$body['car_chassis_no'];
        $result['model_year']=$body['car_model_year'];
        $result['mileage']=$body['car_mileage'];
        $result['steering']=$body['car_steering'];
        $result['transmission']=$body['car_transmission'];
        $result['engine_size']=$body['car_cc'];
        $result['fuel_type']=$body['car_fuel'];
        $result['country']=$body['country'];
        $result['exterior_color']=$body['car_color'];
        $result['manufactured_year']=$body['manu_year'];
        $result['registration_date']=$body['first_registration_year'].$body['first_registration_month'];
        $result['fob']=$body['car_fob_cost'];
        if(isset($body['contents']))
            $result['comment']=$body['contents'];
        $result['seller_id']=$body['car_owner'];
        $result['contact_id']='262';
        // $result['premium']=$body['car_owner'];
        $result['status']=$body['icon_status'];
        if($mode!=3)
            $result['mode']=$mode;
        /*if(isset($body['created']))
            $result['created']=$body['created'];

        $timezone = date_default_timezone_get();
        echo $timezone;
        $modified = date('Y-m-d H:i:s');
        $result['modified']=$modified;*/
        return $result;
    }

    public function getCarImageData($data)
    {
        $result=array();
        $result['car_id']=$data['bbs_idx'];
        $result['image']=$data['base_url'].$data['public_id'].'.webp';
        $result['sort']=$data['sort'];
        return $result;
    }

    function pushNewAndUpdateNotification($body,$mode)
    {
        $app_id=$this->ci->user_bbs->getRegisterID();
        $message["cars"]=$this->getCarData($body,$mode);
        $car_image=$this->ci->user_bbs->getCarImage($message["cars"]["car_id"]);
        $message["car_images"]=$car_image;
        // print_r($message);
        $this->send_notification($app_id,$message);
    }
    function pushDeleteNotification($car_id)
    {
        $app_id=$this->ci->user_bbs->getRegisterID();        
        $message["cars"]=array('car_id'=>$car_id,'mode'=>2);
        $message["car_images"]=array();
        // print_r($message);
        $this->send_notification($app_id,$message);
    }
    function send_notification($registatoin_ids, $message) 
    {
            
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';
 
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );
 
        $headers = array(
            'Authorization: key=' . 'AIzaSyAu6awyUpYGxGkB25v-klfqsbXUKRRjxww',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        // Close connection
        curl_close($ch);
        // echo $result;
    }
    //게시판 선택 삭제

    function adm_bbs_all_delete() {

        $idx = array();

        $idxs = $this->ci->input->get_post('idx');     //배열
        // var_dump($idxs);die();
        $cur_page = $this->ci->input->get_post('cur_page');

        $sParam = $this->ci->input->get_post('sParam');     //검색 string
        //선택 게시물 배열

        foreach ($idxs as $key => $value) {

            $idx = $value;   //게시물 번호

            if ($idx != '') {

                $chassis_no = $this->ci->bbs->get_chassis_by_idx($idx);

                //*************************INSERT LOG ACTIVITY*************************//
                $log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'Car idx: '.$idx.' (chassis: '.$chassis_no[0]->car_chassis_no.') has been deleted by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'car',
                    'created_dt'=>func_get_date_time()
                );
                
                $this->ci->mo_activity_log->insert($log_data);         
                
                // PUSH DELETE CAR
                //$this->pushDeleteNotification($idx);
                
                //$this->deleteSQLite($idx);
                //$this->deleteImageSQLite($idx);

                $where = array('bbs_idx'=>$idx);

                $detail_image = $this->ci->mo_file->select_detail_image($where);

                if (count($detail_image) > 0){

                    foreach($detail_image as $row){

                        $bbs_idx[] = $row->id;
                        $full_path  = $_SERVER["DOCUMENT_ROOT"].$row->base_url.$row->public_id;
                        $thumb_path = $_SERVER["DOCUMENT_ROOT"].$row->base_url.'thumb/'.$row->public_id;
                        
                        @unlink($full_path);
                        @unlink($thumb_path);

                    }

                    $this->ci->mo_file->delete_bbs_file($bbs_idx);                    
                }

                $this->ci->bbs->delete($idx, $this->mcd); 
                if ($_SERVER['SERVER_NAME'] == 'motorbb.com' ){
                    $this->ci->mo_parse->delete_by_idx($idx);
                }
            }
        }

        echo func_jsAlertReplace('Deletion Complete.', '/?c=admin&m=adm_bbs_list&mcd=' . $this->mcd . '&cur_page=' . $cur_page . func_sPUrlEncode($sParam, "sch_word"));
    }

    //게시판 코멘트 등록 처리

    function adm_bbs_comment_write_exec() {

        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');

        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;

        $sParam = $this->ci->input->get_post('sParam');    //검색 string



        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values', TRUE));    //POST 배열을 array['key'] = value 형태로 리턴

        $inData['idx'] = $idx;           //게시판 일련번호

        $inData['menu_code'] = $this->mcd;        //메뉴코드

        $inData['member_id'] = $this->ci->member_id;       //아이디

        $inData['writer_ip'] = $this->ci->input->ip_address();   //작성자 아이피

        $inData['created_dt'] = func_get_date_time();       //작성일



        $comment_reply_yn = func_get_config($this->bbs_config, 'comment_reply_yn');   //코멘트 등록시 답변처리로 간주 여부
        //DB 등록

        $this->ci->bbs->insert_comment($inData, $comment_reply_yn);



        $link = "/?c=admin&m=adm_bbs_detail&mcd=" . $this->mcd . "&idx=" . $idx . "&cur_page=" . $cur_page . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . $sParam;



        echo func_jsAlertReplace('코멘트가 등록 되었습니다.', $link);
    }

    //게시판 코멘트 삭제

    function adm_bbs_comment_delete() {

        $password_chk = $this->ci->input->get_post('password_chk');    //비밀번호

        $cm_idx = $this->ci->input->get_post('cm_idx');

        $idx = $this->ci->input->get_post('idx');

        $cur_page = $this->ci->input->get_post('cur_page');

        if ($cur_page == NULL || $cur_page == '')
            $cur_page = 1;

        $sParam = $this->ci->input->get_post('sParam');    //검색 string
        //비밀번호

        $query = $this->ci->bbs->select_password($cm_idx, $idx, $this->mcd, $password_chk);



        $link = "/?c=admin&m=adm_bbs_detail&mcd=" . $this->mcd . "&idx=" . $idx . "&cur_page=" . $cur_page . func_sPUrlEncode($sParam, "sch_word") . "&sParam=" . $sParam;



        //비밀번호 정보 OK

        if ($query->num_rows() > 0) {

            //DB 삭제

            $this->ci->bbs->delete_comment($cm_idx, $idx, $this->mcd);

            echo func_jsAlertReplace('코멘트가 삭제 되었습니다.', $link);
        } else {

            echo func_jsAlertReplace('비밀번호가 올바르지 않습니다.', $link);
        }
    }

    // 설문조사 결과 다운로드

    function adm_survey_entry_download() {



        $this->ci->load->model('mo_survey', 'survey'); // 설문조사

        $idx = $this->ci->input->get_post('idx');



        $Cfg = $this->ci->survey->config($idx, $this->mcd);

        $Res = $this->ci->survey->select_entry($idx, $this->mcd)->result();



        header('Content-Type: application/force-download');

        header('Content-Disposition: attachment; filename="' . date('YmdHis') . '.csv"');

        header('Expires: 0');

        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

        header("Content-Transfer-Encoding: binary");

        header('Pragma: public');



        $CfgCnt = 0;

        foreach (Array($Cfg['custom'], $Cfg['field']) as $Data) {

            //var_dump($Data);

            foreach ($Data as $Dkey => $Dval) {

                $Prt = '';

                if ($CfgCnt > 0) {

                    $Prt .= ',';
                }



                if (strstr($Dval->title, '"')) {

                    $Dval->title = str_replace('"', '""', $Dval->title);
                }

                if (strstr($Dval->title, ',')) {

                    $Dval->title = '"' . $Dval->title . '"';
                }



                $Prt .= $Dval->title;

                echo iconv('UTF-8', 'EUC-KR', $Prt);

                $CfgCnt++;
            }
        }



        echo "\r\n";



        foreach ($Res as $Rkey => $Rval) {

            $ResCnt = 0;

            foreach ($Rval as $Ckey => $Cval) {

                if ((strstr($Ckey, 'custom') || strstr($Ckey, 'field')) && ($CfgCnt >= $ResCnt)) {

                    $Prt = '';

                    if ($ResCnt > 0) {

                        $Prt .= ',';
                    }

                    if (strstr($Cval, '"')) {

                        $Cval = str_replace('"', '""', $Cval);
                    }

                    if (strstr($Cval, ',')) {

                        $Cval = '"' . $Cval . '"';
                    }

                    $Prt .= $Cval;

                    echo iconv('UTF-8', 'EUC-KR', $Prt);

                    $ResCnt++;
                }
            }

            echo "\r\n";
        }

        exit;
    }

    //reserve popup update

    function adm_reserve_popup_update() {

        $idx = '';

        $idx = $_GET['idx'];

        $grade_no_session = $_SESSION['ADMIN']['session_grade_no'];
        
        if($grade_no_session == 11) {

            $car_owner = $this->ci->user_member->iscar_owner($idx);

            if($car_owner[0]->num_row == 0){

                redirect('/?c=admin', 'refresh');
            }

        }

        $query = $this->ci->bbs->select_detail($idx,'product');
        $result = $query->result();
        
        $select_car_fob_currency = $result[0]->car_fob_currency;
        $select_car_fob_cost = $result[0]->car_fob_cost;

        func_set_data($this->ci, 'car_fob_currency', $select_car_fob_currency);
        func_set_data($this->ci, 'car_fob_cost', $select_car_fob_cost);

        $customer_no = $this->ci->input->get_post('customer_no');

        func_set_data($this->ci, 'idx', $idx);
        func_set_data($this->ci, 'customer_no', $customer_no);

        $this->ci->load->view('admin/adm/bbs/product/reserve_popup', $this->ci->data, false);
    }

    //선적 수정 처리

    function adm_reserve_popup_update_exec() {
        
        $idx = $this->ci->input->get_post('idx');
        $allowSave = true;
        $inData = array();

        $inData = func_get_post_array($this->ci->input->post('values'));    //POST 배열을 array['key'] = value 형태로 리턴
        $inData['menu_code'] = $this->mcd;
        $inData['idx'] = $idx;

        $query_detail = $this ->ci->bbs->select_detail($idx,$inData['menu_code']);
        $select_detail = $query_detail->result();

        // if($inData['car_actual_price']+$inData['car_freight_fee']<$select_detail['0']->car_fob_cost){
        //     $allowSave=false;
        // }

        if ($inData['car_actual_price'] =="") {  
            $allowSave = false;
        } 

         // var_dump($inData);
        if($allowSave){
            /***RESERVE EXECUTE****/
            $result = $this->ci->bbs->update($inData);
            if ($result == 1) {
                //Array of customers that had ordered car exclude the one who success for reserving the car
                // $inventory_customers = $this->ci->member->getInventoryCustomer($idx, $inData['customer_no']);
                // //Send order notification to email
                // foreach($inventory_customers as $customer){
                    // $this->ci->mo_email->sendOrderNotification($customer->email, $customer->member_first_name." ".$customer->member_last_name, $idx);
                // }
                //Send order notification to site inbox
                // $customer_nos = $this->ci->mo_message->getOrderCustomer();
                // var_dump($customer_nos);
                // $customers = array();
                // foreach ($customer_nos as $data) { 
                    // if($data!=$select_detail[0]->customer_no){
                        // $customers[] = $data;
                    // }
                // }
                // $customer_nos = $customers;

                // $this->ci->mo_message->sendMessage($idx,$customers);

                $customers = $this->ci->member->select_customer_no($inData['customer_no']);

                $chassis_no = $this->ci->bbs->get_chassis_by_idx($idx);

                $log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'Reservation of Car idx: '.$idx.' (chassis_no: '.$chassis_no[0]->car_chassis_no.') for customer: '. $customers->email .' has been made by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'car_status',
                    'created_dt'=>func_get_date_time()
                );
                $this->ci->mo_activity_log->insert($log_data);
                        
                // //$this->ci->mo_message->sendMessage($idx);
                echo func_jsAlertReplace('This car has been reserved successfully.', '?c=admin&m=adm_reserve_popup_update&mcd=' . $this->mcd . '&idx=' . $idx . '&page_acc=completed');
            }
        }else{
            echo func_jsAlertReplace('Cannot reserve the car. Please check your detail again.', '?c=admin&m=adm_reserve_popup_update&mcd=' . $this->mcd . '&idx=' . $idx . '&page_acc=not_completed');
        }
        
    }

    function adm_test_order_email(){
         //$this->ci->load->model('mo_email', 'email'); 
         $email = "kysengkean@gmail.com";
         $name = "Sengkean";
         $idx = 9922;
         $this->ci->mo_email->sendOrderNotification($email, $name, $idx);

    }

    function adm_reserve_cancel_exec() {

        $idxs = '';

        $result = false;

        $inData = array();

        $idxs = $_GET['idxs'];
        $idx = explode("'", $idxs);
        $id = implode(",", $idx);
       
        $grade_no_session = $_SESSION['ADMIN']['session_grade_no'];
        
        if($grade_no_session == 11) {

            $car_owner = $this->ci->user_member->iscar_owner($id);

            if($car_owner[0]->num_row == 0){

                redirect('/?c=admin', 'refresh');
            }

        }

        $inData['menu_code'] = $this->mcd;
         // $inData['idx'] = $idxs;  
        $menu_code = $inData['menu_code'];      
        $inData['icon_status'] = 'sale';
        $inData['car_price_type'] = NULL;
        $inData['sales_no'] = NULL;
        $inData['customer_no'] = NULL;
        $inData['car_actual_price'] = NULL;
        $inData['car_actual_price_currency'] = NULL;
        $inData['car_freight_fee'] = NULL;
        $inData['car_freight_fee_currency'] = NULL;
        $inData['reserved_dt'] = NULL;
        
        $result = $this->ci->bbs->reserve_cancel_exec($inData,$menu_code,$id);
        
        if ($result) {
            $r_cancel_idx = substr(substr($id, 0, -1),2);
            $cancel_idx = str_replace(' , , , ', ',', $r_cancel_idx);

            $chassis_nos = $this->ci->bbs->get_chassis_by_idx(explode(",", $cancel_idx));

            foreach ($chassis_nos as $value) {
                $chassis_no[] = $value->car_chassis_no;
            }

            $chassis = implode(',', $chassis_no);

            $log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'Reservation of Car idx: '.$cancel_idx.' (chassis_no: '.$chassis.') for customer: customer@gmail.com has been cancelled by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'car_status',
                    'created_dt'=>func_get_date_time()
                );
            $this->ci->mo_activity_log->insert($log_data);

            echo("<script language=\"JavaScript\" type=\"text/JavaScript\">\n");
            echo("alert('Cancellation Complete!!!!');\n");
            /* echo("window.location = '/?c=admin&m=adm_bbs_ship_popup_update&mcd=product&idx='");
              echo $idxs;
              echo(" ; "); */
            echo("document.location.href = '?c=admin&m=adm_bbs_list&mcd=product'\n");
            echo("</script>\n");
            //$this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/popup', $this->ci->data, false);
            //print_r($this->ci->load->view('?c=admin&m=adm_bbs_list&mcd=product', false));
            //func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/list', $this->ci->data, true));
        } else {
            echo("<script language=\"JavaScript\" type=\"text/JavaScript\">\n");
            echo("alert('Modification Failed!!!!Please try again!!!');\n");

            echo("</script>\n");
            $this->ci->load->view('admin/adm/bbs/' . $this->mcd . '/list', $this->ci->data, false);
        }


    }

    function ceiling($number, $significance = 1)
    {
        return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
    }

    function adm_bbs_excel_customer(){
        ob_end_clean();
        /** Error reporting */
        //error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Asia/Krasnoyarsk');
        if (PHP_SAPI == 'cli')
        die('This example should only be run from a Web Browser');
        /** Include PHPExcel */
        require_once dirname(__FILE__) . '/../PHPExcel/PHPExcel-develop/Classes/PHPExcel.php';
        /** PHPExcel_IOFactory */
        require_once dirname(__FILE__) . '/../PHPExcel/PHPExcel-develop/Classes/PHPExcel/IOFactory.php';
        //echo date('H:i:s') , " Load from Excel5 template" , EOL;
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $objPHPExcel = $objReader->load("PHPExcel/PHPExcel-develop/Examples/templates/bbs_excel_customer.xls");
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("MOTORBB")
        ->setLastModifiedBy("MOTORBB")
        ->setTitle("STOCK LIST FOR CUSTOMER")
        ->setSubject("STOCK LIST FOR CUSTOMER")
        ->setDescription("STOCK LIST FOR CUSTOMER")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("CUSTOMER");
        // Add some data

    /*****RETRIEVED DATA*****/
        /***YOU CAN SET CONDITION WHERE HERE***/
        $where = array();
        
        $country_to = $this->ci->input->get_post('country_to');
        $port_name = $this->ci->input->get_post('port_name');

        $input['created_dt >='] = $this->ci->input->get_post('sch_create_dt_s');
        $input['created_dt <='] = $this->ci->input->get_post('sch_create_dt_e');
        $input['icon_status'] = $this->ci->input->get_post('sch_icon_status');
        $input['country'] = $this->ci->input->get_post('sch_car_location');
        $search_key = $this->ci->input->get_post('sch_condition');
        $input[$search_key] = $this->ci->input->get_post('sch_word');

        $order_by = $this->ci->input->get_post("order_by");
        $order_d = $this->ci->input->get_post("order_d");
        $order='ORDER BY icon_status ASC, created_dt DESC';
        if(!empty($order_by)){
            $order = "ORDER BY ".$order_by." ".$order_d;
        }
        /************************************/
        /**************DO NOT CHANGE*********/
        foreach($input as $key=>$value){
            if(!empty($value)){
                $where[$key]=$value;
            }
        }
        $rows=$this->ci->bbs->adm_select_export_customer($where, $order, true);
        /************************************/
        $today_date = date("Y-m-d-H-i-s");
    /*****END RETRIEVED DATA*****/
        $i=1;

        foreach($rows as $row){
            $i++;
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':U'.$i)->applyFromArray(
                array(
                    'borders' => array(
                          'allborders' => array(
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          )
                      )
                )
            );
            $total_sell_price = $row->car_actual_price + $row->car_freight_fee;
            $date = date_create($row->created_dt);
            $date2 = date_create($row->port_leaved_date);
            $created_dt_format = date_format($date, "Y-m-d");
            $port_leaved_date_format = date_format($date2, "Y-m-d");
            if($total_sell_price > 0){
                $deposit_percent = ($row->allocate_sum / $total_sell_price ) * 100;
            }else{
                $deposit_percent = 0;
            }

            if($row->first_registration_month==0||$row->first_registration_month == ''){
                $month_eng = '';
            }else{
                $month_eng = date('F', mktime(0, 0, 0, $row->first_registration_month, 1)).', ';
            }

            if($row->first_registration_year==0||$row->first_registration_year == ''){
                $year_number = '';
            }else{
                $year_number = $row->first_registration_year;
            }

            if($row->car_model_year==0||$row->car_model_year == ''){
                $model_year = '';
            }else{
                $model_year = $row->car_model_year;
            }

            if($row->car_cc==0||$row->car_cc == ''){
                $cc = '';
            }else{
                $cc = $row->car_cc;
            }

            if($row->car_fob_cost==0||$row->car_fob_cost == ''){
                $fob_cost = '';
            }else{
                $fob_cost = $row->car_fob_currency." ".number_format($row->car_fob_cost);
            }

            if($row->manu_year==0||$row->manu_year==''){
                $manu_year = '';
            }else{
                $manu_year = $row->manu_year;
            }

            if($row->car_mileage==0||$row->car_mileage==''){
                $mileage = '';
            }else{
                $mileage = number_format($row->car_mileage);
            }
            
            $total_price = $this->ci->mo_shipping->get_total_price($port_name,$row->idx,true,true);

            if ($total_price>0) {

                $total_price = $row->car_fob_currency." ".number_format($total_price);

            }else{

                $total_price = 'N/A';
                
            }
                
            

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $i-1)
                ->setCellValue('B'.$i, $row->car_stock_no)
                ->setCellValue('C'.$i, $row->country_name)
                ->setCellValue('D'.$i, $row->car_city)
                ->setCellValue('E'.$i, $row->icon_status)
                ->setCellValue('F'.$i, $row->car_chassis_no)
                ->setCellValue('G'.$i, $row->car_make)
                ->setCellValue('H'.$i, $row->car_model)
                ->setCellValue('I'.$i, $model_year)
                ->setCellValue('J'.$i, $month_eng." ".$year_number)
                ->setCellValue('K'.$i, $manu_year)
                ->setCellValue('L'.$i, $mileage)
                ->setCellValue('M'.$i, $row->car_grade)
                ->setCellValue('N'.$i, $row->car_fuel)
                ->setCellValue('O'.$i, $cc)
                ->setCellValue('P'.$i, $row->car_transmission)
                ->setCellValue('Q'.$i, $row->car_color)
                ->setCellValue('R'.$i, $row->car_drive_type)
                ->setCellValue('S'.$i, $fob_cost)
                ->setCellValue('T'.$i, $total_price);
                $link = "https://www.motorbb.com/?c=user&m=showdetailcar&idx=".$row->idx;
                //set the value of the cell
                $objPHPExcel->getActiveSheet()->SetCellValue('U'.$i, $row->idx);
                //change the data type of the cell
                $objPHPExcel->getActiveSheet()->getCell('U'.$i)->setDataType(PHPExcel_Cell_DataType::TYPE_STRING2);
                ///now set the link
                $objPHPExcel->getActiveSheet()->getCell('U'.$i)->getHyperlink()->setUrl(strip_tags($link));
        }


        
        // Miscellaneous glyphs, UTF-8
       
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('STOCK LIST FOR CUSTOMER');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="stock_list_for_customer('.$today_date.').xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    function adm_bbs_excel_staff(){
        ob_end_clean();
        /** Error reporting */
        //error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Asia/Krasnoyarsk');
        if (PHP_SAPI == 'cli')
        die('This example should only be run from a Web Browser');
        /** Include PHPExcel */
        require_once dirname(__FILE__) . '/../PHPExcel/PHPExcel-develop/Classes/PHPExcel.php';
        /** PHPExcel_IOFactory */
        require_once dirname(__FILE__) . '/../PHPExcel/PHPExcel-develop/Classes/PHPExcel/IOFactory.php';
        //echo date('H:i:s') , " Load from Excel5 template" , EOL;
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $objPHPExcel = $objReader->load("PHPExcel/PHPExcel-develop/Examples/templates/bbs_excel_staff.xls");
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("MOTORBB")
        ->setLastModifiedBy("MOTORBB")
        ->setTitle("STOCK LIST FOR STAFF")
        ->setSubject("STOCK LIST FOR STAFF")
        ->setDescription("STOCK LIST FOR STAFF")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("STAFF");
        // Add some data

    /*****RETRIEVED DATA*****/
        /***YOU CAN SET CONDITION WHERE HERE***/
        $where = array();
        $input['created_dt >='] = $this->ci->input->get_post('sch_create_dt_s');
        $input['created_dt <='] = $this->ci->input->get_post('sch_create_dt_e');
        $input['icon_status'] = $this->ci->input->get_post('sch_icon_status');
        $input['country'] = $this->ci->input->get_post('sch_car_location');
        $search_key = $this->ci->input->get_post('sch_condition');
        $input[$search_key] = $this->ci->input->get_post('sch_word');

        $order_by = $this->ci->input->get_post("order_by");
        $order_d = $this->ci->input->get_post("order_d");
        $order='ORDER BY icon_status ASC, created_dt DESC';
        if(!empty($order_by)){
            $order = "ORDER BY ".$order_by." ".$order_d;
        }
        /************************************/
        /**************DO NOT CHANGE*********/
        foreach($input as $key=>$value){
            if(!empty($value)){
                $where[$key]=$value;
            }
        }

        $rows=$this->ci->bbs->adm_select_export($where, $order, false);

        /************************************/
        $today_date = date("Y-m-d-H-i-s");
    /*****END RETRIEVED DATA*****/
        $i=1;

        foreach($rows as $row){
            $i++;
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':AA'.$i)->applyFromArray(
                array(
                    'borders' => array(
                          'allborders' => array(
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                          )
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->getStyle('M'.$i.':O'.$i)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '90EE90')
                    )
                )
            );

            $objPHPExcel->getActiveSheet()->getStyle('P'.$i.':S'.$i)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'ADD8E6')
                    )
                )
            );

            $objPHPExcel->getActiveSheet()->getStyle('T'.$i.':AA'.$i)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'FFFFE0')
                    )
                )
            );

            $total_sell_price = $row->car_actual_price + $row->car_freight_fee;
            $date = date_create($row->created_dt);
            $date2 = date_create($row->port_leaved_date);
            $created_dt_format = date_format($date, "Y-m-d");
            $port_leaved_date_format = date_format($date2, "Y-m-d");
            if($total_sell_price > 0){
                $deposit_percent = ($row->allocate_sum / $total_sell_price ) * 100;
            }else{
                $deposit_percent = 0;
            }

            if($row->first_registration_month==0||$row->first_registration_month == Null||$row->first_registration_month == ''){
                $month_eng = '';
            }else{
                $month_eng = date('F', mktime(0, 0, 0, $row->first_registration_month, 1)).', ';
            }

            if($row->first_registration_year==0||$row->first_registration_year == Null||$row->first_registration_year == ''){
                $year_number = '';
            }else{
                $year_number = $row->first_registration_year.' Year';
            }
            //Deposit percent
            if($deposit_percent>0){
                $percent =round($deposit_percent,2)."%";
            }else{
                $percent='';
            }

            if($row->car_model_year==0||$row->car_model_year == ''){
                $model_year = '';
            }else{
                $model_year = $row->car_model_year;
            }

            if($row->car_fob_cost==0||$row->car_fob_cost == ''){
                $fob_cost = '';
            }else{
                $fob_cost = $row->car_fob_currency." ".$row->car_fob_cost;
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $row->car_stock_no)
                ->setCellValue('B'.$i, $row->country_name)
                ->setCellValue('C'.$i, $row->car_city)
                ->setCellValue('D'.$i, $row->icon_status)
                ->setCellValue('E'.$i, $row->car_model)
                ->setCellValue('F'.$i, $row->car_chassis_no)
                ->setCellValue('G'.$i, $model_year)
                ->setCellValue('H'.$i, $fob_cost)
                ->setCellValue('I'.$i, ($total_sell_price >0 ? $row->car_actual_price_currency." ".$total_sell_price : ''))
                ->setCellValue('J'.$i, $row->payment_currency_type." ".$row->allocate_sum)
                ->setCellValue('K'.$i, $percent)
                ->setCellValue('L'.$i, $row->sales_name)
                ->setCellValue('M'.$i, $row->customer_first_name)
                ->setCellValue('N'.$i, $row->customer_last_name)
                ->setCellValue('O'.$i, $row->customer_email)
                ->setCellValue('P'.$i, $row->reserved_dt)
                ->setCellValue('Q'.$i, $row->car_price_type)
                ->setCellValue('R'.$i, (!empty($row->car_actual_price) ? $row->car_actual_price_currency." ".$row->car_actual_price : ''))
                ->setCellValue('S'.$i, (!empty($row->car_freight_fee) ? $row->car_freight_fee_currency." ".$row->car_freight_fee : '' ))
                ->setCellValue('T'.$i, $row->shipping_company)
                ->setCellValue('U'.$i, $row->vessel_name)
                ->setCellValue('V'.$i, $row->discharge)
                ->setCellValue('W'.$i, $row->port_leaved_date)
                ->setCellValue('X'.$i, $row->consignee_info)
                ->setCellValue('Y'.$i, $row->bl_no)
                ->setCellValue('Z'.$i, $row->dhl_no)
                ->setCellValue('AA'.$i, $row->invoice_no);
                
        }


        
        // Miscellaneous glyphs, UTF-8
       
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('STOCK LIST FOR STAFF');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="stock_list_for_staff('.$today_date.').xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

    }

    function adm_list_log($prev_next = 'N') {
        $nav_array = array(
            'Home'=>'/?c=admin',
            'Logs List'=>'/?c=admin&m=adm_list_log'
        );

        $where = array();

        $sParam = '';

        $sch_create_dt_s = $this->ci->input->get_post('sch_create_dt_s'); 

        if ($sch_create_dt_s != '') {

            $sParam .= '&sch_create_dt_s=' . $sch_create_dt_s;

            $where['log.created_dt >='] = $sch_create_dt_s . " 00:00:01";

        }

        $sch_create_dt_e = $this->ci->input->get_post('sch_create_dt_e');

        if ($sch_create_dt_e != '') {

            $sParam .= '&sch_create_dt_e=' . $sch_create_dt_e;

            $where['log.created_dt <='] = $sch_create_dt_e . " 23:59:59";

        }

        $sch_condition = $this->ci->input->get_post('sch_condition'); 

        if($sch_condition !='') {

            $where['log.log_type like'] = $sch_condition;

        }

        $sch_author = $this->ci->input->get_post('sch_author');     

        if($sch_author !='') {

            $where['m.member_id like'] = '%'. $sch_author .'%';

        }

        $sch_log_message = $this->ci->input->get_post('sch_log_message');     

        if($sch_log_message !='') {

            $where['log.message like'] = '%'. $sch_log_message .'%';

        }

        $cur_page = $this->ci->input->get_post("cur_page");

        if ($cur_page == NULL || $cur_page == '')

            $cur_page = 1;

        $page_rows = 20;

        $page_views =  10;
        
        $log_list =  $this->ci->bbs->select_log(($cur_page - 1) * $page_rows, $page_rows, $where);

        $file_couter = array();

        $rows = $this->ci->bbs->select_all_log($where);

        $total_rows = count($rows);

        $row_cnt = $total_rows - ($cur_page - 1) * $page_rows;          

        $page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);

        if ($prev_next == 'Y') {

            $pn_data = array();

            $pn_data['isPrev'] = '';

            $prev = ($cur_page - 1) * $page_rows;

            if ($prev > 0) {

                $pn_data['isPrev'] = "PREV";

                $prev = $prev - 1;

            }


            $pn_data['cur_page'] = $cur_page;

            $pn_data['page_views'] = $page_views;

            $pn_data['page_rows'] = $page_rows;

            $pn_data['total_rows'] = $total_rows;

            $pn_data['row_cnt'] = $row_cnt;


            if ($pn_data['isPrev'] == 'PREV') {

                $page_rows = $page_rows + 2;

            } else {

                $page_rows = $page_rows + 1;

            }

            $pn_data['prevList'] = $this->ci->bbs->select_log($prev, $page_rows, $where);


            return $pn_data;

        }

        func_set_data($this->ci, 'file_couter', $file_couter);          

        func_set_data($this->ci, 'total_rows', $total_rows);        

        func_set_data($this->ci, 'total_page', $page_info[0]);      

        func_set_data($this->ci, 'row_cnt', $row_cnt);              

        func_set_data($this->ci, 'cur_page', $cur_page);        

        func_set_data($this->ci, 'sParam', $sParam);

        func_set_data($this->ci, 'page_info', $page_info);               

        func_set_data($this->ci, 'nav_array', $nav_array);

        func_set_data($this->ci, 'log_list',$log_list);

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/log/list', $this->ci->data, true));
       
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
        
    }

    function adm_delete_log_exec(){
        
        $log_id = $this->ci->input->get_post('id');

        
        for($i=0;$i <count($log_id);$i++){

            $log_data = array(
                    'deleted_dt'=>func_get_date_time()
                ); 
            $result = $this->ci->mo_activity_log->delete($log_id[$i],$log_data);
        }

        if($result == true){

            echo func_jsAlertReplace('Deletion Successful!.', '?c=admin&m=adm_list_log');
            
        }
        
    }

    function adm_seller_web_list() {
        $nav_array = array(
            "Home"=>"/?c=admin",
            "Seller's Web"=>"/?c=admin&m=adm_seller_web_list"
        );
        
        $seller_list =  $this->ci->bbs->select_seller_web();
        

        func_set_data($this->ci, 'nav_array', $nav_array);

        func_set_data($this->ci, 'seller_list',$seller_list);

        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/seller_web/list', $this->ci->data, true));
       
        $this->ci->load->view('admin/adm_index', $this->ci->data, false);
        
    }

    function adm_modify_seler_web(){
        $msg ="";
        $data = array();

        $member_id = $this->ci->input->get_post('member_id');

        $data['member_no'] = $this->ci->input->get_post('selected_seller');

        $website_name = $this->ci->input->get_post('website_name');

        $data['fob_commission'] = $this->ci->input->get_post('fob_commission');
        
        if (isset($_POST['Save']))   {

            $member_id = $data['member_no'];

            if($member_id !=''){

                $this->ci->bbs->update_seller_web($website_name,$data);
                $msg ="You have modify the seller's web successfully!";

            }else{

                $msg ="Please select seller before you save!";
                
            }
        }

        $selected_seller_web = $this->ci->bbs->select_seller_web_by_id($member_id); 
        $select_seller = $this->ci->user_member->select_business_type_seller();

        func_set_data($this->ci, 'select_seller', $select_seller);
        func_set_data($this->ci, 'msg', $msg);

        func_set_data($this->ci, 'selected_seller_web', $selected_seller_web);
        $this->ci->load->view('admin/seller_web/modify', $this->ci->data, false);
    }

    function adm_validate_modify_seller_web(){
        $member_no = $this->ci->input->get_post('member_no');
        $change_supporter = $this->ci->bbs->validate_modify_seller_web($member_no);
        if ($change_supporter[0]->num_rows>0) {
            echo $msg = 1;
        }

        echo $msg = 0;
    }

    function adm_get_fob_discount(){
        $idx = $this->ci->input->get_post('idx');
        $fob_discount = $this->ci->bbs->select_detail_for_seller($idx);
        echo $fob_discount[0]->car_fob_cost;
    }

    function adm_upload_image_for_send_email(){
        if(isset($_FILES['file'])){
            if (!empty($_FILES['file']['name'])) {
                
                $errors     = array();
                $file_size  = $_FILES['file']['size'];
                $file_tmp   = $_FILES['file']['tmp_name'];
                $file_ext   = strtolower(end(explode('.',$_FILES['file']['name'])));
                $expensions = array("jpeg","jpg","png");
              
                if(in_array($file_ext,$expensions)=== false){
                    $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }
              
                if(empty($errors)==true){
                    $new_file_name = date("mdy-his.").$file_ext;
                    move_uploaded_file($file_tmp,"uploads/send_newsletter/".$new_file_name);
                    $icon_name = "/uploads/send_newsletter/".$new_file_name;
                    echo $icon_name;
                }
                else{
                    print_r($errors);
                }

            }
        }
    }

}

?>