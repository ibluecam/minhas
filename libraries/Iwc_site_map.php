<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Iwc_site_map.php
 * 개  요 : 사이트맵을 관리한다.
  ------------------------------------------------ */

class Iwc_site_map {

	var $ci;

//constructor
	function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();
	}

//사이트 맵 HTML
	function view_site_map() {

//처리구분
		$exec_type = $this->ci->input->get_post('exec_type');
		$select = '';
		$select_detail = '';
		$seq_no = '';

//사이트맵 조회
		$select = $this->ci->iw_config->select_site_map();

		if ($exec_type == 'UPDATE') {
			$seq_no = $this->ci->input->get_post('seq_no');
//사이트맵 상세 조회
			$select_detail = $this->ci->iw_config->select_detail_site_map($seq_no);

			func_set_data($this->ci, 'iw_site_map_detail_result', $select_detail->result());
		}

		func_set_data($this->ci, 'iw_site_map_result', $select->result());
		func_set_data($this->ci, 'exec_type', $exec_type == '' ? 'INSERT' : $exec_type);
		func_set_data($this->ci, 'seq_no', $seq_no);
		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/site_map/iwc_site_map', $this->ci->data, true)); //컨텐츠
//VIEW 로드
		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);
	}

//사이트 맵 등록&수정 EXEC
	function exec_site_map() {
//처리구분
		$exec_type = $this->ci->input->get_post('exec_type');

//POST 배열을 array['key'] = value 형태로 리턴
		$inData = func_get_post_array($this->ci->input->post('values'));

		$menu_code_grp = $inData['menu_code_grp']; //메뉴레벨 '0' 상위메뉴
//Default 상위 메뉴 선택시
		if ($menu_code_grp == '0') {
			$inData['menu_code_order'] = $this->ci->iw_config->select_max_menu_code_order();
		}
//생성된 상위 메뉴 선택시
		else {
//선택된 부모 메뉴코드를 조회한다.
			$result = $this->ci->iw_config->select_parents_menu_code($inData['menu_code_grp']);

			if (count($result) > 0) {
//등록시
				if ($exec_type != 'UPDATE') {
//메뉴코드 입력시 정렬 순서를 수정한다.
					$this->ci->iw_config->update_menu_code_order($result[0]->menu_code_order, NULL);
					$inData['menu_code_order'] = $result[0]->menu_code_order + 1;
				}
//수정시
				else {
//자기자신을 선택하지 않은 경우..
					if ($result[0]->menu_code != $inData['menu_code']) {
//메뉴코드 입력시 정렬 순서를 수정한다.
						$this->ci->iw_config->update_menu_code_order($result[0]->menu_code_order, $inData['menu_code_order']);
						$inData['menu_code_order'] = $result[0]->menu_code_order + 1;
					}
				}
			}
		}

		$inData['menu_url'] = '/?c=user&amp;mcd=' . $inData['menu_code'];
		$inData['created_dt'] = func_get_date_time();
		unset($inData['menu_code_grp']);

//등록
		if ($exec_type != 'UPDATE') {
			$this->ci->iw_config->insert_site_map($inData);

//컨텐츠 파일 /views/user/contents/ 경로에 해당 디렉토리 및 파일 생성
			if ($inData['menu_type'] == 'contents') {
//컨텐츠 디렉토리 경로
				$sel_dir = 'views/user/contents/' . $inData['menu_code'];
//컨텐츠 상단 경로
				$sel_dir_top = 'views/user/contents_top/' . $inData['menu_code'];
//컨텐츠 디렉토리 생성
				func_make_dir($sel_dir);
//컨텐츠 상단 디렉토리 생성
				func_make_dir($sel_dir_top);
//컨텐츠 파일 생성
				write_file($sel_dir . '/' . $inData['menu_code'] . '.php', '<h1>' . $inData['menu_code_name'] . '</h1>', 'wb');
				@chmod($sel_dir . '/' . $inData['menu_code'] . '.php', 0777);
//컨텐츠 상단 파일 생성
				write_file($sel_dir_top . '/' . $inData['menu_code'] . '.php', '<div style="text-align:right">' . $inData['menu_code_name'] . '</div>', 'wb');
				@chmod($sel_dir_top . '/' . $inData['menu_code'] . '.php', 0777);
			}

//LEFT MENU 생성
			$this->_create_left_menu();

			echo func_jsAlertReplace('메뉴가 등록 되었습니다.', '/?c=iwc&amp;m=view_site_map');
		} else {
			$seq_no = $this->ci->input->get_post('seq_no');
			$this->ci->iw_config->update_site_map($inData, $seq_no);

//컨텐츠 파일 /views/user/contents/ 경로에 해당 디렉토리 및 파일 생성
			if ($inData['menu_type'] == 'contents') {
//컨텐츠 디렉토리 경로
				$sel_dir = 'views/user/contents/' . $inData['menu_code'];
//컨텐츠 상단 경로
				$sel_dir_top = 'views/user/contents_top/' . $inData['menu_code'];

				if (!is_dir($sel_dir)) {
//컨텐츠 디렉토리 생성
					func_make_dir($sel_dir);
//컨텐츠 상단 디렉토리 생성
					func_make_dir($sel_dir_top);
//컨텐츠 파일 생성
					write_file($sel_dir . '/' . $inData['menu_code'] . '.php', '<h1>' . $inData['menu_code_name'] . '</h1>', 'wb');
					@chmod($sel_dir . '/' . $inData['menu_code'] . '.php', 0777);
//컨텐츠 상단 파일 생성
					write_file($sel_dir_top . '/' . $inData['menu_code'] . '.php', '<div style="text-align:right">' . $inData['menu_code_name'] . '</div>', 'wb');
					@chmod($sel_dir_top . '/' . $inData['menu_code'] . '.php', 0777);
				}
			}

//LEFT MENU 생성
//$this->_create_left_menu();

			echo func_jsAlertReplace('메뉴가 수정 되었습니다.', '/?c=iwc&amp;m=view_site_map');
		}
	}

//사이트 맵 삭제 EXEC
	function exec_delete_site_map() {
//POST 배열을 array['key'] = value 형태로 리턴
		$inData = func_get_post_array($this->ci->input->post('values'));

		$seq_no = $this->ci->input->get_post('seq_no');

		if ($this->ci->iw_config->delete_site_map($seq_no)) {
//컨텐츠 파일 /views/user/contents/ 경로에 해당 디렉토리 및 파일 삭제
			if ($inData['menu_type'] == 'contents') {
//디렉토리 경로
				$sel_dir = 'views/user/contents/' . $inData['menu_code'];
				$sel_top_dir = 'views/user/contents_top/' . $inData['menu_code'];
//파일 삭제
				delete_files($sel_dir, TRUE);
				delete_files($sel_top_dir, TRUE);
//디렉토리 삭제
				@rmdir($sel_dir);
				@rmdir($sel_top_dir);
			}
//게시판 파일 /views/user/bbs/ 경로에 해당 디렉토리 및 파일 삭제
			if ($inData['menu_type'] == 'bbs') {
//사용자 디렉토리 경로
				$sel_dir = 'views/user/bbs/' . $inData['menu_code'];
//사용자 파일 삭제
				delete_files($sel_dir, TRUE);
//사용자 디렉토리 삭제
				@rmdir($sel_dir);

				unset($sel_dir);

//사용자 써머리 디렉토리 경로
				$sel_dir = 'views/user/bbs/summary_' . $inData['menu_code'];
//사용자 써머리 파일 삭제
				delete_files($sel_dir, TRUE);
//사용자 써머리 디렉토리 삭제
				@rmdir($sel_dir);

				unset($sel_dir);
//관리자 디렉토리 경로
				$sel_dir = 'views/admin/adm/bbs/' . $inData['menu_code'];
//관리자 파일 삭제
				delete_files($sel_dir, TRUE);
//관리자 디렉토리 삭제
				@rmdir($sel_dir);
			}

//LEFT MENU 생성
//$this->_create_left_menu();

			echo func_jsAlertReplace('메뉴가 삭제 되었습니다.', '/?c=iwc&amp;m=view_site_map');
		} else {
			echo func_jsAlertReplace('메뉴 삭제가 실패 되었습니다', '/?c=iwc&amp;m=view_site_map');
		}
	}

//사이트 맵 메뉴코드 중복 체크
	function exec_dup_chk_menu_code() {
		$menu_code = $this->ci->input->get_post('menu_code');

		$cnt = $this->ci->iw_config->select_chk_menu_code($menu_code);

//해당 메뉴코드가 존재한다면...
		if ($cnt > 0) {
			echo trim("FAIL");
		} else {
			echo trim("SUCCESS");
		}
	}

//LEFT 메뉴 파일 생성
	function _create_left_menu() {
//사이트맵 조회
		$result = $this->ci->iw_config->select_site_map()->result();

//파일명
		$file_name = 'left_menu.php';

//사이트맵 String 생성
		$str = '';
		if (count($result) > 0) {
			$str .= "<!-- 아래 LEFT 메뉴 소스는 사이트 맵 생성 및 수정시 코드가 생성 됩니다. -->\r\n";
			$str .= "<!-- 메뉴그룹(회사소개) 코드 -> noa 이면  그 하위메뉴들의 메뉴코드에는 -> noa 포함(연혁 -> noa0001) 되어야 합니다. -->\r\n";
			$str .= "<!-- 위 기준대로 사이트맵을 작성하지 않은 경우 각각의 if 조건문에 OR 조건 -> 예를들어 || strstr(\$mcd, 'noa0001') 코드 삽입합니다. -->\r\n\r\n";
			foreach ($result as $rows) {
				if ($rows->menu_code_level == '0') {
					$str .= "<!-- $rows->menu_code_name 메뉴그룹 시작 -->\r\n";
					$str .= "<?php\r\n";
					$str .= "if(strstr(\$mcd, '$rows->menu_code'))\r\n{\r\n?>\r\n";
					$str .= "\t<!--해당 위치에 [" . $rows->menu_code_name . "] 메뉴 그룹에 해당하는 하의 메뉴들의 HTML 또는 FLASH 메뉴를 삽입하세요.-->\r\n";
					$str .= "<?\r\n}\r\n?>\r\n";
					$str .= "<!-- //$rows->menu_code_name 메뉴그룹 끝 -->\r\n\r\n";
				}
			}
		}

//디렉토리 경로
		$sel_dir = 'views/user/left_menu/';

//파일 저장
		copy($sel_dir . '/' . $file_name, $sel_dir . '/' . $file_name . "." . date("YmdHis"));
		write_file($sel_dir . '/' . $file_name, $str, 'wb');
		chmod($sel_dir . '/' . $file_name, 0777);
	}

}
?>
