<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Iwc_member.php
 * 개  요 : 회원관리
  ------------------------------------------------ */

class Iwc_member {

	var $ci;

//constructor
	function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();

		$this->ci->data['member_join_mcd'] = '';
		$this->ci->data['member_login_mcd'] = '';

//전체 사이트 맵
		$sitemap = $this->ci->data['site_map_list'];
		if (count($sitemap) > 0) {
			foreach ($sitemap as $rows) {
//회원관련 모듈의 메뉴코드_회원가입
				if (strstr($rows->menu_type, 'member_join')) {
					func_set_data($this->ci, 'member_join_mcd', $rows->menu_code);
				}
//회원관련 모듈의 메뉴코드_로그인
				if (strstr($rows->menu_type, 'member_login')) {
					func_set_data($this->ci, 'member_login_mcd', $rows->menu_code);
				}
			}
		}
	}

//회원가입 설정 정보를 조회한다.
	function select_member_join_config() {
//회원 설정정보 조회
		$query = $this->ci->iw_config->select_member_join_config();
		func_set_data($this->ci, 'member_config_result', $query->result());

		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/member/iwc_member_join_config', $this->ci->data, true));

		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);
	}

//회원가입 설정 정보 EXEC
	function exec_member_join_config() {
		$inData = array();
		$inData['member_join_code'] = $this->ci->input->post('member_join_mcd'); //회원가입 코드
		$skin_cover = $this->ci->input->post('skin_cover'); //덮어쓰기


		$array_values = $this->ci->input->post('values');  //폼값
//key 값을 array로 리턴
		$keys = func_get_array_keys($array_values);

//설정 정보 DB 등록
		for ($i = 0; $i < count($keys); $i++) {
			$inData['key'] = $keys[$i];
			$inData['value'] = $array_values[$keys[$i]];
			$inData['created_dt'] = func_get_date_time();

			$tmp = $this->ci->iw_config->select_detail_member_join_config($inData['member_join_code'], $inData['key']);

			if ($tmp->num_rows() > 0) {
				$this->ci->iw_config->update_member_join_config($inData);
			} else {
				$this->ci->iw_config->insert_member_join_config($inData);
			}
		}

//회원가입 설정 값
		$member_join_conf = func_get_post_array($this->ci->input->post('values')); //POST 배열을 array['key'] = value 형태로 리턴

		if ($this->ci->data['member_join_mcd'] != '') {

//관리자 회원가입 메뉴코드 폴더가 존재하지 않으면...
			if (!is_dir('views/admin/adm/member/' . $inData['member_join_code'])) {
//관리자 회원 메뉴코드 폴더 생성
				func_make_dir('views/admin/adm/member/' . $inData['member_join_code']);

//관리자 회원 스킨 파일 복사
//func_file_copy('views/admin/adm/member/skin/'.$member_join_conf['member_join_skin'], 'views/admin/adm/member/'.$inData['member_join_code']);
				func_file_copy('views/admin/adm/member/skin/basic', 'views/admin/adm/member/' . $inData['member_join_code']);
			}

//사용자 회원가입 메뉴코드 폴더가 존재하지 않으면...
			if (!is_dir('views/user/member_join/' . $inData['member_join_code'])) {
				$join_skin_folder = $member_join_conf['member_join_skin'];

//사용자 메뉴코드 폴더 및 이미지 폴더 생성
				func_make_dir('views/user/member_join/' . $inData['member_join_code']);
				func_make_dir('views/user/member_join/' . $inData['member_join_code'] . '/images');

//사용자 스킨 파일 및 이미지 파일 복사
				func_file_copy('views/user/member_join/skin/' . $join_skin_folder, 'views/user/member_join/' . $inData['member_join_code']);
				func_file_copy('views/user/member_join/skin/' . $join_skin_folder . '/images', 'views/user/member_join/' . $inData['member_join_code'] . '/images');
			}

//스킨 덮어쓰기 선택시
			if (isset($skin_cover)) {
				if ($skin_cover == 'Y') {
//관리자 디렉토리 경로
					$sel_dir = 'views/admin/adm/member/' . $inData['member_join_code'];
//관리자 파일 삭제
					delete_files($sel_dir, TRUE);
//관리자 디렉토리 삭제
					@rmdir($sel_dir);

					unset($sel_dir);

//사용자 디렉토리 경로
					$sel_dir = 'views/user/member_join/' . $inData['member_join_code'];
//사용자 파일 삭제
					delete_files($sel_dir, TRUE);
//사용자 디렉토리 삭제
					@rmdir($sel_dir);

//관리자 회원 메뉴코드 폴더 생성
					func_make_dir('views/admin/adm/member/' . $inData['member_join_code']);

//관리자 회원 스킨 파일 복사
//func_file_copy('views/admin/adm/member/skin/'.$member_join_conf['member_join_skin'], 'views/admin/adm/member/'.$inData['member_join_code']);
					func_file_copy('views/admin/adm/member/skin/basic', 'views/admin/adm/member/' . $inData['member_join_code']);

					$join_skin_folder = $member_join_conf['member_join_skin'];

//사용자 메뉴코드 폴더 및 이미지 폴더 생성
					func_make_dir('views/user/member_join/' . $inData['member_join_code']);
					func_make_dir('views/user/member_join/' . $inData['member_join_code'] . '/images');

//사용자 스킨 파일 및 이미지 파일 복사
					func_file_copy('views/user/member_join/skin/' . $join_skin_folder, 'views/user/member_join/' . $inData['member_join_code']);
					func_file_copy('views/user/member_join/skin/' . $join_skin_folder . '/images', 'views/user/member_join/' . $inData['member_join_code'] . '/images');
				}
			}
		}

//리턴 URL
		$return_url = '/?c=iwc&amp;m=view_member_join_config';
		echo func_jsAlertReplace('회원관리 설정 정보가 등록 되었습니다.', $return_url);
	}

//회원 로그인 설정 정보를 조회한다.
	function select_member_login_config() {
//회원 로그인 정보 조회
		$query = $this->ci->iw_config->select_member_login_config();
		func_set_data($this->ci, 'member_config_result', $query->result());

		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/member/iwc_member_login_config', $this->ci->data, true));

		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);
	}

//회원 로그인 설정 정보 EXEC
	function exec_member_login_config() {
		$inData = array();
		$inData['member_login_code'] = $this->ci->input->post('member_login_mcd'); //로그인 코드
		$skin_cover = $this->ci->input->post('skin_cover'); //덮어쓰기

		$array_values = $this->ci->input->post('values');  //폼값
//key 값을 array로 리턴
		$keys = func_get_array_keys($array_values);

//설정 정보 DB 등록
		for ($i = 0; $i < count($keys); $i++) {
			$inData['key'] = $keys[$i];
			$inData['value'] = $array_values[$keys[$i]];
			$inData['created_dt'] = func_get_date_time();

			$tmp = $this->ci->iw_config->select_detail_member_login_config($inData['member_login_code'], $inData['key']);

			if ($tmp->num_rows() > 0) {
				$this->ci->iw_config->update_member_login_config($inData);
			} else {
				$this->ci->iw_config->insert_member_login_config($inData);
			}
		}

//로그인 설정 값
		$member_login_conf = func_get_post_array($this->ci->input->post('values')); //POST 배열을 array['key'] = value 형태로 리턴

		if ($this->ci->data['member_login_mcd'] != '') {
//사용자 로그인 메뉴코드 폴더가 존재하지 않으면...
			if (!is_dir('views/user/member_login/' . $inData['member_login_code'])) {
				$login_skin_folder = $member_login_conf['member_login_skin'];

//사용자 메뉴코드 폴더 및 이미지 폴더 생성
				func_make_dir('views/user/member_login/' . $inData['member_login_code']);
				func_make_dir('views/user/member_login/' . $inData['member_login_code'] . '/images');

//사용자 스킨 파일 및 이미지 파일 복사
				func_file_copy('views/user/member_login/skin/' . $login_skin_folder, 'views/user/member_login/' . $inData['member_login_code']);
				func_file_copy('views/user/member_login/skin/' . $login_skin_folder . '/images', 'views/user/member_login/' . $inData['member_login_code'] . '/images');
			}

//스킨 덮어쓰기 선택시
			if (isset($skin_cover)) {
				if ($skin_cover == 'Y') {

//사용자 디렉토리 경로
					$sel_dir = 'views/user/member_login/' . $inData['member_login_code'];
//사용자 파일 삭제
					delete_files($sel_dir, TRUE);
//사용자 디렉토리 삭제
					@rmdir($sel_dir);

					$login_skin_folder = $member_login_conf['member_login_skin'];

//사용자 메뉴코드 폴더 및 이미지 폴더 생성
					func_make_dir('views/user/member_login/' . $inData['member_login_code']);
					func_make_dir('views/user/member_login/' . $inData['member_login_code'] . '/images');

//사용자 스킨 파일 및 이미지 파일 복사
					func_file_copy('views/user/member_login/skin/' . $login_skin_folder, 'views/user/member_login/' . $inData['member_login_code']);
					func_file_copy('views/user/member_login/skin/' . $login_skin_folder . '/images', 'views/user/member_login/' . $inData['member_login_code'] . '/images');
				}
			}
		}

//리턴 URL
		$return_url = '/?c=iwc&amp;m=view_member_login_config';
		echo func_jsAlertReplace('로그인 설정 정보가 등록 되었습니다.', $return_url);
	}

//회원등급 HTML
	function view_member_grade() {
//처리구분
		$exec_type = $this->ci->input->get_post('exec_type');
		$select = '';
		$select_detail = '';
		$seq_no = '';

//회원등급 조회
		$select = $this->ci->member->member_grade_select();

		if ($exec_type == 'UPDATE') {
			$seq_no = $this->ci->input->get_post('seq_no');
//회원등급 상세 조회
			$select_detail = $this->ci->member->member_grade_select_detail($seq_no);

			func_set_data($this->ci, 'member_grade_detail_result', $select_detail);
		}

		func_set_data($this->ci, 'member_grade_result', $select->result());
		func_set_data($this->ci, 'exec_type', $exec_type == '' ? 'INSERT' : $exec_type);
		func_set_data($this->ci, 'seq_no', $seq_no);
		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/member/iwc_member_grade', $this->ci->data, true)); //컨텐츠
//VIEW 로드
		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);
	}

//회원등급 등록&수정 EXEC
	function exec_member_grade() {
//처리구분
		$exec_type = $this->ci->input->get_post('exec_type');

//POST 배열을 array['key'] = value 형태로 리턴
		$inData = func_get_post_array($this->ci->input->post('values'));

		$inData['created_dt'] = func_get_date_time();

		if ($exec_type != 'UPDATE') {

			$this->ci->member->insert_member_grade($inData);
			echo func_jsAlertReplace('등록 되었습니다.', '/?c=iwc&amp;m=view_member_grade');
		} else {
			$seq_no = $this->ci->input->get_post('seq_no');
			$this->ci->member->update_member_grade($inData, $seq_no);
			echo func_jsAlertReplace('수정 되었습니다.', '/?c=iwc&amp;m=view_member_grade');
		}
	}

//회원등급 삭제
	function exec_delete_member_grade() {
		$seq_no = $this->ci->input->get_post('seq_no');

		$this->ci->member->delete_member_grade($seq_no);

		echo func_jsAlertReplace('삭제 되었습니다.', '/?c=iwc&amp;m=view_member_grade');
	}

}
?>