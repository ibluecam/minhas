<?php
class Adm_invoice {
	
	var $ci;

	var $mcd ='invoice';

	var $bbs_config = NULL;
	var $frieght_cost_plus    = 250;
    var $inspection_cost_plus = 100;
    var $insurance_cost_plus  = 50;


	//constructor

	function __construct() {

		//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당

		$this->ci = &get_instance();

	    /* ------------------------------------------------------------



		  - Model Load



		  ------------------------------------------------------------ */



		$this->ci->load->model('mo_invoice', 'user_invoice'); //class명과 동일해서 user_invoice

		$this->ci->load->model('mo_survey', 'survey'); // 설문조사

		$this->ci->load->model('mo_member', 'user_member'); // 맴버 

		$this->ci->load->model('mo_bbs', 'user_bbs');

		$this->ci->load->model('mo_customer', 'mo_customer');

		$this->ci->load->model('mo_shipping', 'mo_shipping');

		$this->ci->load->model('mo_activity_log','mo_activity_log');
		


	}
	
	// Search engine  Document 
	function adm_delete_invoice_by_no(){
		$invoice_nos =explode(";", $this->ci->input->get_post('invoice_no'));
		$this->ci->user_invoice->delete_invoice_by_no($invoice_nos);
		
	}
	function adm_invoice_list($prev_next = 'N'){
		
		$where = array(); 		//sql where 조건

		$sParam = '';	  		//검색 string	

		$sch_condition = $this->ci->input->get_post('sch_condition');   		//검색조건

		$sch_word = $this->ci->input->get_post('sch_word');				//검색어

		//var_dump($_POST['sch_condition']); exit();

		//검색조건 + 검색어

		if ($sch_condition != '' && $sch_word != '') {

			$sch_condition_tmp = explode('|', $sch_condition);


			//다중 조건인 경우(제목 + 내용)

			if (count($sch_condition_tmp) > 1) {

				$tmpStr = '';

				for ($i = 0; $i < count($sch_condition_tmp); $i++) {

					if ($tmpStr != '') {

						$tmpStr .= ' or ';

					}



					$tmpStr .= $sch_condition_tmp[$i] . ' ? ';

					$where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';

				}



				$tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

				$where[$tmpStr] = '';


				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;
	

			} else {

				$where[$sch_condition] = '%' . $sch_word . '%';

				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			}

		}

		//var_dump($where); exit();

		
		//체크박스 조건
		$sch_checkbox = array();
		$sch_checkbox = ($this->ci->input->get_post('sch_checkbox'));

		if ($sch_checkbox != '') {
			//var_dump(count($sch_checkbox));
			//var_dump($_POST); exit();

			if(count($sch_checkbox) > 0)
			{
				$tmpStr = '';

				for($i = 0; $i < count($sch_checkbox); $i++)
				{
					if ($tmpStr != '') {

						$tmpStr .= ' or ';

					}
					//echo $sch_checkbox[$i];
					$chk_condition = $sch_checkbox[$i] ;

					//$tmpStr .= $chk_condition[$i] . ' ? ';
					
					$where[$chk_condition] = 'Y';
				}

			}
		}

	


		//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 18;

		$page_views =  10;


		//게시판 공지 목록 조회
		if(isset($_POST['invoice_no'])){
			$invoice_no = $this->ci->input->get_post("invoice_no");
		}else{
			$invoice_no = '';
		}
		

		$invoice_list = $this->ci->user_invoice->select($invoice_no , ($cur_page - 1) * $page_rows, $page_rows, $where);
		
		$file_couter = array();
		

		
		$total_rows = $this->ci->user_invoice->total_select(); 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		//글 번호

		//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



		//이전글 다음글 조회

		if ($prev_next == 'Y') {

			$pn_data = array();

			$pn_data['isPrev'] = '';



			$prev = ($cur_page - 1) * $page_rows;



			if ($prev > 0) {

				$pn_data['isPrev'] = "PREV";

				$prev = $prev - 1;

			}



			$pn_data['cur_page'] = $cur_page;

			$pn_data['page_views'] = $page_views;

			$pn_data['page_rows'] = $page_rows;

			$pn_data['total_rows'] = $total_rows;

			$pn_data['row_cnt'] = $row_cnt;



			if ($pn_data['isPrev'] == 'PREV') {

				$page_rows = $page_rows + 2;

			} else {

				$page_rows = $page_rows + 1;

			}

			$pn_data['prevList'] = $this->ci->bbs->select($invoice_no, $prev, $page_rows, $where)->result();



			return $pn_data;

		}

		func_set_data($this->ci, 'file_couter', $file_couter);   		//file

		func_set_data($this->ci, 'total_rows', $total_rows);   		//조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); 		//전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		//글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   		//현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   		//검색 string
		
		func_set_data($this->ci, 'mcd', $this->mcd);
		
		func_set_data($this->ci, 'invoice_list', $invoice_list->result());  

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/invoice/list', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
		
	}
		
	//Search engine  adm_invoice_view
	
	function adm_invoice_view($prev_next = 'N'){
	
			
		$where = array(); 		//sql where 조건

		$sParam = '';	  		//검색 string

		$mcd = $this->ci->input->get_post("mcd");

		//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 18;
		//(integer) func_get_config($this->bbs_config, 'page_rows');  		//페이지 로우수

		//var_dump($page_rows); exit();

		$page_views =  10;
		
		//(integer) func_get_config($this->bbs_config, 'page_views'); 		//페이지 네비게이션 수

		//게시판 공지 목록 조회
		
		$invoice_no = $this->ci->input->get_post("invoice_no");

		//$bbs_notice_list = $this->ci->user_invoice->select($invoice_no,($cur_page - 1) * $page_rows, $page_rows, $where);
		
		//$bbs_attach_list = $this->ci->user_invoice->select_attach($invoice_no);



		//게시판 일반 목록 조회

		//$bbs_list = $this->ci->user_invoice->select($invoice_no , ($cur_page - 1) * $page_rows, $page_rows, $where);
		$bbs_list = $this->ci->user_invoice->select_invoice_car($invoice_no);
		

		$total_rows = $this->ci->user_invoice->total_select(); 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		//글 번호

		//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



		//이전글 다음글 조회

		if ($prev_next == 'Y') {

			$pn_data = array();

			$pn_data['isPrev'] = '';



			$prev = ($cur_page - 1) * $page_rows;



			if ($prev > 0) {

				$pn_data['isPrev'] = "PREV";

				$prev = $prev - 1;

			}



			$pn_data['cur_page'] = $cur_page;

			$pn_data['page_views'] = $page_views;

			$pn_data['page_rows'] = $page_rows;

			$pn_data['total_rows'] = $total_rows;

			$pn_data['row_cnt'] = $row_cnt;



			if ($pn_data['isPrev'] == 'PREV') {

				$page_rows = $page_rows + 2;

			} else {

				$page_rows = $page_rows + 1;

			}

			$pn_data['prevList'] = $this->ci->bbs->select($invoice_no, $prev, $page_rows, $where)->result();



			return $pn_data;

		}



		func_set_data($this->ci, 'total_rows', $total_rows);   		//조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); 		//전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		//글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   		//현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   		//검색 string
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		//func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list);

		func_set_data($this->ci, 'bbs_list', $bbs_list->result());
		
		//func_set_data($this->ci, 'bbs_attach_list',$bbs_attach_list->result());

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/invoice/view', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	
	
	}

	// Search engine Profoma Invoice 

	function adm_proforma_invoice(){

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/invoice/proforma_invoice', $this->ci->data, true));

		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	}
	
	// Search engine Document detail 
	
	function adm_invoice_detail(){
	
/*		$idx = $this->ci->input->get_post('idx');
		
		if( is_array($idx)){
		
			$idx = $idx[0];
			
		}*/
		
		$where = array(); 		//sql where 조건

		$sParam = '';	  		//검색 string

		$mcd = $this->ci->input->get_post("mcd");

		//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 18;
		//(integer) func_get_config($this->bbs_config, 'page_rows');  		//페이지 로우수

		//var_dump($page_rows); exit();

		$page_views =  10;
		
		//(integer) func_get_config($this->bbs_config, 'page_views'); 		//페이지 네비게이션 수

		//게시판 공지 목록 조회
		
		$invoice_no = $this->ci->input->get_post("invoice_no");

		$bbs_notice_list = $this->ci->user_invoice->select($invoice_no,($cur_page - 1) * $page_rows, $page_rows, $where);
		
		$bbs_attach_list = $this->ci->user_invoice->select_attach($invoice_no);



		//게시판 일반 목록 조회

		//$bbs_list = $this->ci->user_invoice->select($invoice_no , ($cur_page - 1) * $page_rows, $page_rows, $where);
		$bbs_list = $this->ci->user_invoice->select_bl_car($invoice_no);
		//var_dump($bbs_list); exit();

		$total_rows = $this->ci->user_invoice->total_select(); 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		//글 번호

		//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



		
		func_set_data($this->ci, 'total_rows', $total_rows);   		//조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); 		//전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		//글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   		//현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   		//검색 string
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		func_set_data($this->ci,'invoice_no', $invoice_no);

		func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list);

		func_set_data($this->ci, 'bbs_list', $bbs_list->result());
		
		func_set_data($this->ci, 'bbs_attach_list',$bbs_attach_list->result());

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/invoice/modify', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
		
	
	}



	//Search engine  Document register
	// function adm_invoice_register(){
		
	// 	$invoice_no = $this->ci->input->get_post("invoice_no");
		
	// 	if( is_array($invoice_no)){
		
	// 		$invoice_no = $invoice_no[0];
			
	// 	}

		//echo "Ok";
		// $this->ci->load->view('admin/adm/invoice/write.php', $this->ci->data, false);

		//$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	
	// }
	
	//Search engine  Document register_exec
	// function adm_invoice_register_exec(){
	
	// 	//$idx = $this->ci->input->get_post('idx');
	// 	$invoice_no = $this->ci->input->get_post('invoice_no');
		
		
	// 	$inData = array();

	// 	//$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴
		
	// 	$inData['invoice_no'] = $invoice_no	;		//게시물 일련번호
		
	// 	//$inData['icon_status'] = 'releaseok'	;	all icon_status will be decided by sales manually	
		
	// 	$this->ci->user_invoice->update($inData); 		//첨부파일 삭제
		
	// 	// count image upload
		
	// 	$num_imageUpload = count($_FILES['attach1']['name']);
		
		
	// 	for ($i = 0; $i < $num_imageUpload; $i++) {
		
	// 	//선택된 첨부 파일만...

	// 			if ($_FILES['attach1']['tmp_name'][$i] != '') {

	// 				//파일 업로드

	// 				$upload_str = $this->ci->iwc_common->upload('attach1',$i , $this->mcd, $this->bbs_config);

	// 				//파일 업로드 실패

	// 				if ($upload_str != TRUE) {

	// 					show_error($this->ci->iwc_common->upload_error_msg());

	// 				}

	// 				//파일 업로드 성공

	// 				else {

	// 					$upload_file_info = array();

	// 					$upload_file_info = $this->ci->iwc_common->upload_success_msg();



	// 					//첨부파일 DB 등록

	// 					$upload_file_info['invoice_no'] = $invoice_no;
						
	// 					$upload_file_info['menu_code'] = $this->mcd;

	// 					$this->ci->user_invoice->insert_attach($upload_file_info);

	// 				}

	// 			}
	
	// 	}
		
	// 	func_set_data($this->ci, 'invoice_no', $invoice_no);
		
	// 	//print_r($upload_file_info);
		
	// 	echo func_jsAlertReplace('invoice has been upload successfully .', '?c=admin&m=adm_invoice_register&mcd=document&invoice_no='.$invoice_no);
		
	// }



	
		
	//Search engine  Document modify
	function adm_invoice_modify(){
	
		$where = array(); 		//sql where 조건

		$sParam = '';	  		//검색 string
		
		$mcd = $this->ci->input->get_post("mcd");
	
		//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 18;
		//(integer) func_get_config($this->bbs_config, 'page_rows');  		//페이지 로우수

		//var_dump($page_rows); exit();

		$page_views =  10;
		
		//(integer) func_get_config($this->bbs_config, 'page_views'); 		//페이지 네비게이션 수

		//게시판 공지 목록 조회
		
		$invoice_no = $this->ci->input->get_post("invoice_no");
		
		if( is_array($invoice_no)){
		
			$invoice_no = $invoice_no[0];
			
		}

		$bbs_notice_list = $this->ci->user_invoice->select($invoice_no,($cur_page - 1) * $page_rows, $page_rows, $where);
		
		$bbs_attach_list = $this->ci->user_invoice->select_attach($invoice_no);



		//게시판 일반 목록 조회

		//$bbs_list = $this->ci->user_invoice->select($invoice_no , ($cur_page - 1) * $page_rows, $page_rows, $where);
		$bbs_list = $this->ci->user_invoice->select_bl_car($invoice_no);
		//var_dump($bbs_list); exit();

		$total_rows = $this->ci->user_invoice->total_select(); 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		//글 번호

		//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



		func_set_data($this->ci, 'total_rows', $total_rows);   		//조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); 		//전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		//글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   		//현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   		//검색 string
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		func_set_data($this->ci,'invoice_no', $invoice_no);

		func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list);

		func_set_data($this->ci, 'bbs_list', $bbs_list->result());
		
		func_set_data($this->ci, 'bbs_attach_list',$bbs_attach_list->result());

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/invoice/modify', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	
	}
	
	//Search engine  Document modify_exec
	function adm_invoice_modify_exec(){
	
		$idx = $this->ci->input->get_post('idx');
		
		
		$inData = array();

		$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴
		
		$inData['idx'] = $idx	;		//게시물 일련번호
		
		$this->ci->user_invoice->update($inData); 		//첨부파일 삭제
		
		// count image upload
		
		$num_imageUpload = count($_FILES['attach1']['name']);
		
		
		for ($i = 0; $i < $num_imageUpload; $i++) {
		
		//선택된 첨부 파일만...

				if ($_FILES['attach1']['tmp_name'][$i] != '') {

					//파일 업로드

					$upload_str = $this->ci->iwc_common->upload('attach1',$i , $this->mcd, $this->bbs_config);

					//파일 업로드 실패

					if ($upload_str != TRUE) {

						show_error($this->ci->iwc_common->upload_error_msg());

					}

					//파일 업로드 성공

					else {

						$upload_file_info = array();

						$upload_file_info = $this->ci->iwc_common->upload_success_msg();



						//첨부파일 DB 등록

						$upload_file_info['idx'] = $idx;
						
						$upload_file_info['menu_code'] = $this->mcd;

						$this->ci->user_invoice->insert_attach($upload_file_info);

					}

				}
	
		}
		
		func_set_data($this->ci, 'idx', $idx);
		
		echo func_jsAlertReplace('invoice has been upload successfully .', '?c=admin&m=adm_invoice_list');
		
	}


	//Search engine invoice_delete_exec
	function adm_invoice_delete_exec(){
	
		$mcd = $this->ci->input->get_post("mcd");
		
		$invoice_no = $this->ci->input->get_post("invoice_no");
		
		$att_idx = $this->ci->input->get_post("att_idx");
		
		$file_num = $this->ci->input->get_post("file_num");
				
		$where = array(); 		//sql where 조건

		$sParam = '';	  		//검색 string
	
		//$idx = $this->ci->input->get_post('idx');

		
		$att_idx = $this->ci->input->get_post('att_idx');
		
		$bbs_attach_detail_list = $this->ci->user_invoice->select_attach_detail($invoice_no, $att_idx); 		//첨부파일 상세 조회

		$return_result = $this->ci->user_invoice->delete_attach($invoice_no, $att_idx, $this->mcd); 		//첨부파일 삭제
		
		if(($return_result == true) && $file_num == 1){
			
			$inData = array();
	
			//$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴
			
			$inData['invoice_no'] = $invoice_no	;		//게시물 일련번호
			
			//$inData['icon_status'] = ''	;		all icon_status will be decided by sales manually
			
			$this->ci->user_invoice->update($inData); 		//첨부파일 삭제

			
		}

		//업로드 실제 첨부파일 삭제

		if ($bbs_attach_detail_list->num_rows() > 0) {

			foreach ($bbs_attach_detail_list->result() as $rows) {

				//첨부파일이 이미지인 경우 생성된 썸네일 까지 삭제

				@unlink($rows->full_path);

				
				$rows->full_path;

			}

		}
		
		
		//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 18;
		//(integer) func_get_config($this->bbs_config, 'page_rows');  		//페이지 로우수

		//var_dump($page_rows); exit();

		$page_views =  10;
		
		//(integer) func_get_config($this->bbs_config, 'page_views'); 		//페이지 네비게이션 수

		//게시판 공지 목록 조회

		$invoice_no = $this->ci->input->get_post("invoice_no");
		
		if( is_array($invoice_no)){
		
			$invoice_no = $invoice_no[0];
			
		}

		$bbs_notice_list = $this->ci->user_invoice->select($invoice_no,($cur_page - 1) * $page_rows, $page_rows, $where);
		
		$bbs_attach_list = $this->ci->user_invoice->select_attach($invoice_no);



		//게시판 일반 목록 조회

		//$bbs_list = $this->ci->user_invoice->select($invoice_no , ($cur_page - 1) * $page_rows, $page_rows, $where);
		$bbs_list = $this->ci->user_invoice->select_bl_car($invoice_no);
		//var_dump($bbs_list); exit();

		$total_rows = $this->ci->user_invoice->total_select(); 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		//글 번호

		//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



		func_set_data($this->ci, 'total_rows', $total_rows);   		//조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); 		//전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		//글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   		//현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   		//검색 string
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		func_set_data($this->ci,'invoice_no', $invoice_no);

		func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list);

		func_set_data($this->ci, 'bbs_list', $bbs_list->result());
		
		func_set_data($this->ci, 'bbs_attach_list',$bbs_attach_list->result());

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/invoice/modify', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
		
	}

	function adm_save_invoice_no(){
		$bl_nos = $this->ci->input->get_post("bl_nos");
		$invoice_no = $this->ci->input->get_post("invoice_no");
		if(count($bl_nos)>0&&!empty($invoice_no)){
			if($this->ci->user_invoice->save_invoice_no($invoice_no, $bl_nos)){
				echo "Invoice Saved!";
			}else{
				echo "Failed to insert invoice no.";
			}
		}else{
			echo "Failed to insert invoice no. Not enough data.";
		}
	}
	function adm_save_dhl_no(){
		$invoice_nos = $this->ci->input->get_post("invoice_nos");
		$dhl_no = $this->ci->input->get_post("dhl_no");
		if(count($invoice_nos)>0&&!empty($dhl_no)){
			if($this->ci->user_invoice->save_dhl_no($dhl_no, $invoice_nos)){
				echo "DHL Saved!";
			}else{
				echo "Failed to insert DHL no. Not enough data.";
			}
		}else{
			echo "Failed to insert DHL no.";
		}
	}
	function adm_invoice_excel_dwnl(){

		/** Error reporting */
		//error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);

		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

		date_default_timezone_set('Asia/Phnom_Penh');

		/** PHPExcel_IOFactory */
		require_once dirname(__FILE__) . '/../PHPExcel/PHPExcel-develop/Classes/PHPExcel/IOFactory.php';



		//echo date('H:i:s') , " Load from Excel5 template" , EOL;
		$objReader = PHPExcel_IOFactory::createReader('Excel5');
		$objPHPExcel = $objReader->load("PHPExcel/PHPExcel-develop/Examples/templates/blauda_invoice.xls");

		$invoice_no = $_GET['invoice_no'];
		$sql = "SELECT car_stock_no,
					   car_make,
					   car_model,
					   car_chassis_no,
					   car_color,
					   car_model_year,
					   car_actual_price,
					   car_freight_fee,
					   customer_no,
					   invoice_no,
					   DATE_FORMAT(port_leaved_date,'%Y-%m-%d') AS port_leaved_date,
					   car_price_type
				FROM iw_bbs WHERE invoice_no ='$invoice_no'";

		$result = mysql_query($sql);
		
		$i=0;
		while($rows = mysql_fetch_assoc($result)){
			$data[$i] = $rows;
			//var_dump($data[$i]);
			$i++;
		}


		//echo date('H:i:s') , " Add new data to the template" , EOL;
/*		$data = array(array('car_make'		=> 'KIA',
							'car_model'		=> 'Sorrento',
							'chassis_no'		=> '1245-2644',
							'manu_year'		=> 1998,
							'unit_price'	=> 52664
						   ),
					  array('car_make'		=> 'HYUNDAI',
							'car_model'		=> 'Sonata',
							'chassis_no'		=> '6643-2155',
							'manu_year'		=> 2008,
							'unit_price'	=> 65446
						   ),
					  array('car_make'		=> 'BMW',
							'car_model'		=> 'A6',
							'chassis_no'		=> '6656-4445',
							'manu_year'		=> 2015,
							'unit_price'	=> 55458
						   )

					 );*/
		$customer_no = $_GET['customer_no'];
		$qry = "SELECT a.member_first_name,
					   a.member_no,
					   a.address,
					   a.phone_no1,
					   b.country_name
				FROM iw_member a
				LEFT OUTER JOIN iw_country_list b
				ON a.member_country = b.cc
				WHERE a.member_no='$customer_no'";
		$value = mysql_query($qry);
		
		$data2 = mysql_fetch_assoc($value);
		

/*		$data2  = array('customer_name' => 'Raul Jeong',
					  		'customer_id'   => 2547,
					  		'address'		=> 'Incheon, Korea',
					  		'car_price_type' => 'CIF',
					  		'phone_no'      => 'TEL : 032-254-2658',
					  		'country'		=> 'Korea',
					  		'invoice_no'	=> '214-2568-22555',
					  		'port_leaved_date' => '2014-08-12'
					  		//'today_date'	=> '2015-02-15'
					  	   );*/
		//var_dump($data);
		//var_dump($data2);
		//$objPHPExcel->getActiveSheet()->setCellValue('D1', PHPExcel_Shared_Date::PHPToExcel(time()));

		$baseRow = 12;
		foreach($data as $r => $dataRow) {
			$row = $baseRow + $r;
			$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r+1)
										  ->setCellValue('B'.$row, $dataRow['car_stock_no'])
			                              ->setCellValue('C'.$row, $dataRow['car_make'])
			                              ->setCellValue('D'.$row, $dataRow['car_model'])
			                              ->setCellValue('E'.$row, $dataRow['car_chassis_no'])
			                              ->setCellValue('F'.$row, $dataRow['car_color'])
			                              ->setCellValue('G'.$row, $dataRow['car_model_year'])
			                              ->setCellValue('H'.$row, $dataRow['car_actual_price'])
			                              ->setCellValue('I'.$row, $dataRow['car_freight_fee'])
			                              ->setCellValue('J'.$row, $dataRow['car_actual_price']+$dataRow['car_freight_fee']);
			                              //->setCellValue('E'.$row, '=C'.$row.'*D'.$row);
		}


			$objPHPExcel->getActiveSheet()->setCellValue('C5',  $data2['member_first_name'])
			                              ->setCellValue('H5', $data2['member_no'])
			                              ->setCellValue('C6',  $data2['address'])
			                              ->setCellValue('H6', $data[0]['car_price_type'])
			                              ->setCellValue('C7', $data2['phone_no1'])
			                              ->setCellValue('H7', $data2['country_name'])
			                              ->setCellValue('C8', $data[0]['invoice_no'])
			                              ->setCellValue('H8', $data[0]['port_leaved_date']);
			                              //->setCellValue('AC13', $data2['today_date']);


		$objPHPExcel->getActiveSheet()->removeRow($baseRow-1,1);

		$today_date = date("Y-m-d");

		//echo date('H:i:s') , " Write to Excel5 format" , EOL;
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		if (!file_exists('c:/invoice/'.$today_date)) {
		    mkdir('c:/invoice/'.$today_date, 0777, true);
		}

		$objWriter->save('c:/invoice/'.$today_date.'/Invoice('.$data[0]['invoice_no'].').xls');         //var_dump('file name :'.str_replace('.php', '.xls', __FILE__));
		

		//echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;


		// Echo memory peak usage
		//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

		// Echo done
		//echo date('H:i:s') , " Done writing file" , EOL;
		echo 'File has been created in c:/invoice/'.$today_date, EOL;


		echo func_jsAlertReplace('Invoice has been downloaded successfully in C:/invoice/'.$today_date, '?c=admin&m=adm_invoice_view&mcd=invoice&invoice_no='.$invoice_no);

	}

	//*************Start Pro-Forma Invoice******************//

	function adm_invoice_register(){
        $idxs = $this->ci->input->get_post('idx');

        $where = array();
        $where['member.business_type ='] = 'buyer';

        $car_list = $this->ci->user_bbs->select_by_idxs($idxs);

        $customer_list = $this->ci->mo_customer->customer_select($where, 0, 10000);
        $list_destination = $this->ci->bbs->select_destination();

        func_set_data($this->ci, 'list_destination',$list_destination);
        func_set_data($this->ci, 'car_list', $car_list);
        func_set_data($this->ci, 'customer_list', $customer_list->result());


        func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm_register_invoice', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }

    function ceiling($number, $significance = 1)
    {
        return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
    }

	// function adm_invoice_register_exec(){
	// 	ob_clean();
 //    	/** Error reporting */
 //        error_reporting(E_ALL);
	// 	ini_set('display_errors', TRUE);
	// 	ini_set('display_startup_errors', TRUE);

	// 	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

	// 	date_default_timezone_set('Asia/Phnom_Penh');

	// 	require_once dirname(__FILE__) . '/../PHPExcel/PHPExcel-develop/Classes/PHPExcel.php';

 //        /** PHPExcel_IOFactory */
 //        require_once dirname(__FILE__) . '/../PHPExcel/PHPExcel-develop/Classes/PHPExcel/IOFactory.php';

	// 	$objPHPExcel = new PHPExcel();

 //        $objReader = PHPExcel_IOFactory::createReader('Excel5');

 //        $objPHPExcel = $objReader->load("PHPExcel/PHPExcel-develop/Examples/templates/invoice_new.xls");

 //        $idxs = $this->ci->input->get_post('idx');
 //        $customer_no = $this->ci->input->get_post('customer_no');
 //        $country_to = $this->ci->input->get_post('des_country');
 //        $port_name = $this->ci->input->get_post('portname');

 //        $discount = $this->ci->input->get_post('discount');
 //        $batch_data = array();
 //        $today_date = date("Y-m");

 //        foreach ($customer_no as $value) {

 //            if(!empty($value)){

 //                $batch['customer_no'] = $value;
 //                $batch['created_dt'] = func_get_date_time();
 //              	$j = 1;

 //                foreach ($idxs as $bbs_idx) {
 //                    //$customer_nos = $this->ci->user_invoice->check_exist_invoice($bbs_idx);
 //                    //if($customer_nos->num_rows==1){
 //                        //$validate = false;
 //                    //}else{
 //                    	$batch['full_path'] ='uploads/proforma_invoice/'.$today_date.'/proforma_invoice('.$j++.'-'.date("Y-m-d-H-i").').xls';
 //                        $batch['bbs_idx'] = $idx[] = $bbs_idx;
 //                  		$batch['discount_price'] = $discount[$bbs_idx];
 //                        $validate = true;
 //                        $batch_data[] = $batch;
 //                    //}
 //                }
 //            }
 //        }

 //        if($validate == true){

 //            $insert_id = $this->ci->user_invoice->insert_car_invoice_batch($batch_data);

 //            $invoice_idx = $this->ci->user_invoice->select_invoice_idx($idx,$discount,$country_to,$port_name);

 //            $get_customer = $this->ci->user_member->select_customer_no($customer_no[0]);

 //            $customer = $get_customer->member_first_name.' '.$get_customer->member_last_name.chr(10);
 //            $customer .= (!empty($get_customer->address) ? $get_customer->address.chr(10) : '');
 //            $customer .= (!empty($get_customer->phone_no1) ? 'TEL '.$get_customer->phone_no1.' '.$get_customer->phone_no2.' ' : '');
 //            $customer .= (!empty($get_customer->email) ? 'Email '.$get_customer->email : '');
            
 //            $i = 1;

 //            foreach ($invoice_idx as $row) {

 //            	$chassis_no     = $row->car_chassis_no;
 //            	$chassis_nos    = explode('-', $chassis_no);
 //            	$chassis        = $chassis_nos[0];

 //            	$year           = !empty($row->manu_month) ? date("F", strtotime(sprintf("%02s",$row->manu_month))).',' : '';
 //            	$year          .= !empty($row->car_model_year) ? $row->car_model_year : 'N/A';
 //            	$mileage        = !empty($row->car_mileage) ? number_format($row->car_mileage) ." km" : 'N/A';
 //            	$transmission   = !empty($row->car_transmission) ? $row->car_transmission : 'N/A';
	// 			$engine_size    = !empty($row->car_cc) ? number_format($row->car_cc) ." CC" : 'N/A';
	// 			$colour         = !empty($row->car_color) ? $row->car_color : 'N/A';
	// 			$fuel           = !empty($row->car_fuel) ? $row->car_fuel : 'N/A';
	// 			$seats          = !empty($row->car_seat) ? $row->car_seat : 'N/A';
	// 			$vehicle_ref    = !empty($row->car_stock_no) ? $row->car_stock_no : 'N/A';
	// 			$fob		    = !empty($row->car_fob_cost) ? number_format($row->car_fob_cost) : 'ASK';
	// 			$currency       = $row->car_fob_currency;
	// 			$jevic		    = 250;
	// 			$warranty       = 50;
				
	// 			if(empty($row->car_fob_cost)||empty($row->country_to)||empty($row->country_from)||empty($row->car_width)||empty($row->car_height)||empty($row->car_length)){
	//                 $freight_charge_cost = 'ASK';
	//                 $total_price    = 'ASK';
	//                 $total_price_cc = 'ASK';
	//             }else{
	//                 $freight_charge = $this->ceiling($row->shipping_cost * ($row->car_width/1000) * ($row->car_height/1000) * ($row->car_length/1000),50);
	//                 $freight_charge_cost = number_format($freight_charge);

	//                 if($row->member_no==$row->car_owner){
	//                 	$freight_charge_cost = number_format($freight_charge + $this->frieght_cost_plus);
	//                 	$freight_charge = $freight_charge + $this->frieght_cost_plus;
	//                 	$jevic          = $jevic + $this->inspection_cost_plus;
	//                 	$warranty		= $warranty + $this->insurance_cost_plus;
	//                 }
	                
	//                 $total_price = ($row->car_fob_cost + $freight_charge + $jevic + $warranty) - $row->discount_price;

	//                 $total_price_cc = ($row->car_fob_cost + $freight_charge + $warranty) - $row->discount_price;

	//             }

	//             $objPHPExcel->getActiveSheet()->getRowDimension(26)->setVisible(true);

	// 			$objPHPExcel->getActiveSheet()->setCellValue('J10', $insert_id)
	// 										  ->setCellValue('A26', 'JEVIC:')
	// 							              ->setCellValue('J11', date("d-M-Y"))
	// 							              ->setCellValue('A15', $customer)
	// 							              ->setCellValue('B19', $row->car_make.' '.$row->car_model)
	// 							              ->setCellValue('K19', 'Country of Origin: '.$row->country_name)
	// 							              ->setCellValue('B20', $vehicle_ref)
	// 							              ->setCellValue('B21', $year)
	// 							              ->setCellValue('B22', $seats)
	// 							              ->setCellValue('B23', $fuel)
	// 							              ->setCellValue('F20', $chassis)
	// 							              ->setCellValue('F21', $engine_size)
	// 							              ->setCellValue('F22', $colour)
	// 							              ->setCellValue('F23', $mileage)
	// 							              ->setCellValue('J20', $chassis_no)
	// 							              ->setCellValue('J21', $row->idx)
	// 							              ->setCellValue('J22', $transmission)
	// 							              ->setCellValue('J24', $currency)
	// 							              ->setCellValue('J25', $currency)
	// 							              ->setCellValue('J26', $currency)
	// 							              ->setCellValue('J27', $currency)
	// 							              ->setCellValue('J28', $currency)
	// 							              ->setCellValue('J31', $currency)
	// 							              ->setCellValue('K24', $fob)
	// 							              ->setCellValue('K25', $freight_charge_cost)
	// 							              ->setCellValue('K26', $jevic)
	// 							              ->setCellValue('K27', $warranty)
	// 							              ->setCellValue('K28', '-'.$row->discount_price)
	// 							              ->setCellValue('K31', $total_price)
	// 							              ->setCellValue('A31', 'TOTAL PRICE LANDED(CIF) '.$row->port_name)
	// 							              ->setCellValue('C42', $insert_id);
	// 	        $insert_id++;
		
	// 	        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

	// 	        if (!file_exists('uploads/proforma_invoice/'.$today_date)) {
	// 			    mkdir('uploads/proforma_invoice/'.$today_date, 0755, true);
	// 			}

	// 			$objWriter->save('uploads/proforma_invoice/'.$today_date.'/proforma_invoice('.$i.'-'.date("Y-m-d-H-i").').xls');
				
	// 			$objPHPExcel->getActiveSheet()->getRowDimension(26)->setVisible(false);
				
	// 			$objPHPExcel->getActiveSheet()->setCellValue('A26', '')
	// 										  ->setCellValue('J26', '')
	// 										  ->setCellValue('K26', '')
	// 										  ->setCellValue('K31', $total_price_cc);

	// 			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

	// 			$objWriter->save('uploads/proforma_invoice/'.$today_date.'/cc_proforma_invoice('.$i.'-'.date("Y-m-d-H-i").').xls');
 //            	$i++;
 //            }

 //            redirect('/?c=admin&m=adm_proforma_invoice_list', 'refresh');

 //        }
        
 //    }

    function adm_invoice_register_exec(){
		ob_clean();
    	/** Error reporting */
        error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);

		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

		date_default_timezone_set('Asia/Phnom_Penh');

		require_once dirname(__FILE__) . '/../PHPExcel/PHPExcel-develop/Classes/PHPExcel.php';

        /** PHPExcel_IOFactory */
        require_once dirname(__FILE__) . '/../PHPExcel/PHPExcel-develop/Classes/PHPExcel/IOFactory.php';

		$objPHPExcel = new PHPExcel();

        $objReader = PHPExcel_IOFactory::createReader('Excel5');

        $objPHPExcel = $objReader->load("PHPExcel/PHPExcel-develop/Examples/templates/motorbb_invoice.xls");

        $idxs = $this->ci->input->get_post('idx');
        $customer_no = $this->ci->input->get_post('customer_no');
        $country_to = $this->ci->input->get_post('des_country');
        $port_name = $this->ci->input->get_post('portname');
        $discount = $this->ci->input->get_post('discount');

        $batch_data = array();
        $today_date = date("Y-m");

        foreach ($customer_no as $value) {

            if(!empty($value)){

                $batch['customer_no'] = $value;
                $batch['created_dt'] = func_get_date_time();
              	$j = 1;

                foreach ($idxs as $bbs_idx) {
                    $batch['full_path'] ='uploads/proforma_invoice/'.$today_date.'/proforma_invoice('.$j++.'-'.date("Y-m-d-H-i").').xls';
                    $batch['bbs_idx'] = $idx[] = $bbs_idx;
                    $batch['discount_price'] = $discount[$bbs_idx];
                    $validate = true;
                    $batch_data[] = $batch;
                }
            }
        }

        if($validate == true){

            $insert_id = $this->ci->user_invoice->insert_car_invoice_batch($batch_data);

            $invoice_idx = $this->ci->user_invoice->select_invoice_idx($idx,$discount);

            $get_customer = $this->ci->user_member->select_customer_no($customer_no[0]);

            $customer_name = $get_customer->member_first_name.' '.$get_customer->member_last_name;
            $customer_tel = $get_customer->phone_no1.'-'.$get_customer->phone_no2;
            
            $i = 1;

            foreach ($invoice_idx as $row) {

            	$chassis_no     = $row->car_chassis_no;
            	$year           = !empty($row->manu_month) ? date("F", strtotime(sprintf("%02s",$row->manu_month))).',' : '';
            	$year          .= !empty($row->car_model_year) ? $row->car_model_year : 'N/A';
            	$mileage        = !empty($row->car_mileage) ? number_format($row->car_mileage) ." km" : 'N/A';
            	$transmission   = !empty($row->car_transmission) ? $row->car_transmission : 'N/A';
				$engine_size    = !empty($row->car_cc) ? number_format($row->car_cc) ." CC" : 'N/A';
				$colour         = !empty($row->car_color) ? $row->car_color : 'N/A';
				$fuel           = !empty($row->car_fuel) ? $row->car_fuel : 'N/A';
				$seats          = !empty($row->car_seat) ? $row->car_seat : 'N/A';
				$stock_id       = !empty($row->car_stock_no) ? $row->car_stock_no : 'N/A';
				$fob		    = !empty($row->car_fob_cost) ? number_format($row->car_fob_cost) : 'ASK';
				$currency       = $row->car_fob_currency;
				$door           = ($row->door > 0) ? $row->door : '';
				$label_door     = ($row->door > 0) ? "DOORS" : '';
				
				$get_country =  $this->ci->mo_shipping->select_port($country_to);

	            $get_list_price = $this->ci->mo_shipping->get_divided_price($get_country[0]->id,$row->idx);

	            $freight_charge	= $get_list_price['freight_cost'];
	            $inspection		= $get_list_price['inspection_cost'];
				$insurance      = $get_list_price['insurance_cost'];

				if(empty($freight_charge)||empty($row->car_fob_cost)||empty($row->country)||empty($row->car_height)||empty($row->car_length)){
	               
	                $freight_charge_cost = 'ASK';
	                $total_price         = 'ASK';
	                $total_price_cc      = 'ASK';
	                $inspection          = 'ASK';
	                $insurance           = 'ASK';

	            }else{
	               
	                $freight_charge_cost = number_format($freight_charge);

	                $total_price = ($row->car_fob_cost + $freight_charge + $inspection + $insurance) - $row->discount_price;

	                $total_price_cc = ($row->car_fob_cost + $freight_charge + $insurance) - $row->discount_price;

	            }

	            $objPHPExcel->getActiveSheet()->getRowDimension(34)->setVisible(true);

				$objPHPExcel->getActiveSheet()->setCellValue('C7', $insert_id)
								              ->setCellValue('C8', date("d-M-Y"))
								              ->setCellValue('A34', 'INSPECTION:')
								              ->setCellValue('B13', $customer_name)
								              ->setCellValue('B14', $get_customer->address)
								              ->setCellValue('B15', $customer_tel)
								              ->setCellValue('B16', $get_customer->email)
								              ->setCellValue('B23', $row->car_make.' '.$row->car_model)
								              ->setCellValue('H23', 'Country of Origin: '.$row->country_name)
								              ->setCellValue('C25', $stock_id)
								              ->setCellValue('C26', $chassis_no)
								              ->setCellValue('C27', $year)
								              ->setCellValue('C28', $mileage)
								              ->setCellValue('C29', $transmission)
								              ->setCellValue('H25', $engine_size)
								              ->setCellValue('H26', $fuel)
								              ->setCellValue('H27', $colour)
								              ->setCellValue('H28', $seats)
								              ->setCellValue('H29', $door)
								              ->setCellValue('F29', $label_door)
								              ->setCellValue('F31', $currency)
								              ->setCellValue('F32', $currency)
								              ->setCellValue('F33', $currency)
								              ->setCellValue('F34', $currency)
								              ->setCellValue('F35', $currency)
								              ->setCellValue('F36', $currency)
								              ->setCellValue('I31', $fob)
								              ->setCellValue('I32', $insurance)
								              ->setCellValue('I33', $freight_charge_cost)
								              ->setCellValue('I34', $inspection)
								              ->setCellValue('I35', '-'.$row->discount_price)
								              ->setCellValue('I36', $total_price)
								              ->setCellValue('C38', $get_country[0]->port_name);
		        
		
		        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		        if (!file_exists('uploads/proforma_invoice/'.$today_date)) {
				    mkdir('uploads/proforma_invoice/'.$today_date, 0755, true);
				}

				$objWriter->save('uploads/proforma_invoice/'.$today_date.'/proforma_invoice('.$i.'-'.date("Y-m-d-H-i").').xls');
				
				$objPHPExcel->getActiveSheet()->getRowDimension(34)->setVisible(false);
				
				$objPHPExcel->getActiveSheet()->setCellValue('A34', '')
											  ->setCellValue('F34', '')
											  ->setCellValue('I34', '')
											  ->setCellValue('I36', $total_price_cc);

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

				$objWriter->save('uploads/proforma_invoice/'.$today_date.'/cc_proforma_invoice('.$i.'-'.date("Y-m-d-H-i").').xls');
            	$i++;

            	$log_data = array(
                    'author'=>$_SESSION['ADMIN']['member_no'],
                    'message'=>'A Proforma Invoice (Invoice No: '.$insert_id.') has been created by '.$_SESSION['ADMIN']['member_id'],
                    'log_type'=>'proforma_invoice',
                    'created_dt'=>func_get_date_time()
                );
                $this->ci->mo_activity_log->insert($log_data);

                $insert_id++;
            }

            redirect('/?c=admin&m=adm_proforma_invoice_list', 'refresh');

        }
        
    }

    function adm_proforma_invoice_list($prev_next = 'N'){

		$where = array();

		$sParam = '';

		$sch_condition = $this->ci->input->get_post('sch_condition'); 

		$sch_word = $this->ci->input->get_post('sch_word');		

		if ($sch_condition != '' && $sch_word != '') {

			$sch_condition_tmp = explode('|', $sch_condition);


			if (count($sch_condition_tmp) > 1) {

				$tmpStr = '';

				for ($i = 0; $i < count($sch_condition_tmp); $i++) {

					if ($tmpStr != '') {

						$tmpStr .= ' or ';

					}

					$tmpStr .= $sch_condition_tmp[$i] . ' ? ';

					$where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';

				}



				$tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

				$where[$tmpStr] = '';


				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;
	

			} else {

				$where[$sch_condition] = '%' . $sch_word . '%';

				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			}

		}

		$sch_checkbox = array();
		$sch_checkbox = ($this->ci->input->get_post('sch_checkbox'));

		if ($sch_checkbox != '') {

			if(count($sch_checkbox) > 0)
			{
				$tmpStr = '';

				for($i = 0; $i < count($sch_checkbox); $i++)
				{
					if ($tmpStr != '') {

						$tmpStr .= ' or ';

					}

					$chk_condition = $sch_checkbox[$i] ;
					
					$where[$chk_condition] = 'Y';
				}

			}
		}

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 20;

		$page_views =  10;

		if(isset($_POST['invoice_no'])){
			$invoice_no = $this->ci->input->get_post("invoice_no");
		}else{
			$invoice_no = '';
		}
		
		$invoice_list = $this->ci->user_invoice->select_proforma_invoice($invoice_no , ($cur_page - 1) * $page_rows, $page_rows, $where);

		$file_couter = array();

		$total_rows	= $this->ci->user_invoice->select_all_proforma_invoice();

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);

		if ($prev_next == 'Y') {

			$pn_data = array();

			$pn_data['isPrev'] = '';

			$prev = ($cur_page - 1) * $page_rows;

			if ($prev > 0) {

				$pn_data['isPrev'] = "PREV";

				$prev = $prev - 1;

			}


			$pn_data['cur_page'] = $cur_page;

			$pn_data['page_views'] = $page_views;

			$pn_data['page_rows'] = $page_rows;

			$pn_data['total_rows'] = $total_rows;

			$pn_data['row_cnt'] = $row_cnt;


			if ($pn_data['isPrev'] == 'PREV') {

				$page_rows = $page_rows + 2;

			} else {

				$page_rows = $page_rows + 1;

			}

			$pn_data['prevList'] = $this->ci->bbs->select($invoice_no, $prev, $page_rows, $where)->result();


			return $pn_data;

		}

		func_set_data($this->ci, 'file_couter', $file_couter);   		

		func_set_data($this->ci, 'total_rows', $total_rows);   		

		func_set_data($this->ci, 'total_page', $page_info[0]); 		

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		

		func_set_data($this->ci, 'cur_page', $cur_page);	   	

		func_set_data($this->ci, 'sParam', $sParam);		   		
		
		func_set_data($this->ci, 'mcd', $this->mcd);
		
		func_set_data($this->ci, 'invoice_list', $invoice_list);  

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/proforma_invoice/list', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
    }

    function adm_delete_proforma_invoice() {
    	ob_clean();
    	$invoice_nos = explode(";", $this->ci->input->get_post('invoice_no'));

        $delete_file_image = $this->ci->user_invoice->delete_proforma_invoice_file($invoice_nos); 

        if (COUNT($delete_file_image) > 0) {

            foreach ($delete_file_image as $rows) {

            	unlink($rows->full_path);
            	unlink(str_replace('proforma_invoice(','cc_proforma_invoice(',$rows->full_path));

            }
        }

        $this->ci->user_invoice->delete_proforma_invoice($invoice_nos);

        $invoice_id = substr(implode(',', $invoice_nos),0,-1);

        $count_id_invoice = count($invoice_nos) - 1;

        $log_data = array(
            'author'=>$_SESSION['ADMIN']['member_no'],
            'message'=>$count_id_invoice.' Proforma Invoices (Invoice No: '.$invoice_id.') has been deleted by '.$_SESSION['ADMIN']['member_id'],
            'log_type'=>'proforma_invoice',
            'created_dt'=>func_get_date_time()
        );
        $this->ci->mo_activity_log->insert($log_data);

        echo ('Proforma invoices has been deleted.');
    }

    function download_file_proforma_invoice(){
    	ob_end_clean();
    	$full_path_invoice = $this->ci->input->get_post("full_path_invoice");
    	$invoice_no = $this->ci->input->get_post("invoice_no");

    	header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=invoice_'.$invoice_no.'.xls');
		header('Pragma: no-cache');
		readfile($full_path_invoice);
    }
    //*************End Pro-Forma Invoice******************//
}

?>