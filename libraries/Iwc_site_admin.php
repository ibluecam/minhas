<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Iwc_site_admin.php
 * 개  요 : 사이트 관리정보를 설정한다.
  ------------------------------------------------ */

class Iwc_site_admin {

	var $ci;

//constructor
	function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();
	}

//사이트 관리정보 설정 HTML
	function view_site_admin() {
		$config_type = 'site_admin'; //설정 구분
//해당 config 조회
		$select = $this->ci->iw_config->select($config_type);
		$site_admin_chk = $this->ci->iw_config->site_admin_chk();	//사이트 관리자 체크
		$iw_site_admin_chk = $this->ci->iw_config->iw_site_admin_chk(); //인츠웍스 계정 체크

		func_set_data($this->ci, 'site_admin_chk', $site_admin_chk->result());
		func_set_data($this->ci, 'iw_site_admin_chk', $iw_site_admin_chk->result());
		func_set_data($this->ci, 'site_admin', $select->result());

//컨텐츠
		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/site_admin/iwc_site_admin', $this->ci->data, true));
		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);
	}

//사이트 관리정보 설정 EXEC
	function exec_site_admin() {
		$exec_cd = ''; //입력&수정 구분

		$inData = array();
		$inData['config_type'] = 'site_admin';
		$inData['config_type_name'] = '사이트 관리정보 설정';

		$array_values = $this->ci->input->post('values');  //폼값
//key 값을 array로 리턴
		$keys = func_get_array_keys($array_values);

		for ($i = 0; $i < count($keys); $i++) {
			$inData['key'] = $keys[$i];
			$inData['value'] = $array_values[$keys[$i]];
			$inData['desc'] = '';
			$inData['created_dt'] = func_get_date_time();

			$tmp = $this->ci->iw_config->select_detail($inData['config_type'], $inData['key']);

			if ($tmp->num_rows() > 0)
				$exec_cd = 'UPDATE';
			else
				$exec_cd = 'INSERT';

			if ($exec_cd == 'INSERT') {
				$this->ci->iw_config->insert($inData);
			} else {
				$this->ci->iw_config->update($inData);
			}
		}

		echo func_jsAlertReplace('사이트 관리설정 정보가 등록 되었습니다', '/?c=iwc&amp;m=view_site_admin');
	}

//사이트 DB DATA 리셋
	function reset_site_admin() {
		$this->ci->iw_config->delete_all_site_admin();

		echo func_jsAlertReplace('초기화 되었습니다.', '/?c=iwc&amp;m=view_site_admin');
	}

	/* ------------------------------------------------------------------------------------------------ */

//사이트 관리자 계정 생성 EXEC
	function exec_create_admin_account() {
		$fg = ''; //DB 처리 여부

		$inData = array();
		$inData['member_id'] = $this->ci->input->get_post('member_id', TRUE);
		$inData['member_pwd'] = func_base64_encode($this->ci->input->get_post('member_pwd', TRUE));
		$inData['member_name'] = '관리자';
		$inData['grade_no'] = '1';
		$inData['certi_yn'] = 'Y';
		$inData['created_dt'] = func_get_date_Time();

		$query = $this->ci->iw_config->site_admin_chk(); //인츠웍스 관리자 체크
//관리자 정보 존재
		if ($query->num_rows() > 0) {
			$member_no = '';
			$member_id = '';
			$member_pwd = '';
			foreach ($query->result() as $rows) {
				$member_no = $rows->member_no;
				$member_id = $rows->member_id;
				$member_pwd = $rows->member_pwd;
			}
//회원 테이블에 관리자 정보를 수정한다.
			$fg = $this->ci->iw_config->update_admin_account($member_no, $inData);
		}
//관리자 정보 미존재
		else {
//회원 테이블에 관리자 정보를 등록한다.
			$fg = $this->ci->iw_config->insert_admin_account($inData);
		}

		if ($fg > 0) {
			echo 'SUCCESS';
		} else {
			echo 'FAIL';
		}
	}

//인츠웍스 관리자 계정 생성 EXEC
	function exec_iw_create_admin_account() {
		$fg = ''; //DB 처리 여부

		$inData = array();
		$inData['member_id'] = $this->ci->input->get_post('iw_member_id', TRUE);
		$inData['member_pwd'] = func_base64_encode($this->ci->input->get_post('iw_member_pwd', TRUE));
		$inData['member_name'] = 'NOS';
		$inData['use_yn'] = 'Y';
		$inData['created_dt'] = func_get_date_Time();

		$query = $this->ci->iw_config->iw_site_admin_chk(); //사이트 관리자 체크
//인츠웍스 계정 정보 존재
		if ($query->num_rows() > 0) {
			$member_no = '';
			$member_id = '';
			$member_pwd = '';
			foreach ($query->result() as $rows) {
				$member_no = $rows->member_no;
				$member_id = $rows->member_id;
				$member_pwd = $rows->member_pwd;
			}
//인츠웍스 테이블에 관리자 정보를 수정한다.
			$fg = $this->ci->iw_config->update_iw_account($member_no, $inData);
		}
//관리자 정보 미존재
		else {
//인츠웍스 테이블에 관리자 정보를 등록한다.
			$fg = $this->ci->iw_config->insert_iw_account($inData);
		}

		if ($fg > 0) {
			echo 'SUCCESS';
		} else {
			echo 'FAIL';
		}
	}

}
?>