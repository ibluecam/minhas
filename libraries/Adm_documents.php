<?php
class Adm_documents {
	
	var $ci;

	var $mcd ='documents';

	var $bbs_config = NULL;



	//constructor

	function __construct() {

		//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당

		$this->ci = &get_instance();

	    /* ------------------------------------------------------------



		  - Model Load



		  ------------------------------------------------------------ */



		$this->ci->load->model('mo_documents', 'user_documents'); //class명과 동일해서 user_documents

		$this->ci->load->model('mo_survey', 'survey'); // 설문조사

		$this->ci->load->model('mo_member', 'user_member'); // 맴버
		


	}
	function adm_delete_bl_by_no(){
		// $bl_nos = $this->ci->input->get_post('bl_no');
		$bl_nos=explode(";", $this->ci->input->get_post('bl_no'));
		$this->ci->user_documents->delete_bl_by_no($bl_nos);
		echo func_jsAlertReplace('The Document(s) has been deleted successfully .', '/?c=admin&m=adm_documents_list');
		
	}
	// Search engine  Document 
	
	function adm_documents_list($prev_next = 'N'){
		
		$where = array(); 		//sql where 조건

		$sParam = '';	  		//검색 string	

		$sch_condition = $this->ci->input->get_post('sch_condition');   		//검색조건

		$sch_word = $this->ci->input->get_post('sch_word');				//검색어

		//var_dump($_POST['sch_condition']); exit();

		//검색조건 + 검색어

		if ($sch_condition != '' && $sch_word != '') {

			$sch_condition_tmp = explode('|', $sch_condition);


			//다중 조건인 경우(제목 + 내용)

			if (count($sch_condition_tmp) > 1) {

				$tmpStr = '';

				for ($i = 0; $i < count($sch_condition_tmp); $i++) {

					if ($tmpStr != '') {

						$tmpStr .= ' or ';

					}



					$tmpStr .= $sch_condition_tmp[$i] . ' ? ';

					$where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';

				}



				$tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';

				$where[$tmpStr] = '';


				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;
	

			} else {

				$where[$sch_condition] = '%' . $sch_word . '%';

				$sParam .= '&sch_condition=' . $sch_condition;

				$sParam .= '&sch_word=' . $sch_word;

			}

		}

		//var_dump($where); exit();

		
		//체크박스 조건
		$sch_checkbox = array();
		$sch_checkbox = ($this->ci->input->get_post('sch_checkbox'));

		if ($sch_checkbox != '') {
			//var_dump(count($sch_checkbox));
			//var_dump($_POST); exit();

			if(count($sch_checkbox) > 0)
			{
				$tmpStr = '';

				for($i = 0; $i < count($sch_checkbox); $i++)
				{
					if ($tmpStr != '') {

						$tmpStr .= ' or ';

					}
					//echo $sch_checkbox[$i];
					$chk_condition = $sch_checkbox[$i] ;

					//$tmpStr .= $chk_condition[$i] . ' ? ';
					
					$where[$chk_condition] = 'Y';
				}

				//var_dump($where);
				//var_dump($tmpStr); exit();
				//var_dump($where[$chk_condition]); exit();
			}
		}

	


		//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 18;
		//(integer) func_get_config($this->bbs_config, 'page_rows');  		//페이지 로우수

		//var_dump($page_rows); exit();

		$page_views =  10;
		
		//(integer) func_get_config($this->bbs_config, 'page_views'); 		//페이지 네비게이션 수

		//게시판 공지 목록 조회
		if(isset($_POST['bl_no'])){
			$bl_no = $this->ci->input->get_post("bl_no");
		}else{
			$bl_no = '';
		}
		

		$doc_list = $this->ci->user_documents->select($bl_no , ($cur_page - 1) * $page_rows, $page_rows, $where);
		//var_dump($doc_list); exit();
		//var_dump($doc_list->result()); exit();
		
		// create file counter
		
		$file_couter = array();
		
		
/*		foreach ($doc_list->result() as $value) {
		
			$idx = array();
		
			$doc_bl = $this->ci->user_documents->select_bl($value->bl_no); 
			
			foreach( $doc_bl->result() as $row){
							
				array_push($idx,$row->idx);
					
			}
			//print_r($idx);
			
			$doc_attach = $this->ci->user_documents->select_attach($idx);  /////////////////////////

			array_push($file_couter,$doc_attach);
			
			unset($idx);	
		
		}*/

		
		$total_rows = $this->ci->user_documents->total_select(); 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		//글 번호

		//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



		//이전글 다음글 조회

		if ($prev_next == 'Y') {

			$pn_data = array();

			$pn_data['isPrev'] = '';



			$prev = ($cur_page - 1) * $page_rows;



			if ($prev > 0) {

				$pn_data['isPrev'] = "PREV";

				$prev = $prev - 1;

			}



			$pn_data['cur_page'] = $cur_page;

			$pn_data['page_views'] = $page_views;

			$pn_data['page_rows'] = $page_rows;

			$pn_data['total_rows'] = $total_rows;

			$pn_data['row_cnt'] = $row_cnt;



			if ($pn_data['isPrev'] == 'PREV') {

				$page_rows = $page_rows + 2;

			} else {

				$page_rows = $page_rows + 1;

			}

			$pn_data['prevList'] = $this->ci->bbs->select($bl_no, $prev, $page_rows, $where)->result();



			return $pn_data;

		}

		func_set_data($this->ci, 'file_couter', $file_couter);   		//file

		func_set_data($this->ci, 'total_rows', $total_rows);   		//조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); 		//전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		//글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   		//현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   		//검색 string
		
		func_set_data($this->ci, 'mcd', $this->mcd);
		
		func_set_data($this->ci, 'doc_list', $doc_list->result());  

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/documents/list', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
		
	}
		
	//Search engine  adm_documents_view
	
	function adm_documents_view($prev_next = 'N'){
	
			
		$where = array(); 		//sql where 조건

		$sParam = '';	  		//검색 string

		$mcd = $this->ci->input->get_post("mcd");

		//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 18;
		//(integer) func_get_config($this->bbs_config, 'page_rows');  		//페이지 로우수

		//var_dump($page_rows); exit();

		$page_views =  10;
		
		//(integer) func_get_config($this->bbs_config, 'page_views'); 		//페이지 네비게이션 수

		//게시판 공지 목록 조회
		
		$bl_no = $this->ci->input->get_post("bl_no");

		$bbs_notice_list = $this->ci->user_documents->select($bl_no,($cur_page - 1) * $page_rows, $page_rows, $where);
		
		$bbs_attach_list = $this->ci->user_documents->select_attach($bl_no);



		//게시판 일반 목록 조회

		//$bbs_list = $this->ci->user_documents->select($bl_no , ($cur_page - 1) * $page_rows, $page_rows, $where);
		$bbs_list = $this->ci->user_documents->select_bl_car($bl_no);
		//var_dump($bbs_list); exit();

		$total_rows = $this->ci->user_documents->total_select(); 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		//글 번호

		//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



		//이전글 다음글 조회

		if ($prev_next == 'Y') {

			$pn_data = array();

			$pn_data['isPrev'] = '';



			$prev = ($cur_page - 1) * $page_rows;



			if ($prev > 0) {

				$pn_data['isPrev'] = "PREV";

				$prev = $prev - 1;

			}



			$pn_data['cur_page'] = $cur_page;

			$pn_data['page_views'] = $page_views;

			$pn_data['page_rows'] = $page_rows;

			$pn_data['total_rows'] = $total_rows;

			$pn_data['row_cnt'] = $row_cnt;



			if ($pn_data['isPrev'] == 'PREV') {

				$page_rows = $page_rows + 2;

			} else {

				$page_rows = $page_rows + 1;

			}

			$pn_data['prevList'] = $this->ci->bbs->select($bl_no, $prev, $page_rows, $where)->result();



			return $pn_data;

		}



		func_set_data($this->ci, 'total_rows', $total_rows);   		//조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); 		//전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		//글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   		//현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   		//검색 string
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list);

		func_set_data($this->ci, 'bbs_list', $bbs_list->result());
		
		func_set_data($this->ci, 'bbs_attach_list',$bbs_attach_list->result());

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/documents/view', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	
	
	}
	
	// Search engine Document detail 
	
	function adm_documents_detail(){
	
/*		$idx = $this->ci->input->get_post('idx');
		
		if( is_array($idx)){
		
			$idx = $idx[0];
			
		}*/
		
		$where = array(); 		//sql where 조건

		$sParam = '';	  		//검색 string

		$mcd = $this->ci->input->get_post("mcd");

		//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 18;
		//(integer) func_get_config($this->bbs_config, 'page_rows');  		//페이지 로우수

		//var_dump($page_rows); exit();

		$page_views =  10;
		
		//(integer) func_get_config($this->bbs_config, 'page_views'); 		//페이지 네비게이션 수

		//게시판 공지 목록 조회
		
		$bl_no = $this->ci->input->get_post("bl_no");

		$bbs_notice_list = $this->ci->user_documents->select($bl_no,($cur_page - 1) * $page_rows, $page_rows, $where);
		
		$bbs_attach_list = $this->ci->user_documents->select_attach($bl_no);



		//게시판 일반 목록 조회

		//$bbs_list = $this->ci->user_documents->select($bl_no , ($cur_page - 1) * $page_rows, $page_rows, $where);
		$bbs_list = $this->ci->user_documents->select_bl_car($bl_no);
		//var_dump($bbs_list); exit();

		$total_rows = $this->ci->user_documents->total_select(); 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		//글 번호

		//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



		
		func_set_data($this->ci, 'total_rows', $total_rows);   		//조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); 		//전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		//글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   		//현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   		//검색 string
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		func_set_data($this->ci,'bl_no', $bl_no);

		func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list);

		func_set_data($this->ci, 'bbs_list', $bbs_list->result());
		
		func_set_data($this->ci, 'bbs_attach_list',$bbs_attach_list->result());

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/documents/modify', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
		
	
	}



	//Search engine  Document register
	function adm_documents_register(){
		
		$bl_no = $this->ci->input->get_post("bl_no");
		
		if( is_array($bl_no)){
		
			$bl_no = $bl_no[0];
			
		}

		//echo "Ok";
		$this->ci->load->view('admin/adm/documents/write.php', $this->ci->data, false);

		//$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	
	}
	
	//Search engine  Document register_exec
	function adm_documents_register_exec(){
	
		//$idx = $this->ci->input->get_post('idx');
		$bl_no = $this->ci->input->get_post('bl_no');
		
		
		$inData = array();

		//$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴
		
		$inData['bl_no'] = $bl_no	;		//게시물 일련번호

		func_set_data($this->ci, 'bl_no', $bl_no);
		
		//$inData['icon_status'] = 'releaseok'	;	all icon_status will be decided by sales manually	
		
		$this->ci->user_documents->update($inData); 		//첨부파일 삭제
		
		// count image upload
		
		$num_imageUpload = count($_FILES['attach1']['name']);
		
		
		for ($i = 0; $i < $num_imageUpload; $i++) {
		
		//선택된 첨부 파일만...

				if ($_FILES['attach1']['tmp_name'][$i] != '') {

					//파일 업로드

					$upload_str = $this->ci->iwc_common->upload('attach1',$i , $this->mcd, $this->bbs_config);

					//파일 업로드 실패

					if ($upload_str != TRUE) {

						show_error($this->ci->iwc_common->upload_error_msg());

					}

					//파일 업로드 성공

					else {

						$upload_file_info = array();

						$upload_file_info = $this->ci->iwc_common->upload_success_msg();



						//첨부파일 DB 등록

						$upload_file_info['bl_no'] = $bl_no;
						
						$upload_file_info['menu_code'] = $this->mcd;

						$this->ci->user_documents->insert_attach($upload_file_info);

					}

					echo func_jsAlertReplace('Documents has been upload successfully .', '?c=admin&m=adm_documents_register&mcd=document&bl_no='.$bl_no);

				}else{

					echo func_jsAlertReplace('Upload Failed! .', '?c=admin&m=adm_documents_register&mcd=document&bl_no='.$bl_no);
				}
	
		}
		
		
		//print_r($upload_file_info);
		
	}



	
		
	//Search engine  Document modify
	function adm_documents_modify(){
	
		$where = array(); 		//sql where 조건

		$sParam = '';	  		//검색 string
		
		$mcd = $this->ci->input->get_post("mcd");
	
		//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 18;
		//(integer) func_get_config($this->bbs_config, 'page_rows');  		//페이지 로우수

		//var_dump($page_rows); exit();

		$page_views =  10;
		
		//(integer) func_get_config($this->bbs_config, 'page_views'); 		//페이지 네비게이션 수

		//게시판 공지 목록 조회
		
		$bl_no = $this->ci->input->get_post("bl_no");
		
		if( is_array($bl_no)){
		
			$bl_no = $bl_no[0];
			
		}

		$bbs_notice_list = $this->ci->user_documents->select($bl_no,($cur_page - 1) * $page_rows, $page_rows, $where);
		
		$bbs_attach_list = $this->ci->user_documents->select_attach($bl_no);



		//게시판 일반 목록 조회

		//$bbs_list = $this->ci->user_documents->select($bl_no , ($cur_page - 1) * $page_rows, $page_rows, $where);
		$bbs_list = $this->ci->user_documents->select_bl_car($bl_no);
		//var_dump($bbs_list); exit();

		$total_rows = $this->ci->user_documents->total_select(); 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		//글 번호

		//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



		func_set_data($this->ci, 'total_rows', $total_rows);   		//조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); 		//전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		//글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   		//현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   		//검색 string
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		func_set_data($this->ci,'bl_no', $bl_no);

		func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list);

		func_set_data($this->ci, 'bbs_list', $bbs_list->result());
		
		func_set_data($this->ci, 'bbs_attach_list',$bbs_attach_list->result());

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/documents/modify', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
	
	}
	
	//Search engine  Document modify_exec
	function adm_documents_modify_exec(){
	
		$idx = $this->ci->input->get_post('idx');
		
		
		$inData = array();

		$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴
		
		$inData['idx'] = $idx	;		//게시물 일련번호
		
		$this->ci->user_documents->update($inData); 		//첨부파일 삭제
		
		// count image upload
		
		$num_imageUpload = count($_FILES['attach1']['name']);
		
		
		for ($i = 0; $i < $num_imageUpload; $i++) {
		
		//선택된 첨부 파일만...

				if ($_FILES['attach1']['tmp_name'][$i] != '') {

					//파일 업로드

					$upload_str = $this->ci->iwc_common->upload('attach1',$i , $this->mcd, $this->bbs_config);

					//파일 업로드 실패

					if ($upload_str != TRUE) {

						show_error($this->ci->iwc_common->upload_error_msg());

					}

					//파일 업로드 성공

					else {

						$upload_file_info = array();

						$upload_file_info = $this->ci->iwc_common->upload_success_msg();



						//첨부파일 DB 등록

						$upload_file_info['idx'] = $idx;
						
						$upload_file_info['menu_code'] = $this->mcd;

						$this->ci->user_documents->insert_attach($upload_file_info);

					}

				}
	
		}
		
		func_set_data($this->ci, 'idx', $idx);
		
		echo func_jsAlertReplace('Documents has been upload successfully .', '?c=admin&m=adm_documents_list');
		
	}


	//Search engine documents_delete_exec
	function adm_documents_delete_exec(){
	
		$mcd = $this->ci->input->get_post("mcd");
		
		$bl_no = $this->ci->input->get_post("bl_no");
		
		$att_idx = $this->ci->input->get_post("att_idx");
		
		$file_num = $this->ci->input->get_post("file_num");
				
		$where = array(); 		//sql where 조건

		$sParam = '';	  		//검색 string
	
		//$idx = $this->ci->input->get_post('idx');

		
		$att_idx = $this->ci->input->get_post('att_idx');
		
		$bbs_attach_detail_list = $this->ci->user_documents->select_attach_detail($bl_no, $att_idx); 		//첨부파일 상세 조회

		$return_result = $this->ci->user_documents->delete_attach($bl_no, $att_idx, $this->mcd); 		//첨부파일 삭제
		
		if(($return_result == true) && $file_num == 1){
			
			$inData = array();
	
			//$inData = func_get_post_array($this->ci->input->post('values'));  		//POST 배열을 array['key'] = value 형태로 리턴
			
			$inData['bl_no'] = $bl_no	;		//게시물 일련번호
			
			//$inData['icon_status'] = ''	;	all icon_status will be decided by sales manually	
			
			$this->ci->user_documents->update($inData); 		//첨부파일 삭제

			
		}

		//업로드 실제 첨부파일 삭제

		if ($bbs_attach_detail_list->num_rows() > 0) {

			foreach ($bbs_attach_detail_list->result() as $rows) {

				//첨부파일이 이미지인 경우 생성된 썸네일 까지 삭제

				@unlink($rows->full_path);

				
				$rows->full_path;

			}

		}
		
		
		//페이지 조건

		$cur_page = $this->ci->input->get_post("cur_page");

		if ($cur_page == NULL || $cur_page == '')

			$cur_page = 1;

		$page_rows = 18;
		//(integer) func_get_config($this->bbs_config, 'page_rows');  		//페이지 로우수

		//var_dump($page_rows); exit();

		$page_views =  10;
		
		//(integer) func_get_config($this->bbs_config, 'page_views'); 		//페이지 네비게이션 수

		//게시판 공지 목록 조회

		$bl_no = $this->ci->input->get_post("bl_no");
		
		if( is_array($bl_no)){
		
			$bl_no = $bl_no[0];
			
		}

		$bbs_notice_list = $this->ci->user_documents->select($bl_no,($cur_page - 1) * $page_rows, $page_rows, $where);
		
		$bbs_attach_list = $this->ci->user_documents->select_attach($bl_no);



		//게시판 일반 목록 조회

		//$bbs_list = $this->ci->user_documents->select($bl_no , ($cur_page - 1) * $page_rows, $page_rows, $where);
		$bbs_list = $this->ci->user_documents->select_bl_car($bl_no);
		//var_dump($bbs_list); exit();

		$total_rows = $this->ci->user_documents->total_select(); 		//조회 전체 목록수

		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;	   		//글 번호

		//페이지정보 조회

		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);



		func_set_data($this->ci, 'total_rows', $total_rows);   		//조회 전체 목록수

		func_set_data($this->ci, 'total_page', $page_info[0]); 		//전체 페이지

		func_set_data($this->ci, 'row_cnt', $row_cnt);		 		//글번호

		func_set_data($this->ci, 'cur_page', $cur_page);	   		//현재 페이지

		func_set_data($this->ci, 'sParam', $sParam);		   		//검색 string
		
		func_set_data($this->ci, 'mcd',$mcd);
		
		func_set_data($this->ci,'bl_no', $bl_no);

		func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list);

		func_set_data($this->ci, 'bbs_list', $bbs_list->result());
		
		func_set_data($this->ci, 'bbs_attach_list',$bbs_attach_list->result());

		func_set_data($this->ci, 'page_info', $page_info);

		func_set_data($this->ci, 'admin_contents', $this->ci->load->view('admin/adm/documents/modify', $this->ci->data, true));



		$this->ci->load->view('admin/adm_index', $this->ci->data, false);
		
	}
	
}

?>