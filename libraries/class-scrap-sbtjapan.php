<?php

class ScrapSbtjapan{
	public static $domain = 'http://www.sbtjapan.com';
	public static $url = 'http://www.sbtjapan.com/catalog/searchstock/location=50&p_num=';
	public static $car_detail_url = 'http://www.sbtjapan.com/catalog/searchstock/stockid=';
	/*** GET NUMBER OF PAGE ***/
	public static function getNumberOfPage(){
		include_once('scrapwebdata/simple_html_dom.php');
		
		$num_page = 0;
		$html = file_get_html(self::$url);

		$pager_arr_a = $html->find("div[class=pageNumber] a");

		if(empty($pager_arr_a)){
			return 1;
		}

		$last_key = max(array_keys($pager_arr_a));

		$str = $pager_arr_a[$last_key]->onclick;
		$num_page = filter_var($str, FILTER_SANITIZE_NUMBER_INT);

		$html->clear();
		return $num_page;
	}
	/*** GET HTML OF SEARCH RESULT OF THE PAGE NUMBER AFTER EXECUTE ***/
	public static function getSearchResult($pageNumber){
		$result = file_get_contents(self::$url.$pageNumber);
		return $result;
	}
	/*** GET ALL ITEM IDS AS ARRAY FROM A PAGE NUMBER ***/
	public static function getAllItemsFromHtml($pageNumber){
		include_once('scrapwebdata/simple_html_dom.php');
		$items = array();
		$html = ScrapSbtjapan::getSearchResult($pageNumber);
		
		$result_parse = str_get_html($html);
		$arr_tr = $result_parse->find('table',1);
		$i=0;
		foreach($arr_tr->find('tr') as $tr){ 
			$i++;
			
			if (ScrapSbtjapan::FilterVar($tr,'td',1)!='') {
				/*** GET STOCK ID ***/
				$items['id'][] = ScrapSbtjapan::FilterVar($tr,'td',1);
				/*** GET STATUS ***/
				$status_td = $tr->find('td', 1);
				if(count($status_td)>0){
					$items['status'][] = 'sale';
				}else{
					$items['status'][] = 'soldout';
				}
			}
			/*** GET CURRENT FOB PRICE ***/
			if (ScrapSbtjapan::FilterVar($tr,'td',10)!='') {	
				if($tr->find('td',10)->find('span',1)!=null){
					if(filter_var($tr->find('td',10)->find('span',1)->plaintext, FILTER_SANITIZE_NUMBER_INT)!=''){
						$items['old_price'][] = filter_var($tr->find('td',10)->find('span',1)->plaintext, FILTER_SANITIZE_NUMBER_INT);
					}else{
						$items['old_price'][] = filter_var($tr->find('td',10)->find('span',0)->plaintext, FILTER_SANITIZE_NUMBER_INT);
					}	
				}else{
					$items['old_price'][] = 0;
				}	
			}
		}

		$result_parse->clear();

		return $items;
	}

	/*** FILTER ALL NUMBER ***/
	public static function FilterVar($main_children,$children_ele,$position){
		//echo $main_children->find($children_ele,$position)->plaintext."<br/>";
		if($main_children->find($children_ele,$position)!=null){
			return filter_var($main_children->find($children_ele,$position)->plaintext, FILTER_SANITIZE_NUMBER_INT);
		}
	}

	public static function getAllItemsFromPage($page_num){
		/*** EXECUTE ***/
		$items['id'] = array();
		$items['status'] = array();
		$items['old_price'] = array();
		//$num_page = ScrapPrimegt::getNumberOfPage();
		//for($i=1;$i<=$num_page;$i++){
			$arr_items = ScrapSbtjapan::getAllItemsFromHtml($page_num);
			foreach($arr_items['status'] as $key=>$value){
				// if($value=='Sold Out'||$value=='Under Offer'){
				// 	$arr_items['status'][$key] = "soldout";
				// }else{
					$arr_items['status'][$key] = "sale";
				//}
			}
			$items['id'] = array_merge($items['id'], $arr_items['id']);
			$items['status'] = array_merge($items['status'], $arr_items['status']);
			$items['old_price']= array_merge($items['old_price'], $arr_items['old_price']);
		//}
		
		return $items;
	}

	public static function getAllAvailableItems($page_num){
		/*** EXECUTE ***/
		$items['id'] = array();
		$items['status'] = array();
		$items['old_price'] = array();

			$arr_items = ScrapSbtjapan::getAllItemsFromHtml($page_num);
			
			foreach($arr_items['status'] as $key=>$value){
				if($value=='soldout'){
					unset($arr_items['status'][$key]);
					unset($arr_items['id'][$key]);
					unset($arr_items['old_price'][$key]);
				}else{
					$arr_items['status'][$key] = "sale";
				}
			}
			$items['id'] = array_merge($items['id'], $arr_items['id']);
			$items['status'] = array_merge($items['status'], $arr_items['status']);
			$items['old_price']= array_merge($items['old_price'], $arr_items['old_price']);
		
		return $items;
	}
	/*** GET DETAIL ITEMS BY ID ***/
	public static function getItemDetailById($id){
		include_once('scrapwebdata/simple_html_dom.php');
		$item_detail = array();
		$num_page = 0;
		$html = file_get_html(self::$car_detail_url.$id);

		$tbody_html = $html->find("div[class=carDetails] table[class=tabA] tbody");
		$fob_price_span = $html->find("table[class=calculate] tbody tr td dl dd span");
		$currency = $html->find("table[class=calculate] thead tr th select option");
		$image_inputs = $html->find("div[class=photoBox] ul li a img");
		//////////////////////////////////////////////// tbody -> tr        -> td        -> (self) -> html
		/*** ROW 1 COLUMN 2 ***/
		$is_chassis_no = $tbody_html[0]->children[1]->children[0]->children[1]->nodes[0]->innertext;
		$item_detail['chassis_no'] 		= ($is_chassis_no!='' ? $is_chassis_no : '');
		/*** ROW 2 COLUMN 2 ***/
		$car_year 		= $tbody_html[0]->children[1]->children[1]->children[1]->nodes[0]->innertext;	
		$year_month = explode('/', $car_year);
		$item_detail['registrationmonth'] = $year_month[1];
		$item_detail['registrationyear']  = $year_month[0];
		/*** ROW 3 COLUMN 2 ***/
		$item_detail['transmission']      = $tbody_html[0]->children[1]->children[2]->children[1]->nodes[0]->innertext;
		/*** ROW 4 COLUMN 2 ***/
		$item_detail['drive']		      = trim($tbody_html[0]->children[1]->children[3]->children[1]->nodes[0]->innertext);
		/*** ROW 5 COLUMN 2 ***/
		$item_detail['steering'] 		  = trim($tbody_html[0]->children[1]->children[4]->children[1]->nodes[0]->innertext);
		/*** ROW 7 COLUMN 2 ***/
		$item_detail['engine_volume'] 	  = $tbody_html[0]->children[1]->children[6]->children[1]->nodes[0]->innertext;
		/*** ROW 8 COLUMN 2 ***/
		$item_detail['fuel_type'] 		  = $tbody_html[0]->children[1]->children[7]->children[1]->nodes[0]->innertext;
		/*** ROW 1 COLUMN 4 ***/
		$item_detail['model']    		  = $tbody_html[0]->children[1]->children[0]->children[3]->nodes[0]->innertext;
		/*** ROW 3 COLUMN 4 ***/
		$item_detail['exterior_color']    = $tbody_html[0]->children[1]->children[2]->children[3]->nodes[0]->innertext;
		/*** ROW 4 COLUMN 4 ***/
		$item_detail['door'] 	          = trim($tbody_html[0]->children[1]->children[3]->children[3]->nodes[0]->innertext);
		/*** ROW 5 COLUMN 4 ***/
		$item_detail['number_passenger']  = trim($tbody_html[0]->children[1]->children[4]->children[3]->nodes[0]->innertext);
		/*** ROW 6 COLUMN 4 ***/
		$item_detail['product_type']      = $tbody_html[0]->children[1]->children[5]->children[3]->nodes[0]->innertext;
		/*** ROW 7 COLUMN 4 ***/
		$item_detail['mileage'] 		  = $tbody_html[0]->children[1]->children[6]->children[3]->nodes[0]->innertext;

		$get_make_model = $html->find("ul[class=breadCrumb] li");

		$make_model =trim(substr($get_make_model[4]->innertext, 4, -6));
		$make = explode(' ', $make_model);
		$item_detail['make']  = $make[0];
		$item_detail['model']  = $make[1];

		/*** SET DEFAULT VALUE ***/
		$item_detail['car_length']    = '';
		$item_detail['car_width']     = '';
		$item_detail['car_height']    = '';
		$item_detail['manu_year']     = '';
		$item_detail['manu_month']    = '';
		$item_detail['mileage_type']  = '';

		$item_detail['price']		  = filter_var($fob_price_span[0]->innertext, FILTER_SANITIZE_NUMBER_INT);
		$item_detail['currency']	  = $currency[0]->innertext;

		$item_detail['icon_new_yn'] = 'N';
		$item_detail['images'] = array();
		foreach($image_inputs as $input){
			$search= array("changeimageother","('","')");
			$item_detail['images'][] = str_replace($search, '', $input->onmouseover);
		}
		/*** FILTER DATA ***/
		$item_detail['product_type']  = ScrapSbtjapan::getCorrectProductType($item_detail['product_type']);
		$item_detail['engine_volume'] = preg_replace("/[^0-9]/","", $item_detail['engine_volume']);
		$item_detail['mileage']       = preg_replace("/[^0-9]/","", $item_detail['mileage']);
		$item_detail['price']		  = preg_replace("/[^0-9]/","", $item_detail['price']);
		
		foreach($item_detail as $key=>$value){
			if($value==NULL){
				$item_detail[$key]='';
			}
		}
		
		$html->clear();
		return $item_detail;
	}
	public static function getCorrectProductType($product_type){
		$product_arr = array(
					'Sedan'=>'sedans',
					'Hatchback'=>'hatchback',
					'Coupe'=>'coupe',
					'Convertible'=>'convertible',
					'Station Wagon'=>'wagon',
					'Wagon/Mini Van'=>'minivans',
					'SUV'=>'suv',
					'Pick up'=>'pickup',
					'Truck'=>'trucks'
			);
		$product_type = strtolower($product_type);
		foreach($product_arr as $key=>$value){
			if($product_type == strtolower($key)){
				return $value;
			}
		}
		return $product_type;
	}

}

