<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Member_join.php
 * 개  요 : 회원가입 Class
  ------------------------------------------------ */

class Member_join {

	var $ci;
	var $mcd;
	var $member_join_config;

	//constructor
	function __construct() {
		//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();

		/* ------------------------------------------------------------
		  - Model Load
		  ------------------------------------------------------------ */
		$this->ci->load->model('mo_member', 'member');
		$this->ci->load->model('mo_bbs', 'user_bbs'); //class명과 동일해서 user_bbs
		//회원가입 메뉴코드
		$this->mcd = $this->ci->data['mcd'];

		//회원가입 설정정보 조회
		$this->member_join_config = $this->ci->iw_config->select_member_join_config()->result();

		//회원가입을 설정을 하였다면..
		if (count($this->member_join_config) > 0) {
			func_set_data($this->ci, 'member_config_result', $this->member_join_config);

			//스킨 이미지 경로(해당 회원가입 이미지 스킨 경로에 존재)
			$skin_images_url = '/views/user/member_join/' . $this->mcd . '/images';
			func_set_data($this->ci, 'skin_images_url', $skin_images_url);

			//상단 HTML
			$html_header = func_get_config($this->member_join_config, 'member_join_html_header');
			func_set_data($this->ci, 'html_header', $html_header);

			//하단 HTML
			$html_footer = func_get_config($this->member_join_config, 'member_join_html_footer');
			func_set_data($this->ci, 'html_footer', $html_footer);
		} else {
			echo func_jsAlertReplace('Your join is failed.', '/', TRUE);
		}
	}

	//index
	function index() {
		if ($this->ci->data['is_login']) {
			redirect('/', 'refresh'); //로그인 되어 있으면..
		}

		func_set_data($this->ci, 'return_url', $this->ci->input->get_post('return_url')); //이전 URL
		//가입여부 체크 페이지 표시여부 ('Y':가입여부 페이지)
		if (func_get_config($this->member_join_config, 'join_check_yn') != 'Y') {
			return $this->agree_check();
		} else {
			return $this->join_check();
		}
	}

	//가입여부 HTML
	function join_check() {
		return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_join_check', $this->ci->data, true);
	}

	//가입여부 EXEC
	function join_check_exec() {
		//이름 + 주민번호 체크
		$member_name = $this->ci->input->get_post('member_name');
		$jumin_no1 = $this->ci->input->get_post('jumin_no1');
		$jumin_no2 = $this->ci->input->get_post('jumin_no2');

		//가입여부 체크
		$cnt = $this->ci->member->member_join_check($member_name, func_base64_encode($jumin_no1 . $jumin_no2));

		//회원가입이 되어 있는 경우
		if ($cnt > 0) {
			echo func_jsAlertReplace('[' . $member_name . '] have already completed our member join.', $this->ci->data['base_url'] . '&mcd=' . $this->ci->data['member_login_mcd']);
			exit;
		}
		//회원가입이 되어 있지 않은 경우
		else {
		//이용약관 + 개인정보취급방침
			redirect($this->ci->data['base_url'] . '&mcd=' . $this->mcd . '&me=agree_check', 'refresh');
		}
	}

	//이용약관 + 개인정보취급방침 HTML
	function agree_check() {
		if ($this->ci->data['is_login'] == 'Y')
			redirect('/', 'refresh'); //로그인 되어 있으면..

		return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_agree_check', $this->ci->data, true);
	}

	//회원가입 등록 HTML
	function member_join_welcome() {
		return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_join_welcome', $this->ci->data, true);
	}

	//회원가입 등록 HTML
	function member_join_insert() {
		if ($this->ci->data['is_login'] == 'Y')
			redirect('/', 'refresh'); //로그인 되어 있으면..

		return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_join_insert', $this->ci->data, true);
	}

	//회원가입 등록 처리
	function member_join_insert_exec() {
		if ($this->ci->data['is_login'] == 'Y')
			redirect('/', 'refresh'); //로그인 되어 있으면..

		$inData = array();
		$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴
		//주민등록번호 가입여부 체크
		if(isset($inData['jumin_no1']) && isset($inData['jumin_no2'])) {
			$cnt = $this->ci->member->member_jumin_check(func_base64_encode($inData['jumin_no1'] . $inData['jumin_no2']));
		} else {
			$cnt = 0;
		}

		//주민등록번호 등록 되어 있는 경우
		if ($cnt > 0) {
			echo func_jsAlertReplace('입력하신 주민등록번호로 이미 회원가입이 되어 있습니다.', $this->ci->data['base_url'] . '&mcd=' . $this->ci->data['member_login_mcd']);
			exit;
		}

		$inData['member_pwd'] = func_base64_encode($inData['member_pwd']);										  //비밀번호

		/*
		if(isset($inData['jumin_no1']) && isset($inData['jumin_no2'])) {
			$inData['jumin_no'] = func_base64_encode($inData['jumin_no1'] . $inData['jumin_no2']);					  //주민번호
		}

		zipcode는 우편번호
		$inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];											//우편번호
		if(isset($inData['com_zipcode1']) && isset($inData['com_zipcode2'])) {
			$inData['com_zipcode'] = $inData['com_zipcode1'] . $inData['com_zipcode2'];								//우편번호
			unset($inData['com_zipcode1'], $inData['com_zipcode2']);
		}
		*/
		
		$inData['grade_no'] = $this->ci->member->member_grade_default_yn();									   //회원등급
		$inData['contract_agree_yn'] = 'Y';																				//약관동의여부
		$inData['certi_yn'] = func_decode(func_get_config($this->member_join_config, 'certi_yn'), 'Y', 'N', 'Y'); //회원인증
		$inData['write_ip'] = $this->ci->input->ip_address();													 //작성자 아이피
		$inData['created_dt'] = func_get_date_time();															   //회원가입일

		// unset($inData['jumin_no1']);
		// unset($inData['jumin_no2']);
		// unset($inData['zipcode1']);
		// unset($inData['zipcode2']);

		//DB 등록
		$idx = $this->ci->member->member_insert($inData);

		//첨부파일이 존재한다면...
		if(isset($_FILES) && count($_FILES) > 0) {
			foreach($_FILES as $Fkey => $Fval) {
				if($Fval['error'] != 4) {
					//파일 업로드
					$upload_str = $this->ci->iwc_common->upload_image($Fkey, $this->mcd);

					//파일 업로드 실패
					if ($upload_str != TRUE) {
						show_error($this->ci->iwc_common->upload_error_msg());
					}
					//파일 업로드 성공
					else {
						$upload_file_info = array();
						$upload_file_info = $this->ci->iwc_common->upload_success_msg();
						$upload_file_info['category'] = $Fkey;
						$upload_file_info['member_no'] = $idx;
						$this->ci->member->insert_attach($upload_file_info);
						//$inData['photo'] = $upload_file_info['file_name']; //사진 파일명
					}
				}
			}
		}

		if ($idx > 0) {
			echo func_jsAlertReplace('you join us.', $this->ci->data['base_url'] . '&mcd=' . $this->ci->data['member_login_mcd']);
			exit;
		} else {
			echo func_jsAlertReplace('Your join is failed.', '/');
			exit;
		}
	}

	//회원가입 수정 HTML
	function member_join_update() {
		//로그인 되어 있으면...
		if ($this->ci->data['is_login'] == 'Y') {
			$member_id = $this->ci->phpsession->get('member_id', 'USER');
			$member_no = $this->ci->phpsession->get('member_no', 'USER');
			$only_attach = $this->ci->input->get_post('only_attach'); //첨부파일 상세 삭제 여부

			$query = $this->ci->member->member_select_detail($member_id);

			//수정폼에서 첨부 파일만 삭제 할 경우....
			if ($only_attach == 'Y') {
				$att_idx = $this->ci->input->get_post('att_idx');
				list($bbs_attach_detail) = $this->ci->member->select_attach_detail($member_no, $att_idx)->result(); //첨부파일 상세 조회
				$this->ci->member->delete_attach($member_no, $att_idx); //첨부파일 삭제
				@unlink($bbs_attach_detail->full_path);
			}

			$member_attachs = $this->ci->member->select_attach($member_no)->result(); //첨부파일 조회(삭제 후 조회)
			if(isset($member_attachs) && count($member_attachs) > 0) {
				foreach($member_attachs as $val) {
					$member_attach_list[$val->category][] = $val;
				}
			} else {
				$member_attach_list = false;
			}
			unset($member_attachs, $val);

			func_set_data($this->ci, 'member_attach_list', $member_attach_list);
			func_set_data($this->ci, 'member_select_detail', $query);

			return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_join_update', $this->ci->data, true);
		} else {
			redirect('/', 'refresh');
		}
	}

	//회원가입 수정 처리
	function member_join_update_exec() {
		//로그인이 되어 있으면
		if ($this->ci->data['is_login'] == 'Y') {

			$inData = array();
			$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴

			if(!isset($inData['email_recv_yn'])) {
				$inData['email_recv_yn'] = 'N';
			}
			if(!isset($inData['sms_recv_yn'])) {
				$inData['sms_recv_yn'] = 'N';
			}

			/*
			if(isset($inData['com_zipcode1']) && isset($inData['com_zipcode2'])) {
				$inData['com_zipcode'] = $inData['com_zipcode1'] . $inData['com_zipcode2'];								//우편번호
				unset($inData['com_zipcode1'], $inData['com_zipcode2']);
			}
			*/

			$inData['member_pwd'] = func_base64_encode($inData['member_pwd']);					 //비밀번호
			//$inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];					   //우편번호
			$inData['updated_dt'] = func_get_date_time();										  //회원수정일
			//unset($inData['zipcode1']);
			//unset($inData['zipcode2']);*/

			//첨부파일이 존재한다면...
			if(isset($_FILES) && count($_FILES) > 0) {
				foreach($_FILES as $Fkey => $Fval) {
					if($Fval['error'] != 4) {
						//파일 업로드
						$upload_str = $this->ci->iwc_common->upload_image($Fkey, $this->mcd);

						//파일 업로드 실패
						if ($upload_str != TRUE) {
							show_error($this->ci->iwc_common->upload_error_msg());
						}
						//파일 업로드 성공
						else {
							$upload_file_info = array();
							$upload_file_info = $this->ci->iwc_common->upload_success_msg();
							$upload_file_info['category'] = $Fkey;
							$upload_file_info['member_no'] = $inData['member_no'];
							$this->ci->member->insert_attach($upload_file_info);
							//$inData['photo'] = $upload_file_info['file_name']; //사진 파일명
						}
					}
				}
			}

			//DB 등록
			$idx = $this->ci->member->member_update($inData);

			echo func_jsAlertReplace('Your information is modified.', '/');
		} else {
			redirect('/', 'refresh');
		}
	}

	// 비밀번호 수정 HTML
	function member_modify_pwd() {
		//로그인 되어 있으면...
		if ($this->ci->data['is_login'] == 'Y') {
			$member_id = $this->ci->phpsession->get('member_id', 'USER');
			$member_no = $this->ci->phpsession->get('member_no', 'USER');

			$query = $this->ci->member->member_select_detail($member_id);

			func_set_data($this->ci, 'member_select_detail', $query);
			
			return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_modify_pwd', $this->ci->data, true);
		} else {
			redirect('/', 'refresh');
		}
	}

	// 비밀번호 수정 처리
	function member_modify_pwd_exec() {
		//로그인이 되어 있으면
		if ($this->ci->data['is_login'] == 'Y') {

			$inData = array();

			//var_dump($this->ci->input->post('values'));
			//exit;

			$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴

			if(!isset($inData['email_recv_yn'])) {
				$inData['email_recv_yn'] = 'N';
			}
			if(!isset($inData['sms_recv_yn'])) {
				$inData['sms_recv_yn'] = 'N';
			}

			$inData['updated_dt'] = func_get_date_time();										 //회원수정일

			$member_id = $this->ci->phpsession->get('member_id', 'USER');
			$member_pwd = func_base64_encode($inData['member_pwd']);
			$query = $this->ci->member->member_chk($member_id, $member_pwd); //사용자 체크

			if ($query->num_rows() > 0) {
				$inData['member_id'] = $member_id;
				$inData['member_pwd'] = func_base64_encode($this->ci->input->get_post('member_new_pwd'));	//비밀번호

				//DB 등록
				$idx = $this->ci->member->member_update($inData);

				echo func_jsAlertReplace('Your password is modified.', '/');
			} else {
				echo func_jsAlertReplace('Please confirm your password.', $this->ci->data['base_url'] . '&mcd=' . $this->mcd . '&me=member_modify_pwd');
			}
		} else {
			//echo func_jsAlertReplace('unexpacted access.', '/');
			redirect('/', 'refresh');
		}
	}

	// 정보 수정 HTML
	function member_modify_info() {

		//로그인 되어 있으면...
		if ($this->ci->data['is_login'] == 'Y') {
			$member_id = $this->ci->phpsession->get('member_id', 'USER');
			$member_no = $this->ci->phpsession->get('member_no', 'USER');
			$only_attach = $this->ci->input->get_post('only_attach'); //첨부파일 상세 삭제 여부

			$query = $this->ci->member->member_select_detail($member_id);

			//수정폼에서 첨부 파일만 삭제 할 경우....
			if ($only_attach == 'Y') {
				$att_idx = $this->ci->input->get_post('att_idx');
				list($bbs_attach_detail) = $this->ci->member->select_attach_detail($member_no, $att_idx)->result(); //첨부파일 상세 조회
				$this->ci->member->delete_attach($member_no, $att_idx); //첨부파일 삭제
				@unlink($bbs_attach_detail->full_path);
			}

			$member_attachs = $this->ci->member->select_attach($member_no)->result(); //첨부파일 조회(삭제 후 조회)
			if(isset($member_attachs) && count($member_attachs) > 0) {
				foreach($member_attachs as $val) {
					$member_attach_list[$val->category][] = $val;
				}
			} else {
				$member_attach_list = false;
			}
			unset($member_attachs, $val);

			func_set_data($this->ci, 'member_attach_list', $member_attach_list);
			func_set_data($this->ci, 'member_select_detail', $query);

			return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_modify_info', $this->ci->data, true);
		} else {
			redirect('/', 'refresh');
		}
	}

	//회원가입 수정 처리
	function member_modify_info_exec() {
		//로그인이 되어 있으면
		if ($this->ci->data['is_login'] == 'Y') {

			$inData = array();
			$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴

			if(!isset($inData['email_recv_yn'])) {
				$inData['email_recv_yn'] = 'N';
			}
			if(!isset($inData['sms_recv_yn'])) {
				$inData['sms_recv_yn'] = 'N';
			}

			/*
			if(isset($inData['com_zipcode1']) && isset($inData['com_zipcode2'])) {
				$inData['com_zipcode'] = $inData['com_zipcode1'] . $inData['com_zipcode2'];								//우편번호
				unset($inData['com_zipcode1'], $inData['com_zipcode2']);
			}
			*/

			//$inData['member_pwd'] = func_base64_encode($inData['member_pwd']);					 //비밀번호
			//$inData['zipcode'] = $inData['zipcode1'] . $inData['zipcode2'];					   //우편번호
			$inData['updated_dt'] = func_get_date_time();										  //회원수정일
			//unset($inData['zipcode1']);
			//unset($inData['zipcode2']);*/

			//첨부파일이 존재한다면...
			if(isset($_FILES) && count($_FILES) > 0) {
				foreach($_FILES as $Fkey => $Fval) {
					if($Fval['error'] != 4) {
						//파일 업로드
						$upload_str = $this->ci->iwc_common->upload_image($Fkey, $this->mcd);

						//파일 업로드 실패
						if ($upload_str != TRUE) {
							show_error($this->ci->iwc_common->upload_error_msg());
						}
						//파일 업로드 성공
						else {
							$upload_file_info = array();
							$upload_file_info = $this->ci->iwc_common->upload_success_msg();
							$upload_file_info['category'] = $Fkey;
							$upload_file_info['member_no'] = $inData['member_no'];
							$this->ci->member->insert_attach($upload_file_info);
							//$inData['photo'] = $upload_file_info['file_name']; //사진 파일명
						}
					}
				}
			}

			//var_dump($this->ci->input->get_post('member_pwd'));
			//exit;

			$member_id = $this->ci->phpsession->get('member_id', 'USER');
			$member_pwd = func_base64_encode($this->ci->input->get_post('member_pwd'));
			$query = $this->ci->member->member_chk($member_id, $member_pwd); //사용자 체크

			if ($query->num_rows() > 0) {
				//DB 등록
				$idx = $this->ci->member->member_update($inData);

				echo func_jsAlertReplace('Your information is modified.', '/');
			} else {
				echo func_jsAlertReplace('Please confirm your password.', $this->ci->data['base_url'] . '&mcd=' . $this->mcd . '&me=member_modify_info');
			}
		} else {
			redirect('/', 'refresh');
		}
	}

	//회원 탈퇴 처리
	function member_join_delete_exec() {

		//로그인 되어 있으면...
		if ($this->ci->data['is_login'] == 'Y') {
			$member_no = $this->ci->input->get_post('member_no');

			if ($member_no != '') {
				$inData = array();
				$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴
				//비밀번호 비교 체크
				$pass_cnt = $this->ci->member->member_password_check($member_no, func_base64_encode($inData['member_pwd']));

				if ($pass_cnt > 0) {
					//DB 삭제
					$idx = $this->ci->member->member_delete($member_no);

					//세션 제거
					$this->ci->phpsession->clear(null, 'USER');

					echo func_jsAlertReplace('Your secession is completed.', '/');
				} else {
					echo func_jsAlertReplace('Please confirm your password.', '/');
					exit;
				}
			} else {
				redirect('/', 'refresh');
			}
		} else {
			redirect('/', 'refresh');
		}
	}

	/*
	  function member_delete() {

	  //로그인 되어 있으면...
	  if($this->ci->data['is_login'] == 'Y') {
	  $member_no = $this->ci->input->get_post('member_no');

	  //DB 삭제
	  $idx = $this->ci->member->member_delete_adm($member_no);

	  echo func_jsAlertReplace('삭제되었습니다.', '/');
	  }
	  else
	  {
	  redirect('/', 'refresh');
	  }

	  }
	 */

	//회원 마이페이지 HTML
	function member_join_mypage() {
		//로그인 되어 있으면...
		if ($this->ci->data['is_login'] == 'Y') {
			//회원 정보 상세 조회
			$member_id = $this->ci->phpsession->get('member_id', 'USER');

			$query = $this->ci->member->member_select_detail($member_id);

			func_set_data($this->ci, 'member_select_detail', $query);

			//회원 등록 글 조회
			$this->member_join_bbs_select($member_id, 5, 'N');

			return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_join_mypage', $this->ci->data, true);
		} else {
			redirect('/', 'refresh');
		}
	}

	//회원 마이페이지 게시판 리스트 HTML
	function member_join_bbs_list() {
		//로그인 되어 있으면...
		if ($this->ci->data['is_login'] == 'Y') {
			//회원 정보 상세 조회
			$member_id = $this->ci->phpsession->get('member_id', 'USER');

			$query = $this->ci->member->member_select_detail($member_id);

			func_set_data($this->ci, 'member_select_detail', $query);

			//회원 등록 글 조회
			$this->member_join_bbs_select($member_id, 10, 'N');

			return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_join_bbs_list', $this->ci->data, true);
		} else {
			redirect('/', 'refresh');
		}
	}

	//회원 게시판 등록 글 조회
	function member_join_bbs_select($member_id, $rows, $prev_next = 'N') {
		$where = array(); //sql where 조건
		$sParam = '';	  //검색 string
		//추가필드(field1, field2) 검색
		$field1 = $this->ci->input->get_post('field1');
		$field2 = $this->ci->input->get_post('field2');
		if ($field1 != '') {
			$sParam .= '&field1=' . $field1;
			$where['field1 ='] = $field1;
		}
		if ($field2 != '') {
			$sParam .= '&field2=' . $field2;
			$where['field2 ='] = $field2;
		}

		/*
		  //카테고리 검색
		  $category_yn = func_get_config($this->bbs_config, 'category_yn'); //카테고리 검색
		  $category    = $this->ci->input->get_post('category'); //카테고리 코드
		  if($category_yn != 'N' && $category != '') {
		  $sParam .= '&category='.$category;
		  $where['category ='] = $category;
		  }
		 */

		//날짜 검색
		$sch_create_dt_s = $this->ci->input->get_post('sch_create_dt_s'); //등록일 시작
		if ($sch_create_dt_s != '') {
			$sParam .= '&sch_create_dt_s=' . $sch_create_dt_s;
			$where['created_dt >='] = $sch_create_dt_s . " 00:00:01";
		}
		$sch_create_dt_e = $this->ci->input->get_post('sch_create_dt_e'); //등록일 종료
		if ($sch_create_dt_e != '') {
			$sParam .= '&sch_create_dt_e=' . $sch_create_dt_e;
			$where['created_dt <='] = $sch_create_dt_e . " 23:59:59";
		}

		$sch_condition = $this->ci->input->get_post('sch_condition');   //검색조건
		$sch_word = $this->ci->input->get_post('sch_word');		//검색어
		//검색조건 + 검색어
		if ($sch_condition != '' && $sch_word != '') {
			$sch_condition_tmp = explode('|', $sch_condition);

			//다중 조건인 경우(제목 + 내용)
			if (count($sch_condition_tmp) > 1) {
				$tmpStr = '';
				for ($i = 0; $i < count($sch_condition_tmp); $i++) {
					if ($tmpStr != '') {
						$tmpStr .= ' or ';
					}

					$tmpStr .= $sch_condition_tmp[$i] . ' ? ';
					$where['IWSEARCHVALS' . $i] = '%' . $sch_word . '%';
				}

				$tmpStr = 'IWSEARCHKEY(' . $tmpStr . ')';
				$where[$tmpStr] = '';

				$sParam .= '&sch_condition=' . $sch_condition;
				$sParam .= '&sch_word=' . $sch_word;
			} else {
				$where[$sch_condition] = '%' . $sch_word . '%';
				$sParam .= '&sch_condition=' . $sch_condition;
				$sParam .= '&sch_word=' . $sch_word;
			}
		}

		//페이지 조건
		$cur_page = $this->ci->input->get_post("cur_page");
		if ($cur_page == NULL || $cur_page == '')
			$cur_page = 1;
		$page_rows = $rows;  //페이지 로우수
		$page_views = 10; //페이지 네비게이션 수
		//게시판 공지 목록 조회
		$bbs_notice_list = $this->ci->member->bbs_select($member_id, 'Y', ($cur_page - 1) * $page_rows, $page_rows, $where);

		//게시판 일반 목록 조회
		$bbs_list = $this->ci->member->bbs_select($member_id, 'N', ($cur_page - 1) * $page_rows, $page_rows, $where);

		$total_rows = $this->ci->member->total_bbs_select($member_id, $where); //조회 전체 목록수
		$row_cnt = $total_rows - ($cur_page - 1) * $page_rows;			  //글 번호
		//페이지정보 조회
		$page_info = func_get_page_info($total_rows, $page_views, $page_rows, $cur_page);

		//이전글 다음글 조회
		if ($prev_next == 'Y') {
			$pn_data = array();
			$pn_data['isPrev'] = '';

			$prev = ($cur_page - 1) * $page_rows;

			if ($prev > 0) {
				$pn_data['isPrev'] = "PREV";
				$prev = $prev - 1;
			}

			$pn_data['cur_page'] = $cur_page;
			$pn_data['page_views'] = $page_views;
			$pn_data['page_rows'] = $page_rows;
			$pn_data['total_rows'] = $total_rows;
			$pn_data['row_cnt'] = $row_cnt;

			if ($pn_data['isPrev'] == 'PREV') {
				$page_rows = $page_rows + 2;
			} else {
				$page_rows = $page_rows + 1;
			}
			$pn_data['prevList'] = $this->ci->member->select($member_id, 'N', $prev, $page_rows, $where)->result();

			return $pn_data;
		}


		func_set_data($this->ci, 'total_rows', $total_rows);   //조회 전체 목록수
		func_set_data($this->ci, 'total_page', $page_info[0]); //전체 페이지
		func_set_data($this->ci, 'row_cnt', $row_cnt);		 //글번호
		func_set_data($this->ci, 'cur_page', $cur_page);	   //현재 페이지
		func_set_data($this->ci, 'sParam', $sParam);		   //검색 string
		func_set_data($this->ci, 'bbs_notice_list', $bbs_notice_list->result());
		func_set_data($this->ci, 'bbs_list', $bbs_list->result());
		func_set_data($this->ci, 'page_info', $page_info);
	}

	/*
	// 정보 수정 HTML
	function member_modify() {
		if ($this->ci->data['is_login'] != 'Y')
			redirect('/', 'refresh'); //로그인 되어 있지 않으면..

		return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_modify', $this->ci->data, true);
	}
	*/
	// 탈퇴 HTML
	function member_leave() {
		if ($this->ci->data['is_login'] != 'Y')
			redirect('/', 'refresh'); //로그인 되어 있지 않으면..

		return $this->ci->load->view('user/member_join' . '/' . $this->mcd . '/member_leave', $this->ci->data, true);
	}

	// 탈퇴 HTML
	function member_leave_exec() {

		$inData = array();
		$inData = func_get_post_array($this->ci->input->post('values'));  //POST 배열을 array['key'] = value 형태로 리턴
		$inData['member_id'] = $this->ci->phpsession->get('member_id', 'USER');

		
		var_dump($inData);
		exit;



		//DB 등록
		$idx = $this->ci->member->member_leave_insert($inData);

		echo func_jsAlertReplace('bye', '/', TRUE);
	}

}
?>