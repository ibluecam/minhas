<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Iwc_site_meta.php
 * 개  요 : 사이트 메타테그를 관리한다.
  ------------------------------------------------ */

class Iwc_site_meta {

	var $ci;

//constructor
	function __construct() {
//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();
	}

//사이트 메타 테그 HTML
	function view_site_meta() {
		$config_type = 'site_meta'; //설정 구분
//해당 config 조회
		$select = $this->ci->iw_config->select($config_type);

		func_set_data($this->ci, 'iw_config_result', $select->result());

//컨텐츠
		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/site_meta/iwc_site_meta', $this->ci->data, true));
		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);
	}

//사이트 메타 테그 EXEC
	function exec_site_meta() {
		$exec_cd = ''; //입력&수정 구분

		$inData = array();
		$inData['config_type'] = 'site_meta';
		$inData['config_type_name'] = '사이트 메타테그 관리';

		$array_values = $this->ci->input->post('values');  //폼값
//key 값을 array로 리턴
		$keys = func_get_array_keys($array_values);

		for ($i = 0; $i < count($keys); $i++) {
			$inData['key'] = $keys[$i];
			$inData['value'] = $array_values[$keys[$i]];
			$inData['desc'] = '';
			$inData['created_dt'] = func_get_date_time();

			$tmp = $this->ci->iw_config->select_detail($inData['config_type'], $inData['key']);

			if ($tmp->num_rows() > 0)
				$exec_cd = 'UPDATE';
			else
				$exec_cd = 'INSERT';

			if ($exec_cd == 'INSERT') {
				$this->ci->iw_config->insert($inData);
			} else {
				$this->ci->iw_config->update($inData);
			}
		}

		echo func_jsAlertReplace('사이트 메타테그 정보가 입력 되었습니다', '/?c=iwc&amp;m=view_site_meta');
	}

}
?>