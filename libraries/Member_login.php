<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Member_login.php
 * 개  요 : 로그인 Class
  ------------------------------------------------ */

class Member_login {

    var $ci;
    var $mcd;
    var $member_join_config;
    var $member_login_config;

    //constructor
    function __construct() {
        //CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
        $this->ci = &get_instance();

        /* ------------------------------------------------------------
          - Model Load
          ------------------------------------------------------------ */
        $this->ci->load->model('mo_member', 'member');

        //로그인 메뉴코드
        $this->mcd = $this->ci->data['mcd'];

        //회원가입 설정정보 조회
        $this->member_join_config = $this->ci->iw_config->select_member_join_config()->result();

        //로그인 설정정보 조회
        $this->member_login_config = $this->ci->iw_config->select_member_login_config()->result();

        //로그인을 설정을 하였다면..
        if (count($this->member_login_config) > 0) {
            func_set_data($this->ci, 'member_config_result', $this->member_login_config);

            //스킨 이미지 경로(해당 회원가입 이미지 스킨 경로에 존재)
            $skin_images_url = '/views/user/member_login/' . $this->mcd . '/images';
            func_set_data($this->ci, 'skin_images_url', $skin_images_url);

            //상단 HTML
            $html_header = func_get_config($this->member_login_config, 'member_login_html_header');
            func_set_data($this->ci, 'html_header', $html_header);

            //하단 HTML
            $html_footer = func_get_config($this->member_login_config, 'member_login_html_footer');
            func_set_data($this->ci, 'html_footer', $html_footer);
        } else {
            echo func_jsAlertReplace('로그인이 생성되지 않았습니다.\n\n관리자에게 문의하세요.', '/', TRUE);
        }
    }

    //index
    function index() {
        return $this->login();
    }

    //로그인 HTML
    // function login() {
    //     if ($this->ci->data['is_login'] == 'Y')
    //         redirect('/', 'refresh'); //로그인 되어 있으면..

    //     func_set_data($this->ci, 'return_url', $this->ci->input->get_post('return_url')); //이전 URL

    //     return $this->ci->load->view('user/member_login' . '/' . $this->mcd . '/member_login', $this->ci->data, true);
    // }

    //로그인 EXEC
    // function login_exec() {
    //     if ($this->ci->data['is_login'] == 'Y')
    //         redirect('/', 'refresh'); //로그인 되어 있으면..

    //     $member_id = ''; //아이t
    //     $member_pwd = ''; //패스워드
    //     $return_url = ''; //이전 URL

    //     $member_id = $this->ci->input->get_post('member_id', TRUE);
    //     $member_pwd = func_base64_encode($this->ci->input->get_post('member_pwd', TRUE));
    //     $return_url = $this->ci->input->get_post('return_url', TRUE);
    //     $query = $this->ci->member->member_chk($member_id, $member_pwd);
    //     $grad_no = mysql_query("SELECT grade_no FROM iw_member WHERE member_id = '{$member_id}' and member_pwd = '{$member_pwd}' and grade_no = '15'  ");
    //     $check_grand_no = mysql_num_rows($grad_no);
    //     if ($check_grand_no == 0) {
    //         if ($query->num_rows() > 0) {
    //             $row = $query->row();

    //             //회원가입 설정에서 관리자 인증을 사용한 경우
    //             if (func_get_config($this->member_join_config, 'certi_yn') == 'Y') {
    //                 //미인증 회원인 경우
    //                 if ($row->certi_yn != 'Y') {
    //                     //관리자 인증 처리 중입니다.\n인증 처리 완료 후 로그인이 가능합니다
    //                     echo func_jsAlertReplace('The authentication process of the administrator.\n Login is possible after the completion of the authentication process.', $this->ci->data['base_url'] . '&mcd=' . $this->mcd);
    //                     exit;
    //                 }
    //             }

    //             //세션 생성
    //             $this->ci->phpsession->save('member_no', $row->member_no, 'USER');
    //             $this->ci->phpsession->save('member_id', $row->member_id, 'USER');
    //             $this->ci->phpsession->save('member_first_name', $row->member_first_name, 'USER');
    //             $this->ci->phpsession->save('member_last_name', $row->member_last_name, 'USER');
    //             //$this->ci->phpsession->save('email', $row->email, 'USER');
    //             $this->ci->phpsession->save('grade_no', $row->grade_no, 'USER');
    //             $this->ci->phpsession->save('grade_name', $row->grade_name, 'USER');

    //             //로그인 COUNT UPDATE
    //             $this->ci->member->login_count_update($member_id);
    //             //로그인 DATE UPDATE
    //             $this->ci->member->login_date_update($member_id, func_get_date_time());

    //             //이전 URL이 없다면
    //             if ($return_url == '') {
    //                 //메인 페이지
    //                 redirect('/', 'refresh');
    //             } else {
    //                 redirect($return_url, 'refresh');
    //             }
    //         } else {
    //             $url = (("$_SERVER[HTTP_HOST]"=='iblauda.com') ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    //             echo func_jsAlertReplace('You have entered the wrong username or password.', $url.$this->ci->data['base_url'] . '&mcd=' . $this->mcd);
    //             //echo func_jsAlertReplace('You have entered the wrong username or password.', $this->ci->data['base_url'] . '&mcd=' . $this->mcd);
    //         }
    //     } else {
           
    //             echo func_jsAlertReplace('Failed to login. Your account have not verified from your email yet.', $this->ci->data['base_url'] . '&mcd=' . $this->mcd);
    //     }
    // }

    //로그아웃 EXEC
    function logout_exec() {
        $this->ci->phpsession->clear(null, 'USER');
        redirect('/', 'refresh'); //메인 페이지
    }

    //아이디&비밀번호 찾기 HTML
    function search_member_account() {
        if ($this->ci->data['is_login'] == 'Y')
            redirect('/', 'refresh'); //로그인 되어 있으면..

        return $this->ci->load->view('user/member_login' . '/' . $this->mcd . '/member_search_account', $this->ci->data, true);
    }

    //아이디 찾기 EXEC
    function search_member_account_id() {
        if ($this->ci->data['is_login'] == 'Y')
            redirect('/', 'refresh'); //로그인 되어 있으면..







            
//이름 + 주민번호 체크
        $member_name = $this->ci->input->get_post('member_name');
        $jumin_no1 = $this->ci->input->get_post('jumin_no1');
        $jumin_no2 = $this->ci->input->get_post('jumin_no2');

        //아이디 체크
        $query = $this->ci->member->member_account_id($member_name, func_base64_encode($jumin_no1 . $jumin_no2));

        //아이디가 존재할 경우
        if ($query->num_rows() > 0) {
            $row = $query->row();

            func_set_data($this->ci, 'member_id', $row->member_id);

            return $this->ci->load->view('user/member_login' . '/' . $this->mcd . '/member_search_account_id_result', $this->ci->data, true);
        }
        //아이디가 존재하지 않을 경우
        else {
            //아이디&비밀번호 찾기 폼
            echo func_jsAlertReplace('아이디 정보가 존재하지 않습니다.', $this->ci->data['base_url'] . '&mcd=' . $this->mcd . '&me=search_member_account');
        }
    }

    //비밀번호 찾기 EXEC
    function search_member_account_pwd() {
        if ($this->ci->data['is_login'] == 'Y')
            redirect('/', 'refresh'); //로그인 되어 있으면..







            
// 아이디 + 성명 + 주민번호 체크
        $member_first_name = $this->ci->input->get_post('member_first_name');
        $member_last_name = $this->ci->input->get_post('member_last_name');
        $member_id = $this->ci->input->get_post('member_id');

        // 비밀번호 체크
        // $query = $this->ci->member->member_account_pwd($member_id, $member_name, func_base64_encode($jumin_no1 . $jumin_no2));
        $query = $this->ci->member->member_account_pwd($member_id, $member_first_name, $member_last_name);

        //비밀번호가 존재할 경우
        if ($query->num_rows() > 0) {
            $row = $query->row();

            func_set_data($this->ci, 'member_pwd', func_base64_decode($row->member_pwd));

            //비밀번호 메일 발송여부
            if (func_get_config($this->member_login_config, 'passwd_mail_yn') == 'Y') {
                if (false) {

                    $this->load->library('email');

                    $this->email->from('your@example.com', 'Your Name');
                    $this->email->to('someone@example.com');
                    //$this->email->cc('another@another-example.com'); 
                    //$this->email->bcc('them@their-example.com'); 

                    $this->email->subject('Email Test');
                    $this->email->message('Testing the email class.');

                    $this->email->send();

                    echo $this->email->print_debugger();
                } else {
                    $this->ci->load->library('sendmail');  //메일

                    func_set_data($this->ci, 'http_url', $this->ci->config->item('base_url'));
                    //func_set_data($this->ci, 'member_name', $row->member_name);
                    func_set_data($this->ci, 'member_first_name', $row->member_first_name);
                    func_set_data($this->ci, 'member_last_name', $row->member_last_name);
                    func_set_data($this->ci, 'member_id', $row->member_id);
                    func_set_data($this->ci, 'member_pwd', func_base64_decode($row->member_pwd));

                    //$send_mail = func_get_config($this->member_login_config, 'mail_addr'); //보내는 메일
                    //$send_name = func_get_config($this->member_login_config, 'mail_name'); //보내는 사람
                    $send_mail = 'ttcopy@blauda.com';
                    $send_name = 'BLAUDA CO., LTD.';

                    //$to_mail = $row->email;											  //받는 이메일
                    $to_mail = $row->member_id;             //받는 이메일
                    $subject = '[' . $row->member_first_name . '] 님의 비밀번호 정보 입니다.';  //제목
                    //이메일 본문
                    //$contents = $this->ci->load->view("common/member_pwd_find_email", $this->ci->data, true);
                    $contents = $this->ci->load->view("user/member_login/LOGIN/member_find_password_mail", $this->ci->data, true);

                    //메일발송
                    $this->ci->sendmail->clear(TRUE);   //모든 이메일 변수들을 초기화  TRUE 시 첨부파일도 초기화
                    $this->ci->sendmail->setMailType("html");
                    $this->ci->sendmail->setInit();
                    $this->ci->sendmail->setFrom($send_mail, $send_name);
                    $this->ci->sendmail->setTo($to_mail);
                    $this->ci->sendmail->setSubject($subject);
                    $this->ci->sendmail->setMessage($contents);
                    $mail_result = $this->ci->sendmail->send(); //전송여부 TRUE FALSE 반환
                    //var_dump($send_mail);
                    //var_dump($send_name);
                    //var_dump($subject);
                    //var_dump($contents);
                    //var_dump($mail_result);
                    //exit;
                    //echo $this->ci->sendmail->printDebugger();
                }
            }

            //return $this->ci->load->view('user/member_login' . '/' . $this->mcd . '/member_search_account_pwd_result', $this->ci->data, true);
            return $this->ci->load->view('user/member_login' . '/' . $this->mcd . '/member_find_password_result', $this->ci->data, true);
        } else {
            //아이디&비밀번호 찾기 폼
            //echo func_jsAlertReplace($member_first_name . '(' . $member_id . ') 님의 비밀번호 정보가 존재하지 않습니다.', $this->ci->data['base_url'] . '&mcd=' . $this->mcd . '&me=search_member_account');
            echo func_jsAlertReplace($member_first_name . '(' . $member_id . ') 님의 비밀번호 정보가 존재하지 않습니다.', $this->ci->data['base_url'] . '&mcd=' . $this->mcd . '&me=member_find_password');
        }
    }

    //아이디&비밀번호 찾기 HTML
    function member_find_password() {
        if ($this->ci->data['is_login'] == 'Y')
            redirect('/', 'refresh'); //로그인 되어 있으면..

        return $this->ci->load->view('user/member_login' . '/' . $this->mcd . '/member_find_password', $this->ci->data, true);
    }

    //아이디&비밀번호 찾기 HTML
    function member_search_account_pwd_result() {
        if ($this->ci->data['is_login'] == 'Y')
            redirect('/', 'refresh'); //로그인 되어 있으면..

        return $this->ci->load->view('user/member_login' . '/' . $this->mcd . '/member_search_account_pwd_result', $this->ci->data, true);
    }

    function member_search_account() {
        if ($this->ci->data['is_login'] == 'Y')
            redirect('/', 'refresh'); //로그인 되어 있으면..

        return $this->ci->load->view('user/member_login' . '/' . $this->mcd . '/member_search_account', $this->ci->data, true);
    }

    function member_find_password_mail() {
        if ($this->ci->data['is_login'] == 'Y')
            redirect('/', 'refresh'); //로그인 되어 있으면..

        return $this->ci->load->view('user/member_login' . '/' . $this->mcd . '/member_find_password_mail', $this->ci->data, true);
    }

    //아이디&비밀번호 찾기 HTML
    function member_mypage() {
        if ($this->ci->data['is_login'] != 'Y')
            redirect('/', 'refresh'); //로그인 되어 있지 않으면..

        return $this->ci->load->view('user/member_login' . '/' . $this->mcd . '/member_mypage', $this->ci->data, true);
    }

}

?>
