<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* ------------------------------------------------
 * 파일명 : Iwc_bbs.php
 * 개  요 : IWC 게시판을 설정한다.
  ------------------------------------------------ */

class Iwc_bbs {

	var $ci;
	var $mcd;
	var $mcd_name;

	//constructor
	function __construct() {
		//CodeIgniter의 클래스를 사용하기 위해 CodeIgniter객체를 변수로 할당
		$this->ci = &get_instance();
	}

	//게시판 설정 정보를 조회한다.
	function select_bbs_config() {
		//게시판 설정정보 조회
		$query = $this->ci->iw_config->select_bbs_config($this->ci->data['mcd']);

		//게시판 설정에 필요한 회원등급 조회
		$member_grade_query = $this->ci->iw_config->member_grade_config();

		func_set_data($this->ci, 'bbs_config_result', $query->result()); //해당 게시판 설정 정보 조회
		func_set_data($this->ci, 'member_grade_result', $member_grade_query->result()); //게시판 설정에 필요한 회원등급 조회

		func_set_data($this->ci, 'iwc_contents', $this->ci->load->view('admin/iwc/bbs_config/iwc_bbs_config', $this->ci->data, true));

		$this->ci->load->view('admin/iwc_index', $this->ci->data, false);
	}

	//게시판 설정 정보 EXEC
	function exec_bbs_config() {
		$inData = array();
		$inData['bbs_code'] = $this->ci->data['mcd']; //게시판 코드

		$array_values = $this->ci->input->post('values');  //폼값
		$skin_cover = $this->ci->input->post('skin_cover'); //덮어쓰기
		//key 값을 array로 리턴
		$keys = func_get_array_keys($array_values);

		//설정 정보 DB 등록
		for ($i = 0; $i < count($keys); $i++) {
			$inData['key'] = $keys[$i];
			$inData['value'] = $array_values[$keys[$i]];
			$inData['created_dt'] = func_get_date_time();

			$tmp = $this->ci->iw_config->select_detail_bbs_config($inData['bbs_code'], $inData['key']);

			if ($tmp->num_rows() > 0) {
				$this->ci->iw_config->update_bbs_config($inData);
			} else {
				$this->ci->iw_config->insert_bbs_config($inData);
			}
		}

		//게시판 설정 값
		$bbs_conf = func_get_post_array($this->ci->input->post('values')); 			//POST 배열을 array['key'] = value 형태로 리턴
		//관리자 게시판 메뉴코드 폴더가 존재하지 않으면...
		if (!is_dir('views/admin/adm/bbs/' . $this->ci->data['mcd'])) {
			//관리자 게시판 메뉴코드 폴더 생성
			func_make_dir('views/admin/adm/bbs/' . $this->ci->data['mcd']);

			//관리자 게시판 스킨 파일 복사
			func_file_copy('views/admin/adm/bbs/' . $bbs_conf['bbs_type'], 'views/admin/adm/bbs/' . $this->ci->data['mcd']);
		}

		//사용자 게시판 메뉴코드 폴더가 존재하지 않으면...
		if (!is_dir('views/user/bbs/' . $this->ci->data['mcd'])) {
			$tmp = explode(':', $bbs_conf['bbs_type_skin']);
			$skin_folder = $tmp[1]; 			//tmp[0]->스킨타입(일반형, 앨범형), tmp[1]->스킨명(스킨폴더명)
			//사용자 게시판 메뉴코드 폴더 및 이미지 폴더 생성
			func_make_dir('views/user/bbs/' . $this->ci->data['mcd']);
			func_make_dir('views/user/bbs/' . $this->ci->data['mcd'] . '/images');

			//사용자 게시판 스킨 파일 및 이미지 파일 복사
			func_file_copy('views/user/bbs/' . $bbs_conf['bbs_type'] . '/' . $skin_folder, 'views/user/bbs/' . $this->ci->data['mcd']);
			func_file_copy('views/user/bbs/' . $bbs_conf['bbs_type'] . '/' . $skin_folder . '/images', 'views/user/bbs/' . $this->ci->data['mcd'] . '/images');
		}

		//써머리를 사용한다면..
		if ($bbs_conf['summary_yn'] == 'Y') {
			if (!is_dir('views/user/bbs/summary_' . $this->ci->data['mcd'])) {
			//사용자 써머리 메뉴코드 폴더 생성
				func_make_dir('views/user/bbs/summary_' . $this->ci->data['mcd']);

			//사용자 써머리 스킨 파일 복사
				func_file_copy('views/user/bbs/_summary', 'views/user/bbs/summary_' . $this->ci->data['mcd']);
			}
		}

		//스킨 덮어쓰기 선택시
		if (isset($skin_cover)) {
			if ($skin_cover == 'Y') {
				//관리자 디렉토리 경로
				$sel_dir = 'views/admin/adm/bbs/' . $this->ci->data['mcd'];
				//관리자 파일 삭제
				delete_files($sel_dir, TRUE);
				//관리자 디렉토리 삭제
				@rmdir($sel_dir);

				unset($sel_dir);

				//사용자 디렉토리 경로
				$sel_dir = 'views/user/bbs/' . $this->ci->data['mcd'];
				//사용자 파일 삭제
				delete_files($sel_dir, TRUE);
				//사용자 디렉토리 삭제
				@rmdir($sel_dir);

				unset($sel_dir);



				//관리자 게시판 메뉴코드 폴더 생성
				func_make_dir('views/admin/adm/bbs/' . $this->ci->data['mcd']);

				//관리자 게시판 스킨 파일 복사
				func_file_copy('views/admin/adm/bbs/' . $bbs_conf['bbs_type'], 'views/admin/adm/bbs/' . $this->ci->data['mcd']);

				$tmp = explode(':', $bbs_conf['bbs_type_skin']);
				$skin_folder = $tmp[1]; 				//tmp[0]->스킨타입(일반형, 앨범형), tmp[1]->스킨명(스킨폴더명)
				//사용자 게시판 메뉴코드 폴더 및 이미지 폴더 생성
				func_make_dir('views/user/bbs/' . $this->ci->data['mcd']);
				func_make_dir('views/user/bbs/' . $this->ci->data['mcd'] . '/images');

				//사용자 게시판 스킨 파일 및 이미지 파일 복사
				func_file_copy('views/user/bbs/' . $bbs_conf['bbs_type'] . '/' . $skin_folder, 'views/user/bbs/' . $this->ci->data['mcd']);
				func_file_copy('views/user/bbs/' . $bbs_conf['bbs_type'] . '/' . $skin_folder . '/images', 'views/user/bbs/' . $this->ci->data['mcd'] . '/images');

				//써머리를 사용한다면..
				if ($bbs_conf['summary_yn'] == 'Y') {
					//사용자 써머리 디렉토리 경로
					$sel_dir = 'views/user/bbs/summary_' . $this->ci->data['mcd'];
					//사용자 써머리 파일 삭제
					delete_files($sel_dir, TRUE);
					//사용자 써머리 디렉토리 삭제
					@rmdir($sel_dir);

					//사용자 써머리 메뉴코드 폴더 생성
					func_make_dir('views/user/bbs/summary_' . $this->ci->data['mcd']);

					//사용자 써머리 스킨 파일 복사
					func_file_copy('views/user/bbs/_summary', 'views/user/bbs/summary_' . $this->ci->data['mcd']);
				}
			}
		}

		//리턴 URL
		$return_url = '/?c=iwc&amp;m=view_bbs_config&amp;mcd=' . $this->ci->data['mcd'] . '&amp;mcd_name=' . urlencode($this->ci->data['mcd_name']);
		echo func_jsAlertReplace('게시판 관리 설정 정보가 등록 되었습니다.', $return_url);
	}

	//게시판 설정 정보 COPY
	function copy_bbs_config() {
		$copy_bbs_code = $this->ci->input->get_post('bbs_config_copy'); 		//복사할 게시판 코드/명
		//복사할 설정 정보 게시판을 선택 했으면...
		if ($copy_bbs_code != '') {
			//현재 게시판 설정 정보 삭제
			$this->ci->iw_config->delete_bbs_config($this->ci->data['mcd']);

			//복사할 게시판 설정정보 조회
			$query = $this->ci->iw_config->select_bbs_config($copy_bbs_code);

			$inData = array();
			$inData['bbs_code'] = $this->ci->data['mcd']; 			//현재 게시판 코드

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $rows) {
					if ($rows->key == 'bbs_code_name') {
						$inData['key'] = $rows->key;
						$inData['value'] = $this->ci->data['mcd_name'];
					} else {
						$inData['key'] = $rows->key;
						$inData['value'] = $rows->value;
					}
					$inData['created_dt'] = func_get_date_time();

					$this->ci->iw_config->insert_bbs_config($inData);
				}
			}
		}

		//리턴 URL
		$return_url = '/?c=iwc&amp;m=view_bbs_config&amp;mcd=' . $this->ci->data['mcd'] . '&amp;mcd_name=' . urlencode($this->ci->data['mcd_name']);
		echo func_jsAlertReplace('게시판 관리 설정 정보가 복사 되었습니다.\n스킨 복사를 위해 하단의 확인 버튼을 클릭하세요.', $return_url);
	}

}
?>